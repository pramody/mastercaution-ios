//
//  Plotter.h
//  HealthWatch
//
//  Created by METSL MAC MINI on 24/02/16.
//  Copyright © 2016 METSL MAC MINI. All rights reserved.
//

#import <CoreText/CoreText.h>

@interface Plotter : NSObject
{
    
}
+(void)drawPDF :(NSString*)pdffilename :(NSString*)fileName :(int)Mcdlead :(int)leadconfig :(NSString *)EventCode :(NSString *)EventValue :(int)Respiration;
@end
