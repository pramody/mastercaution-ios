//
//  ViewRecordedEcgViewController.m
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 24/06/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import "ViewRecordedEcgViewController.h"
#import "WCFUpload.h"
#import <QuartzCore/QuartzCore.h>
#import "SDCAlertView.h"
#import "SDCAlertViewController.h"
#import <UIView+SDCAutoLayout.h>
#import "Plotter.h"
#import "CommonHelper.h"



@interface ViewRecordedEcgViewController ()
{
   
    
}
@property (nonatomic, assign) BOOL wrap;
@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) NSMutableArray *itemsdata;
@property (nonatomic, strong) NSMutableArray *itemsunit;
@property (nonatomic, strong) NSMutableArray *itemsimage;


@end

@implementation ViewRecordedEcgViewController


@synthesize carousel,scrollView;
@synthesize wrap;
@synthesize items,postureimage,itemsdata,itemsunit,itemsimage,ecgpath,spinnerView;
@synthesize HR1,Resprate,RR,Temp,PR,QRS,QT,QTC,Posture,Eventcode,EventValue,EventDateTime,test_smtp_message,btnSendEmail,_readerViewController;

int leadCount1 = 13;
int sampleRate1 = 125;
int respiration1=0;
int carousalrespiration1=0;
int mcdLead=12;



-(void)viewDidLoad
{
    _readerViewController=nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    
    AppDelegate *  appDelegate =(AppDelegate *)UIApplication.sharedApplication.delegate ;
    appDelegate.orientationKey = @"PRO";
    [appDelegate forceOrientationTakePlace:appDelegate.orientationKey];
    self.carousel.type = iCarouselTypeLinear;// iCarouselTypeCoverFlow2;
    [self.carousel scrollToItemAtIndex:2 animated:YES];
    [self removefilename];
   
    [self plotting:Eventcode :EventDateTime :EventValue :ecgpath];
    
    [self startLoad];
}


-(void)removefilename
{
    NSString* fileName = @"ECG.PDF";
    
    NSArray *arrayPaths =
    NSSearchPathForDirectoriesInDomains(
                                        NSDocumentDirectory,
                                        NSUserDomainMask,
                                        YES);
    NSString *path = [arrayPaths objectAtIndex:0];
    NSString* pdfFileName = [path stringByAppendingPathComponent:fileName];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if([fileManager fileExistsAtPath:pdfFileName])
     [fileManager removeItemAtPath:pdfFileName error:nil];
}

-(NSString*)getPDFFileName
{
    NSString* fileName = @"ECG.PDF";
    
    NSArray *arrayPaths =
    NSSearchPathForDirectoriesInDomains(
                                        NSDocumentDirectory,
                                        NSUserDomainMask,
                                        YES);
    NSString *path = [arrayPaths objectAtIndex:0];
    NSString* pdfFileName = [path stringByAppendingPathComponent:fileName];
    
    return pdfFileName;
    
}

-(void)showPDFFile
{
    NSString* fileName = @"ECG.PDF";
    
    NSArray *arrayPaths =
    NSSearchPathForDirectoriesInDomains(
                                        NSDocumentDirectory,
                                        NSUserDomainMask,
                                        YES);
    NSString *path = [arrayPaths objectAtIndex:0];
    NSString* pdfFileName = [path stringByAppendingPathComponent:fileName];
     //NSURL *url = [NSURL fileURLWithPath:pdfFileName];
   /* UIWebView* webView = [[UIWebView alloc] initWithFrame:CGRectMake(0,110,768,900)];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [webView setScalesPageToFit:YES];
    webView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    [webView loadRequest:request];
    webView.scalesPageToFit = YES;
    webView.paginationBreakingMode = UIWebPaginationBreakingModePage;
    webView.paginationMode = UIWebPaginationModeLeftToRight;
    
    [self.scrollView addSubview:webView];*/
    NSString *file = pdfFileName;
    
    
    
    //ReaderDocument *document = [ReaderDocument withDocumentFilePath:file password:nil];
    ReaderDocument *document =  [[ReaderDocument alloc] initWithFilePath:file password:nil];
      _readerViewController = [[ReaderViewController alloc] initWithReaderDocument:document];
    
    // Pass on the required delegate for handling the close button
    _readerViewController.delegate = self;
    
    // Add the VFR Reader as a child
    [self addChildViewController:_readerViewController];
    [self.view addSubview:_readerViewController.view];
    // Set the location of the VFR Reader to the same as the placeholder view
    _readerViewController.view.frame =  self.scrollView.frame;
  //  [_readerViewController didMoveToParentViewController:self];
    
     [self.btnSendEmail setEnabled:true];

}




-(void)plotting :(NSString *)event_code :(NSString *)event_date_time :(NSString *)event_value :(NSString *)ecg_path
{
    NSMutableArray *results = [[NSMutableArray alloc]init];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ &&%K == %@";
    NSArray* args = @[@"event_datetime",event_date_time,@"event_code",event_code];
    if (EventDateTime.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
        NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"EventStorage"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *Eventsett = (NSManagedObject *)[results objectAtIndex:0];
            respiration1=[[Eventsett valueForKey:@"event_respiration_onoff"]intValue];
            respiration1=0;
            carousalrespiration1=[[Eventsett valueForKey:@"event_respiration_onoff"]intValue];
            mcdLead=[[Eventsett valueForKey:@"event_mcdlead"]intValue];
            
            switch ( [[Eventsett valueForKey:@"event_sampling_rate"]intValue]) {
                case 0:
                    sampleRate1 = 125;//3
                    break;
                case 1:
                    //sampleRate1 = 250;
                    sampleRate1 = 64;//5 lead
                    break;
                case 2:
                    sampleRate1 = 500;//12
                    break;
                default:
                    break;
            }
            
            switch ( [[Eventsett valueForKey:@"event_lead_config"]intValue]) {
                case 0:
                    leadCount1 = 7;//3
                    break;
                case 1:
                    leadCount1 = 8;//5 lead
                    break;
                case 2:
                    leadCount1 = 13;//12
                    break;
                case 3:
                    leadCount1 = 16;//15
                    break;
                    
                default:
                    break;
            }
            
        }
        
        
        
    }
    
    
    
    
    
    
    
    
    //  [self addViews];
    //  [self initialMonitor];
    
    
    if([Posture isEqualToString:@"1"])
    {
        postureimage.image=[UIImage imageNamed:@"laying.png"];
    }
    else if([Posture isEqualToString:@"2"])
    {
        postureimage.image=[UIImage imageNamed:@"running.png"];
        
    }
    else if([Posture isEqualToString:@"3"])
    {
        postureimage.image=[UIImage imageNamed:@"walking.png"];
    }
    else
    {
        postureimage.image=[UIImage imageNamed:@"standing.png"];
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // switch to a background thread and perform your expensive operation
        [self getPDFFileName];
        NSArray *filename=[ecg_path componentsSeparatedByString:@"/"];
        //[Plotter drawPDF:[self getPDFFileName] :filename[8]];
        [Plotter drawPDF:[self getPDFFileName] :filename[8] :mcdLead :leadCount1 :event_code :event_value :respiration1];
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // switch back to the main thread to update your UI
             [self showPDFFile];
           
        });
    });
    
   
}

-(void)plotting1 :(NSString *)event_code :(NSString *)event_date_time :(NSString *)event_value :(NSString *)ecg_path
{
    NSMutableArray *results = [[NSMutableArray alloc]init];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@&&%K == %@";
    NSArray* args = @[@"event_datetime",event_date_time,@"event_code",event_code];
    if (event_date_time.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
        NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"EventStorage"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *Eventsett = (NSManagedObject *)[results objectAtIndex:0];
            respiration1=[[Eventsett valueForKey:@"event_respiration_onoff"]intValue];
            mcdLead=[[Eventsett valueForKey:@"event_mcdlead"]intValue];
            
            switch ( [[Eventsett valueForKey:@"event_sampling_rate"]intValue]) {
                case 0:
                    sampleRate1 = 125;//3
                    break;
                case 1:
                    //sampleRate1 = 250;
                    sampleRate1 = 64;//5 lead
                    break;
                case 2:
                    sampleRate1 = 500;//12
                    break;
                default:
                    break;
            }
            
            switch ( [[Eventsett valueForKey:@"event_lead_config"]intValue]) {
                case 0:
                    leadCount1 = 7;//3
                    break;
                case 1:
                    leadCount1 = 8;//5 lead
                    break;
                case 2:
                    leadCount1 = 13;//12
                    break;
                case 3:
                    leadCount1 = 16;//15
                    break;
                    
                default:
                    break;
            }
            
        }
        
        
        NSString* fileName = @"ECG_Mail.PDF";
        
        NSArray *arrayPaths =
        NSSearchPathForDirectoriesInDomains(
                                            NSDocumentDirectory,
                                            NSUserDomainMask,
                                            YES);
        NSString *path = [arrayPaths objectAtIndex:0];
        NSString* pdfFileName = [path stringByAppendingPathComponent:fileName];
        NSArray *filename=[ecg_path componentsSeparatedByString:@"/"];
        //[Plotter drawPDF:[self getPDFFileName] :filename[8]];
        [Plotter drawPDF:pdfFileName :filename[8] :mcdLead :leadCount1 :event_code :event_value :respiration1];
       // [self showPDFFile];
        
    }
    
  
}

-(void) startLoad
{
    [btnSendEmail setEnabled:false];
    [self.spinnerView startAnimating];
}

-(void) stopLoad
{
      [btnSendEmail setEnabled:true];
    [self.spinnerView stopAnimating];
}


- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}


-(BOOL)canBecomeFirstResponder
{
    return YES;
}


- (void)viewWillDisappear:(BOOL)animated
{
   
    [super viewWillDisappear:animated];
}








-(IBAction)goBackToClinicalRecordsAction:(id)sender
{
    [self dismissViewControllerAnimated:true completion:^{
        nil;
    }];
    
    //self.dismissViewControllerAnimated(true, completion: nil)
}


-(IBAction)goToDashboard:(id)sender{
    
   /* [self dismissViewControllerAnimated:true completion:^{
        nil;
    }];*/
    
     if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
     {
     UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Mainstoryboard_iPad" bundle:nil];
     ECSlidingViewController * controller = (ECSlidingViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ECSlidingStoryboardId"];
     [self presentViewController:controller animated:YES completion:nil];
     
     }
     else
     {
     UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
     ECSlidingViewController * controller = (ECSlidingViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ECSlidingStoryboardId"];
     [self presentViewController:controller animated:YES completion:nil];
     }
}

#pragma mark -
#pragma mark iCarousel methods

- (void)setUp
{
   
    
    //set up data
    self.wrap = NO;
     self.itemsdata = [NSMutableArray array];
    
    NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
   
    
    NSString *temperaturetype=[TempDefault objectForKey:@"TEMPERATURE"];
    
    
    
    if(carousalrespiration1==0)
    {
        
        if([temperaturetype isEqualToString:@"CELSIUS"])
        {
            self.items=[[NSMutableArray alloc]initWithObjects:@"HR",@"Temp",@"RR",@"PR",@"QRS",@"QT",@"QTC",nil];
            self.itemsunit=[[NSMutableArray alloc]initWithObjects:@"bpm",@"c",@"ms",@"ms",@"ms",@"ms",@"ms",nil];
            self.itemsimage=[[NSMutableArray alloc]initWithObjects:@"bpm.png",@"temperature.png",@"clock.png",@"clock.png",@"clock.png",@"clock.png",@"clock.png",nil];
            
            [self.itemsdata addObject:HR1];
            [self.itemsdata addObject:Temp];
            [self.itemsdata addObject:RR];
            [self.itemsdata addObject:PR];
            [self.itemsdata addObject:QRS];
            [self.itemsdata addObject:QT];
            [self.itemsdata addObject:QTC];
        }
        else
        {
            self.items=[[NSMutableArray alloc]initWithObjects:@"HR",@"Temp",@"RR",@"PR",@"QRS",@"QT",@"QTC",nil];
            self.itemsunit=[[NSMutableArray alloc]initWithObjects:@"bpm",@"F",@"ms",@"ms",@"ms",@"ms",@"ms",nil];
            self.itemsimage=[[NSMutableArray alloc]initWithObjects:@"bpm.png",@"temperature.png",@"clock.png",@"clock.png",@"clock.png",@"clock.png",@"clock.png",nil];
            
            
            [self.itemsdata addObject:HR1];
            [self.itemsdata addObject:Temp];
            [self.itemsdata addObject:RR];
            [self.itemsdata addObject:PR];
            [self.itemsdata addObject:QRS];
            [self.itemsdata addObject:QT];
            [self.itemsdata addObject:QTC];
        }
        
        
        
    }
    else
    {
        if([Posture isEqualToString:@"1"])
        {
            if([temperaturetype isEqualToString:@"CELSIUS"])
            {
                self.items=[[NSMutableArray alloc]initWithObjects:@"HR",@"Resp",@"Temp",@"RR",@"PR",@"QRS",@"QT",@"QTC",nil];
                self.itemsunit=[[NSMutableArray alloc]initWithObjects:@"bpm",@"rpm",@"c",@"ms",@"ms",@"ms",@"ms",@"ms",nil];
                self.itemsimage=[[NSMutableArray alloc]initWithObjects:@"bpm.png",@"rpm.png",@"temperature.png",@"clock.png",@"clock.png",@"clock.png",@"clock.png",@"clock.png",nil];
                [self.itemsdata addObject:HR1];
                [self.itemsdata addObject:Resprate];
                [self.itemsdata addObject:Temp];
                [self.itemsdata addObject:RR];
                [self.itemsdata addObject:PR];
                [self.itemsdata addObject:QRS];
                [self.itemsdata addObject:QT];
                [self.itemsdata addObject:QTC];
            }
            else
            {
                self.items=[[NSMutableArray alloc]initWithObjects:@"HR",@"Resp",@"Temp",@"RR",@"PR",@"QRS",@"QT",@"QTC",nil];
                self.itemsunit=[[NSMutableArray alloc]initWithObjects:@"bpm",@"rpm",@"F",@"ms",@"ms",@"ms",@"ms",@"ms",nil];
                self.itemsimage=[[NSMutableArray alloc]initWithObjects:@"bpm.png",@"rpm.png",@"temperature.png",@"clock.png",@"clock.png",@"clock.png",@"clock.png",@"clock.png",nil];
                [self.itemsdata addObject:HR1];
                [self.itemsdata addObject:Resprate];
                [self.itemsdata addObject:Temp];
                [self.itemsdata addObject:RR];
                [self.itemsdata addObject:PR];
                [self.itemsdata addObject:QRS];
                [self.itemsdata addObject:QT];
                [self.itemsdata addObject:QTC];
            }
            
            
        }
        else
        {
            
            if([temperaturetype isEqualToString:@"CELSIUS"])
            {
                self.items=[[NSMutableArray alloc]initWithObjects:@"HR",@"Temp",@"RR",@"PR",@"QRS",@"QT",@"QTC",nil];
                self.itemsunit=[[NSMutableArray alloc]initWithObjects:@"bpm",@"c",@"ms",@"ms",@"ms",@"ms",@"ms",nil];
                self.itemsimage=[[NSMutableArray alloc]initWithObjects:@"bpm.png",@"temperature.png",@"clock.png",@"clock.png",@"clock.png",@"clock.png",@"clock.png",nil];
                
                [self.itemsdata addObject:HR1];
                [self.itemsdata addObject:Temp];
                [self.itemsdata addObject:RR];
                [self.itemsdata addObject:PR];
                [self.itemsdata addObject:QRS];
                [self.itemsdata addObject:QT];
                [self.itemsdata addObject:QTC];
            }
            else
            {
                self.items=[[NSMutableArray alloc]initWithObjects:@"HR",@"Temp",@"RR",@"PR",@"QRS",@"QT",@"QTC",nil];
                self.itemsunit=[[NSMutableArray alloc]initWithObjects:@"bpm",@"F",@"ms",@"ms",@"ms",@"ms",@"ms",nil];
                self.itemsimage=[[NSMutableArray alloc]initWithObjects:@"bpm.png",@"temperature.png",@"clock.png",@"clock.png",@"clock.png",@"clock.png",@"clock.png",nil];
                
                [self.itemsdata addObject:HR1];
                [self.itemsdata addObject:Temp];
                [self.itemsdata addObject:RR];
                [self.itemsdata addObject:PR];
                [self.itemsdata addObject:QRS];
                [self.itemsdata addObject:QT];
                [self.itemsdata addObject:QTC];
            }
        }
    }

    
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]))
    {
        [self setUp];
       
       
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder]))
    {
       HR1=@"0";
        Resprate=@"0";
        Temp=@"0";
        RR=@"0";
        PR=@"0";
        QRS=@"0";
        QT=@"0";
        QTC=@"0";
        Posture=@"1";
        //[self setUp];
        
    }
    return self;
}

- (NSInteger)numberOfItemsInCarousel:(__unused iCarousel *)carousel
{
    return (NSInteger)[self.itemsdata count];
}

- (UIView *)carousel:(__unused iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    UILabel *label = nil;
    UILabel *label1 = nil;
    UILabel *label2 = nil;
    UIImageView *unitimage;
    UIImageView *boundimage;
    //create new view if no view is available for recycling
   // if (view == nil)
   // {
        view = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, 60.0f, 100.0f)];
        //((UIImageView *)view).image = [UIImage imageNamed:@"page.png"];
        view.contentMode = UIViewContentModeLeft;
        label =[[UILabel alloc] init];
        [label setFrame:CGRectMake(8,15,50,20)];
        //  label = [[UILabel alloc] initWithFrame:CGRectMake(0,0,20,20)];
        // label = [[UILabel alloc] initWithFrame:view.bounds];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [label.font fontWithSize:10];
        label.textColor=[UIColor whiteColor];
        label.tag = 1;
        
        label1 =[[UILabel alloc] init];
        [label1 setFrame:CGRectMake(8,35,55,30)];
        label1.backgroundColor = [UIColor clearColor];
        label1.textAlignment = NSTextAlignmentCenter;
        label1.font = [label.font fontWithSize:20];
        label1.textColor=[UIColor whiteColor];
        label1.tag = 1;
        
        label2 =[[UILabel alloc] init];
        [label2 setFrame:CGRectMake(18,60,50,20)];
        label2.backgroundColor = [UIColor clearColor];
        label2.textAlignment = NSTextAlignmentCenter;
        label2.font = [label.font fontWithSize:10];
        label2.textColor=[UIColor whiteColor];
        label2.tag = 1;
        
        unitimage=[[UIImageView alloc]init];
        [unitimage setFrame:CGRectMake(17,65,10,10)];
        
        boundimage=[[UIImageView alloc]init];
        [boundimage setFrame:CGRectMake(0,0,2,80)];
        
        [view addSubview:label];
        [view addSubview:label1];
        [view addSubview:label2];
        [view addSubview:unitimage];
        [view addSubview:boundimage];
        
        
        
   // }
   // else
   // {
        //get a reference to the label in the recycled view
      //  label = (UILabel *)[view viewWithTag:1];
  //  }
    
    //set item label
    //remember to always set any properties of your carousel item
    //views outside of the `if (view == nil) {...}` check otherwise
    //you'll get weird issues with carousel item content appearing
    //in the wrong place in the carousel
    label.text =  self.items[index];//[self.items[(NSUInteger)index] stringValue];
    label1.text =  self.itemsdata[index];
    label2.text =  self.itemsunit[index];
    [unitimage setImage:[UIImage imageNamed:[self.itemsimage objectAtIndex:index]]];
    if(index!=0)
        boundimage.image=[UIImage imageNamed:@"vertical_blue_line.png"];
    return view;
}

- (NSInteger)numberOfPlaceholdersInCarousel:(__unused iCarousel *)carousel
{
    //note: placeholder views are only displayed on some carousels if wrapping is disabled
    return 2;
}

- (UIView *)carousel:(__unused iCarousel *)carousel placeholderViewAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    UILabel *label = nil;
    
    //create new view if no view is available for recycling
    if (view == nil)
    {
        //don't do anything specific to the index within
        //this `if (view == nil) {...}` statement because the view will be
        //recycled and used with other index values later
        view = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, 100.0f, 100.0f)];
        //  ((UIImageView *)view).image = [UIImage imageNamed:@"page.png"];
        view.contentMode = UIViewContentModeLeft;
        
        
        label = [[UILabel alloc] initWithFrame:view.bounds];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [label.font fontWithSize:20.0f];
        label.tag = 1;
        [view addSubview:label];
    }
    else
    {
        //get a reference to the label in the recycled view
        label = (UILabel *)[view viewWithTag:1];
    }
    
    //set item label
    //remember to always set any properties of your carousel item
    //views outside of the `if (view == nil) {...}` check otherwise
    //you'll get weird issues with carousel item content appearing
    //in the wrong place in the carousel
    // label.text = (index == 0)? @"[": @"]";
    
    return view;
}

- (CATransform3D)carousel:(__unused iCarousel *)carousel itemTransformForOffset:(CGFloat)offset baseTransform:(CATransform3D)transform
{
    //implement 'flip3D' style carousel
    transform = CATransform3DRotate(transform, M_PI / 8.0f, 0.0f, 1.0f, 0.0f);
    return CATransform3DTranslate(transform, 0.0f, 0.0f, offset * self.carousel.itemWidth);
}

- (CGFloat)carousel:(__unused iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    //customize carousel display
    switch (option)
    {
        case iCarouselOptionWrap:
        {
            //normally you would hard-code this to YES or NO
            return self.wrap;
        }
        case iCarouselOptionSpacing:
        {
            //add a bit of spacing between the item views
            return value * 1.05f;
        }
        case iCarouselOptionFadeMax:
        {
            if (self.carousel.type == iCarouselTypeCustom)
            {
                //set opacity based on distance from camera
                return 0.0f;
            }
            return value;
        }
        case iCarouselOptionShowBackfaces:
        case iCarouselOptionRadius:
        case iCarouselOptionAngle:
        case iCarouselOptionArc:
        case iCarouselOptionTilt:
        case iCarouselOptionCount:
        case iCarouselOptionFadeMin:
        case iCarouselOptionFadeMinAlpha:
        case iCarouselOptionFadeRange:
        case iCarouselOptionOffsetMultiplier:
        case iCarouselOptionVisibleItems:
        {
            return value;
        }
    }
}

#pragma mark -
#pragma mark iCarousel taps

- (void)carousel:(__unused iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    NSNumber *item = (self.items)[(NSUInteger)index];
    NSLog(@"Tapped view number: %@", item);
}

- (void)carouselCurrentItemIndexDidChange:(__unused iCarousel *)carousel
{
    // NSLog(@"Index: %@", @(self.carousel.currentItemIndex));
}

-(IBAction)EmailSending:(id)sender
{
    //patientdetail
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"PatientDetails" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSString * docname;
    NSError *error = nil;
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        //NSLog(@"%@", result);
        if (result.count > 0) {
            NSManagedObject *patient = (NSManagedObject *)[result objectAtIndex:0];
            //NSLog(@"1 - %@", patient);
            docname= [patient valueForKey:@"doc_email"];
        }
    }
   
    SDCAlertView *alert = [[SDCAlertView alloc] initWithTitle:@"Email message"
                                                      message:@"Enter caregiver's email address"
                                                     delegate:self
                                            cancelButtonTitle:@"Cancel"
                                            otherButtonTitles:@"OK", nil];
    [alert setAlertViewStyle:SDCAlertViewStylePlainTextInput];

     [[alert textFieldAtIndex:0] setText:docname];
    [alert show];

    
    
   
   
   /* UIImage* image = nil;
    
    UIGraphicsBeginImageContext(scrollView.contentSize);
    {
        CGPoint savedContentOffset = scrollView.contentOffset;
        CGRect savedFrame = scrollView.frame;
        
        scrollView.contentOffset = CGPointZero;
        scrollView.frame = CGRectMake(0, 0, scrollView.contentSize.width, scrollView.contentSize.height);
        
        [scrollView.layer renderInContext: UIGraphicsGetCurrentContext()];
        image = UIGraphicsGetImageFromCurrentImageContext();
        
        scrollView.contentOffset = savedContentOffset;
        scrollView.frame = savedFrame;
    }
    UIGraphicsEndImageContext();
    
    if (image != nil) {
        [UIImagePNGRepresentation(image) writeToFile: @"/tmp/test.png" atomically: YES];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
        NSString *docs = [paths objectAtIndex:0];
        NSString * pathname=[NSString stringWithFormat:@"image1.jpg"];
        NSString* path =  [docs stringByAppendingFormat:@"/%@",pathname];
        
        NSData* imageData = [NSData dataWithData:UIImageJPEGRepresentation(image,80)];
        NSError *writeError = nil;
        [imageData writeToFile:path options:NSDataWritingAtomic error:&writeError];
    }

    
     MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
     controller.mailComposeDelegate = self;
     [controller setSubject:@"Check this route out"];
     [controller setMessageBody:@"Attaching a shot of covered route." isHTML:NO];
     
    
     // ATTACHING A SCREENSHOT
     NSData *myData = UIImagePNGRepresentation(image);
     [controller addAttachmentData:myData mimeType:@"image/png" fileName:@"route"];    [controller addAttachmentData:myData mimeType:@"image/png" fileName:@"route"];
     
     // SHOWING MAIL VIEW
     [self presentModalViewController:controller animated:YES];*/
}

- (BOOL)alertView:(SDCAlertView *)alertView shouldDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    
        if(buttonIndex==0)
        {
            return YES;
        }
    
        
        if(buttonIndex==1)
        {
           
            UITextField *Docname = [alertView textFieldAtIndex:0];
            NSLog(@"Docname: %@", Docname.text);
            
            
            
            
            if(Docname.text.length == 0) //check your two textflied has value
            {
                
                NSLog(@"EMPTY");
                
                return NO;
            }
           
            else
            {
                [self sendEmailInBackground:Eventcode :EventDateTime :Docname.text];
                return YES;
            }
            
        }
        else
            return YES;
        
    
}



-(void) sendEmailInBackground :(NSString *)evencde :(NSString *)eventdatetime :(NSString *)Docname{
    NSLog(@"Start Sending");
     [self.btnSendEmail setEnabled:false];
    GlobalVars *globals = [GlobalVars sharedInstance];
    globals.Email_EventCode=evencde;
    globals.Email_EventDatetime=eventdatetime;
    
    CommonHelper *commhelp=[[CommonHelper alloc]init];
      NSString *localeventtime=[commhelp UTCDateInYourFormat:@"yyyy-MM-dd HH:mm:ss" :@"yyyy-MM-dd HH:mm:ss" :eventdatetime];
    
    test_smtp_message = [[SKPSMTPMessage alloc] init];
    test_smtp_message.fromEmail = @"admin@personal-healthwatch.com";
       test_smtp_message.toEmail = Docname;
   // test_smtp_message.relayHost =  @"m.outlook.com";
     test_smtp_message.relayHost =@"smtp.office365.com";
    //test_smtp_message.relayHost =  @"smtp.gmail.com";
    test_smtp_message.requiresAuth = YES;
    test_smtp_message.login = @"admin@personal-healthwatch.com";
     test_smtp_message.pass = @"Ventricular01";
    test_smtp_message.wantsSecure = YES; // smtp.gmail.com doesn't work without TLS!
    
    
    //    test_smtp_message.bccEmail = @"testbcc@test.com";
    
    // Only do this for self-signed certs!
    // test_smtp_message.validateSSLChain = YES;
    test_smtp_message.delegate = self;
    
    NSMutableArray *parts_to_send = [NSMutableArray array];
    
    
    
    
    
    //tempertature type
    NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
    NSString *temperaturetype=[TempDefault objectForKey:@"TEMPERATURE"];
    NSString *tempunit;
    NSString *htmlString;
    int respira=0;
    if([temperaturetype isEqualToString:@"CELSIUS"])
    {
        tempunit= [NSString stringWithFormat:@"C%@", @"\u00B0"];
        
    }
    else
    {
        tempunit=[NSString stringWithFormat:@"F%@", @"\u00B0"];
    }
    
    
    
    //eventvalue
    NSString * patienLoc,*EventReason,*posture;
    NSMutableArray *results = [[NSMutableArray alloc]init];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ &&%K == %@";
    NSArray* args = @[@"event_datetime",eventdatetime,@"event_code",evencde];
    if (eventdatetime.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
        NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"EventStorage"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *Eventsett = (NSManagedObject *)[results objectAtIndex:0];
           
            posture=[Eventsett valueForKey:@"event_posture_code"];
            
            //Posture
            NSString *posturetext;
            if([posture isEqualToString:@"1"])
            {
                 respira=[[Eventsett valueForKey:@"event_respiration_onoff"]intValue];
                posturetext=@"Laying Down";
            }
            else if([posture isEqualToString:@"2"])
            {
                posturetext=@"Running";
                
            }
            else if([posture isEqualToString:@"3"])
            {
                posturetext=@"Walking";
            }
            else
            {
                posturetext=@"Standing";
            }
            
            
            if([[Eventsett valueForKey:@"event_reason"] containsString:@"_"])
            {
                if([[Eventsett valueForKey:@"event_reason"] isEqualToString:@"Patient_Call_Event"])
                {
                    
                    EventReason =[[Eventsett valueForKey:@"event_reason"] stringByReplacingOccurrencesOfString:@"_" withString:@""];
                }
                else
                {
                    EventReason =[NSString stringWithFormat:@"suspected %@",[[Eventsett valueForKey:@"event_reason"] stringByReplacingOccurrencesOfString:@"_" withString:@""]];
                    
                }
                
            }
            else
            {
                EventReason =[NSString stringWithFormat:@"suspected %@",[Eventsett valueForKey:@"event_reason"]];
            }
            
            //patientdetail
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
            
            
            NSEntityDescription *entity = [NSEntityDescription entityForName:@"PatientDetails" inManagedObjectContext:self.managedObjectContext];
            [fetchRequest setEntity:entity];
            
            NSError *error = nil;
            NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
            
            if (error) {
                NSLog(@"Unable to execute fetch request.");
                NSLog(@"%@, %@", error, error.localizedDescription);
                
            } else {
                //NSLog(@"%@", result);
                if (result.count > 0) {
                    NSManagedObject *patient = (NSManagedObject *)[result objectAtIndex:0];
                    //NSLog(@"1 - %@", patient);
                 
                    
                    BOOL savedSwitch= [[NSUserDefaults standardUserDefaults] boolForKey:@"SwitchState"];
                    if(savedSwitch)
                    {
                        patienLoc=[NSString stringWithFormat:@"For User Location Click<a href='http://maps.google.com/maps?z=18&q=%@,%@'>Here</a>",[Eventsett valueForKey:@"event_latitude"],[Eventsett valueForKey:@"event_longitude"]];
                        
                    }
                    else
                    {
                        patienLoc=@"";
                        // patienLoc=[NSString stringWithFormat:@"For User Location Click<a href='http://maps.google.com/maps?z=18&q=%@,%@'>Here</a>",@"19.1178",@"73.0269"];
                    }
                    
                    if(respira==1)
                    {
                        htmlString =
                        [NSString stringWithFormat: @"<html><body>Dear Dr. %@,<br><br>You have received an ECG report of patient %@ , ID %@. The event is of type %@.<br>Please check the attached ECG graph and ECG data for reference.<br>You can contact the patient at %@ <br> <br>Patient ID : %@<br>Name : %@ <br>Event Date & Time : %@<br>HR : %@ bpm<br>Resp Amp :%@ mv <br>Resp Var :%@ <br>Respiration :%@ rpm <br>RR :%@ msec<br>PR : %@ msec<br>QRS : %@ msec<br>QT : %@ msec<br>QTC :%@ msec<br>Temperature :%@  %@<br>Posture :%@<br><br>Regards,<br>Master Caution Admin<br><br>%@",[patient valueForKey:@"doc_name"],[patient valueForKey:@"patient_name"],[patient valueForKey:@"uid"],EventReason,[patient valueForKey:@"patient_mobno"],[patient valueForKey:@"uid"],[patient valueForKey:@"patient_name"],localeventtime,[Eventsett valueForKey:@"event_heartrate"],[Eventsett valueForKey:@"event_respiration_amplitude"],[Eventsett valueForKey:@"event_resp_variance"],[Eventsett valueForKey:@"event_respiration"],[Eventsett valueForKey:@"event_rr_interval"],[Eventsett valueForKey:@"event_pr_interval"],[Eventsett valueForKey:@"event_qrs_interval"],[Eventsett valueForKey:@"event_qt_interval"],[Eventsett valueForKey:@"event_qtc_interval"],[NSString stringWithFormat:@"%.1f",[[Eventsett valueForKey:@"event_temp"]floatValue]],tempunit,posturetext,patienLoc];
                    }
                    else
                    {
                        htmlString =
                        [NSString stringWithFormat: @"<html><body>Dear Dr. %@,<br><br>You have received an ECG report of patient %@ , ID %@. The event is of type %@.<br>Please check the attached ECG graph and ECG data for reference.<br>You can contact the patient at %@ <br> <br>Patient ID : %@<br>Name : %@ <br>Event Date & Time : %@<br>HR : %@ bpm<br>RR :%@ msec<br>PR : %@ msec<br>QRS : %@ msec<br>QT : %@ msec<br>QTC :%@ msec<br>Temperature :%@  %@<br>Posture :%@<br><br>Regards,<br>Master Caution Admin<br><br>%@",[patient valueForKey:@"doc_name"],[patient valueForKey:@"patient_name"],[patient valueForKey:@"uid"],EventReason,[patient valueForKey:@"patient_mobno"],[patient valueForKey:@"uid"],[patient valueForKey:@"patient_name"],localeventtime,[Eventsett valueForKey:@"event_heartrate"],[Eventsett valueForKey:@"event_rr_interval"],[Eventsett valueForKey:@"event_pr_interval"],[Eventsett valueForKey:@"event_qrs_interval"],[Eventsett valueForKey:@"event_qt_interval"],[Eventsett valueForKey:@"event_qtc_interval"],[NSString stringWithFormat:@"%.1f",[[Eventsett valueForKey:@"event_temp"]floatValue]],tempunit,posturetext,patienLoc];
                    }
                    
                    test_smtp_message.subject =[NSString stringWithFormat:@"ECG Report %@",[patient valueForKey:@"family_name"]];
                }
            }
            
            
            
            
        }
        
        
        
    }
    
    
    
    NSString *msg22=[self getEmailTable:eventdatetime];
    //NSLog(@"msg2%@",msg22);
    htmlString= [htmlString stringByAppendingString:msg22];
    
    
    NSAttributedString *messagebody = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
    //If you are not sure how to format your message part, send an email to your self.
    //In Mail.app, View > Message> Raw Source to see the raw text that a standard email client will generate.
    //This should give you an idea of the proper format and options you need
    NSDictionary *plain_text_part = [NSDictionary dictionaryWithObjectsAndKeys:
                                     @"text/plain\r\n\tcharset=UTF-8;\r\n\tformat=flowed", kSKPSMTPPartContentTypeKey,
                                     messagebody.string, kSKPSMTPPartMessageKey,
                                     @"quoted-printable", kSKPSMTPPartContentTransferEncodingKey,
                                     nil];
    [parts_to_send addObject:plain_text_part];
    
   /* NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *docs = [paths objectAtIndex:0];
    NSString * pathname=[NSString stringWithFormat:@"image1.jpg"];
    NSString* path =  [docs stringByAppendingFormat:@"/%@",pathname];
    //NSString *image_path = [[NSBundle mainBundle] pathForResource:@"Qualify" ofType:@"png"];
    NSData *image_data = [NSData dataWithContentsOfFile:path];
    NSDictionary *image_part = [NSDictionary dictionaryWithObjectsAndKeys:
                                @"inline;\r\n\tfilename=\"image1.jpg\"",kSKPSMTPPartContentDispositionKey,
                                @"base64",kSKPSMTPPartContentTransferEncodingKey,
                                @"image/png;\r\n\tname=image1.jpg;\r\n\tx-unix-mode=0666",kSKPSMTPPartContentTypeKey,
                                [image_data encodeWrappedBase64ForData],kSKPSMTPPartMessageKey,
                                nil];
    [parts_to_send addObject:image_part];*/
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *docs = [paths objectAtIndex:0];
    NSString * pathname=[NSString stringWithFormat:@"ECG.PDF"];
    NSString* path =  [docs stringByAppendingFormat:@"/%@",pathname];
    //NSString *image_path = [[NSBundle mainBundle] pathForResource:@"Qualify" ofType:@"png"];
    NSData *image_data = [NSData dataWithContentsOfFile:path];
    NSDictionary *image_part = [NSDictionary dictionaryWithObjectsAndKeys:
                                @"inline;\r\n\tfilename=\"ECG.PDF\"",kSKPSMTPPartContentDispositionKey,
                                @"base64",kSKPSMTPPartContentTransferEncodingKey,
                                @"application/pdf;\r\n\tname=ECG.PDF;\r\n\tx-unix-mode=0666",kSKPSMTPPartContentTypeKey,
                                [image_data encodeWrappedBase64ForData],kSKPSMTPPartMessageKey,
                                nil];
    [parts_to_send addObject:image_part];
    
    NSDictionary *htmlPart = [NSDictionary dictionaryWithObjectsAndKeys:@"text/html",kSKPSMTPPartContentTypeKey,                    htmlString,kSKPSMTPPartMessageKey,@"8bit",kSKPSMTPPartContentTransferEncodingKey, nil];
    test_smtp_message.parts = [NSArray arrayWithObjects:htmlPart,image_part,  nil];
    
    //test_smtp_message.parts = parts_to_send;
    
    
    HighestState = 0;
    
    [test_smtp_message send];
    
    
    
   
}


-(NSString *)getEmailTable :(NSString *)Eventdttime
{
    
    NSString *s1=@"";
    s1=[s1 stringByAppendingString:[NSString stringWithFormat:@"<br>Last month patient event history:<br><table border = '1px'><tr><td align = 'center'>Date & Time</td><td align = 'center'>Event</td></tr>"]];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    CommonHelper *commhelp=[[CommonHelper alloc]init];
  
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"EventStorage" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        //NSLog(@"%@", result);
        if (result.count > 0) {
            for(int j=0;j<result.count;j++)
            {
                NSManagedObject *eventstorage = (NSManagedObject *)[result objectAtIndex:j];
                  NSString *localeventtime=[commhelp UTCDateInYourFormat:@"yyyy-MM-dd HH:mm:ss" :@"yyyy-MM-dd HH:mm:ss" :[eventstorage valueForKey:@"event_datetime"]];
                s1=[s1 stringByAppendingString:[NSString stringWithFormat:@"<tr><td>%@</td><td>%@</td></tr>",localeventtime,[eventstorage valueForKey:@"event_reason"]]];
                
                
            }
            
        }
    }
		  
    
    // finish html
    
    s1=[s1 stringByAppendingString:[NSString stringWithFormat:@"</table></body></html>"]];
    return s1;
    
    
}


#pragma mark SKPSMTPMessage Delegate Methods
- (void)messageState:(SKPSMTPState)messageState;
{
    NSLog(@"HighestState:%lu", (unsigned long)HighestState);
    if (messageState > HighestState)
        HighestState = messageState;
    
}
- (void)messageSent:(SKPSMTPMessage *)SMTPmessage
{
    GlobalVars *globals = [GlobalVars sharedInstance];
    NSMutableArray *results = [[NSMutableArray alloc]init];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ &&%K == %@";
    NSArray* args = @[@"event_datetime",[globals Email_EventDatetime],@"event_code",[globals Email_EventCode]];
    if ([globals Email_EventDatetime].length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
        //NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"EventStorage"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *Eventsett = (NSManagedObject *)[results objectAtIndex:0];
            [Eventsett setValue:@"0" forKey:@"event_mail_sms"];
        }
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email message" message:@"Email message was sent successfully"
                                                   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
    
     [self.btnSendEmail setEnabled:true];
    //DEBUGLOG(@"delegate - message sent");
    
}
- (void)messageFailed:(SKPSMTPMessage *)SMTPmessage error:(NSError *)error
{
     [self.btnSendEmail setEnabled:true];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription]
                                                   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
    // DEBUGLOG(@"delegate - error(%d): %@", [error code], [error localizedDescription]);
}






@end
