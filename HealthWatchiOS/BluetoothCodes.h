//
//  BluetoothCodes.h
//  HealthWatch
//
//  Created by METSL MAC MINI on 26/10/15.
//  Copyright © 2015 METSL MAC MINI. All rights reserved.
//

#ifndef BluetoothCodes_h
#define BluetoothCodes_h

// MCD defines
#define COMMUNICATION_CONTROL_BYTE       241  // 0xF1
#define SETTING_CONTROL_BYTE             242  // 0xf2
#define LIVE_DATA_CONTROL_BYTE           243  // 0xF3
#define EVENT_DATA_CONTROL_BYTE          244  // 0xF4
#define PARAMETER_CONTROL_BYTE           245  // 0xf5

#define COMMUNICATION_FRAME_COMMON_RX                                           0       // 1
#define COMMUNICATION_FRAME_START_CONTIONUOUS_REQ                               1       //31
#define COMMUNICATION_FRAME_STOP_CONTIONUOUS_REQ                                2       //32
#define COMMUNICATION_FRAME_KEEP_ALIVE_RESPONSE                                 3       //27
#define COMMUNICATION_FRAME_ACK                                                 4       //28
#define COMMUNICATION_FRAME_NACK                                                5       //29
#define COMMUNICATION_FRAME_GET_ALL_PARA_FLAG                                   6       //21
#define COMMUNICATION_FRAME_DELETE_ALL_FILES_ON_SYS                             7       //75
#define COMMUNICATION_FRAME_SET_DEFAULT_THRESHOLD__FLAG_SETTING                 8       //new protocol added
#define COMMUNICATION_FRAME_ON_DEMAND_ECG                                       9       //32

#define COMMUNICATION_FRAME_EVENT_FILE_AVAILABLE_TO_TX	                        10      //9
#define COMMUNICATION_FRAME_BUDGE_INFO                                          11      //25
#define COMMUNICATION_FRAME_STORED_EVENT_INFO                                   12      //22
#define COMMUNICATION_FRAME_EVENT_TX_STOP										13      //11
#define COMMUNICATION_FRAME_KEEP_ALIVE											14      //8
#define COMMUNICATION_FRAME_BATTERY_STATUS                                      15      //21
#define COMMUNICATION_FRAME_GET_DEVICE_SETTINGS                                 16      //24
#define COMMUNICATION_FRAME_INVALID_POSTURE_VAL									17      //14
#define COMMUNICATION_FRAME_INVALID_DATA_VAL									18      //15
#define COMMUNICATION_FRAME_INVALID_SUSP_TIME									19      //16
#define COMMUNICATION_FRAME_INVALID_SUSP_PERCNTG								20      //17
#define COMMUNICATION_FRAME_SETTING_OVER                                        21      //new
#define COMMUNICATION_FRAME_SET_DEVICE_SETTINGS                                 22      //for set MCD settings received from MCM
#define COMMUNICATION_FRAME_LEAD_SETTINGS                                       23      //for set MCD settings received from MCM

#define COMMUNICATION_FRAME_SYNC_ALL_BEGIN                                      30      //79
#define COMMUNICATION_FRAME_SYNC_ALL_OVER                                       31      //80

#define COMMUNICATION_FRAME_SYNC_ALL_FROM_MCD_OVER                              33      //82
#define COMMUNICATION_FRAME_NEXT_PACKET_REQ                                     34      //26

#define COMMUNICATION_FRAME_RESP_CALIPER_AMP                                    35       // 21
#define COMMUNICATION_FRAME_EVENT_FETCHER                                       36      //76

#define SETTING_FRAME_GET_DEVICE_ID                                             50      //1
#define SETTING_FRAME_GET_FIRMWARE_DETAILS                                      51      //2
#define SETTING_FRAME_GET_LAST_UPDATE_DATE_TIME                                 52      //3
#define SETTING_FRAME_GET_BUILT_IN_TEST_REPORT                                  53      //4
#define SETTING_FRAME_GET_ST_ELEVATION_VAL                                      54      //5
#define SETTING_FRAME_GET_ST_DEPRESSION_VAL                                     55      //6
#define SETTING_FRAME_GET_T_WAVE_VAL                                            56      //7
#define SETTING_FRAME_GET_BIGEMENY_VAL                                          57      //8
#define SETTING_FRAME_GET_PROLONGED_QT_VAL                                      58      //9
#define SETTING_FRAME_GET_QRS_WIDTH_WIDE_VAL                                    59      //10
#define SETTING_FRAME_GET_VTACH_NARROW_QRS_VAL                                  60      //11
#define SETTING_FRAME_GET_VTACH_WIDE_QRS_VAL                                    61      //12
#define SETTING_FRAME_GET_PVC_VAL                                               62      //13
#define SETTING_FRAME_GET_ASYSTOLE_VAL                                          63      //14
#define SETTING_FRAME_GET_BRADICARDIA_VAL                                       64      //15
#define SETTING_FRAME_GET_APNEA_VAL                                             65      //16
#define SETTING_FRAME_GET_TEMPRATURE_VAL                                        66      //17
#define SETTING_FRAME_GET_FALL_EVENT_VAL                                        67      //18
#define SETTING_FRAME_GET_NO_MOTION_VAL                                         68      //19
#define SETTING_FRAME_GET_RESPIRATION_RATE_VAL                                  69      //20
#define SETTING_FRAME_GET_QRS_WIDTH_NARROW_VAL                                  70      //77
#define SETTING_FRAME_GET_PAUSE_VAL                                             71      //78
#define SETTING_FRAME_GET_TRIGEMENY_VAL                                         72      //83
#define SETTING_FRAME_GET_COUPLET_VAL                                           73      //84
#define SETTING_FRAME_GET_TRIPLET_VAL                                           74      //85
#define SETTING_FRAME_GET_SUSPENSION_PERCNTG                                    75      //87
#define SETTING_FRAME_GET_SUSPENSION_VAL                                        76      //86
#define SETTING_FRAME_GET_CURRENT_LEVEL                                         77      //89
#define SETTING_FRAME_GET_RR_INTERVAL_VAL                                       78      //88
#define SETTING_FRAME_GET_TACHYCARDIA_VAL                                       79      //88

#define SETTING_FRAME_SET_GAIN                                                  100     //22
#define SETTING_FRAME_SET_50_Hz                                                 101     //41
#define SETTING_FRAME_SET_BASELINE                                              102     //45
#define SETTING_FRAME_SET_VOLUME                                                103     //61
#define SETTING_FRAME_SET_DATE_TIME                                             104     //2
#define SETTING_FRAME_SET_PRE_POST_TIME                                         105     //3
#define SETTING_FRAME_SET_REMOTE_ADDRESS                                        106     //4
#define SETTING_FRAME_SET_EVENT_DETECTION_LEAD_VAL                              107     //22
#define SETTING_FRAME_SET_SAMPLE_RATE                                           108     //71
#define SETTING_FRAME_SET_LEAD_SELECTION_FOR_TX                                 109     //23
#define SETTING_FRAME_SET_KEEP_ALIVE_TIME                                       110     //24
#define SETTING_FRAME_SET_RESPIRATION_ENABLE                                    111
#define SETTING_FRAME_SET_MCD_ORIENTATION                                       112

/*Remote to Client Communication Msg's Number*/

#define SETTING_FRAME_SET_ST_ELEVATION_VAL                                      154     //5
#define SETTING_FRAME_SET_DEPRESSION_VAL                                        155     //6
#define SETTING_FRAME_SET_T_WAVE_VAL                                            156     //7
#define SETTING_FRAME_SET_BIGEMENY_VAL                                          157     //8
#define SETTING_FRAME_SET_PROLONGED_QT_VAL                                      158     //9
#define SETTING_FRAME_SET_QRS_WIDTH_WIDE_VAL                                    159     //10
#define SETTING_FRAME_SET_VTACH_NARROW_QRS_VAL                                  160     //11
#define SETTING_FRAME_SET_VTACH_WIDE_QRS_VAL                                    161     //12
#define SETTING_FRAME_SET_PVC_VAL                                               162     //13
#define SETTING_FRAME_SET_ASYSTOLE_VAL                                          163     //14
#define SETTING_FRAME_SET_BRADICARDIA_VAL                                       164     //15
#define SETTING_FRAME_SET_APNEA_VAL                                             165     //16
#define SETTING_FRAME_SET_TEMPERATURE_VAL                                       166     //17
#define SETTING_FRAME_SET_FALL_EVENT_FLAG                                       167     //18
#define SETTING_FRAME_SET_NO_MOTION_VAL                                         168     //19
#define SETTING_FRAME_SET_RESPIRATION_RATE_VAL                                  169     //20
#define SETTING_FRAME_SET_QRS_WIDTH_NARROW_VAL                                  170     //25
#define SETTING_FRAME_SET_PAUSE_VAL                                             171     //26
#define SETTING_FRAME_SET_TRIGEMENY_VAL                                         172     //27
#define SETTING_FRAME_SET_COUPLET_VAL                                           173     //28
#define SETTING_FRAME_SET_TRIPLET_VAL                                           174     //29
#define SETTING_FRAME_SET_SUSPENSION_PERCNTG                                    175     //31
#define SETTING_FRAME_SET_SUSPENSION_TIME                                       176     //30
#define SETTING_FRAME_SET_LEVEL                                                 177     //51
#define SETTING_FRAME_SET_RR_INTERVAL_VAL                                       178      //88
#define SETTING_FRAME_SET_TACHYCARDIA_VAL                                       179      //88




/*Client To Remote Common Frame Status Number*/
#define CLNT_TO_REM_CMN_FRAME_ALL_PARAMETER_TX_START	        1
#define CLNT_TO_REM_CMN_FRAME_ALL_PARAMETER_TX_STOP				2
#define CLNT_TO_REM_CMN_FRAME_CONTINUOUS_TX_START				3
#define CLNT_TO_REM_CMN_FRAME_CONTINUOUS_TX_STOP				4
#define CLNT_TO_REM_CMN_FRAME_CONTINUOUS_TX_RESTART				5
#define CLNT_TO_REM_CMN_FRAME_CHECKSUM_MATCHED					6
#define CLNT_TO_REM_CMN_FRAME_CHECKSUM_NOT_MATCHED				7
#define CLNT_TO_REM_CMN_FRAME_EVENT_TX_START					10
#define CLNT_TO_REM_CMN_FRAME_EVENT_TX_RESTART					12
#define CLNT_TO_REM_CMN_FRAME_MCD_PROCESSING_EVNET				13
#define CLNT_TO_REM_CMN_FRAME_TELL_OPERATION_LEVEL_I			18
#define CLNT_TO_REM_CMN_FRAME_TELL_OPERATION_LEVEL_II			19
#define CLNT_TO_REM_CMN_FRAME_TELL_OPERATION_LEVEL_III			20

#endif /* BluetoothCodes_h */
