//
//  DoctorThreshold.m
//  HealthWatch
//
//  Created by METSL MAC MINI on 31/07/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import "DoctorThreshold.h"


@implementation DoctorThreshold

@dynamic code;
@dynamic created_date_time;
@dynamic created_from;
@dynamic dd;
@dynamic ddflag;
@dynamic device_id;
@dynamic modify_datetime;
@dynamic modify_from;
@dynamic patient_id;
@dynamic posture;
@dynamic status;
@dynamic updated_to_device;
@dynamic updated_to_site;

@end
