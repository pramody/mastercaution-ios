//
//  CustomIndicatorView.h
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 03/07/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import "LLARingSpinnerView.h"

@interface CustomIndicatorView : LLARingSpinnerView
-(void) startLoading:(UIView *)parentView;
-(void)stopLoading:(UIView *)parentView;
@end
