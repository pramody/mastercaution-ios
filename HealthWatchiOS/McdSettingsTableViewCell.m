//
//  McdSettingsTableViewCell.m
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 19/06/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import "McdSettingsTableViewCell.h"

@implementation McdSettingsTableViewCell
@synthesize rowSettingTitle,rowSettingValue,rightArrowBlackImg,oneForAllPickerViewControl,expandView,topShowView,postTimeGrayView,postTimeStepperControl,postTimeStepperText,preTimeGrayView,preTimeStepperControl,preTimeStepperText;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
