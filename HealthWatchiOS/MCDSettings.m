//
//  MCDSettings.m
//  HealthWatch
//
//  Created by METSL MAC MINI on 31/07/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import "MCDSettings.h"


@implementation MCDSettings

@dynamic base_line_fil;
@dynamic bt_device_add;
@dynamic created_by;
@dynamic created_datetime;
@dynamic created_from;
@dynamic fiftyhz_filter;
@dynamic inverted_t_wave;
@dynamic gain;
@dynamic lead_config;
@dynamic leads;
@dynamic mcd_setdatetime;
@dynamic modified_by;
@dynamic modified_from;
@dynamic modified_time;
@dynamic orientation;
@dynamic respiration;
@dynamic respiration_cal;
@dynamic sampling_rate;
@dynamic sixtyhz_filter;
@dynamic status;
@dynamic timeinterval_post;
@dynamic timeinterval_pre;
@dynamic volume_level;
@dynamic updated_to_site;
@dynamic updated_to_mcd;
@dynamic xmpp_msgid;

@end
