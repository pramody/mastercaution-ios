//
//  PatientDetails.h
//  HealthWatch
//
//  Created by METSL MAC MINI on 22/09/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface PatientDetails : NSManagedObject

@property (nonatomic, retain) NSString * active_since;
@property (nonatomic, retain) NSString * blocked;
@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSNumber * code;
@property (nonatomic, retain) NSString * country;
@property (nonatomic, retain) NSString * device_id;
@property (nonatomic, retain) NSString * dob1;
@property (nonatomic, retain) NSString * doc_address;
@property (nonatomic, retain) NSString * doc_email;
@property (nonatomic, retain) NSString * doc_name;
@property (nonatomic, retain) NSString * doc_ph_no;
@property (nonatomic, retain) NSString * doc_country;
@property (nonatomic, retain) NSString * doc_id;
@property (nonatomic, retain) NSString * doc_degree;
@property (nonatomic, retain) NSString * ecode;
@property (nonatomic, retain) NSString * ecode_desc;
@property (nonatomic, retain) NSString * family_name;
@property (nonatomic, retain) NSString * gender1;
@property (nonatomic, retain) NSString * given_name;
@property (nonatomic, retain) NSString * height1;
@property (nonatomic, retain) NSString * level;
@property (nonatomic, retain) NSString * middle_name;
@property (nonatomic, retain) NSString * mode;
@property (nonatomic, retain) NSString * notification_id;
@property (nonatomic, retain) NSString * notification_pwd;
@property (nonatomic, retain) NSString * pat_updated_site;
@property (nonatomic, retain) NSString * patient_add;
@property (nonatomic, retain) NSString * patient_id;
@property (nonatomic, retain) NSString * patient_location;
@property (nonatomic, retain) NSString * patient_name;
@property (nonatomic, retain) NSString * server_name;
@property (nonatomic, retain) NSString * state;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * street;
@property (nonatomic, retain) NSString * uid;
@property (nonatomic, retain) NSString * weight;
@property (nonatomic, retain) NSString * zipcode;
@property (nonatomic, retain) NSString * modified_datetime;
@property (nonatomic, retain) NSString * patient_mobno;


@end
