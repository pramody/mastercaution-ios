//
//  EventStorage.m
//  HealthWatch
//
//  Created by METSL MAC MINI on 01/09/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import "EventStorage.h"


@implementation EventStorage

@dynamic event_datetime;
@dynamic event_ecgfilepath;
@dynamic event_ecg_id;
@dynamic event_glucose;
@dynamic event_latitude;
@dynamic event_longitude;
@dynamic event_reason;
@dynamic event_temp;
@dynamic event_value;
@dynamic event_weight;
@dynamic event_bpdia;
@dynamic event_bpsys;
@dynamic event_code;
@dynamic event_heartrate;
@dynamic event_lead_config;
@dynamic event_lead_detection;
@dynamic event_posture_code;
@dynamic event_pr_interval;
@dynamic event_qrs_interval;
@dynamic event_qtc_interval;
@dynamic event_qt_interval;
@dynamic event_receive_complete;
@dynamic event_respiration;
@dynamic event_respiration_amplitude;
@dynamic event_respiration_onoff;
@dynamic event_resp_variance;
@dynamic event_rr_interval;
@dynamic event_sampling_rate;
@dynamic event_thresholdtype;
@dynamic event_type;
@dynamic event_t_wave;
@dynamic event_updated_to_device;
@dynamic event_mcdlead;
@dynamic event_mail_sms;
@dynamic event_oximetry;
@end
