//
//   Draw the frame.h
//  Master Caution
//
//  Created by METSL MAC MINI on 01/09/16.
//  Copyright © 2016 METSL MAC MINI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface _Draw_the_frame : UIView
{
    CGContextRef context;
      CGContextRef myBitmapContext1;
   
}

@property (nonatomic) CGPoint endPoint;
@property (nonatomic) CGPoint endPoint2;

-(void) plotWave :(NSInteger [])in_pt;
@end
