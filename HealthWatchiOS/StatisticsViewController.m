//
//  StatisticsViewController.m
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 01/07/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import "StatisticsViewController.h"

#import "PNChart.h"


@interface StatisticsViewController ()

@end

@implementation StatisticsViewController
@synthesize headerBlueView,tabDivider1,tabDivider2,tabDivider3,tabHeaderMainView,arrhythmia,ischemia,otherevent,total,pieChart,legend,maxhrvalue,maxrespvalue,maxtempvalue,minhrvalue,minrespvalue,mintempvalue,hrmaxdate,hrmindate,tempmaxdate,tempmindate,respmaxdate,respmindate,tempmaxunit,tempminunit;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    AppDelegate * appDelegate = (AppDelegate*)UIApplication.sharedApplication.delegate ;
    appDelegate.orientationKey = @"PRO";
    [appDelegate forceOrientationTakePlace:appDelegate.orientationKey];
    
    self.slidingViewController.topViewAnchoredGesture = ECSlidingViewControllerAnchoredGestureTapping | ECSlidingViewControllerAnchoredGesturePanning;
    self.slidingViewController.customAnchoredGestures =@[];
    
    
    //data
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"EventStorage" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    
    NSError *error = nil;
    NSString *ecodevalue;
    arrthmiacnt=0;
    othercnt=0;
    tempcnt=0,respcnt=0,apneacnt=0,nomotioncnt=0,ondemandcnt=0,patientcallcnt=0,othercnt=0;
    supraventcnt=0,ventricnt=0,bradcnt=0,pausecnt=0,asvstolecnt=0,pvccnt=0,bigemenvcnt=0,trigemnycnt=0,tripletcnt=0,coupletcnt=0;
    STelcnt=0,STdecnt=0,Prolcnt=0,qrscnt=0,twavecnt=0;
   
    
    hearratevalue =[[NSMutableArray alloc]init];
    temperaturevalue=[[NSMutableArray alloc]init];
    respirationvalue=[[NSMutableArray alloc]init];
    event_datetime=[[NSMutableArray alloc]init];
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        NSLog(@"%@", result);
        
        if (result.count > 0) {
            
            for(int j=0;j<result.count;j++)
            {
                NSManagedObject *eventsett = (NSManagedObject *)[result objectAtIndex:j];
                NSLog(@"1 - %@", eventsett);
                ecodevalue=[[eventsett valueForKey:@"event_code"]stringValue];
                //arrythmia
                if([ecodevalue isEqualToString:@"2007"])
                {
                    supraventcnt=supraventcnt+1;
                    arrthmiacnt=arrthmiacnt+1;
                   [event_datetime addObject:[eventsett valueForKey:@"event_datetime"]];
                    
                    [hearratevalue addObject:[eventsett valueForKey:@"event_heartrate"]];
                    [temperaturevalue addObject:[eventsett valueForKey:@"event_temp"]];
                     [respirationvalue addObject:[eventsett valueForKey:@"event_respiration"]];
                    
                    
                }
                if([ecodevalue isEqualToString:@"2011"])
                {
                    ventricnt=ventricnt+1;
                     arrthmiacnt=arrthmiacnt+1;
                   [event_datetime addObject:[eventsett valueForKey:@"event_datetime"]];
                    [hearratevalue addObject:[eventsett valueForKey:@"event_heartrate"]];
                    [temperaturevalue addObject:[eventsett valueForKey:@"event_temp"]];
                    [respirationvalue addObject:[eventsett valueForKey:@"event_respiration"]];
                    
                }
                if([ecodevalue isEqualToString:@"2044"])
                {
                    arrthmiacnt=arrthmiacnt+1;
                   [event_datetime addObject:[eventsett valueForKey:@"event_datetime"]];
                    [hearratevalue addObject:[eventsett valueForKey:@"event_heartrate"]];
                    [temperaturevalue addObject:[eventsett valueForKey:@"event_temp"]];
                    [respirationvalue addObject:[eventsett valueForKey:@"event_respiration"]];
                                     }
                if([ecodevalue isEqualToString:@"2017"])
                {
                    bradcnt=bradcnt+1;
                    arrthmiacnt=arrthmiacnt+1;
                   [event_datetime addObject:[eventsett valueForKey:@"event_datetime"]];
                    [hearratevalue addObject:[eventsett valueForKey:@"event_heartrate"]];
                    [temperaturevalue addObject:[eventsett valueForKey:@"event_temp"]];
                    [respirationvalue addObject:[eventsett valueForKey:@"event_respiration"]];
                    
                }
                if([ecodevalue isEqualToString:@"2026"])
                {
                    pausecnt=pausecnt+1;
                    arrthmiacnt=arrthmiacnt+1;
                   [event_datetime addObject:[eventsett valueForKey:@"event_datetime"]];
                    [hearratevalue addObject:[eventsett valueForKey:@"event_heartrate"]];
                    [temperaturevalue addObject:[eventsett valueForKey:@"event_temp"]];
                    [respirationvalue addObject:[eventsett valueForKey:@"event_respiration"]];
                    
                }
                if([ecodevalue isEqualToString:@"2015"])
                {
                    pvccnt=pvccnt+1;
                    arrthmiacnt=arrthmiacnt+1;
                   [event_datetime addObject:[eventsett valueForKey:@"event_datetime"]];
                    [hearratevalue addObject:[eventsett valueForKey:@"event_heartrate"]];
                    [temperaturevalue addObject:[eventsett valueForKey:@"event_temp"]];
                    [respirationvalue addObject:[eventsett valueForKey:@"event_respiration"]];
                    
                }
                if([ecodevalue isEqualToString:@"2016"])
                {
                    asvstolecnt=asvstolecnt+1;
                    arrthmiacnt=arrthmiacnt+1;
                   [event_datetime addObject:[eventsett valueForKey:@"event_datetime"]];
                    [hearratevalue addObject:[eventsett valueForKey:@"event_heartrate"]];
                    [temperaturevalue addObject:[eventsett valueForKey:@"event_temp"]];
                    [respirationvalue addObject:[eventsett valueForKey:@"event_respiration"]];
                     
                }
                if([ecodevalue isEqualToString:@"2032"])
                {
                    bigemenvcnt=bigemenvcnt+1;
                    arrthmiacnt=arrthmiacnt+1;
                   [event_datetime addObject:[eventsett valueForKey:@"event_datetime"]];
                    [hearratevalue addObject:[eventsett valueForKey:@"event_heartrate"]];
                    [temperaturevalue addObject:[eventsett valueForKey:@"event_temp"]];
                    [respirationvalue addObject:[eventsett valueForKey:@"event_respiration"]];
                    
                }
                if([ecodevalue isEqualToString:@"2029"])
                {
                    tripletcnt=tripletcnt+1;
                    arrthmiacnt=arrthmiacnt+1;
                   [event_datetime addObject:[eventsett valueForKey:@"event_datetime"]];
                    [hearratevalue addObject:[eventsett valueForKey:@"event_heartrate"]];
                    [temperaturevalue addObject:[eventsett valueForKey:@"event_temp"]];
                    [respirationvalue addObject:[eventsett valueForKey:@"event_respiration"]];
                    
                }
                if([ecodevalue isEqualToString:@"2030"])
                {
                    coupletcnt=coupletcnt+1;
                    arrthmiacnt=arrthmiacnt+1;
                   [event_datetime addObject:[eventsett valueForKey:@"event_datetime"]];
                    [hearratevalue addObject:[eventsett valueForKey:@"event_heartrate"]];
                    [temperaturevalue addObject:[eventsett valueForKey:@"event_temp"]];
                    [respirationvalue addObject:[eventsett valueForKey:@"event_respiration"]];
                     
                }
                if([ecodevalue isEqualToString:@"2031"])
                {
                    trigemnycnt=trigemnycnt+1;
                    arrthmiacnt=arrthmiacnt+1;
                   [event_datetime addObject:[eventsett valueForKey:@"event_datetime"]];
                    [hearratevalue addObject:[eventsett valueForKey:@"event_heartrate"]];
                    [temperaturevalue addObject:[eventsett valueForKey:@"event_temp"]];
                    [respirationvalue addObject:[eventsett valueForKey:@"event_respiration"]];
                     
                }
                
                
                //ischemia
                if([ecodevalue isEqualToString:@"2001"])
                {
                    STelcnt=STelcnt+1;
                    ischemiacnt=ischemiacnt+1;
                   [event_datetime addObject:[eventsett valueForKey:@"event_datetime"]];
                    [hearratevalue addObject:[eventsett valueForKey:@"event_heartrate"]];
                    [temperaturevalue addObject:[eventsett valueForKey:@"event_temp"]];
                    [respirationvalue addObject:[eventsett valueForKey:@"event_respiration"]];
                     
                }
                if([ecodevalue isEqualToString:@"2002"])
                {
                    STdecnt=STdecnt=+1;
                    ischemiacnt=ischemiacnt+1;
                   [event_datetime addObject:[eventsett valueForKey:@"event_datetime"]];
                    [hearratevalue addObject:[eventsett valueForKey:@"event_heartrate"]];
                    [temperaturevalue addObject:[eventsett valueForKey:@"event_temp"]];
                    [respirationvalue addObject:[eventsett valueForKey:@"event_respiration"]];
                     
                }
                if([ecodevalue isEqualToString:@"2003"])
                {
                    twavecnt=twavecnt+1;
                    ischemiacnt=ischemiacnt+1;
                   [event_datetime addObject:[eventsett valueForKey:@"event_datetime"]];
                    [hearratevalue addObject:[eventsett valueForKey:@"event_heartrate"]];
                    [temperaturevalue addObject:[eventsett valueForKey:@"event_temp"]];
                    [respirationvalue addObject:[eventsett valueForKey:@"event_respiration"]];
                     
                }
                if([ecodevalue isEqualToString:@"2005"])
                {
                    Prolcnt=Prolcnt+1;
                    ischemiacnt=ischemiacnt+1;
                    [hearratevalue addObject:[eventsett valueForKey:@"event_heartrate"]];
                    [temperaturevalue addObject:[eventsett valueForKey:@"event_temp"]];
                    [respirationvalue addObject:[eventsett valueForKey:@"event_respiration"]];
                     
                }
                if([ecodevalue isEqualToString:@"2006"])
                {
                    qrscnt=qrscnt+1;
                    ischemiacnt=ischemiacnt+1;
                   [event_datetime addObject:[eventsett valueForKey:@"event_datetime"]];
                    [hearratevalue addObject:[eventsett valueForKey:@"event_heartrate"]];
                    [temperaturevalue addObject:[eventsett valueForKey:@"event_temp"]];
                    [respirationvalue addObject:[eventsett valueForKey:@"event_respiration"]];
                     
                }

                
                //Other
                if([ecodevalue isEqualToString:@"2022"])
                {
                      tempcnt=tempcnt+1;
                    othercnt=othercnt+1;
                   [event_datetime addObject:[eventsett valueForKey:@"event_datetime"]];
                    [hearratevalue addObject:[eventsett valueForKey:@"event_heartrate"]];
                    [temperaturevalue addObject:[eventsett valueForKey:@"event_temp"]];
                    [respirationvalue addObject:[eventsett valueForKey:@"event_respiration"]];
                     
                }
                if([ecodevalue isEqualToString:@"2036"])
                {
                    respcnt=respcnt+1;
                     othercnt=othercnt+1;
                   [event_datetime addObject:[eventsett valueForKey:@"event_datetime"]];
                    [hearratevalue addObject:[eventsett valueForKey:@"event_heartrate"]];
                    [temperaturevalue addObject:[eventsett valueForKey:@"event_temp"]];
                    [respirationvalue addObject:[eventsett valueForKey:@"event_respiration"]];
                     
                }
                if([ecodevalue isEqualToString:@"2021"])
                {
                    apneacnt=apneacnt+1;
                     othercnt=othercnt+1;
                   [event_datetime addObject:[eventsett valueForKey:@"event_datetime"]];
                    [hearratevalue addObject:[eventsett valueForKey:@"event_heartrate"]];
                    [temperaturevalue addObject:[eventsett valueForKey:@"event_temp"]];
                    [respirationvalue addObject:[eventsett valueForKey:@"event_respiration"]];
                     
                }
                if([ecodevalue isEqualToString:@"2024"])
                {
                    nomotioncnt=nomotioncnt+1;
                     othercnt=othercnt+1;
                   [event_datetime addObject:[eventsett valueForKey:@"event_datetime"]];
                    [hearratevalue addObject:[eventsett valueForKey:@"event_heartrate"]];
                    [temperaturevalue addObject:[eventsett valueForKey:@"event_temp"]];
                    [respirationvalue addObject:[eventsett valueForKey:@"event_respiration"]];
                     
                }
                if([ecodevalue isEqualToString:@"2040"])
                {
                    othermanualcnt=othermanualcnt+1;
                    othercnt=othercnt+1;
                   [event_datetime addObject:[eventsett valueForKey:@"event_datetime"]];
                    [hearratevalue addObject:[eventsett valueForKey:@"event_heartrate"]];
                    [temperaturevalue addObject:[eventsett valueForKey:@"event_temp"]];
                    [respirationvalue addObject:[eventsett valueForKey:@"event_respiration"]];
                     
                
                }
                if([ecodevalue isEqualToString:@"2034"])
                {
                    patientcallcnt=patientcallcnt+1;
                   [event_datetime addObject:[eventsett valueForKey:@"event_datetime"]];
                    [hearratevalue addObject:[eventsett valueForKey:@"event_heartrate"]];
                    [temperaturevalue addObject:[eventsett valueForKey:@"event_temp"]];
                    [respirationvalue addObject:[eventsett valueForKey:@"event_respiration"]];
                     
                }
                if([ecodevalue isEqualToString:@"2043"])
                {
                    ondemandcnt=ondemandcnt+1;
                   [event_datetime addObject:[eventsett valueForKey:@"event_datetime"]];
                    [hearratevalue addObject:[eventsett valueForKey:@"event_heartrate"]];
                    [temperaturevalue addObject:[eventsett valueForKey:@"event_temp"]];
                    [respirationvalue addObject:[eventsett valueForKey:@"event_respiration"]];
                     
                }

                
                
            }
        }
    }
    
   
     if (result.count > 0)
     {
   /* NSArray * heartarray = [hearratevalue sortedArrayUsingSelector:@selector(compare:)];
    NSArray * temparray = [temperaturevalue sortedArrayUsingSelector:@selector(compare:)];
     NSArray * resparray = [respirationvalue sortedArrayUsingSelector:@selector(compare:)];*/
    
        
       NSDictionary *dictionary = [NSDictionary dictionaryWithObjects:event_datetime forKeys:hearratevalue];
         // Sort the first array
       NSArray *heartarray = [[dictionary allKeys] sortedArrayUsingSelector:@selector(compare:)];
         // Sort the second array based on the sorted first array
       NSArray *heartdateArray = [dictionary objectsForKeys:heartarray notFoundMarker:[NSNull null]];
         
       NSDictionary *dictionary1 = [NSDictionary dictionaryWithObjects:event_datetime forKeys:temperaturevalue];
         // Sort the first array
       NSArray *temparray = [[dictionary1 allKeys] sortedArrayUsingSelector:@selector(compare:)];
         // Sort the second array based on the sorted first array
       NSArray *tempdateArray = [dictionary1 objectsForKeys:temparray notFoundMarker:[NSNull null]];
         
       NSDictionary *dictionary2 = [NSDictionary dictionaryWithObjects:event_datetime forKeys:respirationvalue];
         // Sort the first array
       NSArray *resparray = [[dictionary2 allKeys] sortedArrayUsingSelector:@selector(compare:)];
         // Sort the second array based on the sorted first array
       NSArray *respdateArray = [dictionary2 objectsForKeys:resparray notFoundMarker:[NSNull null]];
        
     minhrvalue.text  = heartarray[0];
     maxhrvalue.text  =[heartarray lastObject];
     hrmindate.text   =heartdateArray[0];
     hrmaxdate.text   =[heartdateArray lastObject];
    
      CommonHelper *help=[[CommonHelper alloc]init];
      NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
      NSString *temperaturetype=[TempDefault objectForKey:@"TEMPERATURE"];
     if([temperaturetype isEqualToString:@"CELSIUS"])
     {
     mintempvalue.text = [NSString stringWithFormat:@"%.2f",[temparray[0] floatValue]];
     maxtempvalue.text = [NSString stringWithFormat:@"%.2f",[[temparray lastObject] floatValue]];
     tempmaxunit.text=[NSString stringWithFormat:@"(C)%@", @"\u00B0"];
     tempminunit.text=[NSString stringWithFormat:@"(C)%@", @"\u00B0"];
    
     }
     else
     {
         mintempvalue.text =[help celsiustoFahernheit:temparray[0]];
         maxtempvalue.text =[help celsiustoFahernheit:[temparray lastObject]];
         tempmaxunit.text=[NSString stringWithFormat:@"(F)%@", @"\u00B0"];
         tempminunit.text=[NSString stringWithFormat:@"(F)%@", @"\u00B0"];
    }
     tempmindate.text  =tempdateArray[0];
     tempmaxdate.text  =[tempdateArray lastObject];
         
     minrespvalue.text = resparray[0];
     maxrespvalue.text =[resparray lastObject];
     respmindate.text=respdateArray[0];
     respmaxdate.text=[respdateArray lastObject];
         
     }

    
     pieChart.delegate=self;
    NSArray *items = @[[PNPieChartDataItem dataItemWithValue:arrthmiacnt color:PNRed description:@"ARRHYTHMIA"],
                       [PNPieChartDataItem dataItemWithValue:ischemiacnt color:PNBlue description:@"ISCHEMIA"],
                       [PNPieChartDataItem dataItemWithValue:othercnt color:PNGreen description:@"OTHERS"],
                       ];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        pieChart = [[PNPieChart alloc] initWithFrame:CGRectMake(150.0, 255.0, 440.0, 440.0) items:items];
        pieChart.descriptionTextColor = [UIColor whiteColor];
        pieChart.descriptionTextFont  = [UIFont fontWithName:@"Roboto-Medium" size:14.0];
        [pieChart strokeChart];
       
         [self.view addSubview:pieChart];
        self.pieChart.legendStyle = PNLegendItemStyleStacked;
        self.pieChart.legendFont = [UIFont fontWithName:@"Roboto-Medium" size:14.0];
        legend = [self.pieChart getLegendWithMaxWidth:200];
        legend = [self.pieChart getLegendWithMaxWidth:320];
        [legend setFrame:CGRectMake(150, 730, legend.frame.size.width,100)];

        [self.view addSubview:legend];
    } else
    {
        pieChart = [[PNPieChart alloc] initWithFrame:CGRectMake(40.0, 185.0, 240.0, 240.0) items:items];
        pieChart.descriptionTextColor = [UIColor whiteColor];
        pieChart.descriptionTextFont  = [UIFont fontWithName:@"Roboto-Medium" size:10.0];
        [pieChart strokeChart];
         [self.view addSubview:pieChart];
        self.pieChart.legendStyle = PNLegendItemStyleStacked;
        self.pieChart.legendFont = [UIFont fontWithName:@"Roboto-Medium" size:10.0];
        legend = [self.pieChart getLegendWithMaxWidth:320];
        [legend setFrame:CGRectMake(50, 430, legend.frame.size.width,100)];
        [self.view addSubview:legend];
    }
    
    
   
    
    [self initalizeElements];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupInitialValue
{
    blueColorMainAppUIColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"blue_bg_full.png"]];
    headerBlueView.backgroundColor = blueColorMainAppUIColor;
}

-(IBAction)goToDashboard:(id)sender{
    self.slidingViewController.topViewController.view.layer.transform = CATransform3DMakeScale(1, 1, 1);
    self.slidingViewController.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"DashboardStoryboardId"];
}

-(IBAction)leftSideButtonAction:(id)sender{
    [self.slidingViewController anchorTopViewToRightAnimated:YES];
}


-(void) initalizeElements
{
    blueBorderColorForTab = [UIColor colorWithPatternImage:[UIImage imageNamed:@"blue_border_color"]];
    tabHeaderMainView.layer.borderWidth     =   1.0;
    tabHeaderMainView.layer.borderColor     =   blueBorderColorForTab.CGColor;
    
    tabHeaderMainView.layer.cornerRadius    =   7.0;
    tabHeaderMainView.layer.masksToBounds   =   true;
    
    tabDivider1.backgroundColor = blueBorderColorForTab;
    
    tabDivider2.backgroundColor = blueBorderColorForTab;
    tabDivider3.backgroundColor = blueBorderColorForTab;
     [total setSelected:true];
    [arrhythmia setSelected:false];
    [ischemia setSelected:false];
    [otherevent setSelected:false];
}

-(IBAction)btntotal:(id)sender
{
    [pieChart removeFromSuperview];
    [legend removeFromSuperview];
    [total setSelected:true];
    [arrhythmia setSelected:false];
    [ischemia setSelected:false];
    [otherevent setSelected:false];
    NSArray *items = @[[PNPieChartDataItem dataItemWithValue:arrthmiacnt color:PNRed description:@"ARRHYTHMIA"],
                       [PNPieChartDataItem dataItemWithValue:ischemiacnt color:PNBlue description:@"ISCHEMIA"],
                       [PNPieChartDataItem dataItemWithValue:othercnt color:PNGreen description:@"OTHERS"],
                       ];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
       pieChart = [[PNPieChart alloc] initWithFrame:CGRectMake(150.0, 255.0, 440.0, 440.0) items:items];
        pieChart.descriptionTextColor = [UIColor whiteColor];
        pieChart.descriptionTextFont  = [UIFont fontWithName:@"Roboto-Medium" size:14.0];
        [pieChart strokeChart];
        [self.view addSubview:pieChart];
        
        self.pieChart.legendStyle = PNLegendItemStyleStacked;
        self.pieChart.legendFont = [UIFont fontWithName:@"Roboto-Medium" size:14.0];
        legend = [self.pieChart getLegendWithMaxWidth:200];
        legend = [self.pieChart getLegendWithMaxWidth:320];
        [legend setFrame:CGRectMake(150, 730, legend.frame.size.width,100)];
        
        [self.view addSubview:legend];
    } else
    {
        pieChart = [[PNPieChart alloc] initWithFrame:CGRectMake(40.0, 185.0, 240.0, 240.0) items:items];
        pieChart.descriptionTextColor = [UIColor whiteColor];
        pieChart.descriptionTextFont  = [UIFont fontWithName:@"Roboto-Medium" size:10.0];
        [pieChart strokeChart];
        [self.view addSubview:pieChart];
        self.pieChart.legendStyle = PNLegendItemStyleStacked;
        self.pieChart.legendFont = [UIFont fontWithName:@"Roboto-Medium" size:10.0];
        legend = [self.pieChart getLegendWithMaxWidth:320];
        [legend setFrame:CGRectMake(50, 430, legend.frame.size.width,100)];
        [self.view addSubview:legend];
    }
    

}

-(IBAction)btnarrythmi:(id)sender
{
   [pieChart removeFromSuperview];
   [legend removeFromSuperview];
    [arrhythmia setSelected:true];
    [total setSelected:false];
    [ischemia setSelected:false];
    [otherevent setSelected:false];
    NSArray *items = @[[PNPieChartDataItem dataItemWithValue:supraventcnt color:[self getUIColorObjectFromHexString:@"#F2D06B" alpha:.9] description:@"SupraVT"],
                       [PNPieChartDataItem dataItemWithValue:ventricnt color:[self getUIColorObjectFromHexString:@"#F15441" alpha:.9] description:@"VentricularT"],
                       [PNPieChartDataItem dataItemWithValue:bradcnt color:[self getUIColorObjectFromHexString:@"#F28705" alpha:.9] description:@"Bradycardia"],
                       [PNPieChartDataItem dataItemWithValue:pausecnt color:[self getUIColorObjectFromHexString:@"#BFA27E" alpha:.9] description:@"pause"],
                       [PNPieChartDataItem dataItemWithValue:pvccnt color:[self getUIColorObjectFromHexString:@"#F23005" alpha:.9] description:@"PVC"],
                       [PNPieChartDataItem dataItemWithValue:asvstolecnt color:[self getUIColorObjectFromHexString:@"#BF1A0B" alpha:.9] description:@"Asystole"],
                       [PNPieChartDataItem dataItemWithValue:bigemenvcnt color:[self getUIColorObjectFromHexString:@"#590202" alpha:.9] description:@"Bigeminy"],
                       [PNPieChartDataItem dataItemWithValue:trigemnycnt color:[self getUIColorObjectFromHexString:@"#5CC9CB" alpha:.9] description:@"Trigeminy"],
                       [PNPieChartDataItem dataItemWithValue:coupletcnt color:[self getUIColorObjectFromHexString:@"#BBBF34" alpha:.9] description:@"Couplet"],
                       [PNPieChartDataItem dataItemWithValue:tripletcnt color:[self getUIColorObjectFromHexString:@"#038C17" alpha:.9] description:@"Triplet"],
                       ];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        pieChart = [[PNPieChart alloc] initWithFrame:CGRectMake(150.0, 255.0, 440.0, 440.0) items:items];
       
        pieChart.descriptionTextColor = [UIColor whiteColor];
        pieChart.descriptionTextFont  = [UIFont fontWithName:@"Roboto-Medium" size:14.0];
        [pieChart strokeChart];
        [self.view addSubview:pieChart];
        self.pieChart.legendStyle = PNLegendItemStyleStacked;
        self.pieChart.legendFont = [UIFont fontWithName:@"Roboto-Medium" size:14.0];
        legend = [self.pieChart getLegendWithMaxWidth:200];
        legend = [self.pieChart getLegendWithMaxWidth:320];
        [legend setFrame:CGRectMake(150, 730, legend.frame.size.width,100)];
        
        [self.view addSubview:legend];
    } else
    {
       pieChart = [[PNPieChart alloc] initWithFrame:CGRectMake(40.0, 185.0, 240.0, 240.0) items:items];
          [pieChart removeFromSuperview];
        pieChart.descriptionTextColor = [UIColor whiteColor];
        pieChart.descriptionTextFont  = [UIFont fontWithName:@"Roboto-Medium" size:10.0];
        [pieChart strokeChart];
        [self.view addSubview:pieChart];
        
        self.pieChart.legendStyle = PNLegendItemStyleStacked;

        legend = [self.pieChart getLegendWithMaxWidth:320];
        [legend setFrame:CGRectMake(50, 420, legend.frame.size.width,100)];
        [self.view addSubview:legend];
    }

}
-(IBAction)btnischmia:(id)sender
{
    [pieChart removeFromSuperview];
    [legend removeFromSuperview];
     [ischemia setSelected:true];
    [total setSelected:false];
    [arrhythmia setSelected:false];
    [otherevent setSelected:false];
    NSArray *items = @[[PNPieChartDataItem dataItemWithValue:STelcnt color:[self getUIColorObjectFromHexString:@"#2E707B" alpha:.9] description:@"ST El"],
                       [PNPieChartDataItem dataItemWithValue:STdecnt color:[self getUIColorObjectFromHexString:@"#5CC9CB" alpha:.9] description:@"ST De"],
                       [PNPieChartDataItem dataItemWithValue:Prolcnt color:[self getUIColorObjectFromHexString:@"#BBBF34" alpha:.9] description:@"Prol QT"],
                       [PNPieChartDataItem dataItemWithValue:qrscnt color:[self getUIColorObjectFromHexString:@"#590202" alpha:.9] description:@"QRS Wide"],
                       [PNPieChartDataItem dataItemWithValue:twavecnt color:[self getUIColorObjectFromHexString:@"#F15441" alpha:.9] description:@"T Wave"],
                       ];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        pieChart = [[PNPieChart alloc] initWithFrame:CGRectMake(150.0, 255.0, 440.0, 440.0) items:items];
        pieChart.descriptionTextColor = [UIColor whiteColor];
        pieChart.descriptionTextFont  = [UIFont fontWithName:@"Roboto-Medium" size:14.0];
        [pieChart strokeChart];
        [self.view addSubview:pieChart];
        
        self.pieChart.legendStyle = PNLegendItemStyleStacked;
        self.pieChart.legendFont = [UIFont fontWithName:@"Roboto-Medium" size:14.0];
        legend = [self.pieChart getLegendWithMaxWidth:200];
        legend = [self.pieChart getLegendWithMaxWidth:320];
        [legend setFrame:CGRectMake(150, 730, legend.frame.size.width,100)];
        
        [self.view addSubview:legend];
    } else
    {
        pieChart = [[PNPieChart alloc] initWithFrame:CGRectMake(40.0, 185.0, 240.0, 240.0) items:items];
        pieChart.descriptionTextColor = [UIColor whiteColor];
        pieChart.descriptionTextFont  = [UIFont fontWithName:@"Roboto-Medium" size:10.0];
        [pieChart strokeChart];
        [self.view addSubview:pieChart];
        self.pieChart.legendStyle = PNLegendItemStyleStacked;
        self.pieChart.legendFont = [UIFont fontWithName:@"Roboto-Medium" size:10.0];
        legend = [self.pieChart getLegendWithMaxWidth:320];
        [legend setFrame:CGRectMake(50, 430, legend.frame.size.width,100)];
        [self.view addSubview:legend];
    }

}
-(IBAction)btnotherevent:(id)sender
{
    [pieChart removeFromSuperview];
    [legend removeFromSuperview];
    [otherevent setSelected:true];
    [total setSelected:false];     [ischemia setSelected:false];
   
    NSArray *items = @[[PNPieChartDataItem dataItemWithValue:tempcnt color:[self getUIColorObjectFromHexString:@"#5CC9CB" alpha:.9] description:@"Temperature"],
                       [PNPieChartDataItem dataItemWithValue:respcnt color:[self getUIColorObjectFromHexString:@"#F2D06B" alpha:.9] description:@"Respiration Rate"],
                       [PNPieChartDataItem dataItemWithValue:apneacnt color:[self getUIColorObjectFromHexString:@"#590202" alpha:.9] description:@"Apnea"],
                       [PNPieChartDataItem dataItemWithValue:nomotioncnt color:[self getUIColorObjectFromHexString:@"#BF1A0B" alpha:.9] description:@"No Motion"],
                       [PNPieChartDataItem dataItemWithValue:othermanualcnt color:[self getUIColorObjectFromHexString:@"#038C17" alpha:.9] description:@"Other Manual Event"],
                       [PNPieChartDataItem dataItemWithValue:ondemandcnt color:[self getUIColorObjectFromHexString:@"#BFA27E" alpha:.9] description:@"On Demand"],
                       [PNPieChartDataItem dataItemWithValue:patientcallcnt color:PNGreen description:@"Patient Call"],
                       ];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        pieChart = [[PNPieChart alloc] initWithFrame:CGRectMake(150.0, 255.0, 440.0, 440.0) items:items];
        pieChart.descriptionTextColor = [UIColor whiteColor];
        pieChart.descriptionTextFont  = [UIFont fontWithName:@"Avenir-Medium" size:14.0];
        [pieChart strokeChart];
        [self.view addSubview:pieChart];
        
        self.pieChart.legendStyle = PNLegendItemStyleStacked;
        self.pieChart.legendFont = [UIFont fontWithName:@"Roboto-Medium" size:14.0];
        legend = [self.pieChart getLegendWithMaxWidth:200];
        legend = [self.pieChart getLegendWithMaxWidth:320];
        [legend setFrame:CGRectMake(150, 730, legend.frame.size.width,100)];
        
        [self.view addSubview:legend];
    } else
    {
      
        
        pieChart = [[PNPieChart alloc] initWithFrame:CGRectMake(40.0, 185.0, 240.0, 240.0) items:items];
        pieChart.descriptionTextColor = [UIColor whiteColor];
        pieChart.descriptionTextFont  = [UIFont fontWithName:@"Avenir-Medium" size:10.0];
        [pieChart strokeChart];
        [self.view addSubview:pieChart];
        
        self.pieChart.legendStyle = PNLegendItemStyleStacked;
        self.pieChart.legendFont = [UIFont fontWithName:@"Roboto-Medium" size:10.0];
        legend = [self.pieChart getLegendWithMaxWidth:320];
        
        //Move legend to the desired position and add to view
        [legend setFrame:CGRectMake(50, 430, legend.frame.size.width,100)];
        [self.view addSubview:legend];
    }

}

- (UIColor *)getUIColorObjectFromHexString:(NSString *)hexStr alpha:(CGFloat)alpha
{
    // Convert hex string to an integer
    unsigned int hexint = [self intFromHexString:hexStr];
    
    // Create color object, specifying alpha as well
    UIColor *color =
    [UIColor colorWithRed:((CGFloat) ((hexint & 0xFF0000) >> 16))/255
                    green:((CGFloat) ((hexint & 0xFF00) >> 8))/255
                     blue:((CGFloat) (hexint & 0xFF))/255
                    alpha:alpha];
    
    return color;
}
- (unsigned int)intFromHexString:(NSString *)hexStr
{
    unsigned int hexInt = 0;
    
    // Create scanner
    NSScanner *scanner = [NSScanner scannerWithString:hexStr];
    
    // Tell scanner to skip the # character
    [scanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@"#"]];
    
    // Scan hex value
    [scanner scanHexInt:&hexInt];
    
    return hexInt;
}


-(void)userClickedOnPieIndexItem:(NSInteger)pieIndex
{
    NSLog(@"pix%ld",(long)pieIndex);
}

- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}


@end
