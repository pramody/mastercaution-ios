//
//  GeneralSettingsViewController.h
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 01/07/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//



@interface GeneralSettingsViewController : UIViewController<UIActionSheetDelegate>
{
    UIColor *blueColorMainAppUIColor;
    int direction;
    int shakes;
    BOOL standalone_Mode;
    NSString * patientid;
    NSString *deviceid;
    NSString *doctor_Country;
    NSString *doctorname;
    
}
@property (nonatomic, strong) IBOutlet UIView* headerBlueView;

@property (nonatomic, strong) IBOutlet UIActivityIndicatorView * spinner;
@property (nonatomic, strong) IBOutlet UILabel * lblspin;
@property (nonatomic, strong) IBOutlet UILabel * temperaturelbl;
@property (nonatomic, strong) IBOutlet UILabel * changelevellbl;
@property (nonatomic, strong) IBOutlet UILabel * Weightlbl;
@property (nonatomic, strong) IBOutlet UILabel * Heightlbl;


@property (nonatomic, strong) IBOutlet UIImageView * rightArrowBlackImg;
@property (nonatomic, strong) IBOutlet UIImageView * rightArrowBlackImg1;
@property (nonatomic, strong) IBOutlet UIImageView * rightArrowBlackImg2;
@property (nonatomic, strong) IBOutlet UIImageView * rightArrowBlackImg3;
@property (nonatomic, strong) IBOutlet UIImageView * rightArrowBlackImg4;
@property (nonatomic, strong) IBOutlet UIImageView * rightArrowBlackImg5;


-(void)UpdateLevel:(int)data;

@end
