//
//  ClinicalRecordsViewController.h
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 22/06/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//


#import "ClinicalRecordsTableViewCell.h"


enum {
    ALL        = 1,
    DETECTED   = 2,
    MANUAL1     = 3
};
typedef NSInteger ClinicalRecordsTabs;

@interface ClinicalRecordsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *allClinicalDataArray;
    NSMutableArray *cellRowStatus;
    NSMutableArray *currentTabData;
    NSMutableArray *cellExpandFlag;
    UIColor *blueBorderColorForTab;
    UIColor *tableBgGrayColor;
    
    NSUInteger selectedIndex;
    int Respiration;
    
    NSTimer *TableupdateTimer;
   
}



@property (nonatomic, strong) IBOutlet UIView * headerBlueView;
@property (nonatomic, strong) IBOutlet UIView * tabHeaderMainView;
@property (nonatomic, strong) IBOutlet UITableView * tableClinicalRecords;
@property (nonatomic, strong) IBOutlet UIView *tabDivider1;
@property (nonatomic, strong) IBOutlet UIView *tabDivider2;

@property (nonatomic, strong) IBOutlet UIButton * allTabButton;

@property (nonatomic, strong) IBOutlet UIButton *detectedTabButton;

@property (nonatomic, strong) IBOutlet UIButton *manualTabButton;
@property (nonatomic) ClinicalRecordsTabs selectedTabIs;
@property (nonatomic, strong) IBOutlet ClinicalRecordsTableViewCell* tableViewCellCustom;

@property (nonatomic, strong) IBOutlet UILabel *numberofevents;
@property (nonatomic, strong) IBOutlet UILabel *maximumevent;
@property (nonatomic, strong) IBOutlet UILabel *minimumevents;
@property (nonatomic, strong) IBOutlet UILabel *averagevents;








@end
