//
//  EventSuspensionSettingsViewController.h
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 01/07/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//


#import "McdSettingsTableViewCell.h"

@interface EventSuspensionSettingsViewController : UIViewController<UITextFieldDelegate , UITableViewDelegate, UIPickerViewDelegate>
{
    NSIndexPath * lastOpenIndexPath;
    UIColor * tableBgGrayColor;
    NSArray * filterStatusArray;
    NSArray *  mcdGainArray;
    NSArray *  mcgLeadSettingArray;
    NSArray *  mcdEventDetectionLeadArray;
    NSArray *  mcgSamplingRatesArray;
    NSString *  currentPickerViewOpendArray;
    UIColor *blueColorMainAppUIColor;
    
    int direction;
    int shakes;
    UILabel *lblspin;
    NSUInteger indexpthsec;
    NSUInteger indexrw;
    NSString * patientid;
    NSString *deviceid;
     NSString *doctorname;
    
     BOOL standalone_Mode;
}

@property (nonatomic, strong) IBOutlet UIButton *  lockunlocButton;
@property (nonatomic, strong) IBOutlet UIView * headerBlueView;
@property (nonatomic, strong) IBOutlet UITableView * tableViewMcdSettingsTable;
@property (nonatomic, strong) IBOutlet McdSettingsTableViewCell * tableViewMcdSettingsCell;

@property (nonatomic, strong) IBOutlet UIActivityIndicatorView * spinner;
@property (nonatomic, strong) IBOutlet NSString * TWave_Status;

-(void)UpdateSuspensionSett:(int)data;


@end
