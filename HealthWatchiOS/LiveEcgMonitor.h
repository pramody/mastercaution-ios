//
//  LiveEcgMonitor.h
//  HealthWatch
//
//  Created by METSL MAC MINI on 05/09/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "LeadPlayer.h"
#import "iCarousel.h"
#import "SwipeView.h"
#import "CBAutoScrollLabel.h"
#import <Foundation/Foundation.h>


@interface LiveEcgMonitor : UIViewController <iCarouselDataSource, iCarouselDelegate,UIWebViewDelegate,SwipeViewDataSource, SwipeViewDelegate>

{
 NSMutableArray *leads, *buffer;
 NSTimer *drawingTimer, *readDataTimer, *recordingTimer, *popDataTimer, *playSoundTimer;

UIImage *recordingImage;

int second;
BOOL stopTheTimer, autoStart, DEMO, monitoring;

UIButton *btnStart, *btnStop, *photoView;

UIScrollView *scrollView;
UIImage *screenShot;

int layoutScale;  // 0: 3x4   1: 2x6   2: 1x12
int startRecordingIndex, endRecordingIndex, HR;

NSString *now;
BOOL liveMode;
UILabel *labelRate, *labelProfileId, *labelProfileName, *labelMsg;
UIBarButtonItem *statusInfo, *btnDismiss, *btnRefresh;


int countOfPointsInQueue, countOfRecordingPoints;
int currentDrawingPoint, bufferCount;


LeadPlayer *firstLead;

int lastHR;
int newBornMode, errorCount;

NSString *fullString;
NSTimer *myTimer;
    NSArray *data12Arrays;
     NSMutableArray *Allarray;
    UIWebView * webView;
    
}


@property (nonatomic, strong) NSMutableArray *leads, *buffer;
@property (nonatomic, strong) IBOutlet UIButton *btnStart, *photoView, *btnRecord;
@property (nonatomic, strong) IBOutlet UIView *vwProfile, *vwMonitor;
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic) BOOL liveMode, DEMO;
@property (nonatomic, strong) IBOutlet UILabel *labelRate, *labelProfileId, *labelProfileName, *labelMsg;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *statusInfo, *btnDismiss, *btnRefresh;
@property (nonatomic) int startRecordingIndex, HR, newBornMode;
@property (nonatomic) BOOL stopTheTimer;
@property (nonatomic, strong) IBOutlet UILabel *lbDevice;

@property (nonatomic, strong) IBOutlet iCarousel *carousel;
@property (nonatomic, weak) IBOutlet SwipeView *swipeview;
@property (nonatomic, strong) IBOutlet UIImageView *postureimage;

@property (nonatomic, strong) IBOutlet CBAutoScrollLabel *autoScrollLabel;


@property(nonatomic, retain) NSTimer *timer;
@property(weak) NSTimer *repeatingTimer;


- (void)popDemoDataAndPushToLeads1:(NSInteger [])in_pt :(int)numberoflead;
- (void) VitalParam;
-(void)AccDisonncet;
-(void)AccConnect;



@end
