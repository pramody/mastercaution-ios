//
//  StatisticsViewController.h
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 01/07/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//


#import "PNChart.h"

@interface StatisticsViewController : UIViewController<PNChartDelegate>
{
    UIColor *blueColorMainAppUIColor;
    UIColor *blueBorderColorForTab;
    int arrthmiacnt;
    int ischemiacnt;
    int othercnt;
    
    //other
    int tempcnt;
    int respcnt;
    int apneacnt;
    int nomotioncnt;
    int othermanualcnt;
    int patientcallcnt;
    int ondemandcnt;
    
    //arthymia
    int supraventcnt;
    int ventricnt;
    int bradcnt;
    int pausecnt;
    int pvccnt;
    int asvstolecnt;
    int bigemenvcnt;
    int trigemnycnt;
    int coupletcnt;
    int tripletcnt;
    
    //ischemia
    int STelcnt;
    int STdecnt;
    int Prolcnt;
    int qrscnt;
    int twavecnt;
    
    
    NSMutableArray * hearratevalue;
    NSMutableArray *temperaturevalue;
    NSMutableArray *respirationvalue;
    NSMutableArray * event_datetime;
    
    

}
@property (nonatomic, strong) IBOutlet UIView* headerBlueView;
@property (nonatomic, strong) IBOutlet UIView * tabHeaderMainView;
@property (nonatomic, strong) IBOutlet UIView *tabDivider1;
@property (nonatomic, strong) IBOutlet UIView *tabDivider2;
@property (nonatomic, strong) IBOutlet UIView *tabDivider3;
@property (nonatomic, strong) IBOutlet UIButton *arrhythmia;
@property (nonatomic, strong) IBOutlet UIButton *ischemia;
@property (nonatomic, strong) IBOutlet UIButton *otherevent;
@property (nonatomic, strong) IBOutlet UIButton *total;
@property (nonatomic, strong) IBOutlet PNPieChart *pieChart;
@property (nonatomic, strong) IBOutlet UIView * legend;

@property (nonatomic, strong) IBOutlet UILabel * maxhrvalue;
@property (nonatomic, strong) IBOutlet UILabel * minhrvalue;
@property (nonatomic, strong) IBOutlet UILabel * maxtempvalue;
@property (nonatomic, strong) IBOutlet UILabel * mintempvalue;
@property (nonatomic, strong) IBOutlet UILabel * maxrespvalue;
@property (nonatomic, strong) IBOutlet UILabel * minrespvalue;


@property (nonatomic, strong) IBOutlet UILabel * hrmaxdate;
@property (nonatomic, strong) IBOutlet UILabel * hrmindate;
@property (nonatomic, strong) IBOutlet UILabel * tempmaxdate;
@property (nonatomic, strong) IBOutlet UILabel * tempmindate;
@property (nonatomic, strong) IBOutlet UILabel * respmaxdate;
@property (nonatomic, strong) IBOutlet UILabel * respmindate;

@property (nonatomic, strong) IBOutlet UILabel * tempmaxunit;
@property (nonatomic, strong) IBOutlet UILabel * tempminunit;

-(IBAction)btnarrythmi:(id)sender;
-(IBAction)btnischmia:(id)sender;
-(IBAction)btnotherevent:(id)sender;
-(IBAction)btntotal:(id)sender;




@end
