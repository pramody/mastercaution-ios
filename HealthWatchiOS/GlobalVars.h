//
//  GlobalVars.h
//  HealthWatch
//
//  Created by METSL MAC MINI on 04/11/15.
//  Copyright © 2015 METSL MAC MINI. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UploadService.h"
@interface GlobalVars : NSObject
{
   
      NSMutableArray *_plotdata;
       NSMutableArray *_Eventplotdata;
     NSMutableArray *_ecgarray;
    //vitalparameter
      NSInteger  _mcd_HeartRate;
      NSInteger  _mcd_RespirationRate;
      NSInteger  _mcd_Temperature;
      NSInteger  _mcd_Posture;
      NSInteger  _mcd_MCD_LastEvent ;
      NSInteger  _mcd_leads_off;
      NSInteger  _mcd_QRS;
      NSInteger  _mcd_QT;
      NSInteger  _mcd_QTc;
      NSInteger  _mcd_PR;
      NSInteger  _mcd_RR;
      NSInteger  _mcd_DataBuf;
      NSInteger  _mcd_ERAM;
      NSInteger  _mcd_ASIC;
      NSInteger  _mcd_SDCARD_Presence;
      NSInteger  _mcd_SDCARD_ERR;
      NSInteger  _mcd_BatteryStatus;
    //location
      NSString *_latitude,*_longititude;
      NSString * _mcd_DeviceId;
      NSString * _mcd_firmware_version;
      NSString * _mcd_last_updatd;
      NSString *  _mcd_garmentstatus;
      NSString *  _mcd_bluetoothstatus;
      NSString *  _mcd_cloudstatus;
      NSMutableArray *_Leaddata;
    
    
     NSString * _built_accelerometer;
     NSString * _built_electrodes ;
     NSString * _built_battery ;
     NSString * _built_bluetooth ;
     NSString * _built_temperature ;
     NSString * _built_sdcard ;
    
    
     NSString * _mcdmacaddress;
    
      NSInteger  _ChangeLevel;
    NSString *_mcd_sett_fiftyhz,*_mcd_sett_sixthz,*_mcd_sett_gain,*_mcd_sett_lead,*_mcd_sett_lead_config,*_mcd_sett_sampling_rate,*_mcd_sett_pretime,*_mcd_sett_postime,*_mcd_sett_mute,*_mcd_sett_respiration,*_mcd_sett_inverted_t_wave,*_mcd_sett_orientation,*_mcd_sett_baseline;
    NSString *_suspsett_time,*_suspsett_ecodevalue;
    
    NSString *_Thres_DD,*_Thres_YD,*_Thres_RD,*_Thres_flagg,*_Thres_ddflagg,*_Thres_posturecode,*_Thres_SetCode;
    NSMutableArray *_ThresMultiplePosturesArray ;
    
    NSString * _Email_EventCode ;
    NSString * _Email_EventDatetime;
    
    
    NSUInteger  _indexpathsection;
    NSUInteger  _indexpathrow;
    
    
    NSInteger  _BluetoothConnectionflag;
    NSInteger  _Internetflag;
     NSInteger  _MCDSyncflag;
   enum CurrentSetting _currentSetting;
    
    
    BOOL _dropdownValidFlag;
    BOOL _LiveEcgFlag;
    BOOL _suspenseflag;
    BOOL _configconnectFlag;
    BOOL _XMPPflag_on_off;
    
    uint8_t _data2[10000];
    int _writeptr;
    int _difptr1;
    int _readpnter1;
    
    
    UploadService * _WCFService;
    UploadService * _bluetoothService;
    
}

+ (GlobalVars *)sharedInstance;


@property (nonatomic, assign, readwrite) enum CurrentSetting currentSetting;
@property (nonatomic)  UploadService * WCFService;
@property (nonatomic)  UploadService * bluetoothService;

@property(strong, nonatomic, readwrite) NSMutableArray *plotdata;
@property(strong, nonatomic, readwrite) NSMutableArray *Eventplotdata;
@property(strong, nonatomic, readwrite) NSMutableArray *Leaddata;
@property( nonatomic, readwrite) NSInteger  mcd_RespirationRate;
@property( nonatomic, readwrite) NSInteger  mcd_Temperature;
@property( nonatomic, readwrite) NSInteger  mcd_Posture;
@property( nonatomic, readwrite) NSInteger  mcd_MCD_LastEvent;
@property( nonatomic, readwrite) NSInteger  mcd_leads_off;
@property( nonatomic, readwrite) NSInteger  mcd_QRS;
@property( nonatomic, readwrite) NSInteger  mcd_QT;
@property( nonatomic, readwrite) NSInteger  mcd_QTc;
@property( nonatomic, readwrite) NSInteger  mcd_PR;
@property( nonatomic, readwrite) NSInteger  mcd_RR;
@property( nonatomic, readwrite) NSInteger  mcd_ERAM;
@property( nonatomic, readwrite) NSInteger  mcd_DataBuf;
@property( nonatomic, readwrite) NSInteger  mcd_ASIC;
@property( nonatomic, readwrite) NSInteger  mcd_SDCARD_Presence;
@property( nonatomic, readwrite) NSInteger  mcd_SDCARD_ERR;
@property( nonatomic, readwrite) NSInteger  mcd_HeartRate;
@property( nonatomic, readwrite) NSInteger  mcd_BatteryStatus;
@property( nonatomic, readwrite) NSInteger  ChangeLevel;


@property( nonatomic, readwrite) NSUInteger  indexpathsection;
@property( nonatomic, readwrite) NSUInteger  indexpathrow;

@property(strong, nonatomic, readwrite) NSString *latitude;
@property(strong, nonatomic, readwrite) NSString *longititude;
@property(strong, nonatomic, readwrite) NSString *mcd_DeviceId;
@property(strong, nonatomic, readwrite) NSString *mcd_firmware_version;
@property(strong, nonatomic, readwrite) NSString *mcd_last_updatd;
@property(strong, nonatomic, readwrite) NSString *mcd_garmentstatus;
@property(strong, nonatomic, readwrite) NSString *mcd_bluetoothstatus;
@property(strong, nonatomic, readwrite) NSString *mcd_cloudstatus;


@property(strong, nonatomic, readwrite)NSString * built_accelerometer;
@property(strong, nonatomic, readwrite)NSString * built_electrodes ;
@property(strong, nonatomic, readwrite)NSString * built_battery ;
@property(strong, nonatomic, readwrite)NSString * built_bluetooth ;
@property(strong, nonatomic, readwrite)NSString * built_temperature ;
@property(strong, nonatomic, readwrite)NSString * built_sdcard ;

@property(strong, nonatomic, readwrite)NSString * Email_EventCode ;
@property(strong, nonatomic, readwrite)NSString * Email_EventDatetime ;
@property(strong, nonatomic, readwrite) NSMutableArray *ecgarray;


@property(strong, nonatomic, readwrite)NSString * mcdmacaddress;


//mcdsettingpage
@property(strong, nonatomic, readwrite)NSString *mcd_sett_fiftyhz,*mcd_sett_sixthz,*mcd_sett_gain,*mcd_sett_lead,*mcd_sett_lead_config,*mcd_sett_sampling_rate,*mcd_sett_pretime,*mcd_sett_postime,*mcd_sett_mute,*mcd_sett_respiration,*mcd_sett_inverted_t_wave,*mcd_sett_orientation,*mcd_sett_baseline;


//suspension page
@property(strong, nonatomic, readwrite)NSString *suspsett_time,*suspsett_ecodevalue;


//threshold page
@property(strong, nonatomic, readwrite) NSString *Thres_DD,*Thres_YD,*Thres_RD,*Thres_flagg,*Thres_ddflagg,*Thres_posturecode,*Thres_SetCode;
@property(strong, nonatomic, readwrite) NSMutableArray *ThresMultiplePosturesArray;


@property( nonatomic, readwrite) NSInteger BluetoothConnectionflag;
@property( nonatomic, readwrite) NSInteger  Internetflag;
@property( nonatomic, readwrite) NSInteger  MCDSyncflag;

@property( nonatomic, readwrite) BOOL dropdownValidFlag ;
@property( nonatomic, readwrite) BOOL LiveEcgFlag ;
@property( nonatomic, readwrite) BOOL configconnectFlag;
@property( nonatomic, readwrite) BOOL suspenseflag;
@property( nonatomic, readwrite) BOOL XMPPflag_on_off;

@property( nonatomic, readwrite) int  writeptr;
@property( nonatomic, readwrite) int  difptr1;
@property( nonatomic, readwrite) int  readpnter1;

@end
