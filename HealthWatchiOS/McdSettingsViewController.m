//
//  McdSettingsViewController.m
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 19/06/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import "McdSettingsViewController.h"

#import "McdSettingsTableViewCell.h"

#import "SDCAlertView.h"
#import "SDCAlertViewController.h"
#import <UIView+SDCAutoLayout.h>
#import <AudioToolbox/AudioToolbox.h>
#import "MCDSettings.h"
#import "DoctorProfileViewController.h"


@interface McdSettingsViewController ()

@end

@implementation McdSettingsViewController
@synthesize headerBlueView,tableViewMcdSettingsCell,tableViewMcdSettingsTable,preTimeGrayView,preTimeStepperText,preTimeStepperControl,postTimeGrayView,postTimeStepperControl,postTimeStepperText,notchhFilterStatusPickerViewControl,mcdGainPickerViewControl,mcdLeadDetectionSettingPickerViewControl,mcgLeadSettingPickerViewControl,mcgSampleRatePickerViewControl,spinner,lockunlocButton;
bool preTimeValidFlag =true;
bool postTimeValidFlag=true;
bool updateprepostflag=false;

int direction;
int shakes;

NSMutableArray* listingItemsDataMain;
NSMutableArray* cellRowStatus  ;



- (void)viewDidLoad {
    [super viewDidLoad];
    AppDelegate * appDelegate = ( AppDelegate *)UIApplication.sharedApplication.delegate ;
    appDelegate.orientationKey = @"PRO";
    [appDelegate forceOrientationTakePlace:appDelegate.orientationKey];
    self.slidingViewController.topViewAnchoredGesture = ECSlidingViewControllerAnchoredGestureTapping | ECSlidingViewControllerAnchoredGesturePanning;
    self.slidingViewController.customAnchoredGestures =@[];
    
   
     tableBgGrayColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"clinical_tbl_header_bg_gray.png"]];
   filterStatusArray   =  @[@"OFF", @"50", @"60"];
     mcdGainArray                    = @[@"0.5", @"1", @"1.5", @"2"];
    mcgLeadSettingArray             =  @[@"3",@"5", @"12", @"15"];
    mcdEventDetectionLeadArray      =   @[@"II", @"V2", @"V5"];
    mcgSamplingRatesArray           =  @[@"125", @"250", @"500"];
    mcdMuteArray   =  @[@"ON",@"OFF"];
    mcdOrientationArray   =  @[@"Horizontal",@"Vertical"];
    mcdRespirationArray   =  @[@"OFF", @"ON"];
    mcdInvertedTwave= @[@"OFF", @"ON"];
    mcdbaselineArray= @[@"OFF", @"ON"];
    
      blueColorMainAppUIColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"blue_bg_full.png"]];
    headerBlueView.backgroundColor = blueColorMainAppUIColor;
    listingItemsDataMain=[[NSMutableArray alloc] init];
    cellRowStatus=[[NSMutableArray alloc]init];
   
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(DoctorLoginStatus:) name:RWT_DOCTOR_LOGIN_NOTIFICATION object:nil];
    
}


-(void)viewWillAppear:(BOOL)animated
{
     [self initializePickerView];
    [self prepareDataForTabelView];
    [self fetchingpatientdata];
     GlobalVars *global = [GlobalVars sharedInstance];
    if(global.dropdownValidFlag)
      lockunlocButton.selected=true;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)goToDashboard:(id)sender{
    if(!updateprepostflag)
    {
    self.slidingViewController.topViewController.view.layer.transform = CATransform3DMakeScale(1, 1, 1);
    self.slidingViewController.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"DashboardStoryboardId"];
    }
}

-(IBAction)leftSideButtonAction:(id)sender{
    if(!updateprepostflag)
    {
    [self.slidingViewController anchorTopViewToRightAnimated:YES];
    }
}

-(void)initializePickerView
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    standalone_Mode = [defaults boolForKey:@"ActiveStandalonemode"];
    
    notchhFilterStatusPickerViewControl         =  [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 300, 35)];
    mcdGainPickerViewControl                    =  [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 300, 35)];
    mcgLeadSettingPickerViewControl             =   [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 300, 35)];
    mcdLeadDetectionSettingPickerViewControl    =   [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 300, 35)];
    mcgSampleRatePickerViewControl              =   [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 300, 35)];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"MCDSettings" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSString *fiftyhz1,*sixthz1;
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        NSLogger *logger=[[NSLogger alloc]init];
        logger.degbugger = true;
        
        [logger log:@"Coredata Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:[error localizedDescription], @"Mcd Setting page", nil] error:TRUE];
        
    } else {
       // NSLog(@"%@", result);
        if (result.count > 0) {
            NSManagedObject *mcdsett = (NSManagedObject *)[result objectAtIndex:0];
           // NSLog(@"1 - %@", mcdsett);
            ser_pretime=[mcdsett valueForKey:@"timeinterval_pre"];
            ser_postime=[mcdsett valueForKey:@"timeinterval_post"];
            updatepretext=ser_pretime;
            updateposttext=ser_postime;
            fiftyhz1=[mcdsett valueForKey:@"fiftyhz_filter"];
            sixthz1=[mcdsett valueForKey:@"sixtyhz_filter"];
            
            if([fiftyhz1 isEqualToString:@"0"]&&[sixthz1 isEqualToString:@"0"])
            {
                ser_notchfil=@"0";
            }
            else
            {
                if([fiftyhz1 isEqualToString:@"1"])
                {
                     ser_notchfil=@"1";
                }
                else if ([sixthz1 isEqualToString:@"1"])
                {
                     ser_notchfil=@"2";
                }
                else
                {
                    ser_notchfil=@"0";
                }
            }
            ser_gain=[mcdsett valueForKey:@"gain"];
            ser_lead=[mcdsett valueForKey:@"leads"];
            ser_leadconfig=[mcdsett valueForKey:@"lead_config"];
            ser_samplingrte=[mcdsett valueForKey:@"sampling_rate"];
            ser_mute=[mcdsett valueForKey:@"volume_level"];
            ser_respiration=[mcdsett valueForKey:@"respiration"];
            ser_inverted_T_wave=[mcdsett valueForKey:@"inverted_t_wave"];
            ser_orientation=[mcdsett valueForKey:@"orientation"];
            ser_baseline=[mcdsett valueForKey:@"base_line_fil"];
            
        }
    }
    
}

-(void)fetchingpatientdata
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
  
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"PatientDetails" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
       // NSLog(@"%@", result);
        if (result.count > 0) {
            NSManagedObject *patient = (NSManagedObject *)[result objectAtIndex:0];
           // NSLog(@"1 - %@", patient);
            patientid=[patient valueForKey:@"patient_id"];
            deviceid=[patient valueForKey:@"device_id"];
            doctorname=[patient valueForKey:@"doc_name"];
        }
    }
}

- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}



-(void) setMcdSettingsUiElementsForPreTime
{
    /* Pre and Post time picker respectively */
    
    
    preTimeGrayView                         =   [[UIView alloc] initWithFrame:CGRectMake(5, 20, 290, 45)];
    UIColor *lightGrayColor;
    preTimeGrayView.backgroundColor         =    [lightGrayColor colorWithAlphaComponent:0.3];
    
    
}

-(void) setMcdSettingsUiElementsForPostTime
{
    /* Pre and Post time picker respectively */
    
    
    postTimeGrayView                         =   [[UIView alloc] initWithFrame:CGRectMake(5, 95, 290, 45)];
    UIColor *lightGrayColor;
    postTimeGrayView.backgroundColor         =   [lightGrayColor colorWithAlphaComponent:0.3];
    //postTimeGrayView.alpha                 =   0.1
    
    
   
    
}

-(void) setPreTimeToText:(id)unknownTypeParameter
{
    // set Default Minimu  time
    
    NSString * result=[NSString stringWithFormat:@"%@",unknownTypeParameter];
    preTimeStepperText.text  = result;
   
    
    
    /*        let preValue : NSString     = "\(Int(value as NSNumber))"
     preTimeStepperText.text     = preValue*/
}

-(void)setPostTimeToText :(id)unknownTypeParameter
{
    // set Default Minimu  time
     NSString * result=[NSString stringWithFormat:@"%@",unknownTypeParameter];
    postTimeStepperText.text = result;
    
}


-(IBAction)stepperChangeValueAction:(UIStepper *)sender
{
    NSString* keyValue;
  
    //println("Value change \(Int(sender.value).description)")
    if(sender.tag == 100)
    {
          updateprepostflag=true;
        keyValue = @"pre_value";
        
        NSString *preValue =[NSString stringWithFormat:@"%.0f", sender.value];
        updatepretext=preValue;
        //println("\(preValue)")
        
        [[[[listingItemsDataMain objectAtIndex:1]objectForKey:@"rows"]objectAtIndex:0]setValue:preValue forKey:@"pre_value"];
        
        [self.tableViewMcdSettingsTable reloadData];
        
        [self setPreTimeToText:preValue];
        
    }
    else if(sender.tag == 200)
    {
          updateprepostflag=true;
        keyValue = @"post_value";
         NSString *postValue =[NSString stringWithFormat:@"%.0f", sender.value];
         updateposttext=postValue;
            //println("\(postValue)")
        [[[[listingItemsDataMain objectAtIndex:1]objectForKey:@"rows"]objectAtIndex:0]setValue:postValue forKey:@"post_value"];
        
        [self.tableViewMcdSettingsTable reloadData];
        [self setPostTimeToText:postValue];
    }
 
}

-(void)ContinousTextChangeEvent:(UITextField *)textField
{
     updateprepostflag=true;
}


-(void)customTextChangeEvent:(UITextField *)textField
{
    
    if(textField == preTimeStepperText)
    {
        
        //message     =   "Pre-post time is invalid, it should between 10 to 300"
        NSString * inIntValue  = textField.text;
        
        if (inIntValue.intValue >= 10 && inIntValue.intValue <= 300)
        {
            updateprepostflag=true;
            updatepretext=inIntValue;
            //preAndPostTimeValidFlag = true;
            //println("Value set for pre done \(inIntValue)")
            double preTimeChanged  = [textField.text doubleValue];
            preTimeStepperControl.value = preTimeChanged;
            
            
            
            [[[[listingItemsDataMain objectAtIndex:1]objectForKey:@"rows"]objectAtIndex:0]setValue:inIntValue forKey:@"pre_value"];
            
            [self.tableViewMcdSettingsTable reloadData];
            // store value in array in respective key for pre Time
        }
        else
        {
            if(inIntValue.intValue > 300)
            {
                updateprepostflag=false;
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Message"
                                                                    message:@"Pre time should be less than 300"
                                                                   delegate:nil
                                                          cancelButtonTitle:@"Ok"
                                                          otherButtonTitles:nil];
                
                [alertView show];
                
                postTimeStepperText.text = [inIntValue substringToIndex:inIntValue.length - 1];
            }
            if(inIntValue.intValue < 10)
            {
                updateprepostflag=false;
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Message"
                                                                    message:@"Pre time should be more than 10"
                                                                   delegate:nil
                                                          cancelButtonTitle:@"Ok"
                                                          otherButtonTitles:nil];
                
                [alertView show];
                
            }

        }
        
        
    }
    
    if(textField == postTimeStepperText)
    {
        
        NSString * inIntValue = textField.text ;
        if (inIntValue.intValue >= 10 && inIntValue.intValue <= 300)
        {
            updateprepostflag=true;
            updateposttext=inIntValue;
            //preAndPostTimeValidFlag = true;
            //println("Value set for post done \(inIntValue)")
            double postTimeChanged  = [textField.text doubleValue];
            postTimeStepperControl.value = postTimeChanged;
            
            //println("\(postValue)")
            [[[[listingItemsDataMain objectAtIndex:1]objectForKey:@"rows"]objectAtIndex:0]setValue:inIntValue forKey:@"post_value"];
            
            [self.tableViewMcdSettingsTable reloadData];
           
        }
        else
        {
            if(inIntValue.intValue > 300)
            {
                updateprepostflag=false;
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Message"
                                                                message:@"Post time should be less than 300"
                                                               delegate:nil
                                                      cancelButtonTitle:@"Ok"
                                                      otherButtonTitles:nil];
            
            [alertView show];
            
            postTimeStepperText.text = [inIntValue substringToIndex:inIntValue.length - 1];
            }
            if(inIntValue.intValue < 10)
            {
                updateprepostflag=false;
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Message"
                                                                    message:@"Post time should be more than 10"
                                                                   delegate:nil
                                                          cancelButtonTitle:@"Ok"
                                                          otherButtonTitles:nil];
                
                [alertView show];
            
            }

        }
       /* else if(inIntValue.length > 3)
        {
            postTimeStepperText.text = [inIntValue substringToIndex:inIntValue.length - 1];
        }*/
    }
    
    
}

-(IBAction)lockUnlockAction:(UIButton *)sender {
    GlobalVars *global = [GlobalVars sharedInstance];
    if (sender.selected == false)
    {
        
        // --- : Before Unlocking the setting lock, Doctor authentication is mandatory in case of [MCM is conneted with MCC] and bypass [Doctor authentication] in case of stand alone mode
        if(global.BluetoothConnectionflag==2)
        {

        [self presentAlertViewForPassword];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                            message:@"Please Connect MCD Device"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }
    else
    {
        global.dropdownValidFlag=false;
        lockunlocButton.selected=false;
        [self DoctorUniversalLoginTimer:NO];
    }
    
    
}

-(void) prepareDataForTabelView
{
    
    // 1st Section Mute
    NSMutableDictionary *MuteTimeMuDic = [[NSMutableDictionary alloc] init];
    [MuteTimeMuDic setObject:@"MCD Buzzer Settings" forKey:@"title"];
    [MuteTimeMuDic setObject:@"time_interval.png" forKey:@"image_name"];
    
    
    NSMutableDictionary *MuteRowOne = [[NSMutableDictionary alloc] init];
    [MuteRowOne setObject:@"Mute" forKey:@"title"];
    [MuteRowOne setObject:[mcdMuteArray objectAtIndex:[ser_mute intValue]] forKey:@"value"];
    [MuteRowOne setObject:mcdMuteArray forKey:@"list"];
    
    
    NSMutableArray *MuteAllRows = [NSMutableArray arrayWithObjects:MuteRowOne, nil];
    
    [MuteTimeMuDic setObject:MuteAllRows forKey:@"rows"];
    
    
    // 2nd Section TIME INTERVAS
    NSMutableDictionary *prePostTimeMuDic = [[NSMutableDictionary alloc] init];
    [prePostTimeMuDic setObject:@"Event Recording Time" forKey:@"title"];
    [prePostTimeMuDic setObject:@"time_interval.png" forKey:@"image_name"];
   
    
    NSMutableDictionary *perPostRowOne = [[NSMutableDictionary alloc] init];
    [perPostRowOne setObject:@"Pre-time / Post-time interval (seconds)" forKey:@"title"];
    [perPostRowOne setObject:ser_pretime forKey:@"pre_value"];
    [perPostRowOne setObject:ser_postime forKey:@"post_value"];
    
    NSMutableArray *perPostAllRows = [NSMutableArray arrayWithObjects:perPostRowOne, nil];
    
    [prePostTimeMuDic setObject:perPostAllRows forKey:@"rows"];
    
    
    // 2st Section FILTER STATUS
    
    
    NSMutableDictionary *filterStatusMuDic = [[NSMutableDictionary alloc] init];
    [filterStatusMuDic setObject:@"Filter Status" forKey:@"title"];
    [filterStatusMuDic setObject:@"filter_status.png" forKey:@"image_name"];
    
    NSMutableDictionary *filterStatusRowOne = [[NSMutableDictionary alloc] init];
    [filterStatusRowOne setObject:@"Notch Filter Status" forKey:@"title"];
    [filterStatusRowOne setObject:[filterStatusArray objectAtIndex:[ser_notchfil intValue]] forKey:@"value"];
    [filterStatusRowOne setObject:filterStatusArray forKey:@"list"];
    
   /* NSMutableDictionary *filterStatusRowTwo = [[NSMutableDictionary alloc] init];
    [filterStatusRowTwo setObject:@"BaseLine Wander Filter" forKey:@"title"];
    [filterStatusRowTwo setObject:[mcdbaselineArray objectAtIndex:[ser_baseline intValue]] forKey:@"value"];
    [filterStatusRowTwo setObject:mcdbaselineArray forKey:@"list"];*/
    
    
    NSMutableArray *filterStatusAllRows = [NSMutableArray arrayWithObjects:filterStatusRowOne,nil];
    
    [filterStatusMuDic setObject:filterStatusAllRows forKey:@"rows"];

    
    // 2st Section FILTER STATUS
    
    NSMutableDictionary *ecgSettingsMuDic = [[NSMutableDictionary alloc] init];
    [ecgSettingsMuDic setObject:@"ECG Configuration Settings" forKey:@"title"];
    [ecgSettingsMuDic setObject:@"ecg_settings.png" forKey:@"image_name"];
    
    NSMutableDictionary *ecgSettingsMuDicRowOne = [[NSMutableDictionary alloc] init];
    [ecgSettingsMuDicRowOne setObject:@"MCD Gain" forKey:@"title"];
    [ecgSettingsMuDicRowOne setObject:[mcdGainArray objectAtIndex:[ser_gain intValue]] forKey:@"value"];
    [ecgSettingsMuDicRowOne setObject:mcdGainArray forKey:@"list"];

    NSMutableDictionary *ecgSettingsMuDicRowTwo = [[NSMutableDictionary alloc] init];
    [ecgSettingsMuDicRowTwo setObject:@"MCD Lead" forKey:@"title"];
    [ecgSettingsMuDicRowTwo setObject:[mcgLeadSettingArray objectAtIndex:[ser_leadconfig intValue]] forKey:@"value"];
    [ecgSettingsMuDicRowTwo setObject:mcgLeadSettingArray forKey:@"list"];

    NSMutableDictionary *ecgSettingsMuDicRowThree = [[NSMutableDictionary alloc] init];
    [ecgSettingsMuDicRowThree setObject:@"MCD Event detection Lead" forKey:@"title"];
    [ecgSettingsMuDicRowThree setObject:[mcdEventDetectionLeadArray objectAtIndex:[ser_lead intValue]] forKey:@"value"];
    [ecgSettingsMuDicRowThree setObject:mcdEventDetectionLeadArray forKey:@"list"];

    NSMutableDictionary *ecgSettingsMuDicRowFour = [[NSMutableDictionary alloc] init];
    [ecgSettingsMuDicRowFour setObject:@"MCD Sampling Rate (samples/second)" forKey:@"title"];
    [ecgSettingsMuDicRowFour setObject:[mcgSamplingRatesArray objectAtIndex:[ser_samplingrte intValue]] forKey:@"value"];
    [ecgSettingsMuDicRowFour setObject:mcgSamplingRatesArray forKey:@"list"];
    
    NSMutableArray *ecgSettingsMuDicAllRows = [NSMutableArray arrayWithObjects:ecgSettingsMuDicRowOne,ecgSettingsMuDicRowTwo,ecgSettingsMuDicRowThree,ecgSettingsMuDicRowFour, nil];
    
    [ecgSettingsMuDic setObject:ecgSettingsMuDicAllRows forKey:@"rows"];
    
    // 3rd Section respiration
    NSMutableDictionary *RespirationTimeMuDic = [[NSMutableDictionary alloc] init];
    [RespirationTimeMuDic setObject:@"Respiration Settings" forKey:@"title"];
    [RespirationTimeMuDic setObject:@"time_interval.png" forKey:@"image_name"];
    
    
    NSMutableDictionary *RespirationRowOne = [[NSMutableDictionary alloc] init];
    [RespirationRowOne setObject:@"Respiration" forKey:@"title"];
    [RespirationRowOne setObject:[mcdRespirationArray objectAtIndex:[ser_respiration intValue]] forKey:@"value"];
    [RespirationRowOne setObject:mcdRespirationArray forKey:@"list"];
    
    
    NSMutableArray *RespirationAllRows = [NSMutableArray arrayWithObjects:RespirationRowOne, nil];
    
    [RespirationTimeMuDic setObject:RespirationAllRows forKey:@"rows"];
    
    // 4th Section respiration
    NSMutableDictionary *InvertedTimeMuDic = [[NSMutableDictionary alloc] init];
    [InvertedTimeMuDic setObject:@"Inverted T Settings" forKey:@"title"];
    [InvertedTimeMuDic setObject:@"time_interval.png" forKey:@"image_name"];
    
    
    NSMutableDictionary *InvertedRowOne = [[NSMutableDictionary alloc] init];
    [InvertedRowOne setObject:@"Inverted T Detection" forKey:@"title"];
    [InvertedRowOne setObject:[mcdInvertedTwave objectAtIndex:[ser_inverted_T_wave intValue]] forKey:@"value"];
    [InvertedRowOne setObject:mcdInvertedTwave forKey:@"list"];
    
    
    NSMutableArray *InvertedAllRows = [NSMutableArray arrayWithObjects:InvertedRowOne, nil];
    
    [InvertedTimeMuDic setObject:InvertedAllRows forKey:@"rows"];
    
    
    // 5th Section Orientation
    NSMutableDictionary *OreintationTimeMuDic = [[NSMutableDictionary alloc] init];
    [OreintationTimeMuDic setObject:@"Orientation Settings" forKey:@"title"];
    [OreintationTimeMuDic setObject:@"time_interval.png" forKey:@"image_name"];
    
    
    NSMutableDictionary *OrientationRowOne = [[NSMutableDictionary alloc] init];
    [OrientationRowOne setObject:@"Orientation" forKey:@"title"];
    [OrientationRowOne setObject:[mcdOrientationArray objectAtIndex:[ser_orientation intValue]] forKey:@"value"];
    [OrientationRowOne setObject:mcdOrientationArray forKey:@"list"];
    
    
    NSMutableArray *OrientationAllRows = [NSMutableArray arrayWithObjects:OrientationRowOne, nil];
    
    [OreintationTimeMuDic setObject:OrientationAllRows forKey:@"rows"];

    
  
    
      //NSMutableArray *listingItemsDataMain = [[NSMutableArray alloc] init];
    [listingItemsDataMain addObject:MuteTimeMuDic];
    [listingItemsDataMain addObject:prePostTimeMuDic];
    [listingItemsDataMain addObject:filterStatusMuDic];
    [listingItemsDataMain addObject:ecgSettingsMuDic];
    [listingItemsDataMain addObject:RespirationTimeMuDic];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"PatientDetails" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
   
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        if (result.count > 0) {
            NSManagedObject *patient = (NSManagedObject *)[result objectAtIndex:0];
            // NSLog(@"1 - %@", patient);
            
            
            if([[patient valueForKey:@"level"] isEqualToString:@"Level1"]||[[patient valueForKey:@"level"] isEqualToString:@"Level2"])
            {
                //userlevel=@"SAFE GUARD MODE";
            }
            else
            {
                [listingItemsDataMain addObject:InvertedTimeMuDic];
            }
            
        }
    }
   

    [listingItemsDataMain addObject:OreintationTimeMuDic];
    
    
    [self prepareRowStatus];
    
}

-(void) prepareRowStatus
{
    if (listingItemsDataMain.count > 0)
    {
        if (cellRowStatus == nil) {
           // cellRowStatus = NSMutableArray()
        } else {
            [cellRowStatus removeAllObjects];
        }
        NSMutableArray *rowsCount;
        int countSection = 0;
        for(NSDictionary * eachObj in listingItemsDataMain)
        {
           // NSLog(@"eachObj %@",eachObj);
            if([[eachObj objectForKey:@"rows" ] count] > 0)
            {
                 rowsCount = [[NSMutableArray alloc] init];
                for(NSString* eachChildObj in [eachObj objectForKey:@"rows"])
                {
                    [rowsCount addObject:[NSNumber numberWithInt: 0]];
                }
                
            }
            [cellRowStatus insertObject:rowsCount atIndex:countSection];
            countSection++;
        }
    }
    
   // NSLog(@"Current Data Row Status %@",cellRowStatus);
}




-(void) hideKeyboard
{
    [self.view endEditing:true];
}







// MARK: - UITableView Delegates and DataSource


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return listingItemsDataMain.count;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    int noOfRow = (int)[[[listingItemsDataMain objectAtIndex:section]valueForKey:@"rows"]count];
    return noOfRow;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
   
 
   if([cellRowStatus[indexPath.section][indexPath.row]intValue] == 1)
    {
         return 257;
    }
    
    return 64;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 26;
}





- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView * headerView =[[UIView alloc] initWithFrame:CGRectMake(10, 0, 300, 26)];
    headerView.backgroundColor = blueColorMainAppUIColor;
    
    NSString * iconName = [listingItemsDataMain[section] objectForKey:@"image_name"];
    NSString * titleName = [listingItemsDataMain[section] objectForKey:@"title"];
    
    UIImageView * headerIcon  =  [[UIImageView alloc] initWithFrame:CGRectMake(6, 6, 14, 14)];
    headerIcon.image                =  [UIImage imageNamed:iconName];
    [headerView addSubview:headerIcon];
    
    
    UILabel * headerLbl  =   [[UILabel alloc] initWithFrame:CGRectMake(30,7, 280, 15)];
    headerLbl.backgroundColor   =   [UIColor clearColor];
    headerLbl.text              =   titleName; //(allClinicalDataArray[section]["date"] as NSString)
    headerLbl.font              = [UIFont fontWithName:@"Roboto-Medium" size:15]; //Roboto-Bold, Roboto-Condensed 15.0
    headerLbl.textColor         =   [UIColor whiteColor];
    [headerView addSubview:headerLbl];
    
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}



/*- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    //        var headerView : UIView = UIView(frame: CGRectMake(15, 15, 250, 30.6))
    //        headerView.backgroundColor = UIColor.yellowColor()
    
    
    return UIView;
}*/


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString * itemName, *itemValue,*cellIdentifier;
    // NEW ----
    if (indexPath.section == 1)
    {
        cellIdentifier = @"McdSettingsCellID1";
    }
    else
    {
        cellIdentifier = @"McdSettingsCellID2";
    }
    
    tableViewMcdSettingsCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(tableViewMcdSettingsCell == nil)
    {
        tableViewMcdSettingsCell = [[McdSettingsTableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
    }
    
    NSArray * allRowData  = [listingItemsDataMain[indexPath.section] objectForKey:@"rows"];
    if(allRowData.count > 0)
    {
        NSString * preValue;
        NSString * postValue;
        
        itemName        =   [[allRowData objectAtIndex:[indexPath row]] objectForKey:@"title"];
        if([cellIdentifier isEqualToString:@"McdSettingsCellID1"])
        {
            // For Mcd Setting Cell -- For First Row
           
            preValue    =    [[allRowData objectAtIndex:[indexPath row]]objectForKey:@"pre_value"];
            postValue   =    [[allRowData objectAtIndex:[indexPath row]]objectForKey:@"post_value"];
            
            itemValue       = [NSString stringWithFormat:@"%@ / %@",preValue,postValue];
        }
        else
        {
            // For Mcd Setting Cell -- For Rest Row
            
            itemValue       =   [[allRowData objectAtIndex:[indexPath row]]objectForKey:@"value"];
            
            if(indexPath.section == 2 && indexPath.row == 0)
            {
                itemValue = itemValue;
                if (![(itemValue.lowercaseString)  isEqual: @"off"])
                {
                    itemValue =[NSString stringWithFormat:@"%@ Hz",itemValue];
                }
            }
            else if( indexPath.section == 3 && indexPath.row == 1)
            {
                itemValue = [NSString stringWithFormat:@"%@ LEAD",itemValue];
            }
            else if( indexPath.section == 3 && indexPath.row == 2)
            {
                itemValue =[NSString stringWithFormat:@"LEAD %@",itemValue];
            }
        }
        
        // --- Assigning [Item Name, Item Value]
        tableViewMcdSettingsCell.rowSettingTitle.text  =   itemName;
        tableViewMcdSettingsCell.rowSettingValue.text   =   itemValue;
        
        // --- Expand View To Show and Add elements
        BOOL valueIs  =   true;
        CGFloat angleValue  =   6.14;
        if([cellRowStatus[indexPath.section][indexPath.row]intValue] == 1)
        {
            // if cell is detected and cell status is 1 = Expand that the height will be 257 else height will be 64
            valueIs     =   false;
            angleValue  =   7.85;
        }
        
        tableViewMcdSettingsCell.rightArrowBlackImg.transform = CGAffineTransformMakeRotation(angleValue);
        tableViewMcdSettingsCell.expandView.hidden = valueIs;
        
        
        
        // Below Code is for Expand View for Both Cell1 and Cell2, As per condition.
        
        if([cellIdentifier isEqualToString:@"McdSettingsCellID1"]) // For cell is 1
        {
            // Will do it later this part
            preTimeStepperText         =   tableViewMcdSettingsCell.preTimeStepperText;
            preTimeStepperControl      =   tableViewMcdSettingsCell.preTimeStepperControl;
            
            postTimeStepperText        =   tableViewMcdSettingsCell.postTimeStepperText;
            postTimeStepperControl     =   tableViewMcdSettingsCell.postTimeStepperControl;
            
            // Pretime
            
            preTimeStepperText.textAlignment        =  NSTextAlignmentCenter;
            preTimeStepperText.layer.borderWidth    =   1.0;
            preTimeStepperText.layer.borderColor    =   UIColor.lightGrayColor.CGColor;
            preTimeStepperText.keyboardType         =   UIKeyboardTypeNumberPad;
            preTimeStepperText.backgroundColor      =   UIColor.whiteColor;
            [preTimeStepperText addTarget:self action:@selector(customTextChangeEvent:) forControlEvents:UIControlEventEditingDidEnd];
           [preTimeStepperText addTarget:self action:@selector(ContinousTextChangeEvent:) forControlEvents:UIControlEventEditingChanged];
            preTimeStepperText.delegate             =   self;
            //preTimeStepperText.text                 =   preValue
            
            preTimeStepperControl.value             =   [preValue doubleValue];
            preTimeStepperControl.tag               =   100;
            preTimeStepperControl.tintColor         =   [UIColor whiteColor];
            preTimeStepperControl.wraps             =   true;
            preTimeStepperControl.minimumValue      =   10.0;
            preTimeStepperControl.maximumValue      =   300;
            preTimeStepperControl.continuous        =   true;
            preTimeStepperControl.layer.masksToBounds     =    true;
            [preTimeStepperControl addTarget:self  action:@selector(stepperChangeValueAction:) forControlEvents:UIControlEventValueChanged];
            preTimeStepperControl.layer.backgroundColor   =   blueColorMainAppUIColor.CGColor;
            preTimeStepperControl.layer.cornerRadius      =    4.0;
            
            /* Pre and Post time picker respectively */
            [self setPreTimeToText:preValue];
            
            // Posttime
            
            
            
            postTimeStepperText.textAlignment        =   NSTextAlignmentCenter;
            postTimeStepperText.layer.borderWidth    =   1.0;
            postTimeStepperText.layer.borderColor    =   UIColor.lightGrayColor.CGColor;
            postTimeStepperText.keyboardType         =  UIKeyboardTypeNumberPad;
            postTimeStepperText.backgroundColor      =   [UIColor whiteColor];
            [postTimeStepperText addTarget:self action:@selector(customTextChangeEvent:) forControlEvents:UIControlEventEditingDidEnd];
            [postTimeStepperText addTarget:self action:@selector(ContinousTextChangeEvent:) forControlEvents:UIControlEventEditingChanged];
            postTimeStepperText.delegate             =   self;
            //postTimeStepperText.text                 =   postValue
            
            
            postTimeStepperControl.value             =   [postValue doubleValue];
            postTimeStepperControl.tag               =   200;
            postTimeStepperControl.tintColor         =   [UIColor whiteColor];
            postTimeStepperControl.wraps             =   true;
            postTimeStepperControl.minimumValue      =   10.0;
            postTimeStepperControl.maximumValue      =   300;
            postTimeStepperControl.continuous        =   true;
            postTimeStepperControl.layer.masksToBounds   =    true;
            [postTimeStepperControl addTarget:self  action:@selector(stepperChangeValueAction:) forControlEvents:UIControlEventValueChanged];
            postTimeStepperControl.layer.backgroundColor   =   blueColorMainAppUIColor.CGColor;
            postTimeStepperControl.layer.cornerRadius      =    4.0;
            
            /* Pre and Post time picker respectively */
            [self setPostTimeToText:postValue];
            
            
        }
        else if([cellIdentifier isEqualToString:@"McdSettingsCellID2"]) // For cell is 2
        {
            // In these cells we have all uipicker as user will expand the view
          
            NSArray * pickerDataArray =  [[allRowData objectAtIndex:[indexPath row]]objectForKey:@"list"];
             itemValue       =   [[allRowData objectAtIndex:[indexPath row]]objectForKey:@"value"];
            
           // NSLog(@"inff%ld",(long)indexPath.row);
         
            
            
            tableViewMcdSettingsCell.oneForAllPickerViewControl.delegate        = self;
            //tableViewMcdSettingsCell.oneForAllPickerViewControl.dataSource  = self;
            tableViewMcdSettingsCell.oneForAllPickerViewControl.tableRow      =  (int)indexPath.row;
            tableViewMcdSettingsCell.oneForAllPickerViewControl.tableSection  =  (int)indexPath.section;
            
           
            tableViewMcdSettingsCell.oneForAllPickerViewControl.passNsArray   =  pickerDataArray;
            if(indexPath.section==0)
            {
                NSInteger index1=[pickerDataArray indexOfObject:itemValue];
                [tableViewMcdSettingsCell.oneForAllPickerViewControl selectRow:index1 inComponent:0 animated:false];
            }
            else if(indexPath.section==2)
            {    NSInteger index1=[pickerDataArray indexOfObject:itemValue];
                [tableViewMcdSettingsCell.oneForAllPickerViewControl selectRow:index1   inComponent:0 animated:false];
            }
            else if(indexPath.section==3)
            {
                NSInteger index1=[pickerDataArray indexOfObject:itemValue];
                [tableViewMcdSettingsCell.oneForAllPickerViewControl selectRow:index1  inComponent:0 animated:false];
            }
            else if(indexPath.section==4)
            {
                NSInteger index1=[pickerDataArray indexOfObject:itemValue];
                [tableViewMcdSettingsCell.oneForAllPickerViewControl selectRow:index1  inComponent:0 animated:false];
            }
            else if(indexPath.section==5)
            {
                NSInteger index1=[pickerDataArray indexOfObject:itemValue];
                [tableViewMcdSettingsCell.oneForAllPickerViewControl selectRow:index1  inComponent:0 animated:false];
            }
            else if(indexPath.section==6)
            {
                NSInteger index1=[pickerDataArray indexOfObject:itemValue];
                [tableViewMcdSettingsCell.oneForAllPickerViewControl selectRow:index1  inComponent:0 animated:false];
            }
            [tableViewMcdSettingsCell.oneForAllPickerViewControl reloadAllComponents];
            
           
            
        }
        
    
        
        
        
    } // End if Check the count of RowData
    
    
    // NEW ----
    
    
    
                                    tableViewMcdSettingsCell.clipsToBounds = true;
    
                                    return tableViewMcdSettingsCell;
}




- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    GlobalVars *global = [GlobalVars sharedInstance];
   
    // Either the cell can be "TableViewDetectedCellID" or "TableViewManualCellID"
    
    //println(" Detected tap ")
    indexpthsec=[indexPath section];
    indexrw=[indexPath row];
    
    
    if(preTimeValidFlag == true && postTimeValidFlag == true)
    {
        
        // SET 0 = CLOSE
        // SET 1 = OPEN
        
        int rowStatus           =  [cellRowStatus[indexPath.section][indexPath.row]intValue] ;// [cellRowStatus:indexPath.section indexPath.row intValue];
        
        
        // We are not using Else Block as bt default it is Else
        // Row is Alreay OPEN, Now CLOSE it and set the value to 0, indicating it as CLOSE
      
        int newUpdatedRowStatus =   0;
        
        if( rowStatus == 0 )
        {
            // Row is Already ClOSE, Now OPEN it and set the value to 1, indicating it as OPEN
            newUpdatedRowStatus =   1;
            if(!global.dropdownValidFlag)
            {
                if(global.BluetoothConnectionflag==2)
                {
                [self presentAlertViewForPassword];
                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                                    message:@"Please Connect MCD Device"
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];
                }
            }
            // Open a row here
            //self.assignIndividualArrayToCommonArray(indexPath)
        }
        
       /* if(indexPath.section==1)
        {
            if(rowStatus==1)
            {
                if(updateprepostflag)
                {
                    updateprepostflag=false;
                    [self updateprepostvalue_toserver ];
                    
                }
            }
           
            
            
        }*/
        
        
        if(updateprepostflag)
        {
            updateprepostflag=false;
            [self updateprepostvalue_toserver ];
            
        }
        
        //  ########  Make the Last IndexPath to Zero which will close the Cell
        if (lastOpenIndexPath != nil)
        {
            [cellRowStatus[lastOpenIndexPath.section] replaceObjectAtIndex:lastOpenIndexPath.row withObject:[NSNumber numberWithInteger:0]];
            tableViewMcdSettingsCell = (McdSettingsTableViewCell *)[tableView cellForRowAtIndexPath:lastOpenIndexPath];
            tableViewMcdSettingsCell.expandView.hidden = true;
            tableViewMcdSettingsCell.rightArrowBlackImg.transform = CGAffineTransformMakeRotation(6.14);
         
            [self.tableViewMcdSettingsTable reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            //println("Old Index \(lastOpenIndexPath.section) ----  \(lastOpenIndexPath.row)")
        }
        //println("Current Index \(indexPath.section) ----  \(indexPath.row)")
        
       
        //  ########   Update the Current Index Path
        if(global.dropdownValidFlag)
        {
            if(global.BluetoothConnectionflag==2)
            {
                [cellRowStatus[indexPath.section] replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithInteger:newUpdatedRowStatus]];
                [self.tableViewMcdSettingsTable reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                [self.tableViewMcdSettingsTable scrollToRowAtIndexPath :indexPath atScrollPosition:UITableViewScrollPositionTop animated: true];
                
                // Storing LastOpen IndexPath and Store the current one
                lastOpenIndexPath = indexPath;
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                                message:@"Please Connect MCD Device"
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
              //dropdownValidFlag=false;
        
        }
        
    }
    
    //println("Cell Row Status \(cellRowStatus)")
    
}

//    MARK:  UIPICKER VIEW

- (NSInteger)numberOfComponentsInPickerView:(CustomUiPicker *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(CustomUiPicker *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [[pickerView passNsArray]count];
}

- (NSString*)pickerView:(CustomUiPicker *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    NSString * display;
    NSArray * pickerArrayDataForTitle = pickerView.passNsArray;
    if(pickerArrayDataForTitle.count > 0)
    {
        if(pickerView.tableSection == 2 && pickerView.tableRow == 0) // For NotchFilter only
        {
            display = [NSString stringWithFormat:@"%@",pickerArrayDataForTitle[row]];
            if (row != 0)
            {
                display = [NSString stringWithFormat:@"%@",pickerArrayDataForTitle[row]]; //Hz
            }
        }
        else
        {
            display = [NSString stringWithFormat:@"%@",pickerArrayDataForTitle[row]];
        }
    }
   
    
       return display;
}

- (void)pickerView:(CustomUiPicker *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
 
    NSArray *pickerArrayDataForTitle     = pickerView.passNsArray;
    NSString * selectedValue              = pickerArrayDataForTitle[row];
    NSString *leadconf;
    int forSection                  = pickerView.tableSection;
    //int forRow                      = (int)row;
    int forRow                      = pickerView.tableRow;
    
    NSLog(@"%@",selectedValue);
    NSLog(@"rowsec%d %d",forRow,forSection);
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"MCDSettings" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        NSLogger *logger=[[NSLogger alloc]init];
        logger.degbugger = true;
        
        [logger log:@"Coredata Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:[error localizedDescription], @"Mcd Setting page", nil] error:TRUE];
        
    } else {
       // NSLog(@"%@", result);
        if (result.count > 0) {
            NSManagedObject *mcdsett = (NSManagedObject *)[result objectAtIndex:0];
            leadconf=[mcdsett valueForKey:@"lead_config"];
            
        }
    }
    
    NSUserDefaults *Default=[NSUserDefaults standardUserDefaults];
    NSString *firmwareversion=[Default objectForKey:@"Firmware Version"];
    
    
    if(forSection==3&&forRow==2)
    {
        if ((row == 2||row==1)&&(([leadconf intValue]==0)||([leadconf intValue]==1)))
        {
       [pickerView selectRow:0 inComponent:0 animated:YES];
        }
        else
        {
            
          GlobalVars *globals = [GlobalVars sharedInstance];
        [[[[listingItemsDataMain objectAtIndex:forSection]objectForKey:@"rows"]objectAtIndex:forRow]setValue:selectedValue forKey:@"value"];
            if([globals.mcd_bluetoothstatus isEqualToString:@"0"])
            {
        [self.tableViewMcdSettingsTable reloadData];
        lead=[NSString stringWithFormat:@"%ld",(long)row];
        globals.mcd_sett_lead=lead;
        ConfigureMCDController *config=[[ConfigureMCDController alloc]init];
        [config SET_EventDetectionLead : (Byte)lead];
            }
            
        }
    }
    else if ((forSection==3&&forRow==1)&&(row==3)&&([firmwareversion containsString:@"A"]))
    {
          [pickerView selectRow:2 inComponent:0 animated:YES];
    }
    else
    {
   [[[[listingItemsDataMain objectAtIndex:forSection]objectForKey:@"rows"]objectAtIndex:forRow]setValue:selectedValue forKey:@"value"];
    
    [self.tableViewMcdSettingsTable reloadData];
        
      
        GlobalVars *globals = [GlobalVars sharedInstance];
        if([globals.mcd_bluetoothstatus isEqualToString:@"0"])
        {
           
                    if(forSection==1)
                    {
                        
                        
                    }
                    else if (forSection==2)
                    {
                        if(forRow==0)
                        {
                            if(row==0)
                            {
                                fiftyhz=@"0";
                                sixthz=@"0";
                                globals.mcd_sett_fiftyhz=fiftyhz;
                                globals.mcd_sett_sixthz=sixthz;
                                ConfigureMCDController *config=[[ConfigureMCDController alloc]init];
                                [config SET_50HZ_60HZ_FilterState:0];
                            }
                            else if (row==1)
                            {
                                fiftyhz=@"1";
                                sixthz=@"0";
                                globals.mcd_sett_fiftyhz=fiftyhz;
                                globals.mcd_sett_sixthz=sixthz;
                                ConfigureMCDController *config=[[ConfigureMCDController alloc]init];
                                [config SET_50HZ_60HZ_FilterState:1];
                                
                            }
                            else
                            {
                                fiftyhz=@"0";
                                sixthz=@"1";
                                globals.mcd_sett_fiftyhz=fiftyhz;
                                globals.mcd_sett_sixthz=sixthz;
                                ConfigureMCDController *config=[[ConfigureMCDController alloc]init];
                                [config SET_50HZ_60HZ_FilterState:2];
                            }
                            
                            
                        }
                        else if (forRow==1)
                        {
                            baseline=[NSString stringWithFormat:@"%ld",(long)row];
                            globals.mcd_sett_baseline=baseline;
                            ConfigureMCDController *config=[[ConfigureMCDController alloc]init];
                            [config SET_BaseLine_filterState:[baseline intValue]];
                        }
                    }
                    else if(forSection==0)
                    {
                        
                        mute=[NSString stringWithFormat:@"%ld",(long)row];
                        globals.mcd_sett_mute=mute;
                        ConfigureMCDController *config=[[ConfigureMCDController alloc]init];
                        [config SET_Vol_level: [mute intValue]];
                    }
                    else if(forSection==4)
                    {
                        respiration=[NSString stringWithFormat:@"%ld",(long)row];
                        globals.mcd_sett_respiration=respiration;
                        ConfigureMCDController *config=[[ConfigureMCDController alloc]init];
                        [config setRespiration : [respiration intValue]];
                    }
                    else if(forSection==5)
                    {
                        inverted_t_wave=[NSString stringWithFormat:@"%ld",(long)row];
                        globals.mcd_sett_inverted_t_wave=inverted_t_wave;
                        ConfigureMCDController *config=[[ConfigureMCDController alloc]init];
                        [config setInvertedTWave];
                        
                    }
                    else if(forSection==6)
                    {
                        orientation=[NSString stringWithFormat:@"%ld",(long)row];
                        globals.mcd_sett_orientation=orientation;
                        ConfigureMCDController *config=[[ConfigureMCDController alloc]init];
                        [config setOrientation : [orientation intValue]];
                        
                    }
                    else
                    {
                        
                        
                        if(forRow==0)
                        {
                            gain=[NSString stringWithFormat:@"%ld",(long)row];
                            globals.mcd_sett_gain=gain;
                            ConfigureMCDController *config=[[ConfigureMCDController alloc]init];
                            [config SET_GAIN_VALUE :([gain intValue]+1)];
                            
                        }
                        else if(forRow==1)
                        {
                            
                            switch ([[NSString stringWithFormat:@"%ld",(long)row]intValue]) {
                                case 0:
                                    lead_config = @"3";
                                    break;
                                case 1:
                                    lead_config = @"5";
                                    break;
                                case 2:
                                    lead_config = @"12";
                                    break;
                                case 3:
                                {
                                    
                                    lead_config = @"15";
                                    break;
                                }
                                default:
                                    break;
                            }
                            
                            
                            globals.mcd_sett_lead_config=[NSString stringWithFormat:@"%ld",(long)row];
                            ConfigureMCDController *config=[[ConfigureMCDController alloc]init];
                            [config SET_LeadSelect : [lead_config intValue]];
                        }
                        else if(forRow==2)
                        {
                            lead=[NSString stringWithFormat:@"%ld",(long)row];
                            globals.mcd_sett_lead=lead;
                            ConfigureMCDController *config=[[ConfigureMCDController alloc]init];
                            [config SET_EventDetectionLead : (Byte)lead];
                            
                        }
                        else
                        {
                            sampling_rate=[NSString stringWithFormat:@"%ld",(long)row];
                            globals.mcd_sett_sampling_rate=sampling_rate;
                            ConfigureMCDController *config=[[ConfigureMCDController alloc]init];
                            [config SET_SamplingRate : (2-[sampling_rate intValue])];
                            
                            
                        }
                        
                    }
                    
                    
                    
                    
                    /*https://mcc-test-01.personal-healthwatch.com/MCC/MCCThresholdSettings.svc/SetMCDSettings/?DeviceID=123456&PatientID=dee8f70d-6952-4ee1-b2f9-b2f3ded3c294&Gain=2&Filter50Hz=1&Filter60Hz=0&BaseLineFilter=1&Volumelevel=0&TimeIntervalPre=10&TimeIntervalPost=10&BTDeviceAddress=abvchg&Leads=0&SamplingRate=2&EventMode=2&CreatedBy=hila&CreatedFrom=MCM&ModifiedFrom=MCM&CreatedDateTime=10/07/2015&ModifiedDateTime=10/08/2015%20%2006:08:11%20%20&respiration=1&orientation=0*/
                    
                    
                    
                }
            

    }
    
}




- (NSAttributedString *)pickerView:(CustomUiPicker *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString * display;
    NSArray * pickerArrayDataForTitle = pickerView.passNsArray;
      int forSection                  = pickerView.tableSection;
     int forRow                      = pickerView.tableRow;
    
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"MCDSettings" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSString *leadconf;
    
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        NSLogger *logger=[[NSLogger alloc]init];
        logger.degbugger = true;
        
        [logger log:@"Coredata Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:[error localizedDescription], @"Mcd Setting page", nil] error:TRUE];
        
    } else {
       // NSLog(@"%@", result);
        if (result.count > 0) {
            NSManagedObject *mcdsett = (NSManagedObject *)[result objectAtIndex:0];
           // NSLog(@"1 - %@", mcdsett);
            leadconf=[mcdsett valueForKey:@"lead_config"];
            
        }
    }
    
    NSUserDefaults *Default=[NSUserDefaults standardUserDefaults];
    NSString *firmwareversion=[Default objectForKey:@"Firmware Version"];

    if(forSection==3&&forRow==2)
    {
    if ((row == 2||row==1)&&(([leadconf intValue]==0)||([leadconf intValue]==1)))
    {
        
        display = [NSString stringWithFormat:@"%@",pickerArrayDataForTitle[row]];
        return [[NSAttributedString alloc] initWithString:display attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    }
    }
    else if ((forSection==3&&forRow==1)&&(row==3)&&([firmwareversion containsString:@"A"]))
    {
        display = [NSString stringWithFormat:@"%@",pickerArrayDataForTitle[row]];
        return [[NSAttributedString alloc] initWithString:display attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
    }
    return nil;
}


- (void) UpdateMcdSetting:(int)data
{
    NSDate *date = [[NSDate alloc] init];
   // NSLog(@"%@", date);
    
    CommonHelper * commhelp=[[CommonHelper alloc] init];
    GlobalVars *globals = [GlobalVars sharedInstance];
    NSString *modifieddate=[commhelp getDateUTC:@"yyyy-MM-dd" :@"MM/dd/yyyy HH:mm:ss" :DateAsNSDate :@"2014-04-25" :date];
    
    switch (data) {
        case 103:
        {
            NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"MCDSettings"];
            MCDSettings *mcdsettings=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
            
           
            [mcdsettings setValue:[globals mcd_sett_mute] forKey:@"volume_level"];
            [mcdsettings setValue: @"0"              forKey:@"updated_to_site"];
            [mcdsettings setValue:modifieddate forKey:@"modified_time"];
            
            [self.managedObjectContext save:nil];
           break;
        }
        case 105:
        {
            NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"MCDSettings"];
            MCDSettings *mcdsettings=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
            
            
            [mcdsettings setValue:[globals mcd_sett_pretime] forKey:@"timeinterval_pre"];
            [mcdsettings setValue:[globals mcd_sett_postime] forKey:@"timeinterval_post"];
            [mcdsettings setValue: @"0"              forKey:@"updated_to_site"];
            [mcdsettings setValue:modifieddate forKey:@"modified_time"];
            
            [self.managedObjectContext save:nil];
            break;
        }
        case 101:
        {
            NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"MCDSettings"];
            MCDSettings *mcdsettings=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
            
            
            [mcdsettings setValue:[globals mcd_sett_fiftyhz] forKey:@"fiftyhz_filter"];
            [mcdsettings setValue:[globals mcd_sett_sixthz] forKey:@"sixtyhz_filter"];
            [mcdsettings setValue: @"0"              forKey:@"updated_to_site"];
            [mcdsettings setValue:modifieddate forKey:@"modified_time"];
            
            [self.managedObjectContext save:nil];
            break;
        }
        case 102:
        {
            NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"MCDSettings"];
            MCDSettings *mcdsettings=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
            
            
            [mcdsettings setValue:[globals mcd_sett_baseline] forKey:@"base_line_fil"];
            [mcdsettings setValue: @"0"              forKey:@"updated_to_site"];
            [mcdsettings setValue:modifieddate forKey:@"modified_time"];
            
            [self.managedObjectContext save:nil];
            break;
        }
        case 100:
        {
            NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"MCDSettings"];
            MCDSettings *mcdsettings=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
            
            
            [mcdsettings setValue:[globals mcd_sett_gain] forKey:@"gain"];
            [mcdsettings setValue: @"0"              forKey:@"updated_to_site"];
            [mcdsettings setValue:modifieddate forKey:@"modified_time"];
            
            [self.managedObjectContext save:nil];
            break;

        }
        case 109:
        {
            NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"MCDSettings"];
            MCDSettings *mcdsettings=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
            
            
            [mcdsettings setValue:[globals mcd_sett_lead_config] forKey:@"lead_config"];
            [mcdsettings setValue: @"0"              forKey:@"updated_to_site"];
            [mcdsettings setValue:modifieddate forKey:@"modified_time"];
            
            [self.managedObjectContext save:nil];
            break;
        }
        case 107:
        {
            NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"MCDSettings"];
            MCDSettings *mcdsettings=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
            
            
            [mcdsettings setValue:[globals mcd_sett_lead] forKey:@"leads"];
            [mcdsettings setValue: @"0"              forKey:@"updated_to_site"];
            [mcdsettings setValue:modifieddate forKey:@"modified_time"];
            
            [self.managedObjectContext save:nil];
            break;
        }
        case 108:
        {
            NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"MCDSettings"];
            MCDSettings *mcdsettings=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
            
            
            [mcdsettings setValue:[globals mcd_sett_sampling_rate] forKey:@"sampling_rate"];
            [mcdsettings setValue: @"0"              forKey:@"updated_to_site"];
            [mcdsettings setValue:modifieddate forKey:@"modified_time"];
            
            [self.managedObjectContext save:nil];
            break;
        }
        case 111:
        {
            NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"MCDSettings"];
            MCDSettings *mcdsettings=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
            
            
            [mcdsettings setValue:[globals mcd_sett_respiration] forKey:@"respiration"];
            [mcdsettings setValue: @"0"              forKey:@"updated_to_site"];
            [mcdsettings setValue:modifieddate forKey:@"modified_time"];
            
            [self.managedObjectContext save:nil];
            break;

        }
        case 112:
        {
            NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"MCDSettings"];
            MCDSettings *mcdsettings=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
            
            
            [mcdsettings setValue:[globals mcd_sett_orientation] forKey:@"orientation"];
            [mcdsettings setValue: @"0"              forKey:@"updated_to_site"];
            [mcdsettings setValue:modifieddate forKey:@"modified_time"];
            
            [self.managedObjectContext save:nil];
            break;

        }
        case 001:
        {
            NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"MCDSettings"];
            MCDSettings *mcdsettings=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
            
            
            [mcdsettings setValue:[globals mcd_sett_inverted_t_wave] forKey:@"inverted_t_wave"];
            [mcdsettings setValue: @"0"              forKey:@"updated_to_site"];
            [mcdsettings setValue:modifieddate forKey:@"modified_time"];
            
            [self.managedObjectContext save:nil];
            break;
            
        }
        default:
            break;
    }
    
}




-(void)updateprepostvalue_toserver
{
    
    ConfigureMCDController *config=[[ConfigureMCDController alloc]init];
    [config SET_TimeDivIntervfal:[updatepretext intValue]  :[updateposttext intValue]];
    GlobalVars *globals = [GlobalVars sharedInstance];
    globals.mcd_sett_pretime= updatepretext;
    globals.mcd_sett_postime= updateposttext;
    
}






- (void)presentAlertViewForPassword
{
    if(!standalone_Mode)
    {
        SDCAlertView *alert = [[SDCAlertView alloc] initWithTitle:@""
                                                          message:@"Please enter user name and password"
                                                         delegate:self
                                                cancelButtonTitle:@"Cancel"
                                                otherButtonTitles:@"OK", nil];
        [alert setAlertViewStyle:SDCAlertViewStyleLoginAndPasswordInput];
        [alert setTitle:@"Login"];
        [[alert textFieldAtIndex:0] setPlaceholder:@"User name"];
          [alert textFieldAtIndex:0].autocorrectionType = UITextAutocorrectionTypeNo;
        
        self.spinner = [[UIActivityIndicatorView alloc] init];
        self.spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
        [self.spinner setTranslatesAutoresizingMaskIntoConstraints:NO];
        //[spinner startAnimating];
        [alert.contentView addSubview:spinner];
        [spinner sdc_horizontallyCenterInSuperviewWithOffset:-60.0];
        [spinner sdc_verticallyCenterInSuperviewWithOffset:SDCAutoLayoutStandardSiblingDistance];
        
        
        
        lblspin = [[UILabel alloc] init];
        [lblspin setTranslatesAutoresizingMaskIntoConstraints:NO];
        [lblspin setFont:[UIFont systemFontOfSize:12]];
        [alert.contentView addSubview:lblspin];
        [lblspin sdc_horizontallyCenterInSuperview];
        [lblspin sdc_verticallyCenterInSuperviewWithOffset:SDCAutoLayoutStandardSiblingDistance];
        
        
        [alert show];
    }
    else
    {
        DoctorProfileViewController *doctorprofile=[[DoctorProfileViewController alloc]init];
        BOOL checkDocDetail=[doctorprofile UpdatedoctorPatient_rec];
        NSString *mssg;
        if(checkDocDetail)
        {
           
           mssg =@"In order to change this setting, the complete doctor details & patient details must be entered under User Screen and Doctor Screen";
            SDCAlertView *alert = [[SDCAlertView alloc] initWithTitle:@"Validation"
                                                              message:mssg
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                                    otherButtonTitles:@"OK", nil];
            [alert setAlertViewStyle:SDCAlertViewStyleDefault];
            [alert show];
           
        }
        else
        {
              mssg =@"These settings should only be changed by a qualified physician. By approving this message you approve that you are the attending physician";
            SDCAlertView *alert = [[SDCAlertView alloc] initWithTitle:@"Login"
                                                              message:mssg
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                                    otherButtonTitles:@"OK", nil];
            [alert setAlertViewStyle:SDCAlertViewStylePlainTextInput];
              [alert textFieldAtIndex:0].autocorrectionType = UITextAutocorrectionTypeNo;
            self.spinner = [[UIActivityIndicatorView alloc] init];
            self.spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
            [self.spinner setTranslatesAutoresizingMaskIntoConstraints:NO];
            //[spinner startAnimating];
            [alert.contentView addSubview:spinner];
            [spinner sdc_horizontallyCenterInSuperviewWithOffset:-60.0];
            [spinner sdc_verticallyCenterInSuperviewWithOffset:SDCAutoLayoutStandardSiblingDistance];
            
            
            
            lblspin = [[UILabel alloc] init];
            [lblspin setTranslatesAutoresizingMaskIntoConstraints:NO];
            [alert.contentView addSubview:lblspin];
            [lblspin sdc_horizontallyCenterInSuperview];
            [lblspin sdc_verticallyCenterInSuperviewWithOffset:SDCAutoLayoutStandardSiblingDistance];
            
            
            [alert show];
        }
        
       
        
    }

}

- (BOOL)alertView:(SDCAlertView *)alertView shouldDismissWithButtonIndex:(NSInteger)buttonIndex {
    GlobalVars *global = [GlobalVars sharedInstance];
    if(!standalone_Mode)
    {
    
    lblspin.text=@"";
    
    if(buttonIndex==1)
    {
        [self.spinner startAnimating];
        UITextField *username = [alertView textFieldAtIndex:0];
        //NSLog(@"username: %@", username.text);
        
        
        UITextField *password = [alertView textFieldAtIndex:1];
       //NSLog(@"password: %@", password.text);
        

        if(username.text.length == 0 || password.text.length == 0) //check your two textflied has value
        {
            [spinner stopAnimating];
            direction = 1;
            shakes = 0;
             AudioServicesPlaySystemSound (1352);
            [self shake:alertView];
            NSLog(@"EMPTY");
           
            return NO;
        }
        else
        {
           
            lblspin.text=@"Validating...";
            NSString *domainname =[[NSUserDefaults standardUserDefaults] stringForKey:@"DomainName"];
        
            NSString * Fetchingpath=[NSString stringWithFormat:@"https://%@/MCC/MCCWcfService.svc/CargiverAuthentication/?Username=%@&Password=%@&patientId=%@",domainname,username.text,password.text,patientid];
            NSString *bookurl=[Fetchingpath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *URL1=[NSURL URLWithString:bookurl];

            NSLog(@"url%@",URL1);
            
            NSURLRequest *request = [NSURLRequest requestWithURL:URL1];
        
           
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        
        operation.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            NSLog(@"urlresp%@",string);
          
            if([string containsString:@"User Login successfully"])
            {
                global.dropdownValidFlag=true;
                lblspin.text=@"OK";
                [spinner stopAnimating];
                 lockunlocButton.selected=true;
                caregivername=username.text;
                NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
                [userDefault setObject:caregivername forKey:@"CareGiver"];
                [userDefault synchronize];
                 [NSTimer scheduledTimerWithTimeInterval: 120.0 target: self
                                                                  selector: @selector(callAfterSixtySecond:) userInfo: nil repeats: NO];
                dispatch_async (dispatch_get_main_queue(), ^{
                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:indexrw inSection:indexpthsec];
                    [self tableView:self.tableViewMcdSettingsTable didSelectRowAtIndexPath:indexPath];
                    
                    [self.tableViewMcdSettingsTable reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                    [self.tableViewMcdSettingsTable scrollToRowAtIndexPath :indexPath atScrollPosition:UITableViewScrollPositionTop animated: true];
                    
                });
                  [self dismiss:alertView];

            }
            else
            {
                 lockunlocButton.selected=false;
                 AudioServicesPlaySystemSound (1352);
               
                    string = [string stringByReplacingOccurrencesOfString:@"\"" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [string length])];
                   if([string containsString:@"UserName or password is incorrect"])
                   {
                         lblspin.text= @"The user name or password is incorrect";
                   }
                   else if([string containsString:@"Invalid User"])
                   {
                       lblspin.text= @"Invalid user";
                   }
                  else
                lblspin.text= string;
               
               [spinner stopAnimating];
                direction = 1;
                shakes = 0;
                [self shake:alertView];
                [alertView textFieldAtIndex:1].text=@"";
                [alertView textFieldAtIndex:0].text=@"";
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
           /* UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error Retrieving doctors profile"
                                                                message:[error localizedDescription]
                                                               delegate:nil
                                                      cancelButtonTitle:@"Ok"
                                                      otherButtonTitles:nil];
            
            
            [alertView show];*/
            lblspin.text=[error localizedDescription];
             [spinner stopAnimating];
        }];
           
        
            [operation start];
           // [operation waitUntilFinished];
                      return[[operation responseString]boolValue];
         

        
        }
        
    
    }
    else
        return YES;
    }
    else
    {
        if(buttonIndex==0)
        {
            return YES;
        }
        lblspin.text=@"";
        
        if(buttonIndex==1)
        {
            [self.spinner startAnimating];
            UITextField *Docname = [alertView textFieldAtIndex:0];
            NSLog(@"Docname: %@", Docname.text);
            
        
            
            
            if(Docname.text.length == 0) //check your two textflied has value
            {
                [spinner stopAnimating];
                direction = 1;
                shakes = 0;
                AudioServicesPlaySystemSound (1352);
                [self shake:alertView];
                NSLog(@"EMPTY");
                
                return NO;
            }
            else
            {
                
                lblspin.text=@"Validating...";
                
                    
                    if([Docname.text isEqualToString:doctorname])
                    {
                        global.dropdownValidFlag=true;
                        lblspin.text=@"OK";
                        [spinner stopAnimating];
                        lockunlocButton.selected=true;
                        caregivername=Docname.text;
                        NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
                        [userDefault setObject:caregivername forKey:@"CareGiver"];
                        [userDefault synchronize];
                        [NSTimer scheduledTimerWithTimeInterval: 120.0 target: self
                                                       selector: @selector(callAfterSixtySecond:) userInfo: nil repeats: NO];
                        dispatch_async (dispatch_get_main_queue(), ^{
                            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:indexrw inSection:indexpthsec];
                            [self tableView:self.tableViewMcdSettingsTable didSelectRowAtIndexPath:indexPath];
                            
                            [self.tableViewMcdSettingsTable reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                            [self.tableViewMcdSettingsTable scrollToRowAtIndexPath :indexPath atScrollPosition:UITableViewScrollPositionTop animated: true];
                            
                        });
                        [self dismiss:alertView];
                        
                    }
                    else
                    {
                        lockunlocButton.selected=false;
                        AudioServicesPlaySystemSound (1352);
                        lblspin.text=@"Invalid";
                        [spinner stopAnimating];
                        direction = 1;
                        shakes = 0;
                        [self shake:alertView];
                        [alertView textFieldAtIndex:0].text=@"";
                    }
                
                
            }
            
            
        }
        else
            return YES;
        
    }
    
    return NO;
    
}

-(void) callAfterSixtySecond:(NSTimer*) t
{
    NSLog(@"Red");
     GlobalVars *global = [GlobalVars sharedInstance];
    global.dropdownValidFlag=false;
    lockunlocButton.selected=false;
    [self DoctorUniversalLoginTimer:NO];
}

- (BOOL)alertView:(SDCAlertView *)alertView shouldDeselectButtonAtIndex:(NSInteger)buttonIndex {
    return YES;
}


-(void)shake:(SDCAlertView *)theOneYouWannaShake
{
    [UIView animateWithDuration:0.03 animations:^
     {
         theOneYouWannaShake.transform = CGAffineTransformMakeTranslation(5*direction, 0);
     }
                     completion:^(BOOL finished)
     {
         if(shakes >= 10)
         {
             theOneYouWannaShake.transform = CGAffineTransformIdentity;
             return;
         }
         shakes++;
         direction = direction * -1;
         [self shake:theOneYouWannaShake];
     }];
}

-(void)dismiss:(SDCAlertView*)alert
{
    [alert dismissWithClickedButtonIndex:0 animated:YES];
}

- (void)DoctorUniversalLoginTimer:(BOOL)isLoginStatus {
    NSDictionary *connectionDetails = @{@"NoDoctorLogin": @(isLoginStatus)};
    [[NSNotificationCenter defaultCenter] postNotificationName:RWT_DOCTOR_LOGIN_NOTIFICATION object:self userInfo:connectionDetails];
}

- (void)DoctorLoginStatus:(NSNotification *)notification {
    // Connection status changed. Indicate on GUI.
    BOOL status = [(NSNumber *) (notification.userInfo)[@"NoDoctorLogin"] boolValue];
    
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        // Set image based on connection status
        
        
        if (status) {
            
            lockunlocButton.selected=true;
        }
        else
            lockunlocButton.selected=false;
        
    });
}

@end
