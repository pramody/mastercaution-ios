//
//  DoctorProfileViewController.m
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 01/07/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import "DoctorProfileViewController.h"
#import "PatientDetails.h"


#import "SDCAlertView.h"
#import "SDCAlertViewController.h"
#import <UIView+SDCAutoLayout.h>
#import <AudioToolbox/AudioToolbox.h>

@interface DoctorProfileViewController ()

@end

@implementation DoctorProfileViewController
@synthesize headerBlueView,parentMainView,editButton,applyButton,headerTitleLbl,doctorImageReadyOnly,ReadOnlyMainView,firstNameLabel,lastNameLabel,doctorUniqueIdLabel,mobileNoLbl,degreeLbl,emailIdLbl,addressLbl,doctorImageEdit,EditMainView,doctorUniqueIdEditLabel,DocNameText,mobileNoText,DegreeText,emailIdText,addressText,doctorProfileArray,managedObjectContext,expandView,pickerViewControl,blockScreenMainView,StreetText,CityText,StateText,ZipcodeText,CountryText;

bool displaydoctorFrontView = true;
bool phonecallflag=false;
int selectedCountry  = 0;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    AppDelegate * appDelegate = (AppDelegate *)UIApplication.sharedApplication.delegate ;
    appDelegate.orientationKey = @"PRO";
    [appDelegate forceOrientationTakePlace:appDelegate.orientationKey];
    doctorProfileArray=[[NSMutableArray alloc]initWithObjects:@"", @"", @"", @"",nil];
    
    self.slidingViewController.topViewAnchoredGesture = ECSlidingViewControllerAnchoredGestureTapping | ECSlidingViewControllerAnchoredGesturePanning;
    self.slidingViewController.customAnchoredGestures =@[];
    DocNameText.delegate=self;
    doctorUniqueIdEditLabel.delegate=self;
    expandView.hidden=TRUE;
    CountryDicArray=@[@"US",@"Other"];
    
     DocNameFlag=false;
     MobFlag=false;
    // DegreeFlag=false;
    EmailFlag=false;
     AddressFlag=false;
   // DocIDFlag=false;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(IBAction)goToDashboard:(id)sender{
    self.slidingViewController.topViewController.view.layer.transform = CATransform3DMakeScale(1, 1, 1);
    self.slidingViewController.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"DashboardStoryboardId"];
}

-(IBAction)leftSideButtonAction:(id)sender{
    [self.slidingViewController anchorTopViewToRightAnimated:YES];
}



-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    // Force if it is cuming from [[ Event Thresholds Setting ]]
    //self.forceOrientationTakePlace(self)
   UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(hideKeyboard)];
    
    [self.view addGestureRecognizer:tap1];
    
    pickerViewControl.backgroundColor       = UIColor.whiteColor;
    pickerViewControl.delegate = self;
    blockScreenMainView.backgroundColor = [[UIColor darkGrayColor]colorWithAlphaComponent:0.5];
    CountryText.delegate=self;
    StreetText.delegate=self;
    CityText.delegate=self;
    StateText.delegate=self;
    ZipcodeText.delegate=self;
    doctorUniqueIdEditLabel.delegate=self;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL standalone_Mode = [defaults boolForKey:@"ActiveStandalonemode"];
    if(!standalone_Mode)
    {
        self.editButton.hidden = true;
        self.applyButton.hidden = true;
    }
    
    [self intialSetups];
    
}


-(void) intialSetups
{
    blueColorMainAppUIColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"blue_bg_full.png"]];

     headerBlueView.backgroundColor          =   blueColorMainAppUIColor;
    doctorImageReadyOnly.layer.borderWidth =  1.0;
    doctorImageReadyOnly.layer.borderColor =  UIColor.lightGrayColor.CGColor;
    
    // TEXTFIELD Border
    
    CGFloat borderWidth      =     0.3;
    CGColorRef  borderColor    =     [[[UIColor grayColor]colorWithAlphaComponent:0.6]CGColor];
    
    
    doctorUniqueIdEditLabel.layer.borderColor     =     borderColor;
    doctorUniqueIdEditLabel.layer.borderWidth     =     borderWidth;
    doctorUniqueIdEditLabel.leftView        =   [[UIView alloc]initWithFrame:CGRectMake(0,0,15,50)];
    doctorUniqueIdEditLabel.leftViewMode          =     UITextFieldViewModeAlways;
    
    DocNameText.layer.borderColor     =     borderColor;
    DocNameText.layer.borderWidth     =     borderWidth;
    DocNameText.leftView        =   [[UIView alloc]initWithFrame:CGRectMake(0,0,15,50)];
    DocNameText.leftViewMode          =     UITextFieldViewModeAlways;
    
   
    
    mobileNoText.layer.borderColor      =     borderColor;
    mobileNoText.layer.borderWidth      =     borderWidth;
    mobileNoText.leftView       =   [[UIView alloc]initWithFrame:CGRectMake(0,0,15,50)];
    mobileNoText.leftViewMode           =     UITextFieldViewModeAlways;
    
    
    DegreeText.layer.borderColor   =     borderColor;
    DegreeText.layer.borderWidth   =     borderWidth;
    DegreeText.leftView        =   [[UIView alloc]initWithFrame:CGRectMake(0,0,15,50)];
    DegreeText.leftViewMode        =    UITextFieldViewModeAlways;
    
    emailIdText.layer.borderColor       =     borderColor;
    emailIdText.layer.borderWidth       =     borderWidth;
    emailIdText.leftView       =   [[UIView alloc]initWithFrame:CGRectMake(0,0,15,50)];
    emailIdText.leftViewMode            =    UITextFieldViewModeAlways;
    
    
    addressText.layer.borderColor       =     borderColor;
    addressText.layer.borderWidth       =     borderWidth;
    addressText.leftView        =   [[UIView alloc]initWithFrame:CGRectMake(0,0,15,50)];
    addressText.leftViewMode            =     UITextFieldViewModeAlways;
    
    
    
    
    //fetching record
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    //PatientDetails *patientrecord = [NSEntityDescription insertNewObjectForEntityForName:@"PatientDetails"
    //inManagedObjectContext:self.managedObjectContext];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"PatientDetails" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        if (result.count > 0) {
            NSManagedObject *doctor = (NSManagedObject *)[result objectAtIndex:0];
            firstNameLabel.text=[doctor valueForKey:@"doc_name"];
           // lastNameLabel.text=[doctor valueForKey:@"doc_name"];
           // doctorUniqueIdLabel.text=[doctor valueForKey:@"doc_id"];
            mobileNoLbl.text=[doctor valueForKey:@"doc_ph_no"];
           // degreeLbl.text=[doctor valueForKey:@"doc_degree"];
            emailIdLbl.text=[doctor valueForKey:@"doc_email"];
            addressLbl.text=[doctor valueForKey:@"doc_address"];
            
            
        }
    }
    
    DocNameText.text=firstNameLabel.text;
    //doctorUniqueIdEditLabel.text= doctorUniqueIdLabel.text;
    mobileNoText.text= mobileNoLbl.text;
   // DegreeText.text= degreeLbl.text;
    emailIdText.text= emailIdLbl.text;
    addressText.text= addressLbl.text;
    
    [doctorProfileArray replaceObjectAtIndex:0 withObject:firstNameLabel.text];
   // [doctorProfileArray replaceObjectAtIndex:1 withObject:doctorUniqueIdLabel.text];
    [doctorProfileArray replaceObjectAtIndex:1 withObject:mobileNoLbl.text];
   // [doctorProfileArray replaceObjectAtIndex:2 withObject:degreeLbl.text];
    [doctorProfileArray replaceObjectAtIndex:2 withObject:emailIdLbl.text];
    [doctorProfileArray replaceObjectAtIndex:3 withObject:addressLbl.text];
   
    
    
}

- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

-(void) hideKeyboard
{
  
     [self.view endEditing:true];
   
}


-(IBAction)editProfileAction:(UIButton *)sender
{
    
    BOOL standalone_Mode;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    standalone_Mode = [defaults boolForKey:@"ActiveStandalonemode"];
    
    if(standalone_Mode)
   {
    [self hideKeyboard];
    //println("Button action here");
    
    BOOL doTransition = false;
    
    if([sender tag] == 0) // Edit Button Clicked
    {
        doTransition = true;
        displaydoctorFrontView=TRUE;
        //println("Going to Edit");
    }
    else if([sender tag] == 1) // Apply Button Clciked
    {
        doTransition = false; // this will first save the data and that it will execute the transition
        //println("Going to Apply");
        
        //println("View all data to Readyonly now \(userProfileArray)")
        bool validationIs = [self doctorProfileValidation];
        if( validationIs == true)
        {
            [self showAlertView:@"Success Alert" msg: @"Doctor Profile updated successfully!"];
            
            int j=1;
            
            
            
            
            while(j<5)
            {
                if(DocNameFlag)
                {
                    DocNameFlag=false;
                    
                    [doctorProfileArray replaceObjectAtIndex:0 withObject:DocNameText.text];
                    
                    
                    NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"PatientDetails"];
                    PatientDetails *Doctorsettings=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
                    [Doctorsettings setValue:DocNameText.text forKey:@"doc_name"];
                    [Doctorsettings setValue:@"0" forKey:@"pat_updated_site"];
                    
                    [self.managedObjectContext save:nil];
                    
                    
                }
               /* else if (DocIDFlag==true)
                {
                    DocIDFlag=false;
                    
                    [doctorProfileArray replaceObjectAtIndex:1 withObject:doctorUniqueIdEditLabel.text];
                    
                    
                    NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"PatientDetails"];
                    PatientDetails *Doctorsettings=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
                    [Doctorsettings setValue:doctorUniqueIdEditLabel.text forKey:@"doc_id"];
                    [Doctorsettings setValue:@"0" forKey:@"pat_updated_site"];
                    
                    [self.managedObjectContext save:nil];
                }*/
              
                else if (MobFlag==true)
                {
                    MobFlag=false;
                    
                     [doctorProfileArray replaceObjectAtIndex:1 withObject:mobileNoText.text];
                    NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"PatientDetails"];
                    PatientDetails *Doctorsettings=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
                    [Doctorsettings setValue:mobileNoText.text forKey:@"doc_ph_no"];
                    [Doctorsettings setValue:@"0" forKey:@"pat_updated_site"];
                    
                    [self.managedObjectContext save:nil];
                    
                    
                    
                }
               /* else if (DegreeFlag==true)
                {
                    DegreeFlag=false;
                     [doctorProfileArray replaceObjectAtIndex:3 withObject:DegreeText.text];
                    NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"PatientDetails"];
                    PatientDetails *patientsettings=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
                    [patientsettings setValue:DegreeText.text forKey:@"doc_degree"];
                    [patientsettings setValue:@"0" forKey:@"pat_updated_site"];
                    
                    [self.managedObjectContext save:nil];
                    
                }*/
                else if (EmailFlag==true)
                {
                    EmailFlag=false;
                    [doctorProfileArray replaceObjectAtIndex:2 withObject:emailIdText.text];
                    
                    NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"PatientDetails"];
                    PatientDetails *patientsettings=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
                    [patientsettings setValue:emailIdText.text forKey:@"doc_email"];
                    [patientsettings setValue:@"0" forKey:@"pat_updated_site"];
                    
                    [self.managedObjectContext save:nil];
                    
                    
                }
                else if (AddressFlag==true)
                {
                    AddressFlag=false;
                    
                     [doctorProfileArray replaceObjectAtIndex:3 withObject:addressText.text];
                    NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"PatientDetails"];
                    PatientDetails *patientsettings=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
                    [patientsettings setValue:addressText.text forKey:@"doc_address"];
                    [patientsettings setValue:CountryText.text forKey:@"doc_country"];
                    [patientsettings setValue:@"0" forKey:@"pat_updated_site"];
                    
                    [self.managedObjectContext save:nil];
                    
                    
                }
                j++;
            }

            doTransition = true;
            [self updateLabelForReadOnlyView];
        }
        
        
    }
    
    
    if(doTransition == true)
    {
        [UIView transitionWithView:[self parentMainView]
                          duration:1
                           options:displaydoctorFrontView ?UIViewAnimationOptionTransitionFlipFromRight:UIViewAnimationOptionTransitionFlipFromLeft
                        animations:^{
                            
                            //  () -> Void in
                            
                            NSString * headerTitle;
                            self.EditMainView.hidden = true;
                            self.ReadOnlyMainView.hidden = true;
                            self.editButton.hidden = true;
                            self.applyButton.hidden = true;
                            
                            if(displaydoctorFrontView)
                            {
                                self.EditMainView.hidden = false;
                                self.applyButton.hidden = false;
                                headerTitle =@"Edit Doctor Profile";
                                
                            }
                            else
                            {
                                self.ReadOnlyMainView.hidden = false;
                                self.editButton.hidden = false;
                                headerTitle =@"Doctor Profile";
                                
                            }
                            
                            self.headerTitleLbl.text = headerTitle;
                            
                        }
                        completion:^(BOOL finished){
                            
                            
                            
                            displaydoctorFrontView = !displaydoctorFrontView;
                            
                        }];
        
    }
   }

}



-(void) showAlertView:(NSString *)title  msg:(NSString *) message
{
    /*UIAlertView *alert=[[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    */
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       
                                   }];
    [alertVC addAction:cancelAction];
    
    [self presentViewController:alertVC animated:YES completion:nil];
}

// Prepare Picker View for Height




-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    //println("shouldChangeCharactersInRange here UITextField \(textField.text) ===== String \(string) == Range \(range) Range Length \(range.length)")
    
    return true;
    
}



-(void) textFieldDidEndEditing:(UITextField *)textField {
    //println("Should DID End Editing \(textField.text)")
    
}

-(BOOL) textFieldShouldEndEditing:(UITextField *)textField {
    //println("Should End Editing \(textField.text)")
    
    BOOL isValidData=true;
    CommonHelper *comphelp=[[CommonHelper alloc] init];
    NSString * inputValue  = [comphelp trimTheString:textField.text];
    if(DocNameText == textField)
    {
        if (/*inputValue.length > 0 */ true)
        {
            
            [DocNameText resignFirstResponder];
            [DocNameText becomeFirstResponder];
            
            if(DocNameText.text.length>0)
            {
                NSCharacterSet *s = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"];
                s = [s invertedSet];
                NSString *str1 = DocNameText.text;
                NSRange r = [str1 rangeOfCharacterFromSet:s];
                if (r.location != NSNotFound) {
                    NSLog(@"the string contains illegal characters");
                    
                    [self showAlertView: @"Validation Alert"  msg: @"the string contains illegal characters"];
                }
                else
                {
                    DocNameFlag=TRUE;
                    [doctorProfileArray replaceObjectAtIndex:0 withObject:inputValue];
                }
                
            }

            //doctorProfileArray.setObject(inputValue, forKey: "first_name")
        }
    }
   /* else if(doctorUniqueIdEditLabel == textField)
    {
        if ( true )
        {
            [doctorProfileArray replaceObjectAtIndex:1 withObject:inputValue];
            DocIDFlag=TRUE;
            //doctorProfileArray.setObject(inputValue, forKey: "mobile_no")
        }
    }*/
    else if(mobileNoText == textField)
    {
        if (/*inputValue.length > 0 */ true )
        {
             [doctorProfileArray replaceObjectAtIndex:1 withObject:inputValue];
            MobFlag=TRUE;
            //doctorProfileArray.setObject(inputValue, forKey: "mobile_no")
        }
    }
   /* else if(DegreeText == textField)
    {
        if ( true )
        {
            DegreeFlag=TRUE;
             [doctorProfileArray replaceObjectAtIndex:3 withObject:inputValue];
           
            //doctorProfileArray.setObject(inputValue, forKey: "telephone_no")
        }
    }*/
    else if(emailIdText == textField)
    {
        if (/*inputValue.length > 0 */ true )
        {
            EmailFlag=TRUE;
              [doctorProfileArray replaceObjectAtIndex:2 withObject:inputValue];
            //doctorProfileArray.setObject(inputValue, forKey: "email_id")
        }
    }
    else if(addressText == textField)
    {
        if (/*inputValue.length > 0 */ true )
        {
            AddressFlag=TRUE;
              [doctorProfileArray replaceObjectAtIndex:3 withObject:inputValue];            //doctorProfileArray.setObject(inputValue, forKey: "address")
        }
    }
    else if(CountryText == textField)
    {
        if (CountryText.text.length==0)
        {
            return false;
        }
    }
    

    
    return isValidData;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    //println("Should Return \(textField.text)")
    
    return YES;
}

-(BOOL) textFieldShouldBeginEditing:(UITextField *)textField {
    //println("textFieldShouldBeginEditing \(textField)")
     BOOL showKeyBoard = true;
   
    if(CountryText==textField)
    {
        showKeyBoard=false;
        [self generateHeightPickerView ];
        [self showPickerViewControl:true];
        [self hideKeyboard];
        [self showBlockView:true];
        //CountryText.enabled=false;
       // NSInteger genderrowvalue = [CountryDicArray indexOfObject:@"0"];
        [pickerViewControl selectRow:0 inComponent:0 animated:false];

    }
    
    
    return showKeyBoard;
  
}




-(BOOL)doctorProfileValidation
{
    BOOL returnValue =   true;
    NSString * titleTEXT       =   @"Validation Alert";
    NSString * messageTEXT     =   @"Invalid inputs";
    
    
    
    
    if(doctorProfileArray.count > 0)
    {
        int arrayLoopCount = (int)doctorProfileArray.count ;
        
        for (int i = 0; i < arrayLoopCount; i++)
        {
            //println("i == \(i)")
            CommonHelper *comphelp=[[CommonHelper alloc] init];
            NSString * value  = [comphelp trimTheString:doctorProfileArray[i]];
            switch (i) {
                case 0:
                {
                    if(value.length > 30)
                    {
                        returnValue     =   false;
                        messageTEXT     =   @"Name cannot be greater than 30 words!";
                    }
                    else if (value.length == 0)
                    {
                        returnValue     =   false;
                        messageTEXT     =   @"Name cannot be blank!";
                    }
 
                }
                    break;
                /* case 1:
                {
                    if (value.length == 0)
                    {
                        returnValue     =   false;
                        messageTEXT     =   @"Doc ID  cannot be blank!";
                    }
                  break;
                }*/
                    case 1:
                   {
                    if(value.length > 12)
                    {
                        returnValue     =   false;
                        messageTEXT     =   @"Mobile could not be greater than 12!";
                    }
                    else if (value.length == 0)
                    {
                        returnValue     =   false;
                        messageTEXT     =   @"Mobile no cannot be blank!";
                    }
                   
                  }
                     break;
                /*    case 3:
                {
                    if(value.length==0)
                    {
                        returnValue     =   false;
                        messageTEXT     =   @"Qaulification no cannot be blank!";
                       
                    }
                }
                     break;*/
                    case 2:
                  {
                    if(value.length == 0)
                    {
                        returnValue     =   false;
                        messageTEXT     =   @"Email cannot be blank!";
                                            }
                    else if ([comphelp isEmailValid:value :true]==false)
                    {
                        returnValue     =   false;
                        messageTEXT     =   @"Email is Invalid!";
                        
                    }
                   }
                     break;
                    case 3:
                 {
                     if(value.length == 0)
                     {
                         returnValue     =   false;
                         messageTEXT     =   @"Address cannot be blank!";
                        
                     }
                 }
                     break;
                default:
                    break;
            }
           
        }
        
    }
   
    
    
    if(returnValue == false)
    {
        
        [self showAlertView:titleTEXT  msg: messageTEXT];
    }
    
    return returnValue;
}

-(void) updateLabelForReadOnlyView
{
    
    //   ************   This should be uploaded once it is saved in the Database  *****************
    // Update Ready only Labels
  
    NSLog(@"doct%@",doctorProfileArray);
   
    NSString *  firstName       =   doctorProfileArray[0];
   // NSString *  doctor_id       =   doctorProfileArray[1];
    NSString *  mobile          =   doctorProfileArray[1];
   // NSString *  degree          =   doctorProfileArray[3];
    NSString *  email           =   doctorProfileArray[2];
    NSString *  address         =   doctorProfileArray[3];
    
    
    
    firstNameLabel.text         =   firstName;
   // doctorUniqueIdLabel.text    =   doctor_id;
    mobileNoLbl.text            =   mobile;
  //  degreeLbl.text              =   degree;
    emailIdLbl.text             =   email;
    addressLbl.text             =   address;
    
    DocNameText.text=firstNameLabel.text;
  //  doctorUniqueIdEditLabel.text= doctorUniqueIdLabel.text;
    mobileNoText.text= mobileNoLbl.text;
  //  DegreeText.text= degreeLbl.text;
    emailIdText.text= emailIdLbl.text;
    addressText.text= addressLbl.text;
    
}



-(IBAction)Adrressclick:(UIButton *)sender
{
    [self hideKeyboard];
    if(expandView.hidden==TRUE)
    {
        
        [UIView transitionWithView:expandView
                          duration:0.5
                           options:UIViewAnimationOptionTransitionCurlDown
                        animations:^(void){
                            expandView.hidden=FALSE;
                        }
                        completion:nil];
        
        
    }
    
    
   
 
}




//pickerview

-(IBAction)donePressOnPop:(id)sender
{
    NSString * Country = (selectedCountry < CountryDicArray.count ) ? CountryDicArray[selectedCountry] : @"";
    NSLog(@"%@",Country);
     [self cancelPressOnPop:nil];
    if([Country isEqualToString:@"Other"])
    {
        
        CountryText.text=@"";
        phonecallflag=false;
        SDCAlertView *alert = [[SDCAlertView alloc] initWithTitle:@"Country"
                                                          message:@""
                                                         delegate:self
                                                cancelButtonTitle:@"Cancel"
                                                otherButtonTitles:@"OK", nil];
        [alert setAlertViewStyle:SDCAlertViewStylePlainTextInput];
        
        [alert show];

    }
    else
    {
        CountryText.text=Country;
    }
}

-(IBAction)cancelPressOnPop:(id)sender
{
    // Hide Transparent Block View
    [self showBlockView:false];
    
    // Hide All Drop downs
    [self hideAllControls];
    
}

-(void) hideAllControls
{
    [self showPickerViewControl:false];
}

-(void) showBlockView:(BOOL)boolValue
{
    blockScreenMainView.hidden = !boolValue;
}

-(void) showPickerViewControl:(BOOL)boolValue
{
    pickerViewControl.hidden = !boolValue;
}


-(void) generateHeightPickerView
{
    [[self pickerViewControl]reloadAllComponents];
}


//  -------   UIPICKER DELEGATES and DATASOURCE -------  START

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    
    NSInteger pickerComponents = 1; // Default for GENDER
    
    
    return pickerComponents;
    
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSInteger rowInComp = CountryDicArray.count; // Default to GENDER [Male, Female]
   
    
    return rowInComp;
    
}

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    NSString * titleValue;
    
    titleValue = CountryDicArray[row];
    
    return titleValue;
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    

     selectedCountry =(int)row;
    
}

- (BOOL)alertView:(SDCAlertView *)alertView shouldDismissWithButtonIndex:(NSInteger)buttonIndex {
      if(!phonecallflag)
      {
        if(buttonIndex==1)
        {
              UITextField *countryname = [alertView textFieldAtIndex:0];
              if(countryname.text.length!=0)
              {
                  CountryText.text=countryname.text;
                  return YES;
              }
            else
            {
                return NO;
            }
        }
        else
            return YES;
      }
    return YES;
}


-(IBAction)addressdone:(id)sender
{
    NSString *addt=@"";
    
        if(StreetText.text.length!=0)
            addt= [addt stringByAppendingFormat:@"%@",StreetText.text];
         if (CityText.text.length!=0)
            addt=[addt stringByAppendingFormat:@",%@",CityText.text];
         if (StateText.text.length!=0)
            addt=[addt stringByAppendingFormat:@",%@",StateText.text];
         if (CountryText.text.length!=0)
            addt=[addt stringByAppendingFormat:@",%@",CountryText.text];
         if (ZipcodeText.text.length!=0)
            addt=[addt stringByAppendingFormat:@",%@",ZipcodeText.text];
    
    
    addressText.text=addt;
    AddressFlag=TRUE;
    [doctorProfileArray replaceObjectAtIndex:3 withObject:addressText.text];
    
    [UIView transitionWithView:expandView
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCurlUp
                    animations:^(void){
                        expandView.hidden=TRUE;
                    }
                    completion:nil];
}

// Common Methods
-(BOOL)UpdatedoctorPatient_rec
{
    BOOL res = false;
 
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"PatientDetails" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        if (result.count > 0) {
         
            NSManagedObject *doctor = (NSManagedObject *)[result objectAtIndex:0];
            for(int i=0;i<10;i++)
            {
             
            if(i==0)
            {
            if([[doctor valueForKey:@"patient_name"] isEqualToString:@""])
            {
                 res=TRUE;
                break;
               
            }
            }
            if(i==1)
            {
                NSDate *date = [[NSDate alloc] init];
                NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
                [dateFormatter2 setDateFormat:@"MM/dd/yyyy"];
                NSString *dob = [dateFormatter2 stringFromDate:date];
             if([[doctor valueForKey:@"dob1"] isEqualToString:dob])
            {
                 res=TRUE;
                break;
                
            }
            }
            if(i==2)
            {
             if([[doctor valueForKey:@"height1"] isEqualToString:@"31"])
            {
                 res=TRUE;
                break;
                
            }
            }
            if(i==3)
            {
             if([[doctor valueForKey:@"weight"] isEqualToString:@"00"])
            {
                 res=TRUE;
                break;
                
            }
            }
                
          /*  else if(![[doctor valueForKey:@"gender1"] isEqualToString:@"06/29/1900"])
            {
                return FALSE;
            }*/
             if(i==4)
             {
             if([[doctor valueForKey:@"patient_add"] isEqualToString:@"XYZ"])
            {
                  res=false;
                //break;
            }
             }
             if(i==5)
             {
            if([[doctor valueForKey:@"patient_mobno"] isEqualToString:@""])
            {
                 res=false;
                //break;
            }
             }
             if(i==6)
             {
             if([[doctor valueForKey:@"doc_name"] isEqualToString:@""])
            {
                res=TRUE;
                break;
                
            }
             }
            if(i==7)
            {
            if([[doctor valueForKey:@"doc_email"] isEqualToString:@""])
            {
                 res=TRUE;
                break;
                
            }
            }
            if(i==8)
            {
            if([[doctor valueForKey:@"doc_address"] isEqualToString:@""])
            {
                  res=TRUE;
                break;
               
            }
            }
            if(i==9)
            {
            if([[doctor valueForKey:@"doc_ph_no"] isEqualToString:@""])
            {
                res=TRUE;
                break;
                
            }
            }
        }
        }
    }
    return res;
}



-(IBAction)PhonePressed:(id)sender
{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSString *docPhno,*docName;
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"PatientDetails" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
   
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        if (result.count > 0) {
            NSManagedObject *patient = (NSManagedObject *)[result objectAtIndex:0];
            // NSLog(@"1 - %@", patient);
            
            
            docPhno=[patient valueForKey:@"doc_ph_no"];
            docName=[patient valueForKey:@"doc_name"];
            
            if(docPhno.length!=0)
            {
            SDCAlertView *alert = [[SDCAlertView alloc]   initWithTitle:@"" message:@"Do you want to call your doctor!"
                                                               delegate:self
                                                      cancelButtonTitle:@"Dismiss"
                                                      otherButtonTitles:@"Call", nil];
            [alert setAlertViewStyle:SDCAlertViewStyleDefault];
            
            [alert setTitleLabelFont:[UIFont fontWithName:@"Arial" size:24.0]];
            [alert setTitleLabelTextColor:[UIColor blackColor]];
            [alert setMessageLabelFont:[UIFont fontWithName:@"Arial" size:14.0]];
            
            
            UILabel * lblspin = [[UILabel alloc] init];
            [lblspin setTranslatesAutoresizingMaskIntoConstraints:NO];
            [alert.contentView addSubview:lblspin];
            lblspin.backgroundColor=[UIColor blackColor];
            [lblspin sdc_pinWidthToWidthOfView:alert.contentView offset:0];
            [lblspin sdc_pinHeight:3];
            [lblspin sdc_horizontallyCenterInSuperview];
            [lblspin sdc_verticallyCenterInSuperviewWithOffset:
             -40];
            
            // [lblspin sdc_verticallyCenterInSuperviewWithOffset:SDCAutoLayoutStandardSiblingDistance];
            
            UITextView *textView = [[UITextView alloc] init];
            [textView setTranslatesAutoresizingMaskIntoConstraints:NO];
            [textView setEditable:NO];
            textView.text=[NSString stringWithFormat:@"%@\n%@",docName,docPhno];//    @"Doctor Felix Amar\n1234566789";
            textView.autocorrectionType = UITextAutocorrectionTypeNo;
            [alert.contentView addSubview:textView];
            
            textView.backgroundColor=[UIColor clearColor];
            textView.textAlignment=NSTextAlignmentCenter;
            textView.font=[UIFont fontWithName:@"Arial" size:18.0];
            [textView sdc_pinWidthToWidthOfView:alert.contentView offset:-15];
            [textView sdc_pinHeight:50];
            [textView sdc_horizontallyCenterInSuperview];
            [textView sdc_verticallyCenterInSuperviewWithOffset:SDCAutoLayoutStandardSiblingDistance];
            
            NSString * telno=[NSString stringWithFormat:@"tel://%@",docPhno];
                phonecallflag=true;
            [alert showWithDismissHandler: ^(NSInteger buttonIndex) {
                NSLog(@"Tapped button: %@", @(buttonIndex));
                if (buttonIndex == 1) {
                    phonecallflag=false;
                    NSURL *url = [NSURL URLWithString:telno];
                    [[UIApplication  sharedApplication] openURL:url];
                }
                else {
                    NSLog(@"Cancelled");
                }
            }];

            }
        }
    }
    
    
}


-(IBAction)EmailPressed:(id)sender
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSString *docEmail;
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"PatientDetails" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        if (result.count > 0) {
            NSManagedObject *patient = (NSManagedObject *)[result objectAtIndex:0];
            // NSLog(@"1 - %@", patient);
            
            
            docEmail=[patient valueForKey:@"doc_email"];
            
            if(docEmail.length!=0)
            {
            NSArray *srry=[[NSArray alloc] initWithObjects:docEmail, nil];
            if ([MFMailComposeViewController canSendMail])
            {
                MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc] init];
                mailComposer.mailComposeDelegate = self;
                [mailComposer setToRecipients:srry];
                //[mailComposer setSubject:@"MasterCaution Log File"];
                
                /*  UIImage *myImage = [UIImage imageNamed:@"safeguardmode.png"];
                 NSData *imageData = UIImagePNGRepresentation(myImage);
                 [mailComposer addAttachmentData:imageData mimeType:@"image/png" fileName:@"FullTitle.png"];*/
                
                NSString *emailBody = @"Hi there!!";
                [mailComposer setMessageBody:emailBody isHTML:NO];
                
                
                
                // Present mail view controller on screen
                [self presentViewController:mailComposer animated:YES completion:NULL];
                
                // [self presentModalViewController:mailComposer animated:YES];
            }
            
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                                message:@"Your device has not configure email "
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
            }
        }
    }
  

}

#pragma mark - mail compose delegate
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}


@end
