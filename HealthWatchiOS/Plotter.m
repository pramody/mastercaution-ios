//
//  Plotter.m
//  HealthWatch
//
//  Created by METSL MAC MINI on 24/02/16.
//  Copyright © 2016 METSL MAC MINI. All rights reserved.
//

#import "Plotter.h"

@implementation Plotter
int offsetArray[17];
NSInteger p_ecg_y[17];
bool flag_1;
Byte control_code_1;
Byte no_ofBytes_1;
Byte sample_12;
bool plot_ok_1;
int scrheight=1024;
int scrwidth=768;
int blit_start_pt = 0, blit_end_pt = 0;
float cur_x, prev_x;
uint8_t plot_data_1[10000];
int data_counter12;
int end_pt;
int numberofpages = 0;
 long filelength = 0;
int readdataperpage=0;
int display_page = 1;
int colorvalue;
NSString *EVENTCODE,*EVENTVALUE;
int MCDLEAD,LEADCONFIG,RESPIRATION,LEAD_COUNT;

+(void)drawLineFromPoint:(CGPoint)from toPoint:(CGPoint)to :(int)colortype
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetLineWidth(context,1.0);
    
    CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
    
    CGColorRef color;
    if(colortype==0)
    {
      CGFloat components[] = {0, 0, 0, 1.0};
         color = CGColorCreate(colorspace, components);
    }
    else
    {
      CGFloat components[] = {1, 0, 0, 1.0};
         color = CGColorCreate(colorspace, components);
    }
    
   
    // CGContextSetStrokeColor(context, [UIColor blackColor]);
    CGContextSetStrokeColorWithColor(context, color);
    
    //NSLog(@"%0.0f %0.0f",from.y,to.y);
    CGContextMoveToPoint(context, from.x, from.y);
    CGContextAddLineToPoint(context, to.x, to.y);
    CGContextStrokePath(context);
    CGColorSpaceRelease(colorspace);
    CGColorRelease(color);
    
}


+(void)drawPDF :(NSString*)pdffilename :(NSString*)fileName :(int)Mcdlead :(int)leadconfig :(NSString *)EventCode :(NSString *)EventValue :(int)Respiration
{
    
    MCDLEAD=Mcdlead;
    LEADCONFIG=leadconfig;
    EVENTCODE=EventCode;
    EVENTVALUE=EventValue;
    RESPIRATION=Respiration;
    LEAD_COUNT=leadconfig;
    
    if(RESPIRATION==0)
         LEADCONFIG=leadconfig-1;
    
    end_pt=scrwidth-55;
    // Create the PDF context using the default page size of 612 x 792.
    
    blit_end_pt=0;
    blit_start_pt=0;
    
   
    
    NSArray *arrayPaths =
    NSSearchPathForDirectoriesInDomains(
                                        NSDocumentDirectory,
                                        NSUserDomainMask,
                                        YES);
    NSString *path = [arrayPaths objectAtIndex:0];
    NSString* pdfFileName = [path stringByAppendingPathComponent:fileName];
    NSDictionary *attributes = [[NSFileManager defaultManager] attributesOfItemAtPath:pdfFileName error:NULL];
    filelength = [attributes fileSize];
    int framecount;
    if(MCDLEAD==12)
        framecount=20;
    else
        framecount=26;
   
    int totalframes =(int) filelength / framecount;
    double actualplottingframe = totalframes / 4;
    numberofpages = (int) ceil(actualplottingframe /end_pt);
    
    NSInteger currentPage = 0;
    UIGraphicsBeginPDFContextToFile(pdffilename, CGRectZero, nil);
    [self drawPageNumber:currentPage];
    // Mark the beginning of a new page.
    BOOL done = NO;
    
   do {
        // Mark the beginning of a new page.
        UIGraphicsBeginPDFPageWithInfo(CGRectMake(0,0, scrwidth, scrheight), nil);
        [self resetSlate];
        [self drawLabel];
        [self Fileread:fileName];
        
        // Draw a page number at the bottom of each page.
        currentPage++;
        [self drawPageNumber:currentPage];
        
        // Render the current page and update the current range to
        // point to the beginning of the next page.
        
        if(currentPage==numberofpages)
            done = YES;
    } while (!done);
    
    
    UIGraphicsEndPDFContext();
  
   
    
}

+(void)Fileread :(NSString *)fileName1
{
   
    
    NSArray *arrayPaths =
    NSSearchPathForDirectoriesInDomains(
                                        NSDocumentDirectory,
                                        NSUserDomainMask,
                                        YES);
    NSString *path = [arrayPaths objectAtIndex:0];
    NSString* pdfFileName = [path stringByAppendingPathComponent:fileName1];
    
    
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:pdfFileName]) {
        
        // the file doesn't exist,we can write out the text using the  NSString convenience method
        
        NSLog(@"File Does not exist");
        
        
    } else {
        
        // the file already exists, append the text to the end
        
        // get a handle
        NSFileHandle *fileHandle = [NSFileHandle fileHandleForReadingAtPath:pdfFileName];
        
        // move to the end of the file
        //[fileHandle seekToEndOfFile];
        
        // convert the string to an NSData object
        
        
        // write the data to the end of the file
        
        NSData *data=[fileHandle readDataToEndOfFile];
        
        NSDictionary *attributes = [[NSFileManager defaultManager] attributesOfItemAtPath:pdfFileName error:NULL];
        filelength = [attributes fileSize];
        int framecount;
        
        if(MCDLEAD==12)
            framecount=20;
        else
            framecount=26;
       
        
        int totalframes =(int) filelength / framecount;
        //NSLog(@"frames=%d",totalframes);
        double actualplottingframe = totalframes /4.5;
        numberofpages = (int) ceil(actualplottingframe /end_pt);
        readdataperpage = end_pt * 4.6 * framecount;
        blit_end_pt =blit_end_pt+readdataperpage;
        
      //  NSLog(@"datacnt=%d",blit_end_pt);
        
        
        
        
        if(blit_end_pt>filelength)
            blit_end_pt=(int)filelength;
        
        //  NSLog(@"cont%@%@",data,  ecgpath );
        
        // clean up
        [fileHandle closeFile];
        
        
        
        uint8_t * BytePtr = (uint8_t  * )[data bytes];
        NSInteger totalData = [data length] / sizeof(uint8_t);
        
       
        if(totalData!=0)
        {
            
            for (int i = blit_start_pt ; i <blit_end_pt; i++)
            {
                //NSLog(@"data%hhu",BytePtr[i]);
                [self MCDParser:BytePtr[i]];
                
            }
        }
        blit_start_pt=blit_end_pt;
    }
    
}

+(void)drawGrid {
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGFloat full_height =scrheight-53;
    CGFloat full_width =scrwidth-55;
    CGFloat cell_square_width =10;
    
    
   CGContextSetLineWidth(ctx, 1.5);
    
    CGContextSetStrokeColorWithColor(ctx, [UIColor colorWithRed:245.0/ 255 green:181.0/ 255 blue:197.0/255 alpha:1].CGColor);
    
    int pos_x = 50;
    while (pos_x < full_width) {
        CGContextMoveToPoint(ctx, pos_x, 50);
        CGContextAddLineToPoint(ctx, pos_x, full_height);
        pos_x += cell_square_width;
        
        CGContextStrokePath(ctx);
    }
    
    CGFloat pos_y = 50;
    while (pos_y <= full_height) {
        
        CGContextSetLineWidth(ctx, 1.5);
        
        CGContextMoveToPoint(ctx, 50, pos_y);
        CGContextAddLineToPoint(ctx, full_width, pos_y);
        pos_y += cell_square_width;
        
        CGContextStrokePath(ctx);
    }
    
    
    CGContextSetLineWidth(ctx, 0.1);
    
    cell_square_width = cell_square_width / 5;
    pos_x = 50 + cell_square_width;
    while (pos_x < full_width) {
        CGContextMoveToPoint(ctx, pos_x, 50);
        CGContextAddLineToPoint(ctx, pos_x, full_height);
        pos_x += cell_square_width;
        
        CGContextStrokePath(ctx);
    }
    
    pos_y = 50 + cell_square_width;
    while (pos_y <= full_height) {
        CGContextMoveToPoint(ctx, 50, pos_y);
        CGContextAddLineToPoint(ctx, full_width, pos_y);
        pos_y += cell_square_width;
        
        CGContextStrokePath(ctx);
    }
}

+ (void)drawLabel {
    int scrhgt=scrheight-8;
    
    
    /*offsetArray[0] = (scrhgt / LEADCONFIG)+50;
				for (int i = 1; i < LEADCONFIG ; i++) {
                    offsetArray[i] = offsetArray[i - 1]+ (scrhgt / LEADCONFIG);
                    
                }
   */
   
    int offsetlenth=0;
    if(MCDLEAD==15)
    {
        
        offsetlenth=17;
    }
    else if(MCDLEAD==12)
    {
        offsetlenth=13;
        
    }
    switch (LEAD_COUNT) {
        case 7:
           
            offsetArray[0] =  (scrhgt / 8)+50;
            for (int i = 1; i < offsetlenth  ; i++) {
                offsetArray[i] = offsetArray[i - 1]
                + (int) (scrhgt /8);
            }
            offsetArray[12] = offsetArray[6];
            break;
        case 8:
            offsetArray[0] =  (scrhgt / 9)+50;
            for (int i = 1; i < offsetlenth; i++) {
                offsetArray[i] = offsetArray[i - 1]
                + (int) (scrhgt /9);
            }
            offsetArray[12] = offsetArray[7];
            break;
        case 13:
            offsetArray[0] =(scrhgt /14)+25;
            for (int i = 1; i < offsetlenth; i++) {
                offsetArray[i] = offsetArray[i - 1]
                + (int) (scrhgt /14);
            }
            break;
        case 16:
            //				offsetArray = new int[17];
            offsetArray[0] = (scrhgt /17)+50;
            for (int i = 1; i < offsetlenth; i++) {
                offsetArray[i] = offsetArray[i - 1]
                + (int) (scrhgt / 17);
            }
            break;
        default:
            break;
    }
    
    NSMutableArray *ecgtext;
    if(LEAD_COUNT==7)
    {
        if(RESPIRATION==0)
            ecgtext=[[NSMutableArray alloc]initWithObjects:@" I",@" II",@" III",@"aVR",@"aVL",@"aVF", nil];
        else
            ecgtext=[[NSMutableArray alloc]initWithObjects:@" I",@" II",@" III",@"aVR",@"aVL",@"aVF",@"Resp", nil];
    }
    else if (LEAD_COUNT==8)
    {
        if(RESPIRATION==0)
            ecgtext=[[NSMutableArray alloc]initWithObjects:@" I",@" II",@" III",@"aVR",@"aVL",@"aVF",@"V1", nil];
        else
            ecgtext=[[NSMutableArray alloc]initWithObjects:@" I",@" II",@" III",@"aVR",@"aVL",@"aVF",@"V1",@"Resp", nil];
    }
    else if (LEAD_COUNT==13)
    {
        if(RESPIRATION==0)
            ecgtext=[[NSMutableArray alloc]initWithObjects:@" I",@" II",@" III",@"aVR",@"aVL",@"aVF",@"V1",@"V2",@"V3",@"V4",@"V5",@"V6", nil];
        else
            ecgtext=[[NSMutableArray alloc]initWithObjects:@" I",@" II",@" III",@"aVR",@"aVL",@"aVF",@"V1",@"V2",@"V3",@"V4",@"V5",@"V6",@"Resp", nil];
    }
    else
    {
        if(RESPIRATION==0)
            ecgtext=[[NSMutableArray alloc]initWithObjects:@" I",@" II",@" III",@"aVR",@"aVL",@"aVF",@"V1",@"V2",@"V3",@"V4",@"V5",@"V6",@"V7",@"V3R",@"V4R", nil];
        else
            ecgtext=[[NSMutableArray alloc]initWithObjects:@" I",@" II",@" III",@"aVR",@"aVL",@"aVF",@"V1",@"V2",@"V3",@"V4",@"V5",@"V6",@"V7",@"V3R",@"V4R",@"Resp", nil];
    }
    
    
    
    for(int i=0;i<LEADCONFIG;i++)
    {
        NSString* textToDraw = ecgtext[i];
        
        CFStringRef stringRef = (__bridge CFStringRef)textToDraw;
        // Prepare the text using a Core Text Framesetter
        CFAttributedStringRef currentText = CFAttributedStringCreate(NULL, stringRef, NULL);
        CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString(currentText);
        
        CGRect frameRect = CGRectMake(10, offsetArray[i]-30, 300, 50);
        CGMutablePathRef framePath = CGPathCreateMutable();
        CGPathAddRect(framePath, NULL, frameRect);
        
        // Get the frame that will do the rendering.
        CFRange currentRange = CFRangeMake(0,0);
        CTFrameRef frameRef = CTFramesetterCreateFrame(framesetter, currentRange, framePath, NULL);
        
        
        // Get the graphics context.
        CGContextRef ctx = UIGraphicsGetCurrentContext();
        
        CGContextSetRGBFillColor(ctx, 1.0, 1.0, 1.0, 1.0);
        CGContextSelectFont(ctx, "Helvetica", 12,kCGEncodingMacRoman);
        CGAffineTransform xform = CGAffineTransformMake(1.0, 0.0, 0.0, -1.0, 0.0, 0.0);
        CGContextSetTextMatrix(ctx, xform);
        CGContextShowTextAtPoint(ctx, 8, 2, [textToDraw UTF8String], strlen([textToDraw UTF8String]));
        
        // Put the text matrix into a known state. This ensures
        // that no old scaling factors are left in place.
        //CGContextSetTextMatrix(currentContext, CGAffineTransformIdentity);
        
        // Core Text draws from the bottom-left corner up, so flip
        // the current transform prior to drawing.
        //CGContextTranslateCTM(currentContext, 0, 100);
        //CGContextScaleCTM(currentContext, 1.0, -1.0);
        
        // Draw the frame.
        CTFrameDraw(frameRef, ctx);
        
        CFRelease(frameRef);
        CFRelease(stringRef);
        CFRelease(framesetter);
        CGPathRelease(framePath);
    }
    
    cur_x+=50;
    for (int i = 0; i < LEADCONFIG; i++) {
        p_ecg_y[i] = offsetArray[i];
    }
    prev_x=cur_x;
}





+(void)MCD_PutData:(unsigned char *)data_val
{
    if(MCDLEAD==12)
    {
        
        NSInteger ecg_data[13];
        //  unsigned char ECG_saveBuf[30];
        // short anaresult;
        ecg_data[1] = ((data_val[0] << 7 | data_val[1]) - 3000);
        
        ecg_data[2] = ((data_val[2] << 7 | data_val[3]) - 3000);
        
        ecg_data[0] = (ecg_data[1] - ecg_data[2]);
        
        ecg_data[6] = ((data_val[4] << 7 | data_val[5]) - 3000);
        
        ecg_data[7] = ((data_val[6] << 7 | data_val[7]) - 3000);
        
        ecg_data[8] = ((data_val[8] << 7 | data_val[9]) - 3000);
        
        ecg_data[9] = ((data_val[10] << 7 | data_val[11]) - 3000);
        
        ecg_data[10] = ((data_val[12] << 7 | data_val[13]) - 3000);
        
        ecg_data[11] = ((data_val[14] << 7 | data_val[15]) - 3000);
        
        ecg_data[5] = ((ecg_data[1] + ecg_data[2]) / 2);//avf
        
        ecg_data[4] = ((ecg_data[0] - ecg_data[2]) / 2);//avl
        
        ecg_data[3] = ((-ecg_data[0] - ecg_data[1]) / 2);//avr
        
        ecg_data[12] = (data_val[16] & 0xFF) - 90;
         [self plotWave:ecg_data];
       
    }
    else
    {
        NSInteger ecg_data[16];
        //  unsigned char ECG_saveBuf[30];
        // short anaresult;
        ecg_data[1] = ((data_val[0] << 7 | data_val[1]) - 3000);
        
        ecg_data[2] = ((data_val[2] << 7 | data_val[3]) - 3000);
        
        ecg_data[0] = (ecg_data[1] - ecg_data[2]);
        
        ecg_data[6] = ((data_val[4] << 7 | data_val[5]) - 3000);
        
        ecg_data[7] = ((data_val[6] << 7 | data_val[7]) - 3000);
        
        ecg_data[8] = ((data_val[8] << 7 | data_val[9]) - 3000);
        
        ecg_data[9] = ((data_val[10] << 7 | data_val[11]) - 3000);
        
        ecg_data[10] = ((data_val[12] << 7 | data_val[13]) - 3000);
        
        ecg_data[11] = ((data_val[14] << 7 | data_val[15]) - 3000);
        
        ecg_data[5] = ((ecg_data[1] + ecg_data[2]) / 2);//avf
        
        ecg_data[4] = ((ecg_data[0] - ecg_data[2]) / 2);//avl
        
        ecg_data[3] = ((-ecg_data[0] - ecg_data[1]) / 2);//avr
        
        
        
        ecg_data[12] = ((data_val[18] << 7 | data_val[19]) - 3000);//V7
        
        ecg_data[13] = ((data_val[20] << 7 | data_val[21]) - 3000);//V8
        
        ecg_data[14] = ((data_val[22] << 7 | data_val[23]) - 3000);//V9
        
        
        
        ecg_data[15] = (data_val[16] & 0xFF) - 90;
    
        [self plotWave:ecg_data];
    }
    
}

+(void)MCDParser :(unsigned char) in_data
{
    
    
    // parse the data here
    if(in_data > 240)
    {
        control_code_1 = in_data;
        flag_1 = 1;
        no_ofBytes_1 = 0;
    }
    else
    {
        switch(control_code_1)
        {
            case COMMUNICATION_CONTROL_BYTE:
            {
                if(flag_1)
                {
                    sample_12 = in_data;
                    flag_1 = 0;
                    data_counter12 = 0;
                    
                    if(plot_ok_1)
                    {
                        plot_ok_1 = 0;
                        
                        [self MCD_PutData:plot_data_1];
                        
                    }
                }
                else
                {
                    plot_data_1[data_counter12] = in_data;
                    ++data_counter12;
                    
                    if(data_counter12 >= sample_12)
                    {
                        plot_ok_1 = 1;
                        data_counter12 = 0;
                    }
                }
            }
                break;
                
            default:
                break;
        }
    }
}



+(void) resetSlate {
    // reset plot points
    cur_x = 0;
    prev_x = 0;
    for (short i = 0; i < 12; i++) {
        p_ecg_y[i] = 0;
    }
    
    [self drawGrid];
    // draw grid afresh
}

+(void) plotWave :(NSInteger [])in_pt {
    NSInteger ecg_y[17];
   
   
    
    //NSLog(@"%ld",(long)in_pt[6]);
    
    cur_x=cur_x+0.20;
   //cur_x++;
    int end = 15;
    
    /*int offset = 0;
    for (int j = 0; j < end; j++) {
        offset = offsetArray[j];
        
        ecg_y[j] = (short) (offset - (in_pt[j] * 1.0f / 2) / 2.0f);
        
        CGPoint from = CGPointMake(prev_x,  p_ecg_y[j]);
        CGPoint to = CGPointMake(cur_x, ecg_y[j]);
        
        [Plotter drawLineFromPoint:from toPoint:to :0];
        
        
        
        p_ecg_y[j] = ecg_y[j];
        
    }
    
    prev_x = cur_x;*/
    
    
    
    switch (LEAD_COUNT) {
        case 7:
            end = 6;
            break;
        case 8:
            end = 7;
            break;
        case 13:
            end = 12;
            break;
        default:
            break;
    }
    int offset = 0;
    for (int j = 0; j < end; j++) {
        offset = offsetArray[j];
        
        ecg_y[j] = (short)offset-((in_pt[j] * 1.0f / 2) / 5.0f);
     
        
        
        CGPoint from = CGPointMake(prev_x,  p_ecg_y[j]);
        CGPoint to = CGPointMake(cur_x, ecg_y[j]);
        
        
        
       
        
        if([EVENTCODE intValue] ==T_WAVE_VALUE)
        {
            if(j==[EVENTVALUE intValue])
             colorvalue=1;
            else
               colorvalue=0;
            
        }
        else if ([EVENTCODE intValue] ==ST_ELEVATION_VALUES||[EVENTCODE intValue] ==ST_DEPRESSION_VALUES)
        {
            if(j==(roundf([EVENTVALUE intValue]/256)))
                 colorvalue=1;
            else
                 colorvalue=0;
        }
        else
        {
              colorvalue=0;
        }
        
       
        
        [Plotter drawLineFromPoint:from toPoint:to :colorvalue];
       
       
        p_ecg_y[j] = ecg_y[j];
        
    }
    
    if (RESPIRATION == 1) {
        
        
        if (LEADCONFIG == 16) {
            offset = offsetArray[15];
            
            ecg_y[15] = (short) (offset - (in_pt[15] * 1.0f / 2) / 5.0f);
            
            CGPoint from = CGPointMake(prev_x,  p_ecg_y[15]);
            CGPoint to = CGPointMake(cur_x, ecg_y[15]);
            [Plotter drawLineFromPoint:from toPoint:to :colorvalue];
            p_ecg_y[15] = ecg_y[15];
        } else {
            offset = offsetArray[12] ;
            
            ecg_y[12] = (short) (offset - (in_pt[12] * 1.0f / 2) / 5.0f);
            
            CGPoint from = CGPointMake(prev_x,  p_ecg_y[12]);
            CGPoint to = CGPointMake(cur_x, ecg_y[12]);
            [Plotter drawLineFromPoint:from toPoint:to :colorvalue];
            p_ecg_y[12] = ecg_y[12];
        }
    }
    else {
        if(MCDLEAD==15)
        {
            p_ecg_y[15] = ecg_y[end];
        }
        else if(MCDLEAD==12)
        {
            p_ecg_y[12] = ecg_y[end];
        }
        
    }
    
    prev_x = cur_x;
}





+ (void)drawPageNumber:(NSInteger)pageNum
{
    
    
    NSString *pageString = [NSString stringWithFormat:@"Page %ld", (long)pageNum];
    
   // CGSize pageStringSize = [pageString sizeWithAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:12.0f]}];
    UIFont *theFont = [UIFont systemFontOfSize:12];
    CGSize maxSize = CGSizeMake(650, 72);
    

    CGSize pageStringSize = [pageString sizeWithFont:theFont
                                   constrainedToSize:maxSize
                                       lineBreakMode:UILineBreakModeClip];
    CGRect stringRect = CGRectMake(((650.0 - pageStringSize.width) / 2.0),
                                   920.0 + ((72.0 - pageStringSize.height) / 2.0),
                                   pageStringSize.width,
                                   pageStringSize.height);
    
    [pageString drawInRect:stringRect withFont:theFont];
}


@end
