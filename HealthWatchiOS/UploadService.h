//
//  UploadService.h
//  HealthWatch
//
//  Created by METSL MAC MINI on 18/12/15.
//  Copyright © 2015 METSL MAC MINI. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UploadService : NSObject

@property (nonatomic) UIBackgroundTaskIdentifier backgroundTask;
@property (nonatomic) NSInteger frequency;
@property (nonatomic, strong) NSTimer *updateTimer;

- (id) initWithFrequency: (NSInteger) seconds;
- (void) startService;
- (void) doInBackground;
- (void) stopService;

@end
