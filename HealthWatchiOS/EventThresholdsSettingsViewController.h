//
//  EventThresholdsSettingsViewController.h
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 24/06/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//


#import "DoctorSettingTableViewCell.h"
#import "CustomUiPicker.h"

enum
{
 MainCategoryDropDown=0,
 SubCategoryValueDropDown=1
};typedef NSInteger DropDown;

@interface EventThresholdsSettingsViewController : UIViewController<UITableViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UITableViewDataSource>
{
   
    NSMutableArray * Cell_Data_MutableArray ;
    NSArray * Event_Category_Master_NSArray;
    NSArray * Event_Posture_Master_NSArray ;
    NSDictionary * Event_Sub_Category_Master_NSDict ;

    NSMutableDictionary* Main_Updated_NSMutDictonary_Array ;

    NSString * userSelectedMainCategoryEvent;// Will Store the Slug
    NSString * userSelectedMainSinglePostureSlug ; // Will Store Posture the Slug when is only once selected
    NSMutableArray * userSelectedPosturesArray; // Will Store the Array of Slug
    int currentSelectedRowIdForMainEventCat;
     UIColor*blueBorderColorForTab;
    UIColor *yellowUIColor;
    UIColor *redUIColor ;
    NSMutableArray * cellRowStatus;
    
    NSMutableArray *pickcom1data;
      NSMutableArray *pickcom2data;
      NSMutableArray *pickcom3data;
    NSArray *pickswtchdata;
    NSArray *Inputtypearray;
    NSArray * totaldata;
    
    int direction;
    int shakes;
    UILabel *lblspin;
    NSUInteger indexpthsec;
    NSUInteger indexrw;
   
    
    NSString * ddpickvalue;
    NSString * ydpickvalue;
    NSString * rdpickvalue;
    NSString *globalposturecode;
    
    NSString * patientid;
    NSString *deviceid;
    NSString *doctorname;
    
    int universalcellstatus;
    
      BOOL standalone_Mode;
}

//All IBOutlet

@property (nonatomic)  DropDown dropDownInUse;
@property (nonatomic, strong) IBOutlet UIView * headerBlueView;

@property (nonatomic, strong) IBOutlet UIView * rounderViewUnderHeader;
@property (nonatomic, strong) IBOutlet UIView *tabDivider1;
@property (nonatomic, strong) IBOutlet UIView *tabDivider2;
@property (nonatomic, strong) IBOutlet UIView *tabDivider3;
@property (nonatomic, strong) IBOutlet UIView *tabDivider4;


@property (nonatomic, strong) IBOutlet UIButton *layingButton;
@property (nonatomic, strong) IBOutlet UIButton * standingSittingButton;
@property (nonatomic, strong) IBOutlet UIButton *  walkingButton;
@property (nonatomic, strong) IBOutlet UIButton *  runningButton;

@property (nonatomic, strong) IBOutlet UIButton *  lockunlocButton;

@property (nonatomic, strong) IBOutlet UIButton *  selectMainEventThresholdButton;

// Setting Screen Body Properties

@property (nonatomic, strong) IBOutlet UITableView *tableViewDoctorSettings;
@property (nonatomic, strong) IBOutlet DoctorSettingTableViewCell * doctorSettingTableViewCell;


// Header Elements / Property


// *****   Main Cat Drop down elements ****

@property (nonatomic, strong) IBOutlet UIView * mainCatDropDownBlockView;

@property (nonatomic, strong) IBOutlet UIPickerView * mainCatPickerControl;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView * spinner;


-(void)UpdateThresholdvalue:(int)data;

//-(NSMutableDictionary*)getClubedSettingArray:(NSDictionary*)eachSubEventArray (NSString*)postureSlugIs  (NSString*):mainEventSlugIs;

@end
