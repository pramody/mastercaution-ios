//
//  OneTimeViewController.m
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 12/06/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import "OneTimeViewController.h"


#import "PatientDetails.h"
#import "MCDSettings.h"
#import "ThresholdSett.h"
#import "SuspensionSettings.h"
#import "DoctorThreshold.h"
#define NULL_TO_NIL(obj) ({ __typeof__ (obj) __obj = (obj); __obj == [NSNull null] ? nil : obj; })
#import "codes.h"

@interface OneTimeViewController ()

@end

@implementation OneTimeViewController
@synthesize submitButton,serverActivationKeyTextField,serverDomainTextField,spinnerView,indicatorMainBlockView,managedObjectContext,standaloneButton;
 BOOL isThere=true;
- (void)viewDidLoad {
    [super viewDidLoad];
    
        AppDelegate * appDelegate = (AppDelegate *)UIApplication.sharedApplication.delegate ;
        appDelegate.orientationKey = @"PRO";
        [appDelegate forceOrientationTakePlace:appDelegate.orientationKey];
        
        submitButton.layer.borderWidth  =   1.5;
        submitButton.layer.borderColor  =   UIColor.whiteColor.CGColor;
        
        submitButton.layer.cornerRadius =   7.0;
        submitButton.layer.masksToBounds = true;
    
        standaloneButton.layer.borderWidth  =   1.5;
        standaloneButton.layer.borderColor  =   UIColor.whiteColor.CGColor;
    
        standaloneButton.layer.cornerRadius =   7.0;
        standaloneButton.layer.masksToBounds = true;
    
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                       initWithTarget:self
                                       action:@selector(hideKeyboard)];
        
        [self.view addGestureRecognizer:tap];
        
        id delegate = [[UIApplication sharedApplication] delegate];
        self.managedObjectContext = [delegate managedObjectContext];
        // Add it as a subview
        [self checkinternet];
   
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL hasloadBefore = [defaults boolForKey:@"Firstloaddata"];
    
    if (!hasloadBefore)
    {
        [self defaultdatabasevalue];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setBool:YES forKey:@"Firstloaddata"];
        [defaults synchronize];

    }
   
   
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
   
   
    submitButton.layer.borderWidth  =   1.5;
    submitButton.layer.borderColor  =   UIColor.whiteColor.CGColor;
    
    submitButton.layer.cornerRadius =   7.0;
    submitButton.layer.masksToBounds = true;
    
   
    AppDelegate * appDelegate = (AppDelegate *)UIApplication.sharedApplication.delegate ;
    appDelegate.orientationKey = @"PRO";
    [appDelegate forceOrientationTakePlace:appDelegate.orientationKey];
    


}

-(IBAction)StandaloneAction:(id)sender
{
   
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        [MTPopupWindow setWindowMargin:CGSizeMake(300,300)];
    }
   else
       [MTPopupWindow setWindowMargin:CGSizeMake(50,50)];
    
    MTPopupWindow *popup = [[MTPopupWindow alloc] init];
    popup.delegate = self;
    popup.fileName = @"agreement.html";
    popup.Type=@"standalone";
    [popup show];
      
   // [MTPopupWindow showWindowWithHTMLFile:@"agreement.html" insideView:self.view];
    
   

}


- (void) didCloseMTPopupWindow:(MTPopupWindow*)sender {
   //normal call
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:NO forKey:@"ActiveStandalonemode"];
    [defaults synchronize];
    
    
    
    
    [self startLoad];
    [self serverAuthenticationWebCall];
}

- (void) willCloseMTPopupWindow:(MTPopupWindow*)sender {
    
    //standalone call
     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
     [defaults setBool:YES forKey:@"FirstRun"];
     [defaults setBool:YES forKey:@"ActiveStandalonemode"];
     [defaults synchronize];
     
     
     
     if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
     {
     UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Mainstoryboard_iPad" bundle:nil];
     ECSlidingViewController * controller = (ECSlidingViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ECSlidingStoryboardId"];
     [self presentViewController:controller animated:YES completion:nil];
     
     }
     else
     {
     UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
     ECSlidingViewController * controller = (ECSlidingViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ECSlidingStoryboardId"];
     [self presentViewController:controller animated:YES completion:nil];
     }
}


-(IBAction)submitAction:(id)sender
{

    
    BOOL isValid  = true;
    NSString * title;
    NSString * message;
    
    
    title       =   @"Validation Alert";
    
    
    CommonHelper *comphelp=[[CommonHelper alloc] init];
    NSString * domainName  = [comphelp trimTheString:serverDomainTextField.text];
    [self saveDataInUserDefault:domainName :@"DomainName"];
     [self saveDataInUserDefault:serverActivationKeyTextField.text :@"ActivationKEY"];
    
    domainName=(domainName.length>0)?[NSString stringWithFormat:@"https://%@",domainName]:@"";
    NSString * activationKey   = [comphelp trimTheString:serverActivationKeyTextField.text];
    
    if( domainName.length == 0 && activationKey.length == 0 )
    {
        isValid     =   false;
        
        message     =   @"Both! Domain and Activation key fields are required";
        serverActivationKeyTextField.text   =   @"";
        serverDomainTextField.text          =   @"";
        
        
    }
    else if ( domainName.length == 0 )
    {
        isValid     =   false;
        message     =   @"Domain is required";
        serverDomainTextField.text          =   @"";
        //ObjectiveCHelper.isValidIpAddress(serverDomainTextField.text) == false
        
    }
    
    else if ( activationKey.length == 0 )
    {
        isValid     =   false;
        message     =   @"Activation Key is required";
        serverActivationKeyTextField.text   = @"";
        
    }
    
    
    if (isValid == false)
    {
        UIAlertView * alertview=[[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alertview show];
        
    }
    else
    {
       [self hideKeyboard];
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            [MTPopupWindow setWindowMargin:CGSizeMake(300,300)];
        }
        else
            [MTPopupWindow setWindowMargin:CGSizeMake(50,50)];
        
        MTPopupWindow *popup = [[MTPopupWindow alloc] init];
        popup.delegate = self;
        popup.fileName = @"agreement.html";
        popup.Type=@"normal";
        [popup show];
        
    }
    
    
}



-(void) startLoad
{
    [self.spinnerView startLoading:self.indicatorMainBlockView];
}

-(void) stopLoad
{
    [self.spinnerView stopLoading:self.indicatorMainBlockView];
}







-(void) saveDataInUserDefault:(id)dataToSave  :(NSString *)forKey
{
    NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
    [userDefault setObject:dataToSave forKey:forKey];
    [userDefault synchronize];
    
    //NSString * result  = [userDefault objectForKey:forKey];
  // NSLog(@" User Default Current Save was =%@",result);
}




-(void)defaultdatabasevalue
{
    
    NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
    [TempDefault setObject:@"CELSIUS" forKey:@"TEMPERATURE"];
     [TempDefault synchronize];
    
    
    //patient record
    [self defaultpatientvalue];
    
    //MCD Settings
    [self defaultmcdvalue];
    //suspension settings
      [self suspensionObject:[NSNumber numberWithInt:NARROW_QRS_REGULAR_D_DESCRIPTION] :[NSNumber numberWithInt:300]];
      [self suspensionObject:[NSNumber numberWithInt:WIDE_QRS_REGULAR_D_DESCRIPTION] :[NSNumber numberWithInt:300]];
      [self suspensionObject:[NSNumber numberWithInt:BRADICARDIA_REGULAR_D_DESCRIPTION] :[NSNumber numberWithInt:300]];
      [self suspensionObject:[NSNumber numberWithInt:PAUSE] :[NSNumber numberWithInt:300]];
      [self suspensionObject:[NSNumber numberWithInt:PVC_VALUE] :[NSNumber numberWithInt:300]];
      [self suspensionObject:[NSNumber numberWithInt:ASYSTOLE_VALUE] :[NSNumber numberWithInt:300]];
      [self suspensionObject:[NSNumber numberWithInt:BIGEMINY] :[NSNumber numberWithInt:300]];
      [self suspensionObject:[NSNumber numberWithInt:TRIGEMINY] :[NSNumber numberWithInt:300]];
      [self suspensionObject:[NSNumber numberWithInt:COUPLET] :[NSNumber numberWithInt:300]];
      [self suspensionObject:[NSNumber numberWithInt:TRIPLET] :[NSNumber numberWithInt:300]];
    
      [self suspensionObject:[NSNumber numberWithInt:ST_ELEVATION_VALUES] :[NSNumber numberWithInt:300]];
      [self suspensionObject:[NSNumber numberWithInt:ST_DEPRESSION_VALUES] :[NSNumber numberWithInt:300]];
      [self suspensionObject:[NSNumber numberWithInt:PROLNGED_QT_VALUES] :[NSNumber numberWithInt:300]];
      [self suspensionObject:[NSNumber numberWithInt:QRS_WIDE_WIDTH] :[NSNumber numberWithInt:300]];
      [self suspensionObject:[NSNumber numberWithInt:T_WAVE_VALUE] :[NSNumber numberWithInt:300]];
    
      [self suspensionObject:[NSNumber numberWithInt:TEMPERATURE_VALUE] :[NSNumber numberWithInt:300]];
      [self suspensionObject:[NSNumber numberWithInt:LOW_RESPIRATION_RATE] :[NSNumber numberWithInt:300]];
      [self suspensionObject:[NSNumber numberWithInt:APNEA_VALUE] :[NSNumber numberWithInt:300]];
      [self suspensionObject:[NSNumber numberWithInt:NO_MOTION_VALUE] :[NSNumber numberWithInt:300]];
      [self suspensionObject:[NSNumber numberWithInt:FALL_EVENT_VALUE] :[NSNumber numberWithInt:300]];
    
    
           //thresholdvalue
    //InsertDefaultIschemiaSettings
    [self GeneralThresholdObject:[NSNumber numberWithInt:ST_ELEVATION_VALUES] :[NSNumber numberWithInt:0] :@"4" :@"5"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:ST_ELEVATION_VALUES] :[NSNumber numberWithInt:1] :@"4" :@"5"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:ST_ELEVATION_VALUES] :[NSNumber numberWithInt:2] :@"4" :@"5"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:ST_ELEVATION_VALUES] :[NSNumber numberWithInt:3] :@"4" :@"5"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:ST_ELEVATION_VALUES] :[NSNumber numberWithInt:4] :@"4" :@"5"];
    
    [self GeneralThresholdObject:[NSNumber numberWithInt:ST_DEPRESSION_VALUES] :[NSNumber numberWithInt:0] :@"3" :@"4"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:ST_DEPRESSION_VALUES] :[NSNumber numberWithInt:1] :@"3" :@"4"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:ST_DEPRESSION_VALUES] :[NSNumber numberWithInt:2] :@"3" :@"4"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:ST_DEPRESSION_VALUES] :[NSNumber numberWithInt:3] :@"3" :@"4"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:ST_DEPRESSION_VALUES] :[NSNumber numberWithInt:4] :@"3" :@"4"];
    
    [self GeneralThresholdObject:[NSNumber numberWithInt:PROLNGED_QT_VALUES] :[NSNumber numberWithInt:0] :@"460" :@"470"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:PROLNGED_QT_VALUES] :[NSNumber numberWithInt:1] :@"460" :@"470"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:PROLNGED_QT_VALUES] :[NSNumber numberWithInt:2] :@"460" :@"470"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:PROLNGED_QT_VALUES] :[NSNumber numberWithInt:3] :@"460" :@"470"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:PROLNGED_QT_VALUES] :[NSNumber numberWithInt:4] :@"460" :@"470"];
   
    [self GeneralThresholdObject:[NSNumber numberWithInt:QRS_WIDTH_VALUES] :[NSNumber numberWithInt:0] :@"60" :@"50"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:QRS_WIDTH_VALUES] :[NSNumber numberWithInt:1] :@"60" :@"50"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:QRS_WIDTH_VALUES] :[NSNumber numberWithInt:2] :@"60" :@"50"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:QRS_WIDTH_VALUES] :[NSNumber numberWithInt:3] :@"60" :@"50"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:QRS_WIDTH_VALUES] :[NSNumber numberWithInt:4] :@"60" :@"50"];
    
    [self GeneralThresholdObject:[NSNumber numberWithInt:QRS_WIDE_WIDTH] :[NSNumber numberWithInt:0] :@"140" :@"150"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:QRS_WIDE_WIDTH] :[NSNumber numberWithInt:1] :@"140" :@"150"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:QRS_WIDE_WIDTH] :[NSNumber numberWithInt:2] :@"140" :@"150"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:QRS_WIDE_WIDTH] :[NSNumber numberWithInt:3] :@"140" :@"150"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:QRS_WIDE_WIDTH] :[NSNumber numberWithInt:4] :@"140" :@"150"];
    
    [self GeneralThresholdObject:[NSNumber numberWithInt:ST_SUSPENSION] :[NSNumber numberWithInt:0] :@"100" :@"0"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:ST_SUSPENSION] :[NSNumber numberWithInt:1] :@"100" :@"0"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:ST_SUSPENSION] :[NSNumber numberWithInt:2] :@"100" :@"0"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:ST_SUSPENSION] :[NSNumber numberWithInt:3] :@"100" :@"0"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:ST_SUSPENSION] :[NSNumber numberWithInt:4] :@"100" :@"0"];
    
    [self GeneralThresholdObject:[NSNumber numberWithInt:T_WAVE_VALUE] :[NSNumber numberWithInt:0] :@"0" :@"0"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:T_WAVE_VALUE] :[NSNumber numberWithInt:1] :@"0" :@"0"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:T_WAVE_VALUE] :[NSNumber numberWithInt:2] :@"0" :@"0"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:T_WAVE_VALUE] :[NSNumber numberWithInt:3] :@"0" :@"0"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:T_WAVE_VALUE] :[NSNumber numberWithInt:4] :@"0" :@"0"];
    
    
    //InsertDefaultDoctorIschemiaSettings
    [self DoctorThresholdObject:[NSNumber numberWithInt:ST_ELEVATION_VALUES] :[NSNumber numberWithInt:0] :@"2"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:ST_ELEVATION_VALUES] :[NSNumber numberWithInt:1] :@"2"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:ST_ELEVATION_VALUES] :[NSNumber numberWithInt:2] :@"2"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:ST_ELEVATION_VALUES] :[NSNumber numberWithInt:3] :@"2"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:ST_ELEVATION_VALUES] :[NSNumber numberWithInt:4] :@"2"];
    
    [self DoctorThresholdObject:[NSNumber numberWithInt:ST_DEPRESSION_VALUES] :[NSNumber numberWithInt:0] :@"2"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:ST_DEPRESSION_VALUES] :[NSNumber numberWithInt:1] :@"2"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:ST_DEPRESSION_VALUES] :[NSNumber numberWithInt:2] :@"2"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:ST_DEPRESSION_VALUES] :[NSNumber numberWithInt:3] :@"2"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:ST_DEPRESSION_VALUES] :[NSNumber numberWithInt:4] :@"2"];
    
    [self DoctorThresholdObject:[NSNumber numberWithInt:PROLNGED_QT_VALUES] :[NSNumber numberWithInt:0] :@"450"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:PROLNGED_QT_VALUES] :[NSNumber numberWithInt:1] :@"450"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:PROLNGED_QT_VALUES] :[NSNumber numberWithInt:2] :@"450"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:PROLNGED_QT_VALUES] :[NSNumber numberWithInt:3] :@"450"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:PROLNGED_QT_VALUES] :[NSNumber numberWithInt:4] :@"450"];
    
    [self DoctorThresholdObject:[NSNumber numberWithInt:QRS_WIDTH_VALUES] :[NSNumber numberWithInt:0] :@"70"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:QRS_WIDTH_VALUES] :[NSNumber numberWithInt:1] :@"70"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:QRS_WIDTH_VALUES] :[NSNumber numberWithInt:2] :@"70"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:QRS_WIDTH_VALUES] :[NSNumber numberWithInt:3] :@"70"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:QRS_WIDTH_VALUES] :[NSNumber numberWithInt:4] :@"70"];
    
    [self DoctorThresholdObject:[NSNumber numberWithInt:QRS_WIDE_WIDTH] :[NSNumber numberWithInt:0] :@"130"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:QRS_WIDE_WIDTH] :[NSNumber numberWithInt:1] :@"130"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:QRS_WIDE_WIDTH] :[NSNumber numberWithInt:2] :@"130"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:QRS_WIDE_WIDTH] :[NSNumber numberWithInt:3] :@"130"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:QRS_WIDE_WIDTH] :[NSNumber numberWithInt:4] :@"130"];
    
    [self DoctorThresholdObject:[NSNumber numberWithInt:ST_SUSPENSION] :[NSNumber numberWithInt:0] :@"20"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:ST_SUSPENSION] :[NSNumber numberWithInt:1] :@"20"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:ST_SUSPENSION] :[NSNumber numberWithInt:2] :@"20"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:ST_SUSPENSION] :[NSNumber numberWithInt:3] :@"20"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:ST_SUSPENSION] :[NSNumber numberWithInt:4] :@"20"];
    
    [self DoctorThresholdObject:[NSNumber numberWithInt:T_WAVE_VALUE] :[NSNumber numberWithInt:0] :@"0"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:T_WAVE_VALUE] :[NSNumber numberWithInt:1] :@"0"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:T_WAVE_VALUE] :[NSNumber numberWithInt:2] :@"0"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:T_WAVE_VALUE] :[NSNumber numberWithInt:3] :@"0"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:T_WAVE_VALUE] :[NSNumber numberWithInt:4] :@"0"];
    
    //InsertDefaultIArythmiaSettings
    [self GeneralThresholdObject:[NSNumber numberWithInt:NARROW_QRS_REGULAR_D_DESCRIPTION] :[NSNumber numberWithInt:0] :@"160" :@"180"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:NARROW_QRS_REGULAR_D_DESCRIPTION] :[NSNumber numberWithInt:1] :@"160" :@"180"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:NARROW_QRS_REGULAR_D_DESCRIPTION] :[NSNumber numberWithInt:2] :@"160" :@"180"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:NARROW_QRS_REGULAR_D_DESCRIPTION] :[NSNumber numberWithInt:3] :@"160" :@"180"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:NARROW_QRS_REGULAR_D_DESCRIPTION] :[NSNumber numberWithInt:4] :@"160" :@"180"];
    
    [self GeneralThresholdObject:[NSNumber numberWithInt:WIDE_QRS_REGULAR_D_DESCRIPTION] :[NSNumber numberWithInt:0] :@"0" :@"0"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:WIDE_QRS_REGULAR_D_DESCRIPTION] :[NSNumber numberWithInt:1] :@"0" :@"0"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:WIDE_QRS_REGULAR_D_DESCRIPTION] :[NSNumber numberWithInt:2] :@"0" :@"0"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:WIDE_QRS_REGULAR_D_DESCRIPTION] :[NSNumber numberWithInt:3] :@"0" :@"0"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:WIDE_QRS_REGULAR_D_DESCRIPTION] :[NSNumber numberWithInt:4] :@"0" :@"0"];
    
    [self GeneralThresholdObject:[NSNumber numberWithInt:TACHYCARDIA] :[NSNumber numberWithInt:0] :@"160" :@"180"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:TACHYCARDIA] :[NSNumber numberWithInt:1] :@"160" :@"180"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:TACHYCARDIA] :[NSNumber numberWithInt:2] :@"160" :@"180"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:TACHYCARDIA] :[NSNumber numberWithInt:3] :@"160" :@"180"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:TACHYCARDIA] :[NSNumber numberWithInt:4] :@"160" :@"180"];
    
    [self GeneralThresholdObject:[NSNumber numberWithInt:BRADICARDIA_REGULAR_D_DESCRIPTION] :[NSNumber numberWithInt:0] :@"30" :@"25"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:BRADICARDIA_REGULAR_D_DESCRIPTION] :[NSNumber numberWithInt:1] :@"30" :@"25"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:BRADICARDIA_REGULAR_D_DESCRIPTION] :[NSNumber numberWithInt:2] :@"30" :@"25"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:BRADICARDIA_REGULAR_D_DESCRIPTION] :[NSNumber numberWithInt:3] :@"30" :@"25"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:BRADICARDIA_REGULAR_D_DESCRIPTION] :[NSNumber numberWithInt:4] :@"30" :@"25"];
    
    [self GeneralThresholdObject:[NSNumber numberWithInt:PVC_VALUE] :[NSNumber numberWithInt:0] :@"12" :@"15"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:PVC_VALUE] :[NSNumber numberWithInt:1] :@"12" :@"15"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:PVC_VALUE] :[NSNumber numberWithInt:2] :@"12" :@"15"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:PVC_VALUE] :[NSNumber numberWithInt:3] :@"12" :@"15"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:PVC_VALUE] :[NSNumber numberWithInt:4] :@"12" :@"15"];

    [self GeneralThresholdObject:[NSNumber numberWithInt:ASYSTOLE_VALUE] :[NSNumber numberWithInt:0] :@"12" :@"15"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:ASYSTOLE_VALUE] :[NSNumber numberWithInt:1] :@"12" :@"15"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:ASYSTOLE_VALUE] :[NSNumber numberWithInt:2] :@"12" :@"15"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:ASYSTOLE_VALUE] :[NSNumber numberWithInt:3] :@"12" :@"15"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:ASYSTOLE_VALUE] :[NSNumber numberWithInt:4] :@"12" :@"15"];
    
    [self GeneralThresholdObject:[NSNumber numberWithInt:BIGEMINY] :[NSNumber numberWithInt:0] :@"8" :@"10"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:BIGEMINY] :[NSNumber numberWithInt:1] :@"8" :@"10"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:BIGEMINY] :[NSNumber numberWithInt:2] :@"8" :@"10"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:BIGEMINY] :[NSNumber numberWithInt:3] :@"8" :@"10"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:BIGEMINY] :[NSNumber numberWithInt:4] :@"8" :@"10"];
    
    [self GeneralThresholdObject:[NSNumber numberWithInt:TRIGEMINY] :[NSNumber numberWithInt:0] :@"8" :@"10"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:TRIGEMINY] :[NSNumber numberWithInt:1] :@"8" :@"10"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:TRIGEMINY] :[NSNumber numberWithInt:2] :@"8" :@"10"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:TRIGEMINY] :[NSNumber numberWithInt:3] :@"8" :@"10"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:TRIGEMINY] :[NSNumber numberWithInt:4] :@"8" :@"10"];
    
    [self GeneralThresholdObject:[NSNumber numberWithInt:COUPLET] :[NSNumber numberWithInt:0] :@"8" :@"10"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:COUPLET] :[NSNumber numberWithInt:1] :@"8" :@"10"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:COUPLET] :[NSNumber numberWithInt:2] :@"8" :@"10"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:COUPLET] :[NSNumber numberWithInt:3] :@"8" :@"10"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:COUPLET] :[NSNumber numberWithInt:4] :@"8" :@"10"];
    
    [self GeneralThresholdObject:[NSNumber numberWithInt:TRIPLET] :[NSNumber numberWithInt:0] :@"8" :@"10"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:TRIPLET] :[NSNumber numberWithInt:1] :@"8" :@"10"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:TRIPLET] :[NSNumber numberWithInt:2] :@"8" :@"10"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:TRIPLET] :[NSNumber numberWithInt:3] :@"8" :@"10"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:TRIPLET] :[NSNumber numberWithInt:4] :@"8" :@"10"];
    
    [self GeneralThresholdObject:[NSNumber numberWithInt:WIDE_QRS_REGULAR_RELATIVE_DESCRIPTION] :[NSNumber numberWithInt:0] :@"20" :@"30"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:WIDE_QRS_REGULAR_RELATIVE_DESCRIPTION] :[NSNumber numberWithInt:1] :@"20" :@"30"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:WIDE_QRS_REGULAR_RELATIVE_DESCRIPTION] :[NSNumber numberWithInt:2] :@"20" :@"30"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:WIDE_QRS_REGULAR_RELATIVE_DESCRIPTION] :[NSNumber numberWithInt:3] :@"20" :@"30"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:WIDE_QRS_REGULAR_RELATIVE_DESCRIPTION] :[NSNumber numberWithInt:4] :@"20" :@"30"];
    
    [self GeneralThresholdObject:[NSNumber numberWithInt:NARROW_QRS_REGULAR_RELATIVE_DESCRIPTION] :[NSNumber numberWithInt:0] :@"20" :@"30"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:NARROW_QRS_REGULAR_RELATIVE_DESCRIPTION] :[NSNumber numberWithInt:1] :@"20" :@"30"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:NARROW_QRS_REGULAR_RELATIVE_DESCRIPTION] :[NSNumber numberWithInt:2] :@"20" :@"30"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:NARROW_QRS_REGULAR_RELATIVE_DESCRIPTION] :[NSNumber numberWithInt:3] :@"20" :@"30"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:NARROW_QRS_REGULAR_RELATIVE_DESCRIPTION] :[NSNumber numberWithInt:4] :@"20" :@"30"];
    
    [self GeneralThresholdObject:[NSNumber numberWithInt:BRADICARDIA_REGULAR_RELATIVE_DESCRIPTION] :[NSNumber numberWithInt:0] :@"30" :@"20"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:BRADICARDIA_REGULAR_RELATIVE_DESCRIPTION] :[NSNumber numberWithInt:1] :@"30" :@"20"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:BRADICARDIA_REGULAR_RELATIVE_DESCRIPTION] :[NSNumber numberWithInt:2] :@"30" :@"20"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:BRADICARDIA_REGULAR_RELATIVE_DESCRIPTION] :[NSNumber numberWithInt:3] :@"30" :@"20"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:BRADICARDIA_REGULAR_RELATIVE_DESCRIPTION] :[NSNumber numberWithInt:4] :@"30" :@"20"];
    
    [self GeneralThresholdObject:[NSNumber numberWithInt:PAUSE] :[NSNumber numberWithInt:0] :@"5" :@"7"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:PAUSE] :[NSNumber numberWithInt:1] :@"5" :@"7"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:PAUSE] :[NSNumber numberWithInt:2] :@"5" :@"7"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:PAUSE] :[NSNumber numberWithInt:3] :@"5" :@"7"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:PAUSE] :[NSNumber numberWithInt:4] :@"5" :@"7"];
    
    [self GeneralThresholdObject:[NSNumber numberWithInt:HR_SUSPENSION] :[NSNumber numberWithInt:0] :@"19" :@"20"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:HR_SUSPENSION] :[NSNumber numberWithInt:1] :@"19" :@"20"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:HR_SUSPENSION] :[NSNumber numberWithInt:2] :@"19" :@"20"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:HR_SUSPENSION] :[NSNumber numberWithInt:3] :@"19" :@"20"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:HR_SUSPENSION] :[NSNumber numberWithInt:4] :@"19" :@"20"];
    
    //InsertDefaultDoctorIArythmiaSettings
    [self DoctorThresholdObject:[NSNumber numberWithInt:NARROW_QRS_REGULAR_D_DESCRIPTION] :[NSNumber numberWithInt:0] :@"130"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:NARROW_QRS_REGULAR_D_DESCRIPTION] :[NSNumber numberWithInt:1] :@"130"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:NARROW_QRS_REGULAR_D_DESCRIPTION] :[NSNumber numberWithInt:2] :@"130"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:NARROW_QRS_REGULAR_D_DESCRIPTION] :[NSNumber numberWithInt:3] :@"130"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:NARROW_QRS_REGULAR_D_DESCRIPTION] :[NSNumber numberWithInt:4] :@"130"];
    
    [self DoctorThresholdObject:[NSNumber numberWithInt:WIDE_QRS_REGULAR_D_DESCRIPTION] :[NSNumber numberWithInt:0] :@"130"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:WIDE_QRS_REGULAR_D_DESCRIPTION] :[NSNumber numberWithInt:1] :@"130"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:WIDE_QRS_REGULAR_D_DESCRIPTION] :[NSNumber numberWithInt:2] :@"130"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:WIDE_QRS_REGULAR_D_DESCRIPTION] :[NSNumber numberWithInt:3] :@"130"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:WIDE_QRS_REGULAR_D_DESCRIPTION] :[NSNumber numberWithInt:4] :@"130"];
    
    [self DoctorThresholdObject:[NSNumber numberWithInt:TACHYCARDIA] :[NSNumber numberWithInt:0] :@"120"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:TACHYCARDIA] :[NSNumber numberWithInt:1] :@"120"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:TACHYCARDIA] :[NSNumber numberWithInt:2] :@"120"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:TACHYCARDIA] :[NSNumber numberWithInt:3] :@"120"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:TACHYCARDIA] :[NSNumber numberWithInt:4] :@"120"];
    
    [self DoctorThresholdObject:[NSNumber numberWithInt:BRADICARDIA_REGULAR_D_DESCRIPTION] :[NSNumber numberWithInt:0] :@"40"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:BRADICARDIA_REGULAR_D_DESCRIPTION] :[NSNumber numberWithInt:1] :@"40"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:BRADICARDIA_REGULAR_D_DESCRIPTION] :[NSNumber numberWithInt:2] :@"40"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:BRADICARDIA_REGULAR_D_DESCRIPTION] :[NSNumber numberWithInt:3] :@"40"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:BRADICARDIA_REGULAR_D_DESCRIPTION] :[NSNumber numberWithInt:4] :@"40"];
    
    [self DoctorThresholdObject:[NSNumber numberWithInt:PVC_VALUE] :[NSNumber numberWithInt:0] :@"10"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:PVC_VALUE] :[NSNumber numberWithInt:1] :@"10"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:PVC_VALUE] :[NSNumber numberWithInt:2] :@"10"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:PVC_VALUE] :[NSNumber numberWithInt:3] :@"10"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:PVC_VALUE] :[NSNumber numberWithInt:4] :@"10"];
    
    [self DoctorThresholdObject:[NSNumber numberWithInt:ASYSTOLE_VALUE] :[NSNumber numberWithInt:0] :@"10"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:ASYSTOLE_VALUE] :[NSNumber numberWithInt:1] :@"10"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:ASYSTOLE_VALUE] :[NSNumber numberWithInt:2] :@"10"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:ASYSTOLE_VALUE] :[NSNumber numberWithInt:3] :@"10"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:ASYSTOLE_VALUE] :[NSNumber numberWithInt:4] :@"10"];
    
    [self DoctorThresholdObject:[NSNumber numberWithInt:BIGEMINY] :[NSNumber numberWithInt:0] :@"6"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:BIGEMINY] :[NSNumber numberWithInt:1] :@"6"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:BIGEMINY] :[NSNumber numberWithInt:2] :@"6"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:BIGEMINY] :[NSNumber numberWithInt:3] :@"6"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:BIGEMINY] :[NSNumber numberWithInt:4] :@"6"];
    
    [self DoctorThresholdObject:[NSNumber numberWithInt:TRIGEMINY] :[NSNumber numberWithInt:0] :@"6"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:TRIGEMINY] :[NSNumber numberWithInt:1] :@"6"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:TRIGEMINY] :[NSNumber numberWithInt:2] :@"6"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:TRIGEMINY] :[NSNumber numberWithInt:3] :@"6"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:TRIGEMINY] :[NSNumber numberWithInt:4] :@"6"];
    
    [self DoctorThresholdObject:[NSNumber numberWithInt:COUPLET] :[NSNumber numberWithInt:0] :@"6"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:COUPLET] :[NSNumber numberWithInt:1] :@"6"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:COUPLET] :[NSNumber numberWithInt:2] :@"6"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:COUPLET] :[NSNumber numberWithInt:3] :@"6"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:COUPLET] :[NSNumber numberWithInt:4] :@"6"];
    
    [self DoctorThresholdObject:[NSNumber numberWithInt:TRIPLET] :[NSNumber numberWithInt:0] :@"6"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:TRIPLET] :[NSNumber numberWithInt:1] :@"6"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:TRIPLET] :[NSNumber numberWithInt:2] :@"6"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:TRIPLET] :[NSNumber numberWithInt:3] :@"6"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:TRIPLET] :[NSNumber numberWithInt:4] :@"6"];
    
    [self DoctorThresholdObject:[NSNumber numberWithInt:WIDE_QRS_REGULAR_RELATIVE_DESCRIPTION] :[NSNumber numberWithInt:0] :@"20"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:WIDE_QRS_REGULAR_RELATIVE_DESCRIPTION] :[NSNumber numberWithInt:1] :@"20"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:WIDE_QRS_REGULAR_RELATIVE_DESCRIPTION] :[NSNumber numberWithInt:2] :@"20"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:WIDE_QRS_REGULAR_RELATIVE_DESCRIPTION] :[NSNumber numberWithInt:3] :@"20"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:WIDE_QRS_REGULAR_RELATIVE_DESCRIPTION] :[NSNumber numberWithInt:4] :@"20"];
    
    [self DoctorThresholdObject:[NSNumber numberWithInt:NARROW_QRS_REGULAR_RELATIVE_DESCRIPTION] :[NSNumber numberWithInt:0] :@"20"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:NARROW_QRS_REGULAR_RELATIVE_DESCRIPTION] :[NSNumber numberWithInt:1] :@"20"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:NARROW_QRS_REGULAR_RELATIVE_DESCRIPTION] :[NSNumber numberWithInt:2] :@"20"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:NARROW_QRS_REGULAR_RELATIVE_DESCRIPTION] :[NSNumber numberWithInt:3] :@"20"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:NARROW_QRS_REGULAR_RELATIVE_DESCRIPTION] :[NSNumber numberWithInt:4] :@"20"];
    
    [self DoctorThresholdObject:[NSNumber numberWithInt:BRADICARDIA_REGULAR_RELATIVE_DESCRIPTION] :[NSNumber numberWithInt:0] :@"20"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:BRADICARDIA_REGULAR_RELATIVE_DESCRIPTION] :[NSNumber numberWithInt:1] :@"20"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:BRADICARDIA_REGULAR_RELATIVE_DESCRIPTION] :[NSNumber numberWithInt:2] :@"20"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:BRADICARDIA_REGULAR_RELATIVE_DESCRIPTION] :[NSNumber numberWithInt:3] :@"20"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:BRADICARDIA_REGULAR_RELATIVE_DESCRIPTION] :[NSNumber numberWithInt:4] :@"20"];
    
    [self DoctorThresholdObject:[NSNumber numberWithInt:PAUSE] :[NSNumber numberWithInt:0] :@"4"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:PAUSE] :[NSNumber numberWithInt:1] :@"4"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:PAUSE] :[NSNumber numberWithInt:2] :@"4"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:PAUSE] :[NSNumber numberWithInt:3] :@"4"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:PAUSE] :[NSNumber numberWithInt:4] :@"4"];
    
    [self DoctorThresholdObject:[NSNumber numberWithInt:HR_SUSPENSION] :[NSNumber numberWithInt:0] :@"20"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:HR_SUSPENSION] :[NSNumber numberWithInt:1] :@"20"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:HR_SUSPENSION] :[NSNumber numberWithInt:2] :@"20"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:HR_SUSPENSION] :[NSNumber numberWithInt:3] :@"20"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:HR_SUSPENSION] :[NSNumber numberWithInt:4] :@"20"];
    
    
    //InserDefaultDoctorsOtherSettings
    [self DoctorThresholdObject:[NSNumber numberWithInt:TEMPERATURE_VALUE] :[NSNumber numberWithInt:0] :@"40"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:TEMPERATURE_VALUE] :[NSNumber numberWithInt:1] :@"40"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:TEMPERATURE_VALUE] :[NSNumber numberWithInt:2] :@"40"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:TEMPERATURE_VALUE] :[NSNumber numberWithInt:3] :@"40"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:TEMPERATURE_VALUE] :[NSNumber numberWithInt:4] :@"40"];
    
    [self DoctorThresholdObject:[NSNumber numberWithInt:LOW_RESPIRATION_RATE] :[NSNumber numberWithInt:0] :@"12"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:LOW_RESPIRATION_RATE] :[NSNumber numberWithInt:1] :@"12"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:LOW_RESPIRATION_RATE] :[NSNumber numberWithInt:2] :@"12"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:LOW_RESPIRATION_RATE] :[NSNumber numberWithInt:3] :@"12"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:LOW_RESPIRATION_RATE] :[NSNumber numberWithInt:4] :@"12"];
    
    [self DoctorThresholdObject:[NSNumber numberWithInt:HIGH_RESPIRATION_RATE] :[NSNumber numberWithInt:0] :@"90"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:HIGH_RESPIRATION_RATE] :[NSNumber numberWithInt:1] :@"90"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:HIGH_RESPIRATION_RATE] :[NSNumber numberWithInt:2] :@"90"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:HIGH_RESPIRATION_RATE] :[NSNumber numberWithInt:3] :@"90"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:HIGH_RESPIRATION_RATE] :[NSNumber numberWithInt:4] :@"90"];
    
    [self DoctorThresholdObject:[NSNumber numberWithInt:APNEA_VALUE] :[NSNumber numberWithInt:0] :@"20"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:APNEA_VALUE] :[NSNumber numberWithInt:1] :@"20"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:APNEA_VALUE] :[NSNumber numberWithInt:2] :@"20"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:APNEA_VALUE] :[NSNumber numberWithInt:3] :@"20"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:APNEA_VALUE] :[NSNumber numberWithInt:4] :@"20"];
    
    [self DoctorThresholdObject:[NSNumber numberWithInt:NO_MOTION_VALUE] :[NSNumber numberWithInt:0] :@"20"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:NO_MOTION_VALUE] :[NSNumber numberWithInt:1] :@"20"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:NO_MOTION_VALUE] :[NSNumber numberWithInt:2] :@"20"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:NO_MOTION_VALUE] :[NSNumber numberWithInt:3] :@"20"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:NO_MOTION_VALUE] :[NSNumber numberWithInt:4] :@"20"];
    
    [self DoctorThresholdObject:[NSNumber numberWithInt:FALL_EVENT_VALUE] :[NSNumber numberWithInt:0] :@"0"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:FALL_EVENT_VALUE] :[NSNumber numberWithInt:1] :@"0"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:FALL_EVENT_VALUE] :[NSNumber numberWithInt:2] :@"0"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:FALL_EVENT_VALUE] :[NSNumber numberWithInt:3] :@"0"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:FALL_EVENT_VALUE] :[NSNumber numberWithInt:4] :@"0"];
    
    [self DoctorThresholdObject:[NSNumber numberWithInt:LOW_RESPIRATION_AMPLITUDE] :[NSNumber numberWithInt:0] :@"0"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:LOW_RESPIRATION_AMPLITUDE] :[NSNumber numberWithInt:1] :@"0"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:LOW_RESPIRATION_AMPLITUDE] :[NSNumber numberWithInt:2] :@"0"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:LOW_RESPIRATION_AMPLITUDE] :[NSNumber numberWithInt:3] :@"0"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:LOW_RESPIRATION_AMPLITUDE] :[NSNumber numberWithInt:4] :@"0"];
    
    [self DoctorThresholdObject:[NSNumber numberWithInt:HIGH_RESPIRATION_AMPLITUDE] :[NSNumber numberWithInt:0] :@"0"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:HIGH_RESPIRATION_AMPLITUDE] :[NSNumber numberWithInt:1] :@"0"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:HIGH_RESPIRATION_AMPLITUDE] :[NSNumber numberWithInt:2] :@"0"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:HIGH_RESPIRATION_AMPLITUDE] :[NSNumber numberWithInt:3] :@"0"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:HIGH_RESPIRATION_AMPLITUDE] :[NSNumber numberWithInt:4] :@"0"];
    
    [self DoctorThresholdObject:[NSNumber numberWithInt:RESPIRATION_VARIANCE] :[NSNumber numberWithInt:0] :@"0"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:RESPIRATION_VARIANCE] :[NSNumber numberWithInt:1] :@"0"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:RESPIRATION_VARIANCE] :[NSNumber numberWithInt:2] :@"0"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:RESPIRATION_VARIANCE] :[NSNumber numberWithInt:3] :@"0"];
    [self DoctorThresholdObject:[NSNumber numberWithInt:RESPIRATION_VARIANCE] :[NSNumber numberWithInt:4] :@"0"];
    
    
    //InsertDefaultOtherArrhythmiaSettings
    [self GeneralThresholdObject:[NSNumber numberWithInt:TEMPERATURE_VALUE] :[NSNumber numberWithInt:0] :@"41" :@"42"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:TEMPERATURE_VALUE] :[NSNumber numberWithInt:1] :@"41" :@"42"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:TEMPERATURE_VALUE] :[NSNumber numberWithInt:2] :@"41" :@"42"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:TEMPERATURE_VALUE] :[NSNumber numberWithInt:3] :@"41" :@"42"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:TEMPERATURE_VALUE] :[NSNumber numberWithInt:4] :@"41" :@"42"];
    
    [self GeneralThresholdObject:[NSNumber numberWithInt:APNEA_VALUE] :[NSNumber numberWithInt:0] :@"40" :@"50"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:APNEA_VALUE] :[NSNumber numberWithInt:1] :@"40" :@"50"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:APNEA_VALUE] :[NSNumber numberWithInt:2] :@"40" :@"50"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:APNEA_VALUE] :[NSNumber numberWithInt:3] :@"40" :@"50"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:APNEA_VALUE] :[NSNumber numberWithInt:4] :@"40" :@"50"];
    
    [self GeneralThresholdObject:[NSNumber numberWithInt:NO_MOTION_VALUE] :[NSNumber numberWithInt:0] :@"30" :@"40"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:NO_MOTION_VALUE] :[NSNumber numberWithInt:1] :@"30" :@"40"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:NO_MOTION_VALUE] :[NSNumber numberWithInt:2] :@"30" :@"40"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:NO_MOTION_VALUE] :[NSNumber numberWithInt:3] :@"30" :@"40"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:NO_MOTION_VALUE] :[NSNumber numberWithInt:4] :@"30" :@"40"];
    
    [self GeneralThresholdObject:[NSNumber numberWithInt:FALL_EVENT_VALUE] :[NSNumber numberWithInt:0] :@"0" :@"0"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:FALL_EVENT_VALUE] :[NSNumber numberWithInt:1] :@"0" :@"0"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:FALL_EVENT_VALUE] :[NSNumber numberWithInt:2] :@"0" :@"0"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:FALL_EVENT_VALUE] :[NSNumber numberWithInt:3] :@"0" :@"0"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:FALL_EVENT_VALUE] :[NSNumber numberWithInt:4] :@"0" :@"0"];
    
    [self GeneralThresholdObject:[NSNumber numberWithInt:LOW_RESPIRATION_RATE] :[NSNumber numberWithInt:0] :@"9" :@"6"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:LOW_RESPIRATION_RATE] :[NSNumber numberWithInt:1] :@"9" :@"6"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:LOW_RESPIRATION_RATE] :[NSNumber numberWithInt:2] :@"9" :@"6"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:LOW_RESPIRATION_RATE] :[NSNumber numberWithInt:3] :@"9" :@"6"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:LOW_RESPIRATION_RATE] :[NSNumber numberWithInt:4] :@"9" :@"6"];
    
    [self GeneralThresholdObject:[NSNumber numberWithInt:HIGH_RESPIRATION_RATE] :[NSNumber numberWithInt:0] :@"120" :@"130"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:HIGH_RESPIRATION_RATE] :[NSNumber numberWithInt:1] :@"120" :@"130"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:HIGH_RESPIRATION_RATE] :[NSNumber numberWithInt:2] :@"120" :@"130"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:HIGH_RESPIRATION_RATE] :[NSNumber numberWithInt:3] :@"120" :@"130"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:HIGH_RESPIRATION_RATE] :[NSNumber numberWithInt:4] :@"120" :@"130"];
    
    [self GeneralThresholdObject:[NSNumber numberWithInt:LOW_RESPIRATION_AMPLITUDE] :[NSNumber numberWithInt:0] :@"2" :@"1"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:LOW_RESPIRATION_AMPLITUDE] :[NSNumber numberWithInt:1] :@"2" :@"1"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:LOW_RESPIRATION_AMPLITUDE] :[NSNumber numberWithInt:2] :@"2" :@"1"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:LOW_RESPIRATION_AMPLITUDE] :[NSNumber numberWithInt:3] :@"2" :@"1"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:LOW_RESPIRATION_AMPLITUDE] :[NSNumber numberWithInt:4] :@"2" :@"1"];
    
    [self GeneralThresholdObject:[NSNumber numberWithInt:HIGH_RESPIRATION_AMPLITUDE] :[NSNumber numberWithInt:0] :@"4" :@"5"];
     [self GeneralThresholdObject:[NSNumber numberWithInt:HIGH_RESPIRATION_AMPLITUDE] :[NSNumber numberWithInt:1] :@"4" :@"5"];
     [self GeneralThresholdObject:[NSNumber numberWithInt:HIGH_RESPIRATION_AMPLITUDE] :[NSNumber numberWithInt:2] :@"4" :@"5"];
     [self GeneralThresholdObject:[NSNumber numberWithInt:HIGH_RESPIRATION_AMPLITUDE] :[NSNumber numberWithInt:3] :@"4" :@"5"];
     [self GeneralThresholdObject:[NSNumber numberWithInt:HIGH_RESPIRATION_AMPLITUDE] :[NSNumber numberWithInt:4] :@"4" :@"5"];
    
    [self GeneralThresholdObject:[NSNumber numberWithInt:RESPIRATION_VARIANCE] :[NSNumber numberWithInt:0] :@"40" :@"50"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:RESPIRATION_VARIANCE] :[NSNumber numberWithInt:1] :@"40" :@"50"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:RESPIRATION_VARIANCE] :[NSNumber numberWithInt:2] :@"40" :@"50"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:RESPIRATION_VARIANCE] :[NSNumber numberWithInt:3] :@"40" :@"50"];
    [self GeneralThresholdObject:[NSNumber numberWithInt:RESPIRATION_VARIANCE] :[NSNumber numberWithInt:4] :@"40" :@"50"];
    
    
}

-(void)defaultpatientvalue
{
      UIImage * patientimage= [UIImage imageNamed:@"user_profile_high_icon.png"];
    NSMutableDictionary *user_details=[[NSMutableDictionary alloc]init];
    [user_details setObject: UIImagePNGRepresentation(patientimage) forKey: @"PatientImage"];
    [self saveDataInUserDefault:user_details :@"user_details"];
    
    
    NSDate *date = [[NSDate alloc] init];
    // NSLog(@"%@", date);
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"dd MMM YY"];
    NSString *dateString = [dateFormatter1 stringFromDate:date];
    
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
    [dateFormatter2 setDateFormat:@"MM/dd/yyyy"];
    NSString *dob = [dateFormatter2 stringFromDate:date];
    
    PatientDetails *patientrecord = [NSEntityDescription insertNewObjectForEntityForName:@"PatientDetails"
                                                                  inManagedObjectContext:self.managedObjectContext];
    
    patientrecord.city=@"Mumbai";
    patientrecord.code=[NSNumber numberWithInt:1002];
    patientrecord.country=@"I";
    patientrecord.dob1=dob;
    patientrecord.device_id=@"123456";
    patientrecord.doc_ph_no=@"";
    patientrecord.doc_name=@"";
    patientrecord.ecode=@"";
    patientrecord.ecode_desc=@"";
    patientrecord.family_name=@"";
    patientrecord.gender1=@"M";
    patientrecord.given_name=@"";
    patientrecord.height1=@"31";
    patientrecord.level=@"Level1";
    patientrecord.middle_name=@"U2";
    patientrecord.mode=@"";
    patientrecord.notification_id=@"";
    patientrecord.notification_pwd=@"";
    patientrecord.patient_id=@"";
    patientrecord.patient_name=@"User1";
    patientrecord.state=@"MAH";
    patientrecord.street=@"G";
    patientrecord.uid=@"";
    patientrecord.weight=@"00";
    patientrecord.zipcode=@"111111";
    patientrecord.doc_address=@"";
    patientrecord.doc_email=@"";
    patientrecord.patient_add=@"XYZ";
    patientrecord.server_name=@"";
    patientrecord.status=@"";
    patientrecord.patient_mobno=@"";
    patientrecord.doc_country=@"";
    patientrecord.doc_id=@"";
    patientrecord.doc_degree=@"";
    
    patientrecord.active_since=dateString;
    patientrecord.blocked=@"0";
    patientrecord.patient_location=@"0";
    patientrecord.pat_updated_site=@"1";
    patientrecord.modified_datetime=@"";
    
   
        NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
        [TempDefault setObject:@"CELSIUS" forKey:@"TEMPERATURE"];
        [TempDefault synchronize];
   
   
    
        [TempDefault setObject:@"KILOGRAM" forKey:@"WEIGHT"];
        [TempDefault synchronize];
    

        [TempDefault setObject:@"CENTIMETER" forKey:@"HEIGHT"];
        [TempDefault synchronize];
    

    [self.managedObjectContext save:nil];

}
-(void)defaultmcdvalue
{
    NSDate *date1 = [[NSDate alloc] init];
    //NSLog(@"%@", date1);
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
    NSString *dateSt = [dateFormatter1 stringFromDate:date1];
    
    MCDSettings *mcdsettrecord = [NSEntityDescription insertNewObjectForEntityForName:@"MCDSettings"
                                                               inManagedObjectContext:self.managedObjectContext];
    
        mcdsettrecord.bt_device_add=@"";
        mcdsettrecord.base_line_fil=@"1";//on
        mcdsettrecord.created_by=@"MCM";
        mcdsettrecord.fiftyhz_filter=@"1";
        mcdsettrecord.inverted_t_wave=@"1";
        mcdsettrecord.gain=@"1";
        mcdsettrecord.lead_config=@"2";
        mcdsettrecord.leads=@"0";
        mcdsettrecord.mcd_setdatetime=dateSt;
        mcdsettrecord.modified_by=@"MCM";
        mcdsettrecord.respiration_cal=@"1";
        mcdsettrecord.sampling_rate=@"1";
        mcdsettrecord.sixtyhz_filter=@"0";
        mcdsettrecord.timeinterval_post=@"10";
        mcdsettrecord.timeinterval_pre=@"10";
        mcdsettrecord.volume_level=@"1";
        mcdsettrecord.created_datetime=dateSt;
        mcdsettrecord.created_from=@"MCM";
        mcdsettrecord.modified_from=@"MCM";
        mcdsettrecord.modified_time=dateSt;
        mcdsettrecord.orientation=@"1";
        mcdsettrecord.respiration=@"1";//on
        mcdsettrecord.status=@"";
        mcdsettrecord.updated_to_site=@"1";
        mcdsettrecord.updated_to_mcd=@"1";
        mcdsettrecord.xmpp_msgid=@"1";
    
    [self.managedObjectContext save:nil];
}

-(void) suspensionObject:(NSNumber *)eventcde :(NSNumber *)suspenstime{
     SuspensionSettings *suspensionsettrecord = [NSEntityDescription insertNewObjectForEntityForName:@"SuspensionSettings" inManagedObjectContext:self.managedObjectContext];
    NSDate *date = [[NSDate alloc] init];
   // NSLog(@"%@", date);
    
    CommonHelper * commhelp=[[CommonHelper alloc] init];
    NSString *modifieddate = [commhelp getDateInYourFormat:@"yyyy-MM-dd" :@"MM/dd/yyyy HH:mm:ss" :DateAsNSDate :@"2014-04-25" :date];
    suspensionsettrecord.event_code=eventcde;
    suspensionsettrecord.created_date_time=modifieddate;
    suspensionsettrecord.modify_datetime=modifieddate;
    suspensionsettrecord.suspension_time=suspenstime;
    suspensionsettrecord.created_from=@"MCM";
    suspensionsettrecord.modify_from=@"MCM";
    suspensionsettrecord.update_to_device=@"0";
    suspensionsettrecord.updated_to_site=@"1";
     [self.managedObjectContext save:nil];
}

-(void)DoctorThresholdObject:(NSNumber *)eventcde :(NSNumber *)posture :(NSString *)thresdd
{
    DoctorThreshold *doctorthreshrecord = [NSEntityDescription insertNewObjectForEntityForName:@"DoctorThreshold" inManagedObjectContext:self.managedObjectContext];
    NSDate *date = [[NSDate alloc] init];
    //NSLog(@"%@", date);
    
    CommonHelper * commhelp=[[CommonHelper alloc] init];
    NSString *modifieddate = [commhelp getDateInYourFormat:@"yyyy-MM-dd" :@"dd-MM-yyyy HH:mm:ss" :DateAsNSDate :@"2014-04-25" :date];
    doctorthreshrecord.code=eventcde;
    doctorthreshrecord.created_date_time=modifieddate;
    doctorthreshrecord.modify_datetime=modifieddate;
    doctorthreshrecord.posture=posture;
    doctorthreshrecord.dd=thresdd;
    doctorthreshrecord.ddflag=@"0";
    doctorthreshrecord.created_from=@"MCM";
    doctorthreshrecord.modify_from=@"MCM";
    doctorthreshrecord.updated_to_device=@"0";
    doctorthreshrecord.updated_to_site=@"1";
    
    [self.managedObjectContext save:nil];
}

-(void) GeneralThresholdObject:(NSNumber *)eventcde :(NSNumber *)posture :(NSString *)yd  :(NSString *)rd
{
    ThresholdSett *thresholdrecord = [NSEntityDescription insertNewObjectForEntityForName:@"ThresholdSett" inManagedObjectContext:self.managedObjectContext];
    NSDate *date = [[NSDate alloc] init];
   // NSLog(@"%@", date);
    
    CommonHelper * commhelp=[[CommonHelper alloc] init];
    NSString *modifieddate = [commhelp getDateInYourFormat:@"yyyy-MM-dd" :@"dd-MM-yyyy HH:mm:ss" :DateAsNSDate :@"2014-04-25" :date];
    thresholdrecord.code=eventcde;
    thresholdrecord.created_date_time=modifieddate;
    thresholdrecord.modify_datetime=modifieddate;
    thresholdrecord.posture=posture;
    thresholdrecord.yd=yd;
    thresholdrecord.rd=rd;
    thresholdrecord.flag=@"0";
    thresholdrecord.created_from=@"MCM";
    thresholdrecord.modify_from=@"MCM";
    thresholdrecord.update_to_device=@"0";
    thresholdrecord.updated_to_site=@"1";
   
    
    [self.managedObjectContext save:nil];
}


/*func isValidUrl(strUrl : NSString) -> Bool
 {
 //var urlRequest = NSURLRequest(URL: NSURL(string: strUrl)!)
 //return NSURLConnection.canHandleRequest(urlRequest)
 let regExStr = "(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+"
 var predicate = NSPredicate(format: "SELF MATCHES %@", regExStr)
 return  predicate!.evaluateWithObject(strUrl) as Bool
 }*/




-(void) serverAuthenticationWebCall
{
    // Start the loading
    
    CommonHelper *comphelp=[[CommonHelper alloc] init];
    NSString * domainName      =   [comphelp trimTheString:serverDomainTextField.text];
    domainName          =       (domainName.length > 0) ? [NSString  stringWithFormat:@"https://%@", domainName] : @"";
   
    if(isThere)
    {
    NSString *key = serverActivationKeyTextField.text;
    NSString * Fetchingpath=[NSString stringWithFormat:@"%@/MCC/mccwcfservice.svc/GetPatientData/?Key=%@",domainName,key];
    
    // Initialize Session Configuration
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    // Initialize Session Manager
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:sessionConfiguration];
    
    // Configure Manager
    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    
    // Send Request
        NSString *bookurl=[Fetchingpath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *URL1=[NSURL URLWithString:bookurl];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL1];
        

       
    NSLog(@"%@",URL1);
    [[manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            [self showAlertView:nil msg:@"Time Out Error"];
            NSLogger *logger=[[NSLogger alloc]init];
            logger.degbugger = true;
            
            [logger log:@"Server Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:error, @"First Time Login", nil] error:TRUE];
              [self stopLoad];
        }
        
        else {
            
            if (responseObject != nil)
            {
                
                NSString * responseStatus  = [responseObject valueForKey:@"status"];
                NSString *  responseStatusMessage   = [responseObject valueForKey:@"ECodeDescrip"];
                NSString * message;
                
              

                
                if( [responseStatus caseInsensitiveCompare:@"success"] == NSOrderedSame )
                 {
                     
                        //message = @"Login successfully";
                     NSLog(@"%@",responseObject);
                      NSString *  patientid   = [responseObject valueForKey:@"PatientID"];
                        NSString *  Deviceid   = [responseObject valueForKey:@"DeviceID"];
                      NSString *  DocterName   = [responseObject valueForKey:@"DoctorName"];
                       NSString *  PatientUid   = [responseObject valueForKey:@"UID"];
                      NSString *  PatientName   = [responseObject valueForKey:@"PatientName1"];
                      NSString * Servname= [responseObject valueForKey:@"servername"];
                     NSString * NotifyPWD= [responseObject valueForKey:@"NotificationPWD"];
                     NSString * NotifyID=[responseObject valueForKey:@"NotificationID"];
                     UIImage * patientimage= [UIImage imageNamed:@"user_profile_high_icon.png"];
                    
                       [self saveDataInUserDefault:Servname :@"XMPPServerName"];
                      [self saveDataInUserDefault:NotifyPWD :@"XMPPNotifyPWD"];
                      [self saveDataInUserDefault:NotifyID :@"XMPPNotifyID"];
                     
                    NSString *  Patientlevel   = [responseObject valueForKey:@"Level"];
                     if([Patientlevel isEqualToString:@"Level1"])
                     Patientlevel=@"SAFE GUARD MODE";
                     else if([Patientlevel isEqualToString:@"Level2"])
                         Patientlevel=@"PREMIUM GUARD MODE";
                     else if([Patientlevel isEqualToString:@"Level3"])
                         Patientlevel=@"AUTO PREMIUM SAFE GUARD MODE";
                     else
                         Patientlevel=@"PERSONAL ALERT GUARDIAN";

                         
                       
                    //  NSString *  deviceid   = [responseObject valueForKey:@"DeviceID"];
                     
                     NSMutableDictionary *user_details=[[NSMutableDictionary alloc]init];
                      [user_details setObject:PatientName forKey: @"PatientName"];
                      [user_details setObject:patientid forKey: @"PatientID"];
                       [user_details setObject:Deviceid forKey: @"Deviceid"];
                        [user_details setObject:DocterName forKey: @"DoctorName"];
                       [user_details setObject: UIImagePNGRepresentation(patientimage) forKey: @"PatientImage"];
                       [user_details setObject:Patientlevel forKey: @"Patientlevel"];
                      [user_details setObject:PatientUid forKey: @"PatientUid"];
                     
                     
                     NSUserDefaults *userdetail=[NSUserDefaults standardUserDefaults];
                     [userdetail setValue:patientid forKey:@"PatientID"];
                      [userdetail setValue:Deviceid forKey:@"Deviceid"];
                      [self saveDataInUserDefault:user_details :@"user_details"];
                     
                     
                     if([[responseObject valueForKey:@"status"] isEqualToString:@"Success"])
                     {
                         NSMutableArray *results = [[NSMutableArray alloc]init];
                         
                         NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"PatientDetails"];
                         //[fetchRequest setPredicate:pred];
                         results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
                         
                         if (results.count > 0) {
                             NSManagedObject* patientrecord = [results objectAtIndex:0];
                             
                             [patientrecord  setValue:[responseObject valueForKey:@"City"] forKey:@"city"];
                             [patientrecord  setValue:[responseObject valueForKey:@"Code"] forKey:@"code"];
                             [patientrecord  setValue:[responseObject valueForKey:@"Country"] forKey:@"country"];
                             [patientrecord  setValue:[responseObject valueForKey:@"DOB1"] forKey:@"dob1"];
                             [patientrecord  setValue:[responseObject valueForKey:@"DeviceID"] forKey:@"device_id"];
                             [patientrecord  setValue:[responseObject valueForKey:@"DocContactNo"] forKey:@"doc_ph_no"];
                             [patientrecord  setValue:[responseObject valueForKey:@"DoctorName"] forKey:@"doc_name"];
                             [patientrecord  setValue:[responseObject valueForKey:@"ECode"] forKey:@"ecode"];
                             [patientrecord  setValue:[responseObject valueForKey:@"ECodeDescrip"] forKey:@"ecode_desc"];
                             [patientrecord  setValue:[responseObject valueForKey:@"FamilyName"] forKey:@"family_name"];
                             [patientrecord  setValue:[[responseObject valueForKey:@"Gender1"]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"gender1"];
                             [patientrecord  setValue:[responseObject valueForKey:@"GivenName"] forKey:@"given_name"];
                             [patientrecord  setValue:[responseObject valueForKey:@"Height1"] forKey:@"height1"];
                             [patientrecord  setValue:[responseObject valueForKey:@"Level"] forKey:@"level"];
                             [patientrecord  setValue:[responseObject valueForKey:@"MiddleName"] forKey:@"middle_name"];
                             [patientrecord  setValue:[responseObject valueForKey:@"Mode"] forKey:@"mode"];
                             [patientrecord  setValue:[responseObject valueForKey:@"NotificationID"] forKey:@"notification_id"];
                             [patientrecord  setValue:[responseObject valueForKey:@"NotificationPWD"] forKey:@"notification_pwd"];
                             [patientrecord  setValue:[responseObject valueForKey:@"PatientID"] forKey:@"patient_id"];
                             [patientrecord  setValue:[responseObject valueForKey:@"PatientName1"] forKey:@"patient_name"];
                             [patientrecord  setValue:[responseObject valueForKey:@"State"] forKey:@"state"];
                             [patientrecord  setValue:[responseObject valueForKey:@"Street"] forKey:@"street"];
                             [patientrecord  setValue:[responseObject valueForKey:@"UID"] forKey:@"uid"];
                             [patientrecord  setValue:[responseObject valueForKey:@"Weight1"] forKey:@"weight"];
                             [patientrecord  setValue:[responseObject valueForKey:@"Zipcode"] forKey:@"zipcode"];
                             [patientrecord  setValue:[responseObject valueForKey:@"docaddress"] forKey:@"doc_address"];
                             [patientrecord  setValue:[responseObject valueForKey:@"docemail"] forKey:@"doc_email"];
                             [patientrecord  setValue:[responseObject valueForKey:@"patientaddress"] forKey:@"patient_add"];
                             [patientrecord  setValue:[responseObject valueForKey:@"servername"] forKey:@"server_name"];
                             [patientrecord  setValue:[responseObject valueForKey:@"status"] forKey:@"status"];
                              [patientrecord  setValue:[responseObject valueForKey:@"PatientContact"] forKey:@"patient_mobno"];
                             
                             NSDate *date = [[NSDate alloc] init];
                             //NSLog(@"%@", date);
                             NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
                             [dateFormatter1 setDateFormat:@"dd MMM YY"];
                             NSString *dateString = [dateFormatter1 stringFromDate:date];
                             
                             [patientrecord  setValue:dateString forKey:@"active_since"];
                             [patientrecord  setValue:@"0" forKey:@"blocked"];
                             [patientrecord  setValue:@"0" forKey:@"patient_location"];
                             [patientrecord  setValue:@"1" forKey:@"pat_updated_site"];
                             [patientrecord  setValue:@"" forKey:@"modified_datetime"];
                             [self.managedObjectContext save:nil];
                             
                             
                             if([[responseObject valueForKey:@"TempUnit"] isEqualToString:@"Celsius"])
                             {
                                 NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
                                 [TempDefault setObject:@"CELSIUS" forKey:@"TEMPERATURE"];
                                 [TempDefault synchronize];
                             }
                             else
                             {
                                 NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
                                 [TempDefault setObject:@"FAHRENHEIT" forKey:@"TEMPERATURE"];
                                 [TempDefault synchronize];
                             }
                             if([[responseObject valueForKey:@"WeightUnit"] isEqualToString:@"Pound"])
                             {
                                 NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
                                 [TempDefault setObject:@"POUNDS" forKey:@"WEIGHT"];
                                 [TempDefault synchronize];
                             }
                             else
                             {
                                 NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
                                 [TempDefault setObject:@"KILOGRAM" forKey:@"WEIGHT"];
                                 [TempDefault synchronize];
                             }
                             if([[responseObject valueForKey:@"HightUnit"] isEqualToString:@"Feet"])
                             {
                                 NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
                                 [TempDefault setObject:@"FEET AND INCHES" forKey:@"HEIGHT"];
                                 [TempDefault synchronize];
                             }
                             else
                             {
                                 NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
                                 [TempDefault setObject:@"CENTIMETER" forKey:@"HEIGHT"];
                                 [TempDefault synchronize];
                             }
                             
                         } else {
                             NSLog(@"Enter Corect number");
                         }
                         
                         
                     }
                     
                    
                   
                     
                   
                     
                     //request1
                     NSString *strURL = [NSString stringWithFormat:@"%@/MCC/MCCThresholdSettings.svc/GetMCDSettings/?PatientID=%@",domainName,patientid];
                     NSLog(@"scheduleurl : %@",strURL);
                     NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"GET" URLString:strURL parameters:nil error: nil];
                     
                     AFHTTPRequestOperation *operationOne = [[AFHTTPRequestOperation alloc] initWithRequest:request];
                     //operationOne = [AFHTTPResponseSerializer serializer];
                     operationOne.responseSerializer = [AFHTTPResponseSerializer serializer];
                     
                     [operationOne setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
                     {
                      // NSLog(@"Response: %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
                         NSDictionary *responseData;
                         NSError *error;
                         if (responseObject != nil) {
                             responseData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                                            options:NSJSONReadingMutableContainers
                                                                              error:&error];
                         }
                        
                         
                         NSDictionary *member=[responseData objectForKey:@"GetMCDSettingsResult"];
                         NSLog(@"%@",member);
                         
                       
                         for(NSDictionary *tabledict in member)
                         {
                         
                              if([[tabledict valueForKey:@"status"] isEqualToString:@"Success"])
                             {
                                 NSMutableArray *results = [[NSMutableArray alloc]init];
                                
                                     NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"MCDSettings"];
                                     //[fetchRequest setPredicate:pred];
                                     results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
                                     
                                     if (results.count > 0) {
                                         NSManagedObject* mcdsettrecord = [results objectAtIndex:0];
                                         
                                         [mcdsettrecord setValue:[tabledict valueForKey:@"BT_Device_Address"] forKey:@"bt_device_add"];
                                         [mcdsettrecord setValue:[tabledict valueForKey:@"Base_Line_Filter"] forKey:@"base_line_fil"];
                                         [mcdsettrecord setValue:[tabledict valueForKey:@"CreatedBy"] forKey:@"created_by"];
                                         [mcdsettrecord setValue:[tabledict valueForKey:@"FiftyHzFilter"] forKey:@"fiftyhz_filter"];
                                         [mcdsettrecord setValue:[tabledict valueForKey:@"Gain"]forKey:@"gain"];
                                         [mcdsettrecord setValue:[tabledict valueForKey:@"Leadconfig"]forKey:@"lead_config"];
                                         [mcdsettrecord setValue:[tabledict valueForKey:@"Leads"]forKey:@"leads"];
                                         [mcdsettrecord setValue:[tabledict valueForKey:@"MCDSetDateTime"]forKey:@"mcd_setdatetime"];
                                         [mcdsettrecord setValue:[tabledict valueForKey:@"ModifiedBy"]forKey:@"modified_by"];
                                         [mcdsettrecord setValue:[tabledict valueForKey:@"RespitrationCaliper"]forKey:@"respiration_cal"];
                                         [mcdsettrecord setValue:[tabledict valueForKey:@"Sampling_Rate"]forKey:@"sampling_rate"];
                                         [mcdsettrecord setValue:[tabledict valueForKey:@"SixtyHzFilter"]forKey:@"sixtyhz_filter"];
                                         [mcdsettrecord setValue:[tabledict valueForKey:@"TimeIntervalPost"]forKey:@"timeinterval_post"];
                                         [mcdsettrecord setValue:[tabledict valueForKey:@"TimeIntervalPre"]forKey:@"timeinterval_pre"];
                                         [mcdsettrecord setValue:[tabledict valueForKey:@"Volumelevel"]forKey:@"volume_level"];
                                         [mcdsettrecord setValue:[tabledict valueForKey:@"createddatetime"]forKey:@"created_datetime"];
                                         [mcdsettrecord setValue:[tabledict valueForKey:@"createdfrom"]forKey:@"created_from"];
                                         [mcdsettrecord setValue:[tabledict valueForKey:@"modifiedfrom"]forKey:@"modified_from"];
                                         [mcdsettrecord setValue:[tabledict valueForKey:@"modifiedtime"]forKey:@"modified_time"];
                                         [mcdsettrecord setValue:[tabledict valueForKey:@"orientation"]forKey:@"orientation"];
                                         [mcdsettrecord setValue:[tabledict valueForKey:@"respiration"]forKey:@"respiration"];
                                         [mcdsettrecord setValue:[tabledict valueForKey:@"status"]forKey:@"status"];
                                         [mcdsettrecord setValue:@"1"forKey:@"updated_to_site"];
                                         [mcdsettrecord setValue:@"0"forKey:@"updated_to_mcd"];
                                         [mcdsettrecord setValue:@"1"forKey:@"xmpp_msgid"];
                                         [mcdsettrecord setValue:@"0" forKey:@"inverted_t_wave"];
                                         
                                         [self.managedObjectContext save:nil];
                                     } else {
                                         NSLog(@"Enter Corect number");
                                     }
                                
                                 
                             }
                        
                         
                         }

                     }
                        failure:^(AFHTTPRequestOperation *operation, NSError *error)
                     {
                         NSLog(@"%@",[error description]);
                         
                     }];
                     
                     //Request 2
                     NSString *strURL1 = [NSString stringWithFormat:@"%@/MCC/MCCThresholdSettings.svc/GetSuspensionSettings/?PatientID=%@",domainName,patientid];
                     //NSString *strURL1 = [NSString stringWithFormat:@"https://mcc-test-01.personal-healthwatch.com/MCC/MCCThresholdSettings.svc/GetSuspensionSettings/?PatientID=dee8f70d-6952-4ee1-b2f9-b2f3ded3c294"];
                     NSLog(@"scheduleurl : %@",strURL1);
                     NSMutableURLRequest *request2 = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"GET" URLString:strURL1 parameters:nil error: nil];
                     
                     AFHTTPRequestOperation *operationTwo = [[AFHTTPRequestOperation alloc] initWithRequest:request2];
                     //operationTwo = [AFHTTPResponseSerializer serializer];
                      operationTwo.responseSerializer = [AFHTTPResponseSerializer serializer];
                     [operationTwo setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
                     {
                         NSDictionary *responseData;
                         NSError *error;
                         if (responseObject != nil) {
                             responseData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                                            options:NSJSONReadingMutableContainers
                                                                              error:&error];
                         }
                         
                         
                         NSDictionary *member=[responseData objectForKey:@"GetSuspensionSettingsResult"];
                          NSLog(@"%@",member);
                        // SuspensionSettings *suspensionsettrecord = [NSEntityDescription insertNewObjectForEntityForName:@"SuspensionSettings" inManagedObjectContext:self.managedObjectContext];
                        // suspensionsettrecord.=@"";
                   
                         for(NSDictionary *tabledict1 in member)
                         {
                          //setValue: @"MCC" forKey:
                          if([[tabledict1 valueForKey:@"status"] isEqualToString:@"Success"])
                          {
                              
                              NSMutableArray *results = [[NSMutableArray alloc]init];
                              int flag=0;
                              NSPredicate *pred;
                              if ([tabledict1 valueForKey:@"EventCode"]!=NULL) {
                                  pred =  [NSPredicate predicateWithFormat:@"SELF.event_code == %@",[tabledict1 valueForKey:@"EventCode"]];
                                  flag=1;
                              } else {
                                  flag=0;
                                  NSLog(@"Enter Corect Course number");
                              }
                              
                              if (flag == 1) {
                                  
                                  NSLog(@"predicate: %@",pred);
                                  NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"SuspensionSettings"];
                                  [fetchRequest setPredicate:pred];
                                  results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
                                  
                                  if (results.count > 0) {
                                      NSManagedObject* suspensionsett = [results objectAtIndex:0];
                                      [suspensionsett setValue:NULL_TO_NIL([tabledict1 valueForKey:@"DeviceID"]) forKey:@"device_id"];
                                      [suspensionsett setValue:NULL_TO_NIL([tabledict1 valueForKey:@"PatientID"]) forKey:@"patient_id"];
                                      [suspensionsett setValue:[tabledict1 valueForKey:@"EventCode"] forKey:@"event_code"];
                                      [suspensionsett setValue:[tabledict1 valueForKey:@"SuspensionTime"] forKey:@"suspension_time"];
                                      [suspensionsett setValue:[tabledict1 valueForKey:@"CreatedDateTime"] forKey:@"created_date_time"];
                                       [suspensionsett setValue:[tabledict1 valueForKey:@"ModifiedDateTime"] forKey:@"modify_datetime"];
                                      [suspensionsett setValue:[tabledict1 valueForKey:@"CreatedFrom"] forKey:@"created_from"];
                                      [suspensionsett setValue:[tabledict1 valueForKey:@"ModifiedFrom"] forKey:@"modify_from"];
                                      [suspensionsett setValue:[tabledict1 valueForKey:@"status"] forKey:@"status"];
                                      [suspensionsett setValue: @"0" forKey:@"update_to_device"];
                                      [suspensionsett setValue:@"1" forKey:@"updated_to_site"];
                                      
                                    [self.managedObjectContext save:nil];
                                  } else {
                                      NSLog(@"Enter Corect Course number");
                                  }
                                  
                                  
                                  
                              }
                        
                            }
                         }
                         
                         

                         
                     }
                    failure:^(AFHTTPRequestOperation *operation, NSError *error)
                     {
                         NSLog(@"%@",[error description]);
                     }];

                     //Request 3
                     NSString *strURL3 = [NSString stringWithFormat:@"%@/MCC/MCCThresholdSettings.svc/GetThresholdSettings/?PatientID=%@",domainName,patientid];
                     NSLog(@"scheduleurl : %@",strURL3);
                     NSMutableURLRequest *request3 = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"GET" URLString:strURL3 parameters:nil error: nil];
                     
                     AFHTTPRequestOperation *operationThree = [[AFHTTPRequestOperation alloc] initWithRequest:request3];
                     operationThree.responseSerializer = [AFHTTPResponseSerializer serializer];
                     [operationThree setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
                      {
                          NSDictionary *responseData;
                          NSError *error;
                          if (responseObject != nil) {
                              responseData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                                             options:NSJSONReadingMutableContainers
                                                                               error:&error];
                          }
                          
                          
                          NSDictionary *member=[responseData objectForKey:@"GetThresholdSettingsResult"];
                           NSLog(@"%@",member);
                          
                          for(NSDictionary *tabledict2 in member)
                        {
                        if([[tabledict2 valueForKey:@"status"] isEqualToString:@"Success"])
                        {
                            
                            
                           NSMutableArray *results = [[NSMutableArray alloc]init];
                            NSMutableArray *results1 = [[NSMutableArray alloc]init];
                            int flag=0;
                            
                            NSPredicate *pred;
                            NSString* filter = @"%K == %@ && %K == %@";
                            NSArray* args = @[@"code",[tabledict2 valueForKey:@"Code"], @"posture", [tabledict2 valueForKey:@"Posture"]];
                            if ([tabledict2 valueForKey:@"Code"]!=NULL) {
                                pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
                                flag=1;
                            } else {
                                flag=0;
                                NSLog(@"Enter Corect code empty");
                                
                            }
                            
                            if (flag == 1) {
                                
                                
                                NSLog(@"predicate: %@",pred);
                                NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
                                [fetchRequest setPredicate:pred];
                                results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
                                
                                if (results.count > 0) {
                                    
                                    NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
                                    //NSLog(@"1 - %@", doctorssett);
                                    
                                    [doctorssett setValue:NULL_TO_NIL([tabledict2 valueForKey:@"DeviceID"]) forKey:@"device_id"];
                                    [doctorssett setValue:NULL_TO_NIL([tabledict2 valueForKey:@"PatientID"]) forKey:@"patient_id"];
                                    [doctorssett setValue:NULL_TO_NIL([tabledict2 valueForKey:@"CreatedDateTime"]) forKey:@"created_date_time"];
                                    [doctorssett setValue:NULL_TO_NIL([tabledict2 valueForKey:@"CreatedFrom"]) forKey:@"created_from"];
                                    [doctorssett setValue:[tabledict2 valueForKey:@"DD"] forKey:@"dd"];
                                    BOOL returnBoolInt = [[tabledict2 valueForKey:@"DDFlag"] boolValue];
                                    [doctorssett setValue:[NSString stringWithFormat:@"%d",returnBoolInt] forKey:@"ddflag"];
                                    [doctorssett setValue:[tabledict2 valueForKey:@"ModifiedDateTime"] forKey:@"modify_datetime"];
                                    [doctorssett setValue:[tabledict2 valueForKey:@"ModifiedFrom"] forKey:@"modify_from"];
                                    [doctorssett setValue:@"1" forKey:@"updated_to_site"];
                                    [doctorssett setValue:@"0" forKey:@"updated_to_device"];
                                    [doctorssett setValue:[tabledict2 valueForKey:@"status"] forKey:@"status"];
                                    
                                    [self.managedObjectContext save:nil];
                                } else {
                                    NSLog(@"Enter Corect code number");
                                }
                                NSFetchRequest *fetchRequest1 = [[NSFetchRequest alloc]initWithEntityName:@"ThresholdSett"];
                                [fetchRequest1 setPredicate:pred];
                                results1 = [[self.managedObjectContext executeFetchRequest:fetchRequest1 error:nil] mutableCopy];
                                
                                if (results1.count > 0) {
                                    
                                    NSManagedObject *genthressett = (NSManagedObject *)[results1 objectAtIndex:0];
                                    //NSLog(@"1 - %@", genthressett);
                                    [genthressett setValue:NULL_TO_NIL([tabledict2 valueForKey:@"DeviceID"]) forKey:@"device_id"];
                                    [genthressett setValue:NULL_TO_NIL([tabledict2 valueForKey:@"PatientID"]) forKey:@"patient_id"];
                                    [genthressett setValue:NULL_TO_NIL([tabledict2 valueForKey:@"CreatedDateTime"]) forKey:@"created_date_time"];
                                    [genthressett setValue:NULL_TO_NIL([tabledict2 valueForKey:@"CreatedFrom"]) forKey:@"created_from"];
                                    [genthressett setValue:[tabledict2 valueForKey:@"YD"] forKey:@"yd"];
                                    [genthressett setValue:[tabledict2 valueForKey:@"RD"] forKey:@"rd"];
                                     BOOL returnBoolInt = [[tabledict2 valueForKey:@"Flag"] boolValue];
                                    [genthressett setValue:[NSString stringWithFormat:@"%d",returnBoolInt] forKey:@"flag"];
                                    
                                    [genthressett setValue:[tabledict2 valueForKey:@"ModifiedDateTime"] forKey:@"modify_datetime"];
                                    [genthressett setValue:[tabledict2 valueForKey:@"ModifiedFrom"] forKey:@"modify_from"];
                                    [genthressett setValue:@"1" forKey:@"updated_to_site"];
                                    [genthressett setValue:@"0" forKey:@"update_to_device"];
                                    [genthressett setValue:NULL_TO_NIL([tabledict2 valueForKey:@"status"]) forKey:@"status"];
                                    [self.managedObjectContext save:nil];
                                } else {
                                    NSLog(@"Enter Corect general code number");
                                }
                                
                                
                                
                            }

                        }
                    }
                          
                        

            }
                        failure:^(AFHTTPRequestOperation *operation, NSError *error)
                      {
                          NSLog(@"%@",[error description]);
                      }];

                     //Request 4
                    /* NSString *strURL4 = [NSString stringWithFormat:@"%@/MCC/MCCThresholdSettings.svc/GetOnDemandSettings/?DeviceID=%@ &PatientID=%@",domainName,deviceid,patientid];
                     NSLog(@"scheduleurl : %@",strURL4);
                     NSMutableURLRequest *request4 = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"GET" URLString:strURL4 parameters:nil error: nil];
                     
                     AFHTTPRequestOperation *operationFour = [[AFHTTPRequestOperation alloc] initWithRequest:request4];
                     operationFour.responseSerializer = [AFHTTPResponseSerializer serializer];
                     [operationFour setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
                      {
                          NSLog(@"Response: %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);                   }
                      failure:^(AFHTTPRequestOperation *operation, NSError *error)
                      {
                          NSLog(@"%@",[error description]);
                      }];*/

                     NSArray *operations = [AFURLConnectionOperation batchOfRequestOperations:@[operationOne,operationTwo,operationThree] progressBlock:^(NSUInteger numberOfFinishedOperations, NSUInteger totalNumberOfOperations)
                     {
                         //NSLog(@"%i of %i complete",numberOfFinishedOperations,totalNumberOfOperations);
                         //set progress here
                        // yourProgressView.progress = (float)numberOfFinishedOperations/(float)totalNumberOfOperations;
                         
                     } completionBlock:^(NSArray *operations)
                     {
                         NSLog(@"All operations in batch complete");
                          [self stopLoad];
                         NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                         [defaults setBool:YES forKey:@"FirstRun"];
                         [defaults synchronize];
                       
                         //[appDelegate connect];
                          if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                          {
                          UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Mainstoryboard_iPad" bundle:nil];
                          ECSlidingViewController * controller = (ECSlidingViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ECSlidingStoryboardId"];
                          [self presentViewController:controller animated:YES completion:nil];
                          
                          }
                          else
                          {
                          UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                          ECSlidingViewController * controller = (ECSlidingViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ECSlidingStoryboardId"];
                          [self presentViewController:controller animated:YES completion:nil];
                          }
                         
                     }];
                     
                     [[NSOperationQueue mainQueue] addOperations:operations waitUntilFinished:NO];
                  

                        
                        // Save data in Database
                    }
                    else if( [responseStatus caseInsensitiveCompare:@"failure"] == NSOrderedSame )
                    {
                        NSLogger *logger=[[NSLogger alloc]init];
                        logger.degbugger = true;
                        
                        [logger log:@"Server Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:responseStatusMessage, @"First Time Login", nil] error:TRUE];
                        [self stopLoad];
                        message =   responseStatusMessage;
                         [self showAlertView:nil msg:message];
                    }
                    
                
               // [self showAlertView:nil msg:message];
               
                
                
            }
            else if (error != nil)
            {
              
                  [self showAlertView:nil msg:[error description]];
            }
   
           
       
        
      }
        
    }] resume];
  
    }
    
}

-(void) showAlertView:(NSString *)title  msg:(NSString *) message
{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}







-(void) makeWebCallForAuthentication
{
    // Start the loading
   /* [self startLoad];
    
    // Calling the Get Request
    
    var trailUrl = "stream/0/posts/stream/global"
    var params   = [] //["AAPL" : "Apple Inc", "GOOG" : "Google Inc", "AMZN" : "Amazon Inc"]
    
    
    
    var getSampleDataTask : NSURLSessionTask  =    GetRequest().getSampleDataForNetworkTest(trailUrl, params: params, block: {
        
        (responseData : NSArray?, error : NSError?) -> Void in
        
        //var dataArr  = (NSDictionary)
        //println("Error Data in VC \(error?.description)")
        println("Success Data in VC \(responseData)")
        //println("Success Data in VC \(responseData?.objectAtIndex(0))")
        
        // Stop the loading
        self.stopLoad()
        
        
    })*/
    
}

-(void)hideKeyboard
{
    [self.view endEditing:true];
}

-(void)checkinternet
{
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        
        NSLog(@"Reachability changed: %@", AFStringFromNetworkReachabilityStatus(status));
        
        
        switch (status) {
            case AFNetworkReachabilityStatusReachableViaWWAN:
            case AFNetworkReachabilityStatusReachableViaWiFi:
            {
                // -- Reachable -- //
                NSLog(@"Reachable");
                isThere=true;
                break;
            }
            case AFNetworkReachabilityStatusNotReachable:
            default:
            {
                // -- Not reachable -- //
                NSLog(@"Not Reachable");
                  [self showAlertView:nil msg:@"No internet connectivity"];
                [self stopLoad];
               
                NSLogger *logger=[[NSLogger alloc]init];
                logger.degbugger = true;
                
                [logger log:@"Internet Connection" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"No internet connectivity", @"First Time Login", nil] error:TRUE];
                
                isThere=false;
                break;
            }
        }
        
    }];

}



@end
