//
//  WCFUpload.m
//  HealthWatch
//
//  Created by METSL MAC MINI on 18/12/15.
//  Copyright © 2015 METSL MAC MINI. All rights reserved.
//


#import "BackgroundCleanupService.h"


@implementation BackgroundCleanupService

/*
 * The singleton instance. To get an instance, use
 * the getInstance function.
 */
static BackgroundCleanupService *instance = NULL;

/**
 * Singleton instance.
 */
+(BackgroundCleanupService *)getInstance {
    @synchronized(self) {
        if (instance == NULL) {
            instance = [[self alloc] init];
        }
    }
    return instance;
}

- (void)run {
    
    NSLog(@"Helo");
    
}

@end
