//
//  GlobalVars.m
//  HealthWatch
//
//  Created by METSL MAC MINI on 04/11/15.
//  Copyright © 2015 METSL MAC MINI. All rights reserved.
//

#import "GlobalVars.h"

@implementation GlobalVars

@synthesize plotdata = _plotdata;
@synthesize Eventplotdata = _Eventplotdata;
@synthesize Leaddata = _Leaddata;

@synthesize mcd_HeartRate=_mcd_HeartRate;
@synthesize mcd_RespirationRate=  _mcd_RespirationRate;
@synthesize  mcd_Temperature=  _mcd_Temperature;
@synthesize  mcd_Posture=  _mcd_Posture;
@synthesize  mcd_MCD_LastEvent=  _mcd_MCD_LastEvent ;
@synthesize  mcd_leads_off=  _mcd_leads_off;
@synthesize  mcd_QRS=  _mcd_QRS;
@synthesize  mcd_QT=  _mcd_QT;
@synthesize  mcd_QTc=  _mcd_QTc;
@synthesize  mcd_PR=  _mcd_PR;
@synthesize  mcd_RR=  _mcd_RR;
@synthesize  mcd_DataBuf=  _mcd_DataBuf;
@synthesize  mcd_ERAM=  _mcd_ERAM;
@synthesize  mcd_ASIC=  _mcd_ASIC;
@synthesize  mcd_SDCARD_Presence=  _mcd_SDCARD_Presence;
@synthesize  mcd_SDCARD_ERR=  _mcd_SDCARD_ERR;
@synthesize  mcd_BatteryStatus=__mcd_BatteryStatus;
@synthesize  mcd_DeviceId=_mcd_DeviceId;
@synthesize  mcd_firmware_version=_mcd_firmware_version;
@synthesize  mcd_last_updatd=_mcd_last_updatd;
@synthesize  mcd_garmentstatus=_mcd_garmentstatus;
@synthesize  mcd_bluetoothstatus=_mcd_bluetoothstatus;
@synthesize  mcd_cloudstatus=_mcd_cloudstatus;

@synthesize built_accelerometer=_built_accelerometer;
@synthesize built_electrodes=_built_electrodes ;
@synthesize built_battery=_built_battery ;
@synthesize built_bluetooth=_built_bluetooth ;
@synthesize built_temperature =_built_temperature;
@synthesize built_sdcard=_built_sdcard;

@synthesize ChangeLevel=_ChangeLevel;

@synthesize mcd_sett_fiftyhz=_mcd_sett_fiftyhz;
@synthesize mcd_sett_sixthz=_mcd_sett_sixthz;
@synthesize mcd_sett_inverted_t_wave=_mcd_sett_inverted_t_wave;
@synthesize mcd_sett_gain=_mcd_sett_gain;
@synthesize mcd_sett_lead=_mcd_sett_lead;
@synthesize mcd_sett_lead_config=_mcd_sett_lead_config;
@synthesize mcd_sett_sampling_rate=_mcd_sett_sampling_rate;
@synthesize mcd_sett_pretime=_mcd_sett_pretime;
@synthesize mcd_sett_postime=_mcd_sett_postime;
@synthesize mcd_sett_mute=_mcd_sett_mute;
@synthesize mcd_sett_respiration=_mcd_sett_respiration;
@synthesize mcd_sett_orientation=_mcd_sett_orientation;
@synthesize mcd_sett_baseline=_mcd_sett_baseline;

@synthesize suspsett_time=_suspsett_time;
@synthesize suspsett_ecodevalue=_suspsett_ecodevalue;


@synthesize Thres_DD=_Thres_DD;
@synthesize Thres_YD=_Thres_YD;
@synthesize Thres_RD=_Thres_RD;
@synthesize Thres_flagg=_Thres_flagg;
@synthesize Thres_ddflagg=_Thres_ddflagg;
@synthesize Thres_posturecode=_Thres_posturecode;
@synthesize Thres_SetCode=_Thres_SetCode;
@synthesize ThresMultiplePosturesArray=_ThresMultiplePosturesArray;

@synthesize currentSetting=_currentSetting;
@synthesize WCFService=_WCFService;
@synthesize bluetoothService=_bluetoothService;

@synthesize latitude=_latitude;
@synthesize longititude=_longititude;


@synthesize Email_EventCode=_Email_EventCode;
@synthesize Email_EventDatetime=_Email_EventDatetime;
@synthesize BluetoothConnectionflag=_BluetoothConnectionflag;
@synthesize Internetflag=_Internetflag;
@synthesize MCDSyncflag=_MCDSyncflag;


@synthesize writeptr=_writeptr;
@synthesize difptr1=_difptr1;
@synthesize readpnter1=_readpnter1;
@synthesize dropdownValidFlag=_dropdownValidFlag;
@synthesize LiveEcgFlag=_LiveEcgFlag;
@synthesize XMPPflag_on_off=_XMPPflag_on_off;
@synthesize ecgarray=_ecgarray;
@synthesize configconnectFlag=_configconnectFlag;
@synthesize mcdmacaddress=_mcdmacaddress;
@synthesize suspenseflag=_suspenseflag;
@synthesize indexpathsection=_indexpathsection;
@synthesize indexpathrow=_indexpathrow;




+ (GlobalVars *)sharedInstance {
    static dispatch_once_t onceToken;
    static GlobalVars *instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[GlobalVars alloc] init];
    });
    return instance;
}

- (id)init {
    self = [super init];
    if (self) {
        _plotdata = [[NSMutableArray alloc] init];
        _Eventplotdata = [[NSMutableArray alloc] init];
        _Leaddata= [[NSMutableArray alloc] init];
        _ecgarray=[[NSMutableArray alloc]init];
        
        _ThresMultiplePosturesArray=[[NSMutableArray alloc] init];
        // Note these aren't allocated as [[NSString alloc] init] doesn't provide a useful object
        _latitude=nil;
        _longititude=nil;
        _mcd_DeviceId=nil;
        _mcd_firmware_version=nil;
        _mcd_last_updatd=nil;
        _mcd_garmentstatus=nil;
        _mcd_bluetoothstatus=nil;
        _mcd_cloudstatus=nil;
        _mcdmacaddress=nil;
        
        _built_accelerometer=@"0";
        _built_electrodes=@"0";
        _built_battery=@"0";
        _built_bluetooth=@"0";
        _built_temperature=@"0";
        _built_sdcard=@"0";
        
        
        _mcd_sett_fiftyhz=nil;
        _mcd_sett_sixthz=nil;
        _mcd_sett_inverted_t_wave=nil;
        _mcd_sett_gain=nil;
        _mcd_sett_lead=nil;
        _mcd_sett_lead_config=nil;
        _mcd_sett_sampling_rate=nil;
        _mcd_sett_pretime=nil;
        _mcd_sett_postime=nil;
        _mcd_sett_mute=nil;
        _mcd_sett_respiration=nil;
        _mcd_sett_orientation=nil;
        _mcd_sett_baseline=nil;

        _suspsett_time=nil;
        _suspsett_ecodevalue=nil;
        
        _Thres_DD=nil;
        _Thres_YD=nil;
        _Thres_RD=nil;
        _Thres_flagg=nil;
        _Thres_ddflagg=nil;
        _Thres_posturecode=nil;
        _Thres_SetCode=nil;
        
        _Email_EventCode=nil;
        _Email_EventDatetime=nil;
        _dropdownValidFlag=false;
        _LiveEcgFlag=false;
        _configconnectFlag=false;
        _suspenseflag=false;
        _XMPPflag_on_off=false;
       // _currentSetting=nil;
        
    }
    return self;
}

@end
