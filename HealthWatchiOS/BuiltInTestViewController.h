//
//  BuiltInTestViewController.h
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 17/06/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BuiltInTestViewController : UIViewController
{
    UIColor *Greencolor;
    UIColor *Redcolor;
}
//leadbtn
@property (nonatomic, strong) IBOutlet UIButton *  btnleadRA;
@property (nonatomic, strong) IBOutlet UIButton *  btnleadLA;
@property (nonatomic, strong) IBOutlet UIButton *  btnleadLL;
@property (nonatomic, strong) IBOutlet UIButton *  btnleadRL;
@property (nonatomic, strong) IBOutlet UIButton *  btnleadV1;
@property (nonatomic, strong) IBOutlet UIButton *  btnleadV2;
@property (nonatomic, strong) IBOutlet UIButton *  btnleadV3;
@property (nonatomic, strong) IBOutlet UIButton *  btnleadV4;
@property (nonatomic, strong) IBOutlet UIButton *  btnleadV5;
@property (nonatomic, strong) IBOutlet UIButton *  btnleadV6;
@property (nonatomic, strong) IBOutlet UIButton *  btnleadV7;
@property (nonatomic, strong) IBOutlet UIButton *  btnleadV3R;
@property (nonatomic, strong) IBOutlet UIButton *  btnleadV4R;

@property (nonatomic, strong) IBOutlet UIButton *  btnaccelorometer;
@property (nonatomic, strong) IBOutlet UIButton *  btnsdcard;
@property (nonatomic, strong) IBOutlet UIButton *  btninfrared;
@property (nonatomic, strong) IBOutlet UIButton *  btnbluetooth;
@property (nonatomic, strong) IBOutlet UIButton *  btncloud;
@property (nonatomic, strong) IBOutlet UIButton *  btnmcdbattery;


@property (nonatomic, strong) IBOutlet UIImageView *  accelorometer;
@property (nonatomic, strong) IBOutlet UIImageView *  sdcard;
@property (nonatomic, strong) IBOutlet UIImageView *  infrared;
@property (nonatomic, strong) IBOutlet UIImageView *  bluetooth;
@property (nonatomic, strong) IBOutlet UIImageView *  cloud;
@property (nonatomic, strong) IBOutlet UIImageView *  mcdbattery;


-(void)leaddataupdate;
-(void)Builtindataupdate;
@end
