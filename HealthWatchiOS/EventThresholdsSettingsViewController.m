//
//  EventThresholdsSettingsViewController.m
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 24/06/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import "EventThresholdsSettingsViewController.h"

#import "CustomTextField.h"
#import "CustomUIPickerView.h"
#import "SDCAlertView.h"
#import "SDCAlertViewController.h"
#import <UIView+SDCAutoLayout.h>
#import <AudioToolbox/AudioToolbox.h>
#import "DoctorThreshold.h"
#import "ThresholdSett.h"
#import "DoctorProfileViewController.h"




@interface EventThresholdsSettingsViewController()< UIPickerViewDelegate, UIPickerViewDataSource>
{
    UIImageOrientation scrollOrientation;
    CGPoint lastPos;
}
//@property (nonatomic, strong) PickerCellsController *pickersController;

@end

@implementation EventThresholdsSettingsViewController
@synthesize headerBlueView,rounderViewUnderHeader,tabDivider1,tabDivider2,tabDivider3,tabDivider4,layingButton,standingSittingButton,walkingButton,runningButton,selectMainEventThresholdButton,tableViewDoctorSettings,mainCatDropDownBlockView,mainCatPickerControl,doctorSettingTableViewCell,dropDownInUse,spinner,lockunlocButton;

BOOL isPresent = true;



AppDelegate *appDelegate;
- (void)viewDidLoad {
    [super viewDidLoad];
    Cell_Data_MutableArray = [[NSMutableArray  alloc]init ];
    currentSelectedRowIdForMainEventCat = 0;
    
    
    
    // ------------------------------------------- First time Sliding view Setups START  -------------------------------------------
    self.slidingViewController.topViewAnchoredGesture = ECSlidingViewControllerAnchoredGestureTapping | ECSlidingViewControllerAnchoredGesturePanning;
    self.slidingViewController.customAnchoredGestures =@[];
    
    // Paint to Header view
    //headerBlueView.backgroundColor = blueColorMainAppUIColor
    
    
    // Initialize and Allocation of the variables
    userSelectedPosturesArray = [[NSMutableArray alloc]initWithCapacity:4];
    appDelegate =(AppDelegate *)UIApplication.sharedApplication.delegate ;
    appDelegate.orientationKey = @"LAN";
    [appDelegate forceOrientationTakePlace:appDelegate.orientationKey];
    
    globalposturecode=@"1";
    [self initalizeElements];
    // --- : First on did load we need to prepare the array data structure for the screen.
    [self prepareScreenDataMainFunc];
    [self fetchingpatientdata];
    
    
    
    
    
     UISwipeGestureRecognizer *swipeUpDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyBoard:)];
     [swipeUpDown setDirection:(UISwipeGestureRecognizerDirectionUp | UISwipeGestureRecognizerDirectionDown )];
     [tableViewDoctorSettings addGestureRecognizer:swipeUpDown];
     
    
      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(DoctorLoginStatus:) name:RWT_DOCTOR_LOGIN_NOTIFICATION object:nil];
    
    /*let swipeDown = UISwipeGestureRecognizer(target: self, action:"hideKeyBoard")
    swipeDown.direction = UISwipeGestureRecognizerDirection.Down
    tableViewDoctorSettings .addGestureRecognizer(swipeDown)*/
     
   
}



-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    isPresent = false;
    //appDelegate.currentVC = nil
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    mainCatDropDownBlockView.backgroundColor = [[UIColor whiteColor]colorWithAlphaComponent:(0.9)];
    
    //appDelegate.currentVC = self
    //appDelegate.orientationKey = "LAN"
   // [self forceOrientationTakePlace(appDelegate.orientationKey)]
    
    // Work of TableView
    tableViewDoctorSettings.delegate    = self;
    //tableViewDoctorSettings.dataSource  = self
    
    
    // Update the Main Event Cat Button Title
[self updateWholeScreenBaseOnMainEvent];
    
    GlobalVars *global = [GlobalVars sharedInstance];
    if(global.dropdownValidFlag)
        lockunlocButton.selected=true;
}


-(IBAction)goToDashboard:(id)sender{
    self.slidingViewController.topViewController.view.layer.transform = CATransform3DMakeScale(1, 1, 1);
    self.slidingViewController.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"DashboardStoryboardId"];
}

-(IBAction)leftSideButtonAction:(id)sender{
    [self.slidingViewController anchorTopViewToRightAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)hideKeyBoard:(id)sender
{
    //println("Hide Keyboard now")
    [self.view endEditing:true];
}

-(void)initalizeElements
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    standalone_Mode = [defaults boolForKey:@"ActiveStandalonemode"];
   
    [selectMainEventThresholdButton.titleLabel setAdjustsFontSizeToFitWidth:true];
    [standingSittingButton.titleLabel setAdjustsFontSizeToFitWidth:true];
     blueBorderColorForTab = [UIColor colorWithPatternImage:[UIImage imageNamed:@"blue_border_color"]];
     yellowUIColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"yellow_color.png"]];
      redUIColor= [UIColor colorWithPatternImage:[UIImage imageNamed:@"red_color.png"]];
    rounderViewUnderHeader.layer.borderWidth     =   1.0;
    rounderViewUnderHeader.layer.borderColor     =   blueBorderColorForTab.CGColor;
    
    rounderViewUnderHeader.layer.cornerRadius    =   9.0;
    rounderViewUnderHeader.layer.masksToBounds   =   true;
    
   
    
    [self makeAllTabDividerBlueInColor];
    
}

-(void)makeAllTabDividerBlueInColor
{
    tabDivider1.backgroundColor = blueBorderColorForTab;
    tabDivider2.backgroundColor = blueBorderColorForTab;
    tabDivider3.backgroundColor = blueBorderColorForTab;
    tabDivider4.backgroundColor = blueBorderColorForTab;
}

/*override func viewDidDisappear(animated: Bool) {
 isPresent = false
 appDelegate.currentVC
 }*/





-(void) prepareScreenDataMainFunc
{
    NSString *eventThresholdSettingFilePath = [[NSBundle mainBundle] pathForResource:@"Event_Thresholds_Setting" ofType:@"plist"];
    NSDictionary *eventThresholdsSettingItemsNSDic = [NSDictionary dictionaryWithContentsOfFile:eventThresholdSettingFilePath];
    
    //NSLog(@"%@",eventThresholdsSettingItemsNSDic);
   
    
    if (eventThresholdsSettingItemsNSDic.count > 0)
    {
        // ---------------- Adding the Categories To Home array
        NSArray *event_categories_temp = [eventThresholdsSettingItemsNSDic objectForKey:@"Event_Category"];
        Event_Category_Master_NSArray =  event_categories_temp;
        
        //println("Event_Category_Master_NSArray \(Event_Category_Master_NSArray)")
        
        // ---------------- Adding the Postures To Home array
        NSArray *event_posture_temp = [eventThresholdsSettingItemsNSDic objectForKey:@"Event_Postures"];
        Event_Posture_Master_NSArray =  event_posture_temp;
        
        
        // ---------------- Adding the Sub Categories To Home array
        Event_Sub_Category_Master_NSDict = [eventThresholdsSettingItemsNSDic objectForKey:@"Event_Sub_Category"];
      
        
        
        // Preparing DataStore Array
        
        if (Event_Category_Master_NSArray.count > 0)
        {
            Main_Updated_NSMutDictonary_Array   =   [[NSMutableDictionary alloc]init];
            NSString * eventCatMainSlug ;
            NSString * postureSlug;
            //NSString * eventSubCatSlug ;
            
           // NSMutableDictionary* EventCatMD;
            NSDictionary * eachMainCatEventType=[[NSDictionary alloc]init];
            for(eachMainCatEventType in Event_Category_Master_NSArray)
            {
                //println(" Each Events Cates \(eachMainCatEventType)")
                
                
                //println(" ==============================================================   ")
                
                eventCatMainSlug = eachMainCatEventType[@"Slug"];
                
                /*
                 There are Total 3 Option [arrhythmia-slug, ischemia-slug, other-events-slug].
                 arrhythmia-slug, ischemia-slug both have Postures they are total 4, laying, standing/sitting, walking, running.
                 and only other-events-slug have not postures same as rest 2, as above.
                 */
                if ([eventCatMainSlug isEqualToString:@"other-events-slug"])
                {
                    //Will Skipp the Posture part, and will direct move to Subcat Event bunch of evnts
                    
                    if (Event_Sub_Category_Master_NSDict.count > 0)
                    {
                        NSArray * eachSubCatEventBunchNSArray  = Event_Sub_Category_Master_NSDict[eventCatMainSlug];
                        
                        if (eachSubCatEventBunchNSArray.count > 0)
                        {
                            NSMutableDictionary* storingInEachSubCatEventsKey = [[NSMutableDictionary alloc]init];
                            for(NSDictionary* eachSubCatEvent in eachSubCatEventBunchNSArray)
                            {
                                NSString* nameOfSubCatEventKey    =   eachSubCatEvent[@"Slug"];
                                // Store All Values [Thresholds, Yellow, Red] in key for hash mapping
                                [storingInEachSubCatEventsKey setObject:[self getClubedSettingArray:eachSubCatEvent postureslug:postureSlug mainevent:eventCatMainSlug] forKey:nameOfSubCatEventKey];
                                
                                
                                
                            }  // End Loop Each Sub Cat Event [Ventricular, and so on . . .]
                            
                            //println(" Sub Updated events \(storingInEachSubCatEventsKey)")
                            
                            // OLD OLD OLD --------------  Adding [[ storingEachSubCatEventsKey ]] to Posture key
                            //storingInEachPostureKey.setObject(storingInEachSubCatEventsKey, forKey: postureSlug)
                            
                            // --------------  Adding [[ storingInEachPostureKey ]] to Main_Updated_NSMutDictonary_Array key
                            [Main_Updated_NSMutDictonary_Array setObject:storingInEachSubCatEventsKey  forKey: eventCatMainSlug];
                            
                            //println(" @@@@@ Main_Updated_NSMutDictonary_Array UPDATED DATA FULL FOR OTHER-EVENTS \(Main_Updated_NSMutDictonary_Array)")
                            
                        } // End if Cheking Sub Cat Events
                        
                    } // End if Cheking Sub Cat Events For Hashing
                    
                    
                }
                else
                {
                    //
                    if (Event_Posture_Master_NSArray.count > 0)
                    {
                        NSMutableDictionary *storingInEachPostureKey = [[NSMutableDictionary alloc]init];
                        NSDictionary* eachPosture=[[NSDictionary alloc]init];
                        for(eachPosture in Event_Posture_Master_NSArray)
                        {
                            //println(" Each Posture \(eachPosture)")
                            postureSlug = eachPosture[@"Slug"];
                            if (Event_Sub_Category_Master_NSDict.count > 0)
                            {
                                NSArray * eachSubCatEventBunchNSArray  = Event_Sub_Category_Master_NSDict[eventCatMainSlug];
                                
                                if (eachSubCatEventBunchNSArray.count > 0)
                                {
                                    NSMutableDictionary * storingInEachSubCatEventsKey = [[NSMutableDictionary alloc]init];
                                    NSDictionary* eachSubCatEvent=[[NSDictionary alloc]init];
                                    for (eachSubCatEvent in eachSubCatEventBunchNSArray)
                                    {
                                        NSString * nameOfSubCatEventKey    =   eachSubCatEvent[@"Slug"];                                         // Store All Values [Thresholds, Yellow, Red] in key for hash mapping
                                        [storingInEachSubCatEventsKey setObject:[self getClubedSettingArray:eachSubCatEvent postureslug:postureSlug mainevent:eventCatMainSlug] forKey: nameOfSubCatEventKey];
                                        
                                        
                                    }  // End Loop Each Sub Cat Event [Ventricular, and so on . . .]
                                    
                                    //println(" Sub Updated events \(storingInEachSubCatEventsKey)")
                                    
                                    // --------------  Adding [[ storingEachSubCatEventsKey ]] to Posture key
                                    [storingInEachPostureKey setObject:storingInEachSubCatEventsKey forKey: postureSlug];
                                    
                                } // End if Cheking Sub Cat Events
                                
                            } // End if Cheking Sub Cat Events For Hashing
                            
                            
                            
                        } // End of Posture For Loop
                        
                        //println(" Each Posture Updated data \(storingInEachPostureKey)")
                        
                        // --------------  Adding [[ storingInEachPostureKey ]] to Main_Updated_NSMutDictonary_Array key
                        [ Main_Updated_NSMutDictonary_Array setObject:storingInEachPostureKey forKey: eventCatMainSlug];
                        
                        //println(" $$$$$ Main_Updated_NSMutDictonary_Array UPDATED DATA FULL FOR ARRTHYHMIYA AND ISCHEMIA \(Main_Updated_NSMutDictonary_Array)")
                        
                        
                    } // End of Posture Master Count check
                }
                
            } // End of Each Main Cat Event Type For loop
            
        } // End of if Condition Check Main Category
        
    } // End of main array check
    
    
    //  --------  Update the screen
} // End of Function . . .



-(NSMutableDictionary*)getClubedSettingArray:(NSDictionary *)eachSubEventArray postureslug:(NSString *)postureSlugIs  mainevent:(NSString *)mainEventSlugIs
{
   
   
    NSString * doctor_value,*yellow_value,* red_value,*doc_flag,*personal_flag;
    //println("Data is each \(eachSubEventArray)")
    NSString * nameOfSubCatEventName   =   eachSubEventArray[@"Name"];
    //var nameOfSubCatEventKey  =   eachArray["Slug"] as NSString
    
    NSString *inputType               =   eachSubEventArray[@"Input_Type"]; //"TEXT" or "SWITCH"
    NSString * showIn                  =   eachSubEventArray[@"Show_In"];// BOTH, DOCOTR, PERSONAL
    NSNumber *minValue  ;
    NSNumber *val1;
    val1 = eachSubEventArray[@"Min"];
        minValue = val1;
    NSNumber* maxValue;
    NSNumber *val2;
    val2 = eachSubEventArray[@"Max"];
    maxValue = val2;
    
    NSString * sett_code=eachSubEventArray[@"Sett_code"];
    
    if(![globalposturecode isEqualToString:@"BOTH"])
  {
    NSMutableArray *results = [[NSMutableArray alloc]init];
     NSMutableArray *results1 = [[NSMutableArray alloc]init];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",sett_code, @"posture", globalposturecode];
    if (sett_code.length!=0) {
        //pred =  [NSPredicate predicateWithFormat:@"SELF.code == %@",@"2001", @"SELF.posture==%@", @"0"];
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        doctor_value     =    eachSubEventArray[@"Doctor_Value"];
        yellow_value            =    eachSubEventArray[@"Yellow_Value"];
        red_value               =    eachSubEventArray[@"Red_Value"];
        personal_flag=@"";
        doc_flag=@"";
        
    }
    
    if (flag == 1) {
        
        NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
           // NSLog(@"1 - %@", doctorssett);
            if([sett_code isEqualToString:@"2022"])
            {
                NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
                NSString *temperaturetype=[TempDefault objectForKey:@"TEMPERATURE"];
                if([temperaturetype isEqualToString:@"CELSIUS"])
                {
                   // tableViewCellCustom.tempunits.text=[NSString stringWithFormat:@"C%@", @"\u00B0"];
                     doctor_value=[doctorssett valueForKey:@"dd"];
                    doc_flag=[doctorssett valueForKey:@"ddflag"];
                }
                else
                {
                    CommonHelper *help=[[CommonHelper alloc]init];
                   // tableViewCellCustom.tempunits.text=[NSString stringWithFormat:@"F%@", @"\u00B0"];
                   doctor_value=[help celsiustoFahernheit:[doctorssett valueForKey:@"dd"]];
                     doc_flag=[doctorssett valueForKey:@"ddflag"];
                }
           
            }
            else
            {
             doctor_value=[doctorssett valueForKey:@"dd"];
                 doc_flag=[doctorssett valueForKey:@"ddflag"];
            }
            
            NSLog(@"rtrtrt%@",doctor_value);
        } else {
            NSLog(@"Enter Corect code number");
        }
        NSFetchRequest *fetchRequest1 = [[NSFetchRequest alloc]initWithEntityName:@"ThresholdSett"];
        [fetchRequest1 setPredicate:pred];
        results1 = [[self.managedObjectContext executeFetchRequest:fetchRequest1 error:nil] mutableCopy];
        
        if (results1.count > 0) {
            
            NSManagedObject *genthressett = (NSManagedObject *)[results1 objectAtIndex:0];
           // NSLog(@"1 - %@", genthressett);
            if([sett_code isEqualToString:@"2022"])
            {
                NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
                NSString *temperaturetype=[TempDefault objectForKey:@"TEMPERATURE"];
                if([temperaturetype isEqualToString:@"CELSIUS"])
                {
                    // tableViewCellCustom.tempunits.text=[NSString stringWithFormat:@"C%@", @"\u00B0"];
                    yellow_value=[genthressett valueForKey:@"yd"];
                    red_value=[genthressett valueForKey:@"rd"];
                    personal_flag=[genthressett valueForKey:@"flag"];
                }
                else
                {
                    CommonHelper *help=[[CommonHelper alloc]init];
                    // tableViewCellCustom.tempunits.text=[NSString stringWithFormat:@"F%@", @"\u00B0"];
                    yellow_value=[help celsiustoFahernheit:[genthressett valueForKey:@"yd"]];
                    red_value=[help celsiustoFahernheit:[genthressett valueForKey:@"rd"]];
                    personal_flag=[genthressett valueForKey:@"flag"];
                }
                
            }
            else
            {
            yellow_value=[genthressett valueForKey:@"yd"];
            red_value=[genthressett valueForKey:@"rd"];
            personal_flag=[genthressett valueForKey:@"flag"];
            //NSLog(@"ttt%@",yellow_value);
             //NSLog(@"ttt%@",red_value);
            }
        } else {
            NSLog(@"Enter Corect general code number");
        }
        
        
        
    }
      
  }
    else
    {
        // doctor_value     =    eachSubEventArray[@"Doctor_Value"];
         //yellow_value            =    eachSubEventArray[@"Yellow_Value"];
         //red_value               =    eachSubEventArray[@"Red_Value"];
        
       
        NSMutableArray *results = [[NSMutableArray alloc]init];
        NSMutableArray *results2 = [[NSMutableArray alloc]init];
          NSMutableArray *poscode = [[NSMutableArray alloc]init];
        int flag=0;
        NSPredicate *pred;
        NSString* filter = @"%K == %@";
        NSArray* args = @[ @"code",sett_code,];
      
        if (sett_code.length!=0) {
            //pred =  [NSPredicate predicateWithFormat:@"SELF.code == %@",@"2001", @"SELF.posture==%@", @"0"];
            pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
          
            flag=1;
        } else {
            flag=0;
            NSLog(@"Enter Corect code empty");
            doctor_value     =    eachSubEventArray[@"Doctor_Value"];
            yellow_value            =    eachSubEventArray[@"Yellow_Value"];
            red_value               =    eachSubEventArray[@"Red_Value"];
            personal_flag=@"";
            doc_flag=@"";
            
        }
        
        if (flag == 1)
        {
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
            [fetchRequest setPredicate:pred];
           
            results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
          
            if (results.count> 0) {
                
                for(int i=0;i<userSelectedPosturesArray.count;i++)
                {
                    if([userSelectedPosturesArray[i] isEqualToString:@"laying-slug"])
                    {
                        NSNumber* xWrapped = [NSNumber numberWithInt:LayingPostureCode];
                        [poscode addObject:xWrapped];
                    }
                    else if ( [userSelectedPosturesArray[i] isEqualToString:@"standing-sitting-slug"]) // Standing / Sitting
                    {
                        NSNumber* xWrapped = [NSNumber numberWithInt:StandingSittingPostureCode];
                        [poscode addObject:xWrapped];
                    }
                    else if ( [userSelectedPosturesArray[i] isEqualToString:@"walking-slug"] ) // Walking
                    {
                        NSNumber* xWrapped = [NSNumber numberWithInt:WalkingPostureCode];
                        [poscode addObject:xWrapped];

                        
                    }
                    else if ( [userSelectedPosturesArray[i] isEqualToString:@"running-slug"] ) // Running
                    {
                        
                        NSNumber* xWrapped = [NSNumber numberWithInt:RunningPostureCode];
                        [poscode addObject:xWrapped];

                    }
                }
                
                if(userSelectedPosturesArray.count==2)
                {
                    
                    
                    NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:[[poscode firstObject]intValue]];
                      NSManagedObject *doctorssett1 = (NSManagedObject *)[results objectAtIndex:[[poscode lastObject]intValue]];
                    
                   // NSLog(@"dd:%@",[doctorssett valueForKey:@"dd"]);
                    //NSLog(@"dd1:%@",[doctorssett1 valueForKey:@"dd"]);
                    if([[doctorssett valueForKey:@"dd"] isEqualToString:[doctorssett1 valueForKey:@"dd"]])
                    {
                        if([sett_code isEqualToString:@"2022"])
                        {
                            NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
                            NSString *temperaturetype=[TempDefault objectForKey:@"TEMPERATURE"];
                            if([temperaturetype isEqualToString:@"CELSIUS"])
                            {
                                // tableViewCellCustom.tempunits.text=[NSString stringWithFormat:@"C%@", @"\u00B0"];
                                
                                
                                doctor_value=[doctorssett valueForKey:@"dd"];
                                 doc_flag=[doctorssett valueForKey:@"ddflag"];
                                
                            }
                            else
                            {
                                CommonHelper *help=[[CommonHelper alloc]init];
                                // tableViewCellCustom.tempunits.text=[NSString stringWithFormat:@"F%@", @"\u00B0"];
                                doctor_value=[help celsiustoFahernheit:[doctorssett valueForKey:@"dd"]];
                                 doc_flag=[doctorssett valueForKey:@"ddflag"];
                            }
                            
                        }
                        else
                        {
                            doctor_value=[doctorssett valueForKey:@"dd"];
                             doc_flag=[doctorssett valueForKey:@"ddflag"];
                        }
                    }
                    else
                    {
                        doctor_value=@"-";
                         doc_flag=@"0";
                    }
                    
                    
                    NSFetchRequest *fetchRequest2 = [[NSFetchRequest alloc]initWithEntityName:@"ThresholdSett"];
                    [fetchRequest2 setPredicate:pred];
                   
                    results2 = [[self.managedObjectContext executeFetchRequest:fetchRequest2 error:nil] mutableCopy];
                   
                    if (results2.count > 0) {
                    NSManagedObject *genthressett = (NSManagedObject *)[results2 objectAtIndex:[[poscode firstObject]intValue]];
                    NSManagedObject *genthressett1 = (NSManagedObject *)[results2 objectAtIndex:[[poscode lastObject]intValue]];
                    
                    
                    if([[genthressett valueForKey:@"yd"] isEqualToString:[genthressett1 valueForKey:@"yd"]])
                    {
                        if([sett_code isEqualToString:@"2022"])
                        {
                            NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
                            NSString *temperaturetype=[TempDefault objectForKey:@"TEMPERATURE"];
                            if([temperaturetype isEqualToString:@"CELSIUS"])
                            {
                                // tableViewCellCustom.tempunits.text=[NSString stringWithFormat:@"C%@", @"\u00B0"];
                                yellow_value=[genthressett valueForKey:@"yd"];
                                 personal_flag=[genthressett valueForKey:@"flag"];
                                
                            }
                            else
                            {
                                CommonHelper *help=[[CommonHelper alloc]init];
                                // tableViewCellCustom.tempunits.text=[NSString stringWithFormat:@"F%@", @"\u00B0"];
                                yellow_value=[help celsiustoFahernheit:[genthressett valueForKey:@"yd"]];
                                 personal_flag=[genthressett valueForKey:@"flag"];
                                
                            }
                            
                        }
                        else
                        {
                            yellow_value=[genthressett valueForKey:@"yd"];
                             personal_flag=[genthressett valueForKey:@"flag"];
                           // NSLog(@"tttYeloow%@",yellow_value);
                        }
                    }
                    else
                    {
                        yellow_value=@"-";
                         personal_flag=@"-";
                        
                    }
                    
                    
                    if([[genthressett valueForKey:@"rd"] isEqualToString:[genthressett1 valueForKey:@"rd"]])
                    {
                        if([sett_code isEqualToString:@"2022"])
                        {
                            NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
                            NSString *temperaturetype=[TempDefault objectForKey:@"TEMPERATURE"];
                            if([temperaturetype isEqualToString:@"CELSIUS"])
                            {
                                // tableViewCellCustom.tempunits.text=[NSString stringWithFormat:@"C%@", @"\u00B0"];
                                
                                red_value=[genthressett valueForKey:@"rd"];
                                if([personal_flag isEqualToString:@"-"])
                                    personal_flag=@"0";
                                else
                                 personal_flag=[genthressett valueForKey:@"flag"];
                            }
                            else
                            {
                                CommonHelper *help=[[CommonHelper alloc]init];
                                // tableViewCellCustom.tempunits.text=[NSString stringWithFormat:@"F%@", @"\u00B0"];
                                
                                red_value=[help celsiustoFahernheit:[genthressett valueForKey:@"rd"]];
                                if([personal_flag isEqualToString:@"-"])
                                    personal_flag=@"0";
                                else
                                 personal_flag=[genthressett valueForKey:@"flag"];
                            }
                            
                        }
                        else
                        {
                            
                            red_value=[genthressett valueForKey:@"rd"];
                            if([personal_flag isEqualToString:@"-"])
                                personal_flag=@"0";
                            else
                             personal_flag=[genthressett valueForKey:@"flag"];
                           // NSLog(@"tttRed%@",red_value);
                        }
                    }
                    else
                    {
                        
                        red_value=@"-";
                         personal_flag=@"0";
                    }

                    } else {
                        NSLog(@"Enter Corect code number");
                    }
                }
                else if (userSelectedPosturesArray.count==3)
                {
                        
                        
                        NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:[[poscode firstObject]intValue]];
                        NSManagedObject *doctorssett1 = (NSManagedObject *)[results objectAtIndex:[[poscode lastObject]intValue]];
                       NSManagedObject *doctorssett2 = (NSManagedObject *)[results objectAtIndex:[[poscode objectAtIndex:1] intValue]];
                    
                  
                    
                        if([[doctorssett valueForKey:@"dd"] intValue]==[[doctorssett1 valueForKey:@"dd"] intValue] &&[[doctorssett1 valueForKey:@"dd"] intValue]==[[doctorssett2 valueForKey:@"dd"] intValue]&&[[doctorssett valueForKey:@"dd"] intValue]==[[doctorssett2 valueForKey:@"dd"] intValue])
                        {
                            if([sett_code isEqualToString:@"2022"])
                            {
                                NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
                                NSString *temperaturetype=[TempDefault objectForKey:@"TEMPERATURE"];
                                if([temperaturetype isEqualToString:@"CELSIUS"])
                                {
                                    // tableViewCellCustom.tempunits.text=[NSString stringWithFormat:@"C%@", @"\u00B0"];
                                    
                                    
                                    doctor_value=[doctorssett valueForKey:@"dd"];
                                     doc_flag=[doctorssett valueForKey:@"ddflag"];
                                    
                                }
                                else
                                {
                                    CommonHelper *help=[[CommonHelper alloc]init];
                                    // tableViewCellCustom.tempunits.text=[NSString stringWithFormat:@"F%@", @"\u00B0"];
                                    doctor_value=[help celsiustoFahernheit:[doctorssett valueForKey:@"dd"]];
                                     doc_flag=[doctorssett valueForKey:@"ddflag"];
                                }
                                
                            }
                            else
                            {
                                doctor_value=[doctorssett valueForKey:@"dd"];
                                 doc_flag=[doctorssett valueForKey:@"ddflag"];
                            }
                        }
                        else
                        {
                            doctor_value=@"-";
                             doc_flag=@"0";
                        }
                        
                        
                        NSFetchRequest *fetchRequest2 = [[NSFetchRequest alloc]initWithEntityName:@"ThresholdSett"];
                        [fetchRequest2 setPredicate:pred];
                        
                        results2 = [[self.managedObjectContext executeFetchRequest:fetchRequest2 error:nil] mutableCopy];
                        
                        if (results2.count > 0) {
                            NSManagedObject *genthressett = (NSManagedObject *)[results2 objectAtIndex:[[poscode firstObject]intValue]];
                            NSManagedObject *genthressett1 = (NSManagedObject *)[results2 objectAtIndex:[[poscode lastObject]intValue]];
                            NSManagedObject *genthressett2 = (NSManagedObject *)[results2 objectAtIndex:[[poscode objectAtIndex:1]intValue]];
                            
                            if([[genthressett valueForKey:@"yd"] intValue] ==[[genthressett1 valueForKey:@"yd"] intValue]&&[[genthressett1 valueForKey:@"yd"] intValue]==[[genthressett2 valueForKey:@"yd"] intValue]&&[[genthressett valueForKey:@"yd"] intValue]==[[genthressett2 valueForKey:@"yd"] intValue])
                            {
                                if([sett_code isEqualToString:@"2022"])
                                {
                                    NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
                                    NSString *temperaturetype=[TempDefault objectForKey:@"TEMPERATURE"];
                                    if([temperaturetype isEqualToString:@"CELSIUS"])
                                    {
                                        // tableViewCellCustom.tempunits.text=[NSString stringWithFormat:@"C%@", @"\u00B0"];
                                        yellow_value=[genthressett valueForKey:@"yd"];
                                         personal_flag=[genthressett valueForKey:@"flag"];
                                    }
                                    else
                                    {
                                        CommonHelper *help=[[CommonHelper alloc]init];
                                        // tableViewCellCustom.tempunits.text=[NSString stringWithFormat:@"F%@", @"\u00B0"];
                                        yellow_value=[help celsiustoFahernheit:[genthressett valueForKey:@"yd"]];
                                         personal_flag=[genthressett valueForKey:@"flag"];
                                    }
                                    
                                }
                                else
                                {
                                    yellow_value=[genthressett valueForKey:@"yd"];
                                     personal_flag=[genthressett valueForKey:@"flag"];
                                    //NSLog(@"tttYeloow%@",yellow_value);
                                }
                            }
                            else
                            {
                                yellow_value=@"-";
                                 personal_flag=@"-";
                                
                            }
                            
                            
                            if([[genthressett valueForKey:@"rd"] intValue] ==[[genthressett1 valueForKey:@"rd"]intValue]&&[[genthressett1 valueForKey:@"rd"]intValue]==[[genthressett2 valueForKey:@"rd"]intValue]&&[[genthressett valueForKey:@"rd"] intValue]==[[genthressett2 valueForKey:@"rd"]intValue])
                            {
                                if([sett_code isEqualToString:@"2022"])
                                {
                                    NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
                                    NSString *temperaturetype=[TempDefault objectForKey:@"TEMPERATURE"];
                                    if([temperaturetype isEqualToString:@"CELSIUS"])
                                    {
                                        // tableViewCellCustom.tempunits.text=[NSString stringWithFormat:@"C%@", @"\u00B0"];
                                        
                                        red_value=[genthressett valueForKey:@"rd"];
                                        if([personal_flag isEqualToString:@"-"])
                                            personal_flag=@"0";
                                        else
                                         personal_flag=[genthressett valueForKey:@"flag"];
                                    }
                                    else
                                    {
                                        CommonHelper *help=[[CommonHelper alloc]init];
                                        // tableViewCellCustom.tempunits.text=[NSString stringWithFormat:@"F%@", @"\u00B0"];
                                        
                                        red_value=[help celsiustoFahernheit:[genthressett valueForKey:@"rd"]];
                                        if([personal_flag isEqualToString:@"-"])
                                            personal_flag=@"0";
                                        else
                                         personal_flag=[genthressett valueForKey:@"flag"];
                                    }
                                    
                                }
                                else
                                {
                                    
                                    red_value=[genthressett valueForKey:@"rd"];
                                    if([personal_flag isEqualToString:@"-"])
                                        personal_flag=@"0";
                                    else
                                    personal_flag=[genthressett valueForKey:@"flag"];
                                    //NSLog(@"tttRed%@",red_value);
                                }
                            }
                            else
                            {
                                
                                red_value=@"-";
                                 personal_flag=@"0";
                            }
                            
                        } else {
                            NSLog(@"Enter Corect code number");
                        }
                    
                }
                else
                {
                    
                    
                    NSManagedObject *doctorssett =  (NSManagedObject *)[results objectAtIndex:[[poscode firstObject]intValue]];
                    NSManagedObject *doctorssett1 = (NSManagedObject *)[results objectAtIndex:[[poscode lastObject]intValue]];
                    NSManagedObject *doctorssett2 = (NSManagedObject *)[results objectAtIndex:[[poscode objectAtIndex:1] intValue]];
                    NSManagedObject *doctorssett3 = (NSManagedObject *)[results objectAtIndex:[[poscode objectAtIndex:2] intValue]];
                    
                    BOOL isItOpen;
                    isItOpen = ([[doctorssett valueForKey:@"dd"] intValue]==[[doctorssett1 valueForKey:@"dd"] intValue] ? [[doctorssett1 valueForKey:@"dd"] intValue]==[[doctorssett2 valueForKey:@"dd"] intValue]?[[doctorssett2 valueForKey:@"dd"] intValue]==[[doctorssett3 valueForKey:@"dd"] intValue]?YES :NO :NO : NO);
                    
                    if(isItOpen)
                    {
                        if([sett_code isEqualToString:@"2022"])
                        {
                            NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
                            NSString *temperaturetype=[TempDefault objectForKey:@"TEMPERATURE"];
                            if([temperaturetype isEqualToString:@"CELSIUS"])
                            {
                                // tableViewCellCustom.tempunits.text=[NSString stringWithFormat:@"C%@", @"\u00B0"];
                                
                                
                                doctor_value=[doctorssett valueForKey:@"dd"];
                                 doc_flag=[doctorssett valueForKey:@"ddflag"];
                                
                            }
                            else
                            {
                                CommonHelper *help=[[CommonHelper alloc]init];
                                // tableViewCellCustom.tempunits.text=[NSString stringWithFormat:@"F%@", @"\u00B0"];
                                doctor_value=[help celsiustoFahernheit:[doctorssett valueForKey:@"dd"]];
                                 doc_flag=[doctorssett valueForKey:@"ddflag"];
                            }
                            
                        }
                        else
                        {
                            doctor_value=[doctorssett valueForKey:@"dd"];
                            doc_flag=[doctorssett valueForKey:@"ddflag"];
                        }
                    }
                    else
                    {
                        doctor_value=@"-";
                         doc_flag=@"0";
                    }
                    
                    
                    NSFetchRequest *fetchRequest2 = [[NSFetchRequest alloc]initWithEntityName:@"ThresholdSett"];
                    [fetchRequest2 setPredicate:pred];
                    
                    results2 = [[self.managedObjectContext executeFetchRequest:fetchRequest2 error:nil] mutableCopy];
                    
                    if (results2.count > 0) {
                        NSManagedObject *genthressett = (NSManagedObject *)[results2 objectAtIndex:[[poscode firstObject]intValue]];
                        NSManagedObject *genthressett1 = (NSManagedObject *)[results2 objectAtIndex:[[poscode lastObject]intValue]];
                        NSManagedObject *genthressett2 = (NSManagedObject *)[results2 objectAtIndex:[[poscode objectAtIndex:1]intValue]];
                        NSManagedObject *genthressett3 = (NSManagedObject *)[results2 objectAtIndex:[[poscode objectAtIndex:2]intValue]];
                        
                        if([[genthressett valueForKey:@"yd"] intValue] ==[[genthressett1 valueForKey:@"yd"] intValue]&&[[genthressett1 valueForKey:@"yd"] intValue]==[[genthressett2 valueForKey:@"yd"] intValue]&&[[genthressett valueForKey:@"yd"] intValue]==[[genthressett2 valueForKey:@"yd"] intValue])
                        {
                            if([sett_code isEqualToString:@"2022"])
                            {
                                NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
                                NSString *temperaturetype=[TempDefault objectForKey:@"TEMPERATURE"];
                                if([temperaturetype isEqualToString:@"CELSIUS"])
                                {
                                    // tableViewCellCustom.tempunits.text=[NSString stringWithFormat:@"C%@", @"\u00B0"];
                                    yellow_value=[genthressett valueForKey:@"yd"];
                                     personal_flag=[genthressett valueForKey:@"flag"];
                                }
                                else
                                {
                                    CommonHelper *help=[[CommonHelper alloc]init];
                                    // tableViewCellCustom.tempunits.text=[NSString stringWithFormat:@"F%@", @"\u00B0"];
                                    yellow_value=[help celsiustoFahernheit:[genthressett valueForKey:@"yd"]];
                                     personal_flag=[genthressett valueForKey:@"flag"];
                                }
                                
                            }
                            else
                            {
                                yellow_value=[genthressett valueForKey:@"yd"];
                                 personal_flag=[genthressett valueForKey:@"flag"];
                                //NSLog(@"tttYeloow%@",yellow_value);
                            }
                        }
                        else
                        {
                            yellow_value=@"-";
                             personal_flag=@"0";
                            
                        }
                        
                        BOOL GenOpen;
                        GenOpen=[[genthressett valueForKey:@"rd"] intValue] ==[[genthressett1 valueForKey:@"rd"]intValue]?[[genthressett1 valueForKey:@"rd"]intValue]==[[genthressett2 valueForKey:@"rd"]intValue]?[[genthressett2 valueForKey:@"rd"]intValue]==[[genthressett3 valueForKey:@"rd"]intValue]?YES:NO :NO :NO;
                        if(GenOpen)
                        {
                            if([sett_code isEqualToString:@"2022"])
                            {
                                NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
                                NSString *temperaturetype=[TempDefault objectForKey:@"TEMPERATURE"];
                                if([temperaturetype isEqualToString:@"CELSIUS"])
                                {
                                    // tableViewCellCustom.tempunits.text=[NSString stringWithFormat:@"C%@", @"\u00B0"];
                                    
                                    red_value=[genthressett valueForKey:@"rd"];
                                     personal_flag=[genthressett valueForKey:@"flag"];
                                }
                                else
                                {
                                    CommonHelper *help=[[CommonHelper alloc]init];
                                    // tableViewCellCustom.tempunits.text=[NSString stringWithFormat:@"F%@", @"\u00B0"];
                                    
                                    red_value=[help celsiustoFahernheit:[genthressett valueForKey:@"rd"]];
                                     personal_flag=[genthressett valueForKey:@"flag"];
                                }
                                
                            }
                            else
                            {
                                
                                red_value=[genthressett valueForKey:@"rd"];
                                 personal_flag=[genthressett valueForKey:@"flag"];
                                //NSLog(@"tttRed%@",red_value);
                            }
                        }
                        else
                        {
                            
                            red_value=@"-";
                             personal_flag=@"0";
                        }
                        
                    } else {
                        NSLog(@"Enter Corect code number");
                    }
                    
                }
                
            }
            

        }
        else {
            NSLog(@"Enter Corect general code number");
        }
        
        
       /* NSMutableArray *results = [[NSMutableArray alloc]init];
         NSMutableArray *results1 = [[NSMutableArray alloc]init];
        NSMutableArray *results2 = [[NSMutableArray alloc]init];
        NSMutableArray *results3 = [[NSMutableArray alloc]init];
        int flag=0;
        NSPredicate *pred,*pred1;
        NSString* filter = @"%K == %@ && %K == %@";
        NSArray* args = @[ @"posture", @"0",@"code",sett_code,];
        NSArray* args1 = @[ @"posture", @"1",@"code",sett_code,];
        if (sett_code.length!=0) {
            //pred =  [NSPredicate predicateWithFormat:@"SELF.code == %@",@"2001", @"SELF.posture==%@", @"0"];
            pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
             pred1 = [NSPredicate predicateWithFormat:filter argumentArray:args1];
            flag=1;
        } else {
            flag=0;
            NSLog(@"Enter Corect code empty");
            doctor_value     =    eachSubEventArray[@"Doctor_Value"];
            yellow_value            =    eachSubEventArray[@"Yellow_Value"];
            red_value               =    eachSubEventArray[@"Red_Value"];
            
        }
        
        if (flag == 1) {
            
            NSLog(@"predicate: %@,,%@",pred,pred1);
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
            [fetchRequest setPredicate:pred];
             NSFetchRequest *fetchRequest1 = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
             [fetchRequest1 setPredicate:pred1];
            results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
            results1 = [[self.managedObjectContext executeFetchRequest:fetchRequest1 error:nil] mutableCopy];
            if (results.count&&results1.count > 0) {
                
                NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
                NSManagedObject *doctorssett1 = (NSManagedObject *)[results1 objectAtIndex:0];
                // NSLog(@"1 - %@", doctorssett);
                if([[doctorssett valueForKey:@"dd"] isEqualToString:[doctorssett1 valueForKey:@"dd"]])
                {
                if([sett_code isEqualToString:@"2022"])
                {
                    NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
                    NSString *temperaturetype=[TempDefault objectForKey:@"TEMPERATURE"];
                    if([temperaturetype isEqualToString:@"CELSIUS"])
                    {
                        // tableViewCellCustom.tempunits.text=[NSString stringWithFormat:@"C%@", @"\u00B0"];
                        
                       
                            doctor_value=[doctorssett valueForKey:@"dd"];
                        
                    }
                    else
                    {
                        CommonHelper *help=[[CommonHelper alloc]init];
                        // tableViewCellCustom.tempunits.text=[NSString stringWithFormat:@"F%@", @"\u00B0"];
                        doctor_value=[help celsiustoFahernheit:[doctorssett valueForKey:@"dd"]];
                    }
                    
                }
                else
                    doctor_value=[doctorssett valueForKey:@"dd"];
                }
                else
                {
                    doctor_value=@"-";
                }
                
                NSLog(@"rtrtrt%@",doctor_value);
            } else {
                NSLog(@"Enter Corect code number");
            }
            
            
            
            NSFetchRequest *fetchRequest2 = [[NSFetchRequest alloc]initWithEntityName:@"ThresholdSett"];
            [fetchRequest2 setPredicate:pred];
            NSFetchRequest *fetchRequest3 = [[NSFetchRequest alloc]initWithEntityName:@"ThresholdSett"];
            [fetchRequest3 setPredicate:pred1];
            results2 = [[self.managedObjectContext executeFetchRequest:fetchRequest2 error:nil] mutableCopy];
            results3 = [[self.managedObjectContext executeFetchRequest:fetchRequest3 error:nil] mutableCopy];
            if (results2.count &&results3.count > 0) {
                
                NSManagedObject *genthressett = (NSManagedObject *)[results2 objectAtIndex:0];
                NSManagedObject *genthressett1 = (NSManagedObject *)[results3 objectAtIndex:0];
               
                
                if([[genthressett valueForKey:@"yd"] isEqualToString:[genthressett1 valueForKey:@"yd"]])
                {
                if([sett_code isEqualToString:@"2022"])
                {
                    NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
                    NSString *temperaturetype=[TempDefault objectForKey:@"TEMPERATURE"];
                    if([temperaturetype isEqualToString:@"CELSIUS"])
                    {
                        // tableViewCellCustom.tempunits.text=[NSString stringWithFormat:@"C%@", @"\u00B0"];
                        yellow_value=[genthressett valueForKey:@"yd"];
                      
                    }
                    else
                    {
                        CommonHelper *help=[[CommonHelper alloc]init];
                        // tableViewCellCustom.tempunits.text=[NSString stringWithFormat:@"F%@", @"\u00B0"];
                        yellow_value=[help celsiustoFahernheit:[genthressett valueForKey:@"yd"]];
                        
                    }
                    
                }
                else
                {
                    yellow_value=[genthressett valueForKey:@"yd"];
                   
                    NSLog(@"ttt%@",yellow_value);
                                   }
                }
                else
                {
                    yellow_value=@"-";
                    
                    
                }
                
                
                if([[genthressett valueForKey:@"rd"] isEqualToString:[genthressett1 valueForKey:@"rd"]])
                {
                    if([sett_code isEqualToString:@"2022"])
                    {
                        NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
                        NSString *temperaturetype=[TempDefault objectForKey:@"TEMPERATURE"];
                        if([temperaturetype isEqualToString:@"CELSIUS"])
                        {
                            // tableViewCellCustom.tempunits.text=[NSString stringWithFormat:@"C%@", @"\u00B0"];
                           
                            red_value=[genthressett valueForKey:@"rd"];
                        }
                        else
                        {
                            CommonHelper *help=[[CommonHelper alloc]init];
                            // tableViewCellCustom.tempunits.text=[NSString stringWithFormat:@"F%@", @"\u00B0"];
                            
                            red_value=[help celsiustoFahernheit:[genthressett valueForKey:@"rd"]];
                        }
                        
                    }
                    else
                    {
                       
                        red_value=[genthressett valueForKey:@"rd"];
                      
                        NSLog(@"ttt%@",red_value);
                    }
                }
                else
                {
                   
                    red_value=@"-";
                    
                }
                
                
                
            } else {
                NSLog(@"Enter Corect general code number");
            }
            
            
            
        }*/

    }
    
  
    
    BOOL doctor_switch           =    [eachSubEventArray[@"Doctor_Switch"] boolValue];
    BOOL yellow_switch           =    [eachSubEventArray[@"Yellow_Switch"] boolValue];
    BOOL red_switch              =    [eachSubEventArray[@"Red_Switch"]boolValue];
    
    NSArray * read_array;
    if([sett_code isEqualToString:@"2022"])
    {
        NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
        NSString *temperaturetype=[TempDefault objectForKey:@"TEMPERATURE"];
        if([temperaturetype isEqualToString:@"CELSIUS"])
        {
         read_array= eachSubEventArray[@"Read_array"];
        }
        else
        {
            read_array=@[@0,@95.0f,@96.8f,@98.6f,@100.4f,@102.2f,@104.0f,@105.8f,@107.6f,@109.4f,@111.2f];
        }
    }
    else
    {
     read_array= eachSubEventArray[@"Read_array"];
    }
    
    //println(" For @@ \(eventCatMainSlug) @@ == @@ \(postureSlug) @@ == @@ \(nameOfSubCatEventName) @@")
    //println(" For @@ \(eventCatMainSlug) @@ == @@ == @@ \(nameOfSubCatEventName) @@")
    
    // ------  Dictonary to Store values
    
    // Remove Following line when Actual data comes in
   // NSString *doct_value_temp, *yellow_value_temp,*red_value_temp;
    
    NSMutableDictionary * storingEachSubCatEventsValue    = [[NSMutableDictionary alloc]init];
    [storingEachSubCatEventsValue setObject:nameOfSubCatEventName  forKey: @"name"];
    [storingEachSubCatEventsValue setObject:doctor_value    forKey:@"threshold_value"];
    [storingEachSubCatEventsValue setObject:yellow_value    forKey: @"yellow_alert_value"];
    [storingEachSubCatEventsValue setObject:red_value      forKey: @"red_alert_value"];
    
    [storingEachSubCatEventsValue setObject:[NSNumber numberWithBool:doctor_switch]  forKey:@"threshold_switch"];
    [storingEachSubCatEventsValue setObject:[NSNumber numberWithBool:yellow_switch]    forKey:@"yellow_alert_switch"];
   [storingEachSubCatEventsValue setObject:[NSNumber numberWithBool:red_switch]   forKey:@"red_alert_switch"];
    
    
    [storingEachSubCatEventsValue setObject:minValue  forKey: @"min"];
    [storingEachSubCatEventsValue setObject:maxValue  forKey: @"max"];
    [storingEachSubCatEventsValue setObject:showIn    forKey: @"show_in"];
    [storingEachSubCatEventsValue setObject:inputType forKey: @"input_type"];
    
    [storingEachSubCatEventsValue setObject:read_array forKey: @"Read_array"];
    [storingEachSubCatEventsValue setObject:sett_code forKey: @"Sett_code"];
    
    [storingEachSubCatEventsValue setObject:doc_flag forKey: @"doc_flag"];
    [storingEachSubCatEventsValue setObject:personal_flag forKey: @"personal_flag"];
   
    //println("Store data is each \(storingEachSubCatEventsValue)")
    
    return storingEachSubCatEventsValue;
}

// MARK: - Rounded VIEW ACTIONS AND METHODS

-(IBAction)changeMainCategoryEvent:(id)sender
{
    /*[appDelegate doctorSessionFlagBool] = false
    if (appDelegate.doctorSessionFlagBool == false)
    {
        
    }
    else
    {
        // Session and Timer is valid
    }*/
    
    
    //println("Show Doctor login view")
    
    // Set Dropdown in use
    dropDownInUse =MainCategoryDropDown;
    
    // To execute the Further Process first we have to Check the dotor authentication
    mainCatDropDownBlockView.hidden     =   false;
    mainCatPickerControl.delegate       =   self;
    mainCatPickerControl.dataSource     =   self;
    [mainCatPickerControl reloadAllComponents];
}

-(IBAction)dropDownDecidedAction:(UIButton *)sender {
    
    mainCatDropDownBlockView.hidden = true;
    if ([sender tag] == 1 )
    {
        // Cancel tag
    }
    else
    {
        // Use / Done tag
       // NSString * catEventName = [[Event_Category_Master_NSArray objectAtIndex:currentSelectedRowIdForMainEventCat]objectForKey:@"Name"];
        //NSString * catEventSlug = [[Event_Category_Master_NSArray objectAtIndex:currentSelectedRowIdForMainEventCat]objectForKey:@"Slug"];
        
        //println("Name == \(catEventName)  With Slug == \(catEventSlug)")
        
        [self updateWholeScreenBaseOnMainEvent];
    }
    
}

-(void) prepareCellStatusData:(NSMutableArray *)currentCellData
{
    if (currentCellData.count > 0)
    {
        if (cellRowStatus == nil) {
            cellRowStatus =[[NSMutableArray alloc]init];
        } else {
            [cellRowStatus removeAllObjects ];
        }
        
        //int countSection = 0;
        
        
        //cellRowStatus = (count: 4, repeatedValue: 0)
        
        for (id sampleObj in currentCellData)
        {
            [cellRowStatus addObject:[NSNumber numberWithInt:0]];
        }
        
        
        
       
        
        //println("Cell Status 1 \(allData)")
        
        //[cellRowStatus addObjectsFromArray:allData];
    
        //println("Array data 1 \(cellRowStatus)")
        
        
        
          }
    
    //println("Current Data Row Status \(cellRowStatus)")
}


-(void)updateWholeScreenBaseOnMainEvent
{
    // Change Event Button tilte for below two lines
    NSString * catEventName =[[Event_Category_Master_NSArray objectAtIndex:currentSelectedRowIdForMainEventCat]objectForKey:@"Name"];
    //println("Select Main Event Name \(catEventName) And Index \(currentSelectedRowIdForMainEventCat)")
    [selectMainEventThresholdButton setTitle:catEventName.uppercaseString forState:UIControlStateNormal];
    // Set the Slug in Global Variable "userSelectedMainCategoryEvent" and use it while user trying to edit it
   NSString * catEventSlug = [[Event_Category_Master_NSArray objectAtIndex:currentSelectedRowIdForMainEventCat]objectForKey:@"Slug"];
    userSelectedMainCategoryEvent = catEventSlug;
    
    
    // --- : User will select the Main Event As Arrythmia or Ischemia or Other Any one (single) at time, so bases on selection ... conti . . .
    // --- : We will change the Cell_Data_MutableArray
    Cell_Data_MutableArray = [Event_Sub_Category_Master_NSDict  objectForKey:userSelectedMainCategoryEvent];
    //NSLog(@"celddddd%@",Cell_Data_MutableArray);
    //println("Cell_Data_MutableArray \(Cell_Data_MutableArray)")
    
    // --- : Here we are getting number of cells to show in tableview
    // --- : And our table cells will be in collaps and expand, to keep there track we are preparing new array data call Cell
    [self prepareCellStatusData:Cell_Data_MutableArray];

    
    
    if ( [userSelectedMainCategoryEvent isEqualToString:@"arrhythmia-slug"] || [userSelectedMainCategoryEvent isEqualToString:@"ischemia-slug" ] )
    {
        // Get or Set For Arrhythmia
        // --- : Remove All Object First and Add only first index by default
        // --- :  By Default we are setting Laying as pre selected
        //userSelectedPosturesArray.removeAllObjects()
        [self whenOtherEventIsSelected:false];
        [self clickOnAnyPosture:layingButton];
    }
    else if ( [userSelectedMainCategoryEvent isEqualToString:@"ischemia-slug"] )
    {
        // Get or Set For Ischemia
        // --- :  By Default we are setting Laying as pre selected
   
        
    }
    else if ( [userSelectedMainCategoryEvent isEqualToString:@"other-events-slug" ] )
    {
        // Get or Set For Other events
        [self whenOtherEventIsSelected:true];
         globalposturecode=@"1";
    }
    
    
    
    
    
    // Reload Tabel View [When user select Main Event]
    [self reloadTableViewCommonFun];
    [tableViewDoctorSettings scrollToRowAtIndexPath: [NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop  animated: true];
}

-(void) reloadTableViewCommonFun
{
    [tableViewDoctorSettings reloadData];
}



-(NSString* ) getOnOffValue:(int)forValue
{
    return (forValue == 0)? @"OFF" : (forValue == 1) ? @"ON" : @"OFF";
}


-(void)whenOtherEventIsSelected:(BOOL)hide
{
    BOOL useBool = false;
    // This method will call when Other Event is selected
    // Remove previous selection and  Disable all Posture so use can't selected any Postures
    [userSelectedPosturesArray removeAllObjects];
    if (hide == true)
    {
        // Other Event where no Posture is used.
        userSelectedMainSinglePostureSlug = @"no-postures";
    }
    
    
    layingButton.selected             = useBool;
    standingSittingButton.selected    = useBool;
    runningButton.selected            = useBool;
    walkingButton.selected            = useBool;
    
    // Disable all Postures
    layingButton.userInteractionEnabled             = !hide;
    standingSittingButton.userInteractionEnabled    = !hide;
    runningButton.userInteractionEnabled            = !hide;
    walkingButton.userInteractionEnabled            = !hide;
    
    // Change it gray , so that user will get an idea that it is disable
    layingButton.enabled             = !hide;
    layingButton.enabled             = !hide;
    standingSittingButton.enabled    = !hide;
    runningButton.enabled            = !hide;
    walkingButton.enabled            = !hide;
    
    
    
    
}

-(IBAction)clickOnAnyPosture :(UIButton *)sender
{
    // Can be multiselect While choosing Posture
    NSString * slugName;
    if ( [sender tag] == 0 ) // Laying
    {
       
        slugName = @"laying-slug";
    }
    else if ( [sender tag] == 1 ) // Standing / Sitting
    {
        
        slugName =@"standing-sitting-slug";
    }
    else if ( [sender tag] == 2 ) // Walking
    {
        
        slugName = @"walking-slug";
    }
    else if ( [sender tag] == 3 ) // Running
    {
       
        slugName = @"running-slug";
    }
    else
    {
        
    }
    
    if(sender.selected == false)
    {
        [userSelectedPosturesArray addObject:slugName];
        [sender setSelected:true];
    }
    else
    {
        [userSelectedPosturesArray removeObject:slugName];
        [sender setSelected:false];
        // --- : if ALL POSTURES ARE REMOVED THAN SET THE DEFAULT AS "LAYING POSTURE"
        if(userSelectedPosturesArray.count == 0)
        {
            [self clickOnAnyPosture:layingButton];
            return;
            // return for executing the same once
        }
    }
    
    if(userSelectedPosturesArray.count == 1)
    {
        userSelectedMainSinglePostureSlug = userSelectedPosturesArray.firstObject;
        if([userSelectedMainSinglePostureSlug isEqualToString:@"laying-slug"])
        {
            globalposturecode=@"1";
        }
       else if([userSelectedMainSinglePostureSlug isEqualToString:@"standing-sitting-slug"])
        {
            globalposturecode=@"2";
        }
       else if([userSelectedMainSinglePostureSlug isEqualToString:@"walking-slug"])
       {
           globalposturecode=@"3";
       }
        else
        {
              globalposturecode=@"4";
        }
         [self prepareScreenDataMainFunc];
        //println("Single Posture Slug Selected is ==  \(userSelectedMainSinglePostureSlug)")
    }
    else if(userSelectedPosturesArray.count>1)
    {
         userSelectedMainSinglePostureSlug = userSelectedPosturesArray.firstObject;
        globalposturecode=@"BOTH";
        [self prepareScreenDataMainFunc];
    }
    
    
    
    
    //println("Update userSelectedPosturesArray \(userSelectedPosturesArray)")
    
    
    // Changing the divider color
    UIColor *whiteColor = UIColor.lightGrayColor;
    
    // Default make All tab Blue in Color
    [self makeAllTabDividerBlueInColor];
    
    if([userSelectedPosturesArray containsObject:@"laying-slug"] && [userSelectedPosturesArray containsObject:@"standing-sitting-slug" ])
    {
        tabDivider2.backgroundColor = whiteColor;
    }
    if([userSelectedPosturesArray containsObject:@"standing-sitting-slug"] && [userSelectedPosturesArray containsObject:@"walking-slug" ])
    {
        tabDivider3.backgroundColor = whiteColor;
    }
    if([userSelectedPosturesArray containsObject:@"walking-slug"] && [userSelectedPosturesArray containsObject:@"running-slug" ])
    {
        tabDivider4.backgroundColor = whiteColor;
    }
    
    
    // Reload Tabel View [When user select Main Event]
    [self reloadTableViewCommonFun];
    
}

-(IBAction)lockUnlockAction:(UIButton *)sender {
    
     GlobalVars *global = [GlobalVars sharedInstance];
    if (sender.selected == false)
    {
        
        // --- : Before Unlocking the setting lock, Doctor authentication is mandatory in case of [MCM is conneted with MCC] and bypass [Doctor authentication] in case of stand alone mode
        
        if(global.BluetoothConnectionflag==2)
        {
            [self presentAlertViewForPassword];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                            message:@"Please Connect MCD Device"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }
    else
    {
        global.dropdownValidFlag=false;
        lockunlocButton.selected=false;
        [self DoctorUniversalLoginTimer:NO];
    }

    
    
}

-(void)showAlertBox:(NSString*)title msg:(NSString *)message
{
    UIAlertView* alertBox = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"Do it later" otherButtonTitles:@"", nil ];
    [alertBox show];
}
-(void)showValidationAlertBox:(NSString*)title msg:(NSString *)message
{
    UIAlertView* alertBox = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil ];
    [alertBox show];

}




-(void) restricUserInputsOnEdit:(CustomTextField *) textField
{
   NSLog(@"Text Editing %@",textField.text);
    NSString * value = textField.text;
    if((value.length == 1 && value.intValue == 0) || value.length > 3)
    {
        textField.text = [value substringToIndex:(value.length-1)];
    }
}


-(BOOL)checkTheThresholdValueIsInRange:(int)checkValue MINvalue:(int) minValue  MAXvalue:(int) maxValue
{
    /*
     For Each Threshold we have different value and range, so we need to first check the value is given range
     */
    if (checkValue >= minValue && checkValue <= maxValue)
    {
        // It is in Range we have predecided
        return true;
    }
    return false;
}

/*-(void)checkRangeWithOtherThresholds:(int) currentValue  Doctpast:(int)doctorPastValue  Yellowpast:(int)yellowPastValue  RedPAst:(int)redPastValue)
{
    
}*/

// MARK: - UITextField Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return Cell_Data_MutableArray.count;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if([cellRowStatus[indexPath.row]intValue] == 1)
    {
        return 218;
    }
    return 55;
}



- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}



/*func tableView(tableView: UITableView!, viewForFooterInSection section: Int) -> UIView! {
 //        var headerView : UIView = UIView(frame: CGRectMake(15, 15, 250, 30.6))
 //        headerView.backgroundColor = UIColor.yellowColor()
 
 
 return UIView()
 }*/



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString  *cellIdentifier;
    
    cellIdentifier = @"DoctorSettingCellID";
    
    doctorSettingTableViewCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    
    if(doctorSettingTableViewCell == nil)
    {
        doctorSettingTableViewCell =[[DoctorSettingTableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
    }
    doctorSettingTableViewCell.userInteractionEnabled=true;
    doctorSettingTableViewCell.subCatValuePickerView.hidden = true;
    
    BOOL valueIs    = true;
    CGFloat angleValue     =   6.14;
    if([cellRowStatus[indexPath.row]intValue] == 1)
    {
        doctorSettingTableViewCell.subCatValuePickerView.hidden = false;
        valueIs     =   false;
        angleValue  =   7.85;
    }
    
    doctorSettingTableViewCell.rightBlackArrow.transform = CGAffineTransformMakeRotation(angleValue);
    
    // Bind Data To Each Cell
    NSDictionary * eachRowData = [Cell_Data_MutableArray objectAtIndex:indexPath.row];
    /*
     - Input_Type = "TEXT" or "SWITCH"
     -   if "TEXT" means it is normal, and will appear in [Doctor] & [Personal]
     -   if "SWITCH" means it is normal, and will appear only in [Personal]
     */
    NSDictionary * eachSubEventDictonary=[[NSDictionary alloc]init];
    NSString * Input_Type      = [eachRowData objectForKey:@"Input_Type"];
    NSString * Slug            = [eachRowData objectForKey:@"Slug"];
    
    
    UILabel *doctorLabel     = doctorSettingTableViewCell.doctorValueLabel;
    UILabel * yellowLabel     = doctorSettingTableViewCell.yellowValueLabel;
    UILabel * redLabel        = doctorSettingTableViewCell.redValueLabel;
    UILabel * redSingleLabel  = doctorSettingTableViewCell.redSingleValueLabel;
    
    UIButton * yelloweventoff        = doctorSettingTableViewCell.YellowEventOnOff;
    UIButton * redeventoff  = doctorSettingTableViewCell.RedEventOnOff;
    
    // Hide All Labels lable at first
    doctorLabel.hidden      =   true;
    yellowLabel.hidden      =   true;
    redLabel.hidden         =   true;
    redSingleLabel.hidden   =   true;
    
    yelloweventoff.hidden=true;
    redeventoff.hidden=true;
    
    
    if ([Input_Type isEqualToString:@"TEXT"])
    {
        //doctorSettingTableViewCell.doctorAndPersonalMainView.hidden = false
        
        
        
        NSString *doctValue, *yellowValue,*redValue;
        
        NSString * tempSlug  = @" [[[ multiple-slug ]]]";
        
        
        /*if(userSelectedPosturesArray.count > 1) // Multiple posture is selected with Either Arrythmia, OR Ischemia
         {
         // --- : Here there are multiple postures so just show the common value
         doctValue   = @"";
         yellowValue = @"";
         redValue    = @"";
         }*/
        //else
        //{
        // Can be zero or one, and zero will be only in case of Other events, and not for Arrythmia and Ischemia
        /*
         Here in Arraythmia and Ischemia, we have Potures [Lying, walking, runnimg, standing-sitting]
         and not in Other events
         so first we will check the slugs for one Others slug and will code accordinly
         */
        
        
        if (userSelectedPosturesArray.count == 0 && [userSelectedMainCategoryEvent isEqualToString:@"other-events-slug"])
        {
            tempSlug = @"NO-POSTURE";
            // ----  Only for Others Events
            eachSubEventDictonary = [[Main_Updated_NSMutDictonary_Array objectForKey:userSelectedMainCategoryEvent]objectForKey:Slug];
        }
        else
        {
            // ----  Only for Arrythmia and Ischemia
            tempSlug = userSelectedMainSinglePostureSlug;
            // Here Posture is Single selected so we have to show the value of that particular posture
            
            eachSubEventDictonary =   [[[Main_Updated_NSMutDictonary_Array objectForKey:userSelectedMainCategoryEvent]objectForKey:userSelectedMainSinglePostureSlug]objectForKey:Slug];
        }
        
        
        
        // Setting value in the common at end as below
        doctValue   = [eachSubEventDictonary objectForKey:@"threshold_value"];
        yellowValue = [eachSubEventDictonary objectForKey:@"yellow_alert_value"];
        redValue    = [eachSubEventDictonary objectForKey:@"red_alert_value"];
        
        doctValue   = ([doctValue integerValue] == 0)? @"OFF" : doctValue;
        yellowValue = (yellowValue.integerValue == 0) ?@"OFF" : yellowValue;
        redValue    = (redValue.integerValue    == 0) ? @"OFF" : redValue;
        // Show All three Label
        
        

        
        doctorLabel.hidden  =   false;
        yellowLabel.hidden  =   false;
        redLabel.hidden     =   false;
        
        yelloweventoff.hidden=false;
        redeventoff.hidden=false;
        
        
        // Set the value Respectively Labels
        doctorLabel.text  = doctValue;
        yellowLabel.text  = yellowValue;
        redLabel.text     = redValue;
        
        
    }
    else if ([Input_Type isEqualToString:@"SWITCH"])
    {
        
        // Hide Red Single lable
        
        NSString * redValue ;
        
        //var eachSubEventDictonary : NSDictionary!
        if (userSelectedPosturesArray.count == 0 && [userSelectedMainCategoryEvent isEqualToString:@"other-events-slug"])
        {
            // tempSlug = "NO-POSTURE"
            // ----  Only for Others Events
            eachSubEventDictonary =[[Main_Updated_NSMutDictonary_Array objectForKey:userSelectedMainCategoryEvent]objectForKey:Slug];
            
        }
        else
        {
            // ----  Only for Arrythmia and Ischemia
            // tempSlug = userSelectedMainSinglePostureSlug
            // Here Posture is Single selected so we have to show the value of that particular posture
            
            eachSubEventDictonary =   [[[Main_Updated_NSMutDictonary_Array objectForKey:userSelectedMainCategoryEvent]objectForKey:userSelectedMainSinglePostureSlug]objectForKey:Slug];
        }
        
        // Setting value in the common at end as below
        
         if([[eachSubEventDictonary objectForKey:@"name"] isEqualToString:@"HR Suspension"]||[[eachSubEventDictonary objectForKey:@"name"] isEqualToString:@"ST Suspension"])
         {
           
             redValue                =   [eachSubEventDictonary objectForKey:@"threshold_value"];
             if([redValue isEqualToString:@"_"])
             {
                 NSString * displayValueAs      =  @"OFF" ;
                 // Hide Red Single lable
                 redSingleLabel.hidden   =   false;
                 redSingleLabel.text     =   displayValueAs;
             }
             else
             {
                 NSString * displayValueAs      =  redValue ;
                 // Hide Red Single lable
                 redSingleLabel.hidden   =   false;
                 redSingleLabel.text     =   displayValueAs;
             }
             
         }
        else
        {
            
            redValue                =   [eachSubEventDictonary objectForKey:@"red_alert_value"];
            NSString * displayValueAs      =   [self getOnOffValue:[redValue intValue]];
            // Hide Red Single lable
            redSingleLabel.hidden   =   false;
            redSingleLabel.text     =   displayValueAs;
        }
       
    }
    
    
    
    // All my UIPicker Code will be here
    dropDownInUse = SubCategoryValueDropDown;
    
    CustomUIPickerView *pickerViewControl =doctorSettingTableViewCell.subCatValuePickerView;

    pickerViewControl.showsSelectionIndicator   =   true;
    pickerViewControl.inputTypeCom3orCom1       =   Input_Type ;
    pickerViewControl.passDictArray  = eachSubEventDictonary;
   pickerViewControl.delegate        = self;
    // Keys or Slug to store the data (for identification)
   pickerViewControl.passMainCatSlug       =   userSelectedMainCategoryEvent;
   pickerViewControl.passMainSubCatSlug    =   Slug;
    
    [pickerViewControl  reloadAllComponents];
    
    
    if ([Input_Type isEqualToString:@"TEXT"])
    {
        
       // int minValue                =   [[eachSubEventDictonary objectForKey:@"min"] intValue];
       // int maxValue                =  [[eachSubEventDictonary objectForKey:@"max"] intValue];
        NSInteger doctValue               =   [[eachSubEventDictonary objectForKey:@"threshold_value"]integerValue];
        NSInteger yellowValue             =  [[eachSubEventDictonary objectForKey:@"yellow_alert_value"] integerValue];
        NSInteger redValue                =   [[eachSubEventDictonary objectForKey:@"red_alert_value"] integerValue];
        
        
        
        /*  NSMutableArray *arrayData_two=[[NSMutableArray alloc]init];
         int i;
         for(i=minValue;i<=maxValue;i++)
         {
         [arrayData_two addObject:[NSNumber numberWithInt:i]];
         }*/
        
        
        //NSMutableArray *arrayData_one       =   [[NSMutableArray alloc]initWithObjects:@"0", nil];
        //arrayData_one = [NSMutableArray arrayWithArray:[arrayData_one arrayByAddingObjectsFromArray:arrayData_two]];
        // Zero is for Off, when user select Zero the Value will be Off [0 == Off]
        
        
        NSMutableArray *arrayData_one       =   [[NSMutableArray alloc]init];
        NSInteger rowOfDoctor,rowOfYellow,rowOfRed;
        NSString *setcode;
        arrayData_one=[eachSubEventDictonary objectForKey:@"Read_array"];
        setcode=[eachSubEventDictonary objectForKey:@"Sett_code"];
       
        if([setcode isEqualToString:@"2022"])
        {
            float doctV               =   [[eachSubEventDictonary objectForKey:@"threshold_value"]floatValue];
            float yellowV             =  [[eachSubEventDictonary objectForKey:@"yellow_alert_value"] floatValue];
            float redV               =   [[eachSubEventDictonary objectForKey:@"red_alert_value"] floatValue];
            
            rowOfDoctor = [arrayData_one indexOfObject:[NSNumber numberWithFloat:doctV]];
            rowOfYellow = [arrayData_one indexOfObject:[NSNumber numberWithFloat:yellowV]];//[NSString stringWithFormat:@"%d",yellowValue]];
            rowOfRed = [arrayData_one indexOfObject:[NSNumber numberWithFloat:redV]];// [NSString stringWithFormat:@"%d",redValue]];

           
        }
        else
        {
         if([setcode isEqualToString:@"2002"]||[setcode isEqualToString:@"2001"])
         {
             
             
            NSDecimalNumber *doctV = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"0.%ld",(long)doctValue]];
            
             NSDecimalNumber * yellowV=[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"0.%ld",(long)yellowValue]];
  
             NSDecimalNumber * redV = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"0.%ld",(long)redValue]];
  
             
             rowOfDoctor = [arrayData_one indexOfObject:doctV];
             rowOfYellow = [arrayData_one indexOfObject:yellowV] ;

             rowOfRed = [arrayData_one indexOfObject:redV];
             
             doctorLabel.text  =(doctValue  == 0)? @"OFF" :[NSString stringWithFormat:@"0.%ld", (long)doctValue];
             yellowLabel.text  = (yellowValue == 0) ?@"OFF" :[NSString stringWithFormat:@"0.%ld",(long)yellowValue];
             redLabel.text     = (redValue    == 0) ? @"OFF" :[NSString stringWithFormat:@"0.%ld",(long)redValue];
             
            
         }
         else
         {
        rowOfDoctor = [arrayData_one indexOfObject:[NSNumber numberWithInteger :doctValue] ];//[NSString stringWithFormat:@"%d",doctValue]];
        rowOfYellow = [arrayData_one indexOfObject:[NSNumber numberWithInteger:yellowValue]];//[NSString stringWithFormat:@"%d",yellowValue]];
        rowOfRed = [arrayData_one indexOfObject:[NSNumber numberWithInteger:redValue]];// [NSString stringWithFormat:@"%d",redValue]];
             
            
         }
        }
        
        
       /* NSLog(@"rowof%ld",(long)rowOfDoctor);
        NSLog(@"rowofyllow%ld",(long)rowOfYellow);
        NSLog(@"rowofred%ld",(long)rowOfRed);*/
        
        // Set components value Doctor Value
        
        //pickerview data
        
        
        pickcom1data=[[NSMutableArray alloc]init];
        pickcom2data=[[NSMutableArray alloc]init];
        pickcom3data=[[NSMutableArray alloc]init];
        
        
        
        pickcom1data=arrayData_one ;
        
        
        UIColor* doctorTextColor  = (doctValue    == 0) ? UIColor.grayColor : [UIColor blackColor];
        UIColor * yellowTextColor = (yellowValue  == 0) ? UIColor.grayColor : yellowUIColor;
        UIColor * redTextColor    = (redValue     == 0) ? UIColor.grayColor : redUIColor;
        
        
        doctorLabel.textColor   = doctorTextColor;
        yellowLabel.textColor   = yellowTextColor;
        redLabel.textColor      = redTextColor;
        
        [pickerViewControl  selectRow:rowOfDoctor   inComponent:0 animated:false];
        
        
        // Set components value Doctor Value
        [pickerViewControl  selectRow:rowOfYellow inComponent:1 animated:false];
        
        
        // Set components value Doctor Value
        [pickerViewControl  selectRow:rowOfRed inComponent:2 animated:false];
        
      
         doctorSettingTableViewCell.YellowEventOnOff.selected = [[eachSubEventDictonary objectForKey:@"personal_flag"]boolValue];
        doctorSettingTableViewCell.YellowEventOnOff.tag = indexPath.row;
        [doctorSettingTableViewCell.YellowEventOnOff setImage:[UIImage imageNamed:@"notselected.png"] forState:UIControlStateNormal];
        [doctorSettingTableViewCell.YellowEventOnOff setImage:[UIImage imageNamed:@"selected.png"] forState:UIControlStateSelected];

        [ doctorSettingTableViewCell.YellowEventOnOff addTarget:self action:@selector(YellowClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        
    }
    else if ([Input_Type isEqualToString:@"SWITCH"])
    {
       
        // Set components value Doctor Value
      
        
        if([[eachSubEventDictonary objectForKey:@"name"] isEqualToString:@"HR Suspension"]||[[eachSubEventDictonary objectForKey:@"name"] isEqualToString:@"ST Suspension"])
        {
             NSMutableArray *arrayData_one       =   [[NSMutableArray alloc]init];
             arrayData_one=[eachSubEventDictonary objectForKey:@"Read_array"];
             int redValueOnly                =   [[eachSubEventDictonary objectForKey:@"threshold_value"]intValue];
             NSInteger  redvalueindex = [arrayData_one indexOfObject:[NSNumber numberWithInteger :redValueOnly] ];
              [pickerViewControl selectRow:redvalueindex inComponent:0 animated:false];
            UIColor * redSingleTextColor  = [UIColor blackColor];
            redSingleLabel.textColor = redSingleTextColor;
        }
        else
        {
             int redValueOnly                =   [[eachSubEventDictonary objectForKey:@"red_alert_value"]intValue];
              [pickerViewControl selectRow:redValueOnly inComponent:0 animated:false];
        UIColor * redSingleTextColor  = (redValueOnly == 0) ? UIColor.grayColor : UIColor.greenColor;
        redSingleLabel.textColor = redSingleTextColor;
        }
        
    }
    
   
    doctorSettingTableViewCell.subEventNameLabel.text                       =   [eachRowData objectForKey:@"Name"];
    doctorSettingTableViewCell.subEventNameLabel.adjustsFontSizeToFitWidth  =   true;
    
    doctorSettingTableViewCell.subEventMeasureLabel.text                    =   [eachRowData objectForKey:@"Measure"];
    
    doctorSettingTableViewCell.clipsToBounds = true;
    
    return doctorSettingTableViewCell;
}


-(void)YellowClicked:(UIButton*)sender
{
    NSLog(@"%ld",(long)sender.tag);
     GlobalVars *global = [GlobalVars sharedInstance];
    if(global.dropdownValidFlag)
    {
    NSDictionary * eachRowData = [Cell_Data_MutableArray objectAtIndex:sender.tag];
      NSString * Slug            = [eachRowData objectForKey:@"Slug"];
        NSString *yeloflag;
    
    
    if([sender isSelected])
    {
        yeloflag=@"0";
        [[[[Main_Updated_NSMutDictonary_Array objectForKey:userSelectedMainCategoryEvent]objectForKey:userSelectedMainSinglePostureSlug]objectForKey:Slug]setObject:yeloflag forKey:@"personal_flag"];
    }
    
    else
    {
        yeloflag=@"1";
        [sender setSelected:YES];
          [[[[Main_Updated_NSMutDictonary_Array objectForKey:userSelectedMainCategoryEvent]objectForKey:userSelectedMainSinglePostureSlug]objectForKey:Slug] setValue:yeloflag forKey:@"personal_flag"];
        
    }
        
        
        NSString *code= [[[[Main_Updated_NSMutDictonary_Array objectForKey:userSelectedMainCategoryEvent]objectForKey:userSelectedMainSinglePostureSlug]objectForKey:Slug] valueForKey:@"Sett_code"];
        
        
        
        NSMutableArray *results = [[NSMutableArray alloc]init];
        NSMutableArray *results1 = [[NSMutableArray alloc]init];
        int flag=0;
        NSPredicate *pred;
        NSString* filter = @"%K == %@ && %K == %@";
        NSArray* args = @[@"code",code, @"posture", globalposturecode];
        if (code.length!=0) {
            pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
            flag=1;
        } else {
            flag=0;
            NSLog(@"Enter Corect code empty");
            
        }
        
        if (flag == 1) {
            
            
            NSLog(@"predicate: %@",pred);
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
            [fetchRequest setPredicate:pred];
            results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
            
            if (results.count > 0) {
                
                NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
               // NSLog(@"1 - %@", doctorssett);
              
                [doctorssett setValue:@"MCM" forKey:@"modify_from"];
                [doctorssett setValue:@"0" forKey:@"updated_to_site"];
                
                [self.managedObjectContext save:nil];
            } else {
                NSLog(@"Enter Corect code number");
            }
            NSFetchRequest *fetchRequest1 = [[NSFetchRequest alloc]initWithEntityName:@"ThresholdSett"];
            [fetchRequest1 setPredicate:pred];
            results1 = [[self.managedObjectContext executeFetchRequest:fetchRequest1 error:nil] mutableCopy];
            
            if (results1.count > 0) {
                
                NSManagedObject *genthressett = (NSManagedObject *)[results1 objectAtIndex:0];
              //  NSLog(@"1 - %@", genthressett);
                
                [genthressett setValue:[NSString stringWithFormat:@"%@",yeloflag] forKey:@"flag"];
                [genthressett setValue:@"MCM" forKey:@"modify_from"];
                [genthressett setValue:@"0" forKey:@"updated_to_site"];
              
                [self.managedObjectContext save:nil];
            } else {
                NSLog(@"Enter Corect general code number");
            }
            
            
            
        }
       
        
        
        
   
    [self.tableViewDoctorSettings reloadData];
    }
    else
    {
        [self presentAlertViewForPassword];
    }
    
}
-(void)RedClicked:(UIButton*)sender
{
    if (sender.tag == 0)
    {
        // Your code here
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     GlobalVars *global = [GlobalVars sharedInstance];
    indexpthsec=[indexPath section];
    indexrw=[indexPath row];
    
    int  rowStatus   =   [cellRowStatus[indexPath.row]intValue];
    
    int setNewRowValue        =   0;
  
    if (rowStatus == 0)
    {
       
        setNewRowValue  =   1;
         universalcellstatus=setNewRowValue;
        dropDownInUse = SubCategoryValueDropDown;
        if(!global.dropdownValidFlag)
        {
            
            if(global.BluetoothConnectionflag==2)
            {
                [self presentAlertViewForPassword];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                                message:@"Please Connect MCD Device"
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
        }
    }
    
    
    //  ########   Update the row status
    if(global.dropdownValidFlag)
    {
        if(global.BluetoothConnectionflag==2)
        {
        //dropdownValidFlag=false;
          universalcellstatus=setNewRowValue;
    [cellRowStatus replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithInteger:setNewRowValue]];
    
    [self.tableViewDoctorSettings reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    //[tableViewDoctorSettings reloadData];
    [self.tableViewDoctorSettings scrollToRowAtIndexPath :indexPath atScrollPosition:UITableViewScrollPositionTop animated: true];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                            message:@"Please Connect MCD Device"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }
}

- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.isDragging) {
        UIView *myView = cell.contentView;
        CALayer *layer = myView.layer;
        CATransform3D rotationAndPerspectiveTransform = CATransform3DIdentity;
        rotationAndPerspectiveTransform.m34 = 1.0 / -1000;
        if (scrollOrientation == UIImageOrientationDown) {
            rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, M_PI*0.5, 1.0f, 0.0f, 0.0f);
        } else {
            rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, -M_PI*0.5, 1.0f, 0.0f, 0.0f);
        }
        layer.transform = rotationAndPerspectiveTransform;
        [UIView animateWithDuration:1.0 animations:^{
            layer.transform = CATransform3DIdentity;
        }];
        
       /* UIView *cellContentView  = [cell contentView];
        CGFloat rotationAngleDegrees = -30;
        CGFloat rotationAngleRadians = rotationAngleDegrees * (M_PI/180);
        CGPoint offsetPositioning = CGPointMake(500, -20.0);
        CATransform3D transform = CATransform3DIdentity;
        transform = CATransform3DRotate(transform, rotationAngleRadians, -50.0, 0.0, 1.0);
        transform = CATransform3DTranslate(transform, offsetPositioning.x, offsetPositioning.y, -50.0);
        cellContentView.layer.transform = transform;
        cellContentView.layer.opacity = 0.8;
        
        [UIView animateWithDuration:.65 delay:0.0 usingSpringWithDamping:0.85 initialSpringVelocity:.8 options:0 animations:^{
            cellContentView.layer.transform = CATransform3DIdentity;
            cellContentView.layer.opacity = 1;
        } completion:^(BOOL finished) {}];*/
    }
}

- (void) scrollViewDidScroll:(UIScrollView *)scrollView {
    scrollOrientation = scrollView.contentOffset.y > lastPos.y?UIImageOrientationDown:UIImageOrientationUp;
    lastPos = scrollView.contentOffset;
}
// MARK: - UIPickerContor DATASOURCE and DELEGATE



- (NSInteger)numberOfComponentsInPickerView:(CustomUIPickerView *)pickerView{
    
    if(dropDownInUse == MainCategoryDropDown)
    {
        // Main Category Drop Down Values are =Arrhythmia =ischemia =other-events-slug
        return 1;
    }
    else
    {
        // For Any SubCategory Event there can be 3 Components or might be 1
        // 3 Components are =Doctor Valuem, =Yellow Value, =Red Value
        // 1 single Component will be for =On, =Off
        
           CustomUIPickerView * pickerControl=pickerView;
        NSString * inputTypeAs   =  [pickerControl inputTypeCom3orCom1];
        
        //NSLog(@"inout1%@",[pickerControl inputTypeCom3orCom1]);
     
        
       
       
        
        //println("Input Type in delegates \(inputTypeAs) ")
        if ( [inputTypeAs isEqualToString:@"TEXT"])
        {
            
            return 3;
        }
        else // Will be SWITCH which will have only 1 component Value with [[ ON or OFF ]]
        {
            return 1;
        }
        
        
        
    }
    
    
    
}

- (NSInteger)pickerView:(CustomUIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    if(dropDownInUse == MainCategoryDropDown)
    {
        // Main Category Drop Down Values are =Arrhythmia =ischemia =other-events-slug
        return Event_Category_Master_NSArray.count;
    }
    else
    {
        // For Any SubCategory Event there can be 3 Components or might be 1
        // 3 Components are =Doctor Valuem, =Yellow Value, =Red Value
        // 1 single Component will be for =On, =Off
        
        //pickerView1=doctorSettingTableViewCell.subCatValuePickerView;
         CustomUIPickerView * pickerControl=pickerView;
        NSString * inputTypeAs    =   [pickerControl inputTypeCom3orCom1];
        //NSLog(@"inout2%@",[pickerControl inputTypeCom3orCom1]);
        NSDictionary * pickerData    =   [pickerControl passDictArray];
       // NSLog(@"pickderdata:%@",pickerData);
        NSArray * array_read=[pickerData objectForKey:@"Read_array"];
        int  numberOfRows;
        
        if ( [inputTypeAs isEqualToString:@"TEXT"])
        {
            int minValue        =   [[pickerData objectForKey:@"min"]intValue];
            int maxValue        =   [[pickerData objectForKey:@"max"]intValue];
            
            numberOfRows     =   ((maxValue - minValue))+2;
            
            // NSLog(@"Number of Rows in COMPONENTS MAKING NO OF ROWS %ld From MIN %d and MAX %d ROWS %lu",(long)component,minValue,maxValue,numberOfRows);
            //NSLog(@"rowcount%lu",(unsigned long)array_read.count);
            return array_read.count;
        }
        else // Will be SWITCH which will have only 1 component Value with [[ ON or OFF ]]
        {
            //NSLog(@"rowcount%lu",(unsigned long)array_read.count);
            return array_read.count;
            //return 0;
            
        }
        
    }
    
    
    
}

- (NSString*)pickerView:(CustomUIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    if(dropDownInUse == MainCategoryDropDown)
    {
        // Main Category Drop Down Values are =Arrhythmia =ischemia =other-events-slug
        return [[Event_Category_Master_NSArray objectAtIndex:row]objectForKey:@"Name"];
        
    }
    else
    {
      
      
        
        //NSLog(@"comp%ldrow",(long)component);
        //NSLog(@"rrr%ldrow",(long)row);
         CustomUIPickerView * pickerControl=pickerView;
        //NSLog(@"inout3");
        // NSLog(@"%@",[pickerControl inputTypeCom3orCom1]);
        NSString * inputTypeAs    =   [pickerControl inputTypeCom3orCom1];
        
        
        NSDictionary * pickerData =   [pickerControl passDictArray];
        NSMutableArray *arrayData_one       =   [[NSMutableArray alloc]init];
        //println("Input Type in delegates \(inputTypeAs) ")
        if ( [inputTypeAs isEqualToString:@"TEXT"])
        {
            
            
            arrayData_one=[pickerData objectForKey:@"Read_array"];
            
           // NSLog(@"arrdata%@",arrayData_one);
            NSString * title1;
            title1 = (row == 0) ?@"OFF" :[arrayData_one[row] stringValue];
            //NSLog(@"tititiit%@",title1);
            
            
            
            
            return title1;
        }
        else // Will be SWITCH which will have only 1 component Value with [[ ON or OFF ]]
        {
            
            if(component==0)
            {
                arrayData_one=[pickerData objectForKey:@"Read_array"];
                //NSLog(@"arrdata%@",arrayData_one);
                if([[pickerData objectForKey:@"name"] isEqualToString:@"HR Suspension"]||[[pickerData objectForKey:@"name"] isEqualToString:@"ST Suspension"])
                    return [arrayData_one[row] stringValue] ;
                else
                return [self getOnOffValue:[arrayData_one[row] intValue]];
            }
           
            
        }
        
        
        return nil;
        
    }
    
    
}



- (void)pickerView:(CustomUIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    if(dropDownInUse == MainCategoryDropDown)
    {
        // Main Category Drop Down Values are =Arrhythmia =ischemia =other-events-slug
        currentSelectedRowIdForMainEventCat = (int)row;
    }
    else
    {
        NSString *DD,*YD,*RD,*flagg,*ddflagg,*posturecode;
        CustomUIPickerView * pickerControl=pickerView;
        NSString * inputTypeAs                 = [pickerControl inputTypeCom3orCom1];
        NSDictionary * pickerData =  [pickerControl passDictArray];
        
        NSString * pickerMainCatEventSlug          =   [pickerControl passMainCatSlug];
        NSString * pickerMainSubCatEventSlug       =  [pickerControl passMainSubCatSlug];
        NSString * userSelectedValue;
        NSString * valueForKey ; // Doctor, Yellow or Red Value
        NSMutableArray *arrayData_one       =   [[NSMutableArray alloc]init];
        arrayData_one=[pickerData objectForKey:@"Read_array"];
        NSString * settcode=[pickerData objectForKey:@"Sett_code"];

        // println("Input Type in delegates \(inputTypeAs) ")
        if ( [inputTypeAs isEqualToString:@"TEXT"])
        {
            // int minValue            =   [[pickerData objectForKey:@"min"]intValue];
            //int maxValue            =   [[pickerData objectForKey:@"max"]intValue];
            NSInteger doctorOldValue  =   [[pickerData objectForKey:@"threshold_value"]integerValue];
            NSInteger yellowOldValue  =   [[pickerData objectForKey:@"yellow_alert_value"]integerValue];
            NSInteger redOldValue     =   [[pickerData objectForKey:@"red_alert_value"]integerValue];
            NSInteger personal_flg     =  [[pickerData objectForKey:@"personal_flag"]integerValue];
            NSInteger doc_flg     =  [[pickerData objectForKey:@"doc_flag"]integerValue];
            
            
            
                      //userSelectedValue  = (row == 0) ? "Off" : "\(arrayData_one[row])"
            userSelectedValue  =   ([arrayData_one[row] stringValue]);
            //int goToRow;
            if(component == 0) // Value Store for Doctor
            {
                valueForKey = @"threshold_value";
                ddpickvalue=[arrayData_one[row] stringValue];
                NSInteger doctorCurrentValue = [userSelectedValue integerValue];
                
                if ([[pickerData objectForKey:@"name"] isEqualToString:@"Bradycardia"])
                {
                    if (doctorCurrentValue > 0 && doctorCurrentValue <= yellowOldValue)
                    {
                        // Set the Picker control
                        [ pickerView selectRow:row inComponent:component animated:true];
                        
                        // Crossing Yellow Value
                        //println("Crossing :: Doctor Should be less than Yellow")
                        [self showValidationAlertBox:@"Validation Alert"  msg:@"Doctor value should be Greater than Yellow Value"];
                        return;
                    }
                }
                
                else
                {
                if (doctorCurrentValue > 0 && doctorCurrentValue >= yellowOldValue)
                {
                    // Set the Picker control
                    [ pickerView selectRow:row inComponent:component animated:true];
                    
                    // Crossing Yellow Value
                    //println("Crossing :: Doctor Should be less than Yellow")
                    [self showValidationAlertBox:@"Validation Alert"  msg:@"Doctor value should be less than Yellow Value"];
                    return;
                 }
                }
                if([settcode isEqualToString:@"2022"])
                {
                    NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
                    NSString *temperaturetype=[TempDefault objectForKey:@"TEMPERATURE"];
                    if([temperaturetype isEqualToString:@"CELSIUS"])
                    {
                        DD=[NSString stringWithFormat:@"%ld",(long)doctorCurrentValue];
                        YD=[NSString stringWithFormat:@"%ld",(long)yellowOldValue];
                        RD=[NSString stringWithFormat:@"%ld",(long)redOldValue];
                        ddflagg=@"1";
                        flagg=[NSString stringWithFormat:@"%ld",(long)personal_flg];
                    }
                    else
                    {
                        float doctorNewV  =   [userSelectedValue floatValue];
                        float yellowOldV  =   [[pickerData objectForKey:@"yellow_alert_value"]floatValue];
                        float redOldV     =   [[pickerData objectForKey:@"red_alert_value"]floatValue];

                        CommonHelper *help=[[CommonHelper alloc]init];
                        DD=[help Fahernheittocelsius:[NSString stringWithFormat:@"%f",doctorNewV]];
                        YD=[help Fahernheittocelsius:[NSString stringWithFormat:@"%f",yellowOldV]];
                        RD=[help Fahernheittocelsius:[NSString stringWithFormat:@"%f",redOldV]];
                        ddflagg=@"1";
                        flagg=[NSString stringWithFormat:@"%ld",(long)personal_flg];
                    }
                }
                else if ([settcode isEqualToString:@"2002"]||[settcode isEqualToString:@"2001"])
                {
                    NSArray *dd1;
                    if(![userSelectedValue isEqualToString:@"0"])
                     dd1= [userSelectedValue componentsSeparatedByString:@"."];
                    else
                        dd1=@[@"0",@"0"];
                    NSInteger dctoldvue=[dd1[1] integerValue];
                    if (dd1[1] > 0 && dctoldvue >=yellowOldValue)
                    {
                        // Set the Picker control
                        [ pickerView selectRow:row inComponent:component animated:true];
                        
                        // Crossing Yellow Value
                        //println("Crossing :: Doctor Should be less than Yellow")
                        [self showValidationAlertBox:@"Validation Alert"  msg:@"Doctor value should be less than Yellow Value"];
                        return;
                    }
                   else
                   {
                    DD=dd1[1];
                       userSelectedValue =dd1[1];
                    YD=[NSString stringWithFormat:@"%ld",(long)yellowOldValue];
                    RD=[NSString stringWithFormat:@"%ld",(long)redOldValue];
                    ddflagg=@"1";
                    flagg=[NSString stringWithFormat:@"%ld",(long)personal_flg];
                   }
                }
                else
                {
                    DD=[NSString stringWithFormat:@"%ld",(long)doctorCurrentValue];
                    YD=[NSString stringWithFormat:@"%ld",(long)yellowOldValue];
                    RD=[NSString stringWithFormat:@"%ld",(long)redOldValue];
                    ddflagg=@"1";
                    flagg=[NSString stringWithFormat:@"%ld",(long)personal_flg];
                }
                
                
            }
            else if (component == 1) // Value Store for Yellow
            {
                valueForKey = @"yellow_alert_value";
                ydpickvalue=[arrayData_one[row] stringValue];
                NSInteger yellowCurrentValue = userSelectedValue.integerValue;
                
                
                if ([[pickerData objectForKey:@"name"] isEqualToString:@"Bradycardia"])
                {
                    if(doctorOldValue > 0 && yellowCurrentValue > 0 && (yellowCurrentValue >= doctorOldValue || yellowCurrentValue <= redOldValue))
                    {
                        // Get the Row with the help of Doctor value, From array
                        
                       
                        NSInteger row = [arrayData_one indexOfObject:[NSNumber numberWithInteger:doctorOldValue]];
                        
                        row = (row - 1);
                        
                        
                        // Set the Picker control
                        [pickerView selectRow:row inComponent:component animated:true];
                        
                        // Corssing Yellow and Crossing Red
                        //println("Crossing :: Yellow Should be between Doctor and Red")
                        [self showValidationAlertBox:@"Validation Alert" msg:@"Yellow value should be between Red and Doctor Value"];
                        return;
                    }
                }
                
                else
                {
                    
                if(doctorOldValue > 0 && yellowCurrentValue > 0 && (yellowCurrentValue <= doctorOldValue || yellowCurrentValue >= redOldValue))
                {
                    // Get the Row with the help of Doctor value, From array
                    
                    NSInteger row = [arrayData_one indexOfObject:[NSNumber numberWithInteger:doctorOldValue]];
                    
                    row = (row + 1);
                    
                    
                    // Set the Picker control
                    [pickerView selectRow:row inComponent:component animated:true];
                    
                    // Corssing Yellow and Crossing Red
                    //println("Crossing :: Yellow Should be between Doctor and Red")
                    [self showValidationAlertBox:@"Validation Alert" msg:@"Yellow value should be between Doctor and Red Value"];
                    return;
                }
                }
                if([settcode isEqualToString:@"2022"])
                {
                    NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
                    NSString *temperaturetype=[TempDefault objectForKey:@"TEMPERATURE"];
                    if([temperaturetype isEqualToString:@"CELSIUS"])
                    {
                        DD=[NSString stringWithFormat:@"%ld",(long)doctorOldValue];
                        YD=[NSString stringWithFormat:@"%ld",(long)yellowCurrentValue];
                        RD=[NSString stringWithFormat:@"%ld",(long)redOldValue];
                        ddflagg=[NSString stringWithFormat:@"%ld",(long)doc_flg];
                        flagg=[NSString stringWithFormat:@"%ld",(long)personal_flg];

                       
                    }
                    else
                    {
                        float doctorOldV  =  [[pickerData objectForKey:@"threshold_value"]floatValue];
                        float yellowNewV  =  [userSelectedValue floatValue];
                        float redOldV     =   [[pickerData objectForKey:@"red_alert_value"]floatValue];
                        
                        CommonHelper *help=[[CommonHelper alloc]init];
                        DD=[help Fahernheittocelsius:[NSString stringWithFormat:@"%f",doctorOldV]];
                        YD=[help Fahernheittocelsius:[NSString stringWithFormat:@"%f",yellowNewV]];
                        RD=[help Fahernheittocelsius:[NSString stringWithFormat:@"%f",redOldV]];
                        ddflagg=[NSString stringWithFormat:@"%ld",(long)doc_flg];
                        flagg=[NSString stringWithFormat:@"%ld",(long)personal_flg];
                    }
                }
                else if ([settcode isEqualToString:@"2002"]||[settcode isEqualToString:@"2001"])
                {
                    NSArray *yd1;
                    if(![userSelectedValue isEqualToString:@"0"])
                        yd1= [userSelectedValue componentsSeparatedByString:@"."];
                    else
                        yd1=@[@"0",@"0"];
                    
                    NSInteger yelldvue=[yd1[1] integerValue];
                    if (doctorOldValue > 0 && yelldvue > 0 && (yelldvue <= doctorOldValue || yelldvue >= redOldValue))
                    {
                        // Set the Picker control
                         NSDecimalNumber *doctV = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"0.%ld",(long)doctorOldValue]];
                        NSInteger row = [arrayData_one indexOfObject:doctV];
                        
                        row = (row + 1);
                        
                        
                        // Set the Picker control
                        [pickerView selectRow:row inComponent:component animated:true];
                        
                        // Corssing Yellow and Crossing Red
                        //println("Crossing :: Yellow Should be between Doctor and Red")
                        [self showValidationAlertBox:@"Validation Alert" msg:@"Yellow value should be between Doctor and Red Value"];
                    }
                    else
                    {
                        DD=[NSString stringWithFormat:@"%ld",(long)doctorOldValue];
                        userSelectedValue =yd1[1];
                        YD=yd1[1];
                        RD=[NSString stringWithFormat:@"%ld",(long)redOldValue];
                        ddflagg=@"1";
                        flagg=[NSString stringWithFormat:@"%ld",(long)personal_flg];
                    }
                }

                else
                {
                DD=[NSString stringWithFormat:@"%ld",(long)doctorOldValue];
                YD=[NSString stringWithFormat:@"%ld",(long)yellowCurrentValue];
                RD=[NSString stringWithFormat:@"%ld",(long)redOldValue];
                ddflagg=[NSString stringWithFormat:@"%ld",(long)doc_flg];
                flagg=[NSString stringWithFormat:@"%ld",(long)personal_flg];
                }
               
            }
            else if (component == 2) // Value Store for Red
            {
                valueForKey = @"red_alert_value";
                 rdpickvalue=[arrayData_one[row] stringValue];
                NSInteger  redCurrentValue = userSelectedValue.integerValue;
                
                if ([[pickerData objectForKey:@"name"] isEqualToString:@"Bradycardia"])
                {
                    if(yellowOldValue > 0 && redCurrentValue > 0 && redCurrentValue >= yellowOldValue)
                    {
                        // Get the Row with the help of Doctor value, From array
                        
                        NSInteger row = [arrayData_one indexOfObject:[NSNumber numberWithInteger:yellowOldValue ]];
                        
                        row = (row - 1);
                        
                        // NSLog(@"roee%ld",(long)row);
                        
                        [pickerView selectRow:row inComponent:component animated:true];
                        // Set the Picker control
                        
                        
                        // Corssing Yellow and Crossing Red
                        //println("Crossing :: Red Should be greater than Yellow")
                        [self showValidationAlertBox:@"Validation Alert" msg:@"Red value should be Less than Yellow Value"];
                        return;
                    }
                }
                else
                {
                if(yellowOldValue > 0 && redCurrentValue > 0 && redCurrentValue <= yellowOldValue)
                {
                    // Get the Row with the help of Doctor value, From array
                    
                    NSInteger row = [arrayData_one indexOfObject:[NSNumber numberWithInteger:yellowOldValue ]];
                    
                    row = (row + 1);
                    
                   // NSLog(@"roee%ld",(long)row);
                    
                    [pickerView selectRow:row inComponent:component animated:true];
                    // Set the Picker control
                    
                    
                    // Corssing Yellow and Crossing Red
                    //println("Crossing :: Red Should be greater than Yellow")
                    [self showValidationAlertBox:@"Validation Alert" msg:@"Red value should be greater than Yellow Value"];
                    return;
                }
                }
                if([settcode isEqualToString:@"2022"])
                {
                    
                    NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
                    NSString *temperaturetype=[TempDefault objectForKey:@"TEMPERATURE"];
                    if([temperaturetype isEqualToString:@"CELSIUS"])
                    {
                        DD=[NSString stringWithFormat:@"%ld",(long)doctorOldValue];
                        YD=[NSString stringWithFormat:@"%ld",(long)yellowOldValue];
                        RD=[NSString stringWithFormat:@"%ld",(long)redCurrentValue];
                        ddflagg=[NSString stringWithFormat:@"%ld",(long)doc_flg];
                        flagg=[NSString stringWithFormat:@"%ld",(long)personal_flg];
                    }
                    else
                    {
                        float doctorOldV  =   [[pickerData objectForKey:@"threshold_value"]floatValue];
                        float yellowOldV  =   [[pickerData objectForKey:@"yellow_alert_value"]floatValue];
                        float redNewV     =   [userSelectedValue floatValue];;
                        
                        CommonHelper *help=[[CommonHelper alloc]init];
                        DD=[help Fahernheittocelsius:[NSString stringWithFormat:@"%f",doctorOldV]];
                        YD=[help Fahernheittocelsius:[NSString stringWithFormat:@"%f",yellowOldV]];
                        RD=[help Fahernheittocelsius:[NSString stringWithFormat:@"%f",redNewV]];
                        ddflagg=[NSString stringWithFormat:@"%ld",(long)doc_flg];
                        flagg=[NSString stringWithFormat:@"%ld",(long)personal_flg];
                    }
                }
                else if ([settcode isEqualToString:@"2002"]||[settcode isEqualToString:@"2001"])
                {
                    NSArray *rd1;
                    if(![userSelectedValue isEqualToString:@"0"])
                        rd1= [userSelectedValue componentsSeparatedByString:@"."];
                    else
                        rd1=@[@"0",@"0"];
                    NSInteger redoldvue=[rd1[1] integerValue];
                    if (yellowOldValue > 0 && redoldvue > 0 && redoldvue <= yellowOldValue)
                    {
                        // Set the Picker control
                         NSDecimalNumber *yellV = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"0.%ld",(long)yellowOldValue]];
                        NSInteger row = [arrayData_one indexOfObject:yellV];
                        
                        row = (row + 1);
                        
                        // NSLog(@"roee%ld",(long)row);
                        
                        [pickerView selectRow:row inComponent:component animated:true];
                        // Set the Picker control
                        
                        
                        // Corssing Yellow and Crossing Red
                        //println("Crossing :: Red Should be greater than Yellow")
                        [self showValidationAlertBox:@"Validation Alert" msg:@"Red value should be greater than Yellow Value"];
                        return;

                    }
                    else
                    {
                        DD=[NSString stringWithFormat:@"%ld",(long)doctorOldValue];
                        userSelectedValue =rd1[1];
                        YD=[NSString stringWithFormat:@"%ld",(long)yellowOldValue];
                        RD=rd1[1];
                        ddflagg=@"1";
                        flagg=[NSString stringWithFormat:@"%ld",(long)personal_flg];
                    }
                }

                else
                {
                DD=[NSString stringWithFormat:@"%ld",(long)doctorOldValue];
                YD=[NSString stringWithFormat:@"%ld",(long)yellowOldValue];
                RD=[NSString stringWithFormat:@"%ld",(long)redCurrentValue];
                ddflagg=[NSString stringWithFormat:@"%ld",(long)doc_flg];
                flagg=[NSString stringWithFormat:@"%ld",(long)personal_flg];
                }
                //pickerControl.selectRow(20, inComponent: 2, animated: true)
                //return
            }
            
            
        }
        else
        {
            // Will be SWITCH which will have only 1 component Value with [[ ON or OFF ]]
            // We are using only [[ red_alert_value ]] key for Switch
         // 0 == Off, 1 == On
            if ([[pickerData objectForKey:@"name"] isEqualToString:@"HR Suspension"]||[[pickerData objectForKey:@"name"] isEqualToString:@"ST Suspension"]) {
                userSelectedValue   =  [arrayData_one[row] stringValue];
                valueForKey = @"threshold_value";
                DD=[NSString stringWithFormat:@"%@",userSelectedValue];
                YD=@"0";
                RD=@"0";
                ddflagg=@"1";
                flagg=@"1";
            }
            else
            {
            userSelectedValue   =  [arrayData_one[row] stringValue];
            valueForKey = @"red_alert_value";
            DD=@"0";
            YD=@"0";
            RD=[NSString stringWithFormat:@"%@",userSelectedValue];
            ddflagg=userSelectedValue;
            flagg=userSelectedValue;
            }
            
        }
        
        
        // -- Storing data here at once for any events, Currently we are Considering, single posture
        // -- Need to work on Multiple postures -- //
        
        if (userSelectedPosturesArray.count == 0 && [pickerMainCatEventSlug isEqualToString:@"other-events-slug"])
        {
            // ----  Only for Others Events
            //eachSubEventDictonary = Main_Updated_NSMutDictonary_Array.objectForKey(userSelectedMainCategoryEvent)?.objectForKey(Slug) as NSDictionary
            [[[Main_Updated_NSMutDictonary_Array objectForKey:pickerMainCatEventSlug]objectForKey:pickerMainSubCatEventSlug]setObject:userSelectedValue forKey:valueForKey];
            posturecode=@"0";
            
        }
        else
        {
            // ----  Only for Arrythmia and Ischemia
            //tempSlug = userSelectedMainSinglePostureSlug
            // Here Posture is Single selected so we have to show the value of that particular posture
            
            
          /*  Main_Updated_NSMutDictonary_Array .objectForKey(pickerMainCatEventSlug)?.objectForKey(userSelectedMainSinglePostureSlug)?.objectForKey(pickerMainSubCatEventSlug)?.setObject(userSelectedValue, forKey: valueForKey)*/
            
            [[[[Main_Updated_NSMutDictonary_Array objectForKey:pickerMainCatEventSlug]objectForKey:userSelectedMainSinglePostureSlug]objectForKey:pickerMainSubCatEventSlug]setObject:userSelectedValue forKey:valueForKey];
            
            
            
            posturecode=globalposturecode;
        }
        
        
        if([settcode intValue]!=0)
        {
            GlobalVars *globals = [GlobalVars sharedInstance];
            globals.Thres_SetCode=settcode;
            globals.Thres_DD=DD;
            globals.Thres_RD=RD;
            globals.Thres_YD=YD;
            globals.Thres_flagg=flagg;
            globals.Thres_ddflagg=ddflagg;
            globals.Thres_posturecode=globalposturecode;
            globals.ThresMultiplePosturesArray=userSelectedPosturesArray.copy;
            
            if([globals.mcd_bluetoothstatus isEqualToString:@"0"])
            {
                ConfigureMCDController *config=[[ConfigureMCDController alloc]init];
                
                
                if([settcode intValue]==NARROW_QRS_REGULAR_D_DESCRIPTION)
                {
                    if([globalposturecode isEqualToString:@"BOTH"])
                    {
                        for(int i=0;i<userSelectedPosturesArray.count;i++)
                        {
                            if([userSelectedPosturesArray[i] isEqualToString:@"laying-slug"])
                            {
                                
                                [config SET_VentricularTachyNarrow:[DD intValue] :[DD intValue] :[ddflagg intValue]  :LayingPostureCode];
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"standing-sitting-slug"]) // Standing / Sitting
                            {
                                
                               [config SET_VentricularTachyNarrow:[DD intValue] :[DD intValue] :[ddflagg intValue]  :StandingSittingPostureCode];
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"walking-slug"] ) // Walking
                            {
                                
                               [config SET_VentricularTachyNarrow:[DD intValue] :[DD intValue] :[ddflagg intValue] :WalkingPostureCode];
                                
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"running-slug"] ) // Running
                            {
                                
                                
                                [config SET_VentricularTachyNarrow:[DD intValue] :[DD intValue] :[ddflagg intValue] :RunningPostureCode];
                            }
                        }
                    }
                    else
                    [config SET_VentricularTachyNarrow:[DD intValue] :[DD intValue] :[ddflagg intValue] :[posturecode intValue]];
                }
                else if([settcode intValue]==WIDE_QRS_REGULAR_D_DESCRIPTION)
                {
                   /* if([globalposturecode isEqualToString:@"BOTH"])
                    {
                        for(int i=0;i<userSelectedPosturesArray.count;i++)
                        {
                            if([userSelectedPosturesArray[i] isEqualToString:@"laying-slug"])
                            {
                                
                                [config SET_VentricularTachyWide:[DD intValue] :[DD intValue] :[ddflagg intValue] :LayingPostureCode];
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"standing-sitting-slug"]) // Standing / Sitting
                            {
                                
                                 [config SET_VentricularTachyWide:[DD intValue] :[DD intValue] :[ddflagg intValue] :StandingSittingPostureCode];
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"walking-slug"] ) // Walking
                            {
                                
                                [config SET_VentricularTachyWide:[DD intValue] :[DD intValue] :[ddflagg intValue] :WalkingPostureCode];
                                
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"running-slug"] ) // Running
                            {
                                
                                
                                [config SET_VentricularTachyWide:[DD intValue] :[DD intValue] :[ddflagg intValue] :RunningPostureCode];
                            }
                        }
                    }
                    else
                    [config SET_VentricularTachyWide:[DD intValue] :[DD intValue] :[ddflagg intValue] :[posturecode intValue]];*/
                    [self UpdateThresholdvalue:161];
                }
                else if ([settcode intValue]==HR_SUSPENSION)
                {
                    if([globalposturecode isEqualToString:@"BOTH"])
                    {
                        for(int i=0;i<userSelectedPosturesArray.count;i++)
                        {
                            if([userSelectedPosturesArray[i] isEqualToString:@"laying-slug"])
                            {
                                [config SET_SuspensionPercent:1 :[DD intValue] :LayingPostureCode];
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"standing-sitting-slug"]) // Standing / Sitting
                            {
                                
                                [config SET_SuspensionPercent:1 :[DD intValue] :StandingSittingPostureCode];
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"walking-slug"] ) // Walking
                            {
                                [config SET_SuspensionPercent:1 :[DD intValue] :WalkingPostureCode];
                                
                                
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"running-slug"] ) // Running
                            {
                                [config SET_SuspensionPercent:1 :[DD intValue] :RunningPostureCode];
                                
                            }
                        }
                    }
                    else
                        [config SET_SuspensionPercent:1 :[DD intValue] :[posturecode intValue]];
                    
                    
                }
                else if([settcode intValue]==BRADICARDIA_REGULAR_D_DESCRIPTION)
                {
                    if([globalposturecode isEqualToString:@"BOTH"])
                    {
                        for(int i=0;i<userSelectedPosturesArray.count;i++)
                        {
                            if([userSelectedPosturesArray[i] isEqualToString:@"laying-slug"])
                            {
                              
                                 [config SET_Bradycardia:[DD intValue] :[DD intValue] :[ddflagg intValue] :LayingPostureCode];
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"standing-sitting-slug"]) // Standing / Sitting
                            {
                                
                                 [config SET_Bradycardia:[DD intValue] :[DD intValue] :[ddflagg intValue] :StandingSittingPostureCode];
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"walking-slug"] ) // Walking
                            {
                               
                                 [config SET_Bradycardia:[DD intValue] :[DD intValue] :[ddflagg intValue] :WalkingPostureCode];
                                
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"running-slug"] ) // Running
                            {
                                
                              
                                 [config SET_Bradycardia:[DD intValue] :[DD intValue] :[ddflagg intValue] :RunningPostureCode];
                            }
                        }
                    }
                    else
                      [config SET_Bradycardia:[DD intValue] :[DD intValue] :[ddflagg intValue] :[posturecode intValue]];
                }
                else if ([settcode intValue]==PAUSE)
                {
                    if([globalposturecode isEqualToString:@"BOTH"])
                    {
                        for(int i=0;i<userSelectedPosturesArray.count;i++)
                        {
                            if([userSelectedPosturesArray[i] isEqualToString:@"laying-slug"])
                            {
                                
                               [config SET_PauseValue:[DD intValue] :[ddflagg intValue] :LayingPostureCode];
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"standing-sitting-slug"]) // Standing / Sitting
                            {
                                
                               [config SET_PauseValue:[DD intValue] :[ddflagg intValue]:StandingSittingPostureCode];
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"walking-slug"] ) // Walking
                            {
                                
                                [config SET_PauseValue:[DD intValue] :[ddflagg intValue] :WalkingPostureCode];
                                
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"running-slug"] ) // Running
                            {
                                
                                
                               [config SET_PauseValue:[DD intValue] :[ddflagg intValue]:RunningPostureCode];
                            }
                        }
                    }
                    else
                    [config SET_PauseValue:[DD intValue] :[ddflagg intValue] :[posturecode intValue]];
                    
                }
                else if ([settcode intValue]==PVC_VALUE)
                {
                    if([globalposturecode isEqualToString:@"BOTH"])
                    {
                        for(int i=0;i<userSelectedPosturesArray.count;i++)
                        {
                            if([userSelectedPosturesArray[i] isEqualToString:@"laying-slug"])
                            {
                                
                               [config SET_PVC:[ddflagg intValue] :[DD intValue] :LayingPostureCode];
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"standing-sitting-slug"]) // Standing / Sitting
                            {
                                
                                [config SET_PVC:[ddflagg intValue] :[DD intValue]:StandingSittingPostureCode];
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"walking-slug"] ) // Walking
                            {
                                
                                [config SET_PVC:[ddflagg intValue] :[DD intValue] :WalkingPostureCode];
                                
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"running-slug"] ) // Running
                            {
                                
                                
                                [config SET_PVC:[ddflagg intValue] :[DD intValue]:RunningPostureCode];
                            }
                        }
                    }
                    else
                    [config SET_PVC:[ddflagg intValue] :[DD intValue] :[posturecode intValue]];
                }
                else if ([settcode intValue]==ASYSTOLE_VALUE)
                {
                    if([globalposturecode isEqualToString:@"BOTH"])
                    {
                        for(int i=0;i<userSelectedPosturesArray.count;i++)
                        {
                            if([userSelectedPosturesArray[i] isEqualToString:@"laying-slug"])
                            {
                                
                                [config SET_Asystole:[ddflagg intValue] :[DD intValue] :LayingPostureCode];
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"standing-sitting-slug"]) // Standing / Sitting
                            {
                                
                                [config SET_Asystole:[ddflagg intValue] :[DD intValue]:StandingSittingPostureCode];
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"walking-slug"] ) // Walking
                            {
                                
                                [config SET_Asystole:[ddflagg intValue] :[DD intValue] :WalkingPostureCode];
                                
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"running-slug"] ) // Running
                            {
                                
                                
                                [config SET_Asystole:[ddflagg intValue] :[DD intValue]:RunningPostureCode];
                            }
                        }
                    }
                    else
                    [config SET_Asystole:[ddflagg intValue] :[DD intValue] :[posturecode intValue]];
                }
                else if ([settcode intValue]==BIGEMINY)
                {
                    if([globalposturecode isEqualToString:@"BOTH"])
                    {
                        for(int i=0;i<userSelectedPosturesArray.count;i++)
                        {
                            if([userSelectedPosturesArray[i] isEqualToString:@"laying-slug"])
                            {
                                [config SET_BigemenyValue:[ddflagg intValue] :LayingPostureCode :[DD intValue]];
                               
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"standing-sitting-slug"]) // Standing / Sitting
                            {
                                [config SET_BigemenyValue:[ddflagg intValue] :StandingSittingPostureCode :[DD intValue]];
                                
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"walking-slug"] ) // Walking
                            {
                                [config SET_BigemenyValue:[ddflagg intValue] :WalkingPostureCode :[DD intValue]];
                               
                                
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"running-slug"] ) // Running
                            {
                                
                                [config SET_BigemenyValue:[ddflagg intValue] :RunningPostureCode :[DD intValue]];
                                
                            }
                        }
                    }
                    else
                    [config SET_BigemenyValue:[ddflagg intValue] :[posturecode intValue] :[DD intValue]];
                }
                else if ([settcode intValue]==TRIGEMINY)
                {
                    if([globalposturecode isEqualToString:@"BOTH"])
                    {
                        for(int i=0;i<userSelectedPosturesArray.count;i++)
                        {
                            if([userSelectedPosturesArray[i] isEqualToString:@"laying-slug"])
                            {
                                 [config SET_TrigemenyStatus:LayingPostureCode :[ddflagg intValue] :[DD intValue]];
                                
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"standing-sitting-slug"]) // Standing / Sitting
                            {
                                 [config SET_TrigemenyStatus:StandingSittingPostureCode :[ddflagg intValue] :[DD intValue]];
                               
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"walking-slug"] ) // Walking
                            {
                                 [config SET_TrigemenyStatus:WalkingPostureCode :[ddflagg intValue] :[DD intValue]];
                                
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"running-slug"] ) // Running
                            {
                                
                                 [config SET_TrigemenyStatus:RunningPostureCode :[ddflagg intValue] :[DD intValue]];
                              
                            }
                        }
                    }
                    else
                     [config SET_TrigemenyStatus:[posturecode intValue] :[ddflagg intValue] :[DD intValue]];
                }
                else if ([settcode intValue]==COUPLET)
                {
                    if([globalposturecode isEqualToString:@"BOTH"])
                    {
                        for(int i=0;i<userSelectedPosturesArray.count;i++)
                        {
                            if([userSelectedPosturesArray[i] isEqualToString:@"laying-slug"])
                            {
                                  [config SET_CoupletStatus:LayingPostureCode :[ddflagg intValue] :[DD intValue]];
                                
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"standing-sitting-slug"]) // Standing / Sitting
                            {
                                  [config SET_CoupletStatus:StandingSittingPostureCode :[ddflagg intValue] :[DD intValue]];
                                
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"walking-slug"] ) // Walking
                            {
                                  [config SET_CoupletStatus:WalkingPostureCode :[ddflagg intValue] :[DD intValue]];
                               
                                
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"running-slug"] ) // Running
                            {
                                
                                  [config SET_CoupletStatus:RunningPostureCode :[ddflagg intValue] :[DD intValue]];
                                
                            }
                        }
                    }
                    else
                    [config SET_CoupletStatus:[posturecode intValue] :[ddflagg intValue] :[DD intValue]];
                }
                else if ([settcode intValue]==TRIPLET)
                {
                    if([globalposturecode isEqualToString:@"BOTH"])
                    {
                        for(int i=0;i<userSelectedPosturesArray.count;i++)
                        {
                            
                            if([userSelectedPosturesArray[i] isEqualToString:@"laying-slug"])
                            {
                                 [config SET_TripletStatus:LayingPostureCode :[ddflagg intValue] :[DD intValue]];
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"standing-sitting-slug"]) // Standing / Sitting
                            {
                                 [config SET_TripletStatus:StandingSittingPostureCode :[ddflagg intValue] :[DD intValue]];
                                
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"walking-slug"] ) // Walking
                            {
                                 [config SET_TripletStatus:WalkingPostureCode :[ddflagg intValue] :[DD intValue]];
                               
                                
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"running-slug"] ) // Running
                            {
                                [config SET_TripletStatus:RunningPostureCode :[ddflagg intValue] :[DD intValue]];
                                
                            }
                        }
                    }
                    else
                    [config SET_TripletStatus:[posturecode intValue] :[ddflagg intValue] :[DD intValue]];
                }
                
                else if ([settcode intValue]==ST_ELEVATION_VALUES)
                {
                    if([globalposturecode isEqualToString:@"BOTH"])
                    {
                        for(int i=0;i<userSelectedPosturesArray.count;i++)
                        {
                            if([userSelectedPosturesArray[i] isEqualToString:@"laying-slug"])
                            {
                                
                                 [config SET_STelev:[DD intValue] :[ddflagg intValue]  :LayingPostureCode];
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"standing-sitting-slug"]) // Standing / Sitting
                            {
                                
                                [config SET_STelev:[DD intValue] :[ddflagg intValue]:StandingSittingPostureCode];
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"walking-slug"] ) // Walking
                            {
                                
                                 [config SET_STelev:[DD intValue] :[ddflagg intValue] :WalkingPostureCode];
                                
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"running-slug"] ) // Running
                            {
                                
                                 [config SET_STelev:[DD intValue] :[ddflagg intValue]:RunningPostureCode];
                            }
                        }
                    }
                    else
                    [config SET_STelev:[DD intValue] :[ddflagg intValue] :[posturecode intValue]];
                }
                else if ([settcode intValue]==ST_DEPRESSION_VALUES)
                {
                    if([globalposturecode isEqualToString:@"BOTH"])
                    {
                        for(int i=0;i<userSelectedPosturesArray.count;i++)
                        {
                            if([userSelectedPosturesArray[i] isEqualToString:@"laying-slug"])
                            {
                                
                                [config SET_Depre:[DD intValue] :[ddflagg intValue] :LayingPostureCode];
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"standing-sitting-slug"]) // Standing / Sitting
                            {
                                
                               [config SET_Depre:[DD intValue] :[ddflagg intValue]:StandingSittingPostureCode];
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"walking-slug"] ) // Walking
                            {
                                
                                [config SET_Depre:[DD intValue] :[ddflagg intValue] :WalkingPostureCode];
                                
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"running-slug"] ) // Running
                            {
                                
                                [config SET_Depre:[DD intValue] :[ddflagg intValue]:RunningPostureCode];
                            }
                        }
                    }
                    else
                    [config SET_Depre:[DD intValue] :[ddflagg intValue] :[posturecode intValue]];
                }
                else if ([settcode intValue]==PROLNGED_QT_VALUES)
                {
                    if([globalposturecode isEqualToString:@"BOTH"])
                    {
                        for(int i=0;i<userSelectedPosturesArray.count;i++)
                        {
                            if([userSelectedPosturesArray[i] isEqualToString:@"laying-slug"])
                            {
                                
                                 [config SET_ProlongQT:[DD intValue] :[ddflagg intValue] :LayingPostureCode];
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"standing-sitting-slug"]) // Standing / Sitting
                            {
                                
                                 [config SET_ProlongQT:[DD intValue] :[ddflagg intValue]:StandingSittingPostureCode];
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"walking-slug"] ) // Walking
                            {
                                
                                 [config SET_ProlongQT:[DD intValue] :[ddflagg intValue] :WalkingPostureCode];
                                
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"running-slug"] ) // Running
                            {
                                
                                 [config SET_ProlongQT:[DD intValue] :[ddflagg intValue]:RunningPostureCode];
                            }
                        }
                    }
                    else
                    [config SET_ProlongQT:[DD intValue] :[ddflagg intValue] :[posturecode intValue]];
                }
                else if ([settcode intValue]==QRS_WIDE_WIDTH)
                {
                    if([globalposturecode isEqualToString:@"BOTH"])
                    {
                        for(int i=0;i<userSelectedPosturesArray.count;i++)
                        {
                            if([userSelectedPosturesArray[i] isEqualToString:@"laying-slug"])
                            {
                                
                                [config SET_QRSwidthWide:[DD intValue] :[ddflagg intValue] :LayingPostureCode];
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"standing-sitting-slug"]) // Standing / Sitting
                            {
                                
                                [config SET_QRSwidthWide:[DD intValue] :[ddflagg intValue]:StandingSittingPostureCode];
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"walking-slug"] ) // Walking
                            {
                                
                                [config SET_QRSwidthWide:[DD intValue] :[ddflagg intValue] :WalkingPostureCode];
                                
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"running-slug"] ) // Running
                            {
                                
                                [config SET_QRSwidthWide:[DD intValue] :[ddflagg intValue]:RunningPostureCode];
                            }
                        }
                    }
                    else
                    [config SET_QRSwidthWide:[DD intValue] :[ddflagg intValue] :[posturecode intValue]];
                    
                }
                else if ([settcode intValue]==ST_SUSPENSION)
                {
                    if([globalposturecode isEqualToString:@"BOTH"])
                    {
                        for(int i=0;i<userSelectedPosturesArray.count;i++)
                        {
                            if([userSelectedPosturesArray[i] isEqualToString:@"laying-slug"])
                            {
                                [config SET_SuspensionPercent:0 :[DD intValue] :LayingPostureCode];
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"standing-sitting-slug"]) // Standing / Sitting
                            {
                                
                                [config SET_SuspensionPercent:0 :[DD intValue] :StandingSittingPostureCode];
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"walking-slug"] ) // Walking
                            {
                                 [config SET_SuspensionPercent:0 :[DD intValue] :WalkingPostureCode];
                                
                                
                            }
                            else if ( [userSelectedPosturesArray[i] isEqualToString:@"running-slug"] ) // Running
                            {
                                 [config SET_SuspensionPercent:0 :[DD intValue] :RunningPostureCode];
                                
                            }
                        }
                    }
                    else
                         [config SET_SuspensionPercent:0 :[DD intValue] :[posturecode intValue]];
                        
                    
                    
                }
                else if ([settcode intValue]==TEMPERATURE_VALUE)
                {
                    [config SET_Temperature:[DD intValue] :0 :[ddflagg intValue] :[posturecode intValue]];
                }
                else if ([settcode intValue]==HIGH_RESPIRATION_RATE)
                {
                    [config SET_resperationRate:1 :[DD intValue] :[posturecode intValue] :[ddflagg intValue]];
                }
                else if ([settcode intValue]==LOW_RESPIRATION_RATE)
                {
                     [config SET_resperationRate:0 :[DD intValue] :[posturecode intValue] :[ddflagg intValue]];
                }
                else if ([settcode intValue]==APNEA_VALUE)
                {
                    
                    [config SET_Apnea:[ddflagg intValue] :[DD intValue] :[posturecode intValue]];
                }
                
                else if ([settcode intValue]==NO_MOTION_VALUE)
                {
                    int minutes = [DD intValue];
                    if ([ddflagg intValue] == 0) {
                        minutes = 0;
                    }
                    [config SET_NoMotion:minutes];
                }
                else if ([settcode intValue]==FALL_EVENT_VALUE)
                {
                    //[config SET_FallEvent:<#(Byte)#> :<#(int)#>];
                }
               
              
            }

       
       
        
        
      }
        
        [self reloadTableViewCommonFun];
        //println("After save to array data \(Main_Updated_NSMutDictonary_Array)")
        
        
    }
    
}



-(void)UpdateThresholdvalue:(int)data
{
    
    if (data == 160 || data == 161 || data == 164|| data == 175 || data == 171|| data == 162 || data == 163|| data == 157 || data == 172
        || data == 173 || data == 174|| data == 154 || data == 155|| data == 158 || data == 159|| data == 170)
    {
    
        NSDate *date = [[NSDate alloc] init];
        NSLog(@"Updating threshold record");
        GlobalVars *globals = [GlobalVars sharedInstance];
        CommonHelper * commhelp=[[CommonHelper alloc] init];
        
        NSString *Modifieddttime=[commhelp getDateUTC:@"yyyy-MM-dd" :@"MM/dd/yyyy HH:mm:ss" :DateAsNSDate :@"2014-04-25" :date];
        if([[globals Thres_posturecode] isEqualToString:@"BOTH"])
        {
            int posturecode = 0;
            for(int i=0;i<[globals ThresMultiplePosturesArray].count;i++)
            {
                
                if([[globals ThresMultiplePosturesArray][i] isEqualToString:@"laying-slug"])
                {
                     posturecode= LayingPostureCode;
                }
                else if ( [[globals ThresMultiplePosturesArray][i] isEqualToString:@"standing-sitting-slug"]) // Standing / Sitting
                {
                    posturecode= StandingSittingPostureCode;
                    
                }
                else if ( [[globals ThresMultiplePosturesArray][i] isEqualToString:@"walking-slug"] ) // Walking
                {
                    posturecode= WalkingPostureCode;
                    
                    
                }
                else if ( [[globals ThresMultiplePosturesArray][i] isEqualToString:@"running-slug"] ) // Running
                {
                    posturecode= RunningPostureCode;
                    
                }
                
                
                NSMutableArray *results = [[NSMutableArray alloc]init];
                NSMutableArray *results1 = [[NSMutableArray alloc]init];
                int flag=0;
                NSPredicate *pred;
                NSString* filter = @"%K == %@ && %K == %@";
                NSArray* args = @[@"code",[globals Thres_SetCode], @"posture",[NSString stringWithFormat:@"%d",posturecode]];
                if ([globals Thres_SetCode].length!=0) {
                    pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
                    flag=1;
                } else {
                    flag=0;
                    NSLog(@"Enter Corect code empty");
                    
                }
                
                if (flag == 1) {
                    
                    
                    // NSLog(@"predicate: %@",pred);
                    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
                    [fetchRequest setPredicate:pred];
                    results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
                    
                    if (results.count > 0) {
                        
                        NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
                        // NSLog(@"1 - %@", doctorssett);
                        [doctorssett setValue:[globals Thres_DD] forKey:@"dd"];
                        [doctorssett setValue:[NSString stringWithFormat:@"%@",[globals Thres_ddflagg]] forKey:@"ddflag"];
                        [doctorssett setValue:Modifieddttime forKey:@"modify_datetime"];
                        [doctorssett setValue:@"MCM" forKey:@"modify_from"];
                        [doctorssett setValue:@"0" forKey:@"updated_to_site"];
                        [doctorssett setValue:@"0" forKey:@"updated_to_device"];
                        [doctorssett setValue:@""  forKey:@"status"];
                        
                        [self.managedObjectContext save:nil];
                    } else {
                        NSLog(@"Enter Corect code number");
                    }
                    NSFetchRequest *fetchRequest1 = [[NSFetchRequest alloc]initWithEntityName:@"ThresholdSett"];
                    [fetchRequest1 setPredicate:pred];
                    results1 = [[self.managedObjectContext executeFetchRequest:fetchRequest1 error:nil] mutableCopy];
                    
                    if (results1.count > 0) {
                        
                        NSManagedObject *genthressett = (NSManagedObject *)[results1 objectAtIndex:0];
                        // NSLog(@"1 - %@", genthressett);
                        [genthressett setValue:[globals Thres_YD] forKey:@"yd"];
                        [genthressett setValue:[globals Thres_RD] forKey:@"rd"];
                        [genthressett setValue:[NSString stringWithFormat:@"%@",[globals Thres_flagg]] forKey:@"flag"];
                        [genthressett setValue:Modifieddttime forKey:@"modify_datetime"];
                        [genthressett setValue:@"MCM" forKey:@"modify_from"];
                        [genthressett setValue:@"0" forKey:@"updated_to_site"];
                        [genthressett setValue:@"0" forKey:@"update_to_device"];
                        [genthressett setValue:@"" forKey:@"status"];
                        [self.managedObjectContext save:nil];
                    } else {
                        NSLog(@"Enter Corect general code number");
                    }
                    
                    
                    
                }

            }
        }
      else
      {
        
        NSMutableArray *results = [[NSMutableArray alloc]init];
        NSMutableArray *results1 = [[NSMutableArray alloc]init];
        int flag=0;
        NSPredicate *pred;
        NSString* filter = @"%K == %@ && %K == %@";
        NSArray* args = @[@"code",[globals Thres_SetCode], @"posture", [globals Thres_posturecode]];
        if ([globals Thres_SetCode].length!=0) {
            pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
            flag=1;
        } else {
            flag=0;
            NSLog(@"Enter Corect code empty");
            
        }
        
        if (flag == 1) {
            
            
            // NSLog(@"predicate: %@",pred);
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
            [fetchRequest setPredicate:pred];
            results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
            
            if (results.count > 0) {
                
                NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
                // NSLog(@"1 - %@", doctorssett);
                [doctorssett setValue:[globals Thres_DD] forKey:@"dd"];
                [doctorssett setValue:[NSString stringWithFormat:@"%@",[globals Thres_ddflagg]] forKey:@"ddflag"];
                [doctorssett setValue:Modifieddttime forKey:@"modify_datetime"];
                [doctorssett setValue:@"MCM" forKey:@"modify_from"];
                [doctorssett setValue:@"0" forKey:@"updated_to_site"];
                [doctorssett setValue:@"0" forKey:@"updated_to_device"];
                [doctorssett setValue:@""  forKey:@"status"];
                
                [self.managedObjectContext save:nil];
            } else {
                NSLog(@"Enter Corect code number");
            }
            NSFetchRequest *fetchRequest1 = [[NSFetchRequest alloc]initWithEntityName:@"ThresholdSett"];
            [fetchRequest1 setPredicate:pred];
            results1 = [[self.managedObjectContext executeFetchRequest:fetchRequest1 error:nil] mutableCopy];
            
            if (results1.count > 0) {
                
                NSManagedObject *genthressett = (NSManagedObject *)[results1 objectAtIndex:0];
                // NSLog(@"1 - %@", genthressett);
                [genthressett setValue:[globals Thres_YD] forKey:@"yd"];
                [genthressett setValue:[globals Thres_RD] forKey:@"rd"];
                [genthressett setValue:[NSString stringWithFormat:@"%@",[globals Thres_flagg]] forKey:@"flag"];
                [genthressett setValue:Modifieddttime forKey:@"modify_datetime"];
                [genthressett setValue:@"MCM" forKey:@"modify_from"];
                [genthressett setValue:@"0" forKey:@"updated_to_site"];
                [genthressett setValue:@"0" forKey:@"update_to_device"];
                [genthressett setValue:@"" forKey:@"status"];
                [self.managedObjectContext save:nil];
            } else {
                NSLog(@"Enter Corect general code number");
            }
            
            
            
        }
      }
    }
   
}


//Doctorverification


- (void)presentAlertViewForPassword
{
    if(!standalone_Mode)
    {
   
        SDCAlertView *alert = [[SDCAlertView alloc] initWithTitle:@""
                                                          message:@"Please enter user name and password"
                                                         delegate:self
                                                cancelButtonTitle:@"Cancel"
                                                otherButtonTitles:@"OK", nil];
        [alert setAlertViewStyle:SDCAlertViewStyleLoginAndPasswordInput];
        [alert setTitle:@"Login"];
        [[alert textFieldAtIndex:0] setPlaceholder:@"User name"];
        
    
    self.spinner = [[UIActivityIndicatorView alloc] init];
    self.spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    [self.spinner setTranslatesAutoresizingMaskIntoConstraints:NO];
    //[spinner startAnimating];
    [alert.contentView addSubview:spinner];
    [spinner sdc_horizontallyCenterInSuperviewWithOffset:-60.0];
    [spinner sdc_verticallyCenterInSuperviewWithOffset:SDCAutoLayoutStandardSiblingDistance];
    
    
    
    lblspin = [[UILabel alloc] init];
    [lblspin setFont:[UIFont systemFontOfSize:12]];
    [lblspin setTranslatesAutoresizingMaskIntoConstraints:NO];
    [alert.contentView addSubview:lblspin];
    [lblspin sdc_horizontallyCenterInSuperview];
    [lblspin sdc_verticallyCenterInSuperviewWithOffset:SDCAutoLayoutStandardSiblingDistance];
    
    [alert show];
    
  }
  else
  {
      DoctorProfileViewController *doctorprofile=[[DoctorProfileViewController alloc]init];
      BOOL checkDocDetail=[doctorprofile UpdatedoctorPatient_rec];
      NSString *mssg;
      if(checkDocDetail)
      {
          
          mssg =@"In order to change this setting, the complete doctor details & patient details must be entered under User Screen and Doctor Screen";
          SDCAlertView *alert = [[SDCAlertView alloc] initWithTitle:@"Validation"
                                                            message:mssg
                                                           delegate:self
                                                  cancelButtonTitle:nil
                                                  otherButtonTitles:@"OK", nil];
          [alert setAlertViewStyle:SDCAlertViewStyleDefault];
          [alert show];
          
      }
      else
      {
          mssg =@"These settings should only be changed by a qualified physician. By approving this message you approve that you are the attending physician";
          SDCAlertView *alert = [[SDCAlertView alloc] initWithTitle:@"Login"
                                                            message:mssg
                                                           delegate:self
                                                  cancelButtonTitle:@"Cancel"
                                                  otherButtonTitles:@"OK", nil];
          [alert setAlertViewStyle:SDCAlertViewStylePlainTextInput];
          
          self.spinner = [[UIActivityIndicatorView alloc] init];
          self.spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
          [self.spinner setTranslatesAutoresizingMaskIntoConstraints:NO];
          //[spinner startAnimating];
          [alert.contentView addSubview:spinner];
          [spinner sdc_horizontallyCenterInSuperviewWithOffset:-60.0];
          [spinner sdc_verticallyCenterInSuperviewWithOffset:SDCAutoLayoutStandardSiblingDistance];
          
          
          
          lblspin = [[UILabel alloc] init];
          [lblspin setTranslatesAutoresizingMaskIntoConstraints:NO];
          [alert.contentView addSubview:lblspin];
          [lblspin sdc_horizontallyCenterInSuperview];
          [lblspin sdc_verticallyCenterInSuperviewWithOffset:SDCAutoLayoutStandardSiblingDistance];
          
          
          [alert show];
      }
      
      
      
  }



}



- (BOOL)alertView:(SDCAlertView *)alertView shouldDismissWithButtonIndex:(NSInteger)buttonIndex {
     GlobalVars *global = [GlobalVars sharedInstance];
    if(!standalone_Mode)
    {
    
    lblspin.text=@"";
    
    if(buttonIndex==1)
    {
        [self.spinner startAnimating];
        UITextField *username = [alertView textFieldAtIndex:0];
        NSLog(@"username: %@", username.text);
        
        
        UITextField *password = [alertView textFieldAtIndex:1];
        NSLog(@"password: %@", password.text);
        
        
        if(username.text.length == 0 || password.text.length == 0) //check your two textflied has value
        {
            [spinner stopAnimating];
            direction = 1;
            shakes = 0;
            AudioServicesPlaySystemSound (1352);
            [self shake:alertView];
            NSLog(@"EMPTY");
            
            return NO;
        }
        else
        {
            
            lblspin.text=@"Validating...";
            NSString *domainname =[[NSUserDefaults standardUserDefaults] stringForKey:@"DomainName"];
            
           NSString * Fetchingpath=[NSString stringWithFormat:@"https://%@/MCC/MCCWcfService.svc/CargiverAuthentication/?Username=%@&Password=%@&patientId=%@",domainname,username.text,password.text,patientid];
            NSString *bookurl=[Fetchingpath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *URL1=[NSURL URLWithString:bookurl];
            
            NSLog(@"url%@",URL1);
            
            NSURLRequest *request = [NSURLRequest requestWithURL:URL1];
            
            
            
            AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
            
            operation.responseSerializer = [AFHTTPResponseSerializer serializer];
            
            
            [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                NSLog(@"urlresp%@",string);
                
                if([string containsString:@"User Login successfully"])
                {
                    global.dropdownValidFlag=true;
                    lblspin.text=@"OK";
                    [spinner stopAnimating];
                     [NSTimer scheduledTimerWithTimeInterval: 120.0 target: self
                                                                      selector: @selector(callAfterSixtySecond:) userInfo: nil repeats: NO];
                    lockunlocButton.selected=true;
                    NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
                    [userDefault setObject:username.text forKey:@"CareGiver"];
                    [userDefault synchronize];
                    dispatch_async (dispatch_get_main_queue(), ^{
                        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:indexrw inSection:indexpthsec];
                        [self tableView:self.tableViewDoctorSettings didSelectRowAtIndexPath:indexPath];
                        
                        [self.tableViewDoctorSettings reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                        [self.tableViewDoctorSettings scrollToRowAtIndexPath :indexPath atScrollPosition:UITableViewScrollPositionTop animated: true];
                        
                    });
                    [self dismiss:alertView];
                    
                }
                else
                {
                    AudioServicesPlaySystemSound (1352);
                     string = [string stringByReplacingOccurrencesOfString:@"\"" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [string length])];
                    if([string containsString:@"UserName or password is incorrect"])
                    {
                        lblspin.text= @"The user name or password is incorrect";
                    }
                    else if([string containsString:@"Invalid User"])
                    {
                        lblspin.text= @"Invalid user";
                    }
                    else
                        lblspin.text= string;
                    [spinner stopAnimating];
                    direction = 1;
                    shakes = 0;
                    [self shake:alertView];
                    [alertView textFieldAtIndex:1].text=@"";
                    [alertView textFieldAtIndex:0].text=@"";
                    lockunlocButton.selected=false;
                }
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error Retrieving doctors profile"
                                                                    message:[error localizedDescription]
                                                                   delegate:nil
                                                          cancelButtonTitle:@"Ok"
                                                          otherButtonTitles:nil];
                
                
                [alertView show];
                lblspin.text=@"";
                [spinner stopAnimating];
                lockunlocButton.selected=false;
            }];
            
            
            [operation start];
            // [operation waitUntilFinished];
            return[[operation responseString]boolValue];
            
            
            
        }
        
        
    }
    else
        return YES;
   }
  else
    {
        if(buttonIndex==0)
        {
            return YES;
        }
        lblspin.text=@"";
        
        if(buttonIndex==1)
        {
            [self.spinner startAnimating];
            UITextField *Docname = [alertView textFieldAtIndex:0];
            NSLog(@"Docname: %@", Docname.text);
            
            
            
            
            if(Docname.text.length == 0) //check your two textflied has value
            {
                [spinner stopAnimating];
                direction = 1;
                shakes = 0;
                AudioServicesPlaySystemSound (1352);
                [self shake:alertView];
                NSLog(@"EMPTY");
                
                return NO;
            }
            else
            {
                
                lblspin.text=@"Validating...";
                
                
                if([Docname.text isEqualToString:doctorname])
                {
                    global.dropdownValidFlag=true;
                    lblspin.text=@"OK";
                    [spinner stopAnimating];
                    lockunlocButton.selected=true;
                    
                    [NSTimer scheduledTimerWithTimeInterval: 120.0 target: self
                                                   selector: @selector(callAfterSixtySecond:) userInfo: nil repeats: NO];
                    dispatch_async (dispatch_get_main_queue(), ^{
                        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:indexrw inSection:indexpthsec];
                        [self tableView:self.tableViewDoctorSettings didSelectRowAtIndexPath:indexPath];
                        
                        [self.tableViewDoctorSettings reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                        [self.tableViewDoctorSettings scrollToRowAtIndexPath :indexPath atScrollPosition:UITableViewScrollPositionTop animated: true];
                        
                    });
                    [self dismiss:alertView];
                    
                }
                else
                {
                    lockunlocButton.selected=false;
                    AudioServicesPlaySystemSound (1352);
                    lblspin.text=@"Invalid";
                    [spinner stopAnimating];
                    direction = 1;
                    shakes = 0;
                    [self shake:alertView];
                    [alertView textFieldAtIndex:0].text=@"";
                }
                
                
            }
            
            
        }
        else
            return YES;
        
    }
    

   return NO;

}

- (BOOL)alertView:(SDCAlertView *)alertView shouldDeselectButtonAtIndex:(NSInteger)buttonIndex {
    return YES;
}

-(void) callAfterSixtySecond:(NSTimer*) t
{
     GlobalVars *global = [GlobalVars sharedInstance];
    NSLog(@"Red");
   
    if(universalcellstatus==0)
    {
    global.dropdownValidFlag=false;
    lockunlocButton.selected=false;
    [self DoctorUniversalLoginTimer:NO];
    }
   
    /*dropdownValidFlag=false;
    lockunlocButton.selected=false;*/
}

-(void)shake:(SDCAlertView *)theOneYouWannaShake
{
    [UIView animateWithDuration:0.03 animations:^
     {
         theOneYouWannaShake.transform = CGAffineTransformMakeTranslation(5*direction, 0);
     }
                     completion:^(BOOL finished)
     {
         if(shakes >= 10)
         {
             theOneYouWannaShake.transform = CGAffineTransformIdentity;
             return;
         }
         shakes++;
         direction = direction * -1;
         [self shake:theOneYouWannaShake];
     }];
}

-(void)dismiss:(SDCAlertView*)alert
{
    [alert dismissWithClickedButtonIndex:0 animated:YES];
}

- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

-(void)fetchingpatientdata
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"PatientDetails" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        NSLog(@"%@", result);
        if (result.count > 0) {
            NSManagedObject *patient = (NSManagedObject *)[result objectAtIndex:0];
          //  NSLog(@"1 - %@", patient);
            patientid=[patient valueForKey:@"patient_id"];
            deviceid=[patient valueForKey:@"device_id"];
            doctorname=[patient valueForKey:@"doc_name"];
        }
    }
}

- (void)DoctorUniversalLoginTimer:(BOOL)isLoginStatus {
    NSDictionary *connectionDetails = @{@"NoDoctorLogin": @(isLoginStatus)};
    [[NSNotificationCenter defaultCenter] postNotificationName:RWT_DOCTOR_LOGIN_NOTIFICATION object:self userInfo:connectionDetails];
}

- (void)DoctorLoginStatus:(NSNotification *)notification {
    // Connection status changed. Indicate on GUI.
    BOOL status = [(NSNumber *) (notification.userInfo)[@"NoDoctorLogin"] boolValue];
    
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        // Set image based on connection status
        
        
        if (status) {
            
            lockunlocButton.selected=true;
        }
        else
            lockunlocButton.selected=false;
        
    });
}


@end
