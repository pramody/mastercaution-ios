//
//  DashboardViewController.m
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 12/06/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import "DashboardViewController.h"
#import <ExternalAccessory/EAAccessoryManager.h>
#import "LiveEcgMonitor.h"
#import <CoreLocation/CoreLocation.h>
#import "EventStorage.h"
#import "GlobalVars.h"
#import "ViewRecordedEcgViewController.h"
#import "Plotter.h"
#import "WCFUpload.h"
#import "BluetoothService.h"





#define IS_IPHONE5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

@interface DashboardViewController ()<CLLocationManagerDelegate>
@property (weak, nonatomic) NSTimer *timerObj;
@property int countSeconds;
@end

@implementation DashboardViewController
{
     CLLocationManager *locationManager;
}
@synthesize bodyMainView,orangeMainView,centerMainView,userIconImageView,tabDiv1,tabDiv2,tabDiv3,builtInSegmentMainView,load1,load2,load3,load4,countSeconds,timerObj,footerMainView,lblmessage,lblpatientlevel,lblpatientname,lbldate,lbltime,lbltempvalue,btntempunit,lblBPMvalue,lblRPMvalue,btnbatterystatus,btnbluetoothstatus,btncloudstatus,btngarmentstatus,lblRPMimage;
bool isdrag=FALSE;
bool isDragging;
CGRect bodyMainViewFrame;
CGRect footerMainViewFrame;

//bool isFooterReveal;
bool isFooterReveal=false;




- (void)viewDidLoad {
    [super viewDidLoad];
    AppDelegate * appDelegate = (AppDelegate *)UIApplication.sharedApplication.delegate ;
    appDelegate.orientationKey = @"PRO";
    [appDelegate forceOrientationTakePlace:appDelegate.orientationKey];

   [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    UIColor *blueBorderColorForTab = [UIColor colorWithPatternImage:[UIImage imageNamed:@"blue_border_color"]];
      UIColor *blueBorderColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"user_icon_border_color"]];
    userIconImageView.layer.borderWidth  =   1.5;
    userIconImageView.layer.borderColor  =    blueBorderColor.CGColor ;//(__bridge CGColorRef)([UIColor whiteColor]);

    
    userIconImageView.layer.cornerRadius    =   7.0;
    userIconImageView.layer.masksToBounds   = true;
    
    
    
    builtInSegmentMainView.layer.borderWidth  =   1.0;
    builtInSegmentMainView.layer.borderColor =   blueBorderColorForTab.CGColor ; // (__bridge CGColorRef)([UIColor whiteColor]);
    
    builtInSegmentMainView.layer.cornerRadius =   7.0;
    builtInSegmentMainView.layer.masksToBounds = true;
    
    tabDiv1.backgroundColor =blueBorderColorForTab;

    tabDiv2.backgroundColor = blueBorderColorForTab;

    tabDiv3.backgroundColor =blueBorderColorForTab;

    
    orangeMainView.layer.cornerRadius =   7.0;
    orangeMainView.layer.masksToBounds = true;
    orangeMainView.hidden=true;
    
    bodyMainViewFrame      =   self.bodyMainView.frame;
    footerMainViewFrame    =   self.footerMainView.frame;
    
   
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selectorhere) name:@"buttonPressed" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopservice) name:@"stopservice" object:nil];
   // [self initailsetup];
   
  
   // [logger log:@"Event Title" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"value1", @"key1", [NSNumber numberWithBool:false] ,@"installed", nil] error:false];
  /*  NSLogger *logger = [[NSLogger alloc] init];
   
    NSLog(@"Logger Print: %@" ,[logger logPrint]);
    
  
    [[[UIApplication sharedApplication] scheduledLocalNotifications] enumerateObjectsUsingBlock:^(UILocalNotification *notification, NSUInteger idx, BOOL *stop) {
        NSLog(@"Notification %lu: %@",(unsigned long)idx, notification);
           [[UIApplication sharedApplication] cancelLocalNotification:notification] ;
    }];*/
    
    
    /*NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setDay: 29];
    [components setMonth: 9];
    [components setYear: 2015];
    [components setHour: 9];
    [components setMinute:36];
    [components setSecond: 0];
    [calendar setTimeZone: [NSTimeZone defaultTimeZone]];
    NSDate *dateToFire = [calendar dateFromComponents:components];
   
    UIApplication* app = [UIApplication sharedApplication];
    
    UILocalNotification* notifyAlarm = [[UILocalNotification alloc] init];
    if (notifyAlarm)
    {
        notifyAlarm.fireDate = dateToFire;
        notifyAlarm.timeZone = [NSTimeZone defaultTimeZone];
        notifyAlarm.repeatInterval = kCFCalendarUnitDay;
        notifyAlarm.soundName = UILocalNotificationDefaultSoundName;
        notifyAlarm.alertBody = @"wear your garment for getting ecg";
        
        [app scheduleLocalNotification:notifyAlarm];
    }
   */
    
  
   
    
}



-(void)initailsetup
{
    NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
   
    NSData *usrdta=[userDefault objectForKey:@"user_details"];
   // NSLog(@"%@",usrdta);
    
    //userIconImageView.image=[usrdta valueForKey:@"PatientImage"];
    NSData* imageData = [usrdta valueForKey:@"PatientImage"];
   userIconImageView.image = [UIImage imageWithData:imageData];
    
    //lblpatientname.text= [usrdta valueForKey:@"PatientName"];
    //lblpatientlevel.text=[NSString stringWithFormat:@"%@|%@",[usrdta valueForKey:@"PatientUid"],[usrdta valueForKey:@"Patientlevel"]];
    
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
   
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"PatientDetails" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
       // NSLog(@"%@", result);
        if (result.count > 0) {
            NSManagedObject *patient = (NSManagedObject *)[result objectAtIndex:0];
           // NSLog(@"1 - %@", patient);
            lblpatientname.text=[NSString stringWithFormat:@"%@ %@", [patient valueForKey:@"given_name"],[patient valueForKey:@"family_name"]];
            
            
            NSString *  Patientlevel   = [patient valueForKey:@"level"];
            if([Patientlevel isEqualToString:@"Level1"])
                Patientlevel=@"SAFE GUARD MODE";
            else if([Patientlevel isEqualToString:@"Level2"])
                Patientlevel=@"PREMIUM GUARD MODE";
            else if([Patientlevel isEqualToString:@"Level3"])
                Patientlevel=@"AUTO PREMIUM SAFE GUARD MODE";
            else
                Patientlevel=@"PERSONAL ALERT GUARDIAN";
            
            lblpatientlevel.text=[NSString stringWithFormat:@"%@",Patientlevel];
        }
    }
    
    [NSLocale availableLocaleIdentifiers];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
                        
    //setting date time
    NSDate *date = [[NSDate alloc] init];
   // NSLog(@"%@", date);
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                        [dateFormatter setLocale:locale];
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSString *dateString = [dateFormatter stringFromDate:date];
    [dateFormatter setDateFormat:@"dd MMM YY"];
     NSString *dateString1 = [dateFormatter stringFromDate:date];
   // NSLog(@"%@", dateString);
     // NSLog(@"%@", dateString1);
    lbldate.text=dateString1;
    lbltime.text=dateString;
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitHour fromDate:[NSDate date]];
    NSInteger hour = [components hour];
    
    if(hour >= 0 && hour < 12)
    {
       // NSLog(@"Good morning, foo");
        lblmessage.text=[NSString stringWithFormat:@"%@, %@",@"Good Morning",lblpatientname.text];
    }
    else if(hour >= 12 && hour < 17)
    {
       // NSLog(@"Good afternoon");
      lblmessage.text=[NSString stringWithFormat:@"%@, %@",@"Good Afternoon",lblpatientname.text];
    }
    else if(hour >= 17)
    {
       // NSLog(@"Good evening, foo");
      lblmessage.text=[NSString stringWithFormat:@"%@, %@",@"Good Evening",lblpatientname.text];
    }
    
    
    //temperature value
   GlobalVars *globals = [GlobalVars sharedInstance];
    NSLog(@"currentsett:%u",[globals currentSetting]);
    NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
    NSString *temperaturetype=[TempDefault objectForKey:@"TEMPERATURE"];
    if([temperaturetype isEqualToString:@"CELSIUS"])
    {
       [btntempunit setTitle:[NSString stringWithFormat:@"C%@", @"\u00B0"] forState: UIControlStateNormal];
        lbltempvalue.text=[NSString stringWithFormat:@"%ld",(long)[globals mcd_Temperature]];
        
    }
    else
    {
        [btntempunit setTitle:[NSString stringWithFormat:@"F%@", @"\u00B0"] forState: UIControlStateNormal];
        lbltempvalue.text=[NSString stringWithFormat:@"%ld",(long)[globals mcd_Temperature]];
    }
    
    lblBPMvalue.text=[NSString stringWithFormat:@"%ld",(long)[globals mcd_HeartRate]];
     lblRPMvalue.text=[NSString stringWithFormat:@"%ld",(long)[globals mcd_RespirationRate]];
  
    [btntempunit setNeedsDisplay];
    [self selectorhere];
}


- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self initailsetup];
    [self checkinternet];
   self.slidingViewController.topViewAnchoredGesture = ECSlidingViewControllerAnchoredGestureTapping | ECSlidingViewControllerAnchoredGesturePanning;
    self.slidingViewController.customAnchoredGestures =@[];
   [self.navigationController.view addGestureRecognizer:self.slidingViewController.panGesture];
   [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    
    
    AppDelegate * appDelegate = (  AppDelegate *)UIApplication.sharedApplication.delegate ;
    appDelegate.orientationKey = @"PRO";
    [appDelegate forceOrientationTakePlace:appDelegate.orientationKey];
    
    
  
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(IBAction)leftButtonClick:(id)sender
{
    isFooterReveal = false;
    [self hideFooterView];
    [self.slidingViewController anchorTopViewToRightAnimated:YES];
   
}

-(IBAction)rightButtonClick:(id)sender
{
   // self.slidingViewController.anchorLeftPeekAmount = 44;
    isFooterReveal = false;
    [self hideFooterView];
    [self.slidingViewController anchorTopViewToLeftAnimated:YES];
}

-(IBAction)ViewRecordEcgClick:(id)sender
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Mainstoryboard_iPad" bundle:nil];
        ViewRecordedEcgViewController * controller = (ViewRecordedEcgViewController *)[storyboard instantiateViewControllerWithIdentifier:@"view_recorded_ecg_storyboard_id"];
        [self presentViewController:controller animated:YES completion:nil];
        
    }
    else
    {
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ViewRecordedEcgViewController * controller = (ViewRecordedEcgViewController *)[storyboard instantiateViewControllerWithIdentifier:@"view_recorded_ecg_storyboard_id"];
        [self presentViewController:controller animated:YES completion:nil];
        
    }
  


}

-(IBAction)viewliveecg:(id)sender
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Mainstoryboard_iPad" bundle:nil];
        LiveEcgMonitor * controller = (LiveEcgMonitor *)[storyboard instantiateViewControllerWithIdentifier:@"LiveEcgMonitor"];
        [self presentViewController:controller animated:YES completion:nil];
        
    }
    else
    {
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
         LiveEcgMonitor * controller = (LiveEcgMonitor *)[storyboard instantiateViewControllerWithIdentifier:@"LiveEcgMonitor"];
        [self presentViewController:controller animated:YES completion:nil];
        
    }
    
}


-(IBAction)Orangebtnclose:(id)sender
{
    [self.orangeMainView removeFromSuperview];
}


//Footer view action
-(IBAction)handleFooterDrag:(UIPanGestureRecognizer*) recognizer
{
    if(isdrag&&recognizer.state ==UIGestureRecognizerStateEnded)
    {
        isdrag=false;
        CGPoint origin = [recognizer velocityInView:self.view];
        CGFloat velocity=origin.y;
        CGFloat vertical;
        NSTimeInterval duration;
        if(velocity<0)
        {
            vertical=self.footerMainView.frame.origin.y - self.bodyMainView.frame.origin.y;
            duration  = ( MIN(ABS(vertical / velocity), 1.0 ) );
            [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
               [ self showFooterView1];
            } completion:^(BOOL finished){ isFooterReveal = true;}];
        }
        else
        {
            vertical = self.footerMainView.frame.origin.y - self.bodyMainView.frame.origin.y;
            duration =  (MIN(ABS(vertical / velocity), 1.0 ));
            [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
                [self hideFooterView];
            } completion:^(BOOL finished){ isFooterReveal = false;}];
        }
       
    }
    else if (isdrag)
    {
        //var endDragAt : CGFloat = 417
        
        CGFloat endDragAt =  self.revealFooterIs;
        
        //println("2")
        //[self performSegueWithIdentifier:kRevealShadeViewSegueIdentifier sender:nil];
        // Keep track of where we are
        
        CGPoint origin    =  [recognizer locationInView:self.view];
        
        // println("Origin \(origin)")
        if(origin.y <= endDragAt)
        {
            //println("Stop dragging")
            //self.dragging = false;
            //self.footerReveal = true
        }
        
        // As long as we aren't going above the top of the view, have it follow the drag
        
        if(CGRectContainsPoint(self.view.frame, origin))
        {
            //println(" ***** ***** Origin Y \(origin.y)")
            // Only allow dragging to a certain point. Don't let drag further down.
            CGPoint translatedPoint = [recognizer translationInView:self.view];
            //  println("translatedPoint \(translatedPoint)")
            // Our offset is different depending on if the revealable view is showing or not
           
            CGFloat offset = (isFooterReveal) ? (bodyMainViewFrame.size.height - endDragAt): 0.0; // // for iPhone 5
            //let offset = (self.isFooterReveal) ? (self.bodyMainViewFrame.size.height - 217): 0.0 // // for iPhone 4s
           
            if(translatedPoint.y < offset)
            {
                //origin.y >= endDragAt
                // Track the drag
                if(origin.y <= endDragAt)
                { return ;}
                
               [self offsetFrames:(translatedPoint.y - offset)];
                
            }
            else
            {
                // Stick to the bottom
                [self hideFooterView];
                
            }
        }
    }

    else if (recognizer.state == UIGestureRecognizerStateBegan)
    {
        // println("3")
        // Now, we are dragging
        isdrag = TRUE;
        
        CGPoint origin = [recognizer velocityInView:self.view];
        CGFloat velocity=origin.y;
        //println("Velocity Chnages to  = \(velocity)")
        if (velocity < 0&&(isFooterReveal == true))
        {
            //println("Already up and Moving up, ")
            isdrag = FALSE;
        }
        else if (velocity > 0 && isFooterReveal == false)
        {
            //println("Already Down and Moving Down, ")
            isdrag=FALSE;
        }
        else
        {
            // println("No issue ");
            isdrag = TRUE;
        }
    }
    else
    {
        // println("4")
        // println("Sorry i cant DRAG out of my order decided");
         [self stopTimer];
    }
    
}


-(void)hideFooterView
{
    [self offsetFrames:0.0];
    
    
    [self makeAllLoaderHidden];
}

-(CGFloat) revealFooterIs
{
   
    CGFloat footerViewYWhenNormal       =   footerMainViewFrame.origin.y; // When Footer View is Normal
    CGFloat footerViewHWhenNormal       =   footerMainViewFrame.size.height; // When Footer View is Normal
    CGFloat footerRevealIs     =   55;//55 // Will be same on all Sizes Device
    CGFloat revealNewFooterYis          =   (footerViewYWhenNormal - (footerViewHWhenNormal - footerRevealIs));
    
    // println("Reveal Footer till Y \(revealNewFooterYis)")
    return revealNewFooterYis;
}
-(void)showFooterView1
{
    
    //println("Constant reavel footer")
    CGFloat revealTill              =   self.revealFooterIs;
    CGFloat mainBoadyViewHeight     =   bodyMainViewFrame.size.height;
    CGFloat param1       =   -(mainBoadyViewHeight - revealTill);
    [self offsetFrames:param1];
    
    
}



-(void)offsetFrames:(CGFloat)offSetValue
{
    NSLog(@"flod%f",offSetValue);
    self.footerMainView.frame = CGRectOffset(footerMainViewFrame, 0.0,offSetValue);
}





/*- (void)showFooterView {
    
    [self.view bringSubviewToFront:self.footerMainView];
    
    [UIView beginAnimations:@"Show"
                    context:nil];
    [UIView setAnimationDuration:1.5F];
    
    CGRect frame = self.footerMainView.frame;
    
    if (IS_IPHONE5) {
        if(isdrag){
            frame.origin.y = 568-420;
        }else{
            frame.origin.y = 568-150;
        }
    } else {
        frame.origin.y = 1024 - 259;

    }
    
    self.footerMainView.frame = frame;
    [UIView commitAnimations];
}*/

/*- (void) hideFooterView {
    
    [UIView beginAnimations:@"hide"
                    context:nil];
    [UIView setAnimationDuration:1.5F];
    
    CGRect frame = self.footerMainView.frame;
    
    if (IS_IPHONE5) {
        
        if(isdrag){
            frame.origin.y = 568-220;
        }else{
            frame.origin.y = 510;
        }
    } else {
        frame.origin.y = 959;
    }
    self.footerMainView.frame = frame;
    [self makeAllLoaderHidden];
    [self stopTimer];
    [UIView commitAnimations];
}*/


-(IBAction)touchUpInsideAction:(id)sender
{
    //2
    NSLog(@"%s count is %i", __PRETTY_FUNCTION__, countSeconds);
    
    if (countSeconds < 4)
    {
        // Left the button when value was 0, 1, 2, 3 ,  and 4
        [self makeAllLoaderHidden];
        [self stopTimer];
    }
    else
    {
        // If greater than 4 second go ahead
    }
    
}


-(IBAction)touchDownAction:(id)sender
{
    // Touch and dont leave the button on touch will get called
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    countSeconds = 0;
    timerObj = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(callAfterSecondsPass:) userInfo:timerObj repeats:YES];
    
}





-(void)callAfterSecondsPass:(NSTimer *)timeIs
{
    countSeconds = countSeconds + timeIs.timeInterval;
    
    NSLog(@"Call after seconds here %d", countSeconds);
    
    if (countSeconds == 1)
    {
        [load1 setHidden:false];
    }
    else if (countSeconds == 2)
    {
        [load2 setHidden:false];
    }
    else if (countSeconds == 3)
    {
        [load3 setHidden:false];
    }
    else if (countSeconds == 4)
    {
        [load4 setHidden:false];
       
        [self patientcall];
        
    }
    else
    {
        [self stopTimer];
    }
}

-(void)stopTimer
{
    [timerObj invalidate];
    timerObj = nil;
}

- (void)makeAllLoaderHidden
{
    [load1 setHidden:TRUE];
    [load2 setHidden:TRUE];
    [load3 setHidden:TRUE];
    [load4 setHidden:TRUE];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return (toInterfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate {
    return YES;
}
/*- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}*/



-(void)patientcall
{
    [self stopTimer];
    ConfigureMCDController *config=[[ConfigureMCDController alloc]init];
    [config ManualPatientCall:5];

}

//location
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    NSLogger * logger=[[NSLogger alloc]init];
    logger.degbugger = true;
    [logger log:@"Location Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"Failed to Get Your Location", @"Adding clinical record ", nil] error:TRUE];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        longititude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        latitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        location=[NSString stringWithFormat:@"%@,%@",latitude,longititude];
    }
    [locationManager stopUpdatingLocation];
}



-(void)fetchingpatientdata
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"PatientDetails" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
       // NSLog(@"%@", result);
        if (result.count > 0) {
            NSManagedObject *patient = (NSManagedObject *)[result objectAtIndex:0];
           // NSLog(@"1 - %@", patient);
            patientid=[patient valueForKey:@"patient_id"];
            deviceid=[patient valueForKey:@"device_id"];
        }
    }
}

- (void) updatetemp
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"buttonPressed" object:nil];
    
   
}

-(void)selectorhere {
    
    int Respiration;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSLogger *logger=[[NSLogger alloc]init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"MCDSettings" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
        logger.degbugger = true;
        [logger log:@"Coredata Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:[error localizedDescription], @"Adding clinical record", nil] error:TRUE];
        
    } else {
        
        if (result.count > 0) {
            NSManagedObject *mcdsett = (NSManagedObject *)[result objectAtIndex:0];
            
            Respiration=[[mcdsett valueForKey:@"respiration"]intValue];
        }
    }
    
    
    
    
    
    GlobalVars *globals = [GlobalVars sharedInstance];
    if([globals mcd_HeartRate]==-1)
    {
        lblBPMvalue.text=@"-";
    }
    else
    lblBPMvalue.text=[NSString stringWithFormat:@"%ld",(long)[globals mcd_HeartRate]];
    
    if(Respiration==0)
    {
       // lblRPMvalue.text=@"-";
        lblRPMvalue.hidden=YES;
        lblRPMimage.hidden=YES;
    }
    else
    {
        lblRPMvalue.hidden=NO;
        lblRPMimage.hidden=NO;
        if([globals mcd_RespirationRate]==200)
        {
            lblRPMvalue.text=@"-";
        }
        else
        {
            if([globals mcd_RespirationRate]==-1)
            {
                lblRPMvalue.text=@"-";
            }
            else
             lblRPMvalue.text=[NSString stringWithFormat:@"%ld",(long)[globals mcd_RespirationRate]];
        }
    }
   
    
    NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
     CommonHelper *help=[[CommonHelper alloc]init];
        
        NSString *temperaturetype=[TempDefault objectForKey:@"TEMPERATURE"];
        if([temperaturetype isEqualToString:@"CELSIUS"])
        {
            [btntempunit setTitle:[NSString stringWithFormat:@"C%@", @"\u00B0"] forState: UIControlStateNormal];
            if([globals mcd_Temperature]==-1)
            {
                lbltempvalue.text=@"-";
            }
            else
            {
            lbltempvalue.text= [NSString stringWithFormat:@"%ld",(long)[globals mcd_Temperature]];
              
                // tableViewCellCustom.tempunits.text=[NSString stringWithFormat:@"F%@", @"\u00B0"];
               
            }
            
        }
        else
        {
            [btntempunit setTitle:[NSString stringWithFormat:@"F%@", @"\u00B0"] forState: UIControlStateNormal];
            if([globals mcd_Temperature]==-1)
            {
                lbltempvalue.text=@"-";
            }
            else
            lbltempvalue.text=[help celsiustoFahernheit:[NSString stringWithFormat:@"%ld",(long)[globals mcd_Temperature]]];
        }
        
    
    
        if([globals.mcd_bluetoothstatus isEqualToString:@"0"])
       {
           UIImage *btnImage = [UIImage imageNamed:@"bluetooth_icon_normal.png"];
           [btnbluetoothstatus setImage:btnImage forState:UIControlStateNormal];
       }
       else
       {
           UIImage *btnImage = [UIImage imageNamed:@"bluetooth_icon_error.png"];
           [btnbluetoothstatus setImage:btnImage forState:UIControlStateNormal];
       }
    
    //garment status
    if([globals.mcd_garmentstatus isEqualToString:@"0"])
    {
        UIImage *btnImage = [UIImage imageNamed:@"vest_icon_normal.png"];
        [btngarmentstatus setImage:btnImage forState:UIControlStateNormal];
    }
    else
    {
        UIImage *btnImage = [UIImage imageNamed:@"vest_icon_error.png"];
        [btngarmentstatus setImage:btnImage forState:UIControlStateNormal];
    }

    
       //batterystatus
    
    switch (globals.mcd_BatteryStatus) {
        case 0:
        {
            UIImage *btnImage = [UIImage imageNamed:@"battery_icon_error.png"];
            [btnbatterystatus setImage:btnImage forState:UIControlStateNormal];
        }
             break;
        case 1:
        
        case 2:
        {
            NSLog(@"FULL");
            UIImage *btnImage = [UIImage imageNamed:@"battery_icon_normal.png"];
            [btnbatterystatus setImage:btnImage forState:UIControlStateNormal];
        }
            break;
            
        case 3:
        {
            NSLog(@"Third part");
            UIImage *btnImage = [UIImage imageNamed:@"battery_icon_Third.png"];
            [btnbatterystatus setImage:btnImage forState:UIControlStateNormal];
        }
            break;
        case 4:
        {
            NSLog(@"Quater part");
            UIImage *btnImage = [UIImage imageNamed:@"battery_icon_Second.png"];
            [btnbatterystatus setImage:btnImage forState:UIControlStateNormal];
        }
            break;
        case 5:
        {
            NSLog(@"LAst part");
            UIImage *btnImage = [UIImage imageNamed:@"battery_icon_First.png"];
            [btnbatterystatus setImage:btnImage forState:UIControlStateNormal];
        }
            break;
        case 6:
        {
            NSLog(@"Empty");
            UIImage *btnImage = [UIImage imageNamed:@"battery_icon_error.png"];
            [btnbatterystatus setImage:btnImage forState:UIControlStateNormal];
        }
            break;
        default:
            break;
    }
    
   
        [lbltempvalue setNeedsDisplay];
        [lblBPMvalue setNeedsDisplay];
        [lblRPMvalue setNeedsDisplay];
        [btnbatterystatus setNeedsDisplay];
        [btnbluetoothstatus setNeedsDisplay];
        [btngarmentstatus setNeedsDisplay];
}

-(void)checkinternet
{
       GlobalVars *global = [GlobalVars sharedInstance];
       global.bluetoothService = [[BluetoothService alloc] initWithFrequency: 5]; //execute doInBackground each 60 seconds
   [ global.bluetoothService startService];
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        
        NSLog(@"Reachability changed: %@", AFStringFromNetworkReachabilityStatus(status));
        
        
        switch (status) {
            case AFNetworkReachabilityStatusReachableViaWWAN:
            case AFNetworkReachabilityStatusReachableViaWiFi:
            {
                // -- Reachable -- //
                NSLog(@"Reachable");
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                BOOL standalone_Mode = [defaults boolForKey:@"ActiveStandalonemode"];
                if(!standalone_Mode)
                {
                global.WCFService  = [[WCFUpload alloc] initWithFrequency: 60];
                [ global.WCFService startService];
                }
                else
                {
                    global.WCFService  = [[WCFUpload alloc] initWithFrequency:120];
                    [ global.WCFService startService];
                }
                global.mcd_cloudstatus=@"0";
              
                UIImage *btnImage = [UIImage imageNamed:@"cloud_icon_normal.png"];
                [btncloudstatus setImage:btnImage forState:UIControlStateNormal];
                
                global.Internetflag=1;
                
                if( global.BluetoothConnectionflag==2)
                {
                    if(!global.XMPPflag_on_off)
                    {
                        AppDelegate * appDelegate = (AppDelegate *)UIApplication.sharedApplication.delegate;
                        [appDelegate startxmpp];
                    }
                }
                
                
                break;
            }
            case AFNetworkReachabilityStatusNotReachable:
            default:
            {
                // -- Not reachable -- //
                NSLog(@"Not Reachable");
                
                [global.WCFService stopService];
                 global.mcd_cloudstatus=@"1";
                UIImage *btnImage = [UIImage imageNamed:@"cloud_icon_error.png"];
                [btncloudstatus setImage:btnImage forState:UIControlStateNormal];
                
               
                    //[myService stopService];
                global.Internetflag=2;
                
                NSLogger *logger=[[NSLogger alloc]init];
                logger.degbugger = true;
                
                [logger log:@"Internet Connection" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"No internet connectivity", @"First Time Login", nil] error:TRUE];
                
                break;
            }
        }
        
    }];
    
}



- (void)updateservicestatus
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"stopservice" object:nil];
    
    
}
-(void)stopservice
{
    BOOL standalone_Mode;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    standalone_Mode = [defaults boolForKey:@"ActiveStandalonemode"];
    
    if(!standalone_Mode)
    {
        AppDelegate * appDelegate = (AppDelegate *)UIApplication.sharedApplication.delegate;
        [appDelegate disconnect];
    }
    
     GlobalVars *global = [GlobalVars sharedInstance];
    [ global.bluetoothService stopService];
    [ global.WCFService stopService];
    
}

@end
