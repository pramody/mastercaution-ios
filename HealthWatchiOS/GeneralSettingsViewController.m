//
//  GeneralSettingsViewController.m
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 01/07/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import "GeneralSettingsViewController.h"

#import "SDCAlertView.h"
#import "SDCAlertViewController.h"
#import <UIView+SDCAutoLayout.h>
#import <AudioToolbox/AudioToolbox.h>

#import "AHKActionSheet.h"
#import "PatientDetails.h"
#import "BuiltInTestViewController.h"
#import "ConfigureMCDController.h"


#import "SDCAlertController.h"
#import "ViewRecordedEcgViewController.h"
#import "DoctorProfileViewController.h"
#import "WCFUpload.h"

@interface GeneralSettingsViewController ()

@end

@implementation GeneralSettingsViewController
@synthesize headerBlueView,lblspin,spinner,temperaturelbl,rightArrowBlackImg,rightArrowBlackImg1,rightArrowBlackImg2,rightArrowBlackImg3,changelevellbl,Weightlbl,Heightlbl,rightArrowBlackImg4,rightArrowBlackImg5;
bool popupflag;

- (void)viewDidLoad {
    [super viewDidLoad];
    AppDelegate * appDelegate = ( AppDelegate *)UIApplication.sharedApplication.delegate ;
    appDelegate.orientationKey = @"PRO";
    [appDelegate forceOrientationTakePlace:appDelegate.orientationKey];
    self.slidingViewController.topViewAnchoredGesture = ECSlidingViewControllerAnchoredGestureTapping | ECSlidingViewControllerAnchoredGesturePanning;
    self.slidingViewController.customAnchoredGestures =@[];
    NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
    NSString *temperaturetype=[TempDefault objectForKey:@"TEMPERATURE"];
    if([temperaturetype isEqualToString:@"CELSIUS"])
    {
         temperaturelbl.text=@"Celsius";
    }
    else
    {
        
    temperaturelbl.text=@"Fahrenheit";
    }
    
    
   
    NSString *weighttype=[TempDefault objectForKey:@"WEIGHT"];
    if([weighttype isEqualToString:@"KILOGRAM"])
    {
        Weightlbl.text=@"Kilogram";
    }
    else
    {
        
        Weightlbl.text=@"Pounds";
    }
    
   
    NSString *heighttype=[TempDefault objectForKey:@"HEIGHT"];
    if([heighttype isEqualToString:@"FEET AND INCHES"])
    {
        Heightlbl.text=@"Feet and Inches";
    }
    else
    {
        
        Heightlbl.text=@"Centimeters";
    }
    
    
    //changelevel
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"PatientDetails" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
      //  NSLog(@"%@", result);
        if (result.count > 0) {
            NSManagedObject *patient = (NSManagedObject *)[result objectAtIndex:0];
            //NSLog(@"1 - %@", patient);
            
            
            NSString *  Patientlevel   = [patient valueForKey:@"level"];
            if([Patientlevel isEqualToString:@"Level1"])
                Patientlevel=@"SAFE GUARD MODE";
            else if([Patientlevel isEqualToString:@"Level2"])
                Patientlevel=@"PREMIUM GUARD MODE";
            else if([Patientlevel isEqualToString:@"Level3"])
                Patientlevel=@"AUTO PREMIUM SAFE GUARD MODE";
            else
                Patientlevel=@"PERSONAL ALERT GUARDIAN";
            
            changelevellbl.text=Patientlevel;
        }
    }

    [self fetchingpatientdata];
    [self setupInitialValue];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupInitialValue
{
    blueColorMainAppUIColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"blue_bg_full.png"]];
    headerBlueView.backgroundColor = blueColorMainAppUIColor;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
     standalone_Mode = [defaults boolForKey:@"ActiveStandalonemode"];
    rightArrowBlackImg.transform = CGAffineTransformMakeRotation(6.14);
    rightArrowBlackImg1.transform = CGAffineTransformMakeRotation(6.14);
    rightArrowBlackImg2.transform = CGAffineTransformMakeRotation(6.14);
    rightArrowBlackImg3.transform = CGAffineTransformMakeRotation(6.14);
    rightArrowBlackImg4.transform=CGAffineTransformMakeRotation(6.14);
    rightArrowBlackImg5.transform=CGAffineTransformMakeRotation(6.14);
   
}

-(IBAction)goToDashboard:(id)sender{
    self.slidingViewController.topViewController.view.layer.transform = CATransform3DMakeScale(1, 1, 1);
    self.slidingViewController.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"DashboardStoryboardId"];
}

-(IBAction)leftSideButtonAction:(id)sender{
    [self.slidingViewController anchorTopViewToRightAnimated:YES];
}


-(IBAction)Builintest:(id)sender
{
    GlobalVars *global = [GlobalVars sharedInstance];
    if(global.BluetoothConnectionflag==2)
    {

    [self.slidingViewController anchorTopViewToLeftAnimated:YES];
     rightArrowBlackImg.transform = CGAffineTransformMakeRotation(6.14);
     rightArrowBlackImg1.transform = CGAffineTransformMakeRotation(7.85);
     rightArrowBlackImg2.transform = CGAffineTransformMakeRotation(6.14);
     rightArrowBlackImg3.transform = CGAffineTransformMakeRotation(6.14);
     rightArrowBlackImg4.transform=CGAffineTransformMakeRotation(6.14);
     rightArrowBlackImg5.transform=CGAffineTransformMakeRotation(6.14);
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                        message:@"Please Connect MCD Device"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
   
}
-(IBAction)ConfigureUnit:(id)sender
{
    
    
    rightArrowBlackImg.transform = CGAffineTransformMakeRotation(7.85);
    rightArrowBlackImg1.transform = CGAffineTransformMakeRotation(6.14);
    rightArrowBlackImg2.transform = CGAffineTransformMakeRotation(6.14);
    rightArrowBlackImg3.transform = CGAffineTransformMakeRotation(6.14);
    rightArrowBlackImg4.transform = CGAffineTransformMakeRotation(6.14);
    rightArrowBlackImg5.transform = CGAffineTransformMakeRotation(6.14);
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Mainstoryboard_iPad" bundle:nil];
        ConfigureMCDController * controller = (ConfigureMCDController *)[storyboard instantiateViewControllerWithIdentifier:@"ConfigureMCDControllerID"];
        [self presentViewController:controller animated:YES completion:nil];
    }
    else
    {
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ConfigureMCDController * controller = (ConfigureMCDController *)[storyboard instantiateViewControllerWithIdentifier:@"ConfigureMCDControllerID"];
        [self presentViewController:controller animated:YES completion:nil];
    }
}

-(IBAction)goBackToClinicalRecordsAction:(id)sender
{
    [self dismissViewControllerAnimated:true completion:^{
        nil;
    }];
    
    //self.dismissViewControllerAnimated(true, completion: nil)
}
-(IBAction)Changelevel:(id)sender
{
    rightArrowBlackImg.transform = CGAffineTransformMakeRotation(6.14);
    rightArrowBlackImg1.transform = CGAffineTransformMakeRotation(6.14);
    rightArrowBlackImg2.transform = CGAffineTransformMakeRotation(7.85);
    rightArrowBlackImg3.transform = CGAffineTransformMakeRotation(6.14);
    rightArrowBlackImg4.transform = CGAffineTransformMakeRotation(6.14);
    rightArrowBlackImg5.transform = CGAffineTransformMakeRotation(6.14);
    popupflag=true;
    GlobalVars *global = [GlobalVars sharedInstance];
    if(global.BluetoothConnectionflag==2)
    {
    if(!global.dropdownValidFlag)
    [self presentAlertViewForPassword];
    else
         [self performSelector:@selector(popupalertview) withObject:self afterDelay:0.1 ];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                        message:@"Please Connect MCD Device"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
   
   
}
-(IBAction)TemperatureUnit:(id)sender
{
    rightArrowBlackImg.transform = CGAffineTransformMakeRotation(6.14);
    rightArrowBlackImg1.transform = CGAffineTransformMakeRotation(6.14);
    rightArrowBlackImg2.transform = CGAffineTransformMakeRotation(6.14);
    rightArrowBlackImg3.transform = CGAffineTransformMakeRotation(7.85);
    rightArrowBlackImg4.transform = CGAffineTransformMakeRotation(6.14);
    rightArrowBlackImg5.transform = CGAffineTransformMakeRotation(6.14);
     AHKActionSheet *actionSheet = [[AHKActionSheet alloc] initWithTitle:nil];
    popupflag=false;
    UIView *headerView = [[self class] fancyHeaderView:@"Temperature"];
    actionSheet.headerView = headerView;
    
    
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Celsius", nil)
                              image:[UIImage imageNamed:@"Celsius.png"]
                               type:AHKActionSheetButtonTypeDefault
                            handler:^(AHKActionSheet *as) {
                                NSLog(@"cels tapped");
                                temperaturelbl.text=@"Celsius";
                                NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
                                [TempDefault setObject:@"CELSIUS" forKey:@"TEMPERATURE"];
                                [TempDefault synchronize];
                                
                                WCFUpload *wcfcall=[[WCFUpload alloc]init];
                                [wcfcall UpdatePatientUnit];
                            }];
    
    
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Fahrenheit", nil)
                              image:[UIImage imageNamed:@"fahrenhiet.png"]
                               type:AHKActionSheetButtonTypeDefault
                            handler:^(AHKActionSheet *as) {
                                NSLog(@"fahe tapped");
                                temperaturelbl.text=@"Fahrenheit";
                                NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
                                [TempDefault setObject:@"FAHRENHEIT" forKey:@"TEMPERATURE"];
                                [TempDefault synchronize];
                                
                                WCFUpload *wcfcall=[[WCFUpload alloc]init];
                                [wcfcall UpdatePatientUnit];
                            }];
    
    [actionSheet show];
    
    
   

}


-(IBAction)WeightUnit:(id)sender
{
    rightArrowBlackImg.transform = CGAffineTransformMakeRotation(6.14);
    rightArrowBlackImg1.transform = CGAffineTransformMakeRotation(6.14);
    rightArrowBlackImg2.transform = CGAffineTransformMakeRotation(6.14);
    rightArrowBlackImg3.transform = CGAffineTransformMakeRotation(6.14);
    rightArrowBlackImg4.transform = CGAffineTransformMakeRotation(7.85);
    rightArrowBlackImg5.transform = CGAffineTransformMakeRotation(6.14);
    AHKActionSheet *actionSheet = [[AHKActionSheet alloc] initWithTitle:nil];
    popupflag=false;
    UIView *headerView = [[self class] fancyHeaderView:@"Weight"];
    actionSheet.headerView = headerView;
   
    
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Kilogram", nil)
                              image:[UIImage imageNamed:@"kg.png"]
                               type:AHKActionSheetButtonTypeDefault
                            handler:^(AHKActionSheet *as) {
                                NSLog(@"cels tapped");
                                Weightlbl.text=@"Kilogram";
                                NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
                                [TempDefault setObject:@"KILOGRAM" forKey:@"WEIGHT"];
                                [TempDefault synchronize];
                                rightArrowBlackImg4.transform = CGAffineTransformMakeRotation(6.14);
                                
                                WCFUpload *wcfcall=[[WCFUpload alloc]init];
                                [wcfcall UpdatePatientUnit];
                            }];
    
    
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Pounds", nil)
                              image:[UIImage imageNamed:@"lbs.png"]
                               type:AHKActionSheetButtonTypeDefault
                            handler:^(AHKActionSheet *as) {
                                NSLog(@"fahe tapped");
                                Weightlbl.text=@"Pounds";
                                NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
                                [TempDefault setObject:@"POUNDS" forKey:@"WEIGHT"];
                                [TempDefault synchronize];
                                 rightArrowBlackImg4.transform = CGAffineTransformMakeRotation(6.14);
                                WCFUpload *wcfcall=[[WCFUpload alloc]init];
                                [wcfcall UpdatePatientUnit];
                            }];
    
    [actionSheet show];
    
    
}


-(IBAction)HeightUnit:(id)sender
{
    rightArrowBlackImg.transform = CGAffineTransformMakeRotation(6.14);
    rightArrowBlackImg1.transform = CGAffineTransformMakeRotation(6.14);
    rightArrowBlackImg2.transform = CGAffineTransformMakeRotation(6.14);
    rightArrowBlackImg3.transform = CGAffineTransformMakeRotation(6.14);
    rightArrowBlackImg4.transform = CGAffineTransformMakeRotation(6.14);
    rightArrowBlackImg5.transform = CGAffineTransformMakeRotation(7.85);
    AHKActionSheet *actionSheet = [[AHKActionSheet alloc] initWithTitle:nil];
    popupflag=false;
    UIView *headerView = [[self class] fancyHeaderView:@"Height"];
    actionSheet.headerView = headerView;
    
    
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Feet and Inches", nil)
                              image:[UIImage imageNamed:@"fni.png"]
                               type:AHKActionSheetButtonTypeDefault
                            handler:^(AHKActionSheet *as) {
                                NSLog(@"cels tapped");
                                Heightlbl.text=@"Feet and Inches";
                                NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
                                [TempDefault setObject:@"FEET AND INCHES" forKey:@"HEIGHT"];
                                [TempDefault synchronize];
                                
                                WCFUpload *wcfcall=[[WCFUpload alloc]init];
                                [wcfcall UpdatePatientUnit];
                            }];
    
    
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Centimeter", nil)
                              image:[UIImage imageNamed:@"cm.png"]
                               type:AHKActionSheetButtonTypeDefault
                            handler:^(AHKActionSheet *as) {
                                NSLog(@"fahe tapped");
                                Heightlbl.text=@"Centimeter";
                                NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
                                [TempDefault setObject:@"CENTIMETER" forKey:@"HEIGHT"];
                                [TempDefault synchronize];
                                
                                WCFUpload *wcfcall=[[WCFUpload alloc]init];
                                [wcfcall UpdatePatientUnit];
                            }];
    
    [actionSheet show];
    
    
}

- (void)presentAlertViewForPassword
{
    
   if(!standalone_Mode)
   {
    SDCAlertView *alert = [[SDCAlertView alloc] initWithTitle:@""
                                                      message:@"Please enter user name and password"
                                                     delegate:self
                                            cancelButtonTitle:@"Cancel"
                                            otherButtonTitles:@"OK", nil];
    [alert setAlertViewStyle:SDCAlertViewStyleLoginAndPasswordInput];
       [alert setTitle:@"Login"];
        [[alert textFieldAtIndex:0] setPlaceholder:@"User name"];
       
    self.spinner = [[UIActivityIndicatorView alloc] init];
    self.spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    [self.spinner setTranslatesAutoresizingMaskIntoConstraints:NO];
    //[spinner startAnimating];
    [alert.contentView addSubview:spinner];
    [spinner sdc_horizontallyCenterInSuperviewWithOffset:-60.0];
    [spinner sdc_verticallyCenterInSuperviewWithOffset:SDCAutoLayoutStandardSiblingDistance];
    
    
    
    lblspin = [[UILabel alloc] init];
    [lblspin setFont:[UIFont systemFontOfSize:12]];
    [lblspin setTranslatesAutoresizingMaskIntoConstraints:NO];
    [alert.contentView addSubview:lblspin];
    [lblspin sdc_horizontallyCenterInSuperview];
    [lblspin sdc_verticallyCenterInSuperviewWithOffset:SDCAutoLayoutStandardSiblingDistance];
    
    
    [alert show];
   }
   else
   {
       DoctorProfileViewController *doctorprofile=[[DoctorProfileViewController alloc]init];
       BOOL checkDocDetail=[doctorprofile UpdatedoctorPatient_rec];
       NSString *mssg;
       if(checkDocDetail)
       {
           
           mssg =@"In order to change this setting, the complete doctor details & patient details must be entered under User Screen and Doctor Screen";
           SDCAlertView *alert = [[SDCAlertView alloc] initWithTitle:@"Validation"
                                                             message:mssg
                                                            delegate:self
                                                   cancelButtonTitle:nil
                                                   otherButtonTitles:@"OK", nil];
           [alert setAlertViewStyle:SDCAlertViewStyleDefault];
           [alert show];
           
       }
       else
       {
           mssg =@"These settings should only be changed by a qualified physician. By approving this message you approve that you are the attending physician";
           SDCAlertView *alert = [[SDCAlertView alloc] initWithTitle:@"Login"
                                                             message:mssg
                                                            delegate:self
                                                   cancelButtonTitle:@"Cancel"
                                                   otherButtonTitles:@"OK", nil];
           [alert setAlertViewStyle:SDCAlertViewStylePlainTextInput];
           
           self.spinner = [[UIActivityIndicatorView alloc] init];
           self.spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
           [self.spinner setTranslatesAutoresizingMaskIntoConstraints:NO];
           //[spinner startAnimating];
           [alert.contentView addSubview:spinner];
           [spinner sdc_horizontallyCenterInSuperviewWithOffset:-60.0];
           [spinner sdc_verticallyCenterInSuperviewWithOffset:SDCAutoLayoutStandardSiblingDistance];
           
           
           
           lblspin = [[UILabel alloc] init];
           [lblspin setTranslatesAutoresizingMaskIntoConstraints:NO];
           [alert.contentView addSubview:lblspin];
           [lblspin sdc_horizontallyCenterInSuperview];
           [lblspin sdc_verticallyCenterInSuperviewWithOffset:SDCAutoLayoutStandardSiblingDistance];
           
           
           [alert show];
       }
   }
    
    
    
}

- (BOOL)alertView:(SDCAlertView *)alertView shouldDismissWithButtonIndex:(NSInteger)buttonIndex {
     GlobalVars *global = [GlobalVars sharedInstance];
   if(!standalone_Mode)
   {
    lblspin.text=@"";
    if(buttonIndex==1)
    {
        [self.spinner startAnimating];
        UITextField *username = [alertView textFieldAtIndex:0];
        NSLog(@"username: %@", username.text);
        
        
        UITextField *password = [alertView textFieldAtIndex:1];
        NSLog(@"password: %@", password.text);
        
        
        if(username.text.length == 0 || password.text.length == 0) //check your two textflied has value
        {
            [spinner stopAnimating];
            direction = 1;
            shakes = 0;
            AudioServicesPlaySystemSound (1352);
            [self shake:alertView];
            NSLog(@"EMPTY");
            return NO;
        }
        else
        {
            
            lblspin.text=@"Validating...";
            NSString *domainname =[[NSUserDefaults standardUserDefaults] stringForKey:@"DomainName"];
            
             NSString * Fetchingpath=[NSString stringWithFormat:@"https://%@/MCC/MCCWcfService.svc/CargiverAuthentication/?Username=%@&Password=%@&patientId=%@",domainname,username.text,password.text,patientid];
            NSString *bookurl=[Fetchingpath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *URL1=[NSURL URLWithString:bookurl];
            
            NSLog(@"url%@",URL1);
            
            NSURLRequest *request = [NSURLRequest requestWithURL:URL1];
            
            
            
            AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
            
            operation.responseSerializer = [AFHTTPResponseSerializer serializer];
            
            
            [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                NSLog(@"urlresp%@",string);
                
                if([string containsString:@"User Login successfully"])
                {
                   
                    lblspin.text=@"OK";
                    [spinner stopAnimating];
                   
                    [self dismiss:alertView];
                    global.dropdownValidFlag=true;
                    [NSTimer scheduledTimerWithTimeInterval: 120.0 target: self
                                                                      selector: @selector(callAfterSixtySecond:) userInfo: nil repeats: YES];
                    NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
                    [userDefault setObject:username.text forKey:@"CareGiver"];
                    [userDefault synchronize];
                   
                }
                else
                {
                    AudioServicesPlaySystemSound (1352);
                     string = [string stringByReplacingOccurrencesOfString:@"\"" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [string length])];
                    if([string containsString:@"UserName or password is incorrect"])
                    {
                        lblspin.text= @"The user name or password is incorrect";
                    }
                    else if([string containsString:@"Invalid User"])
                    {
                        lblspin.text= @"Invalid user";
                    }
                    else
                        lblspin.text= string;
                    [spinner stopAnimating];
                    direction = 1;
                    shakes = 0;
                    [self shake:alertView];
                    [alertView textFieldAtIndex:1].text=@"";
                    [alertView textFieldAtIndex:0].text=@"";
                }
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error Retrieving doctors profile"
                                                                    message:[error localizedDescription]
                                                                   delegate:nil
                                                          cancelButtonTitle:@"Ok"
                                                          otherButtonTitles:nil];
                
                
                [alertView show];
                
                [spinner stopAnimating];
            }];
            
            
            [operation start];
            //[operation waitUntilFinished];
           
            return[[operation responseString]boolValue];
            //return retvalue;
            
            
        }
        
        
    }
    else
        return YES;
   }
    else
    {
        if(buttonIndex==0)
        {
            return YES;
        }
        lblspin.text=@"";
        
        if(buttonIndex==1)
        {
            [self.spinner startAnimating];
            UITextField *Docname = [alertView textFieldAtIndex:0];
            NSLog(@"Docname: %@", Docname.text);
            
            
            
            
            if(Docname.text.length == 0) //check your two textflied has value
            {
                [spinner stopAnimating];
                direction = 1;
                shakes = 0;
                AudioServicesPlaySystemSound (1352);
                [self shake:alertView];
                NSLog(@"EMPTY");
                
                return NO;
            }
            else
            {
                
                lblspin.text=@"Validating...";
                
                
                if([Docname.text isEqualToString:doctorname])
                {
                    lblspin.text=@"OK";
                    [spinner stopAnimating];
                    
                    [NSTimer scheduledTimerWithTimeInterval: 120.0 target: self
                                                   selector: @selector(callAfterSixtySecond:) userInfo: nil repeats: YES];
                   
                    [self dismiss:alertView];
                    
                }
                else
                {
                   
                    AudioServicesPlaySystemSound (1352);
                    lblspin.text=@"Invalid";
                    [spinner stopAnimating];
                    direction = 1;
                    shakes = 0;
                    [self shake:alertView];
                    [alertView textFieldAtIndex:0].text=@"";
                }
                
                
            }
            
            
        }
        else
            return YES;
        
    }
   
    return NO;
}

- (BOOL)alertView:(SDCAlertView *)alertView shouldDeselectButtonAtIndex:(NSInteger)buttonIndex {
    return YES;
}

-(void) callAfterSixtySecond:(NSTimer*) t
{
     GlobalVars *global = [GlobalVars sharedInstance];
    NSLog(@"red");
     global.dropdownValidFlag=false;
}

-(void)shake:(SDCAlertView *)theOneYouWannaShake
{
    [UIView animateWithDuration:0.03 animations:^
     {
         theOneYouWannaShake.transform = CGAffineTransformMakeTranslation(5*direction, 0);
     }
                     completion:^(BOOL finished)
     {
         if(shakes >= 10)
         {
             theOneYouWannaShake.transform = CGAffineTransformIdentity;
             return;
         }
         shakes++;
         direction = direction * -1;
         [self shake:theOneYouWannaShake];
     }];
}

-(void)fetchingpatientdata
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"PatientDetails" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
       // NSLog(@"%@", result);
        if (result.count > 0) {
            NSManagedObject *patient = (NSManagedObject *)[result objectAtIndex:0];
            //NSLog(@"1 - %@", patient);
            patientid=[patient valueForKey:@"patient_id"];
            deviceid=[patient valueForKey:@"device_id"];
            doctor_Country=[patient valueForKey:@"doc_country"];
            doctorname=[patient valueForKey:@"doc_name"];
        }
    }
}


-(void)popupalertview
{
    
    AHKActionSheet *actionSheet = [[AHKActionSheet alloc] initWithTitle:nil];
    if(popupflag)
    {
    UIView *headerView = [[self class] fancyHeaderView:@"Change Level"];
    actionSheet.headerView = headerView;

    if(!standalone_Mode)
    {
    if([changelevellbl.text isEqualToString:@"SAFE GUARD MODE"])
    {
        [actionSheet addButtonWithTitle:NSLocalizedString(@"SafeGuard Mode", nil)
                                  image:[UIImage imageNamed:@"safeguardmode.png"]
                                   type:AHKActionSheetButtonTypeDisabled
                                handler:^(AHKActionSheet *as) {
                                    
                                    [self updatepatientlevel:1];
                                    
                                    changelevellbl.text=@"SAFE GUARD MODE";
                                    NSLog(@"SafeGuard tapped");
                                }];
    }
    else
    {
    [actionSheet addButtonWithTitle:NSLocalizedString(@"SafeGuard Mode", nil)
                              image:[UIImage imageNamed:@"safeguardmode.png"]
                               type:AHKActionSheetButtonTypeDefault
                            handler:^(AHKActionSheet *as) {
                                
                                 [self updatepatientlevel:1];
                              
                                changelevellbl.text=@"SAFE GUARD MODE";
                                NSLog(@"SafeGuard tapped");
                            }];
    }
    
     if([changelevellbl.text isEqualToString:@"PREMIUM GUARD MODE"])
    {
        [actionSheet addButtonWithTitle:NSLocalizedString(@"Premium Guard Mode", nil)
                                  image:[UIImage imageNamed:@"premiumguardmode.png"]
                                   type:AHKActionSheetButtonTypeDisabled
                                handler:^(AHKActionSheet *as) {
                                    [self updatepatientlevel:2];
                                    changelevellbl.text=@"PREMIUM GUARD MODE";
                                    NSLog(@"Premium Guard tapped");
                                }];
    }
    else
    {
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Premium Guard Mode", nil)
                              image:[UIImage imageNamed:@"premiumguardmode.png"]
                               type:AHKActionSheetButtonTypeDefault
                            handler:^(AHKActionSheet *as) {
                                [self updatepatientlevel:2];
                                 changelevellbl.text=@"PREMIUM GUARD MODE";
                                NSLog(@"Premium Guard tapped");
                            }];
    }
    if([changelevellbl.text isEqualToString:@"AUTO PREMIUM SAFE GUARD MODE"])
     {
         [actionSheet addButtonWithTitle:NSLocalizedString(@"Auto Premium Safe Guard Mode", nil)
                                   image:[UIImage imageNamed:@"autopremiumsafeguardmode.png"]
                                    type:AHKActionSheetButtonTypeDisabled
                                 handler:^(AHKActionSheet *as) {
                                     [self updatepatientlevel:3];
                                     changelevellbl.text=@"AUTO PREMIUM SAFE GUARD MODE";
                                     NSLog(@"Auto Premium Guard tapped");
                                 }];
    }
    else
    {
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Auto Premium Safe Guard Mode", nil)
                              image:[UIImage imageNamed:@"autopremiumsafeguardmode.png"]
                               type:AHKActionSheetButtonTypeDefault
                            handler:^(AHKActionSheet *as) {
                                 [self updatepatientlevel:3];
                                 changelevellbl.text=@"AUTO PREMIUM SAFE GUARD MODE";
                                NSLog(@"Auto Premium Guard tapped");
                            }];
    }
    
    if([changelevellbl.text isEqualToString:@"PERSONAL ALERT GUARDIAN"])
    {
        [actionSheet addButtonWithTitle:NSLocalizedString(@"Personal Alert Guardian", nil)
                                  image:[UIImage imageNamed:@"personalalert-guardian.png"]
                                   type:AHKActionSheetButtonTypeDisabled
                                handler:^(AHKActionSheet *as) {
                                    [self updatepatientlevel:4];
                                    changelevellbl.text=@"PERSONAL ALERT GUARDIAN";
                                    NSLog(@"Personal Alert tapped");
                                }];
    }
    else
    {
    [actionSheet addButtonWithTitle:NSLocalizedString(@"Personal Alert Guardian", nil)
                              image:[UIImage imageNamed:@"personalalert-guardian.png"]
                               type:AHKActionSheetButtonTypeDefault
                            handler:^(AHKActionSheet *as) {
                                 [self updatepatientlevel:4];
                                 changelevellbl.text=@"PERSONAL ALERT GUARDIAN";
                                NSLog(@"Personal Alert tapped");
                            }];
    }
    [actionSheet show];
    }
     else
    {
        [actionSheet addButtonWithTitle:NSLocalizedString(@"SafeGuard Mode", nil)
                                  image:[UIImage imageNamed:@"safeguardmode@2x.png"]
                                   type:AHKActionSheetButtonTypeDefault
                                handler:^(AHKActionSheet *as) {
                                    [self updatepatientlevel:1];
                                     changelevellbl.text=@"SAFE GUARD MODE";
                                    NSLog(@"SafeGuard tapped");
                                }];
        
        if(![doctor_Country isEqualToString:@"US"])
        {
        [actionSheet addButtonWithTitle:NSLocalizedString(@"Personal Alert Guardian", nil)
                                  image:[UIImage imageNamed:@"personalalert-guardian.png"]
                                   type:AHKActionSheetButtonTypeDefault
                                handler:^(AHKActionSheet *as) {
                                    [self updatepatientlevel:4];
                                    changelevellbl.text=@"PERSONAL ALERT GUARDIAN";
                                    NSLog(@"Personal Alert tapped");
                                }];
        }

        [actionSheet show];
    }
    }
   
    
}

+ (UIView *)fancyHeaderView :(NSString *)text
{
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 60)];
    if([text isEqualToString:@"Change Level"])
    {
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"safeguardmode.png"]];
        imageView.frame = CGRectMake(10, 10, 40, 40);
        [headerView addSubview:imageView];
    }
    else if([text isEqualToString:@"Temperature"])
    {
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tempalert.png"]];
        imageView.frame = CGRectMake(10, 10, 40, 40);
        [headerView addSubview:imageView];
    }
    else if([text isEqualToString:@"Weight"])
    {
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"weight12.png"]];
        imageView.frame = CGRectMake(10, 10, 40, 40);
        [headerView addSubview:imageView];
    }
    else
    {
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"height.jpg"]];
        imageView.frame = CGRectMake(10, 10, 40, 40);
        [headerView addSubview:imageView];
    }
    
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(60, 20, 200, 20)];
    label1.text = text;
    label1.textColor = [UIColor blackColor];
    label1.font = [UIFont fontWithName:@"Avenir" size:17.0f];
    label1.backgroundColor = [UIColor clearColor];
    [headerView addSubview:label1];
    
    return  headerView;
}

-(void)dismiss:(SDCAlertView*)alert
{
    [alert dismissWithClickedButtonIndex:0 animated:YES];
    [self performSelector:@selector(popupalertview) withObject:self afterDelay:0.9 ];
}

-(void)updatepatientlevel :(int)level
{
    
    GlobalVars *globals = [GlobalVars sharedInstance];
    if([globals.mcd_bluetoothstatus isEqualToString:@"0"])
    {
       globals.ChangeLevel=level;
        
        ConfigureMCDController *config=[[ConfigureMCDController alloc]init];
        [config SetLevel:(int)[globals ChangeLevel]];
    }
}


-(void)UpdateLevel:(int)data
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    standalone_Mode = [defaults boolForKey:@"ActiveStandalonemode"];
    
    NSLogger *logger=[[NSLogger alloc]init];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
     GlobalVars *globals = [GlobalVars sharedInstance];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"PatientDetails" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSString *created_time;
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
        logger.degbugger = true;
        
        [logger log:@"Coredata Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:[error localizedDescription], @"General Setting Page", nil] error:TRUE];
        
    } else {
       // NSLog(@"%@", result);
        if (result.count > 0) {
            NSManagedObject *patient = (NSManagedObject *)[result objectAtIndex:0];
          //  NSLog(@"1 - %@", patient);
            patientid=[patient valueForKey:@"patient_id"];
            created_time=[patient valueForKey:@"active_since"];
            
        }
    }
    
    NSDate *date = [[NSDate alloc] init];
    NSLog(@"%@", date);
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *modifieddate = [dateFormatter1 stringFromDate:date];
    
    
    
    
    // Prepare an NSDateFormatter to convert to and from the string representation
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"dd MMM yy"];
    NSDate * dateNotFormatted = [dateFormatter dateFromString:created_time];
    [dateFormatter setDateFormat:@"MM/dd/YYYY"];
    NSString * createed_time = [dateFormatter stringFromDate:dateNotFormatted];
    
    NSLog(@"cre%@",created_time);
    NSLog(@"%@",dateNotFormatted);
    
    if(!standalone_Mode)
    {
        NSString *domainname =[[NSUserDefaults standardUserDefaults] stringForKey:@"DomainName"];
        
        //https://mcc-dev-01.personal-healthwatch.com/MCC/MCCWcfService.svc/changepatientlevel/?patientid=c95b8aec-8ea7-4ef4-b5e6-73c2dd445079&level=1&CreatedDateTime=08/07/2014&ModifiedDateTime=2015-08-07%2023:14:53.000
        
        NSString * Fetchingpath=[NSString stringWithFormat:@"https://%@/MCC/MCCWcfService.svc/changepatientlevel/?patientid=%@&level=%d&CreatedDateTime=%@&ModifiedDateTime=%@",domainname,patientid,(int)[globals ChangeLevel] ,createed_time,modifieddate];
        NSString *bookurl=[Fetchingpath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *URL1=[NSURL URLWithString:bookurl];
        
        NSLog(@"url%@",URL1);
        
        NSURLRequest *request = [NSURLRequest requestWithURL:URL1];
        
        
        
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        
        operation.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            
            NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            NSLog(@"urlresp%@",string);
            
            
            logger.degbugger = true;
            [logger log:@"NULL" properties:[NSDictionary dictionaryWithObjectsAndKeys:string, @"General Setting Page", nil] error:false];
            
            
            if([string containsString:@"true"])
            {
                NSLog(@"lvl%d",(int)[globals ChangeLevel] );
                
                NSString *activelevel;
                if([globals ChangeLevel]==1)
                {
                    activelevel=@"Level1";
                }
                else if([globals ChangeLevel]==2)
                {
                    activelevel=@"Level2";
                }
                else if([globals ChangeLevel]==3)
                {
                    activelevel=@"Level3";
                }
                else
                {
                    activelevel=@"Level4";
                }
                
                NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"PatientDetails"];
                PatientDetails *patientsettings=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
                [patientsettings setValue:activelevel forKey:@"level"];
                [patientsettings setValue:@"0" forKey:@"pat_updated_site"];
                [self.managedObjectContext save:nil];
                
            }
            else
            {
                
            }
            
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error Updating  level setting"
                                                                message:[error localizedDescription]
                                                               delegate:nil
                                                      cancelButtonTitle:@"Ok"
                                                      otherButtonTitles:nil];
            
            
            [alertView show];
            logger.degbugger = true;
            
            [logger log:@"Server Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:[error localizedDescription], @"Level changing", nil] error:TRUE];
            
        }];
        
        
        [operation start];
    }
    else
    {
        NSString *activelevel;
        if([globals ChangeLevel]==1)
        {
            activelevel=@"Level1";
        }
        else if([globals ChangeLevel]==2)
        {
            activelevel=@"Level2";
        }
        else if([globals ChangeLevel]==3)
        {
            activelevel=@"Level3";
        }
        else
        {
            activelevel=@"Level4";
        }
        
        NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"PatientDetails"];
        PatientDetails *patientsettings=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
        [patientsettings setValue:activelevel forKey:@"level"];
          [patientsettings setValue:@"0" forKey:@"pat_updated_site"];
        
        [self.managedObjectContext save:nil];
    }

}


- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}


@end
