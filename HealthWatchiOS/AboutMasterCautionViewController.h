//
//  AboutMasterCautionViewController.h
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 18/06/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import <MessageUI/MessageUI.h>

@interface AboutMasterCautionViewController : UIViewController<MFMailComposeViewControllerDelegate>
{
  
    int tagWas;
    UIView * viewToAnimate;
}


@property (nonatomic, strong) IBOutlet UIView *  headerBlueViewAboutUs;
@property (nonatomic, strong) IBOutlet UIView *  leftMenuButton;
@property (nonatomic, strong) IBOutlet UIView *  dashboardButton;

@property (nonatomic, strong) IBOutlet UIView *  leftBackArrow;

@property (nonatomic, strong) IBOutlet UIView *  aboutUsDetailsMainView;
@property (nonatomic, strong) IBOutlet UIView *  termsAndConditionsMainView;
@property (nonatomic, strong) IBOutlet UIView *  toursDetailsMainView;
@property (nonatomic, strong) IBOutlet UIView *  logDetailsMainView;
@property (nonatomic, strong) IBOutlet UILabel *  headerTitleLbl;

@property (nonatomic, strong) IBOutlet UILabel *  versionnumber;
@property (nonatomic, strong) IBOutlet UILabel *  versionnoheader;
@property (nonatomic, strong) IBOutlet UILabel *  buildnumber;
@property (nonatomic, strong) IBOutlet UILabel *  mcdversion;

@property (nonatomic, strong) IBOutlet UITextView * txtloginformat;
@property (nonatomic, strong) IBOutlet UIWebView * termsweb;

-(IBAction)goToDashboard:(id)sender;
-(IBAction)leftSideButtonAction:(id)sender;
-(IBAction)goToDetails:(id)sender;
-(IBAction)backToMainAboutUs:(id)sender;

-(void)makeTransitionForView:(UIView*)forView :(BOOL)hideViewBool;
@end
