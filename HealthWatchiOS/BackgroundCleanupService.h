//
//  WCFUpload.h
//  HealthWatch
//
//  Created by METSL MAC MINI on 18/12/15.
//  Copyright © 2015 METSL MAC MINI. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BackgroundCleanupService : NSObject

+ (BackgroundCleanupService *)getInstance;

- (void)run;

@end

