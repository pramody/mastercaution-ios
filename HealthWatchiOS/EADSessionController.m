
#import "EADSessionController.h"


#import "EventStorage.h"
#import "MCDSettings.h"
#import "DashboardViewController.h"
#import "BuiltInTestViewController.h"
#import "EventStruct.h"
#import "McdSettingsViewController.h"
#import "GeneralSettingsViewController.h"
#import "EventSuspensionSettingsViewController.h"
#import "EventThresholdsSettingsViewController.h"
#import "LiveEcgMonitor.h"
#import "WCFUpload.h"
#import <AudioToolbox/AudioToolbox.h>
#import " Draw the frame.h"
#import "SDCAlertView.h"
#import "SDCAlertViewController.h"
#import <UIView+SDCAutoLayout.h>
#import "SDCAlertController.h"

NSString *EADSessionDataReceivedNotification = @"EADSessionDataReceivedNotification";

@implementation EADSessionController
Byte _controlByte;
Byte no_of_Bytes;
bool flag;
Byte control_code;
int data_counter;
uint8_t mcd_msg[10000];
Byte sample;
bool plot_ok;
bool eventecg_ok;
bool eventVital_ok;
uint8_t plot_data[10000];
uint8_t EventECGData[5000];
uint8_t MCD_paramBuff[50];

int suspensionTimeThresholdCode = 0;
bool resphighthreshold = false;
bool FirstKeepAlive=false;
bool keepflag=false;
int statusUploadCounter = 0;
int EventFrameCount = 0;
int NackCounter = 0;
int ecgdatacounter=0;
bool ecgflag=false;




//enum CurrentSetting globals.currentSetting=SYNC;
//enum ComparisionType comparisiontype;




@synthesize accessory = _accessory;
@synthesize protocolString = _protocolString;
#define INPUT_BUFFER_SIZE 25000
uint8_t data2[INPUT_BUFFER_SIZE];

int readpnter1;
int writeptr1;
int difptr1;
#pragma mark Internal

// low level write method - write data to the accessory while there is space available and data to write
- (void)_writeData {
    while (([[_session outputStream] hasSpaceAvailable]) && ([_writeData length] > 0))
    {
        
        NSInteger bytesWritten = [[_session outputStream] write:[_writeData bytes] maxLength:[_writeData length]];
       if (bytesWritten == -1)
        {
            NSLog(@"write error");
            break;
        }
        else if (bytesWritten > 0)
        {
             [_writeData replaceBytesInRange:NSMakeRange(0, bytesWritten) withBytes:NULL length:0];
        }
    }
}

// low level read method - read data while there is data and space available in the input buffer
- (void)_readData {
#define EAD_INPUT_BUFFER_SIZE 4096
    
        uint8_t buf[EAD_INPUT_BUFFER_SIZE];
    
        
        NSInteger bytesRead = [[_session inputStream] read:buf maxLength:EAD_INPUT_BUFFER_SIZE];
        NSLog(@"read %ld bytes from input stream", (long)bytesRead);
       // Byte indet;
    
        if(bytesRead>0)
            for (int i = 0 ; i < bytesRead; i++)
            {
                
                // NSLog(@"data Byte chunk : %x", buf[i]);
              //  indet=buf[i];
                // ecgflag=false;
                
                if(writeptr1>=INPUT_BUFFER_SIZE)
                {
                    writeptr1=0;
                }
                
                [self MCDParser:buf[i]];
                    // note: all code below this line won't be executed, because the above method NEVER returns.
                
            }
    
   
   
    
    
   // NSLog(@"dif%d:write%d:read%d",difptr1,writeptr1,readpnter1);
    
}

- (void)_readData1 {
    
    //sir mthod
#define EAD_INPUT_BUFFER_SIZE 4096

    uint8_t buf[EAD_INPUT_BUFFER_SIZE];
    
    
    //while([[_session inputStream] hasBytesAvailable])
   // {
        NSInteger bytesRead = [[_session inputStream] read:buf maxLength:EAD_INPUT_BUFFER_SIZE];
        /*if (_readData == nil) {
            _readData = [[NSMutableData alloc] init];
        }
        [_readData appendBytes:(void *)buf length:bytesRead];*/
        NSLog(@"read %ld bytes from input stream", (long)bytesRead);
        
       /* if([self readBytesAvailable]>200)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:EADSessionDataReceivedNotification object:self userInfo:nil];
            
        }*/
       
     
         // sleep(1);
    
     //  NSData *data = buf;
    // if (data) {
    
    
     
     
     //uint8_t * BytePtr = (uint8_t  * )[data bytes];
     //NSInteger totalData = [data length] / sizeof(uint8_t);
     
     if(bytesRead>0)
     for (int i = 0 ; i < bytesRead; i++)
     {
        
    // NSLog(@"data Byte chunk : %x", buf[i]);
     data2[writeptr1]=buf[i];
          ecgflag=true;
     difptr1++;
     writeptr1++;
           ecgflag=false;
     if(writeptr1>=INPUT_BUFFER_SIZE)
     {
     writeptr1=0;
     }
       
     
     }
   
    [self thread2];
    

     NSLog(@"dif%d:write%d:read%d",difptr1,writeptr1,readpnter1);

}


-(void)thread2
{
   
    
    Byte indet;
    while(difptr1>0)
    {
       if(!ecgflag)
       {
        indet=data2[readpnter1];
        readpnter1++;
        difptr1--;
        if(readpnter1>=INPUT_BUFFER_SIZE)
        {
            readpnter1=0;
            
        }
        [self MCDParser:indet];
       }
        
    }
    
    
     //[NSThread sleepForTimeInterval:0.3] ;// sleep(1);
    
    
    
}


#pragma mark Public Methods

+ (EADSessionController *)sharedController
{
    static EADSessionController *sessionController = nil;
    if (sessionController == nil) {
        sessionController = [[EADSessionController alloc] init];
    }

    return sessionController;
}



- (void)dealloc
{
    [self closeSession];
    [self setupControllerForAccessory:nil withProtocolString:nil];

}

// initialize the accessory with the protocolString
- (void)setupControllerForAccessory:(EAAccessory *)accessory withProtocolString:(NSString *)protocolString
{
    _accessory = accessory;
    _protocolString = [protocolString copy];
}

// open a session with the accessory and set up the input and output stream on the default run loop
- (BOOL)openSession
{
   
    [_accessory setDelegate:self];
    _session = [[EASession alloc] initWithAccessory:_accessory forProtocol:_protocolString];
    if (!_session)
    {
        return false;
    }
        
    // here: start the looprk
    [[_session inputStream] setDelegate:self];
    // [[_session inputStream] scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    //[[_session inputStream] open];
    
    [[_session outputStream] setDelegate:self];
    //  [[_session outputStream] scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    // [[_session outputStream] open];
    [[_session inputStream] scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [[_session inputStream] open];
    
    [[_session outputStream] scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [[_session outputStream] open];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0);
    dispatch_async(queue, ^{
        
        [[NSRunLoop currentRunLoop] run];
        // note: all code below this line won't be executed, because the above method NEVER returns.
    });
   
   
        return true;
    
}



// close the session with the accessory.
- (void)closeSession
{
    [[_session inputStream] close];
    [[_session inputStream] removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [[_session inputStream] setDelegate:nil];
    [[_session outputStream] close];
    [[_session outputStream] removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [[_session outputStream] setDelegate:nil];

   // [_session release];
    _session = nil;

    //[_writeData release];
    _writeData = nil;
   // [_readData release];
    _readData = nil;
}

// high level write data method
- (void)writeData:(NSData *)data
{
    if (_writeData == nil) {
        _writeData = [[NSMutableData alloc] init];
    }

    [_writeData appendData:data];
    [self _writeData];
}

// high level read method 
- (NSData *)readData:(NSUInteger)bytesToRead
{
    NSData *data = nil;
    if ([_readData length] >= bytesToRead) {
        NSRange range = NSMakeRange(0, bytesToRead);
        data = [_readData subdataWithRange:range];
        [_readData replaceBytesInRange:range withBytes:NULL length:0];
    }
    //NSLog(@"%@",data);
    return data;
}

// get number of bytes read into local buffer
- (NSUInteger)readBytesAvailable
{
    return [_readData length];
}

#pragma mark EAAccessoryDelegate


- (void)accessoryDidDisconnect:(EAAccessory *)accessory
{
     [thread2timer invalidate];
    NSLog(@"accessory disconnect");
    FirstKeepAlive=false;
    GlobalVars *global = [GlobalVars sharedInstance];
     global.BluetoothConnectionflag=1;
    global.mcd_bluetoothstatus=@"1";
    //[global.plotdata removeAllObjects];
    global.mcd_garmentstatus=@"1";
    
    global.mcd_BatteryStatus=0;
    global.mcd_HeartRate=0;
    global.mcd_Temperature=0;
    global.mcd_RespirationRate=0;
    
    
    global.built_accelerometer = @"0";
    global.built_electrodes = @"0";
    global.built_battery =@"0";
    global.built_bluetooth = @"0";
    global.built_temperature = @"0";
    global.built_sdcard = @"0";
    
    for (int j = 0; j < 13; j++) {
       
        [global.Leaddata  insertObject:@"1" atIndex:j];
        
    }
    
    DashboardViewController *dsah=[[DashboardViewController alloc]init];
    [dsah updatetemp];
    
    BuiltInTestViewController *built=[[BuiltInTestViewController alloc]init];
    [built leaddataupdate];
    
    LiveEcgMonitor *lvcg=[[LiveEcgMonitor alloc]init];
    [lvcg VitalParam];
    [lvcg AccDisonncet];
    
    BOOL standalone_Mode;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    standalone_Mode = [defaults boolForKey:@"ActiveStandalonemode"];
    
    if(!standalone_Mode)
    {
        AppDelegate * appDelegate = (AppDelegate *)UIApplication.sharedApplication.delegate;
        [appDelegate disconnect];
    }
  
   

    // do something ...
}

#pragma mark NSStreamDelegateEventExtensions

// asynchronous NSStream handleEvent method
- (void)stream:(NSStream *)aStream handleEvent:(NSStreamEvent)eventCode
{
    switch (eventCode) {
        case NSStreamEventNone:
            break;
        case NSStreamEventOpenCompleted:
        {
      // thread2timer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(thread2) userInfo:nil repeats:YES];
            
           // [[NSRunLoop mainRunLoop] addTimer:thread2timer forMode:NSRunLoopCommonModes];
            
        }
            break;
        case NSStreamEventHasBytesAvailable:
        {
             [self _readData];
          
        
           
          /*
            dispatch_queue_t myBackgroundQ = dispatch_queue_create("com.healthwatch.backgroundDelay", NULL);
            // Could also get a global queue; in this case, don't release it below.
            dispatch_time_t delay = dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC);
            dispatch_after(delay, myBackgroundQ, ^(void){
                [self _readData];
            });*/
          
           
        }
            break;
        case NSStreamEventHasSpaceAvailable:
        {
              [self _writeData];
            
           
        }
            break;
        case NSStreamEventErrorOccurred:
            NSLog(@"EVENT ERROr");
            break;
        case NSStreamEventEndEncountered:
        {
             NSLog(@"EVENT END");
            [thread2timer invalidate];
        }
            break;
        default:
            break;
    }
}



-(void)MCDParser :(unsigned char) in_data
{
    
    GlobalVars *globals = [GlobalVars sharedInstance];
    // parse the data here
    if(in_data > 240)
    {
        control_code = in_data;
        flag = 1;
        no_of_Bytes = 0;
    }
    else
    {
        switch(control_code)
        {
            case COMMUNICATION_CONTROL_BYTE:
                if(flag)
                {
                    sample = in_data + 2; // have to consider 0xF1 , no of Bytes in the msg string for chksum
                    flag = 0;
                    data_counter = 0;
                    mcd_msg[data_counter]= 0xF1;
                    data_counter++;
                    mcd_msg[data_counter]= in_data;
                    data_counter++;
                }
                else
                {
                    // collect the message
                    mcd_msg[data_counter]= in_data;
                    ++data_counter;
                    if(data_counter >= sample)
                    {
                        // send msg for processing
                        if(mcd_msg[data_counter -1] == [self getCheckSum:mcd_msg :data_counter - 1])
                            [self Process_F1_Msg:mcd_msg :data_counter];
                        else
                            
                            [self sendNACK:COMMUNICATION_CONTROL_BYTE : mcd_msg[2] :0];
                        //sendNACK(COMMUNICATION_CONTROL_BYTE, mcd_msg[2], 0 );
                    }
                }
                
                break;
                
            case SETTING_CONTROL_BYTE:
                if(flag)
                {
                    sample = in_data + 2;
                    flag = 0;
                    data_counter = 0;
                    mcd_msg[data_counter]= 0xF2;
                    data_counter++;
                    mcd_msg[data_counter]= in_data;
                    data_counter++;
                }
                else
                {
                    // collect the message
                    mcd_msg[data_counter]= in_data;
                    ++data_counter;
                    if(data_counter >= sample)
                    {
                        // send msg for processing
                        if(mcd_msg[data_counter -1] == [self getCheckSum:mcd_msg :data_counter - 1])
                            [self Process_F2_Msg:mcd_msg :data_counter];
                        
                        else
                            [self sendNACK:SETTING_CONTROL_BYTE : mcd_msg[2] :0];
                        // sendNACK(SETTING_CONTROL_BYTE, mcd_msg[2], 0 );
                    }
                }
                break;
                
            case LIVE_DATA_CONTROL_BYTE:// live control Byte
                // [self sendEcgACK];
            {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                
               BOOL ecgchecksum=[defaults boolForKey:@"ECGCheckSum"];
               
                if(flag)
                {
                    sample = in_data;
                    flag = 0;
                    data_counter = 0;
                    
                    if(plot_ok)
                    {
                        plot_ok = 0;
                        if(ecgchecksum)
                        {
                        if(mcd_msg[data_counter -1] == [self getCheckSum:mcd_msg :data_counter - 1])
                            [self MCD_PutData:plot_data];
                        }
                        else
                        {
                              [self MCD_PutData:plot_data];
                        }
                      
                    }
                }
                else
                {
                    plot_data[data_counter] = in_data;
                    ++data_counter;
                    
                    if(data_counter >= sample)
                    {
                        plot_ok = 1;
                        data_counter = 0;
                    }
                }
            }
                break;
                
            case EVENT_DATA_CONTROL_BYTE: // event data
            {
                
                if(flag)
                {
                    sample = in_data+2;
                    flag = 0;
                    data_counter = 0;
                    
                    EventECGData[data_counter]= 0xF1;
                    data_counter++;
                    EventECGData[data_counter]= in_data;
                    data_counter++;
                    
                    if(eventecg_ok)
                    {
                        eventecg_ok=0;
                        
                        
                       
                    }
                    // #### ##   handle event Data here
                    
                    
                }
                else
                {
                    if(data_counter<99)
                    {
                    EventECGData[data_counter]=in_data;
                    }
                    ++data_counter;
                    
                    
                    if(data_counter >= sample)
                    {
                        /* for(int k=0;k<data_counter;k++)
                         {
                         NSNumber *number = [NSNumber numberWithInteger:EventECGData[k]];
                         [plotdataa addObject:number];
                         }*/
                        
                        [self onEventECGDataRecieved:EventECGData :data_counter];
                        data_counter = 0;
                        eventecg_ok=1;
                        
                        
                    }
                }
                
                /* EventFrameCount++;
                 if (EventFrameCount >= 100) {
                 EventFrameCount = 0;
                 [self onEventECGDataRecieved1:plotdataa];
                 [plotdataa removeAllObjects];
                 }*/
            }
                break;
                
            case PARAMETER_CONTROL_BYTE: // vital parameters
                if(flag)
                {
                    sample = in_data;
                    flag = 0;
                    data_counter = 0;
                    
                    // #### ##   handle event Data here
                    if(eventVital_ok)
                    {
                        eventVital_ok=0;
                    }
                }
                else
                {
                    
                    globals.mcd_DataBuf = 0;
                    globals.mcd_ERAM = 0;
                    globals.mcd_ASIC = 0;
                    globals.mcd_SDCARD_Presence = 0;
                    globals.mcd_SDCARD_ERR = 0;
                    
                    
                    MCD_paramBuff[data_counter] = in_data;
                    ++data_counter;
                    if(data_counter >= sample)
                    {
                        data_counter = 0;
                        eventVital_ok=1;
                        globals.mcd_HeartRate =(MCD_paramBuff[0] * 128) +( MCD_paramBuff[1]);
                        globals.mcd_RespirationRate = MCD_paramBuff[2];
                        globals.mcd_Temperature = MCD_paramBuff[3] + (float)(MCD_paramBuff[4] /100);
                        globals.mcd_Posture = MCD_paramBuff[5];
                        globals.mcd_MCD_LastEvent = MCD_paramBuff[6];
                       // globals.mcd_leads_off = MCD_paramBuff[7] * 256 + MCD_paramBuff[8];
                        globals.mcd_QRS = MCD_paramBuff[11] << 7 | MCD_paramBuff[12];
                        globals.mcd_QT  = MCD_paramBuff[13] * 128 + MCD_paramBuff[14];
                        globals.mcd_QTc = MCD_paramBuff[15] * 128 + MCD_paramBuff[16];
                        globals.mcd_PR  = MCD_paramBuff[17] * 128 + MCD_paramBuff[18];
                        globals.mcd_RR  = MCD_paramBuff[19] * 128 + MCD_paramBuff[20];
                        
                        
                        //NSLog(@"posture%d", globals.mcd_Posture);
                        NSLog(@"vital parameter received");
                        //NSLog(@"LeadOff%hhu",MCD_paramBuff[7]);
                        
                        //Lead status
                        Byte  LeadStatus1 = MCD_paramBuff[7];
                        Byte  LeadStatus2 = MCD_paramBuff[8];
                        
                        
                        NSLog(@"%hhu", LeadStatus1);
                        NSLog(@"%hhu",LeadStatus2);
                        
                       
                       // NSLog(@"%@", [self convertHexToBinary:[NSString stringWithFormat:@"%i", LeadStatus1]]);
                        
                        
                        
                       // NSLog(@"%@", [self convertHexToBinary:[NSString stringWithFormat:@"%i", LeadStatus2]]);
                        
                        NSMutableArray * leaddata=[[NSMutableArray alloc]init];
                        // int statuscounter = 0;
                        for (Byte i = 0; i < 7; i++) {
                            Byte result = 0;
                            if (i < 7) {
                                if ([self BitStatus:LeadStatus1 :i]  > 0) {
                                    result = 1;
                                }
                            }
                            //  NSString *lead=[NSString stringWithFormat:@"%@",[NSNumber numberWithUnsignedChar: result]];
                            // NSInteger row=[[NSNumber numberWithUnsignedChar:i]intValue];
                            
                            [leaddata insertObject:[NSString stringWithFormat:@"%@",[NSNumber numberWithUnsignedChar: result]] atIndex:[[NSNumber numberWithUnsignedChar:i]intValue]];
                            
                            
                            
                            
                            
                            // if 1 its proper else problem
                            
                        }
                        int i = 7;
                        for (Byte j = 0; j < 6; j++) {
                            Byte result = 0;
                            if([self BitStatus:LeadStatus2 :j] > 0) {
                                result = 1;
                            }
                            [leaddata insertObject:[NSString stringWithFormat:@"%@",[NSNumber numberWithUnsignedChar: result]] atIndex:[[NSNumber numberWithUnsignedChar:(i+j)]intValue]];
                            
                            
                            
                        }
                        
                        if([self BitStatus:(Byte)MCD_paramBuff[10] :(Byte)0] > 0) {
                            globals.mcd_DataBuf = 1;
                        }
                        if ([self BitStatus:(Byte)MCD_paramBuff[10] :(Byte)1] > 0) {
                            globals.mcd_ERAM = 1;
                        }
                        if ([self BitStatus:(Byte)MCD_paramBuff[10] :(Byte)2] > 0) {
                            globals.mcd_ASIC = 1;
                        }
                        if ([self BitStatus:(Byte)MCD_paramBuff[10] :(Byte)3] > 0) {
                            globals.mcd_SDCARD_Presence = 1;
                        }
                        if ([self BitStatus:(Byte)MCD_paramBuff[10] :(Byte)4] > 0) {
                            globals.mcd_SDCARD_ERR = 1;
                        }
                        
                        DashboardViewController *dsah=[[DashboardViewController alloc]init];
                        [dsah updatetemp];
                        
                        BuiltInTestViewController *built=[[BuiltInTestViewController alloc]init];
                        [built leaddataupdate];
                        globals.Leaddata=leaddata;
                        
                        LiveEcgMonitor *lvcg=[LiveEcgMonitor alloc];
                        [lvcg VitalParam];
                        
                        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                        BOOL standalone_Mode = [defaults boolForKey:@"ActiveStandalonemode"];
                        
                        statusUploadCounter++;
                        if (statusUploadCounter >= 15)
                        {
                            statusUploadCounter = 0;
                            
                            if(!standalone_Mode)
                            {
                                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^ {
                                    
                                    WCFUpload *wcfcall=[[WCFUpload alloc]init];
                                    [wcfcall sendEcgelectrodeStatus:[leaddata[0]integerValue] :[leaddata[1] integerValue] :[leaddata[2] integerValue] :[leaddata[3] integerValue] :[leaddata[4] integerValue] :[leaddata[5] integerValue] :[leaddata[6]integerValue] :[leaddata[7]integerValue] :[leaddata[8]integerValue] :[leaddata[9] integerValue]];
                                    
                                    [wcfcall SendPatientlivestatus];
                                });
                           
                            }
                          
                        }
                        // FireHREvent(m_HeartRate,m_bedNo); // check limit
                        // if(m_MCD_LastEvent > -1 && show_aaryth == 0)
                        //show_aaryth = m_MCD_LastEvent;
                    }
                }
                break;
                
                
            default:
                break;
        }
    }
}



-(Byte)BitStatus :(Byte)number  :(Byte)mask
{
    return  (number&(1 << mask));
}


-(void) sendEcgACK
{
    unsigned char msg[5] = { 0xF1,  0x03, 0x04, 14 };
    msg[4] =[self getCheckSum:msg :4];
    NSData * dataarr = [NSData dataWithBytes:msg length: 5];
    NSLog(@"ACK:%@",dataarr);
   [self writeData:dataarr];
    
}

-(void) sendACK:(unsigned char) control_Byte  :(unsigned char) msg_no
{
    unsigned char msg[5] = { 0xF1,  0x03, 0x04, msg_no };
    msg[4] =[self getCheckSum:msg :4];
    NSData * dataarr = [NSData dataWithBytes:msg length: 5];
    NSLog(@"ACK:%@",dataarr);
   [self writeData:dataarr];
    
}

-(void) sendNACK:(unsigned char) control_Byte :(unsigned char) msg_no  :(unsigned char) reason
{
    // control Byte, no of Bytes = 4, msg type = 5, nack to which msg_no, chksum
    unsigned char msg[6] = { 0xF1, 0x04, 0x05, msg_no, reason};
    
    msg[5] = [self getCheckSum:msg :5];
    NSData * dataarr = [NSData dataWithBytes:msg length: 6];
    NSLog(@"NACK:%@",dataarr);
   [self writeData:dataarr];
}

-(unsigned char)getCheckSum :(unsigned char *)u8_fn_InputArrayPtr :(short) u16_fn_InputLen
{
    short u16_fn_LoopCounter;
    long u32_fn_Sum = 0;
    unsigned char u8_fn_CheckSum = 0;
    
    for(u16_fn_LoopCounter=0; u16_fn_LoopCounter < u16_fn_InputLen; u16_fn_LoopCounter++)
    {
        if(u8_fn_InputArrayPtr[u16_fn_LoopCounter] > 127)
        {
        }
        else
        {
            u32_fn_Sum = u32_fn_Sum + u8_fn_InputArrayPtr[u16_fn_LoopCounter];
        }
    }
    
    u8_fn_CheckSum = u32_fn_Sum % 128;
    return u8_fn_CheckSum;
}

-(void)Process_F1_Msg :(unsigned char *)msg   :(short) msg_len
{
    GlobalVars *globals = [ GlobalVars sharedInstance];
    //NSLog(@"msg2:%x",msg[2]);
    switch (msg[2])
    {
        case COMMUNICATION_FRAME_ACK:
            [self onACK:msg[3]];
            break;
            
        case COMMUNICATION_FRAME_NACK:
            // nack recevied hence last command not answered may have to resend.
            [self onNACK:msg[3]]; // not response on this command hence exit function
            break;
            
        case COMMUNICATION_FRAME_EVENT_FILE_AVAILABLE_TO_TX://10
            // send ack
            [ self sendACK:msg[0] :msg[2] ];
            break;
            
        case COMMUNICATION_FRAME_BUDGE_INFO:
            // read the budge info here
        {
            [ self sendACK:msg[0] :msg[2] ];
            
            [self onEventBudgeRecieved:&msg[3]];
        }
            break;
            
        case COMMUNICATION_FRAME_STORED_EVENT_INFO:
        {
            [ self sendACK:msg[0] :msg[2] ];
            [self onEventHeaderRecieved:&msg[3]];
            
        }
            break;
            
        case COMMUNICATION_FRAME_EVENT_TX_STOP:
        {
            [ self sendACK:msg[0] :msg[2] ];
            
            /* if (EventFrameCount > 0) {
             EventFrameCount = 0;
             [self onEventECGDataRecieved1:plotdataa];
             [plotdataa removeAllObjects];
             // EventECGData.clear();
             //memset(EventECGData, 0, sizeof(EventECGData));
             }*/
            if(ecgdatacounter<5&&ecgdatacounter!=0)
            {
                ecgdatacounter=0;
                NSFileManager *fileManager = [NSFileManager defaultManager];
                if (![fileManager fileExistsAtPath:universalfilepath]) {
                    
                    // the file doesn't exist,we can write out the text using the  NSString convenience method
                    
                    
                    NSError *error = noErr;
                    BOOL success =  [resultantdata writeToFile:universalfilepath options:NSDataWritingAtomic error:&error];
                    
                    if (!success) {
                        // handle the error
                        NSLog(@"%@", error);
                    }
                    
                } else {
                    
                    // the file already exists, append the text to the end
                    
                    // get a handle
                    fileHandle = [NSFileHandle fileHandleForWritingAtPath:universalfilepath];
                    
                    // move to the end of the file
                    [fileHandle seekToEndOfFile];
                    
                    // convert the string to an NSData object
                    
                    
                    // write the data to the end of the file
                    [fileHandle writeData:resultantdata];
                    
                    // clean up
                    
                    
                    
                }
            }
           [self onEventComplete];
            
        }
            break;
            
        case COMMUNICATION_FRAME_KEEP_ALIVE:
        {
            NSLog(@"Got KeePlive");
            [ self sendACK:msg[0] :msg[2]];
            NSLog(@" SETKeePlive");
            if(globals.configconnectFlag)
            {
                keepflag=true;
                [self sendMacAddress];
                
            }
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
           
            if([globals.mcdmacaddress isEqualToString: [defaults valueForKey:@"MacAddress"]])
            {
             if(!FirstKeepAlive)
             {
                 FirstKeepAlive=true;
                 
                if([[defaults valueForKey:@"MCDSyncflag"] isEqualToString:@"1"])
                {
                 if(keepflag)
                 {
                     
            [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(getdviceid) userInfo:nil repeats:NO];
                     keepflag=false;
                 }
                 else
                     [self getdviceid];
               // globals.MCDSyncflag=1;
                LiveEcgMonitor *lvcg=[[LiveEcgMonitor alloc]init];
                [lvcg AccConnect];
                }
                 else
                 {
                     LiveEcgMonitor *lvcg=[[LiveEcgMonitor alloc]init];
                     [lvcg AccConnect];
                     
                    [self SET_DateTime];
                 }
                 
             }
                
            }
            
        }
            break;
            
        case COMMUNICATION_FRAME_BATTERY_STATUS:
        {
            [ self sendACK:msg[0] :msg[2] ];
            
            globals.mcd_BatteryStatus=msg[3];
        }
            break;
            
        case COMMUNICATION_FRAME_GET_DEVICE_SETTINGS:
        {
            [ self sendACK:msg[0] :msg[2] ];
            [self onMCDSettingsRecieved:&msg[3]];
        }
            break;
            
        case COMMUNICATION_FRAME_INVALID_POSTURE_VAL:
            break;
            
        case COMMUNICATION_FRAME_INVALID_DATA_VAL:
            break;
            
        case COMMUNICATION_FRAME_INVALID_SUSP_TIME:
            break;
            
        case COMMUNICATION_FRAME_INVALID_SUSP_PERCNTG:
            break;
            
        case COMMUNICATION_FRAME_SETTING_OVER:
        {
            [ self sendACK:msg[0] :msg[2] ];
            [self onFinishedGettingSettings];
        }
            break;
        case  COMMUNICATION_FRAME_LEAD_SETTINGS:
        {
            NSLog(@"msg2:%x",msg[3]);
            
            [self onLeadSettingRecieved:&msg[3]];
        }
           break;
        default:
            
            return; // not understood hence either send ack or ignore
            break;
    }
    
    
    
    
}


-(void)Process_F2_Msg :(unsigned char*) msg  :(short) msg_len
{
    
    
    [ self sendACK:msg[0] :msg[3] ];
    switch (msg[2])
    {
            
        case SETTING_FRAME_GET_DEVICE_ID:
        {
            
            
            NSString * deviceid =[NSString stringWithFormat:@" %c%c%c%c%c%c%c%c%c%c%c%c%c%c",(char)msg[3],(char)msg[4],(char)msg[5],(char)msg[6],(char)msg[7],(char)msg[8], (char)msg[9],(char)msg[10],(char)msg[11],(char)msg[12],(char)msg[13],(char)msg[14],(char) msg[15],(char) msg[16]];
            [self onDeviceIDRecieved:deviceid];
        }
            break;
        case SETTING_FRAME_GET_FIRMWARE_DETAILS:
        {
            
            
            NSString * firmware =[NSString stringWithFormat:@" %@.%@.%@.%@.%@ ",[NSString stringWithFormat:@"%d",[[NSNumber numberWithUnsignedChar:msg[3]] intValue ]] ,[NSString stringWithFormat:@"%d",[[NSNumber numberWithUnsignedChar:msg[4]] intValue ]],[NSString stringWithFormat:@"%d",[[NSNumber numberWithUnsignedChar:msg[5]] intValue ]],[NSString stringWithFormat:@"%c", [[NSNumber numberWithUnsignedChar:msg[6]] charValue ]],[NSString stringWithFormat:@"%c",[[NSNumber numberWithUnsignedChar:msg[7]] charValue ]]];
            
            
            NSCalendar *calendar = [NSCalendar currentCalendar];
            [calendar setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
            NSDateComponents *components1 = [[NSDateComponents alloc] init];
            
            [components1 setDay: msg[8]] ;
            [components1 setMonth: msg[9]];
            [components1 setYear:((msg[10]) + 2000)];
            [components1 setHour:msg[11]];
            [components1 setMinute: msg[12]];
            [components1 setSecond: msg[13]];
            
            NSDate *getTime = [calendar dateFromComponents:components1];
            NSLog(@"timeee%@",getTime);
            CommonHelper *commhelp=[[CommonHelper alloc]init];
            
            NSString *SET_DATE_TIME = [commhelp getDateUTC:@"yyyy-MM-dd" :@"dd MMM, yyyy HH:mm:ss" :DateAsNSDate :@"2014-04-25" :getTime];
            
            
            [self onFirmwareDetails:firmware :SET_DATE_TIME];
        }
            break;
        case SETTING_FRAME_GET_LAST_UPDATE_DATE_TIME:
            break;
        case SETTING_FRAME_GET_BUILT_IN_TEST_REPORT:
            [self onBuiltInTestRecieved:msg[3]];
            break;
        case SETTING_FRAME_GET_ST_ELEVATION_VAL:
            [self onSTELevationThresholdRecieved:&msg[3]];
            break;
        case SETTING_FRAME_GET_ST_DEPRESSION_VAL:
            [self onSTDepressionThresholdRecieved:&msg[3]];
            break;
        case SETTING_FRAME_GET_T_WAVE_VAL:
            [self onTWaveThresholdRecieved:&msg[3]];
            break;
        case SETTING_FRAME_GET_BIGEMENY_VAL:
            [self onBigeminyThresholdRecieved:&msg[3]];
            break;
        case SETTING_FRAME_GET_PROLONGED_QT_VAL:
            [self onProlongedQTThresholdRecieved:&msg[3]];
            break;
        case SETTING_FRAME_GET_QRS_WIDTH_WIDE_VAL:
            [self onWideQRSThresholdRecieved:&msg[3]];
            break;
        case SETTING_FRAME_GET_VTACH_NARROW_QRS_VAL:
            [self onSTachyThresholdRecieved:&msg[3]];
            break;
        case SETTING_FRAME_GET_VTACH_WIDE_QRS_VAL:
            [self onVTachyThresholdRecieved:&msg[3]];
            break;
        case SETTING_FRAME_GET_PVC_VAL:
            [self onPVCThresholdRecieved:&msg[3]];
            break;
        case SETTING_FRAME_GET_ASYSTOLE_VAL:
            [self onAsystoleThresholdRecieved:&msg[3]];
            break;
        case SETTING_FRAME_GET_BRADICARDIA_VAL:
            [self onBradycardiaThresholdRecieved:&msg[3]];
            break;
        case SETTING_FRAME_GET_APNEA_VAL:
            [self onApneaThresholdRecieved:&msg[3]];
            break;
        case SETTING_FRAME_GET_TEMPRATURE_VAL:
            [self onTempratureThresholdRecieved:&msg[3]];
            break;
        case SETTING_FRAME_GET_FALL_EVENT_VAL:
            [self onFallEventThresholdRecieved:&msg[3]];
            break;
        case SETTING_FRAME_GET_NO_MOTION_VAL:
            [self onNoMotionThresholdRecieved:&msg[3]];
            break;
        case SETTING_FRAME_GET_RESPIRATION_RATE_VAL:
            [self onRespirationThresholdRecieved:&msg[3]];
            break;
        case SETTING_FRAME_GET_QRS_WIDTH_NARROW_VAL:
            [self onNarrowQRSThresholdRecieved:&msg[3]];
            break;
        case SETTING_FRAME_GET_PAUSE_VAL:
            [self onPauseThresholdRecieved:&msg[3]];
            break;
        case SETTING_FRAME_GET_TRIGEMENY_VAL:
            [self onTrigeminyThresholdRecieved:&msg[3]];
            break;
        case SETTING_FRAME_GET_COUPLET_VAL:
            [self onCoupletThresholdRecieved:&msg[3]];
            break;
        case SETTING_FRAME_GET_TRIPLET_VAL:
            [self onTripletThresholdRecieved:&msg[3]];
            break;
        case SETTING_FRAME_GET_SUSPENSION_PERCNTG:
            [self onSuspensionPercentageRecieved:&msg[3]];
            break;
        case SETTING_FRAME_GET_SUSPENSION_VAL:
            [self onSuspensionSettingsRecieved:&msg[3]];
            break;
        case SETTING_FRAME_GET_CURRENT_LEVEL:
            break;
        case SETTING_FRAME_GET_RR_INTERVAL_VAL:
            break;
        case SETTING_FRAME_GET_TACHYCARDIA_VAL:
            [self onTachycardiaRecieved:&msg[3]];
            break;
            
            
        default:
            [ self sendACK:msg[0] :msg[0] ];
            break;
    }
}



-(void)onACK:(int) data {
    GlobalVars *globals = [ GlobalVars sharedInstance];
    /*[self ackStatus = ACKStatus.ACK;
     
     try {
     Thread.sleep(100);
     } catch (InterruptedException e) {
     // TODO Auto-generated catch block
     e.printStackTrace();
     }*/
    
    NackCounter = 0;
    switch (data)
    {
            
        case COMMUNICATION_FRAME_ON_DEMAND_ECG ://9
        {
            break;
        }
        case SETTING_FRAME_SET_DATE_TIME://104
        {
            NSLog(@"date set");
             NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            if([[defaults valueForKey:@"MCDSyncflag"] isEqualToString:@"2"])
            {
                NSLog(@"date set after syncing");
            }
            else
            {
            [self SET_MobDetectionTimeInterval:10];
                
            NSLog(@"current%d",globals.currentSetting);
            if (globals.currentSetting == SYNC) {
                globals.currentSetting = MCDSETTING;
                NSLog(@"current%d",globals.currentSetting);
            }
            }
            
            break;
        }
        case SETTING_FRAME_SET_REMOTE_ADDRESS://106
        {
            globals.configconnectFlag=false;
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setValue:globals.mcdmacaddress forKey:@"MacAddress"];
            [defaults synchronize];
            break;
        }
            
        case SETTING_FRAME_SET_KEEP_ALIVE_TIME://110
        {
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
            
            NSEntityDescription *entity = [NSEntityDescription entityForName:@"PatientDetails" inManagedObjectContext:self.managedObjectContext];
            [fetchRequest setEntity:entity];
            
            NSError *error = nil;
            NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
            
            if (error) {
              //  NSLog(@"Unable to execute fetch request.");
             //   NSLog(@"%@, %@", error, error.localizedDescription);
                
            } else {
                
                if (result.count > 0) {
                    NSManagedObject *patient = (NSManagedObject *)[result objectAtIndex:0];
                    
                    globals.currentSetting = MCDSETTING;
                    
                    if([[patient valueForKey:@"level"] isEqualToString:@"Level1"])
                        [self SetLevel:1];
                    else if([[patient valueForKey:@"level"] isEqualToString:@"Level2"])
                        [self SetLevel:2];
                    else if([[patient valueForKey:@"level"] isEqualToString:@"Level3"])
                        [self SetLevel:3];
                    else if([[patient valueForKey:@"level"] isEqualToString:@"Level4"])
                        [self SetLevel:4];
                    
                    
                }
            }
            break;
            
        }
        case COMMUNICATION_FRAME_SET_DEVICE_SETTINGS://22
            if (globals.currentSetting == ARRYTHMIA) {
                posturecode=StandingSittingPostureCode;
                [self sendSupraVentricularTachycardiaThreshold];
            } else {
                EventThresholdsSettingsViewController *Thresholdsett=[[EventThresholdsSettingsViewController alloc]init];
                [Thresholdsett UpdateThresholdvalue:data];
            }
            
            break;
            
        case SETTING_FRAME_SET_ST_ELEVATION_VAL://154
        {
            if(globals.currentSetting==ISCHMIA)
            {
                switch (posturecode) {
                        
                    case StandingSittingPostureCode:
                    {
                        posturecode=LayingPostureCode;
                        [self sendSTElevationThreshold];
                    }
                        break;
                    case LayingPostureCode:
                    {
                        posturecode=WalkingPostureCode;
                        [self sendSTElevationThreshold];
                    }
                        break;
                    case WalkingPostureCode:
                    {
                        posturecode=RunningPostureCode;
                        [self sendSTElevationThreshold];
                    }
                        break;
                    case RunningPostureCode:
                    {
                        posturecode=StandingSittingPostureCode;
                        [self sendSTDepressionThreshold];
                    }
                        break;
                        
                    default:
                        break;
                }
            }else {
                EventThresholdsSettingsViewController *Thresholdsett=[[EventThresholdsSettingsViewController alloc]init];
                [Thresholdsett UpdateThresholdvalue:data];
            }
            break;
        }
        case SETTING_FRAME_SET_DEPRESSION_VAL://155
        {
            if(globals.currentSetting==ISCHMIA)
            {
                switch (posturecode) {
                        
                    case StandingSittingPostureCode:
                    {
                        posturecode=LayingPostureCode;
                        [self sendSTDepressionThreshold];
                    }
                        break;
                    case LayingPostureCode:
                    {
                        posturecode=WalkingPostureCode;
                        [self sendSTDepressionThreshold];
                    }
                        break;
                    case WalkingPostureCode:
                    {
                        posturecode=RunningPostureCode;
                        [self sendSTDepressionThreshold];
                    }
                        break;
                    case RunningPostureCode:
                    {
                        posturecode=StandingSittingPostureCode;
                        [self sendSTSuspensionThreshold];
                    }
                        break;
                        
                    default:
                        break;
                }
            }else {
                EventThresholdsSettingsViewController *Thresholdsett=[[EventThresholdsSettingsViewController alloc]init];
                [Thresholdsett UpdateThresholdvalue:data];
            }
            break;
        }
        case SETTING_FRAME_SET_T_WAVE_VAL:
        {
            
        }
        case SETTING_FRAME_SET_BIGEMENY_VAL://157
        {
            if(globals.currentSetting==ARRYTHMIA)
            {
                switch (posturecode) {
                        
                    case StandingSittingPostureCode:
                    {
                        posturecode=LayingPostureCode;
                        [self sendBigeminyThreshold];
                    }
                        break;
                    case LayingPostureCode:
                    {
                        posturecode=WalkingPostureCode;
                        [self sendBigeminyThreshold];
                    }
                        break;
                    case WalkingPostureCode:
                    {
                        posturecode=RunningPostureCode;
                        [self sendBigeminyThreshold];
                    }
                        break;
                    case RunningPostureCode:
                    {
                        posturecode=StandingSittingPostureCode;
                        [self sendTrigeminyThreshold];
                    }
                        break;
                        
                    default:
                        break;
                }
            }else {
                EventThresholdsSettingsViewController *Thresholdsett=[[EventThresholdsSettingsViewController alloc]init];
                [Thresholdsett UpdateThresholdvalue:data];
            }
            break;
        }
        case SETTING_FRAME_SET_PROLONGED_QT_VAL://158
        {
            if(globals.currentSetting==ISCHMIA)
            {
                switch (posturecode) {
                        
                    case StandingSittingPostureCode:
                    {
                        posturecode=LayingPostureCode;
                        [self sendProlongedqtThreshold];
                    }
                        break;
                    case LayingPostureCode:
                    {
                        posturecode=WalkingPostureCode;
                        [self sendProlongedqtThreshold];
                    }
                        break;
                    case WalkingPostureCode:
                    {
                        posturecode=RunningPostureCode;
                        [self sendProlongedqtThreshold];
                    }
                        break;
                    case RunningPostureCode:
                    {
                        posturecode=StandingSittingPostureCode;
                        [self sendQRSNarrowThreshold];
                    }
                        break;
                        
                    default:
                        break;
                }
            }else {
                EventThresholdsSettingsViewController *Thresholdsett=[[EventThresholdsSettingsViewController alloc]init];
                [Thresholdsett UpdateThresholdvalue:data];
            }
            break;
            
            
        }
        case SETTING_FRAME_SET_QRS_WIDTH_WIDE_VAL://159
        {
            if(globals.currentSetting==ISCHMIA)
            {
                switch (posturecode) {
                        
                    case StandingSittingPostureCode:
                    {
                        posturecode=LayingPostureCode;
                        [self sendQRSWideThreshold];
                    }
                        break;
                    case LayingPostureCode:
                    {
                        posturecode=WalkingPostureCode;
                        [self sendQRSWideThreshold];
                    }
                        break;
                    case WalkingPostureCode:
                    {
                        posturecode=RunningPostureCode;
                        [self sendQRSWideThreshold];
                    }
                        break;
                    case RunningPostureCode:
                    {
                        posturecode=StandingSittingPostureCode;
                        globals.currentSetting = OTHER;
                        [self sendtempratureThreshold];
                    }
                        break;
                        
                    default:
                        break;
                }
            }else {
                EventThresholdsSettingsViewController *Thresholdsett=[[EventThresholdsSettingsViewController alloc]init];
                [Thresholdsett UpdateThresholdvalue:data];
            }
            break;
        }
            
        case SETTING_FRAME_SET_VTACH_NARROW_QRS_VAL://160
        {
            if ([globals currentSetting] == ARRYTHMIA) {
                
                switch (posturecode) {
                        
                    case StandingSittingPostureCode:
                    {
                        posturecode=LayingPostureCode;
                        [self sendSupraVentricularTachycardiaThreshold];
                    }
                        break;
                    case LayingPostureCode:
                    {
                        posturecode=WalkingPostureCode;
                        [self sendSupraVentricularTachycardiaThreshold];
                    }
                        break;
                    case WalkingPostureCode:
                    {
                        posturecode=RunningPostureCode;
                        [self sendSupraVentricularTachycardiaThreshold];
                    }
                        break;
                    case RunningPostureCode:
                    {
                        posturecode=StandingSittingPostureCode;
                         [self sendBradycardiaThreshold];
                        //[self sendVentricularTachycardiaThreshold];
                    }
                        break;
                        
                        
                    default:
                        break;
                }
                
                
            } else {
                
                EventThresholdsSettingsViewController *Thresholdsett=[[EventThresholdsSettingsViewController alloc]init];
                [Thresholdsett UpdateThresholdvalue:data];
                
            }
            break;
        }
        case SETTING_FRAME_SET_VTACH_WIDE_QRS_VAL://161
        {
            if ([globals currentSetting] == ARRYTHMIA) {
                switch (posturecode) {
                        
                    case StandingSittingPostureCode:
                    {
                        posturecode=LayingPostureCode;
                        [self sendVentricularTachycardiaThreshold];
                    }
                        break;
                    case LayingPostureCode:
                    {
                        posturecode=WalkingPostureCode;
                        [self sendVentricularTachycardiaThreshold];
                    }
                        break;
                    case WalkingPostureCode:
                    {
                        posturecode=RunningPostureCode;
                        [self sendVentricularTachycardiaThreshold];
                    }
                        break;
                    case RunningPostureCode:
                    {
                        posturecode=StandingSittingPostureCode;
                        [self sendBradycardiaThreshold];
                    }
                        
                        break;
                    default:
                        break;
                }
            }
            else
            {
                EventThresholdsSettingsViewController *Thresholdsett=[[EventThresholdsSettingsViewController alloc]init];
                [Thresholdsett UpdateThresholdvalue:data];
            }
            break;
            
        }
            
        case SETTING_FRAME_SET_PVC_VAL://162
        {
            if ([globals currentSetting] == ARRYTHMIA)
            {
                switch (posturecode) {
                        
                    case StandingSittingPostureCode:
                    {
                        posturecode=LayingPostureCode;
                        [self sendPVCThreshold];
                    }
                        break;
                    case LayingPostureCode:
                    {
                        posturecode=WalkingPostureCode;
                        [self sendPVCThreshold];
                    }
                        break;
                    case WalkingPostureCode:
                    {
                        posturecode=RunningPostureCode;
                        [self sendPVCThreshold];
                    }
                        break;
                    case RunningPostureCode:
                    {
                        posturecode=StandingSittingPostureCode;
                        [self sendAsystoleThreshold];
                    }
                        break;
                        
                        break;
                    default:
                        break;
                }
            } else {
                EventThresholdsSettingsViewController *Thresholdsett=[[EventThresholdsSettingsViewController alloc]init];
                [Thresholdsett UpdateThresholdvalue:data];
                
            }
            break;
            
            
        }
        case SETTING_FRAME_SET_ASYSTOLE_VAL://163
        {
            if([globals currentSetting]==ARRYTHMIA)
            {
                switch (posturecode) {
                        
                    case StandingSittingPostureCode:
                    {
                        posturecode=LayingPostureCode;
                        [self sendAsystoleThreshold];
                    }
                        break;
                    case LayingPostureCode:
                    {
                        posturecode=WalkingPostureCode;
                        [self sendAsystoleThreshold];
                    }
                        break;
                    case WalkingPostureCode:
                    {
                        posturecode=RunningPostureCode;
                        [self sendAsystoleThreshold];
                    }
                        break;
                    case RunningPostureCode:
                    {
                        posturecode=StandingSittingPostureCode;
                        [self sendBigeminyThreshold];
                    }
                        break;
                        
                        
                    default:
                        break;
                }
            }else {
                EventThresholdsSettingsViewController *Thresholdsett=[[EventThresholdsSettingsViewController alloc]init];
                [Thresholdsett UpdateThresholdvalue:data];
            }
            break;
            
        }
            
        case SETTING_FRAME_SET_BRADICARDIA_VAL://164
        {
            if ([globals currentSetting] == ARRYTHMIA) {
                switch (posturecode) {
                        
                    case StandingSittingPostureCode:
                    {
                        posturecode=LayingPostureCode;
                        [self sendBradycardiaThreshold];
                    }
                        break;
                    case LayingPostureCode:
                    {
                        posturecode=WalkingPostureCode;
                        [self sendBradycardiaThreshold];
                    }
                        break;
                    case WalkingPostureCode:
                    {
                        posturecode=RunningPostureCode;
                        [self sendBradycardiaThreshold];
                    }
                        break;
                    case RunningPostureCode:
                    {
                        posturecode=StandingSittingPostureCode;
                        [self sendHRSuspensionpercThreshold];
                    }
                        break;
                        
                    default:
                        break;
                }
            }
            else
            {
                EventThresholdsSettingsViewController *Thresholdsett=[[EventThresholdsSettingsViewController alloc]init];
                [Thresholdsett UpdateThresholdvalue:data];
            }
            break;
            
        }
            
        case  SETTING_FRAME_SET_APNEA_VAL  ://165
        {
            if ([globals currentSetting] == OTHER) {
                
                globals.currentSetting =SUSPENSION;
                [self sendNoMotionThreshold];
                
            } else {
                EventThresholdsSettingsViewController *Thresholdsett=[[EventThresholdsSettingsViewController alloc]init];
                [Thresholdsett UpdateThresholdvalue:data];
            }
            break;
            
        }
        case SETTING_FRAME_SET_TEMPERATURE_VAL://166
        {
            
            if ([globals currentSetting] == OTHER) {
                [self sendRespirationLowThreshold];
                
            } else {
                EventThresholdsSettingsViewController *Thresholdsett=[[EventThresholdsSettingsViewController alloc]init];
                [Thresholdsett UpdateThresholdvalue:data];
            }
            break;
        }
        case SETTING_FRAME_SET_FALL_EVENT_FLAG:
        {
            
        }
        case SETTING_FRAME_SET_NO_MOTION_VAL://168
        {
            
            if ([globals currentSetting] ==SUSPENSION) {
                [self sendSuspensionTimeThreshold];
                
            } else {
                EventThresholdsSettingsViewController *Thresholdsett=[[EventThresholdsSettingsViewController alloc]init];
                [Thresholdsett UpdateThresholdvalue:data];
            }
            break;
        }
        case SETTING_FRAME_SET_RESPIRATION_RATE_VAL://169
        {
            
            if (globals.currentSetting == OTHER) {
                if (resphighthreshold) {
                    
                    [self sendRespirationHighThreshold];
                } else
                    [self sendApneaThreshold];
            } else {
                
                EventThresholdsSettingsViewController *Thresholdsett=[[EventThresholdsSettingsViewController alloc]init];
                [Thresholdsett UpdateThresholdvalue:data];
            }
        }
        case SETTING_FRAME_SET_QRS_WIDTH_NARROW_VAL://170
        {
            if([globals currentSetting]==ISCHMIA)
            {
                switch (posturecode) {
                        
                    case StandingSittingPostureCode:
                    {
                        posturecode=LayingPostureCode;
                        [self sendQRSNarrowThreshold];
                    }
                        break;
                    case LayingPostureCode:
                    {
                        posturecode=WalkingPostureCode;
                        [self sendQRSNarrowThreshold];
                    }
                        break;
                    case WalkingPostureCode:
                    {
                        posturecode=RunningPostureCode;
                        [self sendQRSNarrowThreshold];
                    }
                        break;
                    case RunningPostureCode:
                    {
                        posturecode=StandingSittingPostureCode;
                        [self sendQRSWideThreshold];
                    }
                        break;
                        
                    default:
                        break;
                }
            }else {
                EventThresholdsSettingsViewController *Thresholdsett=[[EventThresholdsSettingsViewController alloc]init];
                [Thresholdsett UpdateThresholdvalue:data];
            }
            break;
            
        }
        case SETTING_FRAME_SET_PAUSE_VAL://171
        {
            if([globals currentSetting]==ARRYTHMIA)
            {
                switch (posturecode) {
                        
                    case StandingSittingPostureCode:
                    {
                        posturecode=LayingPostureCode;
                        [self sendPauseThreshold];
                    }
                        break;
                    case LayingPostureCode:
                    {
                        posturecode=WalkingPostureCode;
                        [self sendPauseThreshold];
                    }
                        break;
                    case WalkingPostureCode:
                    {
                        posturecode=RunningPostureCode;
                        [self sendPauseThreshold];
                    }
                        break;
                    case RunningPostureCode:
                    {
                        posturecode=StandingSittingPostureCode;
                        [self sendPVCThreshold];
                    }
                        break;
                        
                    default:
                        break;
                }
            } else {
                
                EventThresholdsSettingsViewController *Thresholdsett=[[EventThresholdsSettingsViewController alloc]init];
                [Thresholdsett UpdateThresholdvalue:data];
            }
            break;
            
        }
        case SETTING_FRAME_SET_TRIGEMENY_VAL://172
        {
            if([globals currentSetting]==ARRYTHMIA)
            {
                switch (posturecode) {
                        
                    case StandingSittingPostureCode:
                    {
                        posturecode=LayingPostureCode;
                        [self sendTrigeminyThreshold];
                    }
                        break;
                    case LayingPostureCode:
                    {
                        posturecode=WalkingPostureCode;
                        [self sendTrigeminyThreshold];
                    }
                        break;
                    case WalkingPostureCode:
                    {
                        posturecode=RunningPostureCode;
                        [self sendTrigeminyThreshold];
                    }
                        break;
                    case RunningPostureCode:
                    {
                        posturecode=StandingSittingPostureCode;
                        [self sendCoupletThreshold];
                    }
                        break;
                        
                    default:
                        break;
                }
            } else {
                EventThresholdsSettingsViewController *Thresholdsett=[[EventThresholdsSettingsViewController alloc]init];
                [Thresholdsett UpdateThresholdvalue:data];
            }
            break;
            
        }
        case SETTING_FRAME_SET_COUPLET_VAL://173
        {
            if([globals currentSetting]==ARRYTHMIA)
            {
                switch (posturecode) {
                        
                    case StandingSittingPostureCode:
                    {
                        posturecode=LayingPostureCode;
                        [self sendCoupletThreshold];
                    }
                        break;
                    case LayingPostureCode:
                    {
                        posturecode=WalkingPostureCode;
                        [self sendCoupletThreshold];
                    }
                        break;
                    case WalkingPostureCode:
                    {
                        posturecode=RunningPostureCode;
                        [self sendCoupletThreshold];
                    }
                        break;
                    case RunningPostureCode:
                    {
                        posturecode=StandingSittingPostureCode;
                        [self sendTripletThreshold];
                    }
                        break;
                        
                    default:
                        break;
                }
            }else {
                EventThresholdsSettingsViewController *Thresholdsett=[[EventThresholdsSettingsViewController alloc]init];
                [Thresholdsett UpdateThresholdvalue:data];
            }
            break;
            
        }
        case SETTING_FRAME_SET_TRIPLET_VAL://174
        {
            if([globals currentSetting]==ARRYTHMIA)
            {
                switch (posturecode) {
                        
                    case StandingSittingPostureCode:
                    {
                        posturecode=LayingPostureCode;
                        [self sendTripletThreshold];
                    }
                        break;
                    case LayingPostureCode:
                    {
                        posturecode=WalkingPostureCode;
                        [self sendTripletThreshold];
                    }
                        break;
                    case WalkingPostureCode:
                    {
                        posturecode=RunningPostureCode;
                        [self sendTripletThreshold];
                    }
                        break;
                    case RunningPostureCode:
                    {
                        globals.currentSetting=ISCHMIA;
                        posturecode=StandingSittingPostureCode;
                        [self sendSTElevationThreshold];
                    }
                        break;
                        
                    default:
                        break;
                }
            }else {
                EventThresholdsSettingsViewController *Thresholdsett=[[EventThresholdsSettingsViewController alloc]init];
                [Thresholdsett UpdateThresholdvalue:data];
            }
            break;
        }
        case SETTING_FRAME_SET_SUSPENSION_PERCNTG://175
        {
            if ([globals currentSetting] == ARRYTHMIA) {
                switch (posturecode) {
                        
                    case StandingSittingPostureCode:
                    {
                        posturecode=LayingPostureCode;
                        [self sendHRSuspensionpercThreshold];
                    }
                        break;
                    case LayingPostureCode:
                    {
                        posturecode=WalkingPostureCode;
                        [self sendHRSuspensionpercThreshold];
                    }
                        break;
                    case WalkingPostureCode:
                    {
                        posturecode=RunningPostureCode;
                        [self sendHRSuspensionpercThreshold];
                    }
                        break;
                    case RunningPostureCode:
                    {
                        posturecode=StandingSittingPostureCode;
                        [self sendPauseThreshold];
                    }
                        break;
                        
                    default:
                        break;
                }
                
            } else if ([globals currentSetting] == ISCHMIA) {
                switch (posturecode) {
                        
                    case StandingSittingPostureCode:
                    {
                        posturecode=LayingPostureCode;
                        [self sendSTSuspensionThreshold];
                    }
                        break;
                    case LayingPostureCode:
                    {
                        posturecode=WalkingPostureCode;
                        [self sendSTSuspensionThreshold];
                    }
                        break;
                    case WalkingPostureCode:
                    {
                        posturecode=RunningPostureCode;
                        [self sendSTSuspensionThreshold];
                    }
                        break;
                    case RunningPostureCode:
                    {
                        posturecode=StandingSittingPostureCode;
                        [self sendProlongedqtThreshold];
                    }
                        break;
                        
                    default:
                        break;
                }
                
            }
            else{
                EventThresholdsSettingsViewController *Thresholdsett=[[EventThresholdsSettingsViewController alloc]init];
                [Thresholdsett UpdateThresholdvalue:data];
            }
            break;
        }
        case SETTING_FRAME_SET_SUSPENSION_TIME://176
        {
            if (globals.currentSetting == SUSPENSION) {
                [self sendSuspensionTimeThreshold];
                
            } else {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                BOOL Inverted_T_Mode = [defaults boolForKey:@"InvertedTFlag"];
                if(Inverted_T_Mode)
                {
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    [defaults setBool:NO forKey:@"InvertedTFlag"];
                    [defaults synchronize];
                    McdSettingsViewController *mcdsett=[[McdSettingsViewController alloc]init];
                    [mcdsett UpdateMcdSetting:001];
                    break;
                    
                    
                }
                else
                {
                    NSLog(@"suspense%hhd",globals.suspenseflag);
                    if(!globals.suspenseflag)
                    {
                    EventSuspensionSettingsViewController *Suspensionsett=[[EventSuspensionSettingsViewController alloc]init];
                    [Suspensionsett UpdateSuspensionSett:data];
                    }
                    else
                    {
                        globals.suspenseflag=false;
                    }
                }
            }
           
            break;
        }
        case SETTING_FRAME_SET_LEVEL://177
        {
            
            if ([globals currentSetting] == MCDSETTING) {
                [self sendMCDSettings];
                globals.currentSetting =ARRYTHMIA;
            }
            else {
                GeneralSettingsViewController *Generalsett=[[GeneralSettingsViewController alloc]init];
                [Generalsett UpdateLevel:data];
            }
            break;
        }
        case SETTING_FRAME_SET_RR_INTERVAL_VAL://178
        {
            break;
        }
        case SETTING_FRAME_SET_TACHYCARDIA_VAL://179
        {
            break;
        }
        case SETTING_FRAME_SET_GAIN ://                                                 100
        {
            McdSettingsViewController *mcdsett=[[McdSettingsViewController alloc]init];
            [mcdsett UpdateMcdSetting:data];
            break;
        }
        case SETTING_FRAME_SET_50_Hz ://                                                101
        {
            McdSettingsViewController *mcdsett=[[McdSettingsViewController alloc]init];
            [mcdsett UpdateMcdSetting:data];
            break;
        }
        case SETTING_FRAME_SET_BASELINE://                                              102
        {
            McdSettingsViewController *mcdsett=[[McdSettingsViewController alloc]init];
            [mcdsett UpdateMcdSetting:data];
            break;
        }
        case SETTING_FRAME_SET_VOLUME ://                                               103
        {
            McdSettingsViewController *mcdsett=[[McdSettingsViewController alloc]init];
            [mcdsett UpdateMcdSetting:data];
            break;
        }
        case SETTING_FRAME_SET_PRE_POST_TIME://                                         105
        {
            McdSettingsViewController *mcdsett=[[McdSettingsViewController alloc]init];
            [mcdsett UpdateMcdSetting:data];
            break;
        }
            
        case SETTING_FRAME_SET_EVENT_DETECTION_LEAD_VAL://                              107
        {
            McdSettingsViewController *mcdsett=[[McdSettingsViewController alloc]init];
            [mcdsett UpdateMcdSetting:data];
            break;
        }
        case SETTING_FRAME_SET_SAMPLE_RATE://                                           108
        {
            McdSettingsViewController *mcdsett=[[McdSettingsViewController alloc]init];
            [mcdsett UpdateMcdSetting:data];
            break;
            
        }
        case SETTING_FRAME_SET_LEAD_SELECTION_FOR_TX://                                 109
        {
            McdSettingsViewController *mcdsett=[[McdSettingsViewController alloc]init];
            [mcdsett UpdateMcdSetting:data];
            break;
            
        }
        case SETTING_FRAME_SET_RESPIRATION_ENABLE://                                    111
        {
            McdSettingsViewController *mcdsett=[[McdSettingsViewController alloc]init];
            [mcdsett UpdateMcdSetting:data];
            
            break;
        }
        case SETTING_FRAME_SET_MCD_ORIENTATION: //                                      112
        {
            McdSettingsViewController *mcdsett=[[McdSettingsViewController alloc]init];
            [mcdsett UpdateMcdSetting:data];
            break;
        }
        default:
            break;
            
            
    }
}


-(void)getdviceid
{
    
      GlobalVars *globals = [ GlobalVars sharedInstance];
    [self Get_Device_ID];
    globals.currentSetting=SYNC;
    NSLog(@"Remote address success");
}


-(void)onNACK :(int) data
{
    GlobalVars *globals = [ GlobalVars sharedInstance];
    if (NackCounter < 5)
    {
        NackCounter++;
        switch (data) {
            case 50:
                [self Get_Device_ID];
                break;
            case 104:
                [self SET_DateTime];
                break;
            case 51:
                [self Get_Firmware_Details];
                break;
            case 110:
                [self SET_MobDetectionTimeInterval:(4)];
                break;
            case 1:
                [self StartContinousECG];
                
                //  MasterCaution.log.info("NACK recieved for Live ECG");
                
                break;
            case 2:
                [self StopContinousECG];
                
                //MasterCaution.log.info("NACK recieved for Stopping Live ECG");
                
                break;
                
            case 177:
            {
                NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
                
                NSEntityDescription *entity = [NSEntityDescription entityForName:@"PatientDetails" inManagedObjectContext:self.managedObjectContext];
                [fetchRequest setEntity:entity];
                
                NSError *error = nil;
                NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
                
                if (error) {
                    NSLog(@"Unable to execute fetch request.");
                    NSLog(@"%@, %@", error, error.localizedDescription);
                    
                } else {
                    
                    if (result.count > 0) {
                        NSManagedObject *patient = (NSManagedObject *)[result objectAtIndex:0];
                        
                        globals.currentSetting = MCDSETTING;
                        
                        if([[patient valueForKey:@"level"] isEqualToString:@"Level1"])
                            [self SetLevel:1];
                        else if([[patient valueForKey:@"level"] isEqualToString:@"Level2"])
                            [self SetLevel:2];
                        else if([[patient valueForKey:@"level"] isEqualToString:@"Level3"])
                            [self SetLevel:3];
                        else if([[patient valueForKey:@"level"] isEqualToString:@"Level4"])
                            [self SetLevel:4];
                        
                        
                    }
                }
                
                //MasterCaution.log.info("NACK recieved for LEVEL : "
                // + String.valueOf(Tabactivity.Level));
            }
                break;
            case 16:
                [self Get_MCDSettings];
                
                // MasterCaution.log.info("NACK recieved for MCD Settings");
                
                break;
                
            case 22:
                if (globals.currentSetting ==ARRYTHMIA)
                    [self sendMCDSettings];
                else
                    //sendBroascast(data);
                    break;
                
            case 154:
                if (globals.currentSetting ==ISCHMIA)
                    [self sendSTElevationThreshold];
                else
                    //sendBroascast(data);
                    break;
                
            case 155:
                if (globals.currentSetting ==ISCHMIA)
                    [ self sendSTElevationThreshold];
                else
                    //sendBroascast(data);
                    break;
            case 157:
                if (globals.currentSetting ==ARRYTHMIA)
                    [self sendBigeminyThreshold];
                else
                    //sendBroascast(data);
                    break;
            case 158:
                if (globals.currentSetting ==ISCHMIA)
                    [self sendProlongedqtThreshold];
                else
                    //sendBroascast(data);
                    break;
            case 159:
                if (globals.currentSetting ==ISCHMIA)
                    [self sendQRSWideThreshold];
                else
                    //sendBroascast(data);
                    break;
            case 160:
                if (globals.currentSetting ==ARRYTHMIA)
                    [self sendSupraVentricularTachycardiaThreshold];
                break;
            case 161:
                if (globals.currentSetting ==ARRYTHMIA)
                    [self sendVentricularTachycardiaThreshold];
                else
                    //sendBroascast(data);
                    break;
            case 162:
                if (globals.currentSetting ==ARRYTHMIA)
                    [self sendPVCThreshold];
                else
                    //sendBroascast(data);
                    break;
            case 163:
                if (globals.currentSetting ==ARRYTHMIA)
                    [self sendAsystoleThreshold];
                else
                    //sendBroascast(data);
                    break;
            case 164:
                if (globals.currentSetting ==ARRYTHMIA)
                    [self sendBradycardiaThreshold];
                else
                    //sendBroascast(data);
                    break;
            case 171:
                if (globals.currentSetting ==ARRYTHMIA)
                    [self sendPauseThreshold];
                else
                    //sendBroascast(data);
                    break;
            case 172:
                if (globals.currentSetting ==ARRYTHMIA)
                    [self sendTrigeminyThreshold];
                else
                    //sendBroascast(data);
                    break;
            case 173:
                if (globals.currentSetting ==ARRYTHMIA)
                    [self sendCoupletThreshold];
                else
                    //sendBroascast(data);
                    break;
            case 174:
                if (globals.currentSetting ==ARRYTHMIA)
                    [self sendTripletThreshold];
                else
                    //sendBroascast(data);
                    break;
            case 175:
                if (globals.currentSetting ==ARRYTHMIA) {
                    [self sendHRSuspensionpercThreshold];
                } else if (globals.currentSetting ==ISCHMIA) {
                    [self sendSTSuspensionThreshold];
                } else
                    //sendBroascast(data);
                    break;
                
            case 165:
                if (globals.currentSetting ==OTHER) {
                    [self sendApneaThreshold];
                } else
                    //sendBroascast(data);
                    
                    break;
            case 169:
                if (globals.currentSetting ==OTHER) {
                    if (resphighthreshold) {
                        [self sendRespirationHighThreshold];
                    } else {
                        
                        [self sendRespirationLowThreshold];
                    }
                } else
                    //sendBroascast(data);
                    break;
                
            case 166:
                if (globals.currentSetting ==OTHER) {
                    [self sendtempratureThreshold ];
                } else
                    //sendBroascast(data);
                    case 168:
                    if (globals.currentSetting ==OTHER) {
                        [self sendNoMotionThreshold];
                    } else
                        //sendBroascast(data);
                        break;
            case 176:
                if (globals.currentSetting ==SUSPENSION) {
                    [self sendSuspensionTimeThreshold ];
                    
                } else {
                    //sendBroascast(data);
                }
                
                break;
                
            default:
                /*
                 Intent intent = new Intent(ACTION_NACK_RECIEVED);
                 intent.putExtra("ackcode", data);
                 
                 sendBroadcast(intent);
                 MasterCaution.log.info("NACK recieved for "
                 + String.valueOf(data));*/
                
                break;
        }
    }
    else {
        NackCounter = 0;
    }
}



-(IBAction)sendmacClick:(id)sender
{
    unsigned char msg[16] = { 0xF2,14, 106,'0','0','0','0','0','0','0','0','0','0','0','0',0 };
    msg[15] =[self getCheckSum:msg :15];
    NSData * dataarr = [NSData dataWithBytes:msg length:16];
    NSLog(@"ACK:%@",dataarr);
   [self writeData:dataarr];
    
    // [self presentAlertViewFormacadd];
    
    
}
-(void)sendMacAddress
{
    unsigned char msg[16] = { 0xF2,14, 106,'0','0','0','0','0','0','0','0','0','0','0','0',0 };
    msg[15] =[self getCheckSum:msg :15];
    NSData * dataarr = [NSData dataWithBytes:msg length:16];
    NSLog(@"ACK:%@",dataarr);
   [self writeData:dataarr];
}






-(void)MCD_PutData:(unsigned char *)data_val
{
    //NSMutableArray * defaultarr=[[NSMutableArray alloc]init];
    
    //GlobalVars *globals = [GlobalVars sharedInstance];
    NSUserDefaults *Default=[NSUserDefaults standardUserDefaults];
    NSString *firmwareversion=[Default objectForKey:@"Firmware Version"];
    if([ firmwareversion containsString:@"A"])
    {
        
         NSInteger ecg_data[13];
        //  unsigned char ECG_saveBuf[30];
        // short anaresult;
       // int t_ecgdata[8];
        
      //  int i,j = 0;
        // get the 16 Byte array from the comm port
        
       /* t_ecgdata[0] = *(ecg_data + L2) = ( (*(data_val + 0) * 128)
                                           + (*(data_val + 1))
                                           - 3000); // mulitplied by .6 to ensure that the display height matches
        
        t_ecgdata[1] = *(ecg_data + L3) = ( (*(data_val + 2) * 128)
                                           + (*(data_val + 3) )
                                           -    3000);
        
        j = 2;
        for (i = 0; i < 6; i++)
        {
            j += 2;
            t_ecgdata[i + 2] = *(ecg_data + i + 6) = ((*(data_val + j) * 128)
                                                      + (*(data_val + 1 + j) )
                                                      -    3000);
        }
        
        *(ecg_data + L1) = *(ecg_data + L2) - *(ecg_data + L3) ;
        *(ecg_data + AVF) = (*(ecg_data  + L2) + *(ecg_data + L3))  / 2;//1.732F ;
        *(ecg_data + AVL) = (*(ecg_data  + L1) + -*(ecg_data  + L3))  / 2;//1.732F ;
        *(ecg_data + AVR) = (-*(ecg_data  + L1) + -*(ecg_data  + L2))  / 2;//1.732F ;
        *(ecg_data + RESP) = *(data_val + 16)-90;*/
        
        
        ecg_data[1] = ((data_val[0] << 7 | data_val[1]) - 3000);
        
        ecg_data[2] = ((data_val[2] << 7 | data_val[3]) - 3000);
        
        ecg_data[0] = (ecg_data[1] - ecg_data[2]);
        
        ecg_data[6] = ((data_val[4] << 7 | data_val[5]) - 3000);
        
        ecg_data[7] = ((data_val[6] << 7 | data_val[7]) - 3000);
        
        ecg_data[8] = ((data_val[8] << 7 | data_val[9]) - 3000);
        
        ecg_data[9] = ((data_val[10] << 7 | data_val[11]) - 3000);
        
        ecg_data[10] = ((data_val[12] << 7 | data_val[13]) - 3000);
        
        ecg_data[11] = ((data_val[14] << 7 | data_val[15]) - 3000);
        
        ecg_data[5] = ((ecg_data[1] + ecg_data[2]) / 2);//avf
        
        ecg_data[4] = ((ecg_data[0] - ecg_data[2]) / 2);//avl
        
        ecg_data[3] = ((-ecg_data[0] - ecg_data[1]) / 2);//avr
        
        ecg_data[12] = (data_val[16] & 0xFF) - 90;
        
        
        /* for (int i = 0; i < 13; i++) {
            // NSLog(@"%ld",(long)ecg_data[i]);
            NSNumber *number = [NSNumber numberWithInteger:ecg_data[i]];
           // [plot_dta1 insertObject:number atIndex:i];
            [globals.plotdata addObject:number];
        }*/
         LiveEcgMonitor*lead = [[LiveEcgMonitor alloc] init];
       [lead popDemoDataAndPushToLeads1:ecg_data :12];
        
       /*  _Draw_the_frame *lead1= [[_Draw_the_frame alloc] init];
        [lead1 plotWave:ecg_data];*/
      
    }
    else
    {
        NSInteger ecg_data[16];
        //  unsigned char ECG_saveBuf[30];
        // short anaresult;
      /*  short t_ecgdata[8];
        
        int i,j = 0;
        // get the 16 Byte array from the comm port
        
        t_ecgdata[0] = *(ecg_data + L2) = ( (long)(*(data_val + 0) * 128)
                                           + (*(data_val + 1)  )
                                           -    3000); // mulitplied by .6 to ensure that the display height matches
        
        t_ecgdata[1] = *(ecg_data + L3) = ( (long)(*(data_val + 2) * 128)
                                           + (*(data_val + 3) )
                                           -    3000);
        
        j = 2;
        for (i = 0; i < 6; i++)
        {
            j += 2;
            t_ecgdata[i + 2] = *(ecg_data + i + 6) = ((long)(*(data_val + j) * 128)
                                                      + (*(data_val + 1 + j) )
                                                      -    3000);
        }
        
        *(ecg_data + L1) = *(ecg_data + L2) - *(ecg_data + L3) ;
        *(ecg_data + AVF) = (short)(*(ecg_data  + L2) + *(ecg_data + L3))  / 2;//1.732F ;
        *(ecg_data + AVL) = (short)(*(ecg_data  + L1) + -*(ecg_data  + L3))  / 2;//1.732F ;
        *(ecg_data + AVR) = (short)(-*(ecg_data  + L1) + -*(ecg_data  + L2))  / 2;//1.732F ;*/
        
        ecg_data[1] = ((data_val[0] << 7 | data_val[1]) - 3000);
        
        ecg_data[2] = ((data_val[2] << 7 | data_val[3]) - 3000);
        
        ecg_data[0] = (ecg_data[1] - ecg_data[2]);
        
        ecg_data[6] = ((data_val[4] << 7 | data_val[5]) - 3000);
        
        ecg_data[7] = ((data_val[6] << 7 | data_val[7]) - 3000);
        
        ecg_data[8] = ((data_val[8] << 7 | data_val[9]) - 3000);
        
        ecg_data[9] = ((data_val[10] << 7 | data_val[11]) - 3000);
        
        ecg_data[10] = ((data_val[12] << 7 | data_val[13]) - 3000);
        
        ecg_data[11] = ((data_val[14] << 7 | data_val[15]) - 3000);
        
        ecg_data[5] = ((ecg_data[1] + ecg_data[2]) / 2);//avf
        
        ecg_data[4] = ((ecg_data[0] - ecg_data[2]) / 2);//avl
        
        ecg_data[3] = ((-ecg_data[0] - ecg_data[1]) / 2);//avr
        
        
        
        ecg_data[12] = ((data_val[18] << 7 | data_val[19]) - 3000);//V7
        
        ecg_data[13] = ((data_val[20] << 7 | data_val[21]) - 3000);//V8
        
        ecg_data[14] = ((data_val[22] << 7 | data_val[23]) - 3000);//V9
        
      
        
        ecg_data[15] = (data_val[16] & 0xFF) - 90;
        
        
       /* for (int i = 0; i < 16; i++) {
            // NSLog(@"%ld",(long)ecg_data[i]);
            NSNumber *number = [NSNumber numberWithInteger:ecg_data[i]];
          //  [plot_dta1 insertObject:number atIndex:i];
            [globals.plotdata addObject:number];
            [defaultarr addObject:number];
            
            
        }*/
        LiveEcgMonitor*lead = [[LiveEcgMonitor alloc] init];
        [lead popDemoDataAndPushToLeads1:ecg_data :15];
      
        
    }
    
   
    
    
}


#define YEAR_OFFSET 1980
-(void)SET_DateTime
{
    NSCalendar* calendar = [NSCalendar currentCalendar];
    [calendar setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSDateComponents *compon = [calendar components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear |NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond fromDate:[NSDate date]];
    
    int year =(int) (compon.year - 2000);
    NSLog(@"comp%@",compon);
    unsigned char msg[10] = { 0xF2,8, 104,(Byte)(compon.day),(Byte)(compon.month),(Byte)year,(Byte)compon.hour,(Byte)compon.minute,(Byte)compon.second,0 };
    // unsigned char msg[5] = { 0xF2,8, 104,(Byte)compon,0 };
    msg[9] =[self getCheckSum:msg :9];
    NSData * dataarr = [NSData dataWithBytes:msg length: 10];
    NSLog(@"ACK:%@",dataarr);
   [self writeData:dataarr];
    
    
}
-(void)StartContinousECG
{
    unsigned char msg[5] = { 0xF1,3, 1, 0x55,0 };
    
    msg[4] =[self getCheckSum:msg :4];
    NSData * dataarr = [NSData dataWithBytes:msg length: 5];
    NSLog(@"ECGACK:%@",dataarr);
   [self writeData:dataarr];
}

-(void) StopContinousECG{
    
    unsigned char msg[5] = { 0xF1,3, 1, 0xAA,0 };
    
    msg[4] =[self getCheckSum:msg :4];
    NSData * dataarr = [NSData dataWithBytes:msg length: 5];
   [self writeData:dataarr];
}

-(void) sendMCDSettings{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"MCDSettings" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        NSLogger *logger=[[NSLogger alloc]init];
        logger.degbugger = true;
        
        [logger log:@"Coredata Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:[error localizedDescription], @"Mcd Setting page", nil] error:TRUE];
        
    } else {
      //  NSLog(@"%@", result);
        if (result.count > 0) {
            NSManagedObject *mcdsett = (NSManagedObject *)[result objectAtIndex:0];
            // NSLog(@"1 - %@", mcdsett);
            
            
            int leadconfig = 0;
            switch ([[mcdsett valueForKey:@"lead_config"] intValue]) {
                case 0:
                    leadconfig = 3;
                    break;
                case 1:
                    leadconfig = 5;
                    break;
                case 2:
                    leadconfig = 12;
                    break;
                case 3:
                    leadconfig = 15;
                    break;
                    
                default:
                    break;
            }
            
            int filter50_60=0;
            if ([[mcdsett valueForKey:@"fiftyhz_filter"] intValue]== 0 &&[[mcdsett valueForKey:@"sixtyhz_filter"] intValue] == 0)
            { filter50_60 = 2; }
            else if([[mcdsett valueForKey:@"sixtyhz_filter"] intValue] == 1)
            { filter50_60 = 1; }
            else if([[mcdsett valueForKey:@"fiftyhz_filter"] intValue] == 1)
            { filter50_60 = 0; }
            
            
            [self SetMCDSettings:filter50_60 :[[mcdsett valueForKey:@"base_line_fil"] intValue]:[[mcdsett valueForKey:@"leads"]intValue] :[[mcdsett valueForKey:@"gain"]intValue]+1 :2-[[mcdsett valueForKey:@"sampling_rate"]intValue] :[[mcdsett valueForKey:@"timeinterval_pre"]intValue] :[[mcdsett valueForKey:@"timeinterval_post"]intValue] :[[mcdsett valueForKey:@"volume_level"]intValue] :leadconfig :[[mcdsett valueForKey:@"respiration"]intValue] :[[mcdsett valueForKey:@"orientation"]intValue]];
            
            
            
            
        }
    }
    
    
}

-(void) sendSupraVentricularTachycardiaThreshold{
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",NARROW_QRS_REGULAR_D_DESCRIPTION];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",posturecode]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
        //NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            //NSLog(@"1 - %@", doctorssett);
            
            
            [self SET_VentricularTachyNarrow:[[doctorssett valueForKey:@"dd"] intValue] :[[doctorssett valueForKey:@"dd"] intValue] :[[doctorssett valueForKey:@"ddflag"] intValue] :[[doctorssett valueForKey:@"posture"] intValue]];
            
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
    
    
}


-(void) sendVentricularTachycardiaThreshold {
    
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",WIDE_QRS_REGULAR_D_DESCRIPTION];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",posturecode]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
        //NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            //NSLog(@"1 - %@", doctorssett);
            
            
            [self SET_VentricularTachyWide:[[doctorssett valueForKey:@"dd"]intValue] :[[doctorssett valueForKey:@"dd"]intValue] :[[doctorssett valueForKey:@"ddflag"]intValue] :[[doctorssett valueForKey:@"posture"]intValue]];
            
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
    
    
}

-(void) sendBradycardiaThreshold{
    
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",BRADICARDIA_REGULAR_D_DESCRIPTION];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",posturecode]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
        //NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            // NSLog(@"1 - %@", doctorssett);
            
            
            [self SET_Bradycardia:[[doctorssett valueForKey:@"dd"]intValue] :[[doctorssett valueForKey:@"dd"]intValue] :[[doctorssett valueForKey:@"ddflag"]intValue] :[[doctorssett valueForKey:@"posture"]intValue]];
            
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
    
    
    
}

-(void) sendHRSuspensionpercThreshold {
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",HR_SUSPENSION];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",posturecode]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
       // NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            //NSLog(@"1 - %@", doctorssett);
            
            
            
            [self SET_SuspensionPercent:1 :[[doctorssett valueForKey:@"dd"]intValue] :[[doctorssett valueForKey:@"posture"]intValue]];
            
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
    
    
}

-(void) sendPauseThreshold{
    
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",PAUSE];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",posturecode]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
       // NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            // NSLog(@"1 - %@", doctorssett);
            
            
            
            [self SET_PauseValue:[[doctorssett valueForKey:@"dd"]intValue] :[[doctorssett valueForKey:@"ddflag"]intValue] :[[doctorssett valueForKey:@"posture"]intValue]];
            
            
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
    
    
}

-(void) sendPVCThreshold{
    
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",PVC_VALUE];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",posturecode]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
        //NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            //NSLog(@"1 - %@", doctorssett);
            
            
            
            [self SET_PVC:[[doctorssett valueForKey:@"ddflag"]intValue] :[[doctorssett valueForKey:@"dd"]intValue] :[[doctorssett valueForKey:@"posture"]intValue]];
            
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
    
    
}

-(void) sendAsystoleThreshold{
    
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",ASYSTOLE_VALUE];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",posturecode]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
       // NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            // NSLog(@"1 - %@", doctorssett);
            
            
            
            [self SET_Asystole:[[doctorssett valueForKey:@"ddflag"]intValue] :[[doctorssett valueForKey:@"dd"]intValue] :[[doctorssett valueForKey:@"posture"]intValue]];
            
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
    
    
}

-(void) sendBigeminyThreshold{
    
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",BIGEMINY];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",posturecode]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
       // NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            // NSLog(@"1 - %@", doctorssett);
            
            
            
            [self  SET_BigemenyValue:[[doctorssett valueForKey:@"ddflag"]intValue] :[[doctorssett valueForKey:@"posture"]intValue] :[[doctorssett valueForKey:@"dd"]intValue]];
            
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
    
}

-(void) sendTrigeminyThreshold{
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",TRIGEMINY];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",posturecode]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
        //NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            //  NSLog(@"1 - %@", doctorssett);
            
            
            
            [self  SET_TrigemenyStatus:[[doctorssett valueForKey:@"posture"]intValue] :[[doctorssett valueForKey:@"ddflag"]intValue] :[[doctorssett valueForKey:@"dd"]intValue]];
            
            
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
    
    
}

-(void) sendCoupletThreshold {
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",COUPLET];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",posturecode]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
       // NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            // NSLog(@"1 - %@", doctorssett);
            
            
            
            [self  SET_CoupletStatus:[[doctorssett valueForKey:@"posture"]intValue] :[[doctorssett valueForKey:@"ddflag"]intValue] :[[doctorssett valueForKey:@"dd"]intValue]];
            
            
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
    
    
}

-(void) sendTripletThreshold {
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",TRIPLET];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",posturecode]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
      //  NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            // NSLog(@"1 - %@", doctorssett);
            
            
            
            [self  SET_TripletStatus:[[doctorssett valueForKey:@"posture"]intValue] :[[doctorssett valueForKey:@"ddflag"]intValue] :[[doctorssett valueForKey:@"dd"]intValue]];
            
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
    
    
}

-(void) sendSTElevationThreshold {
    
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",ST_ELEVATION_VALUES];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",posturecode]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
        //NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            //NSLog(@"1 - %@", doctorssett);
            
            
            
            [self  SET_STelev:[[doctorssett valueForKey:@"dd"]intValue] :[[doctorssett valueForKey:@"ddflag"]intValue] :[[doctorssett valueForKey:@"posture"]intValue]];
            
            
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
    
    
}

-(void) sendSTDepressionThreshold {
    
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",ST_DEPRESSION_VALUES];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",posturecode]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
        //NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            //NSLog(@"1 - %@", doctorssett);
            
            
            
            [self  SET_Depre:[[doctorssett valueForKey:@"dd"]intValue] :[[doctorssett valueForKey:@"ddflag"]intValue] :[[doctorssett valueForKey:@"posture"]intValue]];
            
            
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
    
    
}

-(void) sendSTSuspensionThreshold {
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",ST_SUSPENSION];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",posturecode]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
       // NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            //NSLog(@"1 - %@", doctorssett);
            
            
            
            [self  SET_SuspensionPercent :0 :[[doctorssett valueForKey:@"dd"]intValue] :[[doctorssett valueForKey:@"posture"]intValue]];
            
            
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
    
    
}

-(void) sendProlongedqtThreshold {
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",PROLNGED_QT_VALUES];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",posturecode]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
       // NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            // NSLog(@"1 - %@", doctorssett);
            
            
            
            [self  SET_ProlongQT:[[doctorssett valueForKey:@"dd"]intValue] :[[doctorssett valueForKey:@"ddflag"]intValue] :[[doctorssett valueForKey:@"posture"]intValue]];
            
            
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
    
    
}

-(void) sendQRSNarrowThreshold{
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",QRS_WIDTH_VALUES];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",posturecode]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
       // NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            //NSLog(@"1 - %@", doctorssett);
            
            
            
            [self  SET_QRSwidthNarow:[[doctorssett valueForKey:@"dd"]intValue] :[[doctorssett valueForKey:@"ddflag"]intValue] :[[doctorssett valueForKey:@"posture"]intValue]];
            
            
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
    
}

-(void) sendQRSWideThreshold{
    
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",QRS_WIDE_WIDTH];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",posturecode]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
      //  NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            //NSLog(@"1 - %@", doctorssett);
            
            
            
            [self  SET_QRSwidthWide:[[doctorssett valueForKey:@"dd"]intValue] :[[doctorssett valueForKey:@"ddflag"]intValue] :[[doctorssett valueForKey:@"posture"]intValue]];
            
            
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
    
    
}

-(void) sendtempratureThreshold {
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",TEMPERATURE_VALUE];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",1]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
       // NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            //NSLog(@"1 - %@", doctorssett);
            
            NSLog(@"Temperaturevalue%d",[[doctorssett valueForKey:@"dd"]intValue]);
            
            [self  SET_Temperature :[[doctorssett valueForKey:@"dd"]intValue] :0 :[[doctorssett valueForKey:@"ddflag"]intValue] :[[doctorssett valueForKey:@"posture"]intValue]];
            
            
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
    
    
}

-(void) sendRespirationHighThreshold{
    
    resphighthreshold = false;
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",HIGH_RESPIRATION_RATE];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",1]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
       // NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            // NSLog(@"1 - %@", doctorssett);
            
            
            
            [self  SET_resperationRate:1 :[[doctorssett valueForKey:@"dd"]intValue]:[[doctorssett valueForKey:@"posture"]intValue] :[[doctorssett valueForKey:@"ddflag"]intValue]];
            
            
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
    
}

-(void) sendRespirationLowThreshold{
    
    resphighthreshold = true;
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",LOW_RESPIRATION_RATE];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",1]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
       // NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            // NSLog(@"1 - %@", doctorssett);
            
            
            
            [self  SET_resperationRate:0 :[[doctorssett valueForKey:@"dd"]intValue]:[[doctorssett valueForKey:@"posture"]intValue] :[[doctorssett valueForKey:@"ddflag"]intValue]];
            
            
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
    
}

-(void) sendApneaThreshold{
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",APNEA_VALUE];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",1]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
       // NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            //NSLog(@"1 - %@", doctorssett);
            
            
            
            [self  SET_Apnea:[[doctorssett valueForKey:@"ddflag"]intValue] :[[doctorssett valueForKey:@"dd"]intValue] :[[doctorssett valueForKey:@"ddflag"]intValue]];
            
            
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
    
    
}

-(void) sendNoMotionThreshold{
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",NO_MOTION_VALUE];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",1]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
       // NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            // NSLog(@"1 - %@", doctorssett);
            
            
            
            [self  SET_resperationRate:0 :[[doctorssett valueForKey:@"dd"]intValue]:[[doctorssett valueForKey:@"posture"]intValue] :[[doctorssett valueForKey:@"ddflag"]intValue]];
            int minutes = [[doctorssett valueForKey:@"dd"]intValue];
            if ([[doctorssett valueForKey:@"ddflag"]intValue] == 0) {
                minutes = 0;
            }
            
            [self SET_NoMotion:minutes];
             suspensionTimeThresholdCode = NARROW_QRS_REGULAR_D_DESCRIPTION;// SupraVentricularTach
            
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
    
    
}

-(void) sendSuspensionTimeThreshold{
    // Codes codes = new Codes();
    // TODO Auto-generated method stub
    GlobalVars *globals = [ GlobalVars sharedInstance];
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    int flag=0;
  
     int SETTINGS_CODE,SUSPENSION_TIME;
     NSString *code=[NSString stringWithFormat:@"%d",suspensionTimeThresholdCode];
    NSPredicate *pred;
    NSString* filter = @"%K == %@";
    NSArray* args = @[@"event_code",code];
    

    
    if (suspensionTimeThresholdCode!=0) {
         pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        // NSLog(@"Enter Corect code number");
    }
    
    if (flag == 1) {
        
        // NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"SuspensionSettings"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            NSManagedObject* suspenssett = [results objectAtIndex:0];
           
          
           
            SETTINGS_CODE=[[suspenssett valueForKey:@"event_code"]intValue];
            SUSPENSION_TIME=[[suspenssett valueForKey:@"suspension_time"] intValue];
            switch (SETTINGS_CODE) {
                    
                case 2001:
                {
                    // st elevation
                    [self SET_SuspTime:SUSPENSION_TIME :0];
                    suspensionTimeThresholdCode = ST_DEPRESSION_VALUES;
                }
                    break;
                case 2002:
                {
                    // st depression
                    [self SET_SuspTime:SUSPENSION_TIME :1];
                    suspensionTimeThresholdCode = PROLNGED_QT_VALUES;
                }
                    break;
                    
                case 2003:
                {
                    // T wave
                    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
                    
                    NSEntityDescription *entity = [NSEntityDescription entityForName:@"MCDSettings" inManagedObjectContext:self.managedObjectContext];
                    [fetchRequest setEntity:entity];
                    
                    NSError *error = nil;
                    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
                    
                    if (error) {
                        NSLog(@"Unable to execute fetch request.");
                        NSLog(@"%@, %@", error, error.localizedDescription);
                        NSLogger *logger=[[NSLogger alloc]init];
                        logger.degbugger = true;
                        
                        [logger log:@"Coredata Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:[error localizedDescription], @"Mcd Setting page", nil] error:TRUE];
                        
                    } else {
                        // NSLog(@"%@", result);
                        if (result.count > 0) {
                            NSManagedObject *mcdsett = (NSManagedObject *)[result objectAtIndex:0];
                            // NSLog(@"1 - %@", mcdsett);
                            
                            
                            if([[mcdsett valueForKey:@"inverted_t_wave"] isEqualToString:@"0"])
                            {
                                [self SET_SuspTime:0 :2];
                                suspensionTimeThresholdCode = TEMPERATURE_VALUE;
                            }
                            else
                            {
                                [self SET_SuspTime:SUSPENSION_TIME :2];
                                suspensionTimeThresholdCode = TEMPERATURE_VALUE;
                            }
                        }
                    }
                    
                }
                    break;
                    
                case 2005:
                    // prolonged
                    
                    [self SET_SuspTime:SUSPENSION_TIME :4];
                    //suspensionTimeThresholdCode = QRS_WIDTH_VALUES;
                    suspensionTimeThresholdCode =QRS_WIDE_WIDTH;
                    break;
                case 2006:
                    // QRS_WIDTH_VALUES
                    [self SET_SuspTime:SUSPENSION_TIME :16];
                    suspensionTimeThresholdCode = QRS_WIDE_WIDTH;
                    break;
                case 2007:
                    // NARROW_QRS_REGULAR_D_DESCRIPTION // SuperVentericular Tachycardia
                    [self SET_SuspTime:SUSPENSION_TIME :6];
                    suspensionTimeThresholdCode = WIDE_QRS_REGULAR_D_DESCRIPTION;
                    
                    break;
                case 2011:
                    // WIDE_QRS_REGULAR_D_DESCRIPTION // Ventricular Tachycardia
                    
                    [self SET_SuspTime:SUSPENSION_TIME :7];
                    suspensionTimeThresholdCode = BRADICARDIA_REGULAR_D_DESCRIPTION;
                    
                    break;
                case 2017:
                    // brady
                    [self SET_SuspTime:SUSPENSION_TIME :10];
                    suspensionTimeThresholdCode = PAUSE;
                    
                    break;
                case 2022:
                    // Temp
                    [self SET_SuspTime:SUSPENSION_TIME :12];
                    suspensionTimeThresholdCode = LOW_RESPIRATION_RATE;
                    break;
                    
                case 2023:
                    // fall
                {
                    [self SET_SuspTime:SUSPENSION_TIME :13];
                   
                }
                    break;
                case 2021:
                    // Apnea
                    [self SET_SuspTime:SUSPENSION_TIME :11];
                    suspensionTimeThresholdCode = NO_MOTION_VALUE;
                    break;
                case 2015:
                    
                    // PVC
                    [self SET_SuspTime:SUSPENSION_TIME :8];
                    suspensionTimeThresholdCode = ASYSTOLE_VALUE;
                    break;
                case 2016:
                    // Asy
                    [self SET_SuspTime:SUSPENSION_TIME :9];
                    suspensionTimeThresholdCode = BIGEMINY;
                    break;
                case 2024:
                    // No motion
                {
                    
                    [self SET_SuspTime:SUSPENSION_TIME :14];
                    globals.suspenseflag=true;
                    globals.currentSetting = NONE;
                   // globals.MCDSyncflag=2;
                    
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    [defaults setValue:@"2" forKey:@"MCDSyncflag"];
                    [defaults synchronize];
                    NSLog(@"currentsetting%d",globals.currentSetting);
                }
                    break;
                    
                case 2032: // Bigemeny
                    [self SET_SuspTime:SUSPENSION_TIME :3];
                    suspensionTimeThresholdCode = TRIGEMINY;
                    break;
                case 2031: // trigemeny
                    [self SET_SuspTime:SUSPENSION_TIME :18];
                    suspensionTimeThresholdCode = COUPLET;
                    break;
                case 2030: // couplet
                    [self SET_SuspTime:SUSPENSION_TIME :19];
                    suspensionTimeThresholdCode = TRIPLET;
                    break;
                case 2029: // Triplet
                    [self SET_SuspTime:SUSPENSION_TIME :20];
                    suspensionTimeThresholdCode = ST_ELEVATION_VALUES;
                    break;
                    
                case 2036: // Respiration rate
                    [self SET_SuspTime:SUSPENSION_TIME :15];
                    suspensionTimeThresholdCode = APNEA_VALUE;
                    break;
                case 2026: // Pause
                    [self SET_SuspTime:SUSPENSION_TIME :17];
                    suspensionTimeThresholdCode = PVC_VALUE;
                    break;
                case 2027:
                    // QRS Wide
                    [self SET_SuspTime:SUSPENSION_TIME :5];
                    suspensionTimeThresholdCode = T_WAVE_VALUE;
                    break;
                default:
                    break;
            }
            
          
        } else {
            // NSLog(@"Enter Corect code number");
        }
    }
    
    
}



-(void) onEventBudgeRecieved:(Byte[]) data
{
    
    
    AudioServicesPlaySystemSound (1352);
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
    
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    if (state == UIApplicationStateBackground)
    {
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:0];
        localNotification.alertBody = @"New Event Occured";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
        NSDictionary *infoDict = [NSDictionary dictionaryWithObjectsAndKeys:@"EVENTBUDGE", @"key", nil];
        localNotification.userInfo = infoDict;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
    
    
    
    
    NSLogger*logger=[[NSLogger alloc]init];
    
    EventStorage *Eventsettings = [NSEntityDescription insertNewObjectForEntityForName:@"EventStorage"
                                                                inManagedObjectContext:self.managedObjectContext];
    
    
    
    NSString * filename = [NSString stringWithFormat:@"%d_%d_%d_%d_%d_%d.raw",data[0],data[1],(data[2] + 2000),data[3],data[4],data[5]];
    NSLog(@"filll%@",filename);
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //make a file name to write the data to using the documents directory:
    NSString *EVENT_ECG_FILEPATH = [NSString stringWithFormat:@"%@/%@",
                                    documentsDirectory,filename];
    
    /*NSString *content = @"";
     [content writeToFile:EVENT_ECG_FILEPATH
     atomically:NO
     encoding:NSStringEncodingConversionAllowLossy
     error:nil];*/
    
    
    float _temp = (float) (data[9] + ((float) data[10] / 10.0f));
    NSString * EVENT_TEMPERATURE = [NSString stringWithFormat:@"%f",_temp];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components1 = [[NSDateComponents alloc] init];
    [components1 setDay: data[0]];
    [components1 setMonth: data[1]];
    [components1 setYear:(data[2] + 2000)];
    [components1 setHour:data[3]];
    [components1 setMinute: data[4]];
    [components1 setSecond: data[5]];
    [calendar setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];//[NSTimeZone defaultTimeZone]];
    NSDate *getTime = [calendar dateFromComponents:components1];
    CommonHelper *commhelp=[[CommonHelper alloc]init];
    NSString *EVENT_DATE_TIME = [commhelp getDateUTC:@"yyyy-MM-dd HH:mm:ss" :@"yyyy-MM-dd HH:mm:ss" :DateAsNSDate :@"2014-04-25" :getTime];
    
    
    
    
    
    BOOL savedSwitch= [[NSUserDefaults standardUserDefaults] boolForKey:@"SwitchState"];
    if(savedSwitch)
    {
        
        GlobalVars *globals = [GlobalVars sharedInstance];
        latit=[globals latitude];
        longit=[globals longititude];
    }
    else
    {
        latit=@"0";
        longit=@"0";
        location=[NSString stringWithFormat:@"0,0"];
    }
    
    
    
    
    // To be
    // added after protocol is fix
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"MCDSettings" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSString *samplingrate,*respiration_ONOFF,*leadconf;
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
        logger.degbugger = true;
        [logger log:@"Coredata Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:[error localizedDescription], @"Adding clinical record", nil] error:TRUE];
        
    } else {
       // NSLog(@"%@", result);
        if (result.count > 0) {
            NSManagedObject *mcdsett = (NSManagedObject *)[result objectAtIndex:0];
            samplingrate=[mcdsett valueForKey:@"sampling_rate"];
            respiration_ONOFF=[mcdsett valueForKey:@"respiration"];
            leadconf=[mcdsett valueForKey:@"lead_config"];
        }
    }
    
    //MCDLEad
    NSString *Event_McdLead;
    NSUserDefaults * Default = [NSUserDefaults standardUserDefaults];
    
    NSString *firmwareversion=[Default objectForKey:@"Firmware Version"];
    if([ firmwareversion containsString:@"A"])
    {
        Event_McdLead=@"12";
    }
    else if([ firmwareversion containsString:@"B"])//B
        Event_McdLead=@"15";
    
    
    
    
    NSString * Event_systolic = @"0";
    NSString * Event_diastolic = @"0";
    NSString * Event_wgtbox = @"0";
    NSString * Event_Glucose = @"0";
    NSString * EVENT_LEAD_DETECTION = [NSString stringWithFormat:@"%d",data[8]];
    NSString * EVENT_POSTURE_CODE = [NSString stringWithFormat:@"%d",data[7]];
    //NSString *EVENT_TYPE=@"2";
    int eventval = (data[6] - 1);
    NSString *EVENT_THRESHOLD_TYPE;
    NSLog(@"case 25 Event Val :%d" , eventval);
    
    
    NSArray *sampleeventarray=[[NSArray alloc]initWithObjects:@"Normal",@"Ventricular Tachycardia",@"Triplet",@"Couplet", @"Trigeminy", @"Bigeminy", @"Premature Ventricular Contraction", @"Asystole", @"Pause", @"Inverted T", @"SupraVentricular Tachycardia", @"Bradycardia", @"ST Elevation", @"ST Depression", @"Prolonged QT", @"Wide QRS", @"Low QRS", @"Unknown", @"Patient Call", @"Respiration Rate High Event", @"Apnea Event", @"Temperature High Event",@"Free Fall Event", @"No Motion", @"Respiration Rate Low Event",@"On Demand", @"HR High Threshold", @"HR Low Threshold", nil];
    
    
    NSArray *Arrythmiacont=[[NSArray alloc]initWithObjects:@"Ventricular Tachycardia",@"Triplet",@"Couplet", @"Trigeminy", @"Bigeminy", @"Premature Ventricular Contraction", @"Asystole", @"Pause", @"SupraVentricular Tachycardia", @"Bradycardia", nil];
    
    NSArray *Ischemiacont=[[NSArray alloc]initWithObjects:@"ST Elevation", @"ST Depression", @"Prolonged QT", @"Wide QRS", @"Low QRS", nil];
    
    
    NSArray *Othercont=[[NSArray alloc]initWithObjects: @"Respiration Rate High_Event", @"Apnea Event", @"Temperature High Event",@"Free Fall Event", @"No Motion", @"Respiration Rate Low Event",@"HR High Threshold", @"HR Low Threshold", nil];
    /* MasterCaution.log.info("case 25 Event Recieved. Event Number : "
     + eventval + " Filename : "
     + _EventStorage.EVENT_ECG_FILEPATH);*/
    
    
    if (eventval >=sampleeventarray.count) {
        
        logger.degbugger = true;
        [logger log:@"Event data Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:[error localizedDescription], @"Ignoring Event as the event value is greater than number of events i.e. greater than ", nil] error:TRUE];
        
        
    }
    else
    {
        
        if([Arrythmiacont containsObject:sampleeventarray[eventval]])
        {
            EVENT_THRESHOLD_TYPE=@"1";
        }
        else if ([Ischemiacont containsObject:sampleeventarray[eventval]])
        {
            EVENT_THRESHOLD_TYPE=@"2";
        }
        else if ([Othercont containsObject:sampleeventarray[eventval]])
        {
            EVENT_THRESHOLD_TYPE=@"3";
        }
        else
        {
            EVENT_THRESHOLD_TYPE=@"0";
        }
        
        [self ToastNotification:sampleeventarray[eventval]];
        
        int eventvalue = ((data[11] & 0xFF) << 7 | (data[12] & 0xFF));
        NSLog(@"eventval%d",eventvalue);
        // EventStruct * eventstruct=[self eventcode:eventvalue :sampleeventarray[eventval]];
        // int EVENT_CODE=[self eventcode:sampleeventarray[eventval]];
        //int EVENT_CODE=eventstruct.code;
        Eventsettings.event_datetime=EVENT_DATE_TIME;
        Eventsettings.event_ecgfilepath= EVENT_ECG_FILEPATH ;
        Eventsettings.event_ecg_id=@"";
        Eventsettings.event_glucose=Event_Glucose;
        Eventsettings.event_latitude=latit;
        Eventsettings.event_longitude =longit;
        Eventsettings.event_reason =sampleeventarray[eventval];
        Eventsettings.event_temp =EVENT_TEMPERATURE;
        Eventsettings.event_value =[NSString stringWithFormat:@"%d",eventvalue];
        Eventsettings.event_weight =Event_wgtbox;
        Eventsettings.event_bpdia =Event_diastolic;
        Eventsettings.event_bpsys =Event_systolic;
        //Eventsettings.event_code =[NSNumber numberWithInt:EVENT_CODE];
        Eventsettings.event_heartrate =[NSString stringWithFormat:@"%d",((data[13] << 7) | data[14])];
        Eventsettings.event_lead_config =leadconf;
        Eventsettings.event_lead_detection =EVENT_LEAD_DETECTION;
        Eventsettings.event_posture_code =EVENT_POSTURE_CODE;
        Eventsettings.event_pr_interval =[NSString stringWithFormat:@"%d",((data[28] << 7) | data[29])];
        Eventsettings.event_qrs_interval =[NSString stringWithFormat:@"%d",data[20] << 7 | data[21]];
        Eventsettings.event_qtc_interval =[NSString stringWithFormat:@"%d",data[30] << 7 | data[31]];
        Eventsettings.event_qt_interval =[NSString stringWithFormat:@"%d",data[26] << 7 | data[27]];
        Eventsettings.event_receive_complete =@"0";
        Eventsettings.event_respiration =[NSString stringWithFormat:@"%d",data[15] & 0xFF];
        Eventsettings.event_respiration_amplitude =[NSString stringWithFormat:@"%d",data[24] << 7
                                                    | data[25]];
        Eventsettings.event_respiration_onoff =respiration_ONOFF;
        Eventsettings.event_resp_variance =[NSString stringWithFormat:@"%d",data[18] << 7 | data[19]];
        Eventsettings.event_rr_interval =[NSString stringWithFormat:@"%d",data[16] << 7 | data[17]];
        Eventsettings.event_sampling_rate =samplingrate;
        Eventsettings.event_thresholdtype =EVENT_THRESHOLD_TYPE;
        //Eventsettings.event_type =EVENT_TYPE;
        Eventsettings.event_t_wave = [NSString stringWithFormat:@"%d",data[22] << 7 | data[23]];
        Eventsettings.event_updated_to_device =@"0";
        Eventsettings.event_mcdlead=Event_McdLead;
        Eventsettings.event_mail_sms=@"0";
        
        Eventsettings =[self SetEventValue:Eventsettings :sampleeventarray[eventval]];
        
        
        [self.managedObjectContext save:nil];
        
    }
    [self sendBTEventNotificationWithEventOccured:YES];
    [self sendBTEventNotificationWithNumberOfEventOccured:YES];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL standalone_Mode = [defaults boolForKey:@"ActiveStandalonemode"];
    if(!standalone_Mode)
    {
          dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^ {
         
              WCFUpload *wcfcall=[[WCFUpload alloc]init];
              [wcfcall UploadEventHeader1:EVENT_DATE_TIME :[Eventsettings.event_code stringValue]];
         });
    
    }
}


-(void) onEventHeaderRecieved:(Byte[]) data {
    
    GlobalVars *globals = [GlobalVars sharedInstance];
   // globals.MCDSyncflag=2;
    NSLogger*logger=[[NSLogger alloc]init];
    
    
    NSString * filename = [NSString stringWithFormat:@"%d_%d_%d_%d_%d_%d.raw",data[0],data[1],(data[2] + 2000),data[3],data[4],data[5]];
    NSLog(@"Header%@",filename);
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //make a file name to write the data to using the documents directory:
    universalfilepath = [NSString stringWithFormat:@"%@/%@",
                         documentsDirectory,filename];
    
     float _temp = (float) (data[9] + ((float) data[10] / 10.0f));
   // float _temp = (float) (data[10] + ((float) data[11] / 10.0f));
    NSString * EVENT_TEMPERATURE = [NSString stringWithFormat:@"%f",_temp];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components1 = [[NSDateComponents alloc] init];
    [components1 setDay: data[0]];
    [components1 setMonth: data[1]];
    [components1 setYear:(data[2] + 2000)];
    [components1 setHour:data[3]];
    [components1 setMinute: data[4]];
    [components1 setSecond: data[5]];
    [calendar setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    NSDate *getTime = [calendar dateFromComponents:components1];
    CommonHelper *commhelp=[[CommonHelper alloc]init];
    NSString *EVENT_DATE_TIME = [commhelp getDateUTC:@"yyyy-MM-dd HH:mm:ss" :@"yyyy-MM-dd HH:mm:ss" :DateAsNSDate :@"2014-04-25" :getTime];
    
    NSLog(@"etve%@",EVENT_DATE_TIME);
    
    
    
    BOOL savedSwitch= [[NSUserDefaults standardUserDefaults] boolForKey:@"SwitchState"];
    if(savedSwitch)
    {
        //[commhelp startupdatinglocation];
        
        latit=[globals latitude];
        longit=[globals longititude];
    }
    else
    {
        latit=@"0";
        longit=@"0";
        location=[NSString stringWithFormat:@"0,0"];
    }
    
    
    
    
    // To be
    // added after protocol is fix
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"MCDSettings" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSString *samplingrate,*respiration_ONOFF,*leadconf;
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
        logger.degbugger = true;
        [logger log:@"Coredata Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:[error localizedDescription], @"Adding clinical record", nil] error:TRUE];
        
    } else {
       // NSLog(@"%@", result);
        if (result.count > 0) {
            NSManagedObject *mcdsett = (NSManagedObject *)[result objectAtIndex:0];
            samplingrate=[mcdsett valueForKey:@"sampling_rate"];
            respiration_ONOFF=[mcdsett valueForKey:@"respiration"];
            leadconf=[mcdsett valueForKey:@"lead_config"];
        }
    }
    
    
    
    
    int eventval = data[6] - 1;
    NSString *Event_McdLead;
    NSString *EVENT_THRESHOLD_TYPE;
    NSString *Event_Type;
    NSString * EventCode,*Event_Ecgid;
    NSUserDefaults * Default = [NSUserDefaults standardUserDefaults];
    
    NSString *firmwareversion=[Default objectForKey:@"Firmware Version"];
    if([ firmwareversion containsString:@"A"])
    {
        Event_McdLead=@"12";
    }
    else if([ firmwareversion containsString:@"B"])//B
        Event_McdLead=@"15";
    
    
    NSArray *sampleeventarray=[[NSArray alloc]initWithObjects:@"Normal",@"Ventricular Tachycardia",@"Triplet",@"Couplet", @"Trigeminy", @"Bigeminy", @"Premature Ventricular Contraction", @"Asystole", @"Pause", @"Inverted T", @"SupraVentricular Tachycardia", @"Bradycardia", @"ST Elevation", @"ST Depression", @"Prolonged QT", @"Wide QRS", @"Low QRS", @"Unknown", @"Patient Call", @"Respiration Rate High Event", @"Apnea Event", @"Temperature High Event",@"Free Fall Event", @"No Motion", @"Respiration Rate Low Event",@"On Demand", @"HR High Threshold", @"HR Low Threshold", nil];
    
    
    NSArray *Arrythmiacont=[[NSArray alloc]initWithObjects:@"Ventricular Tachycardia",@"Triplet",@"Couplet", @"Trigeminy", @"Bigeminy", @"Premature Ventricular Contraction", @"Asystole", @"Pause", @"SupraVentricular Tachycardia", @"Bradycardia", nil];
    
    NSArray *Ischemiacont=[[NSArray alloc]initWithObjects:@"ST Elevation", @"ST Depression", @"Prolonged QT", @"Wide QRS", @"Low QRS", nil];
    
    
    NSArray *Othercont=[[NSArray alloc]initWithObjects: @"Respiration Rate High_Event", @"Apnea Event", @"Temperature High Event",@"Free Fall Event", @"No Motion", @"Respiration Rate Low Event",@"HR High Threshold", @"HR Low Threshold", nil];
    /* MasterCaution.log.info("case 25 Event Recieved. Event Number : "
     + eventval + " Filename : "
     + _EventStorage.EVENT_ECG_FILEPATH);*/
    
    
    
    if (eventval >=sampleeventarray.count) {
        
        logger.degbugger = true;
        [logger log:@"Event data Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:[error localizedDescription], @"Ignoring Event as the event value is greater than number of events i.e. greater than ", nil] error:TRUE];
        
        
    }
    else
    {
        
        if([Arrythmiacont containsObject:sampleeventarray[eventval]])
        {
            EVENT_THRESHOLD_TYPE=@"1";
        }
        else if ([Ischemiacont containsObject:sampleeventarray[eventval]])
        {
            EVENT_THRESHOLD_TYPE=@"2";
        }
        else if ([Othercont containsObject:sampleeventarray[eventval]])
        {
            EVENT_THRESHOLD_TYPE=@"3";
        }
        else
        {
            EVENT_THRESHOLD_TYPE=@"0";
        }
        
        
        int eventvalue = ((data[11] & 0xFF) << 7 | (data[12] & 0xFF));
        NSString * Event_systolic = @"0";
        NSString * Event_diastolic = @"0";
        NSString * Event_wgtbox = @"0";
        NSString * Event_Glucose = @"0";
        NSString * EVENT_LEAD_DETECTION = [NSString stringWithFormat:@"%d", data[8]];
        NSString * EVENT_POSTURE_CODE = [NSString stringWithFormat:@"%d",data[7]];
        
        
        
        
        
        NSMutableArray *results = [[NSMutableArray alloc]init];
        int flag=0;
        NSPredicate *pred;
        NSString* filter = @"%K == %@";
        NSArray* args = @[@"event_datetime",EVENT_DATE_TIME];
        if (EVENT_DATE_TIME.length!=0) {
            pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
            flag=1;
        } else {
            flag=0;
            NSLog(@"Enter Corect code empty");
            
        }
        
        if (flag == 1) {
            
            
           // NSLog(@"predicate: %@",pred);
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"EventStorage"];
            [fetchRequest setPredicate:pred];
            results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
            
            if (results.count > 0) {
                
                NSManagedObject *EVENTSTORAGE = (NSManagedObject *)[results objectAtIndex:0];
                NSLog(@"datematched");
                
                
                
                
                [EVENTSTORAGE setValue:[NSString stringWithFormat:@"%d",data[16] << 7 | data[17]] forKey:@"event_rr_interval"];
                [EVENTSTORAGE setValue:[NSString stringWithFormat:@"%d",data[20] << 7 | data[21]] forKey:@"event_qrs_interval"];
                [EVENTSTORAGE setValue:[NSString stringWithFormat:@"%d",data[22] << 7 | data[23]] forKey:@"event_t_wave"];
                [EVENTSTORAGE setValue:[NSString stringWithFormat:@"%d",data[24] << 7
                                        | data[25]] forKey:@"event_respiration_amplitude"];
                [EVENTSTORAGE setValue:[NSString stringWithFormat:@"%d",data[30] << 7 | data[31]] forKey:@"event_qtc_interval"];
                [EVENTSTORAGE setValue:[NSString stringWithFormat:@"%d",data[28] << 7 | data[29]] forKey:@"event_pr_interval"];
                
                [EVENTSTORAGE setValue:EVENT_DATE_TIME forKey:@"event_datetime"];
                [EVENTSTORAGE setValue:@"3" forKey:@"event_receive_complete"];
                EventCode=[[EVENTSTORAGE valueForKey:@"event_code"]stringValue];
                Event_Type=[EVENTSTORAGE valueForKey:@"event_type"];
                Event_Ecgid=[EVENTSTORAGE valueForKey:@"event_ecg_id"];
                
                [self.managedObjectContext save:nil];
                // _EventDBadapter.UpdateReceiveComplete(3,_EventStorage.EVENT_ECG_FILEPATH);
                if(Event_Ecgid.length!=0)
                {
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    BOOL standalone_Mode = [defaults boolForKey:@"ActiveStandalonemode"];
                    if(!standalone_Mode)
                    {
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^ {
                            
                            WCFUpload *wcfcall=[[WCFUpload alloc]init];
                            [wcfcall UploadEventHeader1:EVENT_DATE_TIME :EventCode];
                        });
                        
                    }
                }
                
                
                
            } else {
                 [self ToastNotification:sampleeventarray[eventval]];
                EventStorage *Eventsettings = [NSEntityDescription insertNewObjectForEntityForName:@"EventStorage"
                                                                            inManagedObjectContext:self.managedObjectContext];
                Eventsettings.event_datetime=EVENT_DATE_TIME;
                Eventsettings.event_ecgfilepath= universalfilepath;
                Eventsettings.event_ecg_id=@"";
                Eventsettings.event_glucose=Event_Glucose;
                Eventsettings.event_latitude=latit;
                Eventsettings.event_longitude =longit;
                Eventsettings.event_reason =sampleeventarray[eventval];
                Eventsettings.event_temp =EVENT_TEMPERATURE;
                Eventsettings.event_value =[NSString stringWithFormat:@"%d",eventvalue];
                Eventsettings.event_weight =Event_wgtbox;
                Eventsettings.event_bpdia =Event_diastolic;
                Eventsettings.event_bpsys =Event_systolic;
                //Eventsettings.event_code =[NSNumber numberWithInt:EVENT_CODE];
                Eventsettings.event_heartrate =[NSString stringWithFormat:@"%d",((data[13] << 7) | data[14])];
                Eventsettings.event_lead_config =leadconf;
                Eventsettings.event_lead_detection =EVENT_LEAD_DETECTION;
                Eventsettings.event_posture_code =EVENT_POSTURE_CODE;
                Eventsettings.event_pr_interval =[NSString stringWithFormat:@"%d",((data[28] << 7) | data[29])];
                Eventsettings.event_qrs_interval =[NSString stringWithFormat:@"%d",data[20] << 7 | data[21]];
                Eventsettings.event_qtc_interval =[NSString stringWithFormat:@"%d",data[30] << 7 | data[31]];
                Eventsettings.event_qt_interval =[NSString stringWithFormat:@"%d",data[26] << 7 | data[27]];
                Eventsettings.event_receive_complete =@"0";
                Eventsettings.event_respiration =[NSString stringWithFormat:@"%d",data[15] & 0xFF];
                Eventsettings.event_respiration_amplitude =[NSString stringWithFormat:@"%d",data[24] << 7
                                                            | data[25]];
                Eventsettings.event_respiration_onoff =respiration_ONOFF;
                Eventsettings.event_resp_variance =[NSString stringWithFormat:@"%d",data[18] << 7 | data[19]];
                Eventsettings.event_rr_interval =[NSString stringWithFormat:@"%d",data[16] << 7 | data[17]];
                Eventsettings.event_sampling_rate =samplingrate;
                Eventsettings.event_thresholdtype =EVENT_THRESHOLD_TYPE;
                //Eventsettings.event_type =EVENT_TYPE;
                Eventsettings.event_t_wave = [NSString stringWithFormat:@"%d",data[22] << 7 | data[23]];
                Eventsettings.event_updated_to_device =@"0";
                Eventsettings.event_mcdlead=Event_McdLead;
                
                
                NSLog(@"case 25 Event Val :%d" , eventval);
                
                
                Eventsettings =[self SetEventValue:Eventsettings :sampleeventarray[eventval]];
                Event_Type= Eventsettings.event_type;
                NSLog(@"Enter Corect code number header");
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                BOOL standalone_Mode = [defaults boolForKey:@"ActiveStandalonemode"];
                if(!standalone_Mode)
                {
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^ {
                        
                        WCFUpload *wcfcall=[[WCFUpload alloc]init];
                        [wcfcall UploadEventHeader1:EVENT_DATE_TIME :[Eventsettings.event_code stringValue]];
                    });
                
                }
            }
            
        }
        
        
        
        
    }
    [self ShowNotification:[Event_Type intValue] :EVENT_THRESHOLD_TYPE];
    [self sendBTEventNotificationWithEventOccured:YES];
   
}

- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

-(EventStorage*)SetEventValue:(EventStorage*) _EventStorage :(NSString*) event {
    EventStorage *eventstorage = _EventStorage;
    
    
    
    EventStruct * eventStruct =[self eventcode:eventstorage :event];
    NSLog(@"------------- Value  : %f",eventStruct.value);
    eventstorage.event_code =[NSNumber numberWithInt: eventStruct.code];
    
    
    eventstorage.event_type = @"3";
    
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSMutableArray *results1 = [[NSMutableArray alloc]init];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",[NSString stringWithFormat:@"%@",eventstorage.event_code], @"posture", eventstorage.event_posture_code];
    if ([NSString stringWithFormat:@"%@",eventstorage.event_code].length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
        // NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            if ([[doctorssett valueForKey:@"ddflag"] isEqualToString:@"1"]) {
                
                switch (eventStruct.comparisionType) {
                    case NORMAL:
                        if (eventStruct.value >= [[doctorssett valueForKey:@"dd"] floatValue])
                            eventstorage.event_type = @"2";
                        break;
                    case INVERSE:
                        if (eventStruct.value >= [[doctorssett valueForKey:@"dd"] floatValue])
                            eventstorage.event_type = @"2";
                        break;
                    default:
                        break;
                }
            }
            
            
        }
        else {
            NSLog(@"Enter Corect code number");
        }
        
        
        
        if ([[self PatientLevel] isEqualToString:@"Level4"]) {
            NSFetchRequest *fetchRequest1 = [[NSFetchRequest alloc]initWithEntityName:@"ThresholdSett"];
            [fetchRequest1 setPredicate:pred];
            results1 = [[self.managedObjectContext executeFetchRequest:fetchRequest1 error:nil] mutableCopy];
            
            if (results1.count > 0) {
                
                NSManagedObject *genthressett = (NSManagedObject *)[results1 objectAtIndex:0];
                
                
                
                if ([[genthressett valueForKey:@"flag"] isEqualToString:@"1"])
                {
                    
                    if (eventStruct.code == WIDE_QRS_REGULAR_D_DESCRIPTION)
                    {
                        eventstorage.event_type = @"1";
                        
                    }
                    
                    else
                    {
                        
                        float yd = [[genthressett valueForKey:@"yd"]floatValue];
                        float rd = [[genthressett valueForKey:@"rd"]floatValue];
                        float value = eventStruct.value;
                        switch (eventStruct.comparisionType) {
                            case INVERSE:
                                
                                if (value < rd) {
                                    eventstorage.event_type = @"1";
                                    // Tabactivity._showPersonalBadge = true;
                                } else if (value >= rd && value <= yd) {
                                    eventstorage.event_type =@"0";
                                    // Tabactivity._showPersonalBadge = true;
                                }
                                break;
                            case NORMAL:
                                
                                if (value > rd) {
                                    eventstorage.event_type = @"1";
                                    
                                    // Tabactivity._showPersonalBadge = true;
                                } else if (value >= yd && value <= rd) {
                                    eventstorage.event_type =@"0";
                                    
                                    // Tabactivity._showPersonalBadge = true;
                                }
                                break;
                                
                            default:
                                break;
                        }// End switch case
                        
                    }
                    
                }// End if Flag == 1
                
                
                
            }
            else {
                NSLog(@"Enter Corect general code number");
            }
            
        }
        
        
        
    }
    return eventstorage;
}

-(void) ShowNotification :(int) eventtype  :(NSString *)thresholdtype{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
     NSString *Header,*docPhno,*docName;
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"PatientDetails" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    NSString * userlvl;
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        if (result.count > 0) {
            NSManagedObject *patient = (NSManagedObject *)[result objectAtIndex:0];
            // NSLog(@"1 - %@", patient);
            
            userlvl   = [patient valueForKey:@"level"];
            docPhno=[patient valueForKey:@"doc_ph_no"];
            docName=[patient valueForKey:@"doc_name"];
            
        }
    }
    /*  NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
     NSData *usrdta=[userDefault objectForKey:@"user_details"];
     userlevel=[usrdta valueForKey:@"Patientlevel"];*/
    
    

    switch (eventtype) {
        case 0:
        {
            if([userlvl isEqualToString:@"Level3"]||[userlvl isEqualToString:@"Level4"])
            {
                UIColor *Headercolor;
                switch (eventtype) {
                    case 0:
                        Headercolor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"yellow_color.png"]]; // ("Y");
                        break;
                    case 1:
                        Headercolor= [UIColor colorWithPatternImage:[UIImage imageNamed:@"red_color.png"]];// ("R");
                        break;
                    case 2:
                        Headercolor=[UIColor blueColor];// ("R");
                        break;
                    case 3:
                        Headercolor=[UIColor grayColor];// ("R");
                        break;
                        
                    default:
                        break;
                }
                
                
                switch ([thresholdtype intValue]) {
                    case 0:
                        Header=@"";
                        break;
                    case 1:
                        Header=@"Arrhythmia";
                        break;
                    case 2:
                        Header=@"Ischemia";
                        break;
                    case 3:
                        Header=@"Other Events";
                        break;
                    default:
                        break;
                }
                
                SDCAlertView *alert = [[SDCAlertView alloc]   initWithTitle:Header message:@"Please call your doctor immediately!"
                                                                   delegate:self
                                                          cancelButtonTitle:@"Dismiss"
                                                          otherButtonTitles:@"Call", nil];
                [alert setAlertViewStyle:SDCAlertViewStyleDefault];
                
                [alert setTitleLabelFont:[UIFont fontWithName:@"Arial" size:24.0]];
                [alert setTitleLabelTextColor:Headercolor];
                [alert setMessageLabelFont:[UIFont fontWithName:@"Arial" size:14.0]];
                
                
                UILabel * lblspin = [[UILabel alloc] init];
                [lblspin setTranslatesAutoresizingMaskIntoConstraints:NO];
                [alert.contentView addSubview:lblspin];
                lblspin.backgroundColor=Headercolor;
                [lblspin sdc_pinWidthToWidthOfView:alert.contentView offset:0];
                [lblspin sdc_pinHeight:3];
                [lblspin sdc_horizontallyCenterInSuperview];
                [lblspin sdc_verticallyCenterInSuperviewWithOffset:
                 -40];
                
                // [lblspin sdc_verticallyCenterInSuperviewWithOffset:SDCAutoLayoutStandardSiblingDistance];
                
                UITextView *textView = [[UITextView alloc] init];
                [textView setTranslatesAutoresizingMaskIntoConstraints:NO];
                [textView setEditable:NO];
                textView.text=[NSString stringWithFormat:@"%@\n%@",docName,docPhno];//    @"Doctor Felix Amar\n1234566789";
                textView.autocorrectionType = UITextAutocorrectionTypeNo;
                [alert.contentView addSubview:textView];
                
                textView.backgroundColor=[UIColor clearColor];
                textView.textAlignment=NSTextAlignmentCenter;
                textView.font=[UIFont fontWithName:@"Arial" size:18.0];
                [textView sdc_pinWidthToWidthOfView:alert.contentView offset:-15];
                [textView sdc_pinHeight:50];
                [textView sdc_horizontallyCenterInSuperview];
                [textView sdc_verticallyCenterInSuperviewWithOffset:SDCAutoLayoutStandardSiblingDistance];
                
                NSString * telno=[NSString stringWithFormat:@"tel://%@",docPhno];
                [alert showWithDismissHandler: ^(NSInteger buttonIndex) {
                    NSLog(@"Tapped button: %@", @(buttonIndex));
                    if (buttonIndex == 1) {
                        NSURL *url = [NSURL URLWithString:telno];
                        [[UIApplication  sharedApplication] openURL:url];
                    }
                    else {
                        NSLog(@"Cancelled");
                    }
                }];
            }

        }
           
            break;
        case 1:
        {
            if([userlvl isEqualToString:@"Level3"]||[userlvl isEqualToString:@"Level4"])
            {
            UIColor * redUIColor= [UIColor colorWithPatternImage:[UIImage imageNamed:@"red_color.png"]];
            
            SDCAlertView *alert = [[SDCAlertView alloc]   initWithTitle:@"URGENT" message:@""
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil, nil];
            [alert setAlertViewStyle:SDCAlertViewStyleDefault];
            
            [alert setTitleLabelFont:[UIFont fontWithName:@"Arial" size:24.0]];
            [alert setTitleLabelTextColor:redUIColor];
            [alert setMessageLabelFont:[UIFont fontWithName:@"Arial" size:14.0]];
            
            
            UILabel * lblspin = [[UILabel alloc] init];
            [lblspin setTranslatesAutoresizingMaskIntoConstraints:NO];
            [alert.contentView addSubview:lblspin];
            lblspin.backgroundColor=redUIColor;
            [lblspin sdc_pinWidthToWidthOfView:alert.contentView offset:0];
            [lblspin sdc_pinHeight:3];
            [lblspin sdc_horizontallyCenterInSuperview];
            [lblspin sdc_verticallyCenterInSuperviewWithOffset:
             -40];
            
            // [lblspin sdc_verticallyCenterInSuperviewWithOffset:SDCAutoLayoutStandardSiblingDistance];
            
            UITextView *textView = [[UITextView alloc] init];
            [textView setTranslatesAutoresizingMaskIntoConstraints:NO];
            [textView setEditable:NO];
            textView.text=@"Go To Hospital";
            textView.autocorrectionType = UITextAutocorrectionTypeNo;
            [alert.contentView addSubview:textView];
            
            textView.backgroundColor=[UIColor clearColor];
            textView.textAlignment=NSTextAlignmentCenter;
            textView.font=[UIFont fontWithName:@"Arial" size:18.0];
            [textView sdc_pinWidthToWidthOfView:alert.contentView offset:-15];
            [textView sdc_pinHeight:50];
            [textView sdc_horizontallyCenterInSuperview];
            [textView sdc_verticallyCenterInSuperviewWithOffset:SDCAutoLayoutStandardSiblingDistance];
            
            
            [alert show];
         }
        }
            break;
            
        default:
            /*
            mNotifyBuilder.setContentText("New Event occured.").setNumber(
                                                                          ++numMessages);
            mNotifyBuilder.setSmallIcon(R.drawable.okscreen);
            notifyID = 1;
            resultIntent.putExtra("ispersonalshow", 1);
            resultPendingIntent = PendingIntent.getActivity(
                                                            getApplicationContext(), 0, resultIntent,
                                                            PendingIntent.FLAG_UPDATE_CURRENT);
            mNotifyBuilder.setContentIntent(resultPendingIntent);*/
            break;
    }
    
    
}

-(void)ToastNotification :(NSString *)eventname
{
    NSString *message = [NSString stringWithFormat:@"%@ event has been detected",eventname];
    
    UIAlertView *toast = [[UIAlertView alloc] initWithTitle:nil
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:nil
                                          otherButtonTitles:nil, nil];
    [toast show];
    
    int duration = 3; // duration in seconds
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [toast dismissWithClickedButtonIndex:0 animated:YES];
    });
}
    
    
    
-(NSString *)PatientLevel
    {
        //changelevel
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"PatientDetails" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        // NSLog(@"%@", result);
        if (result.count > 0) {
            NSManagedObject *patient = (NSManagedObject *)[result objectAtIndex:0];
            // NSLog(@"1 - %@", patient);
            
            
            NSString *  Patientlevel   = [patient valueForKey:@"level"];
            return Patientlevel;
            
            
        }
    }
    return 0;
    
}

-(EventStruct *)eventcode:( EventStorage *)_eventstorage :(NSString *)eventType
{
    int val = 0;
    EventStruct * eventStruct = [[EventStruct alloc]init];
    eventStruct.comparisionType=NORMAL;
    eventStruct.value=[_eventstorage.event_value floatValue];
    //int eventStruct=0;
    
    if([eventType isEqualToString:@"Normal"])
    {
        eventStruct.code = 0;
        eventStruct.comparisionType=NOO;
    }
    
    if([eventType isEqualToString:@"Ventricular Tachycardia"])
        eventStruct.code  = WIDE_QRS_REGULAR_D_DESCRIPTION;
    
    if([eventType isEqualToString:@"Triplet"])
        eventStruct.code  = TRIPLET;
    
    if([eventType isEqualToString:@"Couplet"])
        eventStruct.code =COUPLET;
    
    if([eventType isEqualToString:@"Trigeminy"])
        eventStruct.code =TRIGEMINY;
    
    if([eventType isEqualToString:@"Bigeminy"])
        eventStruct.code =BIGEMINY;
    
    if([eventType isEqualToString:@"Premature Ventricular Contraction"])
        eventStruct.code =PVC_VALUE;
    
    if([eventType isEqualToString:@"Asystole"])
        eventStruct.code =ASYSTOLE_VALUE;
    
    if([eventType isEqualToString:@"Pause"])
        eventStruct.code =PAUSE;
    
    if([eventType isEqualToString:@"Inverted T"])
    {
        eventStruct.code =T_WAVE_VALUE;
        eventStruct.value = [_eventstorage.event_t_wave floatValue];
        eventStruct.comparisionType =NOO;
    }
    
    
    if([eventType isEqualToString:@"SupraVentricular Tachycardia"])
        eventStruct.code =NARROW_QRS_REGULAR_D_DESCRIPTION;
    
    if([eventType isEqualToString:@"Bradycardia"])
    {
        eventStruct.code =BRADICARDIA_REGULAR_D_DESCRIPTION;
        eventStruct.comparisionType = INVERSE;
    }
    
    if([eventType isEqualToString:@"ST Elevation"])
    {
        eventStruct.code =ST_ELEVATION_VALUES;
        val =  (int)roundf([_eventstorage.event_value floatValue]) & 0x7f;//   Math.round( Float.valueOf(_eventstorage.EVENT_VALUE)) & 0x7f;
        eventStruct.value = val;
    }
    if([eventType isEqualToString:@"ST Depression"])
    {
        eventStruct.code =ST_DEPRESSION_VALUES;
        val =(int)roundf([_eventstorage.event_value floatValue]) & 0x7f;;
        eventStruct.value = val;
    }
    
    if([eventType isEqualToString:@"Prolonged QT"])
        eventStruct.code =PROLNGED_QT_VALUES;
    
    if([eventType isEqualToString:@"Wide QRS"])
        eventStruct.code =QRS_WIDE_WIDTH;
    
    if([eventType isEqualToString:@"Low QRS"])
    {
        eventStruct.code =QRS_WIDTH_VALUES;
        eventStruct.comparisionType = INVERSE;
    }
    
    if([eventType isEqualToString:@"Unknown"])
    {
        eventStruct.code =UNKNOWM;
        eventStruct.comparisionType = NOO;
    }
    
    if([eventType isEqualToString:@"Patient Call"])
    {
        eventStruct.code =MANUAL;
        eventStruct.comparisionType = NOO;
    }
    
    if([eventType isEqualToString:@"Respiration Rate High Event"])
    {
        eventStruct.code =HIGH_RESPIRATION_RATE;
        eventStruct.value = [_eventstorage.event_respiration floatValue];
    }
    
    if([eventType isEqualToString:@"Apnea Event"])
        eventStruct.code =APNEA_VALUE;
    
    if([eventType isEqualToString:@"Temperature High Event"])
    {
        eventStruct.code =TEMPERATURE_VALUE;
        eventStruct.value = [_eventstorage.event_temp floatValue];
    }
    
    if([eventType isEqualToString:@"Free Fall Event"])
        eventStruct.code =FALL_EVENT_VALUE;
    
    if([eventType isEqualToString:@"No Motion"])
        eventStruct.code =NO_MOTION_VALUE;
    
    if([eventType isEqualToString:@"Respiration Rate Low Event"])
    {
        eventStruct.code =LOW_RESPIRATION_RATE;
        
        eventStruct.value = [_eventstorage.event_respiration floatValue];
        eventStruct.comparisionType = INVERSE;
    }
    
    if([eventType isEqualToString:@"On Demand"])
    {
        eventStruct.code =ON_DEMAND;
        eventStruct.comparisionType = NOO;
    }
    
    if([eventType isEqualToString:@"HR High Threshold"])
        eventStruct.code =NARROW_QRS_REGULAR_RELATIVE_DESCRIPTION;
    
    if([eventType isEqualToString:@"HR Low Threshold"])
    {
        eventStruct.code =BRADICARDIA_REGULAR_RELATIVE_DESCRIPTION;
        eventStruct.comparisionType = INVERSE;
    }
    
    
    
    return eventStruct;
}



-(void) onEventECGDataRecieved:( uint8_t *) data :(int)datasize {
    
   /* int framelength;
    NSUserDefaults *Default=[NSUserDefaults standardUserDefaults];
    NSString *firmwareversion=[Default objectForKey:@"Firmware Version"];
    if([ firmwareversion containsString:@"A"])
    {
        framelength=20;
    }
    else
    {
        framelength=26;
    }
    */
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:universalfilepath]) {
         resultantdata=[[NSMutableData alloc]init];
    }
    
    NSData *data1;
    if(ecgdatacounter==0)
    {
         resultantdata = [[NSMutableData alloc] init];
        data1 = [NSData dataWithBytes:data length:datasize];
        [resultantdata appendData:data1];
        ecgdatacounter++;
    }
    else
    {
    data1 = [NSData dataWithBytes:data length:datasize];
    [resultantdata appendData:data1];
        ecgdatacounter++;
    }
    // NSData *data1 = [NSData dataWithBytes:data length:framelength];
    //NSLog(@"dtt%@",data1);
    
    if(ecgdatacounter==5)
    {
        ecgdatacounter=0;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:universalfilepath]) {
        
        // the file doesn't exist,we can write out the text using the  NSString convenience method
       

        NSError *error = noErr;
        BOOL success =  [resultantdata writeToFile:universalfilepath options:NSDataWritingAtomic error:&error];
        
        if (!success) {
            // handle the error
            NSLog(@"%@", error);
        }
        
    } else {
        
        // the file already exists, append the text to the end
        
        // get a handle
        fileHandle = [NSFileHandle fileHandleForWritingAtPath:universalfilepath];
        
        // move to the end of the file
        [fileHandle seekToEndOfFile];
        
        // convert the string to an NSData object
        
        
        // write the data to the end of the file
        [fileHandle writeData:resultantdata];
        
        // clean up
        
        
        
    }
    }
    // System.out.println("Data Saving in File");
    // _file.writeToFile(ecgdata);
    // System.out.println("Data Saved in File");
}

-(void) onEventECGDataRecieved1:( NSMutableArray *) data {
    
    
    
    NSData* data1 = [NSKeyedArchiver archivedDataWithRootObject:data];
    // NSData *data1 = [NSData dataWithBytes:data length:sizeof(data)];
    // NSData *data1 = [NSData dataWithBytes:data length:framelength];
    //NSLog(@"dtt%@",data1);
    
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:universalfilepath]) {
        
        // the file doesn't exist,we can write out the text using the  NSString convenience method
        
        NSError *error = noErr;
        BOOL success =  [data1 writeToFile:universalfilepath options:NSDataWritingAtomic error:&error];
        
        if (!success) {
            // handle the error
            NSLog(@"%@", error);
        }
        
    } else {
        
        // the file already exists, append the text to the end
        
        // get a handle
        fileHandle = [NSFileHandle fileHandleForWritingAtPath:universalfilepath];
        
        // move to the end of the file
        [fileHandle seekToEndOfFile];
        
        // convert the string to an NSData object
        
        
        // write the data to the end of the file
        [fileHandle writeData:data1];
        
        // clean up
        
        
        
    }
    // System.out.println("Data Saving in File");
    // _file.writeToFile(ecgdata);
    // System.out.println("Data Saved in File");
}

-(void) onEventComplete
{
    if(fileHandle !=NULL)
    {
        [fileHandle closeFile];
        
        NSMutableArray *results = [[NSMutableArray alloc]init];
        int flag=0;
        NSPredicate *pred;
        NSString* filter = @"%K == %@";
        NSArray* args = @[@"event_ecgfilepath",universalfilepath];
        if (universalfilepath.length!=0) {
            pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
            flag=1;
        } else {
            flag=0;
            NSLog(@"Enter Corect code empty");
            
        }
        
        if (flag == 1) {
            
            
            NSLog(@"predicate: %@",pred);
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"EventStorage"];
            [fetchRequest setPredicate:pred];
            results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
            
            if (results.count > 0) {
                
                NSManagedObject *EVENTSTORAGE = (NSManagedObject *)[results objectAtIndex:0];
                
                [EVENTSTORAGE setValue:@"1" forKey:@"event_receive_complete"];
                [EVENTSTORAGE setValue:@"1" forKey:@"event_mail_sms"];
                
                
                [self.managedObjectContext save:nil];
                
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                BOOL standalone_Mode = [defaults boolForKey:@"ActiveStandalonemode"];
                if(!standalone_Mode)
                {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^ {
                    
                    WCFUpload *wcfcall=[[WCFUpload alloc]init];
                    [wcfcall UploadECG:[EVENTSTORAGE valueForKey:@"event_ecg_id"]  :universalfilepath] ;
                });
                }
                
                
            } else {
                NSLog(@"Enter Corect code number header");
            }
            
        }
    }
    [self sendBTEventNotificationWithEventOccured:YES];
}




/* Funciton toSet Gain as 1.5 */

-(void) onMCDSettingsRecieved:(Byte[]) data {
    /* for (int i = 0; i < data.length; i++) {
     if (data[i] < 0) {
     DefaultThreshold defaultThresholdvalues = new DefaultThreshold();
     defaultThresholdvalues.InsertDefaultMCDSettings(this);
     // Tabactivity._isGettingSetings = false;
     return;
     }
     }*/
    NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"MCDSettings"];
    MCDSettings *mcdsettings=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
    
    [mcdsettings setValue:[NSString stringWithFormat:@"%d",data[5] - 1] forKey:@"gain"];
    [mcdsettings setValue: [NSString stringWithFormat:@"%d",data[1]] forKey:@"fiftyhz_filter"];
    [mcdsettings setValue: [NSString stringWithFormat:@"%d",data[1]] forKey:@"sixtyhz_filter"];
    [mcdsettings setValue: [NSString stringWithFormat:@"%d",data[3]] forKey:@"base_line_fil"];
    [mcdsettings setValue: [NSString stringWithFormat:@"%d",data[16]] forKey:@"volume_level"];
    [mcdsettings setValue: [NSString stringWithFormat:@"%d",data[12] << 7 | data[13]] forKey:@"timeinterval_pre"];
    [mcdsettings setValue: [NSString stringWithFormat:@"%d",data[14] << 7 | data[15]] forKey:@"timeinterval_post"];
    [mcdsettings setValue: [NSString stringWithFormat:@"%d",data[4]] forKey:@"leads"];
    [mcdsettings setValue: [NSString stringWithFormat:@"%d",2 - data[11]] forKey:@"sampling_rate"];
    
    
    [mcdsettings setValue: @"0"              forKey:@"updated_to_site"];
    switch (data[7]) {
        case 3:
            [mcdsettings setValue:@"0" forKey:@"lead_config"];
            break;
        case 5:
            [mcdsettings setValue:@"1" forKey:@"lead_config"];
            
            break;
        case 12:
            [mcdsettings setValue:@"2" forKey:@"lead_config"];
            break;
        default:
            break;
    }
    
    
    
    
    [self.managedObjectContext save:nil];
    /* MCDSettingsDbAdapter mcdSettingsDbAdapter = new MCDSettingsDbAdapter(
     this);
     mcdSettingsDbAdapter.open();
     
     mcdSettingsDbAdapter.UpdateNotchFilter(data[1]);
     mcdSettingsDbAdapter.UpdateBaseLineFilter(data[3]);
     mcdSettingsDbAdapter.UpdateEventDetectionLead(data[4]);
     mcdSettingsDbAdapter.UpdateGain(data[5] - 1);
     
     mcdSettingsDbAdapter.UpdatePostTime(data[14] << 7 | data[15]);
     mcdSettingsDbAdapter.UpdatePreTime(data[12] << 7 | data[13]);
     mcdSettingsDbAdapter.UpdateSampleRate(2 - data[11]);
     mcdSettingsDbAdapter.UpdateVolume(data[16]);
     mcdSettingsDbAdapter.Updatetosite(0);
     mcdSettingsDbAdapter.close();
     
     [self GET_AllvaluesAtOnce();*/
    
}

-(void) onLeadSettingRecieved:(Byte[]) data {
    NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"MCDSettings"];
    MCDSettings *mcdsettings=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
    
    NSString *leadvalue= [NSString stringWithFormat:@"%d",data[0]];
    NSLog(@"%@",leadvalue);
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL standalone_Mode = [defaults boolForKey:@"ActiveStandalonemode"];
    
    if(![[mcdsettings valueForKey:@"leads"] isEqualToString:leadvalue])
    {
        NSDate *date = [[NSDate alloc] init];
        CommonHelper * commhelp=[[CommonHelper alloc] init];
        NSString *modifieddate=[commhelp getDateUTC:@"yyyy-MM-dd" :@"MM/dd/yyyy HH:mm:ss" :DateAsNSDate :@"2014-04-25" :date];
        
         [mcdsettings setValue: [NSString stringWithFormat:@"%d",data[0]] forKey:@"leads"];
         [mcdsettings setValue: @"0"              forKey:@"updated_to_site"];
         [mcdsettings setValue:modifieddate forKey:@"modified_time"];
          [self.managedObjectContext save:nil];
        
        if(!standalone_Mode)
        {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^ {
                
                WCFUpload *wcfcall=[[WCFUpload alloc]init];
                [wcfcall uploadMCDSetting];
            });
       
        }
        
    }
    
}

-(void) onDeviceIDRecieved:(NSString *) DeviceID {
    
    NSLog(@"Got Device ID :%@",DeviceID);
    GlobalVars *global = [GlobalVars sharedInstance];
    global.mcd_DeviceId=DeviceID;
    [self Get_Firmware_Details];
    
}

-(void) onFirmwareDetails:(NSString *)version :(NSString *)lastupdatedon {
    
    
    NSLog(@"Got Firmware Version :%@ Last Updated:%@",version,lastupdatedon);
    GlobalVars *global = [GlobalVars sharedInstance];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:version forKey:@"Firmware Version" ];
    [defaults synchronize];
    
    NSArray *ecgchcksum=[version componentsSeparatedByString:@"."];
    
    if([ecgchcksum[3] intValue]>=19)
    {
        if([ecgchcksum[2] intValue]>7)
        {
            if([ecgchcksum[1] intValue]>0)
            {
                [defaults setBool:true forKey:@"ECGCheckSum"];
                [defaults synchronize];
            }
            else
            {
               
                [defaults setBool:false forKey:@"ECGCheckSum"];
                [defaults synchronize];
            }
        }
        else
        {
           
            [defaults setBool:false forKey:@"ECGCheckSum"];
            [defaults synchronize];
        }
 
    }
    else{
        
       
        [defaults setBool:false forKey:@"ECGCheckSum"];
        [defaults synchronize];
    }
    
    
    global.mcd_firmware_version=version;
    global.mcd_last_updatd=lastupdatedon;
    
   
    BOOL standalone_Mode = [defaults boolForKey:@"ActiveStandalonemode"];
    if(!standalone_Mode)
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^ {
            
            WCFUpload *wcfcall=[[WCFUpload alloc]init];
            [wcfcall mcmVersionDetails:version];
        });
   
    }
    if ([version containsString:@"A"]) {
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSLogger *logger=[[NSLogger alloc]init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"MCDSettings" inManagedObjectContext:self.managedObjectContext];
        [fetchRequest setEntity:entity];
        
        NSError *error = nil;
        NSString *leadconfig;
        NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
        
        if (error) {
            NSLog(@"Unable to execute fetch request.");
            NSLog(@"%@, %@", error, error.localizedDescription);
            
            logger.degbugger = true;
            [logger log:@"Coredata Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:[error localizedDescription], @"Adding firmwaredetail record", nil] error:TRUE];
            
        } else {
            
            if (result.count > 0) {
                NSManagedObject *mcdsett = (NSManagedObject *)[result objectAtIndex:0];
                leadconfig=[mcdsett valueForKey:@"lead_config"];
                if([leadconfig intValue]==3)
                {
                    [mcdsett setValue:@"2" forKey:@"lead_config"];
                    [self.managedObjectContext save:nil];
                }
                
                
            }
            
        }
    }
    
    
    
    [self SET_DateTime];
    
    // sendBroadcast(intent);
    
    
}
-(void) onBuiltInTestRecieved:(Byte)data {
    NSLog(@"Built In Received");
    
    
    GlobalVars *globals = [GlobalVars sharedInstance];
    globals.built_accelerometer = @"0";
    globals.built_electrodes = @"0";
    globals.built_battery =@"0";
    globals.built_bluetooth = @"0";
    globals.built_temperature = @"0";
    globals.built_sdcard = @"0";
    
    
    
    NSLog(@"%@",[NSString stringWithFormat:@"%i",data]);
    
    NSLog(@"%i",data );
    
    NSLog(@"%@", [self convertHexToBinary:[NSString stringWithFormat:@"%i",data]]);
    NSString * theString=[self convertHexToBinary:[NSString stringWithFormat:@"%i",data]];
    
    NSMutableArray *chars = [[NSMutableArray alloc] initWithCapacity:[theString length]];
    for (int i=0; i < [theString length]; i++) {
        NSString *ichar  = [NSString stringWithFormat:@"%C", [theString characterAtIndex:i]];
        [chars addObject:ichar];
        
    }
    NSLog(@"%@",chars);
    if ([self BitStatus:(Byte)data  :0] > 0) {
        globals.built_accelerometer = @"1";
    }
    if ([self BitStatus:(Byte)data :1] > 0) {
        globals.built_electrodes = @"1";
    }
    if ([self BitStatus:(Byte)data  :2] > 0) {
        globals.built_battery = @"1";
    }
    if ([self BitStatus:(Byte)data :3] > 0) {
        globals.built_bluetooth = @"1";
    }
    if ([self BitStatus:(Byte)data :4] > 0) {
        globals.built_temperature = @"1";
    }
    if ([self BitStatus:(Byte)data :5] > 0) {
        globals.built_sdcard = @"1";
    }
    
    
    
    
    BuiltInTestViewController *built=[[BuiltInTestViewController alloc]init];
    [built Builtindataupdate];
    
    NSArray * bityesno = [[NSArray alloc]initWithObjects:@"False",@"True", nil];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^ {
        
        WCFUpload *wcfcall=[[WCFUpload alloc]init];
        
        [wcfcall bitInsert:bityesno[[globals.built_accelerometer intValue]] :bityesno[[globals.built_electrodes intValue]] :bityesno[[globals.built_battery intValue]] :bityesno[[globals.built_bluetooth intValue]] :bityesno[[globals.built_temperature intValue]] :bityesno[[globals.built_sdcard intValue]]];
    });

  
    
}

- (NSString*)convertHexToBinary:(NSString*)hexString
{
    NSMutableString *returnString = [NSMutableString string];
    for(int i = 0; i < [hexString length]; i++)
    {
        char c = [[hexString lowercaseString] characterAtIndex:i];
        
        switch(c) {
            case '0': [returnString appendString:@"0000"]; break;
            case '1': [returnString appendString:@"0001"]; break;
            case '2': [returnString appendString:@"0010"]; break;
            case '3': [returnString appendString:@"0011"]; break;
            case '4': [returnString appendString:@"0100"]; break;
            case '5': [returnString appendString:@"0101"]; break;
            case '6': [returnString appendString:@"0110"]; break;
            case '7': [returnString appendString:@"0111"]; break;
            case '8': [returnString appendString:@"1000"]; break;
            case '9': [returnString appendString:@"1001"]; break;
            case 'a': [returnString appendString:@"1010"]; break;
            case 'b': [returnString appendString:@"1011"]; break;
            case 'c': [returnString appendString:@"1100"]; break;
            case 'd': [returnString appendString:@"1101"]; break;
            case 'e': [returnString appendString:@"1110"]; break;
            case 'f': [returnString appendString:@"1111"]; break;
            default : break;
        }
    }
    
    return returnString;
}


-(void)onSTELevationThresholdRecieved:(Byte[]) data{
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",ST_ELEVATION_VALUES];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",data[1]]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
        NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            //NSLog(@"1 - %@", doctorssett);
            [doctorssett setValue:[NSString stringWithFormat:@"%d",data[3]] forKey:@"dd"];
            [doctorssett setValue:[NSString stringWithFormat:@"%d",data[2]] forKey:@"ddflag"];
            [doctorssett setValue:@"MCD" forKey:@"modify_from"];
            [doctorssett setValue:@"0" forKey:@"updated_to_site"];
            [doctorssett setValue:@"1" forKey:@"updated_to_device"];
            
            
            [self.managedObjectContext save:nil];
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
    
}


-(void) onSTDepressionThresholdRecieved:(Byte[]) data {
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",ST_DEPRESSION_VALUES];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",data[1]]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
        NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            //NSLog(@"1 - %@", doctorssett);
            [doctorssett setValue:[NSString stringWithFormat:@"%d",data[3]] forKey:@"dd"];
            [doctorssett setValue:[NSString stringWithFormat:@"%d",data[2]] forKey:@"ddflag"];
            [doctorssett setValue:@"MCD" forKey:@"modify_from"];
            [doctorssett setValue:@"0" forKey:@"updated_to_site"];
            [doctorssett setValue:@"1" forKey:@"updated_to_device"];
            
            
            [self.managedObjectContext save:nil];
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
}


-(void) onTWaveThresholdRecieved:(Byte[]) data {
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",T_WAVE_VALUE];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",data[1]]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
        NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            // NSLog(@"1 - %@", doctorssett);
            [doctorssett setValue:@"" forKey:@"dd"];
            [doctorssett setValue:[NSString stringWithFormat:@"%d",data[2]] forKey:@"ddflag"];
            [doctorssett setValue:@"MCD" forKey:@"modify_from"];
            [doctorssett setValue:@"0" forKey:@"updated_to_site"];
            [doctorssett setValue:@"1" forKey:@"updated_to_device"];
            
            
            [self.managedObjectContext save:nil];
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
    
}


-(void) onBigeminyThresholdRecieved:(Byte[]) data {
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",BIGEMINY];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",data[1]]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
        NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            // NSLog(@"1 - %@", doctorssett);
            [doctorssett setValue:[NSString stringWithFormat:@"%d",data[3]] forKey:@"dd"];
            [doctorssett setValue:[NSString stringWithFormat:@"%d",data[2]] forKey:@"ddflag"];
            [doctorssett setValue:@"MCD" forKey:@"modify_from"];
            [doctorssett setValue:@"0" forKey:@"updated_to_site"];
            [doctorssett setValue:@"1" forKey:@"updated_to_device"];
            
            
            [self.managedObjectContext save:nil];
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
}

-(void) onProlongedQTThresholdRecieved:(Byte[]) data {
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",PROLNGED_QT_VALUES];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",data[1]]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
        NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            //NSLog(@"1 - %@", doctorssett);
            [doctorssett setValue:[NSString stringWithFormat:@"%d",data[3] << 7 | data[4]] forKey:@"dd"];
            [doctorssett setValue:[NSString stringWithFormat:@"%d",data[2]] forKey:@"ddflag"];
            [doctorssett setValue:@"MCD" forKey:@"modify_from"];
            [doctorssett setValue:@"0" forKey:@"updated_to_site"];
            [doctorssett setValue:@"1" forKey:@"updated_to_device"];
            
            
            [self.managedObjectContext save:nil];
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
    
}

-(void) onWideQRSThresholdRecieved:(Byte[]) data {
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",QRS_WIDE_WIDTH];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",data[1]]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
        NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            //NSLog(@"1 - %@", doctorssett);
            [doctorssett setValue:[NSString stringWithFormat:@"%d",data[3] << 7 | data[4]] forKey:@"dd"];
            [doctorssett setValue:[NSString stringWithFormat:@"%d",data[2]] forKey:@"ddflag"];
            [doctorssett setValue:@"MCD" forKey:@"modify_from"];
            [doctorssett setValue:@"0" forKey:@"updated_to_site"];
            [doctorssett setValue:@"1" forKey:@"updated_to_device"];
            
            
            [self.managedObjectContext save:nil];
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
    
}

-(void) onSTachyThresholdRecieved:(Byte[]) data {
    
    for(int i=0;i<2;i++)
    {
        NSString *code;
        NSMutableArray *results = [[NSMutableArray alloc]init];
        if(i==0)
            code=[NSString stringWithFormat:@"%d",NARROW_QRS_REGULAR_D_DESCRIPTION];
        else
            code=[NSString stringWithFormat:@"%d",NARROW_QRS_REGULAR_RELATIVE_DESCRIPTION];
        int flag=0;
        NSPredicate *pred;
        NSString* filter = @"%K == %@ && %K == %@";
        NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",data[1]]];
        if (code.length!=0) {
            pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
            flag=1;
        } else {
            flag=0;
            NSLog(@"Enter Corect code empty");
            
        }
        
        if (flag == 1) {
            
            
            NSLog(@"predicate: %@",pred);
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
            [fetchRequest setPredicate:pred];
            results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
            
            if (results.count > 0) {
                
                NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
                //NSLog(@"1 - %@", doctorssett);
                if(i==0)
                    [doctorssett setValue:[NSString stringWithFormat:@"%d",data[5] << 7 | data[6]] forKey:@"dd"];
                else
                    [doctorssett setValue:[NSString stringWithFormat:@"%d",data[9] << 7 | data[10]] forKey:@"dd"];
                [doctorssett setValue:[NSString stringWithFormat:@"%d",data[2]] forKey:@"ddflag"];
                [doctorssett setValue:@"MCD" forKey:@"modify_from"];
                [doctorssett setValue:@"0" forKey:@"updated_to_site"];
                [doctorssett setValue:@"1" forKey:@"updated_to_device"];
                
                
                [self.managedObjectContext save:nil];
            } else {
                NSLog(@"Enter Corect code number");
            }
            
        }
    }
    
}

-(void) onVTachyThresholdRecieved:(Byte[]) data {
    
    for(int i=0;i<2;i++)
    {
        NSString *code;
        NSMutableArray *results = [[NSMutableArray alloc]init];
        if(i==0)
            code=[NSString stringWithFormat:@"%d",WIDE_QRS_REGULAR_D_DESCRIPTION];
        else
            code=[NSString stringWithFormat:@"%d",WIDE_QRS_REGULAR_RELATIVE_DESCRIPTION];
        int flag=0;
        NSPredicate *pred;
        NSString* filter = @"%K == %@ && %K == %@";
        NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",data[1]]];
        if (code.length!=0) {
            pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
            flag=1;
        } else {
            flag=0;
            NSLog(@"Enter Corect code empty");
            
        }
        
        if (flag == 1) {
            
            
            NSLog(@"predicate: %@",pred);
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
            [fetchRequest setPredicate:pred];
            results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
            
            if (results.count > 0) {
                
                NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
                //NSLog(@"1 - %@", doctorssett);
                if(i==0)
                    [doctorssett setValue:[NSString stringWithFormat:@"%d",data[5] << 7 | data[6]] forKey:@"dd"];
                else
                    [doctorssett setValue:[NSString stringWithFormat:@"%d",data[9] << 7 | data[10]] forKey:@"dd"];
                [doctorssett setValue:[NSString stringWithFormat:@"%d",data[2]] forKey:@"ddflag"];
                [doctorssett setValue:@"MCD" forKey:@"modify_from"];
                [doctorssett setValue:@"0" forKey:@"updated_to_site"];
                [doctorssett setValue:@"1" forKey:@"updated_to_device"];
                
                
                [self.managedObjectContext save:nil];
            } else {
                NSLog(@"Enter Corect code number");
            }
            
        }
    }
}

-(void) onPVCThresholdRecieved:(Byte[])data {
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",PVC_VALUE];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",data[1]]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
        NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            // NSLog(@"1 - %@", doctorssett);
            [doctorssett setValue:[NSString stringWithFormat:@"%d",data[3]] forKey:@"dd"];
            [doctorssett setValue:[NSString stringWithFormat:@"%d",data[2]] forKey:@"ddflag"];
            [doctorssett setValue:@"MCD" forKey:@"modify_from"];
            [doctorssett setValue:@"0" forKey:@"updated_to_site"];
            [doctorssett setValue:@"1" forKey:@"updated_to_device"];
            
            
            [self.managedObjectContext save:nil];
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
    
}

-(void)onAsystoleThresholdRecieved:(Byte[]) data {
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",ASYSTOLE_VALUE];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",data[1]]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
        NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            // NSLog(@"1 - %@", doctorssett);
            [doctorssett setValue:[NSString stringWithFormat:@"%d",data[3]] forKey:@"dd"];
            [doctorssett setValue:[NSString stringWithFormat:@"%d",data[2]] forKey:@"ddflag"];
            [doctorssett setValue:@"MCD" forKey:@"modify_from"];
            [doctorssett setValue:@"0" forKey:@"updated_to_site"];
            [doctorssett setValue:@"1" forKey:@"updated_to_device"];
            
            
            [self.managedObjectContext save:nil];
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
}

-(void) onBradycardiaThresholdRecieved:(Byte[]) data {
    
    for(int i=0;i<2;i++)
    {
        NSString *code;
        NSMutableArray *results = [[NSMutableArray alloc]init];
        if(i==0)
            code=[NSString stringWithFormat:@"%d",BRADICARDIA_REGULAR_D_DESCRIPTION];
        else
            code=[NSString stringWithFormat:@"%d",BRADICARDIA_REGULAR_RELATIVE_DESCRIPTION];
        int flag=0;
        NSPredicate *pred;
        NSString* filter = @"%K == %@ && %K == %@";
        NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",data[1]]];
        if (code.length!=0) {
            pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
            flag=1;
        } else {
            flag=0;
            NSLog(@"Enter Corect code empty");
            
        }
        
        if (flag == 1) {
            
            
            NSLog(@"predicate: %@",pred);
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
            [fetchRequest setPredicate:pred];
            results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
            
            if (results.count > 0) {
                
                NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
                // NSLog(@"1 - %@", doctorssett);
                if(i==0)
                    [doctorssett setValue:[NSString stringWithFormat:@"%d",data[5] << 7 | data[6]] forKey:@"dd"];
                else
                    [doctorssett setValue:[NSString stringWithFormat:@"%d",data[9] << 7 | data[10]] forKey:@"dd"];
                [doctorssett setValue:[NSString stringWithFormat:@"%d",data[2]] forKey:@"ddflag"];
                [doctorssett setValue:@"MCD" forKey:@"modify_from"];
                [doctorssett setValue:@"0" forKey:@"updated_to_site"];
                [doctorssett setValue:@"1" forKey:@"updated_to_device"];
                
                
                [self.managedObjectContext save:nil];
            } else {
                NSLog(@"Enter Corect code number");
            }
            
        }
    }
}

-(void) onApneaThresholdRecieved:(Byte[]) data {
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",APNEA_VALUE];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",data[1]]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
        NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            //NSLog(@"1 - %@", doctorssett);
            [doctorssett setValue:[NSString stringWithFormat:@"%d",data[3]] forKey:@"dd"];
            [doctorssett setValue:[NSString stringWithFormat:@"%d",data[2]] forKey:@"ddflag"];
            [doctorssett setValue:@"MCD" forKey:@"modify_from"];
            [doctorssett setValue:@"0" forKey:@"updated_to_site"];
            [doctorssett setValue:@"1" forKey:@"updated_to_device"];
            
            
            [self.managedObjectContext save:nil];
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
    
}

-(void) onTempratureThresholdRecieved:(Byte[]) data {
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",TEMPERATURE_VALUE];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",data[1]]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
        NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            //NSLog(@"1 - %@", doctorssett);
            [doctorssett setValue:[NSString stringWithFormat:@"%d",data[3]] forKey:@"dd"];
            [doctorssett setValue:[NSString stringWithFormat:@"%d",data[2]] forKey:@"ddflag"];
            [doctorssett setValue:@"MCD" forKey:@"modify_from"];
            [doctorssett setValue:@"0" forKey:@"updated_to_site"];
            [doctorssett setValue:@"1" forKey:@"updated_to_device"];
            
            
            [self.managedObjectContext save:nil];
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
}

-(void) onFallEventThresholdRecieved:(Byte[]) data {
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",FALL_EVENT_VALUE];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",data[1]]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
        NSFetchRequest *fetchRequest1 = [[NSFetchRequest alloc]initWithEntityName:@"ThresholdSett"];
        [fetchRequest1 setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest1 error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *genthressett = (NSManagedObject *)[results objectAtIndex:0];
            //NSLog(@"1 - %@", genthressett);
            [genthressett setValue:@"" forKey:@"yd"];
            [genthressett setValue:@"" forKey:@"rd"];
            [genthressett setValue:[NSString stringWithFormat:@"%d",data[2]] forKey:@"flag"];
            [genthressett setValue:@"MCD" forKey:@"modify_from"];
            [genthressett setValue:@"0" forKey:@"updated_to_site"];
            [genthressett setValue:@"1" forKey:@"update_to_device"];
            [self.managedObjectContext save:nil];
        } else {
            NSLog(@"Enter Corect general code number");
        }
        
    }
    
}

-(void) onNoMotionThresholdRecieved:(Byte[]) data {
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",NO_MOTION_VALUE];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",data[1]]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
        NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            // NSLog(@"1 - %@", doctorssett);
            if (data[2] == 0) {
                
                [doctorssett setValue:[NSString stringWithFormat:@"%d",1] forKey:@"dd"];
                [doctorssett setValue:[NSString stringWithFormat:@"%d",0] forKey:@"ddflag"];
            } else {
                
                [doctorssett setValue:[NSString stringWithFormat:@"%d",data[2]] forKey:@"dd"];
                [doctorssett setValue:[NSString stringWithFormat:@"%d",1] forKey:@"ddflag"];
            }
            
            [doctorssett setValue:@"MCD" forKey:@"modify_from"];
            [doctorssett setValue:@"0" forKey:@"updated_to_site"];
            [doctorssett setValue:@"1" forKey:@"updated_to_device"];
            
            
            [self.managedObjectContext save:nil];
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
}

-(void) onRespirationThresholdRecieved:(Byte[]) data {
    NSString *code;
    NSMutableArray *results = [[NSMutableArray alloc]init];
    if (data[3] == 0) {
        code=[NSString stringWithFormat:@"%d",LOW_RESPIRATION_RATE];
    } else {
        code=[NSString stringWithFormat:@"%d",HIGH_RESPIRATION_RATE];
    }
    
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",data[1]]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
        NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            //NSLog(@"1 - %@", doctorssett);
            [doctorssett setValue:[NSString stringWithFormat:@"%d",data[4]] forKey:@"dd"];
            [doctorssett setValue:[NSString stringWithFormat:@"%d",data[2]] forKey:@"ddflag"];
            [doctorssett setValue:@"MCD" forKey:@"modify_from"];
            [doctorssett setValue:@"0" forKey:@"updated_to_site"];
            [doctorssett setValue:@"1" forKey:@"updated_to_device"];
            
            
            [self.managedObjectContext save:nil];
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
}

-(void) onNarrowQRSThresholdRecieved:(Byte[]) data{
    
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",QRS_WIDTH_VALUES];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",data[1]]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
        NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            //NSLog(@"1 - %@", doctorssett);
            [doctorssett setValue:[NSString stringWithFormat:@"%d",data[3] << 7 | data[4]] forKey:@"dd"];
            [doctorssett setValue:[NSString stringWithFormat:@"%d",data[2]] forKey:@"ddflag"];
            [doctorssett setValue:@"MCD" forKey:@"modify_from"];
            [doctorssett setValue:@"0" forKey:@"updated_to_site"];
            [doctorssett setValue:@"1" forKey:@"updated_to_device"];
            
            
            [self.managedObjectContext save:nil];
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
}

-(void) onPauseThresholdRecieved:(Byte[]) data{
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",PAUSE];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",data[1]]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
        NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            //NSLog(@"1 - %@", doctorssett);
            [doctorssett setValue:[NSString stringWithFormat:@"%d",data[3] << 7 | data[4]] forKey:@"dd"];
            [doctorssett setValue:[NSString stringWithFormat:@"%d",data[2]] forKey:@"ddflag"];
            [doctorssett setValue:@"MCD" forKey:@"modify_from"];
            [doctorssett setValue:@"0" forKey:@"updated_to_site"];
            [doctorssett setValue:@"1" forKey:@"updated_to_device"];
            
            
            [self.managedObjectContext save:nil];
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
    
}

-(void) onTrigeminyThresholdRecieved:(Byte[]) data {
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",TRIGEMINY];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",data[1]]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
        NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            //NSLog(@"1 - %@", doctorssett);
            [doctorssett setValue:[NSString stringWithFormat:@"%d",data[3]] forKey:@"dd"];
            [doctorssett setValue:[NSString stringWithFormat:@"%d",data[2]] forKey:@"ddflag"];
            [doctorssett setValue:@"MCD" forKey:@"modify_from"];
            [doctorssett setValue:@"0" forKey:@"updated_to_site"];
            [doctorssett setValue:@"1" forKey:@"updated_to_device"];
            
            
            [self.managedObjectContext save:nil];
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
}

-(void) onCoupletThresholdRecieved:(Byte[])data {
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",COUPLET];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",data[1]]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
        NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            // NSLog(@"1 - %@", doctorssett);
            [doctorssett setValue:[NSString stringWithFormat:@"%d",data[3]] forKey:@"dd"];
            [doctorssett setValue:[NSString stringWithFormat:@"%d",data[2]] forKey:@"ddflag"];
            [doctorssett setValue:@"MCD" forKey:@"modify_from"];
            [doctorssett setValue:@"0" forKey:@"updated_to_site"];
            [doctorssett setValue:@"1" forKey:@"updated_to_device"];
            
            
            [self.managedObjectContext save:nil];
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
}

-(void) onTripletThresholdRecieved:(Byte[]) data {
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",TRIPLET];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",data[1]]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
        NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            //NSLog(@"1 - %@", doctorssett);
            [doctorssett setValue:[NSString stringWithFormat:@"%d",data[3]] forKey:@"dd"];
            [doctorssett setValue:[NSString stringWithFormat:@"%d",data[2]] forKey:@"ddflag"];
            [doctorssett setValue:@"MCD" forKey:@"modify_from"];
            [doctorssett setValue:@"0" forKey:@"updated_to_site"];
            [doctorssett setValue:@"1" forKey:@"updated_to_device"];
            
            
            [self.managedObjectContext save:nil];
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
}

-(void) onSuspensionSettingsRecieved:(Byte[]) data {
    
    
    int SUSPENSION_TIME = data[2] << 7 | data[3];
    int SETTINGS_CODE;
    
    switch (data[1]) {
        case 0:
            SETTINGS_CODE = ST_ELEVATION_VALUES;
            
            break;
        case 1:
            SETTINGS_CODE = ST_DEPRESSION_VALUES;
            
            break;
        case 2:
            SETTINGS_CODE = T_WAVE_VALUE;
            
            break;
        case 3:
            SETTINGS_CODE = BIGEMINY;
            
            break;
            
        case 4:
            SETTINGS_CODE = PROLNGED_QT_VALUES;
            
            break;
        case 5:
            SETTINGS_CODE = QRS_WIDTH_VALUES;
            
            break;
        case 6:
            SETTINGS_CODE = NARROW_QRS_REGULAR_D_DESCRIPTION;
            
            break;
        case 7:
            SETTINGS_CODE = WIDE_QRS_REGULAR_D_DESCRIPTION;
            
            break;
        case 8:
            SETTINGS_CODE = PVC_VALUE;
            
            break;
        case 9:
            SETTINGS_CODE = ASYSTOLE_VALUE;
            
            break;
        case 10:
            SETTINGS_CODE = BRADICARDIA_REGULAR_D_DESCRIPTION;
            
            break;
        case 11:
            SETTINGS_CODE = APNEA_VALUE;
            
            break;
        case 12:
            SETTINGS_CODE = TEMPERATURE_VALUE;
            
            break;
        case 13:
            SETTINGS_CODE = FALL_EVENT_VALUE;
            
            break;
        case 14:
            SETTINGS_CODE = NO_MOTION_VALUE;
            
            break;
        case 15:
            SETTINGS_CODE = LOW_RESPIRATION_RATE;
            
            break;
        case 16:
            SETTINGS_CODE = QRS_WIDE_WIDTH;
            
            break;
        case 17:
            SETTINGS_CODE = PAUSE;
            
            break;
        case 18:
            SETTINGS_CODE = TRIGEMINY;
            
            break;
        case 19:
            SETTINGS_CODE = COUPLET;
            
            break;
        case 20:
            SETTINGS_CODE = TRIPLET;
            
            break;
        case 21:
            SETTINGS_CODE = 0;
            
            break;
        default:
            break;
    }
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    int flag=0;
    NSPredicate *pred;
    if ([NSString stringWithFormat:@"%d",SETTINGS_CODE].length!=0) {
        pred =  [NSPredicate predicateWithFormat:@"SELF.event_code == %@",[NSString stringWithFormat:@"%d",SETTINGS_CODE]];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect Course number");
    }
    
    if (flag == 1) {
        
        NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"SuspensionSettings"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        
        if (results.count > 0) {
            NSManagedObject* suspensionsett = [results objectAtIndex:0];
            [suspensionsett setValue:[NSNumber numberWithInt:SUSPENSION_TIME] forKey:@"suspension_time"];
            [suspensionsett setValue:[NSNumber numberWithInt:SETTINGS_CODE] forKey:@"event_code"];
            [suspensionsett setValue: @"MCD" forKey:@"modify_from"];
            [suspensionsett setValue:@"0" forKey:@"updated_to_site"];
            [suspensionsett setValue:@"1" forKey:@"update_to_device"];
            
            [self.managedObjectContext save:nil];
        } else {
            NSLog(@"Enter Corect Course number");
        }
        
        
        
    }
    
    
}

-(void) onSuspensionPercentageRecieved:(Byte[]) data {
    NSString *code;
    NSMutableArray *results = [[NSMutableArray alloc]init];
    switch (data[2]) {
        case 0:
            // St
            code=[NSString stringWithFormat:@"%d",ST_SUSPENSION];
            break;
        case 1:
            // HR
            code=[NSString stringWithFormat:@"%d",HR_SUSPENSION];
            break;
        default:
            break;
    }
    
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",data[1]]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
        NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            //NSLog(@"1 - %@", doctorssett);
            [doctorssett setValue:[NSString stringWithFormat:@"%d",data[3]] forKey:@"dd"];
            [doctorssett setValue:[NSString stringWithFormat:@"%d",0] forKey:@"ddflag"];
            [doctorssett setValue:@"MCD" forKey:@"modify_from"];
            [doctorssett setValue:@"0" forKey:@"updated_to_site"];
            [doctorssett setValue:@"1" forKey:@"updated_to_device"];
            
            
            [self.managedObjectContext save:nil];
        } else {
            NSLog(@"Enter Corect code number");
        }
        
    }
}

-(void) onTachycardiaRecieved:(Byte[]) data{
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=[NSString stringWithFormat:@"%d",TACHYCARDIA];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ && %K == %@";
    NSArray* args = @[@"code",code, @"posture",[NSString stringWithFormat:@"%d",data[1]]];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
        NSFetchRequest *fetchRequest1 = [[NSFetchRequest alloc]initWithEntityName:@"ThresholdSett"];
        [fetchRequest1 setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest1 error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *genthressett = (NSManagedObject *)[results objectAtIndex:0];
            //NSLog(@"1 - %@", genthressett);
            [genthressett setValue:[NSString stringWithFormat:@"%d",data[3] << 7 | data[4]] forKey:@"yd"];
            [genthressett setValue:@"" forKey:@"rd"];
            [genthressett setValue:[NSString stringWithFormat:@"%d",data[2]] forKey:@"flag"];
            [genthressett setValue:@"MCD" forKey:@"modify_from"];
            [genthressett setValue:@"0" forKey:@"updated_to_site"];
            [genthressett setValue:@"1" forKey:@"update_to_device"];
            [self.managedObjectContext save:nil];
        } else {
            NSLog(@"Enter Corect general code number");
        }
        
    }
    
}

-(void) onFinishedGettingSettings {
    
    // MasterCaution.log.info("Finished Getting Settings");
    // Intent intent = new Intent(UploderService.ACTION_UPLOADALLSETTINGS);
    //  sendBroadcast(intent);
    
}

/* Set On Demand ECG */
-(void) OnDemandECG:(int)time {
    
    NSLog(@"On Demand");
    unsigned char msg[5] = { 0xf1,3,9,(Byte) time,0 };
    msg[4] =[self getCheckSum:msg :4];
    NSData * dataarr = [NSData dataWithBytes:msg length:5];
   [self writeData:dataarr];
}


/* Set Patient Call*/
-(void) ManualPatientCall:(int)time {
    
    NSLog(@"Patient Call");
    unsigned char msg[5] = { 0xf1,3,24,(Byte) time,0 };
    msg[4] =[self getCheckSum:msg :4];
    NSData * dataarr = [NSData dataWithBytes:msg length:5];
    [self writeData:dataarr];
    
}

/* Funciton toGet MCD Settings */
-(void) Get_MCDSettings{
    unsigned char msg[6] = { 0xf1,3,16,0,0 };
    msg[5] =[self getCheckSum:msg :5];
    NSData * dataarr = [NSData dataWithBytes:msg length:6];
   [self writeData:dataarr];
}

/* Funciton toGet Device ID */
-(void) Get_Device_ID{
    unsigned char msg[5] = { 0xF2,3,50,0,0 };
    msg[4] =[self getCheckSum:msg :4];
    NSData * dataarr = [NSData dataWithBytes:msg length:5];
    NSLog(@"ACK:%@",dataarr);
   [self writeData:dataarr];
    
}

/* Funciton toGet Firmware Details */
-(void) Get_Firmware_Details{
    NSLog(@"Firmware Details");
    unsigned char msg[5] = { 0xF2,3,51,0,0 };
    msg[4] =[self getCheckSum:msg :4];
    NSData * dataarr = [NSData dataWithBytes:msg length:5];
   [self writeData:dataarr];
    
}

/* Funciton toGet Last Threshold Values Updates Date and Time */
-(void) Get_Last_Thresh_Values {
    NSLog(@"Thresh value");
    unsigned char msg[5] = { 0xf2,3,52,0,0 };
    msg[4] =[self getCheckSum:msg :4];
    NSData * dataarr = [NSData dataWithBytes:msg length:5];
   [self writeData:dataarr];
}

/* Funciton toBuilt in Test Request */
-(void) Get_BuiltInTestREQ {
    NSLog(@"Built Test");
    unsigned char msg[5] = { 0xF2,3,53,0,0 };
    msg[4] =[self getCheckSum:msg :4];
    NSData * dataarr = [NSData dataWithBytes:msg length:5];
    //NSLog(@"%@",dataarr);
   [self writeData:dataarr];
    
}
//set mcd setting
-(void) SetLevel:(int) level {
    NSLog(@"SET level");
    unsigned char msg[5] = { 0xf2,3,177,(Byte)level,0 };
    msg[4] =[self getCheckSum:msg :4];
    NSData * dataarr = [NSData dataWithBytes:msg length:5];
    NSLog(@"ACK%@",dataarr);
   [self writeData:dataarr];
    
}

-(void) SET_GAIN_VALUE:(float) GainValue {
    NSLog(@"gain value");
    unsigned char msg[5] = { 0xf2,3,100,(Byte) GainValue,0 };
    msg[4] =[self getCheckSum:msg :4];
    NSData * dataarr = [NSData dataWithBytes:msg length:6];
   [self writeData:dataarr];
}

-(void) SET_50HZ_60HZ_FilterState:(int) state {
    NSLog(@"SET filterstate");
    unsigned char msg[5] = { 0xf2,3,101,(Byte)state,0 };
    msg[4] =[self getCheckSum:msg :4];
    NSData * dataarr = [NSData dataWithBytes:msg length:5];
   [self writeData:dataarr];
    
}

/* Funciton toSet Baseline Filter ON or OFF */
-(void) SET_BaseLine_filterState:(int) state {
    NSLog(@"SET baseline");
    unsigned char msg[5] = { 0xf2,3,102,(Byte)state,0 };
    msg[4] =[self getCheckSum:msg :4];
    NSData * dataarr = [NSData dataWithBytes:msg length:5];
   [self writeData:dataarr];
    
}

/* Funciton toSet Volume level on MCD */
-(void) SET_Vol_level:(int) value
{    NSLog(@"SET vol level");
    unsigned char msg[5] = { 0xf2,3,103,(Byte)value,0 };
    msg[4] =[self getCheckSum:msg :4];
    NSData * dataarr = [NSData dataWithBytes:msg length:5];
   [self writeData:dataarr];
    
}




/* Funciton toSet Time division intervals Pre and Post time */
-(void) SET_TimeDivIntervfal:(int) pretime_sec :(int) posttime_sec {
    NSLog(@"SET TimeDiv");
    unsigned char msg[8] = { 0xf2,6,105,(Byte)(pretime_sec >> 7),(Byte) (pretime_sec & 0x7f), (Byte) (posttime_sec >> 7),
        (Byte)(posttime_sec & 0x7f),0 };
    msg[7] =[self getCheckSum:msg :7];
    NSData * dataarr = [NSData dataWithBytes:msg length:8];
   [self writeData:dataarr];
    
}

/* Funciton toSet Respiration Callipers(Amplitude) */
-(void) SET_RespCalipers:(int) positive_axis  :(int) negative_axis {
    NSLog(@"SET RespCaliper");
    unsigned char msg[6] = { 0xf2,4,113,(Byte) positive_axis,(Byte) negative_axis,0 };
    msg[5] =[self getCheckSum:msg :5];
    NSData * dataarr = [NSData dataWithBytes:msg length:6];
   [self writeData:dataarr];
    
}

-(void) setRespiration:(int) state {
    NSLog(@"SET Respiration");
    unsigned char msg[5] = { 0xf2,3,111,(Byte)state,0 };
    msg[4] =[self getCheckSum:msg :4];
    NSData * dataarr = [NSData dataWithBytes:msg length:5];
   [self writeData:dataarr];
}

-(void)setInvertedTWave
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:YES forKey:@"InvertedTFlag"];
    [defaults synchronize];
    [self SET_SuspTime:0 :2];
}

-(void) setOrientation:(int)state {
    NSLog(@"SET Orientation");
    unsigned char msg[5] = { 0xf2,3,112,(Byte)state,0 };
    msg[4] =[self getCheckSum:msg :4];
    NSData * dataarr = [NSData dataWithBytes:msg length:5];
   [self writeData:dataarr];
}

-(void) SET_LeadSelect:(int) leadByte1 {
    // Create Lead selction logic 71/72/73/74
    NSLog(@"SET LeadSelect");
    unsigned char msg[6] = { 0xf2,4,109,(Byte)127,(Byte)leadByte1,0 };
    msg[5] =[self getCheckSum:msg :5];
    NSData * dataarr = [NSData dataWithBytes:msg length:6];
   [self writeData:dataarr];
    
}

-(void) SET_EventDetectionLead:(int) lead {
    NSLog(@"SET EventDetection");
    unsigned char msg[5] = { 0xf2,3,107,(Byte) lead,0 };
    msg[4] =[self getCheckSum:msg :4];
    NSData * dataarr = [NSData dataWithBytes:msg length:5];
   [self writeData:dataarr];
    
}

/* Funciton toSet User Configurable Sampling Rate */
-(void) SET_SamplingRate:(int) rate {
    NSLog(@"SET Sampling");
    unsigned char msg[5] = { 0xf2,3,108,(Byte) rate,0 };
    msg[4] =[self getCheckSum:msg :4];
    NSData * dataarr = [NSData dataWithBytes:msg length:5];
   [self writeData:dataarr];
    
}

/* Funciton toSet User Configurable Lead Selection in Continuous/Event Mode */
-(void) SET_UserConfigSampleRate:(int) value {
    NSLog(@"SET UserConfig");
    switch (value) {
        case 0:
        {
            unsigned char msg[5] = { 0xf1,3,1,74,0 };
            msg[4] =[self getCheckSum:msg :4];
            NSData * dataarr = [NSData dataWithBytes:msg length:5];
           [self writeData:dataarr];
        }
            break;
        case 1:
        {
            unsigned char msg[6] = { 0xf1,3,1,73,0 };
            msg[5] =[self getCheckSum:msg :5];
            NSData * dataarr = [NSData dataWithBytes:msg length:6];
           [self writeData:dataarr];
        }
            break;
        case 2:
        {
            unsigned char msg[5] = { 0xf1,3,1,72,0 };
            msg[4] =[self getCheckSum:msg :4];
            NSData * dataarr = [NSData dataWithBytes:msg length:5];
           [self writeData:dataarr];
        }
            break;
        case 3:
        {
            unsigned char msg[5] = { 0xf1,3,1,71,0 };
            msg[4] =[self getCheckSum:msg :4];
            NSData * dataarr = [NSData dataWithBytes:msg length:5];
           [self writeData:dataarr];
            break;
        }
    }
    /*
     * Byte[] outByte = { (Byte) 0xf1, 3, 23, 5, 0};
     * outByte[outByte.length-1]=(Byte)calc_chksum(outByte);
     * mConnection.sendData(outByte, SendID);
     */
}

// THRESHOLD VALUE ASIGNMENTS PROTOCOLS

/* Funciton toAssign ST Elevation Values */
-(void) SET_STelev:(int) Y_value :(int) flag :(int) posture {
    NSLog(@"SET Telev");
    unsigned char msg[7] = { 0xf2,5,154,(Byte) posture,
        (Byte) flag, (Byte) Y_value,0 };
    msg[6] =[self getCheckSum:msg :6];
    NSData * dataarr = [NSData dataWithBytes:msg length:7];
   [self writeData:dataarr];
    
}

/* Funciton toAssign ST Depression Values */
-(void) SET_Depre:(int) Y_value :(int) flag  :(int) posture {
    NSLog(@"SET Depre");
    unsigned char msg[7] = { 0xf2,5,155,(Byte) posture,
        (Byte) flag, (Byte) Y_value,0 };
    msg[6] =[self getCheckSum:msg :6];
    NSData * dataarr = [NSData dataWithBytes:msg length:7];
   [self writeData:dataarr];
    
}

/* Funciton toAssign T Wave Values */
/*
 * To set Pseudo norm Event; Data Byte 1 = 2; To set Inversion Event; Data
 * Byte 1 = 1; To set Normal Event; Data Byte 1 = 0;
 */
-(void) SET_TwaleValue:(int) flag :(int) posture {
    NSLog(@"SET TwaleValue");
    unsigned char msg[6] = { 0xf2,4,156,(Byte) posture,
        (Byte) flag,0 };
    msg[5] =[self getCheckSum:msg :5];
    NSData * dataarr = [NSData dataWithBytes:msg length:6];
   [self writeData:dataarr];
    
}

/* Funciton toAssign Q Wave abnormal Values */// REplaced for Bigemeny value
-(void) SET_BigemenyValue:(int) flag :(int) posture :(int) Y_value {
    NSLog(@"SET Bigemny");
    unsigned char msg[7] = { 0xf2,5,157,(Byte) posture,
        (Byte) flag, (Byte) Y_value,0 };
    msg[6] =[self getCheckSum:msg :6];
    NSData * dataarr = [NSData dataWithBytes:msg length:7];
   [self writeData:dataarr];
}

/* Funciton toAssign Prolonged QT Values */
-(void) SET_ProlongQT:(int) Y_value :(int) flag :(int) posture  {
    NSLog(@"SET Prolong");
    unsigned char msg[8] = { 0xf2,6,158,(Byte) posture,
        (Byte) flag, (Byte) (Y_value >> 7), (Byte) (Y_value & 0x7f),0 };
    msg[7] =[self getCheckSum:msg :7];
    NSData * dataarr = [NSData dataWithBytes:msg length:8];
   [self writeData:dataarr];
    
}

/* Funciton toAssign QRS Width Values(wide) */
-(void) SET_QRSwidthWide:(int) Y_value :( int) flag :(int) posture {
    NSLog(@"SET QRSwidthwide");
    unsigned char msg[8] = { 0xf2,6,159,(Byte) posture,
        (Byte) flag, (Byte) (Y_value >> 7), (Byte) (Y_value & 0x7f),0 };
    msg[7] =[self getCheckSum:msg :7];
    NSData * dataarr = [NSData dataWithBytes:msg length:8];
   [self writeData:dataarr];
    
}

/* Funciton toAssign Tachycardia Values(wide) */
-(void) SET_Tachycardia:(int) Y_value :(int) flag :(int) posture {
    NSLog(@"SET Tachycardia");
    unsigned char msg[8] = { 0xf2,6,179,(Byte) posture,
        (Byte) flag, (Byte) (Y_value >> 7), (Byte) (Y_value & 0x7f),0 };
    msg[7] =[self getCheckSum:msg :7];
    NSData * dataarr = [NSData dataWithBytes:msg length:8];
   [self writeData:dataarr];
}

-(void) SET_SuspensionPercent:(int) code :(int) percent :(int) posture {
    NSLog(@"SET SuspensionPercent");
    unsigned char msg[7] = { 0xf2,5,175,(Byte) posture,
        (Byte) code, (Byte)percent,0 };
    msg[6] =[self getCheckSum:msg :6];
    NSData * dataarr = [NSData dataWithBytes:msg length:7];
   [self writeData:dataarr];
}

/* Funciton toAssign QRS Width Values(narrow) */
-(void) SET_QRSwidthNarow:(int) Y_value :(int) flag :(int) posture {
    NSLog(@"SET QRSwidthNarrow");
    unsigned char msg[8] = { 0xf2,6,170,(Byte) posture,
        (Byte) flag, (Byte) (Y_value >> 7), (Byte) (Y_value & 0x7f),0 };
    msg[7] =[self getCheckSum:msg :7];
    NSData * dataarr = [NSData dataWithBytes:msg length:8];
   [self writeData:dataarr];
    
}

/* Funciton toAssign Ventricular Tachycardia (Narrow QRS) Values */
-(void) SET_VentricularTachyNarrow:(int) Y1_value :(int) Y2_value :(int) flag :(int) posture {
    NSLog(@"SET VentricularTachNarrow");
    unsigned char msg[14] = { 0xf2,12,160,(Byte) posture,
        (Byte) flag, (Byte) (Y1_value >> 7), (Byte) (Y1_value & 0x7f),(Byte)(Y1_value >> 7), (Byte) (Y1_value & 0x7f),
        (Byte) (Y2_value >> 7), (Byte) (Y2_value & 0x7f),
        (Byte) (Y2_value >> 7), (Byte) (Y2_value & 0x7f),0 };
    msg[13] =[self getCheckSum:msg :13];
    NSData * dataarr = [NSData dataWithBytes:msg length:14];
    NSLog(@"venack:%@",dataarr);
   [self writeData:dataarr];
    
}

/* Funciton toAssign Ventricular Tachycardia (Wide QRS) Values */
-(void) SET_VentricularTachyWide:(int) Y1_value :(int) Y2_value :(int) flag
                                :(int) posture {
    NSLog(@"SET VedntricularTachywide");
    unsigned char msg[14] = { 0xf2,12,161,(Byte) posture,
        (Byte) flag, (Byte) (Y1_value >> 7), (Byte) (Y1_value & 0x7f),(Byte)(Y1_value >> 7), (Byte) (Y1_value & 0x7f),
        (Byte) (Y2_value >> 7), (Byte) (Y2_value & 0x7f),
        (Byte) (Y2_value >> 7), (Byte) (Y2_value & 0x7f),0};
    msg[13] =[self getCheckSum:msg :13];
    NSData * dataarr = [NSData dataWithBytes:msg length:14];
   [self writeData:dataarr];
    
}

/* Funciton toAssign PVC Values */
-(void) SET_PVC:(int) active_flag :(int) Y_value :(int )posture {
    NSLog(@"SET PVC");
    unsigned char msg[7] = { 0xf2,5,162,(Byte) posture,
        (Byte) active_flag, (Byte) (Y_value),0 };
    msg[6] =[self getCheckSum:msg :6];
    NSData * dataarr = [NSData dataWithBytes:msg length:7];
   [self writeData:dataarr];
    
}

/* Funciton toAssign Asystole Values */
-(void) SET_Asystole:(int) active_flag :(int) Y_value :(int) posture {
    NSLog(@"SET Asystole");
    unsigned char msg[7] = { 0xf2,5,163,(Byte) posture,
        (Byte) active_flag, (Byte) (Y_value),0 };
    msg[6] =[self getCheckSum:msg :6];
    NSData * dataarr = [NSData dataWithBytes:msg length:7];
   [self writeData:dataarr];
    
}

/* Funciton toAssign Bradicardia Values */
-(void) SET_Bradycardia:(int) Y1_value :(int) Y2_value :(int) flag
                       :(int )posture {
    NSLog(@"SET BRadycardia");
    unsigned char msg[14] = { 0xf2,12,164,(Byte) posture,
        (Byte) flag, (Byte) (Y1_value >> 7), (Byte) (Y1_value & 0x7f),(Byte)(Y1_value >> 7), (Byte) (Y1_value & 0x7f),
        (Byte) (Y2_value >> 7), (Byte) (Y2_value & 0x7f),
        (Byte) (Y2_value >> 7), (Byte) (Y2_value & 0x7f),0 };
    msg[13] =[self getCheckSum:msg :13];
    NSData * dataarr = [NSData dataWithBytes:msg length:14];
   [self writeData:dataarr];
    
}

/* Funciton toAssign Apnea Values */
-(void) SET_Apnea:(int) active_flag :(int) Y_value :(int)posture {
    NSLog(@"SET Apnea");
    unsigned char msg[7] = { 0xf2,5,165,(Byte) posture,
        (Byte) active_flag, (Byte) (Y_value),0 };
    msg[6] =[self getCheckSum:msg :6];
    NSData * dataarr = [NSData dataWithBytes:msg length:7];
   [self writeData:dataarr];
    
}

/* Funciton toAssign temperature*/
-(void) SET_Temperature:(int) _beforeFraction :(int) _afterFraction
                       :(int) flag :(int) posture{
    NSLog(@"SET Temperature");
    unsigned char msg[9] = { 0xf2,7,166,(Byte) posture,
        (Byte) flag,(Byte) (_beforeFraction >> 7),
        (Byte) (_beforeFraction & 0x7f), (Byte) _afterFraction,0 };
    msg[8] =[self getCheckSum:msg :8];
    NSData * dataarr = [NSData dataWithBytes:msg length:9];
   [self writeData:dataarr];
    
}

/* Funciton toAssign Fall Event Values */
-(void) SET_FallEvent:(int) active_flag :(int) posture {
    NSLog(@"SET FallEvent");
    unsigned char msg[6] = { 0xf2,4,167,(Byte) posture,
        (Byte) active_flag,0 };
    msg[5] =[self getCheckSum:msg :5];
    NSData * dataarr = [NSData dataWithBytes:msg length:6];
   [self writeData:dataarr];
    
}



/* Funciton toAssign No Motion Time Values */
-(void) SET_NoMotion :(int)minutes{
    NSLog(@"SET NoMotion");
    unsigned char msg[5] = { 0xf2,3,168,(Byte) minutes,0 };
    msg[4] =[self getCheckSum:msg :4];
    NSData * dataarr = [NSData dataWithBytes:msg length:5];
   [self writeData:dataarr];
    
}

/* Funciton toAssign Respiration Rate Values */
-(void) SET_resperationRate :(int) high_low :(int) Y_value :(int) posture
                            :(int) flag {
    NSLog(@"SET RespirationRate");
    unsigned char msg[8] = { 0xf2,6,169,(Byte) posture,
        (Byte) flag, (Byte) high_low, (Byte) (Y_value),0 };
    msg[7] =[self getCheckSum:msg :7];
    NSData * dataarr = [NSData dataWithBytes:msg length:8];
   [self writeData:dataarr];
    
}

/* Funciton toAssign PAUS Values(In Sec) */
-(void) SET_PauseValue:(int) Y_value :(int) flag :(int) posture {
    NSLog(@"SET PauseValue");
    unsigned char msg[7] = { 0xf2,5,171,(Byte) posture,
        (Byte) flag, (Byte) (Y_value),0 };
    msg[6] =[self getCheckSum:msg :6];
    NSData * dataarr = [NSData dataWithBytes:msg length:7];
   [self writeData:dataarr];
    
}



/* Funciton toSet Mobile detection time interval */
-(void) SET_MobDetectionTimeInterval:(int) timevalue {
    
    
    if(timevalue > 4)
        timevalue = 4;
    unsigned char msg[6] = { 0xf2,4,110,(Byte) (timevalue >> 7),
        (Byte) (timevalue & 0x7f),0 };
    msg[5] =[self getCheckSum:msg :5];
    NSData * dataarr = [NSData dataWithBytes:msg length:6];
    NSLog(@"MobTimeInterval");
   [self writeData:dataarr];
    
}

/* Funciton toKeep Alive ACK to MCD */
-(void) SET_KeepAlive {
    unsigned char msg[5] = { 0xf1,3,1,6,0 };
    msg[4] =[self getCheckSum:msg :4];
    NSData * dataarr = [NSData dataWithBytes:msg length:5];
   [self writeData:dataarr];
    
    
}



-(void) SetMCDSettings:(int) filter50_60 :(int) baselinefilter :
(int) detectionlead :(int) gainsettings :(int) samplingrate
                      :(int) pretime_sec :(int) posttime_sec :(int) volumelevel :(int) leadconfig
                      :(int) respiration :(int) orientation {
    NSLog(@"MCD Setting");
    unsigned char msg[21] = { 0xf1,19,22,(Byte) filter50_60,
        (Byte) baselinefilter, (Byte) detectionlead,
        (Byte) gainsettings, 127, (Byte) leadconfig, 0, 0, 0,
        (Byte) samplingrate, (Byte) (pretime_sec >> 7),
        (Byte) (pretime_sec & 0x7f), (Byte) (posttime_sec >> 7),
        (Byte) (posttime_sec & 0x7f), (Byte) volumelevel,
        (Byte) respiration, (Byte) orientation,0 };
    msg[20] =[self getCheckSum:msg :20];
    NSData * dataarr = [NSData dataWithBytes:msg length:21];
    NSLog(@"ACK%@",dataarr);
   [self writeData:dataarr];
    
    
}

// Abhi added 4 new values Set functions - start // bigemeny is used
// replacing pereveious Q wave code

-(void) SET_TrigemenyStatus:(int) posture :(int) flag :(int) Y_value {
    NSLog(@"SET Trigeminystatus");
    unsigned char msg[7] = { 0xf2,5,172,(Byte) posture,
        (Byte) flag, (Byte) Y_value,0 };
    msg[6] =[self getCheckSum:msg :6];
    NSData * dataarr = [NSData dataWithBytes:msg length:7];
   [self writeData:dataarr];
    
    
}

-(void) SET_CoupletStatus:(int) posture :(int) flag :(int) Y_value{
    NSLog(@"SET CoupletStatus");
    unsigned char msg[7] = { 0xf2,5,173,(Byte) posture,
        (Byte) flag, (Byte) Y_value,0 };
    msg[6] =[self getCheckSum:msg :6];
    NSData * dataarr = [NSData dataWithBytes:msg length:7];
   [self writeData:dataarr];
}

-(void) SET_TripletStatus:(int) posture :(int)flag :(int) Y_value {
    NSLog(@"SET TripleStatus");
    unsigned char msg[7] = { 0xf2,5,174,(Byte) posture,
        (Byte) flag, (Byte) Y_value,0 };
    msg[6] =[self getCheckSum:msg :6];
    NSData * dataarr = [NSData dataWithBytes:msg length:7];
    [self writeData:dataarr];
    
}


-(void) SET_SuspTime:(int) Y_value :(int) SetingCode{
    NSLog(@"SET SuspTime");
    unsigned char msg[7] = { 0xf2,5,176,(Byte) SetingCode,
        (Byte) (Y_value >> 7), (Byte) (Y_value & 0x7f),0 };
    msg[6] =[self getCheckSum:msg :6];
    NSData * dataarr = [NSData dataWithBytes:msg length:7];
    [self writeData:dataarr];
    
}

- (void)sendBTEventNotificationWithEventOccured:(BOOL)isEventStatus {
    NSDictionary *connectionDetails = @{@"EventOccured": @(isEventStatus)};
    [[NSNotificationCenter defaultCenter] postNotificationName:RWT_EVENT_STATUS_NOTIFICATION object:self userInfo:connectionDetails];
    
    
}

- (void)sendBTEventNotificationWithNumberOfEventOccured:(BOOL)isEventStatus {
    NSDictionary *connectionDetails = @{@"NoEventOccured": @(isEventStatus)};
    [[NSNotificationCenter defaultCenter] postNotificationName:RWT_EVENT_STATUS_NOTIFICATION object:self userInfo:connectionDetails];
}


@end
