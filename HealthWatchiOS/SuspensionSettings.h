//
//  SuspensionSettings.h
//  HealthWatch
//
//  Created by METSL MAC MINI on 31/07/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface SuspensionSettings : NSManagedObject

@property (nonatomic, retain) NSString * created_date_time;
@property (nonatomic, retain) NSString * created_from;
@property (nonatomic, retain) NSString * device_id;
@property (nonatomic, retain) NSNumber * event_code;
@property (nonatomic, retain) NSString * modify_datetime;
@property (nonatomic, retain) NSString * modify_from;
@property (nonatomic, retain) NSString * patient_id;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSNumber * suspension_time;
@property (nonatomic, retain) NSString * update_to_device;
@property (nonatomic, retain) NSString * updated_to_site;

@end
