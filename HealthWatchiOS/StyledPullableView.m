
#import "StyledPullableView.h"



@implementation StyledPullableView

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        
        UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"blue_footer_bg.png"]];
        imgView.frame = CGRectMake(0, 0, 320, 460);
        [self addSubview:imgView];
        
    }
    return self;
}

@end
