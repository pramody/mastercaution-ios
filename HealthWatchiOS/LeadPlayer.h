//
//  ViewController.m
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 12/06/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//
#import <UIKit/UIKit.h>

@class LiveEcgMonitor;


@interface LeadPlayer : UIView <UIGestureRecognizerDelegate> {
	CGPoint drawingPoints[1000];
	CGPoint endPoint, endPoint2, endPoint3, viewCenter;
	int currentPoint;
	CGContextRef context;
	
	LiveEcgMonitor *__unsafe_unretained liveMonitor;
	
	NSMutableArray *pointsArray;
    
	int index;
	NSString *label;
	
	int count;
	UIView *lightSpot;
	int pos_x_offset;
    UIImage *incrementalImage;
    UIImageView *tempDrawImage;
    CGContextRef myBitmapContext;
    
   
}

@property (nonatomic, strong) NSMutableArray *pointsArray;
@property (nonatomic, strong) UIView *lightSpot;
@property (nonatomic, strong) NSString *label;
@property (nonatomic, unsafe_unretained) LiveEcgMonitor *liveMonitor;

@property (nonatomic) int index;
@property (nonatomic) int currentPoint;
@property (nonatomic) int pos_x_offset;

@property (nonatomic) CGPoint viewCenter;

@property (nonatomic) UIColor *leadcolor;


- (void)fireDrawing :(float)cur_x;
- (void)fireDrawing1;
- (void)fireDrawing;
- (void)drawGrid:(CGContextRef)ctx;
- (void)drawCurve:(CGContextRef)ctx;
- (void)drawLabel:(CGContextRef)ctx;
- (void)clearDrawing;
- (void)redraw;
- (CGFloat)getPosX:(int)tick;
- (BOOL)pointAvailable:(NSInteger)pointIndex;
- (void)resetBuffer;
- (void)addGestureRecgonizer;

@end
