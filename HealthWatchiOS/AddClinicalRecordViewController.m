//
//  AddClinicalRecordViewController.m
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 01/07/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import "AddClinicalRecordViewController.h"

#import "EventStorage.h"
#import "WCFUpload.h"


@interface AddClinicalRecordViewController () 

@end

@implementation AddClinicalRecordViewController
{
    
}
@synthesize headerBlueView,mainScrollView,systolicTextBox,diastolicTextBox,oximetryTextBox,weightTextBox,glucoseTextBox,totalBmiLbl,clinicalDataArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    AppDelegate * appDelegate = (AppDelegate *)UIApplication.sharedApplication.delegate ;
    appDelegate.orientationKey = @"PRO";
    [appDelegate forceOrientationTakePlace:appDelegate.orientationKey];

     clinicalDataArray =[[NSMutableArray alloc]initWithObjects:@"", @"", @"", @"", @"", @"",nil];
  
    
    self.systolicTextBox.delegate=self;
    self.diastolicTextBox.delegate=self;
    self.glucoseTextBox.delegate=self;
    
    // ------------------------------------------- First time Sliding view Setups START  -------------------------------------------
    
    
    self.slidingViewController.topViewAnchoredGesture = ECSlidingViewControllerAnchoredGestureTapping | ECSlidingViewControllerAnchoredGesturePanning;
    self.slidingViewController.customAnchoredGestures =@[];
    
    [self setupInitialValue];
    [self fetchingpatientdata];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
    BOOL savedSwitch= [[NSUserDefaults standardUserDefaults] boolForKey:@"SwitchState"];
    if(savedSwitch)
    {
        
        [[LocationHandler getSharedInstance]setDelegate:self];
        [[LocationHandler getSharedInstance]startUpdating];
    }
    else
    {
        latitude=@"0";
        longititude=@"0";
        location=[NSString stringWithFormat:@"0,0"];
    }


    

}

-(void)setupInitialValue
{
    blueColorMainAppUIColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"blue_bg_full.png"]];
     backGroundGrayColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"common_bg_gray_for_all.png"]];
   
    headerBlueView.backgroundColor = blueColorMainAppUIColor;
    self.mainScrollView.backgroundColor   =   UIColor.clearColor;
    self.view.backgroundColor             =   backGroundGrayColor;
    
    
    CGFloat borderWidth = 1.5;
    systolicTextBox.layer.borderWidth     =   borderWidth;
    systolicTextBox.layer.borderColor     =   UIColor.lightGrayColor.CGColor;
    
    diastolicTextBox.layer.borderWidth    =   borderWidth;
    diastolicTextBox.layer.borderColor    =   UIColor.lightGrayColor.CGColor;
    
    oximetryTextBox.layer.borderWidth     =   borderWidth;
    oximetryTextBox.layer.borderColor     =   UIColor.lightGrayColor.CGColor;
    
    weightTextBox.layer.borderWidth       =   borderWidth;
    weightTextBox.layer.borderColor       =   UIColor.lightGrayColor.CGColor;
    
    glucoseTextBox.layer.borderWidth      =   borderWidth;
    glucoseTextBox.layer.borderColor      =   UIColor.lightGrayColor.CGColor;
    
    
    
    
}


-(IBAction)goToDashboard:(id)sender{
    self.slidingViewController.topViewController.view.layer.transform = CATransform3DMakeScale(1, 1, 1);
    self.slidingViewController.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"DashboardStoryboardId"];
}

-(IBAction)leftSideButtonAction:(id)sender{
    [self.slidingViewController anchorTopViewToRightAnimated:YES];
}


-(void)dismissKeyboard {
    
     [self.view endEditing:YES];
    
}




-(void) hideKeyboard
{
    [self.view endEditing:YES];
   
}



-(IBAction)applyButtonAction:(id)sender
 {
    
    //println("Check validation and Save data")
    // Hide keryboard
     [self hideKeyboard];
     GlobalVars *globals = [GlobalVars sharedInstance];
     BOOL validationIs = [self clinicalProfileValidation];
    if (validationIs == true)
    {
        if((systolicTextBox.text.length!=0)||(diastolicTextBox.text.length!=0)||glucoseTextBox.text.length!=0||weightTextBox.text.length!=0||oximetryTextBox.text.length!=0)
        {
            BOOL val = true;
         if(systolicTextBox.text.length!=0)
         {
              val=diastolicTextBox.text.length!=0? true :false;
         }
         if(diastolicTextBox.text.length!=0)
         {
             val=systolicTextBox.text.length!=0? true :false;
         }
            
        if(val==true)
        {
        [self startLoad];
         NSLogger *logger=[[NSLogger alloc]init];
        NSDate *date = [[NSDate alloc] init];
        NSLog(@"%@", date);
        
        CommonHelper * commhelp=[[CommonHelper alloc] init];
       // NSString *Eventdttime = [commhelp getDateInYourFormat:@"yyyy-MM-dd" :@"yyyy-MM-dd HH:mm:ss" :DateAsNSDate :@"2014-04-25" :date];
         NSString *Eventdttime = [commhelp getDateUTC:@"yyyy-MM-dd" :@"yyyy-MM-dd HH:mm:ss" :DateAsNSDate :@"2014-04-25" :date];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"MCDSettings" inManagedObjectContext:self.managedObjectContext];
        [fetchRequest setEntity:entity];
        
        NSError *error = nil;
        NSString *samplingrate;
        NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
        
        if (error) {
            NSLog(@"Unable to execute fetch request.");
            NSLog(@"%@, %@", error, error.localizedDescription);
            
           
            logger.degbugger = true;
            [logger log:@"Coredata Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:[error localizedDescription], @"Adding clinical record", nil] error:TRUE];
            
        } else {
            NSLog(@"%@", result);
            if (result.count > 0) {
                  NSManagedObject *mcdsett = (NSManagedObject *)[result objectAtIndex:0];
                samplingrate=[mcdsett valueForKey:@"sampling_rate"];
            }
        }
        
        NSString *Event_McdLead;
       
            Event_McdLead=@"0";
               
         NSString * EVENT_TEMPERATURE = @"0";
         NSString *EVENT_ECG_FILEPATH = @"";
         NSString *EVENT_LEAD_DETECTION =@"0";
         NSString *EVENT_POSTURE_CODE = @"0";
         NSString *EVENT_REASON = @"Manual";
         NSString *EVENT_VALUE = @"0";
         NSString *EVENT_RECIEVE_COMPLETE = @"1";
        // NSString *EVENT_ECG_ID =@"";
         NSString *EVENT_HEARTRATE =@"0";
         NSString *EVENT_RESPIRATION = @"0";
         NSString *EVENT_TYPE = @"2";
         NSString *EVENT_RR_INTERVAL =@"0";
         NSString *EVENT_RESP_VARIANCE = @"0";
         NSString *Event_QRSInterval=@"0";
         NSString *Event_TWave=@"0";
         NSString *Event_RespirationAmp=@"0";
         NSString *Event_QTC=@"0";
         NSString *Event_PRInterval=@"0";
        // NSString *Event_RR=@"0";
         NSString *Event_QT=@"0";
         //NSString *Event_RespirationStatus=@"0";
        // NSString *Event_SelectedLead=@"0";
        int EVENT_CODE = OTHER_MANUAL_EVENT;

        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        BOOL standalone_Mode = [defaults boolForKey:@"ActiveStandalonemode"];
        
        if(!standalone_Mode)
        {
            
            EventStorage *Eventsettings = [NSEntityDescription insertNewObjectForEntityForName:@"EventStorage"
                                                                        inManagedObjectContext:self.managedObjectContext];
            Eventsettings.event_datetime=Eventdttime;
            Eventsettings.event_ecgfilepath= EVENT_ECG_FILEPATH ;
            Eventsettings.event_ecg_id=@"0";
            Eventsettings.event_glucose=glucoseTextBox.text;
            Eventsettings.event_latitude=globals.latitude;
            Eventsettings.event_longitude =globals.longititude;
            Eventsettings.event_reason =EVENT_REASON;
            Eventsettings.event_temp =EVENT_TEMPERATURE;
            Eventsettings.event_value =EVENT_VALUE;
            Eventsettings.event_weight =weightTextBox.text;
            Eventsettings.event_bpdia =diastolicTextBox.text;
            Eventsettings.event_bpsys =systolicTextBox.text;
            Eventsettings.event_code =[NSNumber numberWithInt:EVENT_CODE];
            Eventsettings.event_heartrate =EVENT_HEARTRATE;
            Eventsettings.event_lead_config =@"0";
            Eventsettings.event_lead_detection =EVENT_LEAD_DETECTION;
            Eventsettings.event_posture_code =EVENT_POSTURE_CODE;
            Eventsettings.event_pr_interval =Event_PRInterval;
            Eventsettings.event_qrs_interval =Event_QRSInterval;
            Eventsettings.event_qtc_interval =Event_QTC;
            Eventsettings.event_qt_interval =Event_QT;
            Eventsettings.event_receive_complete =EVENT_RECIEVE_COMPLETE;
            Eventsettings.event_respiration =EVENT_RESPIRATION;
            Eventsettings.event_respiration_amplitude =Event_RespirationAmp;
            Eventsettings.event_respiration_onoff =@"0";
            Eventsettings.event_resp_variance =EVENT_RESP_VARIANCE;
            Eventsettings.event_rr_interval =EVENT_RR_INTERVAL;
            Eventsettings.event_sampling_rate =samplingrate;
            Eventsettings.event_thresholdtype =@"0";
            Eventsettings.event_type =EVENT_TYPE;
            Eventsettings.event_t_wave =Event_TWave;
            Eventsettings.event_updated_to_device =@"0";
            Eventsettings.event_mcdlead=Event_McdLead;
            Eventsettings.event_mail_sms=@"0";
            Eventsettings.event_oximetry=oximetryTextBox.text;
            
            [self.managedObjectContext save:nil];
       
            WCFUpload *wcfcall=[[WCFUpload alloc]init];
            [wcfcall UploadEventHeader1:Eventdttime :[NSString stringWithFormat:@"%d",EVENT_CODE]];
     }
       else
       {
           EventStorage *Eventsettings = [NSEntityDescription insertNewObjectForEntityForName:@"EventStorage"
                                                                       inManagedObjectContext:self.managedObjectContext];
           Eventsettings.event_datetime=Eventdttime;
           Eventsettings.event_ecgfilepath= EVENT_ECG_FILEPATH ;
           Eventsettings.event_ecg_id=@"";
           Eventsettings.event_glucose=glucoseTextBox.text;
           Eventsettings.event_latitude=globals.latitude;
           Eventsettings.event_longitude =globals.longititude;
           Eventsettings.event_reason =EVENT_REASON;
           Eventsettings.event_temp =EVENT_TEMPERATURE;
           Eventsettings.event_value =EVENT_VALUE;
           Eventsettings.event_weight =weightTextBox.text;
           Eventsettings.event_bpdia =diastolicTextBox.text;
           Eventsettings.event_bpsys =systolicTextBox.text;
           Eventsettings.event_code =[NSNumber numberWithInt:EVENT_CODE];
           Eventsettings.event_heartrate =EVENT_HEARTRATE;
           Eventsettings.event_lead_config =@"0";
           Eventsettings.event_lead_detection =EVENT_LEAD_DETECTION;
           Eventsettings.event_posture_code =EVENT_POSTURE_CODE;
           Eventsettings.event_pr_interval =Event_PRInterval;
           Eventsettings.event_qrs_interval =Event_QRSInterval;
           Eventsettings.event_qtc_interval =Event_QTC;
           Eventsettings.event_qt_interval =Event_QT;
           Eventsettings.event_receive_complete =EVENT_RECIEVE_COMPLETE;
           Eventsettings.event_respiration =EVENT_RESPIRATION;
           Eventsettings.event_respiration_amplitude =Event_RespirationAmp;
           Eventsettings.event_respiration_onoff =@"0";
           Eventsettings.event_resp_variance =EVENT_RESP_VARIANCE;
           Eventsettings.event_rr_interval =EVENT_RR_INTERVAL;
           Eventsettings.event_sampling_rate =samplingrate;
           Eventsettings.event_thresholdtype =@"0";
           Eventsettings.event_type =EVENT_TYPE;
           Eventsettings.event_t_wave =Event_TWave;
           Eventsettings.event_updated_to_device =@"0";
           Eventsettings.event_mcdlead=Event_McdLead;
           Eventsettings.event_mail_sms=@"0";
            Eventsettings.event_oximetry=oximetryTextBox.text;
           [self.managedObjectContext save:nil];
          
       }
     
        [self stopLoad];
        [self showAlertView:@"" msg: @"Clinical data was added successfully"];
        
        self.slidingViewController.topViewController.view.layer.transform = CATransform3DMakeScale(1, 1, 1);
        self.slidingViewController.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"clinical_records_storyboard_id"];
        [self.slidingViewController resetTopViewAnimated:true];
        }
         else
         {
             [self showAlertView:@"" msg: @"Please enter both diastolic and systolic value "];

         }
        }
        else
        {
            [self showAlertView:@"" msg: @"Clinical data is empty"];
        }
    }
     

}

//location
-(void)didUpdateToLocation:(CLLocation *)newLocation
              fromLocation:(CLLocation *)oldLocation{
    //NSLog(@"%@",[NSString stringWithFormat:@"Latitude: %f",newLocation.coordinate.latitude]);
    //NSLog(@"%@",[NSString stringWithFormat:@"Longitude: %f",newLocation.coordinate.longitude]);
    GlobalVars *globals = [GlobalVars sharedInstance];
    globals.latitude  = [NSString stringWithFormat:@"%.8f",newLocation.coordinate.latitude];
    globals.longititude = [NSString stringWithFormat:@"%.8f", newLocation.coordinate.longitude];
    
}



-(void)viewWillDisappear:(BOOL)animated
{
    [self.view endEditing:YES]; // added this in for case when keyboard was already on screen
  
}

// UITEXT-FILED DELGATES


-(BOOL) textFieldShouldEndEditing:(UITextField *)textField {
    //println("Should End Editing \(textField.text)")
    
    BOOL isValidData=true;
    
    CommonHelper *comphelp=[[CommonHelper alloc] init];
    NSString * inputValue  = [comphelp trimTheString:textField.text];
    
    if (systolicTextBox == textField)
    {
        // Remove trim if any
        systolicTextBox.text = inputValue;
         [clinicalDataArray replaceObjectAtIndex:0 withObject:inputValue];
        
        //}
    }
    else if (diastolicTextBox == textField)
    {
        // Remove trim if any
        diastolicTextBox.text = inputValue;
        
          [clinicalDataArray replaceObjectAtIndex:1 withObject:inputValue];
        //}
        
    }
    else if (oximetryTextBox == textField)
    {
        // Remove trim if any
        oximetryTextBox.text = inputValue;
          [clinicalDataArray replaceObjectAtIndex:2 withObject:inputValue];
        
        
        //}
        
        
    }
    else if (weightTextBox == textField)
    {
        //println("userWeightEnterAsStr = \(inputValue)")
        int charLength                  =   (int)inputValue.length;
        //println("charLength = \(charLength)")
        int inputWeightInt;
        
        inputWeightInt = (charLength > 0 && charLength <= 3 ) ? [(inputValue) intValue] : 0;
        
        if ( inputWeightInt != 0 )
        {
            // Calculate BMI and Add to Label
            
            
            int heightFeets       =5 ;//[clinicalDataArray[3] intValue]; // at 3rd index we have Height in Feets
            int heightInches       =10;// [clinicalDataArray[4] intValue]; // at 4th index we have Height in Inches
            
           
            
               [clinicalDataArray replaceObjectAtIndex:3 withObject:[NSString stringWithFormat:@"%d",inputWeightInt]];
            [self calculateBmiAndUpdateLabelForEditView:heightFeets : heightInches  : inputWeightInt];
        }
        else
        {
            //self.showAlertView(validationErrorTitle, message: weightErrorBodyMessage)
            //isValidData = false
            [clinicalDataArray replaceObjectAtIndex:3 withObject:[NSString stringWithFormat:@"%d",inputWeightInt]];
            
        }
        
    }
    else if (glucoseTextBox == textField)
    {
        
        //self.showAlertView(validationErrorTitle, message: weightErrorBodyMessage)
        //isValidData = false
        
           [clinicalDataArray replaceObjectAtIndex:5 withObject:inputValue];
    }
    return isValidData;
}




-(void) calculateBmiAndUpdateLabelForEditView:(int)feets :(int)inches :(int)weight
{
    if (feets > 0 && weight > 0)
    {   CommonHelper * commhelp=[[CommonHelper alloc] init];
        NSString *bmiValue = [commhelp getBmiValueForHeightInFeetInches:feets :inches :weight];
        if (bmiValue.length > 0)
        {
             [clinicalDataArray replaceObjectAtIndex:4 withObject:bmiValue];
            totalBmiLbl.text       = clinicalDataArray[4];
        }
    }
    
    
}


-(BOOL) clinicalProfileValidation
{
    BOOL returnValue =   true;
    NSString * titleTEXT       =   @"Adding Clinical Validation Alert";
    NSString * messageTEXT     =   @"Invalid inputs";
    
    
    if (clinicalDataArray.count > 0)
    {
        int arrayLoopCount = (int)clinicalDataArray.count;
        
        for (int i = 0; i < arrayLoopCount; i++)
        {
            //println("i == \(i)")
            CommonHelper *comphelp=[[CommonHelper alloc] init];
            NSString * value  = [comphelp trimTheString:clinicalDataArray[i]];
            
            if (i == 0) // 0 = SYSTOLIC
            {
                if (  value.length > 3 || value.integerValue>300)
                {
                returnValue     =   false;
                messageTEXT     =   @"Systolic should be between 1 to 300!";
                break;
                }
                
            }
            else if (i == 1) // 1 = DIASTOLIC
            {
                if ( value.length > 3 || value.integerValue>300)
                {
                returnValue     =   false;
                messageTEXT     =   @"Diastolic should be between 1 to 300!";
                break;
                }
                if([systolicTextBox.text intValue] <[diastolicTextBox.text intValue])
                {
                    returnValue     =   false;
                    messageTEXT     =   @"Diastolic cannot be greater than Systolic";
                    break;
                }
            }
            else if (i == 2) // 2 = OXIMETRY
            {
                if ( value.length > 3 || value.integerValue>100)
                {
                returnValue     =   false;
                messageTEXT     =   @"Oximetry should be between 1 to 100!";
                break;
                }
            }
            else if (i == 3) // 3 = WEIGHT
            {
                 if ( value.length > 3)
                 {
                     returnValue =   false;
                     messageTEXT     =   @"Weight should be between 1 to 999!";
                     break;
                 }
            }
            else if ( i == 5 )
            {
                if (value.length > 3  || value.integerValue>200)
                {
                returnValue =   false;
                messageTEXT     =   @"Glucose should be between 1 to 200!";
                break;
                }
                
            }
            
        }
        
    } // IF ENDS HERE
    
    NSLog(@"%@", clinicalDataArray);
    if(returnValue == false)
    {
         [self showAlertView:titleTEXT  msg: messageTEXT];
    }
    
    return returnValue;
}


-(void) showAlertView:(NSString *)title  msg:(NSString *) message
{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

-(void)fetchingpatientdata
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"PatientDetails" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        NSLog(@"%@", result);
        if (result.count > 0) {
            NSManagedObject *patient = (NSManagedObject *)[result objectAtIndex:0];
            NSLog(@"1 - %@", patient);
            patientid=[patient valueForKey:@"patient_id"];
            deviceid=[patient valueForKey:@"device_id"];
        }
    }
}

- (BOOL) textField: (UITextField *)theTextField shouldChangeCharactersInRange:(NSRange)range replacementString: (NSString *)string {
    //return yes or no after comparing the characters
    
    // allow backspace
    if (!string.length)
    {
        return YES;
    }
    
    if(theTextField&&range.location==0)
    {
    if ([string hasPrefix:@"0"])
    {
        return NO;
    }
    }
    
    if(theTextField.text.length == 3)
        return NO;
           // allow digit 0 to 9
    /*if ([string intValue])
    {
        return YES;
    }*/
    
    return YES;
}

- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}


-(void) startLoad
{
    [self.spinnerView startLoading:self.indicatorMainBlockView];
}

-(void) stopLoad
{
    [self.spinnerView stopLoading:self.indicatorMainBlockView];
}


@end
