//
//  DoctorProfileViewController.h
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 01/07/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//


enum
{
 DOCTORID    =   0,
 FIRSTNAME   =   1,
 LASTNAME    =   2,
 MOBILE      =   3,
 PHONE       =   4,
 EMAIL       =   5,
 ADDRESS     =   6
    
};typedef NSInteger validFields;

#import <MessageUI/MessageUI.h>
@interface DoctorProfileViewController : UIViewController<UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate,MFMailComposeViewControllerDelegate>
{
    UIColor *backGroundGrayColor;
    UIColor *blueColorMainAppUIColor;
     NSArray *CountryDicArray;
    
    BOOL DocNameFlag;
    BOOL MobFlag;
    BOOL DegreeFlag;
    BOOL EmailFlag;
    BOOL AddressFlag;
     BOOL DocIDFlag;
   
}




// IBOutlet
@property (nonatomic, strong) IBOutlet UIView * headerBlueView;
@property (nonatomic, strong) IBOutlet UIView * parentMainView;
@property (nonatomic, strong) IBOutlet UILabel * headerTitleLbl;
@property (nonatomic, strong) IBOutlet UIButton * applyButton;

@property (nonatomic, strong) IBOutlet UIButton * editButton;



// - ReadyOnly
@property (nonatomic, strong) IBOutlet UIImageView *  doctorImageReadyOnly;
@property (nonatomic, strong) IBOutlet UIView *  ReadOnlyMainView;
@property (nonatomic, strong) IBOutlet UILabel * firstNameLabel ;
@property (nonatomic, strong) IBOutlet UILabel * lastNameLabel ;
@property (nonatomic, strong) IBOutlet UILabel * doctorUniqueIdLabel ;
@property (nonatomic, strong) IBOutlet UILabel * mobileNoLbl ;
@property (nonatomic, strong) IBOutlet UILabel * degreeLbl;
@property (nonatomic, strong) IBOutlet UILabel * emailIdLbl ;
@property (nonatomic, strong) IBOutlet UILabel * addressLbl ;


// - Edit
@property (nonatomic, strong) IBOutlet UIImageView * doctorImageEdit;
@property (nonatomic, strong) IBOutlet UIView * EditMainView;
@property (nonatomic, strong) IBOutlet UITextField * doctorUniqueIdEditLabel;
@property (nonatomic, strong) IBOutlet UITextField * DocNameText;
@property (nonatomic, strong) IBOutlet UITextField * mobileNoText;
@property (nonatomic, strong) IBOutlet UITextField *  DegreeText;
@property (nonatomic, strong) IBOutlet UITextField * emailIdText;
@property (nonatomic, strong) IBOutlet UITextField * addressText;
@property (nonatomic, strong) IBOutlet NSMutableArray *doctorProfileArray;

@property (nonatomic, strong) IBOutlet UIView * expandView;
@property (nonatomic, strong) IBOutlet UIPickerView * pickerViewControl;
@property (nonatomic, strong) IBOutlet UIView * blockScreenMainView;

@property (nonatomic, strong) IBOutlet UITextField * StreetText;
@property (nonatomic, strong) IBOutlet UITextField * CityText;
@property (nonatomic, strong) IBOutlet UITextField * StateText;
@property (nonatomic, strong) IBOutlet UITextField * ZipcodeText;
@property (nonatomic, strong) IBOutlet UITextField * CountryText;

@property (nonatomic, strong)IBOutlet NSManagedObjectContext * managedObjectContext;

-(IBAction)Adrressclick:(UIButton *)sender;

// Variables and Properties ["", "", "", "", "", "", ""]

-(BOOL)UpdatedoctorPatient_rec;


@end
