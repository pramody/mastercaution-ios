//
//  CommonHelper.m
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 29/06/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import "CommonHelper.h"
#import <CoreLocation/CoreLocation.h>


@interface CommonHelper ()
{
    
}
@end

@implementation CommonHelper
{
    
}

 customDateFormat dateNeedAs;

// Check the Email is valid or not return True Or False

-(BOOL)isEmailValid:(NSString *)emailAddress  :(BOOL)stric
{
   
    NSString* stricterFilterString    =   @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString* laxString               =   @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString* emailRegex              =   stric == true ? stricterFilterString : laxString;
    NSPredicate * emailTest  =   [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:emailAddress];
}

/*
 
 -(BOOL) NSStringIsValidEmail:(NSString *)checkString
 {
 BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
 NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
 NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
 NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
 NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
 return [emailTest evaluateWithObject:checkString];
 }
 
 */

// Trim the String and get trimed string
-(NSString *) trimTheString:(NSString *)stringToTrim
{
   
    return  [stringToTrim stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}


// Get Body Mass Index
-(NSString *) getBmiValueForHeightInFeetInches:(float)heightInFeets  :(float)heightInInches   : (float)weightInKgs
{
    
    NSNumberFormatter *format=[[NSNumberFormatter alloc]init];
    format.numberStyle =NSNumberFormatterDecimalStyle;
    format.maximumFractionDigits = 2;
    format.roundingMode =NSNumberFormatterRoundUp;
    // Convert Feets and Inches to Meters separately
    float heightFeetToMeters =   (heightInFeets * 0.3048); // 1 FEET == 0.3048
    float heightInchesToMeters    =   (heightInInches * 0.0254);// 1 INCH == 0.0254
    
    // Total up the Meters in one and round up
    float totalMeters  = (heightFeetToMeters + heightInchesToMeters);
   // NSNumber *totalMetersRound = [format numberFromString:[format stringFromNumber:[NSNumber numberWithFloat:totalMeters]]];
    
    NSNumber* getYourBmi =  [NSNumber numberWithFloat :((weightInKgs) / (totalMeters * totalMeters))];
    NSString * getYourBmiRound = [format stringFromNumber:getYourBmi];

    
    return getYourBmiRound;
    
    
}

-(NSString *) getDateInYourFormat:(NSString *)actualDateFormat : (NSString*) youWantDateFormat  : (customDateFormat)needDateAs   :(NSString *)dateAsNSString  :(NSDate*)dateAsNSDate
{
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = actualDateFormat;
    dateFormatter.timeZone = [NSTimeZone systemTimeZone];
    NSDate * converNSStringDateToNSDate = dateAsNSDate;
    
    // if date is in String than we need to convert it to nsdate
    if(needDateAs == DateAsNSString)
    {
        converNSStringDateToNSDate = [dateFormatter dateFromString:dateAsNSString];
    }
    
    dateFormatter.dateFormat = youWantDateFormat;
    NSString * finalDateStr = [dateFormatter stringFromDate:converNSStringDateToNSDate];
    
    return finalDateStr;
    
    
}

-(NSString *) getDateUTC:(NSString *)actualDateFormat : (NSString*) youWantDateFormat  : (customDateFormat)needDateAs   :(NSString *)dateAsNSString  :(NSDate*)dateAsNSDate
{
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = actualDateFormat;
     dateFormatter.timeZone = [NSTimeZone timeZoneWithAbbreviation: @"UTC"];
    NSDate * converNSStringDateToNSDate = dateAsNSDate;
    
    // if date is in String than we need to convert it to nsdate
    if(needDateAs == DateAsNSString)
    {
        converNSStringDateToNSDate = [dateFormatter dateFromString:dateAsNSString];
    }
    
    dateFormatter.dateFormat = youWantDateFormat;
    NSString * finalDateStr = [dateFormatter stringFromDate:converNSStringDateToNSDate];
    
    return finalDateStr;
    
    
}


-(NSString *) UTCDateInYourFormat:(NSString *)actualDateFormat : (NSString*) youWantDateFormat  :(NSString *)dateAsNSString
{
    
    NSDateFormatter * format = [[NSDateFormatter alloc] init];
    
    // from server dateFormat
    [format setDateFormat:actualDateFormat];
    
    // get date from server
    NSDate * dateTemp = [format dateFromString:dateAsNSString];
    
    // new date format
    [format setDateFormat:youWantDateFormat];
    
    // convert Timezone
    NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
    
    NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    
    NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:dateTemp];
    
    NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:dateTemp];
    
    NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;
    
    // get final date in LocalTimeZone
    NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:dateTemp];
    
    // convert to String
    NSString *dateStr = [format stringFromDate:destinationDate];
    
    
    return dateStr;
    
    
}



-(NSString *)celsiustoFahernheit:(NSString *)celsius
{
    double celsflot =[celsius doubleValue];
    double fahren=(celsflot*(9.0/5.0)+32);
    return [NSString stringWithFormat:@"%0.2f",fahren];
}

-(NSString *)Fahernheittocelsius:(NSString *)fahrenheit
{
    int celsius=(([fahrenheit doubleValue]- 32)*(5.0/9.0));
    return [NSString stringWithFormat:@"%d",celsius];
}





- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

@end
