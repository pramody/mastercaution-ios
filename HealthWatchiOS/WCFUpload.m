//
//  WCFUpload.m
//  HealthWatch
//
//  Created by METSL MAC MINI on 18/12/15.
//  Copyright © 2015 METSL MAC MINI. All rights reserved.
//

#import "WCFUpload.h"
#import "MCDSettings.h"
#import "PatientDetails.h"
#import "EventStorage.h"
#import "ViewRecordedEcgViewController.h"
#import "ConfigureMCDController.h"


@implementation WCFUpload
@synthesize test_smtp_message;
int eventcnter=0;
bool eventflag=false;
bool suspensionflag=false;

- (void)doInBackground{
    NSLog(@"WCF Service remaining = %.1f seconds", [UIApplication sharedApplication].backgroundTimeRemaining);
    NSLog(@"WCF Service running at %.1ld seconds", [self getCurrentNetworkTime]);
    
    GlobalVars *global=[GlobalVars sharedInstance];
    
    if( [UIApplication sharedApplication].backgroundTimeRemaining<10)
    {
        [ global.WCFService startService];
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL standalone_Mode = [defaults boolForKey:@"ActiveStandalonemode"];
    
    
    if(global.Internetflag==1)
    {
    if (!standalone_Mode)
    {
   [self uploadMCDSetting];
  
   [self UploadThresholdSetting];
   [self UploadPatientDetail];
        
     if(eventcnter==15||eventcnter==0)
     {
      if(!eventflag)
      {
     [self UploadEventHeader];
        
      }
         if(!suspensionflag)
         {
              [self uploadSuspensionSetting];
         }
          eventcnter=0;
     }
    }
    else
    {
        [self UploadingEmail_sms];
    }
    }
    BOOL savedSwitch= [[NSUserDefaults standardUserDefaults] boolForKey:@"SwitchState"];
    if(savedSwitch)
    {
        
        [[LocationHandler getSharedInstance]setDelegate:self];
        [[LocationHandler getSharedInstance]startUpdating];
    }
    
    eventcnter++;
   
}



-(void)didUpdateToLocation:(CLLocation *)newLocation
              fromLocation:(CLLocation *)oldLocation{
     //NSLog(@"%@",[NSString stringWithFormat:@"Latitude: %f",newLocation.coordinate.latitude]);
    //NSLog(@"%@",[NSString stringWithFormat:@"Longitude: %f",newLocation.coordinate.longitude]);
     GlobalVars *globals = [GlobalVars sharedInstance];
    globals.latitude  = [NSString stringWithFormat:@"%.8f",newLocation.coordinate.latitude];
    globals.longititude = [NSString stringWithFormat:@"%.8f", newLocation.coordinate.longitude];
    
}
// Your methods
-(void)uploadMCDSetting
{
    NSDate *date = [[NSDate alloc] init];
    NSLog(@"%@", date);
    
    CommonHelper * commhelp=[[CommonHelper alloc] init];
    
    NSString *modifieddate=[commhelp getDateUTC:@"yyyy-MM-dd" :@"MM/dd/yyyy HH:mm:ss" :DateAsNSDate :@"2014-04-25" :date];
    
    
    NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
    NSUserDefaults * userDefault1 = [NSUserDefaults standardUserDefaults];
    
    
    
    NSString * patientid = [userDefault valueForKey:@"PatientID"];
    NSString * deviceid = [userDefault valueForKey:@"Deviceid"];
    NSString *caregivername=[userDefault1 valueForKey:@"CareGiver"];
    
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=@"0";
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@";
    NSArray* args = @[@"updated_to_site",code];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
        NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"MCDSettings"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *mcdsett = (NSManagedObject *)[results objectAtIndex:0];
           // NSLog(@"1 - %@", mcdsett);
            
            NSString *domainname =[[NSUserDefaults standardUserDefaults] stringForKey:@"DomainName"];
            NSString *fiftyhz;
            if([[mcdsett valueForKey:@"sixtyhz_filter"] isEqualToString:@"0"] &&[[mcdsett valueForKey:@"fiftyhz_filter"] isEqualToString:@"0"] )
            {
                fiftyhz=@"0";
            }
            else if ([[mcdsett valueForKey:@"sixtyhz_filter"] isEqualToString:@"0"] &&[[mcdsett valueForKey:@"fiftyhz_filter"] isEqualToString:@"1"] )
            {
                 fiftyhz=@"1";
            }
            else
            {
                fiftyhz=@"2";
            }
            
            NSString * Fetchingpath=[NSString stringWithFormat:@"https://%@/MCC/MCCThresholdSettings.svc/SetMCDSettings/?DeviceID=%@&PatientID=%@&Gain=%@&Filter50Hz=%@&Filter60Hz=%@&BaseLineFilter=%@&Volumelevel=%@&TimeIntervalPre=%@&TimeIntervalPost=%@&BTDeviceAddress=%@&Leads=%@&SamplingRate=%@&EventMode=%@&CreatedBy=%@&CreatedFrom=%@&ModifiedFrom=%@&CreatedDateTime=%@&ModifiedDateTime=%@&respiration=%@&orientation=%@&twave=%@",domainname,deviceid,patientid,[mcdsett valueForKey:@"gain"],fiftyhz,[mcdsett valueForKey:@"sixtyhz_filter"],[mcdsett valueForKey:@"base_line_fil"],[mcdsett valueForKey:@"volume_level"],[mcdsett valueForKey:@"timeinterval_pre"],[mcdsett valueForKey:@"timeinterval_post"],[mcdsett valueForKey:@"bt_device_add"],[mcdsett valueForKey:@"leads"],[mcdsett valueForKey:@"sampling_rate"],[mcdsett valueForKey:@"lead_config"],caregivername,@"MCM",@"MCM",[mcdsett valueForKey:@"created_datetime"],modifieddate,[mcdsett valueForKey:@"respiration"],[mcdsett valueForKey:@"orientation"],[mcdsett valueForKey:@"inverted_t_wave"]];
           
            
            NSString *bookurl=[Fetchingpath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *URL1=[NSURL URLWithString:bookurl];
            
            NSLog(@"url%@",URL1);
            
            NSURLRequest *request = [NSURLRequest requestWithURL:URL1];
            
            
            
            AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
            
            operation.responseSerializer = [AFHTTPResponseSerializer serializer];
            
            
            [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                NSLog(@"urlresp%@",string);
                
                NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
                id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                
                if([ [json objectForKey:@"status"] isEqualToString:@"Success"])
                {
                    NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"MCDSettings"];
                    MCDSettings *mcdsettings=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
            
                    [mcdsettings setValue:@"MCC" forKey:@"created_from"];
                    [mcdsettings setValue:@"MCC" forKey:@"modified_from"];
                    [mcdsettings setValue:modifieddate forKey:@"modified_time"];
                    
                    [mcdsettings setValue: @"1"              forKey:@"updated_to_site"];
                    
                    [self.managedObjectContext save:nil];
                    
                    
                    
                }
                else
                {
                    NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"MCDSettings"];
                    MCDSettings *mcdsettings=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
                    
                  
                    [mcdsettings setValue:@"MCC" forKey:@"created_from"];
                    [mcdsettings setValue:@"MCM" forKey:@"modified_from"];
                    [mcdsettings setValue:modifieddate forKey:@"modified_time"];
                    
                    [mcdsettings setValue: @"0"              forKey:@"updated_to_site"];
                    
                    [self.managedObjectContext save:nil];
                    
                    
                }
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
               /* UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error updating MCd Setting"
                                                                    message:[error localizedDescription]
                                                                   delegate:nil
                                                          cancelButtonTitle:@"Ok"
                                                          otherButtonTitles:nil];
                
                
                [alertView show];*/
                
                NSLogger * logger=[[NSLogger alloc]init];
                logger.degbugger = true;
                
                [logger log:@"Server Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:[error localizedDescription], @"Mcd updation ", nil] error:TRUE];
                
               
                
            }];
            
            
            [operation start];
            
            
            
            
            
            
        } else {
            NSLog(@"Enter Corect code number MCD ");
        }
        
    }
    
   
}



-(void)uploadSuspensionSetting
{
    suspensionflag=true;
    
    NSDate *date = [[NSDate alloc] init];
    NSLog(@"%@", date);
    
    CommonHelper * commhelp=[[CommonHelper alloc] init];
    
    NSString *modifieddate=[commhelp getDateUTC:@"yyyy-MM-dd" :@"MM/dd/yyyy HH:mm:ss" :DateAsNSDate :@"2014-04-25" :date];
   
    //[date dateByAddingTimeInterval:1]
    NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
   // NSUserDefaults * userDefault1 = [NSUserDefaults standardUserDefaults];
   
    
    
    NSString * patientid = [userDefault valueForKey:@"PatientID"];
    NSString * deviceid = [userDefault valueForKey:@"Deviceid"];

    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=@"0";
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@";
    NSArray* args = @[@"updated_to_site",code];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1)
    {
        
        
        NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"SuspensionSettings"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *suspsett = (NSManagedObject *)[results objectAtIndex:0];
           // NSLog(@"1 - %@", suspsett);
            
            NSString *domainname =[[NSUserDefaults standardUserDefaults] stringForKey:@"DomainName"];
            
            
            NSString * Fetchingpath=[NSString stringWithFormat:@"https://%@/MCC/MCCThresholdsettings.svc/SetSupensionTime/?DeviceID=%@&PatientID=%@&EventCode=%@&SuspensionTime=%@&CreatedFrom=%@&ModifiedFrom=%@&CreatedDateTime=%@&ModifiedDateTime=%@",domainname,deviceid,patientid,[suspsett valueForKey:@"event_code"],[suspsett valueForKey:@"suspension_time"],[suspsett valueForKey:@"created_from"],[suspsett valueForKey:@"modify_from"],[suspsett valueForKey:@"created_date_time"],modifieddate];
            
            
            
            NSString *bookurl=[Fetchingpath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *URL1=[NSURL URLWithString:bookurl];
            
            NSLog(@"url%@",URL1);
            
            NSURLRequest *request = [NSURLRequest requestWithURL:URL1];
            
            
            
            AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
            
            operation.responseSerializer = [AFHTTPResponseSerializer serializer];
            
            
            [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                NSLog(@"urlresp%@",string);
                
                
                NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
                id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
                
                if([ [json objectForKey:@"status"] isEqualToString:@"Success"])
                {
                    [suspsett setValue:modifieddate forKey:@"modify_datetime"];
                    [suspsett setValue:@"1" forKey:@"updated_to_site"];
                    [suspsett setValue:[json objectForKey:@"status"] forKey:@"status"];
                    
                    [self.managedObjectContext save:nil];
     
                }
                else
                {
                    [suspsett setValue:modifieddate forKey:@"modify_datetime"];
                    [suspsett setValue:@"0" forKey:@"updated_to_site"];
                     [suspsett setValue:[json objectForKey:@"status"] forKey:@"status"];
                    [self.managedObjectContext save:nil];
                    
                }
                
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
               /* UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error updating suspension timer"
                                                                    message:[error localizedDescription]
                                                                   delegate:nil
                                                          cancelButtonTitle:@"Ok"
                                                          otherButtonTitles:nil];
                
                
                [alertView show];*/
                NSLogger *logger=[[NSLogger alloc]init];
                logger.degbugger = true;
                [logger log:@"Server Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:[error localizedDescription], @"Suspension Setting page", nil] error:TRUE];
                
            }];
            
            
            [operation start];
            
            
        } else {
            NSLog(@"Enter Corect code number Suspension");
        }
        
    }
    suspensionflag=false;
}

-(void)uploadSuspensionSetting1 :(NSString *)eventsuspensiontime :(NSString *)eventcode
{
    NSDate *date = [[NSDate alloc] init];
    NSLog(@"%@", date);
    
    CommonHelper * commhelp=[[CommonHelper alloc] init];
    
    NSString *modifieddate=[commhelp getDateUTC:@"yyyy-MM-dd" :@"MM/dd/yyyy HH:mm:ss" :DateAsNSDate :@"2014-04-25" :date];
    
    //[date dateByAddingTimeInterval:1]
    NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
    // NSUserDefaults * userDefault1 = [NSUserDefaults standardUserDefaults];
    
    NSString * patientid = [userDefault valueForKey:@"PatientID"];
    NSString * deviceid = [userDefault valueForKey:@"Deviceid"];
    
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=@"0";
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@&&%K == %@";
    NSArray* args = @[@"event_code",eventcode,@"suspension_time",eventsuspensiontime];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1)
    {
        
        
        NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"SuspensionSettings"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *suspsett = (NSManagedObject *)[results objectAtIndex:0];
           // NSLog(@"1 - %@", suspsett);
            
            NSString *domainname =[[NSUserDefaults standardUserDefaults] stringForKey:@"DomainName"];
            
            
            NSString * Fetchingpath=[NSString stringWithFormat:@"https://%@/MCC/MCCThresholdsettings.svc/SetSupensionTime/?DeviceID=%@&PatientID=%@&EventCode=%@&SuspensionTime=%@&CreatedFrom=%@&ModifiedFrom=%@&CreatedDateTime=%@&ModifiedDateTime=%@",domainname,deviceid,patientid,[suspsett valueForKey:@"event_code"],[suspsett valueForKey:@"suspension_time"],[suspsett valueForKey:@"created_from"],[suspsett valueForKey:@"modify_from"],[suspsett valueForKey:@"created_date_time"],modifieddate];
            
            
            
            NSString *bookurl=[Fetchingpath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *URL1=[NSURL URLWithString:bookurl];
            
            NSLog(@"url%@",URL1);
            
            NSURLRequest *request = [NSURLRequest requestWithURL:URL1];
            
            
            
            AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
            
            operation.responseSerializer = [AFHTTPResponseSerializer serializer];
            
            
            [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                NSLog(@"urlresp%@",string);
                
                
                NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
                id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                
                
                if([ [json objectForKey:@"status"] isEqualToString:@"Success"])
                {
                    [suspsett setValue:modifieddate forKey:@"modify_datetime"];
                    [suspsett setValue:@"1" forKey:@"updated_to_site"];
                    [suspsett setValue:[json objectForKey:@"status"] forKey:@"status"];
                    
                    [self.managedObjectContext save:nil];
                    
                }
                else
                {
                    [suspsett setValue:modifieddate forKey:@"modify_datetime"];
                    [suspsett setValue:@"0" forKey:@"updated_to_site"];
                    [suspsett setValue:[json objectForKey:@"status"] forKey:@"status"];
                    [self.managedObjectContext save:nil];
                    
                }
                
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             
                NSLogger *logger=[[NSLogger alloc]init];
                logger.degbugger = true;
                [logger log:@"Server Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:[error localizedDescription], @"Suspension Setting page", nil] error:TRUE];
                
            }];
            
            
            [operation start];
            
            
        } else {
            NSLog(@"Enter Corect code number Suspension");
        }
        
    }
    
}


-(void)UploadThresholdSetting
{
    NSDate *date = [[NSDate alloc] init];
    NSLog(@"%@", date);
    
    CommonHelper * commhelp=[[CommonHelper alloc] init];
    
    NSString *modifieddate=[commhelp getDateUTC:@"yyyy-MM-dd" :@"MM/dd/yyyy HH:mm:ss" :DateAsNSDate :@"2014-04-25" :date];
    
    
    NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];

    NSString * patientid = [userDefault valueForKey:@"PatientID"];
    NSString * deviceid = [userDefault valueForKey:@"Deviceid"];
    
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSMutableArray *results1 = [[NSMutableArray alloc]init];
    NSString *code=@"0";
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@";
    NSArray* args = @[@"updated_to_site",code];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1)
    {
        
        
        NSLog(@"predicate: %@",pred);
       
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        
        NSFetchRequest *fetchRequest1 = [[NSFetchRequest alloc]initWithEntityName:@"ThresholdSett"];
        [fetchRequest1 setPredicate:pred];
        results1 = [[self.managedObjectContext executeFetchRequest:fetchRequest1 error:nil] mutableCopy];
        
        if (results.count > 0&&results1.count>0) {
            
            NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
            NSManagedObject *genthressett = (NSManagedObject *)[results1 objectAtIndex:0];
         
            
            NSString *domainname =[[NSUserDefaults standardUserDefaults] stringForKey:@"DomainName"];
            NSString *posture;
            if([[[doctorssett valueForKey:@"code"] stringValue] isEqualToString:@"2024"])
            {
                  posture=@"0";
            }
            else
            {
                posture= [genthressett valueForKey:@"posture"];
            }
            
           NSString * Fetchingpath=[NSString stringWithFormat:@"https://%@/MCC/MCCThresholdSettings.svc/SetThresholdSettingsnew/?PatientID=%@&DeviceID=%@&Code=%d&DD=%@&YD=%@&RD=%@&Posture=%@&Flag=%@&DDFlag=%@&CreatedFrom=%@&ModifiedFrom=%@&CreatedDateTime=%@&ModifiedDateTime=%@",domainname,patientid,deviceid,[[doctorssett valueForKey:@"code"] intValue],[doctorssett valueForKey:@"dd"],[genthressett valueForKey:@"yd"],[genthressett valueForKey:@"rd"],posture,[genthressett valueForKey:@"flag"],[doctorssett valueForKey:@"ddflag"],[doctorssett valueForKey:@"created_from"],[doctorssett valueForKey:@"modify_from"],[doctorssett valueForKey:@"created_date_time"],modifieddate];
            
            
            
            NSString *bookurl=[Fetchingpath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *URL1=[NSURL URLWithString:bookurl];
            
            NSLog(@"url%@",URL1);
            
            NSURLRequest *request = [NSURLRequest requestWithURL:URL1];
            
            
            
            AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
            
            operation.responseSerializer = [AFHTTPResponseSerializer serializer];
            
            
            [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                NSLog(@"urlresp%@",string);
                
                
                NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
                id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                
                
                if([ [json objectForKey:@"status"] isEqualToString:@"Success"])
                {
                    [doctorssett setValue:modifieddate forKey:@"modify_datetime"];
                    [doctorssett setValue:@"1" forKey:@"updated_to_site"];
                    [doctorssett setValue: [json objectForKey:@"status"] forKey:@"status"];
                   
                    
                    [genthressett setValue:modifieddate forKey:@"modify_datetime"];
                    [genthressett setValue:@"1" forKey:@"updated_to_site"];
                    [genthressett setValue:[json objectForKey:@"status"] forKey:@"status"];
                    
                    [self.managedObjectContext save:nil];
                    
                }
                else
                {
                    [doctorssett setValue:modifieddate forKey:@"modify_datetime"];
                    [doctorssett setValue:@"0" forKey:@"updated_to_site"];
                     [doctorssett setValue: [json objectForKey:@"status"] forKey:@"status"];
                    
                    [genthressett setValue:modifieddate forKey:@"modify_datetime"];
                    [genthressett setValue:@"0" forKey:@"updated_to_site"];
                     [genthressett setValue:[json objectForKey:@"status"] forKey:@"status"];
                    
                    [self.managedObjectContext save:nil];
                    
                }
                
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
              /*  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error updating suspension timer"
                                                                    message:[error localizedDescription]
                                                                   delegate:nil
                                                          cancelButtonTitle:@"Ok"
                                                          otherButtonTitles:nil];
                
                
                [alertView show];*/
                NSLogger *logger=[[NSLogger alloc]init];
                logger.degbugger = true;
                [logger log:@"Server Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:[error localizedDescription], @"Threshold Setting page", nil] error:TRUE];
                
            }];
            
            
            [operation start];
            
            
        } else {
            NSLog(@"Enter Corect code number Threshold");
        }
        
    }

}


-(void)UploadPatientDetail
{
    NSDate *date = [[NSDate alloc] init];
    NSLog(@"%@", date);
    
    CommonHelper * commhelp=[[CommonHelper alloc] init];
    
   // NSString *modifieddate=[commhelp getDateUTC:@"yyyy-MM-dd" :@"MM/dd/yyyy HH:mm:ss" :DateAsNSDate :@"2014-04-25" :date];
    
    
    NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
    
    NSString * patientid = [userDefault valueForKey:@"PatientID"];
     NSString *domainname =[[NSUserDefaults standardUserDefaults] stringForKey:@"DomainName"];
    
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=@"0";
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@";
    NSArray* args = @[@"pat_updated_site",code];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1)
    {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"PatientDetails"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        if (results.count > 0)
        {
             NSManagedObject *PatientDetail = (NSManagedObject *)[results objectAtIndex:0];
            NSString *createddatetime=[PatientDetail valueForKey:@"active_since"];
            createddatetime = [commhelp getDateInYourFormat :@"dd MMM yy" :@"MM/dd/yyyy HH:mm:ss" :DateAsNSString :createddatetime :nil];

            
            NSDate *date = [[NSDate alloc] init];
            NSLog(@"%@", date);
            NSString *ModifiedDatetime=[commhelp getDateUTC:@"yyyy-MM-dd" :@"MM/dd/yyyy HH:mm:ss" :DateAsNSDate :@"2014-04-25" :date];
         /*   NSCalendar *calendar = [NSCalendar currentCalendar];
            NSDateComponents *components = [calendar components:(NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear| NSCalendarUnitHour | NSCalendarUnitMinute|NSCalendarUnitSecond) fromDate:date];
            
            
            
             //[components setSecond:1];
            
            [self WCFPatientUpdate:patientid :[PatientDetail valueForKey:@"given_name"] :@"0" :createddatetime :[commhelp getDateUTC:@"yyyy-MM-dd" :@"MM/dd/yyyy HH:mm:ss" :DateAsNSDate :@"2014-04-25" :[date dateByAddingTimeInterval:-1]] :domainname];
            
          
            
            [self WCFPatientUpdate:patientid :[PatientDetail valueForKey:@"family_name"] :@"2" :createddatetime :[commhelp getDateUTC:@"yyyy-MM-dd" :@"MM/dd/yyyy HH:mm:ss" :DateAsNSDate :@"2014-04-25" :[date dateByAddingTimeInterval:-1]] :domainname];
            
            
            [self WCFPatientUpdate:patientid :[PatientDetail valueForKey:@"dob1"] :@"3" :createddatetime :[commhelp getDateUTC:@"yyyy-MM-dd" :@"MM/dd/yyyy HH:mm:ss" :DateAsNSDate :@"2014-04-25" :[date dateByAddingTimeInterval:-1]] :domainname];
          
            [self WCFPatientUpdate:patientid :[PatientDetail valueForKey:@"height1"] :@"4" :createddatetime :[commhelp getDateUTC:@"yyyy-MM-dd" :@"MM/dd/yyyy HH:mm:ss" :DateAsNSDate :@"2014-04-25" :[date dateByAddingTimeInterval:-1]] :domainname];
            
            [self WCFPatientUpdate:patientid :[PatientDetail valueForKey:@"weight"] :@"5" :createddatetime :[commhelp getDateUTC:@"yyyy-MM-dd" :@"MM/dd/yyyy HH:mm:ss" :DateAsNSDate :@"2014-04-25" :[date dateByAddingTimeInterval:-1]] :domainname];
            [self WCFPatientUpdate:patientid :genderservtext :@"6" :createddatetime :[commhelp getDateUTC:@"yyyy-MM-dd" :@"MM/dd/yyyy HH:mm:ss" :DateAsNSDate :@"2014-04-25" :[calendar dateFromComponents:components]] :domainname];
          */
            
            
            NSString *genderservtext;
            if([[PatientDetail valueForKey:@"gender1"] isEqualToString:@"M"])
            {
                genderservtext=@"0";
               
            }
            else if([[PatientDetail valueForKey:@"gender1"] isEqualToString:@"F"])
            {
                genderservtext=@"1";
            }
           else if([[PatientDetail valueForKey:@"gender1"] isEqualToString:@"O"])
            {
                genderservtext=@"2";
            }
            else
            {
                genderservtext=@"3";
            }

            NSString *dob=[commhelp getDateUTC:@"MM/dd/yyyy" :@"MM/dd/yyyy" :DateAsNSString :[PatientDetail valueForKey:@"dob1"] :nil];
            
            
            
            
            NSString * Fetchingpath=[NSString stringWithFormat:@"https://%@/MCC/MCCWcfService.svc/changepatientdetailsByISO/?patientid=%@&GivenName=%@&MiddleName=%@&FamilyName=%@&DOB=%@&Height=%@&Weight=%@&Gender=%@&SSN=%@&CreatedDateTime=%@&ModifiedDateTime=%@&Street=%@&City=%@&State=%@&Country=%@&ZipCode=%@&ContactNo=%@",domainname,patientid,[PatientDetail valueForKey:@"given_name"],[PatientDetail valueForKey:@"middle_name"],[PatientDetail valueForKey:@"family_name"],dob,[PatientDetail valueForKey:@"height1"],[PatientDetail valueForKey:@"weight"],[PatientDetail valueForKey:@"gender1"],[PatientDetail valueForKey:@"uid"],createddatetime,ModifiedDatetime,[PatientDetail valueForKey:@"street"],[PatientDetail valueForKey:@"city"],[PatientDetail valueForKey:@"state"],[PatientDetail valueForKey:@"country"],[PatientDetail valueForKey:@"zipcode"],[PatientDetail valueForKey:@"patient_mobno"]];
          
            /* changepatientdetailsByISO/?patientid={patientid}&GivenName={GivenName}&MiddleName={MiddleName}&FamilyName={FamilyName}&DOB={DOB}&Height={Height}&Weight={Weight}&Gender={Gender}&CreatedDateTime={CreatedDateTime}&ModifiedDateTime={ModifiedDateTime}&Street={Street}&City={City}&State={State}&Country={Country}&ZipCode={ZipCode}*/
            
            
            NSString *bookurl=[Fetchingpath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *URL1=[NSURL URLWithString:bookurl];
            
            NSLog(@"url%@",URL1);
            
            NSURLRequest *request = [NSURLRequest requestWithURL:URL1];
            NSLogger *logger=[[NSLogger alloc]init];
            
            
            AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
            
            operation.responseSerializer = [AFHTTPResponseSerializer serializer];
            
            
            [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)  {
                
                NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                NSLog(@"urlresp%@",string);
                
                
                
                [logger log:@"NULL" properties:[NSDictionary dictionaryWithObjectsAndKeys:string, @"User Profile page", nil] error:false];
                
                NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
                
                id result = [NSJSONSerialization JSONObjectWithData:data
                                                            options:NSJSONReadingAllowFragments
                                                              error:NULL];
                
                if([result isEqualToString:@"true"])
                {
                    NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"PatientDetails"];
                    PatientDetails *patientsettings=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
                    
                    [patientsettings setValue:ModifiedDatetime forKey:@"modified_datetime"];
                    [patientsettings setValue:@"1" forKey:@"pat_updated_site"];
                    
                    [self.managedObjectContext save:nil];
                    
                }
                else
                {
                    NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"PatientDetails"];
                    PatientDetails *patientsettings=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
                    [patientsettings setValue:ModifiedDatetime forKey:@"modified_datetime"];
                    [patientsettings setValue:@"0" forKey:@"pat_updated_site"];
                    
                    [self.managedObjectContext save:nil];
                    
                }
                
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
               
                logger.degbugger = true;
                
                [logger log:@"Server Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:[error localizedDescription], @"Last Name Updation", nil] error:TRUE];
                
            }];
            
            [operation start];
            
            
        }
        else
        {
             NSLog(@"Enter Corect code number PAtient");
        }

        
    }

}


-(void)WCFPatientUpdate :(NSString*) patientID :(NSString*) data :(NSString*) type :(NSString*) createddatetime
                        :(NSString*) modifieddatetime :(NSString *)domainname
{
    
     NSString * Fetchingpath=[NSString stringWithFormat:@"https://%@/MCC/MCCWcfService.svc/ChangePatientDetails/?patientid=%@&data=%@&type=%@&CreatedDateTime=%@&ModifiedDateTime=%@",domainname,patientID,data,type,createddatetime,modifieddatetime];
    
   
    
    NSString *bookurl=[Fetchingpath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *URL1=[NSURL URLWithString:bookurl];
    
    NSLog(@"url%@",URL1);
    
    NSURLRequest *request = [NSURLRequest requestWithURL:URL1];
    NSLogger *logger=[[NSLogger alloc]init];
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)  {
        
        NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSLog(@"urlresp%@",string);
        
       
        
        [logger log:@"NULL" properties:[NSDictionary dictionaryWithObjectsAndKeys:string, @"User Profile page", nil] error:false];
        
        NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
        
        id result = [NSJSONSerialization JSONObjectWithData:data
                                                    options:NSJSONReadingAllowFragments
                                                      error:NULL];
        
        if([result isEqualToString:@"true"])
        {
            NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"PatientDetails"];
            PatientDetails *patientsettings=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
           
            [patientsettings setValue:modifieddatetime forKey:@"modified_datetime"];
            [patientsettings setValue:@"1" forKey:@"pat_updated_site"];
            
            [self.managedObjectContext save:nil];
            
        }
        else
        {
            NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"PatientDetails"];
            PatientDetails *patientsettings=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
            [patientsettings setValue:modifieddatetime forKey:@"modified_datetime"];
            [patientsettings setValue:@"0" forKey:@"pat_updated_site"];
            
            [self.managedObjectContext save:nil];
            
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
       /* UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error Retrieving User profile"
                                                            message:[error localizedDescription]
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        
        
        [alertView show];
        */
        logger.degbugger = true;
        
        [logger log:@"Server Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:[error localizedDescription], @"Last Name Updation", nil] error:TRUE];
        
    }];
    
    [operation start];

}




-(void)UploadEventHeader
{
    
    
   
    CommonHelper *commhelp=[[CommonHelper alloc]init];
   
    
    
    NSDate *date = [[NSDate alloc] init];
    NSLog(@"%@", date);
    
    
    NSString *Eventdttime=[commhelp getDateUTC:@"yyyy-MM-dd" :@"MM/dd/yyyy HH:mm:ss" :DateAsNSDate :@"2014-04-25" :date];
    
    
    NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
   
    NSString * patientid = [userDefault valueForKey:@"PatientID"];
    NSString * deviceid = [userDefault valueForKey:@"Deviceid"];
    NSString *strMCDTYPE;
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=@"0";
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ &&%K == %@";
    NSArray* args = @[@"event_updated_to_device",code,@"event_receive_complete",@"1"];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1)
    {
        
        
        NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"EventStorage"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *EventSett = (NSManagedObject *)[results objectAtIndex:0];
            
            NSString * location=[NSString stringWithFormat:@"%@,%@",[EventSett valueForKey:@"event_latitude"],[EventSett valueForKey:@"event_longitude"]];
            NSString *domainname =[[NSUserDefaults standardUserDefaults] stringForKey:@"DomainName"];
            if([[EventSett valueForKey:@"event_mcdlead"] isEqualToString:@"12"])
            {
                strMCDTYPE = @"0";
            }
            else if([[EventSett valueForKey:@"event_mcdlead"] isEqualToString:@"15"])
            {
                strMCDTYPE = @"1";
            }
            
            
            NSString * Fetchingpath,*bpdiastolic,*systolic,*glucose,*weight,*oximetry;
            if([[[EventSett valueForKey:@"event_code"]stringValue] isEqualToString:@"2040"])
            {
                if([[EventSett valueForKey:@"event_bpdia"] length]==0)
                {
                    bpdiastolic=@"0";
                }
                else
                {
                    bpdiastolic=[EventSett valueForKey:@"event_bpdia"];
                }
                
                if([[EventSett valueForKey:@"event_bpsys"] length]==0)
                {
                    systolic=@"0";
                }
                else
                {
                    systolic=[EventSett valueForKey:@"event_bpsys"];
                }
                if([[EventSett valueForKey:@"event_glucose"] length]==0)
                {
                    glucose=@"0";
                }
                else
                {
                    glucose=[EventSett valueForKey:@"event_glucose"];
                }
                
                if([[EventSett valueForKey:@"event_weight"] length]==0)
                {
                    weight=@"0";
                }
                else
                {
                    weight=[EventSett valueForKey:@"event_weight"];
                }
                if([[EventSett valueForKey:@"event_oximetry"] length]==0)
                {
                    oximetry=@"0";
                }
                else
                {
                    oximetry=[EventSett valueForKey:@"event_oximetry"];
                }
                
                Fetchingpath=[NSString stringWithFormat:@"https://%@/MCC/MCCWcfService.svc/EventDetails/?PatientID=%@&DeviceID=%@&Eventdttime=%@&EventCode=%@&EventType=%@&EVENT_ECG_FILEPATH=%@&EVENT_POSTURE_CODE=%@&EVENT_LEAD_DETECTION=%@&EVENT_TEMPERATURE=%@&EVENT_VALUE=%@&Resprate=%@&Heartrate=%@&Location=%@&Status=%@&Glucose=%@&Weight=%@&BPDia=%@&BPSys=%@&samplingrate=%@&QRSInterval=%@&TWave=%@&RespirationAmp=%@&QTC=%@&PRInterval=%@&RR=%@&QT=%@&RespirationVariance=%@&ecgid=%@&RespirationStatus=%@&SelectedLead=%@&MCDLead=%@&Oximetry=%@",domainname,patientid,deviceid,Eventdttime,[EventSett valueForKey:@"event_code"],[EventSett valueForKey:@"event_type"],@"",[EventSett valueForKey:@"event_posture_code"],[EventSett valueForKey:@"event_lead_detection"],[EventSett valueForKey:@"event_temp"],[EventSett valueForKey:@"event_value"],[EventSett valueForKey:@"event_respiration"],[EventSett valueForKey:@"event_heartrate"],location,@"0",glucose,weight,bpdiastolic,systolic,[EventSett valueForKey:@"event_sampling_rate"],[EventSett valueForKey:@"event_qrs_interval"],[EventSett valueForKey:@"event_t_wave"],[EventSett valueForKey:@"event_respiration_amplitude"],[EventSett valueForKey:@"event_qtc_interval"],[EventSett valueForKey:@"event_pr_interval"],[EventSett valueForKey:@"event_rr_interval"],[EventSett valueForKey:@"event_qt_interval"],[EventSett valueForKey:@"event_resp_variance"],@"",[EventSett valueForKey:@"event_respiration_onoff"],[EventSett valueForKey:@"event_lead_config"],@"0",oximetry];
            
            }
            else
            {
            Fetchingpath=[NSString stringWithFormat:@"https://%@/MCC/MCCWcfService.svc/EventDetails/?PatientID=%@&DeviceID=%@&Eventdttime=%@&EventCode=%@&EventType=%@&EVENT_ECG_FILEPATH=%@&EVENT_POSTURE_CODE=%@&EVENT_LEAD_DETECTION=%@&EVENT_TEMPERATURE=%@&EVENT_VALUE=%@&Resprate=%@&Heartrate=%@&Location=%@&Status=%@&Glucose=%@&Weight=%@&BPDia=%@&BPSys=%@&samplingrate=%@&QRSInterval=%@&TWave=%@&RespirationAmp=%@&QTC=%@&PRInterval=%@&RR=%@&QT=%@&RespirationVariance=%@&ecgid=%@&RespirationStatus=%@&SelectedLead=%@&MCDLead=%@&Oximetry=%@",domainname,patientid,deviceid,Eventdttime,[EventSett valueForKey:@"event_code"],[EventSett valueForKey:@"event_type"],[EventSett valueForKey:@"event_ecgfilepath"],[EventSett valueForKey:@"event_posture_code"],[EventSett valueForKey:@"event_lead_detection"],[EventSett valueForKey:@"event_temp"],[EventSett valueForKey:@"event_value"],[EventSett valueForKey:@"event_respiration"],[EventSett valueForKey:@"event_heartrate"],location,@"0",[EventSett valueForKey:@"event_glucose"],[EventSett valueForKey:@"event_weight"],[EventSett valueForKey:@"event_bpdia"],[EventSett valueForKey:@"event_bpsys"],[EventSett valueForKey:@"event_sampling_rate"],[EventSett valueForKey:@"event_qrs_interval"],[EventSett valueForKey:@"event_t_wave"],[EventSett valueForKey:@"event_respiration_amplitude"],[EventSett valueForKey:@"event_qtc_interval"],[EventSett valueForKey:@"event_pr_interval"],[EventSett valueForKey:@"event_rr_interval"],[EventSett valueForKey:@"event_qt_interval"],[EventSett valueForKey:@"event_resp_variance"],[EventSett valueForKey:@"event_ecg_id"],[EventSett valueForKey:@"event_respiration_onoff"],[EventSett valueForKey:@"event_lead_config"],strMCDTYPE,@"0"];
            }
            
            
            NSString *bookurl=[Fetchingpath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *URL1=[NSURL URLWithString:bookurl];
            
            NSLog(@"url%@",URL1);
            
            NSURLRequest *request = [NSURLRequest requestWithURL:URL1];
            
            
            
            AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
            
            operation.responseSerializer = [AFHTTPResponseSerializer serializer];
            
            
            [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                NSLog(@"urlresp%@",string);
                
                
                NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
                id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                
                
                if([ [json objectForKey:@"status"] isEqualToString:@"Success"])
                {
                    if([[[EventSett valueForKey:@"event_code"]stringValue] isEqualToString:@"2040"])
                    {
                        [EventSett setValue:@"1" forKey:@"event_updated_to_device"];
                        
                        [self.managedObjectContext save:nil];
                        
                        ConfigureMCDController *config=[[ConfigureMCDController alloc]init];
                        [config sendBTEventNotificationWithEventOccured:YES];
                    }
                    else
                    {
                    [EventSett setValue:[json objectForKey:@"ECGID"] forKey:@"event_ecg_id"];
                     [self UploadECG:[json objectForKey:@"ECGID"]  :[EventSett valueForKey:@"event_ecgfilepath"]] ;
                    [self.managedObjectContext save:nil];
                    }
                }
                else
                {
                     [EventSett setValue:@"0" forKey:@"event_updated_to_device"];
                    
                    [self.managedObjectContext save:nil];
                    
                }
                
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
               /* UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error updating Event data"
                                                                    message:[error localizedDescription]
                                                                   delegate:nil
                                                          cancelButtonTitle:@"Ok"
                                                          otherButtonTitles:nil];
                
                
                [alertView show];*/
                NSLogger *logger=[[NSLogger alloc]init];
                logger.degbugger = true;
                [logger log:@"Server Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:[error localizedDescription], @"Budge Event Status", nil] error:TRUE];
                
            }];
            
            
            [operation start];
            
            
        } else {
            NSLog(@"Enter Corect code number Suspension");
        }
        
    }

}



-(void)UploadEventHeader1 :(NSString *)event_dat_tme  :(NSString *)event_code
{
    
    eventflag=true;
    
    CommonHelper *commhelp=[[CommonHelper alloc]init];
    
    
    
    NSDate *date = [[NSDate alloc] init];
    NSLog(@"%@", date);
    
    
    NSString *Eventdttime=[commhelp getDateUTC:@"yyyy-MM-dd" :@"MM/dd/yyyy HH:mm:ss" :DateAsNSDate :@"2014-04-25" :date];
    
    
    NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
   
    NSString * patientid = [userDefault valueForKey:@"PatientID"];
    NSString * deviceid = [userDefault valueForKey:@"Deviceid"];
    NSString *strMCDTYPE;
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *code=event_code;
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ &&%K == %@";
    NSArray* args = @[@"event_code",code,@"event_datetime",event_dat_tme];
    if (code.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1)
    {
        
        
        NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"EventStorage"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *EventSett = (NSManagedObject *)[results objectAtIndex:0];
            
            NSString * location=[NSString stringWithFormat:@"%@,%@",[EventSett valueForKey:@"event_latitude"],[EventSett valueForKey:@"event_longitude"]];
            NSString *domainname =[[NSUserDefaults standardUserDefaults] stringForKey:@"DomainName"];
            if([[EventSett valueForKey:@"event_mcdlead"] isEqualToString:@"12"])
            {
                strMCDTYPE = @"0";
            }
            else if([[EventSett valueForKey:@"event_mcdlead"] isEqualToString:@"15"])
            {
                strMCDTYPE = @"1";
            }
            NSString * Fetchingpath,*bpdiastolic,*systolic,*glucose,*weight,*oximetry;
            if([[[EventSett valueForKey:@"event_code"]stringValue] isEqualToString:@"2040"])
            {
                if([[EventSett valueForKey:@"event_bpdia"] length]==0)
                {
                    bpdiastolic=@"0";
                }
                else
                {
                    bpdiastolic=[EventSett valueForKey:@"event_bpdia"];
                }
                
                if([[EventSett valueForKey:@"event_bpsys"] length]==0)
                {
                    systolic=@"0";
                }
                else
                {
                    systolic=[EventSett valueForKey:@"event_bpsys"];
                }
                if([[EventSett valueForKey:@"event_glucose"] length]==0)
                {
                    glucose=@"0";
                }
                else
                {
                    glucose=[EventSett valueForKey:@"event_glucose"];
                }
                
                if([[EventSett valueForKey:@"event_weight"] length]==0)
                {
                    weight=@"0";
                }
                else
                {
                    weight=[EventSett valueForKey:@"event_weight"];
                }
                if([[EventSett valueForKey:@"event_oximetry"] length]==0)
                {
                    oximetry=@"0";
                }
                else
                {
                    oximetry=[EventSett valueForKey:@"event_oximetry"];
                }
                
                Fetchingpath=[NSString stringWithFormat:@"https://%@/MCC/MCCWcfService.svc/EventDetails/?PatientID=%@&DeviceID=%@&Eventdttime=%@&EventCode=%@&EventType=%@&EVENT_ECG_FILEPATH=%@&EVENT_POSTURE_CODE=%@&EVENT_LEAD_DETECTION=%@&EVENT_TEMPERATURE=%@&EVENT_VALUE=%@&Resprate=%@&Heartrate=%@&Location=%@&Status=%@&Glucose=%@&Weight=%@&BPDia=%@&BPSys=%@&samplingrate=%@&QRSInterval=%@&TWave=%@&RespirationAmp=%@&QTC=%@&PRInterval=%@&RR=%@&QT=%@&RespirationVariance=%@&ecgid=%@&RespirationStatus=%@&SelectedLead=%@&MCDLead=%@&Oximetry=%@",domainname,patientid,deviceid,Eventdttime,[EventSett valueForKey:@"event_code"],[EventSett valueForKey:@"event_type"],@"",[EventSett valueForKey:@"event_posture_code"],[EventSett valueForKey:@"event_lead_detection"],[EventSett valueForKey:@"event_temp"],[EventSett valueForKey:@"event_value"],[EventSett valueForKey:@"event_respiration"],[EventSett valueForKey:@"event_heartrate"],location,@"0",glucose,weight,bpdiastolic,systolic,[EventSett valueForKey:@"event_sampling_rate"],[EventSett valueForKey:@"event_qrs_interval"],[EventSett valueForKey:@"event_t_wave"],[EventSett valueForKey:@"event_respiration_amplitude"],[EventSett valueForKey:@"event_qtc_interval"],[EventSett valueForKey:@"event_pr_interval"],[EventSett valueForKey:@"event_rr_interval"],[EventSett valueForKey:@"event_qt_interval"],[EventSett valueForKey:@"event_resp_variance"],@"",[EventSett valueForKey:@"event_respiration_onoff"],[EventSett valueForKey:@"event_lead_config"],@"0",oximetry];
                
            }
            else
            {
                Fetchingpath=[NSString stringWithFormat:@"https://%@/MCC/MCCWcfService.svc/EventDetails/?PatientID=%@&DeviceID=%@&Eventdttime=%@&EventCode=%@&EventType=%@&EVENT_ECG_FILEPATH=%@&EVENT_POSTURE_CODE=%@&EVENT_LEAD_DETECTION=%@&EVENT_TEMPERATURE=%@&EVENT_VALUE=%@&Resprate=%@&Heartrate=%@&Location=%@&Status=%@&Glucose=%@&Weight=%@&BPDia=%@&BPSys=%@&samplingrate=%@&QRSInterval=%@&TWave=%@&RespirationAmp=%@&QTC=%@&PRInterval=%@&RR=%@&QT=%@&RespirationVariance=%@&ecgid=%@&RespirationStatus=%@&SelectedLead=%@&MCDLead=%@&Oximetry=%@",domainname,patientid,deviceid,Eventdttime,[EventSett valueForKey:@"event_code"],[EventSett valueForKey:@"event_type"],[EventSett valueForKey:@"event_ecgfilepath"],[EventSett valueForKey:@"event_posture_code"],[EventSett valueForKey:@"event_lead_detection"],[EventSett valueForKey:@"event_temp"],[EventSett valueForKey:@"event_value"],[EventSett valueForKey:@"event_respiration"],[EventSett valueForKey:@"event_heartrate"],location,@"0",[EventSett valueForKey:@"event_glucose"],[EventSett valueForKey:@"event_weight"],[EventSett valueForKey:@"event_bpdia"],[EventSett valueForKey:@"event_bpsys"],[EventSett valueForKey:@"event_sampling_rate"],[EventSett valueForKey:@"event_qrs_interval"],[EventSett valueForKey:@"event_t_wave"],[EventSett valueForKey:@"event_respiration_amplitude"],[EventSett valueForKey:@"event_qtc_interval"],[EventSett valueForKey:@"event_pr_interval"],[EventSett valueForKey:@"event_rr_interval"],[EventSett valueForKey:@"event_qt_interval"],[EventSett valueForKey:@"event_resp_variance"],[EventSett valueForKey:@"event_ecg_id"],[EventSett valueForKey:@"event_respiration_onoff"],[EventSett valueForKey:@"event_lead_config"],strMCDTYPE,@"0"];
            }
            
            
            NSString *bookurl=[Fetchingpath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *URL1=[NSURL URLWithString:bookurl];
            
            NSLog(@"url%@",URL1);
            
            NSURLRequest *request = [NSURLRequest requestWithURL:URL1];
            
            
            
            AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
            
            operation.responseSerializer = [AFHTTPResponseSerializer serializer];
            
            
            [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                NSLog(@"urlresp%@",string);
                
                
                NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
                id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                
                
                if([ [json objectForKey:@"status"] isEqualToString:@"Success"])
                {
                    if([[[EventSett valueForKey:@"event_code"]stringValue] isEqualToString:@"2040"])
                    {
                        [EventSett setValue:@"1" forKey:@"event_updated_to_device"];
                        
                        [self.managedObjectContext save:nil];
                        
                        ConfigureMCDController *config=[[ConfigureMCDController alloc]init];
                        [config sendBTEventNotificationWithEventOccured:YES];
                    }
                    else
                    {
                        [EventSett setValue:[json objectForKey:@"ECGID"] forKey:@"event_ecg_id"];
                        //[self UploadECG:[json objectForKey:@"ECGID"]  :[EventSett valueForKey:@"event_ecgfilepath"]] ;
                        [self.managedObjectContext save:nil];
                    }
                }
                else
                {
                    [EventSett setValue:@"0" forKey:@"event_updated_to_device"];
                    
                    [self.managedObjectContext save:nil];
                    
                }
                
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                /* UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error updating Event data"
                 message:[error localizedDescription]
                 delegate:nil
                 cancelButtonTitle:@"Ok"
                 otherButtonTitles:nil];
                 
                 
                 [alertView show];*/
                NSLogger *logger=[[NSLogger alloc]init];
                logger.degbugger = true;
                [logger log:@"Server Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:[error localizedDescription], @"Budge Event Status", nil] error:TRUE];
                
            }];
            
            
            [operation start];
            
            
        } else {
            NSLog(@"Enter Corect code number Suspension");
        }
        
    }
    eventflag=false;
}


-(void)UploadECG:(NSString *)Ecgid :(NSString *)FileName
{
    NSString *domainname =[[NSUserDefaults standardUserDefaults] stringForKey:@"DomainName"];
    
    
    NSString * Fetchingpath=[NSString stringWithFormat:@"https://%@/MCC/eCGUpload.aspx?ecgid=%@",domainname,Ecgid];
    
    
    
    NSString *bookurl=[Fetchingpath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *URL1=[NSURL URLWithString:bookurl];
    
    NSLog(@"url%@",URL1);
  
    
   NSArray *filename=[FileName componentsSeparatedByString:@"/"];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSString *filePath = [documentsPath stringByAppendingPathComponent:filename[8]];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:filePath]) {
        
        // the file doesn't exist,we can write out the text using the  NSString convenience method
        
        NSLog(@"File Does not exist");
        
        
    } else {
        
        // the file already exists, append the text to the end
        
        // get a handle
        NSFileHandle *fileHandle = [NSFileHandle fileHandleForReadingAtPath:filePath];
        
        // move to the end of the file
        //[fileHandle seekToEndOfFile];
        
        // convert the string to an NSData object
        
        
        // write the data to the end of the file
        
        NSData *Data=[fileHandle readDataToEndOfFile];
      
      
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
         manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        [manager POST:bookurl parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            [formData appendPartWithFileData:Data
                                        name:@"ECGFile"
                                    fileName:filename[8] mimeType:@"application/octet-stream"];
            
        } success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"Response: %@", responseObject);
            NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            NSLog(@"dt%@",string);
            if([string containsString:@"is uploaded!"])
            {
            NSMutableArray *results = [[NSMutableArray alloc]init];
            int flag=0;
            NSPredicate *pred;
            NSString* filter = @"%K == %@";
            NSArray* args = @[@"event_ecg_id",Ecgid];
            if (Ecgid.length!=0) {
                pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
                flag=1;
            } else {
                flag=0;
                NSLog(@"Enter Corect code empty");
                
            }
            
            if (flag == 1)
            {
                
                
               // NSLog(@"predicate: %@",pred);
                NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"EventStorage"];
                [fetchRequest setPredicate:pred];
                results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
                
                if (results.count > 0)
                {
                    NSManagedObject *EventSett = (NSManagedObject *)[results objectAtIndex:0];
                    [EventSett setValue:@"1" forKey:@"event_updated_to_device"];
                    
                    [self.managedObjectContext save:nil];
                    
                    ConfigureMCDController *config=[[ConfigureMCDController alloc]init];
                    [config sendBTEventNotificationWithEventOccured:YES];
                }
            }
        }
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
        }];

        
        // clean up
        [fileHandle closeFile];
        
       
        
    }

    
}

-(void)SendPatientlivestatus
{
    NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
    
    NSString * patientid = [userDefault valueForKey:@"PatientID"];
    NSString * deviceid = [userDefault valueForKey:@"Deviceid"];
    
    CommonHelper *commhelp=[[CommonHelper alloc]init];
    NSDate *date = [[NSDate alloc] init];
    NSLog(@"%@", date);
    NSString *ModifiedDatetime=[commhelp getDateUTC:@"yyyy-MM-dd" :@"MM/dd/yyyy HH:mm:ss" :DateAsNSDate :@"2014-04-25" :date];
    
      NSString *domainname =[[NSUserDefaults standardUserDefaults] stringForKey:@"DomainName"];
    
    GlobalVars *globals = [GlobalVars sharedInstance];
   
    
    NSString * Fetchingpath=[NSString stringWithFormat:@"https://%@/MCC/MCCWcfService.svc/PatientLiveStatus/?PatientID=%@&DeviceID=%@&Resprate=%ld&Heartrate=%ld&QRSInterval=%ld&TWave=%d&RespirationAmp=%d&QTC=%ld&PRInterval=%ld&RR=%ld&QT=%ld&RespirationVariance=%d&Status=%d&CreatedDatetime=%@&ModifieldDateTime=%@",domainname,patientid,deviceid,(long)[globals mcd_RespirationRate],(long) globals.mcd_HeartRate,(long)globals.mcd_QRS,0,0,(long)globals.mcd_QTc,(long)globals.mcd_PR,(long)globals.mcd_RR,(long)globals.mcd_QT,0,1,ModifiedDatetime,ModifiedDatetime];
   
    
    NSLogger *logger=[[NSLogger alloc]init];
    
    NSString *bookurl=[Fetchingpath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *URL1=[NSURL URLWithString:bookurl];
    
    NSLog(@"url%@",URL1);
    
    NSURLRequest *request = [NSURLRequest requestWithURL:URL1];
    
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSLog(@"urlresp%@",string);
        
        
        NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        
        if([ [json objectForKey:@"status"] isEqualToString:@"Success"])
        {
            
           
            logger.degbugger = true;
            [logger log:@"Server Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"Success", @"Patient Live Status", nil] error:TRUE];
            
        }
        else
        {
            logger.degbugger = true;
            [logger log:@"Server Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"Failure", @"Patient Live Status", nil] error:TRUE];
            
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
      /*  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error updating Event data"
                                                            message:[error localizedDescription]
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        
        
        [alertView show];*/
       
        logger.degbugger = true;
        [logger log:@"Server Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:[error localizedDescription], @"Patient Live Status", nil] error:TRUE];
        
    }];
    
    
    [operation start];
    
    
}




-(void)bitInsert :(NSString *)accelerometerStatus :(NSString *)electrodestatus :(NSString *)battery_status :(NSString *)bluetooth_status
                 :(NSString *)ir_sensor_status :(NSString *)sdcard_status
{
    
    
    BOOL standalone_Mode;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    standalone_Mode = [defaults boolForKey:@"ActiveStandalonemode"];
    
    if(!standalone_Mode)
    {
    CommonHelper *commhelp=[[CommonHelper alloc]init];
    NSDate *date = [[NSDate alloc] init];
    NSLog(@"%@", date);
    NSString *ModifiedDatetime=[commhelp getDateUTC:@"yyyy-MM-dd" :@"MM/dd/yyyy HH:mm:ss" :DateAsNSDate :@"2014-04-25" :date];
    
    NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
   
    NSString * patientid = [userDefault valueForKey:@"PatientID"];
    NSString * deviceid = [userDefault valueForKey:@"Deviceid"];
    
    NSString *domainname =[[NSUserDefaults standardUserDefaults] stringForKey:@"DomainName"];
    
    
    NSString * Fetchingpath=[NSString stringWithFormat:@"https://%@/MCC/MCCWcfService.svc/BITINSERT/?PatientID=%@&DeviceID=%@&Accelerometer_Status=%@&Electrode_Status=%@&Battery_Status=%@&Bluetooth_Status=%@&IR_Sensor_Status=%@&SDCard_Status=%@&createddt=%@&modifieddt=%@",domainname,patientid,deviceid,accelerometerStatus,electrodestatus,battery_status,bluetooth_status,ir_sensor_status,sdcard_status,ModifiedDatetime,ModifiedDatetime];
    
    
    NSLogger *logger=[[NSLogger alloc]init];
    
    NSString *bookurl=[Fetchingpath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *URL1=[NSURL URLWithString:bookurl];
    
    NSLog(@"url%@",URL1);
    
    NSURLRequest *request = [NSURLRequest requestWithURL:URL1];
    
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSLog(@"urlresp%@",string);
        
        
        NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        
        if([ [json objectForKey:@"status"] isEqualToString:@"Success"])
        {
            
            
            logger.degbugger = true;
            [logger log:@"Server Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"Success", @"BitStatus", nil] error:TRUE];
            
        }
        else
        {
            logger.degbugger = true;
            [logger log:@"Server Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"Failure", @"BitStatus", nil] error:TRUE];
            
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        /*UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error updating BIT data"
                                                            message:[error localizedDescription]
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];*/
        
        
       // [alertView show];
        
        logger.degbugger = true;
        [logger log:@"Server Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:[error localizedDescription], @"BitStatus", nil] error:TRUE];
        
    }];
    
    
    [operation start];
    }

}


-(void)sendEcgelectrodeStatus :(NSInteger)RA  :(NSInteger)LA :(NSInteger)LL :(NSInteger)RL :(NSInteger)V11 :(NSInteger)V22
                       :(NSInteger)V33 :(NSInteger)V44 :(NSInteger)V55 :(NSInteger)V66
{
    
    
    
    BOOL standalone_Mode;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    standalone_Mode = [defaults boolForKey:@"ActiveStandalonemode"];
    
    if(!standalone_Mode)
    {
        
        NSArray * electrodestatus =[[NSArray alloc]initWithObjects:@"True",@"False", nil];
    
    
    CommonHelper *commhelp=[[CommonHelper alloc]init];
    NSDate *date = [[NSDate alloc] init];
    NSLog(@"%@", date);
    NSString *ModifiedDatetime=[commhelp getDateUTC:@"yyyy-MM-dd" :@"MM/dd/yyyy HH:mm:ss" :DateAsNSDate :@"2014-04-25" :date];
    
    NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
  
    NSString * patientid = [userDefault valueForKey:@"PatientID"];
    NSString * deviceid = [userDefault valueForKey:@"Deviceid"];
    
    NSString *domainname =[[NSUserDefaults standardUserDefaults] stringForKey:@"DomainName"];
    
    
    NSString * Fetchingpath=[NSString stringWithFormat:@"https://%@/MCC/MCCWcfService.svc/ECGElectrodeStatus/?PatientID=%@&DeviceID=%@&RA=%@&LA=%@&LL=%@&RL=%@&V1=%@&V2=%@&V3=%@&V4=%@&V5=%@&V6=%@&createddt=%@&modifieddt=%@",domainname,patientid,deviceid,electrodestatus[RA],electrodestatus[LA],electrodestatus[LL],electrodestatus[RL],electrodestatus[V11],electrodestatus[V22],electrodestatus[V33],electrodestatus[V44],electrodestatus[V55],electrodestatus[V66],ModifiedDatetime,ModifiedDatetime];
    
    
    NSLogger *logger=[[NSLogger alloc]init];
    
    NSString *bookurl=[Fetchingpath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *URL1=[NSURL URLWithString:bookurl];
    
    NSLog(@"url%@",URL1);
    
    NSURLRequest *request = [NSURLRequest requestWithURL:URL1];
    
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSLog(@"urlresp%@",string);
        
        
        if([string containsString:@"true"])
        {
            
            
            logger.degbugger = true;
            [logger log:@"Server Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"Success", @"Electrode Status", nil] error:TRUE];
            
        }
        else
        {
            logger.degbugger = true;
            [logger log:@"Server Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"Failure", @"Electrode Status", nil] error:TRUE];
            
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
      /*  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error updating Vital Param"
                                                            message:[error localizedDescription]
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];*/
        
        
       // [alertView show];
        
        logger.degbugger = true;
        [logger log:@"Server Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:[error localizedDescription], @"Electrode Status", nil] error:TRUE];
        
    }];
    
    
    [operation start];
        
    }
}

-(void)UpdatePatientUnit
{
    NSString *TempUnit,*WeightUnit,*HeightUnit;
    
    NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
    NSString *temperaturetype=[TempDefault objectForKey:@"TEMPERATURE"];
    if([temperaturetype isEqualToString:@"CELSIUS"])
    {
       TempUnit=@"Celsius";
    }
    else
    {
        
        TempUnit=@"Fahrenheit";
    }
    
    
    
    NSString *weighttype=[TempDefault objectForKey:@"WEIGHT"];
    if([weighttype isEqualToString:@"KILOGRAM"])
    {
        WeightUnit=@"Kilogram";
    }
    else
    {
        
        WeightUnit=@"Pound";
    }
    
    
    NSString *heighttype=[TempDefault objectForKey:@"HEIGHT"];
    if([heighttype isEqualToString:@"FEET AND INCHES"])
    {
        HeightUnit=@"Feet";
    }
    else
    {
        
        HeightUnit=@"Centimeter";
    }
    
    
    
    
    CommonHelper *commhelp=[[CommonHelper alloc]init];
    NSDate *date = [[NSDate alloc] init];
    NSLog(@"%@", date);
    NSString *ModifiedDatetime=[commhelp getDateUTC:@"yyyy-MM-dd" :@"MM/dd/yyyy HH:mm:ss" :DateAsNSDate :@"2014-04-25" :date];
    
    NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];

    
    NSString * patientid = [userDefault valueForKey:@"PatientID"];
   
    
    NSString *domainname =[[NSUserDefaults standardUserDefaults] stringForKey:@"DomainName"];
    
    
    NSString * Fetchingpath=[NSString stringWithFormat:@"https://%@/MCC/MCCWcfService.svc/InsertTemperatureUnit/?UserID=%@&Tempunit=%@&Weightunit=%@&HeightUnit=%@&Createdfrom=%@&Modifyfrom=%@&CreatedDatetime=%@&ModifyDatetime=%@",domainname,patientid,TempUnit,WeightUnit,HeightUnit,@"MCM",@"MCM",ModifiedDatetime,ModifiedDatetime];
    
    
    NSLogger *logger=[[NSLogger alloc]init];
    
    NSString *bookurl=[Fetchingpath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *URL1=[NSURL URLWithString:bookurl];
    
    NSLog(@"url%@",URL1);
    
    NSURLRequest *request = [NSURLRequest requestWithURL:URL1];
    
    
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSLog(@"urlresp%@",string);
        
        
        if([string containsString:@"Success"])
        {
            
            
            logger.degbugger = true;
            [logger log:@"Server Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"Success", @"Patient Unit", nil] error:TRUE];
            
        }
        else
        {
            logger.degbugger = true;
            [logger log:@"Server Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"Failure", @"Patient Unit", nil] error:TRUE];
            
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        /*  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error updating Vital Param"
         message:[error localizedDescription]
         delegate:nil
         cancelButtonTitle:@"Ok"
         otherButtonTitles:nil];*/
        
        
        // [alertView show];
        
        logger.degbugger = true;
        [logger log:@"Server Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:[error localizedDescription], @"Patient Unit ", nil] error:TRUE];
        
    }];
    
    
    [operation start];
    

}


-(void)UploadingEmail_sms
{
    NSMutableArray *results = [[NSMutableArray alloc]init];
    NSString *Emails_statuscode=@"1";
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@";
    NSArray* args = @[@"event_mail_sms",Emails_statuscode];
    if (Emails_statuscode.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1)
    {
        
        
        NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"EventStorage"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *EventSett = (NSManagedObject *)[results objectAtIndex:0];
            
            ViewRecordedEcgViewController *recordecg=[ViewRecordedEcgViewController alloc];
            
            [recordecg plotting1:[EventSett valueForKey:@"event_code"] :[EventSett valueForKey:@"event_datetime"]:[EventSett valueForKey:@"event_value"] :[EventSett valueForKey:@"event_ecgfilepath"]];
            [self sendEmailInBackground:[EventSett valueForKey:@"event_code"] :[EventSett valueForKey:@"event_datetime"]];
            
        }
    }

    
}



-(void)mcmVersionDetails :(NSString *)hardwareVersion
{
         BOOL standalone_Mode;
   
    NSString * softwareversion = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    
    
        
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        standalone_Mode = [defaults boolForKey:@"ActiveStandalonemode"];
        
        if(!standalone_Mode)
        {
            CommonHelper *commhelp=[[CommonHelper alloc]init];
            NSDate *date = [[NSDate alloc] init];
            NSLog(@"%@", date);
            NSString *createddt=[commhelp getDateUTC:@"yyyy-MM-dd" :@"MM/dd/yyyy HH:mm:ss" :DateAsNSDate :@"2014-04-25" :date];
            
            NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
           
            NSString * patientid = [userDefault valueForKey:@"PatientID"];
            NSString * deviceid = [userDefault valueForKey:@"Deviceid"];
            
            NSString *domainname =[[NSUserDefaults standardUserDefaults] stringForKey:@"DomainName"];
            
            
            NSString * Fetchingpath=[NSString stringWithFormat:@"https://%@/MCC/MCCWcfService.svc/MCMVersionDetails/?PatientID=%@&DeviceID=%@&HardwareVersion=%@&SoftwareVersion=%@&createddt=%@",domainname,patientid,deviceid,hardwareVersion,softwareversion,createddt];
            
            
            NSLogger *logger=[[NSLogger alloc]init];
            
            NSString *bookurl=[Fetchingpath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *URL1=[NSURL URLWithString:bookurl];
            
            NSLog(@"url%@",URL1);
            
            NSURLRequest *request = [NSURLRequest requestWithURL:URL1];
            
            
            
            AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
            
            operation.responseSerializer = [AFHTTPResponseSerializer serializer];
            
            
            [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                NSLog(@"urlresp%@",string);
                
                
                NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
                id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                
                
                if([ [json objectForKey:@"status"] isEqualToString:@"Success"])
                {
                    
                    
                    logger.degbugger = true;
                    [logger log:@"Server Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"Success", @"MCM VersionDetails", nil] error:TRUE];
                    
                }
                else
                {
                    logger.degbugger = true;
                    [logger log:@"Server Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"Failure", @"MCM VersionDetails", nil] error:TRUE];
                    
                }
                
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                
                logger.degbugger = true;
                [logger log:@"Server Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:[error localizedDescription], @"MCM VersionDetails", nil] error:TRUE];
                
            }];
            
            
            [operation start];
        }

    
}
- (long) getCurrentNetworkTime{
    return ([[NSDate date] timeIntervalSince1970]);
}


-(void) sendEmailInBackground :(NSString *)evencde :(NSString *)eventdatetime{
    NSLog(@"Start Sending");
    GlobalVars *globals = [GlobalVars sharedInstance];
    globals.Email_EventCode=evencde;
    globals.Email_EventDatetime=eventdatetime;
    
    CommonHelper *commhelp=[[CommonHelper alloc]init];
    NSString *localeventtime=[commhelp UTCDateInYourFormat:@"yyyy-MM-dd HH:mm:ss" :@"yyyy-MM-dd HH:mm:ss" :eventdatetime];
    
    test_smtp_message = [[SKPSMTPMessage alloc] init];
    test_smtp_message.fromEmail = @"admin@personal-healthwatch.com";;
    //test_smtp_message.toEmail = @"pramody@maestros.net";
    test_smtp_message.relayHost =  @"m.outlook.com";
    test_smtp_message.requiresAuth = YES;
    test_smtp_message.login = @"admin@personal-healthwatch.com";
    test_smtp_message.pass = @"Ventricular01";
    test_smtp_message.wantsSecure = YES; // smtp.gmail.com doesn't work without TLS!
    
    //    test_smtp_message.bccEmail = @"testbcc@test.com";
    
    // Only do this for self-signed certs!
    // test_smtp_message.validateSSLChain = NO;
    test_smtp_message.delegate = self;
    
    NSMutableArray *parts_to_send = [NSMutableArray array];
    
    
    
    
    
    //tempertature type
    NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
    NSString *temperaturetype=[TempDefault objectForKey:@"TEMPERATURE"];
    NSString *tempunit;
    NSString *htmlString;
    int respira=0;
    if([temperaturetype isEqualToString:@"CELSIUS"])
    {
        tempunit= [NSString stringWithFormat:@"C%@", @"\u00B0"];
        
    }
    else
    {
        tempunit=[NSString stringWithFormat:@"F%@", @"\u00B0"];
    }
    
     bool docemailflag=false;
    
    //eventvalue
    NSString * patienLoc,*EventReason,*posture;
    NSMutableArray *results = [[NSMutableArray alloc]init];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ &&%K == %@";
    NSArray* args = @[@"event_datetime",eventdatetime,@"event_code",evencde];
    if (eventdatetime.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
        NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"EventStorage"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *Eventsett = (NSManagedObject *)[results objectAtIndex:0];
           
            posture=[Eventsett valueForKey:@"event_posture_code"];
            
            //Posture
            NSString *posturetext;
            if([posture isEqualToString:@"1"])
            {
                 respira=[[Eventsett valueForKey:@"event_respiration_onoff"]intValue];
                posturetext=@"Laying";
            }
            else if([posture isEqualToString:@"2"])
            {
                posturetext=@"Running";
                
            }
            else if([posture isEqualToString:@"3"])
            {
                posturetext=@"Walking";
            }
            else
            {
                posturetext=@"Standing";
            }
            
            
            if([[Eventsett valueForKey:@"event_reason"] containsString:@"_"])
            {
                if([[Eventsett valueForKey:@"event_reason"] isEqualToString:@"Patient_Call_Event"])
                {
                    
                    EventReason =[[Eventsett valueForKey:@"event_reason"] stringByReplacingOccurrencesOfString:@"_" withString:@""];
                }
                else
                {
                    EventReason =[NSString stringWithFormat:@"suspected %@",[[Eventsett valueForKey:@"event_reason"] stringByReplacingOccurrencesOfString:@"_" withString:@""]];
                    
                }
                
            }
            else
            {
                EventReason =[NSString stringWithFormat:@"suspected %@",[Eventsett valueForKey:@"event_reason"]];
            }
            
            //patientdetail
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
            
            
            NSEntityDescription *entity = [NSEntityDescription entityForName:@"PatientDetails" inManagedObjectContext:self.managedObjectContext];
            [fetchRequest setEntity:entity];
            
            NSError *error = nil;
            NSString *patientname;
           
            NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
            
            if (error) {
                NSLog(@"Unable to execute fetch request.");
                NSLog(@"%@, %@", error, error.localizedDescription);
                
            } else {
                //NSLog(@"%@", result);
                if (result.count > 0) {
                    NSManagedObject *patient = (NSManagedObject *)[result objectAtIndex:0];
                    //NSLog(@"1 - %@", patient);
                     test_smtp_message.toEmail = [patient valueForKey:@"doc_email"];
                    NSString *docemail=[patient valueForKey:@"doc_email"];
                    if(docemail.length!=0)
                    {
                        docemailflag=true;
                    }
                    else
                        docemailflag=false;
                    BOOL savedSwitch= [[NSUserDefaults standardUserDefaults] boolForKey:@"SwitchState"];
                    if(savedSwitch)
                    {
                        patienLoc=[NSString stringWithFormat:@"For User Location Click<a href='http://maps.google.com/maps?z=18&q=%@,%@'>Here</a>",[Eventsett valueForKey:@"event_latitude"],[Eventsett valueForKey:@"event_longitude"]];
                        
                    }
                    else
                    {
                        patienLoc=@"";
                        // patienLoc=[NSString stringWithFormat:@"For User Location Click<a href='http://maps.google.com/maps?z=18&q=%@,%@'>Here</a>",@"19.1178",@"73.0269"];
                    }
                    
                    patientname=[NSString stringWithFormat:@"%@ %@",[patient valueForKey:@"given_name"],[patient valueForKey:@"family_name"]];
                    if(respira==1)
                    {
                        
                        htmlString =
                        [NSString stringWithFormat: @"<html><body>Dear Dr. %@,<br><br>You have received an ECG report of patient %@ , ID %@. The event is of type %@.<br>Please check the attached ECG graph and ECG data for reference.<br>You can contact the patient at %@ <br> <br>Patient ID : %@<br>Name : %@ <br>Event Date & Time : %@<br>HR : %@ bpm<br>Resp Amp :%@ mv <br>Resp Var :%@ <br>RR :%@ msec<br>PR : %@ msec<br>QRS : %@ msec<br>QT : %@ msec<br>QTC :%@msec<br>Temperature :%@  %@<br>Posture :%@<br><br>Regards,<br>Master Caution Admin<br><br>%@",[patient valueForKey:@"doc_name"],patientname,[patient valueForKey:@"uid"],EventReason,[patient valueForKey:@"patient_mobno"],[patient valueForKey:@"uid"],patientname,localeventtime,[Eventsett valueForKey:@"event_heartrate"],[Eventsett valueForKey:@"event_respiration_amplitude"],[Eventsett valueForKey:@"event_resp_variance"],[Eventsett valueForKey:@"event_rr_interval"],[Eventsett valueForKey:@"event_pr_interval"],[Eventsett valueForKey:@"event_qrs_interval"],[Eventsett valueForKey:@"event_qt_interval"],[Eventsett valueForKey:@"event_qtc_interval"],[NSString stringWithFormat:@"%.1f",[[Eventsett valueForKey:@"event_temp"]floatValue]],tempunit,posturetext,patienLoc];
                    }
                    else
                    {
                        htmlString =
                        [NSString stringWithFormat: @"<html><body>Dear Dr. %@,<br><br>You have received an ECG report of patient %@ , ID %@. The event is of type %@.<br>Please check the attached ECG graph and ECG data for reference.<br>You can contact the patient at %@ <br> <br>Patient ID : %@<br>Name : %@ <br>Event Date & Time : %@<br>HR : %@ bpm<br>RR :%@ msec<br>PR : %@ msec<br>QRS : %@ msec<br>QT : %@ msec<br>QTC :%@msec<br>Temperature :%@  %@<br>Posture :%@<br><br>Regards,<br>Master Caution Admin<br><br>%@",[patient valueForKey:@"doc_name"],patientname,[patient valueForKey:@"uid"],EventReason,[patient valueForKey:@"patient_mobno"],[patient valueForKey:@"uid"],patientname,localeventtime,[Eventsett valueForKey:@"event_heartrate"],[Eventsett valueForKey:@"event_rr_interval"],[Eventsett valueForKey:@"event_pr_interval"],[Eventsett valueForKey:@"event_qrs_interval"],[Eventsett valueForKey:@"event_qt_interval"],[Eventsett valueForKey:@"event_qtc_interval"],[NSString stringWithFormat:@"%.1f",[[Eventsett valueForKey:@"event_temp"]floatValue]],tempunit,posturetext,patienLoc];
                    }
                    
                    test_smtp_message.subject =[NSString stringWithFormat:@"ECG Report %@",[patient valueForKey:@"family_name"]];
                }
            }
            
            
            
            
        }
        
        
        
    }
    
    
     if(docemailflag)
     {
    NSString *msg22=[self getEmailTable:eventdatetime];
    //NSLog(@"msg2%@",msg22);
    htmlString= [htmlString stringByAppendingString:msg22];
    
    
    NSAttributedString *messagebody = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
    //If you are not sure how to format your message part, send an email to your self.
    //In Mail.app, View > Message> Raw Source to see the raw text that a standard email client will generate.
    //This should give you an idea of the proper format and options you need
    NSDictionary *plain_text_part = [NSDictionary dictionaryWithObjectsAndKeys:
                                     @"text/plain\r\n\tcharset=UTF-8;\r\n\tformat=flowed", kSKPSMTPPartContentTypeKey,
                                     messagebody.string, kSKPSMTPPartMessageKey,
                                     @"quoted-printable", kSKPSMTPPartContentTransferEncodingKey,
                                     nil];
    [parts_to_send addObject:plain_text_part];
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *docs = [paths objectAtIndex:0];
    NSString * pathname=[NSString stringWithFormat:@"ECG_Mail.PDF"];
    NSString* path =  [docs stringByAppendingFormat:@"/%@",pathname];
    //NSString *image_path = [[NSBundle mainBundle] pathForResource:@"Qualify" ofType:@"png"];
    NSData *image_data = [NSData dataWithContentsOfFile:path];
    NSDictionary *image_part = [NSDictionary dictionaryWithObjectsAndKeys:
                                @"inline;\r\n\tfilename=\"ECG_Mail.PDF\"",kSKPSMTPPartContentDispositionKey,
                                @"base64",kSKPSMTPPartContentTransferEncodingKey,
                                @"application/pdf;\r\n\tname=ECG_Mail.PDF;\r\n\tx-unix-mode=0666",kSKPSMTPPartContentTypeKey,
                                [image_data encodeWrappedBase64ForData],kSKPSMTPPartMessageKey,
                                nil];
    [parts_to_send addObject:image_part];
    
    NSDictionary *htmlPart = [NSDictionary dictionaryWithObjectsAndKeys:@"text/html",kSKPSMTPPartContentTypeKey,                    htmlString,kSKPSMTPPartMessageKey,@"8bit",kSKPSMTPPartContentTransferEncodingKey, nil];
    test_smtp_message.parts = [NSArray arrayWithObjects:htmlPart,image_part,nil];
    
    //test_smtp_message.parts = parts_to_send;
    
    
    HighestState = 0;
    
   
    [test_smtp_message send];
    
     }
    
    
    /* MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
     controller.mailComposeDelegate = self;
     [controller setSubject:@"Check this route out"];
     [controller setMessageBody:@"Attaching a shot of covered route." isHTML:NO];
     
     // MAKING A SCREENSHOT
     UIGraphicsBeginImageContext(scrollView.frame.size);
     [scrollView.layer renderInContext:UIGraphicsGetCurrentContext()];
     UIImage *screenshot = UIGraphicsGetImageFromCurrentImageContext();
     UIGraphicsEndImageContext();
     
     
     
     // ATTACHING A SCREENSHOT
     NSData *myData = UIImagePNGRepresentation(screenshot);
     [controller addAttachmentData:myData mimeType:@"image/png" fileName:@"route"];    [controller addAttachmentData:myData mimeType:@"image/png" fileName:@"route"];
     
     // SHOWING MAIL VIEW
     [self presentModalViewController:controller animated:YES];*/
}


-(NSString *)getEmailTable :(NSString *)Eventdttime
{
    
    NSString *s1=@"";
    s1=[s1 stringByAppendingString:[NSString stringWithFormat:@"<br>Last month patient event history:<br><table border = '1px'><tr><td align = 'center'>Date & Time</td><td align = 'center'>Event</td></tr>"]];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
     CommonHelper *commhelp=[[CommonHelper alloc]init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"EventStorage" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        //NSLog(@"%@", result);
        if (result.count > 0) {
            for(int j=0;j<result.count;j++)
            {
                NSManagedObject *eventstorage = (NSManagedObject *)[result objectAtIndex:j];
                NSString *localeventtime=[commhelp UTCDateInYourFormat:@"yyyy-MM-dd HH:mm:ss" :@"yyyy-MM-dd HH:mm:ss" :[eventstorage valueForKey:@"event_datetime"]];
                s1=[s1 stringByAppendingString:[NSString stringWithFormat:@"<tr><td>%@</td><td>%@</td></tr>",localeventtime,[eventstorage valueForKey:@"event_reason"]]];
                
                
            }
            
        }
    }
		  
    
    // finish html
    
    s1=[s1 stringByAppendingString:[NSString stringWithFormat:@"</table></body></html>"]];
    return s1;
    
    
}


#pragma mark SKPSMTPMessage Delegate Methods
- (void)messageState:(SKPSMTPState)messageState;
{
    NSLog(@"HighestState:%lu", (unsigned long)HighestState);
    if (messageState > HighestState)
        HighestState = messageState;
    
}
- (void)messageSent:(SKPSMTPMessage *)SMTPmessage
{
    GlobalVars *globals = [GlobalVars sharedInstance];
    
    [self sms:[globals Email_EventCode] :[globals Email_EventDatetime]];
    
    
    NSMutableArray *results = [[NSMutableArray alloc]init];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ &&%K == %@";
    NSArray* args = @[@"event_datetime",[globals Email_EventDatetime],@"event_code",[globals Email_EventCode]];
    if ([globals Email_EventDatetime].length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
        //NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"EventStorage"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *Eventsett = (NSManagedObject *)[results objectAtIndex:0];
            [Eventsett setValue:@"0" forKey:@"event_mail_sms"];
        }
    }
    
   /* UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success" message:@"Sent!"
                                                   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];*/
    //DEBUGLOG(@"delegate - message sent");
    
}
- (void)messageFailed:(SKPSMTPMessage *)SMTPmessage error:(NSError *)error
{
    
    NSLogger * logger=[[NSLogger alloc]init];
    logger.degbugger = true;
    
    [logger log:@"Email Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:[error localizedDescription], @"Unable to connect Server", nil] error:TRUE];
   /* UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription]
                                                   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];*/
    
    // DEBUGLOG(@"delegate - error(%d): %@", [error code], [error localizedDescription]);
}


-(void)sms :(NSString *)eventcde :(NSString *)evntdatetime
{
    
    NSLog(@"sending sms");
    NSString *kTwilioSID = @"AC6f77158319849f47814f2be32232d49b";
    NSString *kTwilioSecret = @"e7b97f44bae771a72a576a457c34b32c";
    NSString *kFromNumber = @"+12013971746";
    NSString *kToNumber;// = @"%2B917738626052";
    NSString *kMessage ;//= @"Hi this Health Watch Team";
    
    //eventstorage
    NSString * EventReason,*mesg;
    NSMutableArray *results = [[NSMutableArray alloc]init];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ &&%K == %@";
    NSArray* args = @[@"event_datetime",evntdatetime,@"event_code",eventcde];
    if (evntdatetime.length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
        //NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"EventStorage"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            
            NSManagedObject *Eventsett = (NSManagedObject *)[results objectAtIndex:0];
            
            if([[Eventsett valueForKey:@"event_reason"] containsString:@"_"])
            {
                if([[Eventsett valueForKey:@"event_reason"] isEqualToString:@"Patient_Call_Event"])
                {
                    
                    EventReason =[[Eventsett valueForKey:@"event_reason"] stringByReplacingOccurrencesOfString:@"_" withString:@""];
                }
                else
                {
                    EventReason =[NSString stringWithFormat:@"suspected %@",[[Eventsett valueForKey:@"event_reason"] stringByReplacingOccurrencesOfString:@"_" withString:@""]];
                    
                }
                
            }
            else
            {
                EventReason =[NSString stringWithFormat:@"suspected %@",[Eventsett valueForKey:@"event_reason"]];
            }
            
        }
        
        
        //patientdetail
        NSFetchRequest *fetchRequest1 = [[NSFetchRequest alloc] init];
        
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"PatientDetails" inManagedObjectContext:self.managedObjectContext];
        [fetchRequest1 setEntity:entity];
        
        NSError *error = nil;
        NSArray *result1 = [self.managedObjectContext executeFetchRequest:fetchRequest1 error:&error];
        
        if (error) {
            NSLog(@"Unable to execute fetch request.");
            NSLog(@"%@, %@", error, error.localizedDescription);
            
        }
        else
        {
            //NSLog(@"%@", result);
            if (result1.count > 0) {
                NSManagedObject *patient = (NSManagedObject *)[result1 objectAtIndex:0];
                
                kToNumber=[NSString stringWithFormat:@"%@%@",@"%2B",[patient valueForKey:@"doc_ph_no"]];
                
                mesg=[NSString stringWithFormat:@"Dear Dr. %@ you have received an ECG report for %@ from %@ ,ID %@. To view the ECG data please check your E-Mail inbox. You can call the patient at the following number %@ ",[patient valueForKey:@"doc_name"],EventReason,[patient valueForKey:@"patient_name"],[patient valueForKey:@"patient_id"],[patient valueForKey:@"patient_mobno"]];
                
            }
        }
    }
    kMessage=mesg;
    
    NSString *urlString = [NSString
                           stringWithFormat:@"https://%@:%@@api.twilio.com/2010-04-01/Accounts/%@/Messages/",
                           kTwilioSID, kTwilioSecret,kTwilioSID];
    
    
    // NSString *urlString=@"https://api.twilio.com/2010-04-01/Accounts/AC6f77158319849f47814f2be32232d49b/Messages";
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    
    // Set up the body
    NSString *bodyString = [NSString stringWithFormat:@"From=%@&To=%@&Body=%@", kFromNumber, kToNumber, kMessage];
    NSData *data = [bodyString dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:data];
    NSError *error;
    NSURLResponse *response;
    NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    // Handle the received data
    if (error) {
        NSLog(@"Error: %@", error);
    } else {
        NSString *receivedString = [[NSString alloc]initWithData:receivedData encoding:NSUTF8StringEncoding];
        NSLog(@"Request sent. %@", receivedString);
    }
    
    
    
}




- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

@end
