//
//  ThresholdSett.m
//  HealthWatch
//
//  Created by METSL MAC MINI on 31/07/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import "ThresholdSett.h"


@implementation ThresholdSett

@dynamic code;
@dynamic created_date_time;
@dynamic created_from;
@dynamic device_id;
@dynamic flag;
@dynamic modify_datetime;
@dynamic modify_from;
@dynamic patient_id;
@dynamic posture;
@dynamic rd;
@dynamic status;
@dynamic yd;
@dynamic update_to_device;
@dynamic updated_to_site;

@end
