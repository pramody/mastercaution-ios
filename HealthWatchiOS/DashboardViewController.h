//
//  DashboardViewController.h
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 12/06/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//


#import "UploadService.h"

@interface DashboardViewController : UIViewController<ECSlidingViewControllerDelegate>
{
     NSString *latitude,*longititude;
    NSString * patientid;
    NSString *deviceid;
     NSString *location;
    
   // UploadService * WCFService;
    //UploadService * bluetoothService;
}

@property (nonatomic, strong) IBOutlet UIView *bodyMainView;
@property (nonatomic, strong) IBOutlet UIView*footerMainView;
@property (nonatomic, strong) IBOutlet UIView *orangeMainView;
@property (nonatomic, strong) IBOutlet UIView *centerMainView;

@property (nonatomic, strong) IBOutlet UIView *  builtInSegmentMainView;
@property (nonatomic, strong) IBOutlet UIView *  tabDiv1;
@property (nonatomic, strong) IBOutlet UIView *  tabDiv2;
@property (nonatomic, strong) IBOutlet UIView *  tabDiv3;

@property (nonatomic, strong) IBOutlet UIButton *  userCallButton;
@property (nonatomic, strong) IBOutlet UIImageView *  load1;
@property (nonatomic, strong) IBOutlet UIImageView *  load2;
@property (nonatomic, strong) IBOutlet UIImageView *  load3;
@property (nonatomic, strong) IBOutlet UIImageView *  load4;


@property (nonatomic, strong) IBOutlet UILabel *  lblpatientname;
@property (nonatomic, strong) IBOutlet UILabel *  lblpatientlevel;
@property (nonatomic, strong) IBOutlet UILabel *  lblmessage;
@property (nonatomic, strong) IBOutlet UILabel *  lbldate;
@property (nonatomic, strong) IBOutlet UILabel *  lbltime;

@property (nonatomic, strong) IBOutlet UILabel *  lbltempvalue;
@property (nonatomic, strong) IBOutlet UILabel *  lblBPMvalue;
@property (nonatomic, strong) IBOutlet UILabel *  lblRPMvalue;
@property (nonatomic, strong) IBOutlet UIButton *  lblRPMimage;
@property (nonatomic, strong) IBOutlet UIButton *  btntempunit;
@property (nonatomic, strong) IBOutlet UIButton *  btnbatterystatus;
@property (nonatomic, strong) IBOutlet UIButton *  btngarmentstatus;
@property (nonatomic, strong) IBOutlet UIButton *  btnbluetoothstatus;
@property (nonatomic, strong) IBOutlet UIButton *  btncloudstatus;



@property (nonatomic, strong) IBOutlet UIImageView * userIconImageView;

@property (nonatomic, strong) IBOutlet UIView * userDetailsMainView;

-(IBAction)leftButtonClick:(id)sender;
-(IBAction)rightButtonClick:(id)sender;
-(IBAction)ViewRecordEcgClick:(id)sender;
-(IBAction)Orangebtnclose:(id)sender;
-(IBAction)handleFooterDrag:(UIPanGestureRecognizer*)recognizer;
- (void) updatetemp;
- (void)updateservicestatus;

@end
