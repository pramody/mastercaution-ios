//
//  CommonHelper.h
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 29/06/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import <Foundation/Foundation.h>

enum
{
    DateAsNSDate,
    DateAsNSString
};typedef NSInteger customDateFormat;

@interface CommonHelper : NSObject
{
   
   
}

-(NSString *) getBmiValueForHeightInFeetInches:(float)heightInFeets  :(float)heightInInches   : (float)weightInKgs;
-(NSString *) trimTheString:(NSString *)stringToTrim;
-(NSString *) getDateInYourFormat:(NSString *)actualDateFormat : (NSString*) youWantDateFormat  : (customDateFormat)needDateAs   :(NSString *)dateAsNSString  :(NSDate*)dateAsNSDate;
-(BOOL)isEmailValid:(NSString *)emailAddress  :(BOOL)stric;
-(NSString *)celsiustoFahernheit:(NSString *)celsius;
-(NSString *)Fahernheittocelsius:(NSString *)fahrenheit;
-(NSString *) UTCDateInYourFormat:(NSString *)actualDateFormat : (NSString*) youWantDateFormat  :(NSString *)dateAsNSString;

-(NSString *) getDateUTC:(NSString *)actualDateFormat : (NSString*) youWantDateFormat  : (customDateFormat)needDateAs   :(NSString *)dateAsNSString  :(NSDate*)dateAsNSDate;






@end
