//
//  AppDelegate.m
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 12/06/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import "AppDelegate.h"
#import "ECSlidingViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "EventStorage.h"
//xmpp
#import "GCDAsyncSocket.h"
#import "XMPP.h"
#import "XMPPLogging.h"
#import "XMPPReconnect.h"
#import "XMPPCapabilitiesCoreDataStorage.h"
#import "XMPPRosterCoreDataStorage.h"
#import "XMPPvCardAvatarModule.h"
#import "XMPPvCardCoreDataStorage.h"
#import "XMPPPing.h"
#import "DDLog.h"
#import "DDTTYLogger.h"
#import "PatientDetails.h"
#import "MCDSettings.h"
#import "SuspensionSettings.h"
#import "ThresholdSett.h"
#import "DoctorThreshold.h"
#import <CFNetwork/CFNetwork.h>
#import "CommonHelper.h"
#import <EventKit/EventKit.h>


#import "SDCAlertView.h"
#import "SDCAlertViewController.h"
#import <UIView+SDCAutoLayout.h>
#import "SDCAlertController.h"
#import "WCFUpload.h"




// Log levels: off, error, warn, info, verbose
#if DEBUG
static const int ddLogLevel = LOG_LEVEL_VERBOSE;
#else
static const int ddLogLevel = LOG_LEVEL_INFO;
#endif

@interface AppDelegate ()<CLLocationManagerDelegate>
- (void)setupStream;
- (void)teardownStream;

- (void)goOnline;
- (void)goOffline;
@end

@implementation AppDelegate
{
    CLLocationManager *locationManager;
}
@synthesize xmppStream;
@synthesize xmppReconnect;
@synthesize xmppRoster;
@synthesize xmppRosterStorage;
@synthesize xmppvCardTempModule;
@synthesize xmppvCardAvatarModule;
@synthesize xmppCapabilities;
@synthesize xmppCapabilitiesStorage;
@synthesize xmppPing;
@synthesize orientationKey,doctorSessionFlagBool,doctorSessionTimer;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    UIStoryboard* storyboard;
     //[[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    //location alert
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways) {
        [locationManager startUpdatingLocation];
    }else{
        [locationManager requestWhenInUseAuthorization];
        [locationManager requestAlwaysAuthorization];
    }
    
    
    //Notification alert
    if([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]) {
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert) categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        
    } else {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge];
        
    }
    
    
    

    UILocalNotification *locationNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if (locationNotification) {
        // Set icon badge number to zero
        application.applicationIconBadgeNumber = 0;
    }
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        storyboard = [UIStoryboard storyboardWithName:@"Mainstoryboard_iPad" bundle:nil];
    } else
    {
        storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    }
   // UIViewController *currentVC ;
     orientationKey = @"PRO";
     //doctorSessionFlagBool;
     //doctorSessionTimer ;
    
   
    GlobalVars *global=[GlobalVars sharedInstance];
    global.BluetoothConnectionflag=1;
    
    //xmpp
     [self setupStream];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL hasRunBefore = [defaults boolForKey:@"FirstRun"];
    
    if (!hasRunBefore)
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Mainstoryboard_iPad" bundle:nil];
            UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"OneTimeViewController"];
            self.window.rootViewController=vc;
        }
        else
        {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"OneTimeViewController"];
        self.window.rootViewController=vc;
        }

        
    }
    else
    {
    
        
        
       if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Mainstoryboard_iPad" bundle:nil];
            ECSlidingViewController * controller = (ECSlidingViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ECSlidingStoryboardId"];
            
            self.window.rootViewController=controller;

        }
        else
        {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ECSlidingViewController * controller = (ECSlidingViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ECSlidingStoryboardId"];
        
       // UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"IDD"];
        self.window.rootViewController=controller;
        }
    }
    
     NSURL *tempUrl =  [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    
    NSLog(@"tempUrl %@", tempUrl);
    
    
    
    return YES;
}


-(void)startxmpp
{
    [DDLog addLogger:[DDTTYLogger sharedInstance] withLogLevel:XMPP_LOG_FLAG_SEND_RECV];
    
   
    
    if (![self connect])
    {
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.0 * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
          
            NSLogger *logger=[[NSLogger alloc]init];
            logger.degbugger = true;
            
            [logger log:@"NULL" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"Xmpp started", @"Loading of app", nil] error:false];
            // [navigationController presentViewController:settingsViewController animated:YES completion:NULL];
        });
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
   
    
    DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    
#if TARGET_IPHONE_SIMULATOR
    DDLogError(@"The iPhone simulator does not process background network traffic. "
               @"Inbound traffic is queued until the keepAliveTimeout:handler: fires.");
#endif
    
    if ([application respondsToSelector:@selector(setKeepAliveTimeout:handler:)])
    {
        [application setKeepAliveTimeout:600 handler:^{
            
            DDLogVerbose(@"KeepAliveHandler");
            
          
            // Do other keep alive stuff here.
        }];
    }
    
    
   

}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];

}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    
    [self teardownStream];

    [self saveContext];
}

-(UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window{
    
    //println("CURRENT VC \(currentVC)")
    if ([orientationKey isEqualToString:@"LAN"])
    {
        
        //let eventThresholdsController = currentVC as EventThresholdsSettingsViewController
        
        //println(" ISPresent == \(eventThresholdsController.isPresent)")
        return  (UIInterfaceOrientationMaskLandscapeLeft)
        | (UIInterfaceOrientationMaskLandscapeRight);
    
       
    }
    return (UIInterfaceOrientationMaskPortrait); //Int(UIInterfaceOrientationMask.All.rawValue);
}



#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.HealthWatchiOS.app.HealthWatchiOS" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"HealthWatchiOS" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"HealthWatchiOS.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}






-(void)forceOrientationTakePlace:(NSString*)orientKey
{
    //Either "LAN" => Landscape / "PRO" => Protrait
    if ([orientKey isEqualToString:@"LAN"])
    {
        NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationLandscapeLeft];
        [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
       
    }
    else
    {
        NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
        [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
      
    }
}




- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    //UIApplicationState state = [application applicationState];
   /* if (state == UIApplicationStateActive) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Reminder"
                                                        message:notification.alertBody
                                                       delegate:self cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }*/
    
    // Request to reload table view data
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"reloadData" object:self];
    
    // Set icon badge number to zero
   
    
    NSDictionary* userInfo = [notification userInfo];
    NSLog(@"UserInfo = %@", userInfo);
    
    NSString* beaconkey = [[notification userInfo] objectForKey:@"beaconKey"];
    
    if([beaconkey isEqualToString:@"ONDEMAND"]) {
        
        NSLog(@"This is notification 1");
       
            //Show an in-app banner
            //do tasks
            application.applicationIconBadgeNumber = 0;
           // [self performSelector:@selector(ondemandcall) withObject:nil afterDelay:0];
        

    }
    

}



//xmpp
- (void)dealloc
{
    [self teardownStream];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Core Data
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (NSManagedObjectContext *)managedObjectContext_roster
{
    return [xmppRosterStorage mainThreadManagedObjectContext];
}

- (NSManagedObjectContext *)managedObjectContext_capabilities
{
    return [xmppCapabilitiesStorage mainThreadManagedObjectContext];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Private
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)setupStream
{
    NSAssert(xmppStream == nil, @"Method setupStream invoked multiple times");
    
    // Setup xmpp stream
    //
    // The XMPPStream is the base class for all activity.
    // Everything else plugs into the xmppStream, such as modules/extensions and delegates.
    
    xmppStream = [[XMPPStream alloc] init];
    
#if !TARGET_IPHONE_SIMULATOR
    {
        // Want xmpp to run in the background?
        //
        // P.S. - The simulator doesn't support backgrounding yet.
        //        When you try to set the associated property on the simulator, it simply fails.
        //        And when you background an app on the simulator,
        //        it just queues network traffic til the app is foregrounded again.
        //        We are patiently waiting for a fix from Apple.
        //        If you do enableBackgroundingOnSocket on the simulator,
        //        you will simply see an error message from the xmpp stack when it fails to set the property.
        
        xmppStream.enableBackgroundingOnSocket = YES;
    }
#endif
    
    // Setup reconnect
    //
    // The XMPPReconnect module monitors for "accidental disconnections" and
    // automatically reconnects the stream for you.
    // There's a bunch more information in the XMPPReconnect header file.
    
    xmppReconnect = [[XMPPReconnect alloc] init];
    
    // Setup roster
    //
    // The XMPPRoster handles the xmpp protocol stuff related to the roster.
    // The storage for the roster is abstracted.
    // So you can use any storage mechanism you want.
    // You can store it all in memory, or use core data and store it on disk, or use core data with an in-memory store,
    // or setup your own using raw SQLite, or create your own storage mechanism.
    // You can do it however you like! It's your application.
    // But you do need to provide the roster with some storage facility.
    
    xmppRosterStorage = [[XMPPRosterCoreDataStorage alloc] init];
    //	xmppRosterStorage = [[XMPPRosterCoreDataStorage alloc] initWithInMemoryStore];
    
    xmppRoster = [[XMPPRoster alloc] initWithRosterStorage:xmppRosterStorage];
    
    xmppRoster.autoFetchRoster = YES;
    xmppRoster.autoAcceptKnownPresenceSubscriptionRequests = YES;
    
    // Setup vCard support
    //
    // The vCard Avatar module works in conjuction with the standard vCard Temp module to download user avatars.
    // The XMPPRoster will automatically integrate with XMPPvCardAvatarModule to cache roster photos in the roster.
    
    xmppvCardStorage = [XMPPvCardCoreDataStorage sharedInstance];
    xmppvCardTempModule = [[XMPPvCardTempModule alloc] initWithvCardStorage:xmppvCardStorage];
    
    xmppvCardAvatarModule = [[XMPPvCardAvatarModule alloc] initWithvCardTempModule:xmppvCardTempModule];
    
    // Setup capabilities
    //
    // The XMPPCapabilities module handles all the complex hashing of the caps protocol (XEP-0115).
    // Basically, when other clients broadcast their presence on the network
    // they include information about what capabilities their client supports (audio, video, file transfer, etc).
    // But as you can imagine, this list starts to get pretty big.
    // This is where the hashing stuff comes into play.
    // Most people running the same version of the same client are going to have the same list of capabilities.
    // So the protocol defines a standardized way to hash the list of capabilities.
    // Clients then broadcast the tiny hash instead of the big list.
    // The XMPPCapabilities protocol automatically handles figuring out what these hashes mean,
    // and also persistently storing the hashes so lookups aren't needed in the future.
    //
    // Similarly to the roster, the storage of the module is abstracted.
    // You are strongly encouraged to persist caps information across sessions.
    //
    // The XMPPCapabilitiesCoreDataStorage is an ideal solution.
    // It can also be shared amongst multiple streams to further reduce hash lookups.
    
    xmppCapabilitiesStorage = [XMPPCapabilitiesCoreDataStorage sharedInstance];
    xmppCapabilities = [[XMPPCapabilities alloc] initWithCapabilitiesStorage:xmppCapabilitiesStorage];
    
    xmppCapabilities.autoFetchHashedCapabilities = YES;
    xmppCapabilities.autoFetchNonHashedCapabilities = NO;
    
    // Activate xmpp modules
    
    [xmppReconnect         activate:xmppStream];
    [xmppRoster            activate:xmppStream];
    [xmppvCardTempModule   activate:xmppStream];
    [xmppvCardAvatarModule activate:xmppStream];
    [xmppCapabilities      activate:xmppStream];
    
    // Add ourself as a delegate to anything we may be interested in
    
    [xmppStream addDelegate:self delegateQueue:dispatch_get_main_queue()];
    [xmppRoster addDelegate:self delegateQueue:dispatch_get_main_queue()];
    
    // Optional:
    //
    // Replace me with the proper domain and port.
    // The example below is setup for a typical google talk account.
    //
    // If you don't supply a hostName, then it will be automatically resolved using the JID (below).
    // For example, if you supply a JID like 'user@quack.com/rsrc'
    // then the xmpp framework will follow the xmpp specification, and do a SRV lookup for quack.com.
    //
    // If you don't specify a hostPort, then the default (5222) will be used.
    
    //	[xmppStream setHostName:@"talk.google.com"];
    //	[xmppStream setHostPort:5222];
    
    
    // You may need to alter these settings depending on the server you're connecting to
    customCertEvaluation = YES;
    xmppPing = [[XMPPPing alloc] initWithDispatchQueue:dispatch_get_main_queue()];
    xmppPing.respondsToQueries = YES;
    [xmppPing activate:xmppStream];
}

- (void)teardownStream
{
    [xmppStream removeDelegate:self];
    [xmppRoster removeDelegate:self];
    
    [xmppReconnect         deactivate];
    [xmppRoster            deactivate];
    [xmppvCardTempModule   deactivate];
    [xmppvCardAvatarModule deactivate];
    [xmppCapabilities      deactivate];
    
    [xmppStream disconnect];
    
    xmppStream = nil;
    xmppReconnect = nil;
    xmppRoster = nil;
    xmppRosterStorage = nil;
    xmppvCardStorage = nil;
    xmppvCardTempModule = nil;
    xmppvCardAvatarModule = nil;
    xmppCapabilities = nil;
    xmppCapabilitiesStorage = nil;
}

// It's easy to create XML elments to send and to read received XML elements.
// You have the entire NSXMLElement and NSXMLNode API's.
//
// In addition to this, the NSXMLElement+XMPP category provides some very handy methods for working with XMPP.
//
// On the iPhone, Apple chose not to include the full NSXML suite.
// No problem - we use the KissXML library as a drop in replacement.
//
// For more information on working with XML elements, see the Wiki article:
// https://github.com/robbiehanson/XMPPFramework/wiki/WorkingWithElements

- (void)goOnline
{
    XMPPPresence *presence = [XMPPPresence presence]; // type="available" is implicit
       //XMPPPresence *presence = [XMPPPresence presenceWithType:@"available"];
    NSString *domain=[xmppStream.myJID domain];
    //domain=@"win-rvu1d9st00q";
    //Google set their presence priority to 24, so we do the same to be compatible.
    
    /*if([domain isEqualToString:@"gmail.com"]
     || [domain isEqualToString:@"gtalk.com"]
     || [domain isEqualToString:@"talk.google.com"])*/
    
   NSString *servname=[[NSUserDefaults standardUserDefaults] stringForKey:@"XMPPServerName"];
    if([domain isEqualToString:servname])
    {
        NSXMLElement *priority = [NSXMLElement elementWithName:@"priority" stringValue:@"24"];
        NSXMLElement *status = [NSXMLElement elementWithName:@"status"];
        [presence addChild:priority];
        [status setStringValue:@"available"];
        [presence addChild:status];
    }
    
    [[self xmppStream] sendElement:presence];
}

- (void)goOffline
{
    XMPPPresence *presence = [XMPPPresence presenceWithType:@"unavailable"];

    [[self xmppStream] sendElement:presence];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Connect/disconnect
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (BOOL)connect
{
     GlobalVars *globals = [GlobalVars sharedInstance];
    if (![xmppStream isDisconnected]) {
        globals.XMPPflag_on_off=false;
        return YES;
        
    }
    
    NSString *NWid= [[NSUserDefaults standardUserDefaults] stringForKey:@"XMPPNotifyID"];
    NSString *Servername= [[NSUserDefaults standardUserDefaults] stringForKey:@"XMPPServerName"];
    
    NSString *myJID = [NSString stringWithFormat:@"%@@%@/IOS",NWid,Servername];
    NSString *myPassword = [[NSUserDefaults standardUserDefaults] stringForKey:@"XMPPNotifyPWD"];
    xmppStream.hostName = [[NSUserDefaults standardUserDefaults] stringForKey:@"DomainName"];
    
    
    /*NSString *NWid= @"metslpatient48";
    NSString *Servername=@"win-rvu1d9st00q";
    
    NSString *myJID = [NSString stringWithFormat:@"%@@%@/IOS",NWid,Servername];
     
    NSString *myPassword = @"101116050662";
    xmppStream.hostName = @"mcc-test-01.personal-healthwatch.com";*/
    
    
    
    
    
    //NSString *myPassword = @"270715055004";
    //NSString *myJID = @"anitab@win-rvu1d9st00q";
    //xmppStream.hostName = @"mcc-test-01.personal-healthwatch.com";
    
   // [xmppStream setHostPort:5222];
    // You may need to alter these settings depending on the server you're connecting to
   
    //
    // If you don't want to use the Settings view to set the JID,
    // uncomment the section below to hard code a JID and password.
    //
    // myJID = @"user@gmail.com/xmppframework";
    // myPassword = @"";
    
    if (myJID == nil || myPassword == nil) {
        return NO;
    }
    
     [xmppStream setMyJID:[XMPPJID jidWithString:myJID]];
     self.xmppStream.enableBackgroundingOnSocket = YES;
    
    password = myPassword;
    
    NSError *error = nil;
    if (![xmppStream connectWithTimeout:XMPPStreamTimeoutNone error:&error])
    {
      /*  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error connecting"
                                                            message:@"See console for error details." 
                                                           delegate:nil 
                                                  cancelButtonTitle:@"Ok" 
                                                  otherButtonTitles:nil];
        [alertView show];*/
        
        NSLogger *logger=[[NSLogger alloc]init];
        logger.degbugger = true;
        
        [logger log:@"XMPP" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"Error connecting:", @"XMPP status", nil] error:TRUE];
        NSLog(@"%@",error);
        DDLogError(@"Error connecting: %@", error);
         globals.XMPPflag_on_off=false;
        return NO;
    }
     globals.XMPPflag_on_off=true;
    return YES;
}

- (void)disconnect
{
    GlobalVars *globals = [GlobalVars sharedInstance];
     globals.XMPPflag_on_off=false;
    [self goOffline];
    [xmppStream disconnect];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark XMPPStream Delegate
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)xmppStream:(XMPPStream *)sender socketDidConnect:(GCDAsyncSocket *)socket
{
    DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
}

- (void)xmppStream:(XMPPStream *)sender willSecureWithSettings:(NSMutableDictionary *)settings
{
    DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    
    NSString *expectedCertName = [xmppStream.myJID domain];
    if (expectedCertName)
    {
        settings[(NSString *) kCFStreamSSLPeerName] = expectedCertName;
    }
    
    if (customCertEvaluation)
    {
        settings[GCDAsyncSocketManuallyEvaluateTrust] = @(YES);
    }
}


- (void)xmppStream:(XMPPStream *)sender didReceiveTrust:(SecTrustRef)trust
 completionHandler:(void (^)(BOOL shouldTrustPeer))completionHandler
{
    DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    
    // The delegate method should likely have code similar to this,
    // but will presumably perform some extra security code stuff.
    // For example, allowing a specific self-signed certificate that is known to the app.
    
    dispatch_queue_t bgQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(bgQueue, ^{
        
        SecTrustResultType result = kSecTrustResultDeny;
        OSStatus status = SecTrustEvaluate(trust, &result);
        
        if (status == noErr && (result == kSecTrustResultProceed || result == kSecTrustResultUnspecified)) {
            completionHandler(YES);
        }
        else {
            completionHandler(NO);
        }
    });
}

- (void)xmppStreamDidSecure:(XMPPStream *)sender
{
    DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
}

- (void)xmppStreamDidConnect:(XMPPStream *)sender
{
    DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    
    isXmppConnected = YES;
    
    NSError *error = nil;
    
    if (![[self xmppStream] authenticateWithPassword:password error:&error])
    {
        DDLogError(@"Error authenticating: %@", error);
    }
}

- (void)xmppStreamDidAuthenticate:(XMPPStream *)sender
{
    DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    
    [self goOnline];
}

- (void)xmppStream:(XMPPStream *)sender didNotAuthenticate:(NSXMLElement *)error
{
    DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
}

- (BOOL)xmppStream:(XMPPStream *)sender didReceiveIQ:(XMPPIQ *)iq
{
    DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    
    return NO;
}

- (void)xmppStream:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)message
{
    DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    
    // A simple example of inbound message handling.
    
    if ([message isChatMessageWithBody])
    {
        /*XMPPUserCoreDataStorageObject *user = [xmppRosterStorage userForJID:[message from]
                                                                 xmppStream:xmppStream
                                                       managedObjectContext:[self managedObjectContext_roster]];*/
        
        NSString *body = [[message elementForName:@"body"] stringValue];
       // NSString *displayName = [user displayName];
       // NSString *displayName =[[message attributeForName:@"from"] stringValue];
        NSArray * msgComponents = [body componentsSeparatedByString:@","];
        NSLog(@"msg%@",msgComponents);
        
        
      
          /*  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:displayName
                                                                message:body
                                                               delegate:nil
                                                      cancelButtonTitle:@"Ok"
                                                      otherButtonTitles:nil];
            [alertView show];*/
            
            //start
            if ([msgComponents[0] intValue] == 0) {
                
              //  [ self sendAckMessage:message :3];
            }
            if (([msgComponents[0] intValue]) > 2000 && ([msgComponents[0] intValue]) < 3000)
            {
                
                int settingcode = 0 ;
                    
                if (msgComponents.count <= 4) {
                    // Suspension Time
                    
                    settingcode = 1;
                    
        
                    NSMutableArray *results = [[NSMutableArray alloc]init];
                    int flag=0;
                    NSPredicate *pred;
                    if ([msgComponents[0] length]!=0) {
                        pred =  [NSPredicate predicateWithFormat:@"SELF.event_code == %@",msgComponents[0]];
                        flag=1;
                    } else {
                        flag=0;
                        NSLog(@"Enter Corect Course number");
                    }
                    
                    if (flag == 1) {
                        
                        NSLog(@"predicate: %@",pred);
                        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"SuspensionSettings"];
                        [fetchRequest setPredicate:pred];
                        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
                        
                        
                        if (results.count > 0) {
                            NSManagedObject* suspensionsett = [results objectAtIndex:0];
                            [suspensionsett setValue:[NSNumber numberWithInt:[msgComponents[2] intValue]] forKey:@"suspension_time"];
                             [suspensionsett setValue:[NSNumber numberWithInt:[msgComponents[0] intValue] ] forKey:@"event_code"];
                            [suspensionsett setValue: @"MCC" forKey:@"modify_from"];
                            [suspensionsett setValue:@"1" forKey:@"updated_to_site"];
                            
                            [self.managedObjectContext save:nil];
                        } else {
                            NSLog(@"Enter Corect Course number");
                        }
                        
                        
                        
                    }
                    
                    
                    GlobalVars *globals = [GlobalVars sharedInstance];
                    
                    if([globals.mcd_bluetoothstatus isEqualToString:@"0"])
                    {
                        ConfigureMCDController *config=[[ConfigureMCDController alloc]init];
                        if([globals currentSetting]==NONE)
                        {
                            globals.suspsett_ecodevalue=@"XMPP";
                            globals.suspsett_time=msgComponents[2];
                           
                            switch ([msgComponents[0] intValue])
                            {
                                    
                                case 2001:
                                {
                                    // st elevation
                                     [config SET_SuspTime:[msgComponents[2] intValue] :0];
                                   
                                }
                                    break;
                                case 2002:
                                {
                                    // st depression
                                   [config SET_SuspTime:[msgComponents[2] intValue] :1];
                                }
                                    break;
                                    
                                case 2003:
                                {
                                    // T wave
                                    [config SET_SuspTime:[msgComponents[2] intValue] :2];
                                    break;
                                    
                                case 2005:
                                    // prolonged
                                    
                                    [config SET_SuspTime:[msgComponents[2] intValue] :4];
                                    break;
                                case 2006:
                                    // QRS_WIDTH_VALUES
                                   [config SET_SuspTime:[msgComponents[2] intValue] :16];
                                    break;
                                case 2007:
                                    // NARROW_QRS_REGULAR_D_DESCRIPTION // SuperVentericular Tachycardia
                                  [config SET_SuspTime:[msgComponents[2] intValue] :6];
                                    
                                    break;
                                case 2011:
                                    // WIDE_QRS_REGULAR_D_DESCRIPTION // Ventricular Tachycardia
                                    
                                    [config SET_SuspTime:[msgComponents[2] intValue] :7];
                                    
                                    break;
                                case 2017:
                                    // brady
                                   [config SET_SuspTime:[msgComponents[2] intValue] :10];
                                    
                                    break;
                                case 2022:
                                    // Temp
                                    [config SET_SuspTime:[msgComponents[2] intValue] :12];
                                    break;
                                    
                                case 2023:
                                    // fall
                                {
                                    [config SET_SuspTime:[msgComponents[2] intValue] :13];
                                    
                                }
                                    break;
                                case 2021:
                                    // Apnea
                                   [config SET_SuspTime:[msgComponents[2] intValue] :11];
                                    break;
                                case 2015:
                                    
                                    // PVC
                                   [config SET_SuspTime:[msgComponents[2] intValue] :8];
                                    break;
                                case 2016:
                                    // Asy
                                    [config SET_SuspTime:[msgComponents[2] intValue] :9];
                                    break;
                                case 2024:
                                    // No motion
                                {
                                    
                                    [config SET_SuspTime:[msgComponents[2] intValue] :14];
                                }
                                    break;
                                    
                                case 2032: // Bigemeny
                                   [config SET_SuspTime:[msgComponents[2] intValue] :3];
                                    break;
                                case 2031: // trigemeny
                                   [config SET_SuspTime:[msgComponents[2] intValue] :18];
                                    break;
                                case 2030: // couplet
                                    [config SET_SuspTime:[msgComponents[2] intValue] :19];
                                    break;
                                case 2029: // Triplet
                                   [config SET_SuspTime:[msgComponents[2] intValue] :20];
                                    break;
                                    
                                case 2036: // Respiration rate
                                    [config SET_SuspTime:[msgComponents[2] intValue] :15];
                                    break;
                                case 2026: // Pause
                                    [config SET_SuspTime:[msgComponents[2] intValue] :17];
                                    break;
                                case 2027:
                                    // QRS Wide
                                   [config SET_SuspTime:[msgComponents[2] intValue] :5];
                                    break;
                                default:
                                    break;
                            }

                        
                            }
                    
                        }
                    }
                    
                    /*
                    
                   if (BluetoothService.getBluetoothServiceInstance().mcmBluetoothConnection.mState == MCMBluetoothConnection.STATE_CONNECTED) {
                        if(BluetoothService.getBluetoothServiceInstance().currentSetting != CurrentSetting.NONE)
                        {
                            suspensionSettings.UPDATED_TO_DEVICE = 2;
                        }
                    }
                    else{
                        suspensionSettings.UPDATED_TO_DEVICE = 0;
                    }*/
                    
                    [ self sendAckMessage:message :3];
                    
                    
                } else {
                    // Extract the components of the notification
                    NSString * code = msgComponents[0];
                    NSString *YD = msgComponents[2];
                    NSString * RD = msgComponents[3];
                    NSString * Posture = msgComponents[4];
                    NSString * Flag = msgComponents[5];
                    NSString * DD = msgComponents[6];
                    NSString * DFlag = msgComponents[7];
                    // doctorsThreshold logic
                    
                    // Save if other settings
                    if (([code isEqualToString:@"2021"]) || ([code isEqualToString:@"2022"]) || ([code isEqualToString:@"2024"]) || ([code isEqualToString:@"2035"]) || ([code isEqualToString:@"2036"]))
                    {
                        
                        NSMutableArray *results = [[NSMutableArray alloc]init];
                        NSMutableArray *results1 = [[NSMutableArray alloc]init];
                        int flag=0;
                        NSPredicate *pred;
                        NSArray* args;
                        NSString* filter = @"%K == %@ && %K == %@";
                        if([code isEqualToString:@"2024"])
                            args = @[@"code",code, @"posture",@"1"];
                         else
                        args = @[@"code",code, @"posture", Posture];
                        if (code.length!=0) {
                            pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
                            flag=1;
                        } else {
                            flag=0;
                            NSLog(@"Enter Corect code empty");
                            
                        }
                        
                        if (flag == 1) {
                            
                            
                            NSLog(@"predicate: %@",pred);
                            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
                            [fetchRequest setPredicate:pred];
                            results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
                            
                            if (results.count > 0) {
                                
                                NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
                                NSLog(@"1 - %@", doctorssett);
                                [doctorssett setValue:DD forKey:@"dd"];
                                [doctorssett setValue:DFlag forKey:@"ddflag"];
                                [doctorssett setValue:@"MCM" forKey:@"modify_from"];
                                [doctorssett setValue:@"1" forKey:@"updated_to_site"];
                                [doctorssett setValue:@"0" forKey:@"updated_to_device"];
                            
                                
                                [self.managedObjectContext save:nil];
                            } else {
                                NSLog(@"Enter Corect code number");
                            }
                            NSFetchRequest *fetchRequest1 = [[NSFetchRequest alloc]initWithEntityName:@"ThresholdSett"];
                            [fetchRequest1 setPredicate:pred];
                            results1 = [[self.managedObjectContext executeFetchRequest:fetchRequest1 error:nil] mutableCopy];
                            
                            if (results1.count > 0) {
                                
                                NSManagedObject *genthressett = (NSManagedObject *)[results1 objectAtIndex:0];
                                NSLog(@"1 - %@", genthressett);
                                [genthressett setValue:YD forKey:@"yd"];
                                [genthressett setValue:RD forKey:@"rd"];
                                [genthressett setValue:Flag forKey:@"flag"];
                                [genthressett setValue:@"MCM" forKey:@"modify_from"];
                                [genthressett setValue:@"1" forKey:@"updated_to_site"];
                                [genthressett setValue:@"0" forKey:@"update_to_device"];
                                [self.managedObjectContext save:nil];
                            } else {
                                NSLog(@"Enter Corect general code number");
                            }
                            
                            
                            
                        }

                        
                        
                        settingcode = 2;
                      

                       [ self sendAckMessage:message :3];
                        
                        GlobalVars *globals = [GlobalVars sharedInstance];
                        
                        if([globals.mcd_bluetoothstatus isEqualToString:@"0"])
                        {
                            ConfigureMCDController *config=[[ConfigureMCDController alloc]init];
                            if([globals currentSetting]==NONE)
                            {
                                globals.Thres_SetCode=code;
                                globals.Thres_DD=DD;
                                globals.Thres_RD=RD;
                                globals.Thres_YD=YD;
                                globals.Thres_flagg=Flag;
                                globals.Thres_ddflagg=DFlag;
                                globals.Thres_posturecode=Posture;
                                
                               
                                
                               
                                if ([code intValue]==TEMPERATURE_VALUE)
                                {
                                     [config SET_Temperature:[DD intValue] :0 :[DFlag intValue] :[Posture intValue]];
                                }
                                else if ([code intValue]==HIGH_RESPIRATION_RATE)
                                {
                                    [config SET_resperationRate:1 :[DD intValue] :[Posture intValue] :[DFlag intValue]];
                                }
                                else if ([code intValue]==LOW_RESPIRATION_RATE)
                                {
                                    [config SET_resperationRate:0 :[DD intValue] :[Posture intValue] :[DFlag intValue]];
                                }
                                else if ([code intValue]==APNEA_VALUE)
                                {
                                  
                                    [config SET_Apnea:[DFlag intValue] :[DD intValue] :[Posture intValue]];
                                }
                                
                                else if ([code intValue]==NO_MOTION_VALUE)
                                {
                                    int minutes = [DD intValue];
                                    if ([DFlag intValue] == 0) {
                                        minutes = 0;
                                    }
                                    [config SET_NoMotion:minutes];
                                }
                            }
                            
                        }
                       
                    }
                    else
                    {
                        
                        NSMutableArray *results = [[NSMutableArray alloc]init];
                        NSMutableArray *results1 = [[NSMutableArray alloc]init];
                        int flag=0;
                        NSPredicate *pred;
                        NSString* filter = @"%K == %@ && %K == %@";
                        NSArray* args = @[@"code",code, @"posture", Posture];
                        if (code.length!=0) {
                            pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
                            flag=1;
                        } else {
                            flag=0;
                            NSLog(@"Enter Corect code empty");
                            
                        }
                        
                        if (flag == 1) {
                            
                            
                            NSLog(@"predicate: %@",pred);
                            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"DoctorThreshold"];
                            [fetchRequest setPredicate:pred];
                            results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
                            
                            if (results.count > 0) {
                                
                                NSManagedObject *doctorssett = (NSManagedObject *)[results objectAtIndex:0];
                                NSLog(@"1 - %@", doctorssett);
                                [doctorssett setValue:DD forKey:@"dd"];
                                [doctorssett setValue:DFlag forKey:@"ddflag"];
                                [doctorssett setValue:@"MCM" forKey:@"modify_from"];
                                [doctorssett setValue:@"1" forKey:@"updated_to_site"];
                                [doctorssett setValue:@"0" forKey:@"updated_to_device"];
                                
                                
                                [self.managedObjectContext save:nil];
                            } else {
                                NSLog(@"Enter Corect code number");
                            }
                            NSFetchRequest *fetchRequest1 = [[NSFetchRequest alloc]initWithEntityName:@"ThresholdSett"];
                            [fetchRequest1 setPredicate:pred];
                            results1 = [[self.managedObjectContext executeFetchRequest:fetchRequest1 error:nil] mutableCopy];
                            
                            if (results1.count > 0) {
                                
                                NSManagedObject *genthressett = (NSManagedObject *)[results1 objectAtIndex:0];
                                NSLog(@"1 - %@", genthressett);
                                [genthressett setValue:YD forKey:@"yd"];
                                [genthressett setValue:RD forKey:@"rd"];
                                [genthressett setValue:Flag forKey:@"flag"];
                                [genthressett setValue:@"MCM" forKey:@"modify_from"];
                                [genthressett setValue:@"1" forKey:@"updated_to_site"];
                                [genthressett setValue:@"0" forKey:@"update_to_device"];
                                [self.managedObjectContext save:nil];
                            } else {
                                NSLog(@"Enter Corect general code number");
                            }
                            
                            
                            
                        }

                        
                        
                        [ self sendAckMessage:message :3];
                         GlobalVars *globals = [GlobalVars sharedInstance];
                        if([globals.mcd_bluetoothstatus isEqualToString:@"0"])
                        {
                            ConfigureMCDController *config=[[ConfigureMCDController alloc]init];
                            if([globals currentSetting]==NONE)
                            {
                                globals.Thres_SetCode=code;
                                globals.Thres_DD=DD;
                                globals.Thres_RD=RD;
                                globals.Thres_YD=YD;
                                globals.Thres_flagg=Flag;
                                globals.Thres_ddflagg=DFlag;
                                globals.Thres_posturecode=Posture;
                                
                               
                                if([code intValue]==NARROW_QRS_REGULAR_D_DESCRIPTION)
                                {
                                   
                                 [config SET_VentricularTachyNarrow:[DD intValue] :[DD intValue] :[DFlag intValue] :[Posture intValue]];
                                }
                                else if([code intValue]==WIDE_QRS_REGULAR_D_DESCRIPTION)
                                {
                                  [config SET_VentricularTachyWide:[DD intValue] :[DD intValue] :[DFlag intValue] :[Posture intValue]];
                                }
                                else if([code intValue]==BRADICARDIA_REGULAR_D_DESCRIPTION)
                                {
                                     [config SET_Bradycardia:[DD intValue] :[DD intValue] :[DFlag intValue] :[Posture intValue]];
                                }
                                else if ([code intValue]==PAUSE)
                                {
                                   
                                        [config SET_PauseValue:[DD intValue] :[DFlag intValue] :[Posture intValue]];
                                    
                                }
                                else if ([code intValue]==PVC_VALUE)
                                {
                                   
                                        [config SET_PVC:[DFlag intValue] :[DD intValue] :[Posture intValue]];
                                }
                                else if ([code intValue]==ASYSTOLE_VALUE)
                                {
                                        [config SET_Asystole:[DFlag intValue] :[DD intValue] :[Posture intValue]];
                                }
                                else if ([code intValue]==BIGEMINY)
                                {
                                        [config SET_BigemenyValue:[DFlag intValue] :[Posture intValue] :[DD intValue]];
                                }
                                else if ([code intValue]==TRIGEMINY)
                                {
                                        [config SET_TrigemenyStatus:[Posture intValue] :[DFlag intValue] :[DD intValue]];
                                }
                                else if ([code intValue]==COUPLET)
                                {
                                        [config SET_CoupletStatus:[Posture intValue] :[DFlag intValue] :[DD intValue]];
                                }
                                else if ([code intValue]==TRIPLET)
                                {
                                        [config SET_TripletStatus:[Posture intValue] :[DFlag intValue] :[DD intValue]];
                                }
                                
                                else if ([code intValue]==ST_ELEVATION_VALUES)
                                {
                                        [config SET_STelev:[DD intValue] :[DFlag intValue] :[Posture intValue]];
                                }
                                else if ([code intValue]==ST_DEPRESSION_VALUES)
                                {
                                        [config SET_Depre:[DD intValue] :[DFlag intValue] :[Posture intValue]];
                                }
                                else if ([code intValue]==PROLNGED_QT_VALUES)
                                {
                                        [config SET_ProlongQT:[DD intValue] :[DFlag intValue] :[Posture intValue]];
                                }
                                else if ([code intValue]==QRS_WIDE_WIDTH)
                                {
                                        [config SET_QRSwidthWide:[DD intValue] :[DFlag intValue] :[Posture intValue]];
                                    
                                }

                                
                               
                            }
                            
                        }
                        
                        
                    }
                }
                
                
            
                
            }
            
            if ([msgComponents[0] intValue] == 1005)
            {
               
                 NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"PatientDetails"];
               // NSPredicate *predicate=[NSPredicate predicateWithFormat:@"vehicle_id==%@",vehicle_id]; // If required to fetch specific vehicle
                //fetchRequest.predicate=predicate;
                 PatientDetails *patientrecord=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
                
                [patientrecord setValue: msgComponents[2] forKey:@"given_name"];
                [patientrecord setValue: msgComponents[3] forKey:@"middle_name"];
                [patientrecord setValue: msgComponents[4] forKey:@"family_name"];
                [patientrecord setValue: msgComponents[5] forKey:@"dob1"];
                [patientrecord setValue: msgComponents[6] forKey:@"gender1"];
                [patientrecord setValue: msgComponents[7] forKey:@"height1"];
                [patientrecord setValue: msgComponents[8] forKey:@"weight"];
                NSString *patadd=[NSString stringWithFormat:@"%@,%@,%@,%@",msgComponents[9],msgComponents[10],msgComponents[11],msgComponents[12]];
                [patientrecord setValue:patadd forKey:@"patient_add"];
                  [patientrecord setValue: msgComponents[13] forKey:@"zipcode"];
                  [patientrecord setValue: msgComponents[14] forKey:@"uid"];
                
                [self.managedObjectContext save:nil];
                //sendAckMessage(xmppMsg, 3);
                [ self sendAckMessage:message :3];
               
            }
            
            if ([msgComponents[0] intValue] == 1006)
            {
                NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"PatientDetails"];
                PatientDetails *patientrecord=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
                
                [patientrecord setValue: msgComponents[2] forKey:@"doc_ph_no"];
                NSString * docadd=[msgComponents[3] stringByReplacingOccurrencesOfString:@"|" withString:@","];
                [patientrecord setValue:docadd forKey:@"doc_address"];
                [patientrecord setValue: msgComponents[4] forKey:@"doc_name"];
                
                [self.managedObjectContext save:nil];
        
                [ self sendAckMessage:message :3];

              
            }
            
            if ([msgComponents[0] intValue] == 1007)
            {
                NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"PatientDetails"];
                PatientDetails *patientrecord=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
                
                NSString *Patientlevel;
                if([msgComponents[2] isEqualToString:@"1"])
                    Patientlevel=@"Level1";
                else if([msgComponents[2] isEqualToString:@"2"])
                    Patientlevel=@"Level2";
                else if([msgComponents[2] isEqualToString:@"3"])
                    Patientlevel=@"Level3";
                else
                    Patientlevel=@"Level4";
                [patientrecord setValue:Patientlevel forKey:@"level"];
                
                [self.managedObjectContext save:nil];
                
                [ self sendAckMessage:message :3];
                
                
                GlobalVars *globals = [GlobalVars sharedInstance];
                if([globals.mcd_bluetoothstatus isEqualToString:@"0"])
                {
                    NSLog(@"%u",[globals currentSetting]);
                    if([globals currentSetting]==NONE)
                    {
                        
                            globals.ChangeLevel=[msgComponents[2] intValue];
                   
                    
                    ConfigureMCDController *config=[[ConfigureMCDController alloc]init];
                    [config SetLevel:(int)[globals ChangeLevel]];
                    }
                }

                /*
                if (BluetoothService.getBluetoothServiceInstance().mcmBluetoothConnection.mState == MCMBluetoothConnection.STATE_CONNECTED) {
                    MasterCaution.log.debug("Set level");
                   
                    
                    synchronized (xmppManager) {
                        xmppManager.XmmppCommand = true;
                        xmppManager.XmppACK = false ;
                        BluetoothService.getBluetoothServiceInstance().mcmBluetoothConnection
                        .SetLevel(Integer.valueOf(msgComponents[2]));
                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException e) {
                            // block
                            MasterCaution.log.log(org.apache.log4j.Level.ERROR, e.getMessage(), e);
                            e.printStackTrace();
                        }
                        //}
                        if (xmppManager.XmppACK) {
                            MasterCaution.log.debug("Level Ack Recieved");
                            xmppManager.XmmppCommand = false;
                            
                        }
                        else{
                            xmppManager.XmmppCommand = false;
                            xmppManager.XmppACK = true ;
                        }
                    }
                    
                }
                
                Intent intent = new Intent(ACTION_LEVEL_CHANGED);
                _context.sendBroadcast(intent);*/
                
            }
            if ([msgComponents[0] intValue] == 3001)
            {
                NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"MCDSettings"];
                MCDSettings *mcdsettings=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
                
                
                NSString * fifthz,*sixtyhz;
                if ([ msgComponents[3] intValue]== 0)
                {
                    fifthz=@"0";
                    sixtyhz=@"0";
                }
                else if([ msgComponents[3] intValue] == 1)
                {
                    fifthz=@"1";
                    sixtyhz=@"0";
                }
                else if([ msgComponents[3] intValue] == 2)
                { fifthz=@"0";
                    sixtyhz=@"1"; }
                  [mcdsettings setValue: msgComponents[2] forKey:@"gain"];
                  [mcdsettings setValue: fifthz forKey:@"fiftyhz_filter"];
                  [mcdsettings setValue: sixtyhz forKey:@"sixtyhz_filter"];
                  //[mcdsettings setValue: msgComponents[5] forKey:@"base_line_fil"];
                 [mcdsettings setValue:@"1" forKey:@"base_line_fil"];
                  [mcdsettings setValue: msgComponents[6] forKey:@"volume_level"];
                  [mcdsettings setValue: msgComponents[7] forKey:@"timeinterval_pre"];
                  [mcdsettings setValue: msgComponents[8] forKey:@"timeinterval_post"];
                  [mcdsettings setValue: msgComponents[9] forKey:@"bt_device_add"];
                  [mcdsettings setValue: msgComponents[10] forKey:@"leads"];
                  [mcdsettings setValue: msgComponents[11] forKey:@"sampling_rate"];
                  [mcdsettings setValue: msgComponents[12] forKey:@"lead_config"];
                  [mcdsettings setValue: msgComponents[13] forKey:@"respiration"];
                  [mcdsettings setValue: msgComponents[14] forKey:@"orientation"];
                  [mcdsettings setValue: @"1"              forKey:@"updated_to_site"];
                  [mcdsettings setValue:msgComponents[msgComponents.count - 1] forKey:@"xmpp_msgid"];
                  [mcdsettings setValue: msgComponents[17] forKey:@"inverted_t_wave"];
               
               
 
                  [self.managedObjectContext save:nil];
                
                
                GlobalVars *globals = [GlobalVars sharedInstance];
                if([globals.mcd_bluetoothstatus isEqualToString:@"0"])
                {
                    if([globals currentSetting]==NONE)
                    {
                        
                        int leadconfig = 0;
                        switch ( [msgComponents[12] intValue]) {
                            case 0:
                                leadconfig = 3;
                                break;
                            case 1:
                                leadconfig = 5;
                                break;
                            case 2:
                                leadconfig = 12;
                                break;
                            case 3:
                                leadconfig = 15;
                                break;
                                
                            default:
                                break;
                        }
                        
                      

                        
                       
                        ConfigureMCDController *config=[[ConfigureMCDController alloc]init];
                        
                        
                         [config SetMCDSettings:[msgComponents[3] intValue] :[ msgComponents[5] intValue]:[ msgComponents[10] intValue] :[msgComponents[2]intValue]+1 :2-[msgComponents[11] intValue] :[msgComponents[7] intValue] :[msgComponents[8] intValue] :[msgComponents[6] intValue] :leadconfig :[msgComponents[13] intValue] :[msgComponents[14] intValue]];
                       
                    }
                }
                
               
                
              
                [ self sendAckMessage:message :3];
                
               
                
            }
            if ([msgComponents[0] intValue] == 3002) {
               //occurs every
                
                
           
                
                CommonHelper * help=[[CommonHelper alloc]init];
                
                NSString *startdate1=[help UTCDateInYourFormat:@"yyyy-MM-dd HH:mm:ss" : @"yyyy-MM-dd"  :[msgComponents[3] stringByReplacingOccurrencesOfString:@"." withString:@""]];
                NSString *enddate1=[help UTCDateInYourFormat:@"yyyy-MM-dd HH:mm:ss" : @"yyyy-MM-dd"  :[msgComponents[4] stringByReplacingOccurrencesOfString:@"." withString:@""]];
                
                
                
                NSString *SD1=[help UTCDateInYourFormat:@"yyyy-MM-dd HH:mm:ss" : @"yyyy-MM-dd HH:mm:ss"  :[msgComponents[3] stringByReplacingOccurrencesOfString:@"." withString:@""]];
                NSString *ED1=[help UTCDateInYourFormat:@"yyyy-MM-dd HH:mm:ss" : @"yyyy-MM-dd HH:mm:ss"  :[msgComponents[4] stringByReplacingOccurrencesOfString:@"." withString:@""]];
                
                NSDateFormatter *dateFormat_t = [[NSDateFormatter alloc] init];
                [dateFormat_t setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate *date1 = [dateFormat_t dateFromString:SD1];
                NSDate *date2 = [dateFormat_t dateFromString:ED1];
                
                NSTimeInterval secondsBetween = [date2 timeIntervalSinceDate:date1];
                
                int numberOfDays = secondsBetween / 86400;
                
                NSArray *cdd = [startdate1 componentsSeparatedByString:@" "];
                startdate1 = cdd[0];
                
                NSString *Timeinterval1=msgComponents[5];
               // int  hrs=[Timeinterval1 intValue]/60;
               // int timeinterval=hrs*3600;
              
                NSString *Starttime=[help UTCDateInYourFormat:@"HH:mm:ss" : @"HH:mm:ss"  :msgComponents[6]];
                NSString *Endtime=[help UTCDateInYourFormat:@"HH:mm:ss" : @"HH:mm:ss"  :msgComponents[7]];
                
                
               NSMutableArray *AllDatesBuf=[[NSMutableArray alloc]init];

               NSDateFormatter* df_local = [[NSDateFormatter alloc] init];
                [df_local setTimeZone:[NSTimeZone defaultTimeZone]];
                [df_local setDateFormat:@"HH:mm:ss"];
             
               
                
                
                
                NSDate *Starttime_local1 = [df_local dateFromString:Starttime];
                NSDate *Endtime_local = [df_local dateFromString:Endtime];
               
                NSDateFormatter* started_local = [[NSDateFormatter alloc] init];
                 [started_local setTimeZone:[NSTimeZone defaultTimeZone]];
                [started_local setDateFormat:@"HH:mm:ss"];
                
                NSString * startedtime=[started_local stringFromDate:Starttime_local1];
                
                NSDateComponents *compon = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
               // compon.timeZone = [NSTimeZone defaultTimeZone];
                NSArray * time=[startedtime componentsSeparatedByString:@":"];
               
                compon.hour   = [time[0] intValue];
                compon.minute = [time[1] intValue];
                compon.second = [time[2] intValue];
                
               //difference between dates
                
                
              NSTimeInterval distanceBetweenDates = [Endtime_local timeIntervalSinceDate:Starttime_local1];
                NSLog(@"dist%f",distanceBetweenDates);
                int secondsInAnHour = [Timeinterval1 intValue]*60;
                NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
                
                NSDate *date = [[NSCalendar currentCalendar] dateFromComponents:compon];
                
                
                NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
                [timeFormatter setTimeZone:[NSTimeZone defaultTimeZone]];
                [timeFormatter setDateFormat:@"HH:mm:ss"];
                 NSUInteger seconds;
                
                 for (int i=0; i<=numberOfDays; i++)
                 {
                     seconds=0;
                  for (int j=0; j<=hoursBetweenDates;j++)
                  {
                     NSUInteger sec1;
                    NSDate *newDate= [NSDate dateWithTimeInterval:seconds sinceDate:date];
                    NSLog(@"newDate = %@", newDate);
                    NSString *formatted = [timeFormatter stringFromDate:newDate];
                    NSLog(@"Formatted = %@", formatted);
                    [AllDatesBuf addObject:formatted];
                      sec1 = [Timeinterval1 intValue]*60;
                      seconds+=sec1;
                   }
                 }
                
                timeFormatter=nil;

            
                NSDateComponents *components;
                
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
                [dateFormat setLocale:[NSLocale currentLocale]];
                [dateFormat setDateFormat:@"yyyy-MM-dd"];
                
                
                NSDate *startDate =[dateFormat dateFromString:startdate1];
                NSDate *endDate = [dateFormat dateFromString:enddate1];
                
                
                
                
                components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay fromDate:startDate toDate:endDate options:0];
                
                int days = (int)[components day];
                
                for (int x = 0; x <= days; x++) {
                    NSLog(@"%@", [dateFormat stringFromDate:startDate]);
                    NSString * setdate=[dateFormat stringFromDate:startDate];
                    NSArray * splitdate=[setdate componentsSeparatedByString:@"-"];
                    int dayy=[splitdate[2] intValue];
                    int month=[splitdate[1] intValue];
                    int year=[splitdate[0] intValue];
                    
                    for (int i = 0; i < AllDatesBuf.count; i++)
                    {
                        NSArray * time=[AllDatesBuf[i] componentsSeparatedByString:@":"];
                        int hour=[time[0] intValue];
                        int minute=[time[1] intValue];
                        int second=[time[2] intValue];
                        
                        
                        NSCalendar *calendar = [NSCalendar currentCalendar];
                      
                        NSDateComponents *components1 = [[NSDateComponents alloc] init];
                        [components1 setDay:dayy];
                        [components1 setMonth:month];
                        [components1 setYear:year];
                        [components1 setHour:hour];
                        [components1 setMinute:minute];
                        [components1 setSecond:second];
                          NSLog(@"cal%@",components1);
                        [calendar setTimeZone: [NSTimeZone defaultTimeZone]];
                        NSDate *dateToFire = [calendar dateFromComponents:components1];
                        [self scheduleNotificationForDate:dateToFire];
                        startDate = [startDate dateByAddingTimeInterval:(60 * 60 * 24)];
                    }
                }
                
                
                
                
                [ self sendAckMessage:message :3];
                
              
                
            }
            if ([msgComponents[0] intValue] == 3003) {
                
                
                //occurs at
                NSString *startdate1=[msgComponents[3] stringByReplacingOccurrencesOfString:@"." withString:@""];
                NSString *enddate1=[msgComponents[4] stringByReplacingOccurrencesOfString:@"." withString:@""];
                
                
                NSArray *AllDatesBuf;
                AllDatesBuf=[[msgComponents[5]  stringByReplacingOccurrencesOfString:@"," withString:@""]componentsSeparatedByString:@"|"];
                
                //NSMutableArray *AllDate_HHMM;
              
    
              /*  for (int i = 0; i < AllDatesBuf.count; i++) {
                    
                    if (AllDatesBuf[i] != NULL) {
                            [AllDate_HHMM addObject:AllDatesBuf[i]];
                    }
                }*/
                
                
                NSDateComponents *components;
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
                [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
                NSDate *startDate = [dateFormat dateFromString:startdate1];
                NSDate *endDate = [dateFormat dateFromString:enddate1];
                
                
                
                [dateFormat setDateFormat:@"yyyy-MM-dd"];
                
                components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay fromDate:startDate toDate:endDate options:0];
                
                int days = (int)[components day];
                
                for (int x = 0; x <= days; x++) {
                    NSLog(@"%@", [dateFormat stringFromDate:startDate]);
                    NSString * setdate=[dateFormat stringFromDate:startDate];
                    NSArray * splitdate=[setdate componentsSeparatedByString:@"-"];
                    int dayy=[splitdate[2] intValue];
                    int month=[splitdate[1] intValue];
                    int year=[splitdate[0] intValue];
                    for (int i = 1; i < AllDatesBuf.count-1; i++)
                    {
                        NSArray * time=[AllDatesBuf[i] componentsSeparatedByString:@":"];
                    int hour=[time[0] intValue];
                    int minute=[time[1] intValue];
                    int second=[time[2] intValue];
                    
                    
                    NSCalendar *calendar = [NSCalendar currentCalendar];
                    NSDateComponents *components1 = [[NSDateComponents alloc] init];
                    [components1 setDay:dayy];
                    [components1 setMonth:month];
                    [components1 setYear:year];
                    [components1 setHour:hour];
                    [components1 setMinute:minute];
                    [components1 setSecond:second];
                         NSLog(@"cal%@",components1);
                    [calendar setTimeZone: [NSTimeZone defaultTimeZone]];
                    NSDate *dateToFire = [calendar dateFromComponents:components1];
                    [self scheduleNotificationForDate:dateToFire];
                    startDate = [startDate dateByAddingTimeInterval:(60 * 60 * 24)];
                    }
                }

                
                             
                
                [ self sendAckMessage:message :3];
               
            }
            
            
            if ([msgComponents[0] intValue] == 3005)
            {
                  [ self sendAckMessage:message :3];
                
                WCFUpload *wcfcall=[[WCFUpload alloc]init];
                [wcfcall SendPatientlivestatus];
               
            }
            if ([msgComponents[0] intValue] == 4001)
            {
                NSString * ecgId = msgComponents[2];
                [self fetchingpatientdata];
                NSString *thresholdtype,*EventType,*Header;
                NSMutableArray *results = [[NSMutableArray alloc]init];
                int flag=0;
                NSPredicate *pred;
                NSString* filter = @"%K == %@";
                NSArray* args = @[@"event_ecg_id",ecgId];
                if (ecgId.length!=0) {
                    pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
                    flag=1;
                } else {
                    flag=0;
                    NSLog(@"Enter Corect code empty");
                    
                }
                
                if (flag == 1) {
                    
                    
                    NSLog(@"predicate: %@",pred);
                    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"EventStorage"];
                    [fetchRequest setPredicate:pred];
                    results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
                    
                    if (results.count > 0) {
                        
                        NSManagedObject *Eventsett = (NSManagedObject *)[results objectAtIndex:0];
                        thresholdtype=[Eventsett valueForKey:@"event_thresholdtype"];
                        EventType=[Eventsett valueForKey:@"event_type"];
                    }
                    
                    
                    
                }
                switch ([thresholdtype intValue]) {
                    case 0:
                        Header=@"";
                        break;
                    case 1:
                        Header=@"Arrhythmia";
                        break;
                    case 2:
                        Header=@"Ischemia";
                        break;
                    case 3:
                        Header=@"Other Events";
                        break;
                    default:
                        break;
                }
                
                UIColor *Headercolor;
                switch ([EventType intValue]) {
                    case 0:
                        Headercolor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"yellow_color.png"]]; // ("Y");
                        break;
                    case 1:
                        Headercolor= [UIColor colorWithPatternImage:[UIImage imageNamed:@"red_color.png"]];// ("R");
                        break;
                    case 2:
                       Headercolor=[UIColor blueColor];// ("R");
                        break;
                    case 3:
                        Headercolor=[UIColor grayColor];// ("R");
                        break;
                        
                    default:
                        break;
                }
                
             
                
                SDCAlertView *alert = [[SDCAlertView alloc]   initWithTitle:Header message:@"Please call your doctor immediately!"
                                                                   delegate:self
                                                          cancelButtonTitle:@"Dismiss"
                                                          otherButtonTitles:@"Call", nil];
                [alert setAlertViewStyle:SDCAlertViewStyleDefault];
                
                [alert setTitleLabelFont:[UIFont fontWithName:@"Arial" size:24.0]];
                [alert setTitleLabelTextColor:Headercolor];
                [alert setMessageLabelFont:[UIFont fontWithName:@"Arial" size:14.0]];
                
                
                UILabel * lblspin = [[UILabel alloc] init];
                [lblspin setTranslatesAutoresizingMaskIntoConstraints:NO];
                [alert.contentView addSubview:lblspin];
                lblspin.backgroundColor=Headercolor;
                [lblspin sdc_pinWidthToWidthOfView:alert.contentView offset:0];
                [lblspin sdc_pinHeight:3];
                [lblspin sdc_horizontallyCenterInSuperview];
                [lblspin sdc_verticallyCenterInSuperviewWithOffset:
                 -40];
                
                // [lblspin sdc_verticallyCenterInSuperviewWithOffset:SDCAutoLayoutStandardSiblingDistance];
                
                UITextView *textView = [[UITextView alloc] init];
                [textView setTranslatesAutoresizingMaskIntoConstraints:NO];
                [textView setEditable:NO];
                textView.text=[NSString stringWithFormat:@"%@\n%@",docName,docPhno];//    @"Doctor Felix Amar\n1234566789";
                textView.autocorrectionType = UITextAutocorrectionTypeNo;
                [alert.contentView addSubview:textView];
                
                textView.backgroundColor=[UIColor clearColor];
                textView.textAlignment=NSTextAlignmentCenter;
                textView.font=[UIFont fontWithName:@"Arial" size:18.0];
                [textView sdc_pinWidthToWidthOfView:alert.contentView offset:-15];
                [textView sdc_pinHeight:50];
                [textView sdc_horizontallyCenterInSuperview];
                [textView sdc_verticallyCenterInSuperviewWithOffset:SDCAutoLayoutStandardSiblingDistance];
                
                NSString * telno=[NSString stringWithFormat:@"tel://%@",docPhno];
                [alert showWithDismissHandler: ^(NSInteger buttonIndex) {
                    NSLog(@"Tapped button: %@", @(buttonIndex));
                    if (buttonIndex == 1) {
                        NSURL *url = [NSURL URLWithString:telno];
                        [[UIApplication  sharedApplication] openURL:url];
                    }
                    else {
                        NSLog(@"Cancelled");
                    }
                }];
              
            }
            if ([msgComponents[0] intValue] == 4002) {
              
               // NSString * ecgId = msgComponents[2];
                //UIColor * yellowUIColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"yellow_color.png"]];
                UIColor * redUIColor= [UIColor colorWithPatternImage:[UIImage imageNamed:@"red_color.png"]];
                
                SDCAlertView *alert = [[SDCAlertView alloc]   initWithTitle:@"URGENT" message:@""
                                                                   delegate:self
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil, nil];
                [alert setAlertViewStyle:SDCAlertViewStyleDefault];
                
                [alert setTitleLabelFont:[UIFont fontWithName:@"Arial" size:24.0]];
                [alert setTitleLabelTextColor:redUIColor];
                [alert setMessageLabelFont:[UIFont fontWithName:@"Arial" size:14.0]];
                
                
                UILabel * lblspin = [[UILabel alloc] init];                         [lblspin setTranslatesAutoresizingMaskIntoConstraints:NO];
                [alert.contentView addSubview:lblspin];
                lblspin.backgroundColor=redUIColor;
                [lblspin sdc_pinWidthToWidthOfView:alert.contentView offset:0];
                [lblspin sdc_pinHeight:3];
                [lblspin sdc_horizontallyCenterInSuperview];
                [lblspin sdc_verticallyCenterInSuperviewWithOffset:
                 -40];
                
                // [lblspin sdc_verticallyCenterInSuperviewWithOffset:SDCAutoLayoutStandardSiblingDistance];
                
                UITextView *textView = [[UITextView alloc] init];
                [textView setTranslatesAutoresizingMaskIntoConstraints:NO];
                [textView setEditable:NO];
                textView.text=@"Go To Hospital";
                textView.autocorrectionType = UITextAutocorrectionTypeNo;
                [alert.contentView addSubview:textView];
                
                textView.backgroundColor=[UIColor clearColor];
                textView.textAlignment=NSTextAlignmentCenter;
                textView.font=[UIFont fontWithName:@"Arial" size:18.0];
                [textView sdc_pinWidthToWidthOfView:alert.contentView offset:-15];
                [textView sdc_pinHeight:50];
                [textView sdc_horizontallyCenterInSuperview];
                [textView sdc_verticallyCenterInSuperviewWithOffset:SDCAutoLayoutStandardSiblingDistance];
                
                
                [alert show];
                    
            
            }
            if ([msgComponents[0] intValue] == 3004) {
                // On Demand
                
                NSString * time = msgComponents[2];
                ConfigureMCDController *config=[[ConfigureMCDController alloc]init];
                [config OnDemandECG:[time intValue]];
                
             
                
            }
            //End
            
       /*  if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive)
        {
        }
        else
        {
            // We are not active, so use a local notification instead
            NSString * notifybody=@"Record updated from MCC";
            UILocalNotification *localNotification = [[UILocalNotification alloc] init];
            localNotification.alertAction = @"Ok";
            localNotification.alertBody = [NSString stringWithFormat:@"\n%@",notifybody];
            
            [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
        }*/
    }
}

-(void) scheduleNotificationForDate: (NSDate*)date {
    /* Here we cancel all previously scheduled notifications */
    
    NSTimeInterval timeInterval = [date timeIntervalSinceNow];
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:timeInterval target:self
                                                    selector:@selector(ondemandcall)  userInfo:nil  repeats:false];
    
    [[NSRunLoop mainRunLoop] addTimer: timer forMode:NSRunLoopCommonModes];
    
   // [[UIApplication sharedApplication] cancelAllLocalNotifications];
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = date;
    NSLog(@"Notification will be shown on: %@ ",localNotification.fireDate);
    
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    localNotification.alertBody = @"On Demand ECG Occured"; //[NSString stringWithFormat:@"%@",date];
    localNotification.alertAction = NSLocalizedString(@"View details", nil);
    
    
    
    //localNotification.repeatInterval = NSCalendarUnitDay;
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    localNotification.applicationIconBadgeNumber = -1;
    NSDictionary *infoDict = [[NSDictionary alloc] initWithObjectsAndKeys:@"ONDEMAND",@"beaconKey", nil];
    localNotification.userInfo = infoDict;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

//On demand
-(void)ondemandcall
{
    ConfigureMCDController *config=[[ConfigureMCDController alloc]init];
    [config OnDemandECG:1];
    
}


-(void)fetchingpatientdata
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"PatientDetails" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        NSLog(@"%@", result);
        if (result.count > 0) {
            NSManagedObject *patient = (NSManagedObject *)[result objectAtIndex:0];
            NSLog(@"1 - %@", patient);
            patientid=[patient valueForKey:@"patient_id"];
            deviceid=[patient valueForKey:@"device_id"];
            docPhno=[patient valueForKey:@"doc_ph_no"];
            docName=[patient valueForKey:@"doc_name"];
            
        }
    }
}




- (void)xmppStream:(XMPPStream *)sender didReceivePresence:(XMPPPresence *)presence
{
    DDLogVerbose(@"%@: %@ - %@", THIS_FILE, THIS_METHOD, [presence fromStr]);
    
    NSXMLElement *showStatus = [presence elementForName:@"status"];
    NSString *presenceString = [showStatus stringValue];
    NSString *customMessage = [[presence elementForName:@"show"]stringValue];
    
    NSLog(@"Presence : %@, and presenceMessage: %@",presenceString,customMessage);
}

- (void)xmppStream:(XMPPStream *)sender didReceiveError:(id)error
{
    DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
}

- (void)xmppStreamDidDisconnect:(XMPPStream *)sender withError:(NSError *)error
{
    DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    
    if (!isXmppConnected)
    {
        DDLogError(@"Unable to connect to server. Check xmppStream.hostName");
        NSLog(@"Unable to connect to server. Check xmppStream.hostName");
        NSLogger *logger=[[NSLogger alloc]init];
        logger.degbugger = true;
        
        [logger log:@"XMPP " properties:[NSDictionary dictionaryWithObjectsAndKeys:@"Unable to connect to server", @"XMPP status", nil] error:TRUE];
        GlobalVars *globals = [GlobalVars sharedInstance];
            globals.XMPPflag_on_off=false;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark XMPPRosterDelegate
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)xmppRoster:(XMPPRoster *)sender didReceiveBuddyRequest:(XMPPPresence *)presence
{
    DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    
    XMPPUserCoreDataStorageObject *user = [xmppRosterStorage userForJID:[presence from]
                                                             xmppStream:xmppStream
                                                   managedObjectContext:[self managedObjectContext_roster]];
    
    NSString *displayName = [user displayName];
    NSString *jidStrBare = [presence fromStr];
    NSString *body = nil;
    
    if (![displayName isEqualToString:jidStrBare])
    {
        body = [NSString stringWithFormat:@"Buddy request from %@ <%@>", displayName, jidStrBare];
    }
    else
    {
        body = [NSString stringWithFormat:@"Buddy request from %@", displayName];
    }
    
    
    if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:displayName
                                                            message:body
                                                           delegate:nil 
                                                  cancelButtonTitle:@"Not implemented"
                                                  otherButtonTitles:nil];
        [alertView show];
    } 
    else 
    {
        // We are not active, so use a local notification instead
        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
        localNotification.alertAction = @"Not implemented";
        localNotification.alertBody = body;
        
        [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
    }
    
}


- (void)sendAckMessage:(XMPPMessage *)message :(int)status
{
    NSLog(@"sendack");
   /* XMPPUserCoreDataStorageObject *user = [xmppRosterStorage userForJID:[message from]
                                                xmppStream:xmppStream
                                                   managedObjectContext:[self managedObjectContext_roster]];*/
    
    NSString *body = [[message elementForName:@"body"] stringValue];
    NSArray * msgComponent = [body componentsSeparatedByString:@","];
     NSString * msgID = msgComponent[0] ;
     NSString *to = [[message attributeForName:@"from"] stringValue];
     NSString *NWid= [[NSUserDefaults standardUserDefaults] stringForKey:@"XMPPNotifyID"];
     NSString *from=[NSString stringWithFormat:@"%@@%@",NWid,[[NSUserDefaults standardUserDefaults] stringForKey:@"DomainName"]];
    NSString *messageStr = [NSString stringWithFormat:@"|ACK|%@|%@|%d|%@",NWid,msgID,status,msgComponent[msgComponent.count - 1]];
    
    if([messageStr length] > 0)
    {
        NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
        [body setStringValue:messageStr];
        
        NSXMLElement *message1 = [NSXMLElement elementWithName:@"message"];
        [message1 addAttributeWithName:@"type" stringValue:@"chat"];
        [message1 addAttributeWithName:@"to" stringValue:to];
        [message1 addAttributeWithName:@"from" stringValue:from];
        [message1 addChild:body];
        
        NSLog(@"mess%@",message1);
        [xmppStream sendElement:message1];
        
        
        
    }
}

- (void) resetCoreData
{
   
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"HealthWatchiOS.sqlite"];
    
    NSError* error = nil;
   /*
    //NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtURL:storeURL error:NULL];
    
    
    
    if([fileManager fileExistsAtPath:[NSString stringWithContentsOfURL:storeURL encoding:NSASCIIStringEncoding error:&error]])
    {
        [fileManager removeItemAtURL:storeURL error:nil];
    }
    
    self.managedObjectContext = nil;
    self.persistentStoreCoordinator = nil;*/
    
    NSPersistentStore *store = [self.persistentStoreCoordinator.persistentStores lastObject];
   
   
    [self.persistentStoreCoordinator removePersistentStore:store error:&error];
    [[NSFileManager defaultManager] removeItemAtURL:storeURL error:&error];
    
    
    NSLog(@"Data Reset");
    
    //Make new persistent store for future saves   (Taken From Above Answer)
    if (![self.persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // do something with the error
    }
    
    
}




@end
