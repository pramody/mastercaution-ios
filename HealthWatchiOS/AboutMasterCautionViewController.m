//
//  AboutMasterCautionViewController.m
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 18/06/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import "AboutMasterCautionViewController.h"


@interface AboutMasterCautionViewController ()

@end

@implementation AboutMasterCautionViewController
@synthesize aboutUsDetailsMainView,termsAndConditionsMainView,toursDetailsMainView,headerBlueViewAboutUs,headerTitleLbl,leftBackArrow,leftMenuButton,dashboardButton,buildnumber,versionnumber,mcdversion,versionnoheader,logDetailsMainView,txtloginformat,termsweb;

- (void)viewDidLoad {
    [super viewDidLoad];
    AppDelegate * appDelegate = ( AppDelegate *)UIApplication.sharedApplication.delegate ;
    appDelegate.orientationKey = @"PRO";
    [appDelegate forceOrientationTakePlace:appDelegate.orientationKey];
    self.slidingViewController.topViewAnchoredGesture = ECSlidingViewControllerAnchoredGestureTapping | ECSlidingViewControllerAnchoredGesturePanning;
    self.slidingViewController.customAnchoredGestures =@[];
    
    versionnumber.text =[NSString stringWithFormat:@"Version no: %@",[[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"]];
    versionnoheader.text=versionnumber.text;
    buildnumber.text = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
   
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
   
   
    mcdversion.text=[NSString stringWithFormat:@"MCD Version: %@", [defaults valueForKey:@"Firmware Version"]];
    
    NSLogger *logger = [[NSLogger alloc] init];

    txtloginformat.text=[logger logPrint];
    txtloginformat.editable=NO;
    
    //loading file in html
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"agreement" ofType:@"html"];
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    [termsweb loadHTMLString:htmlString baseURL: [[NSBundle mainBundle] bundleURL]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)goToDashboard:(id)sender{
    self.slidingViewController.topViewController.view.layer.transform = CATransform3DMakeScale(1, 1, 1);
    self.slidingViewController.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"DashboardStoryboardId"];
}

-(IBAction)leftSideButtonAction:(id)sender{
     [self.slidingViewController anchorTopViewToRightAnimated:YES];
}
-(IBAction)goToDetails:(UIButton *)sender{
    [self hideAllDetailsMainView];
    tagWas = (int)[sender tag] ;
    if ( tagWas == 1 )
    {
        //println("About us details")
        //aboutUsDetailsMainView.hidden = false
        viewToAnimate = aboutUsDetailsMainView;
        
    }
    else if ( tagWas == 2 )
    {
        //println("Terms and Conditions")
        //termsAndConditionsMainView.hidden = false
        viewToAnimate =  termsAndConditionsMainView;
    }
    else if ( tagWas == 3 )
    {
        //println("Tour is onn")
        // toursDetailsMainView.hidden = false
        viewToAnimate = toursDetailsMainView;
    }
    else if ( tagWas == 4 )
    {
        //println("Tour is onn")
        // toursDetailsMainView.hidden = false
        viewToAnimate = logDetailsMainView;
    }
    
    [self makeTransitionForView:viewToAnimate:false];
    

}
-(IBAction)backToMainAboutUs:(id)sender{
    if ( tagWas == 1 )
    {
        //println("About us details GO BACK")
        //aboutUsDetailsMainView.hidden = false
        viewToAnimate = aboutUsDetailsMainView;
        
    }
    else if ( tagWas == 2 )
    {
        //println("Terms and Conditions GO BACK")
        //termsAndConditionsMainView.hidden = false
        viewToAnimate =  termsAndConditionsMainView;
    }
    else if (tagWas == 3 )
    {
        //println("Tour is onn GO BACK")
        // toursDetailsMainView.hidden = false
        viewToAnimate = toursDetailsMainView;
    }
    else if (tagWas == 4 )
    {
        //println("Tour is onn GO BACK")
        // toursDetailsMainView.hidden = false
        viewToAnimate = logDetailsMainView;
    }
    
    
    tagWas = 0;
   
   [self makeTransitionForView:viewToAnimate:true];
}



// change the title as per user click
-(void)changeTitle
{
    NSString*title = @"ABOUT US";
    
    if (tagWas == 1){
        title = @"ABOUT MASTER CAUTION";
    } else if (tagWas == 2) {
        title = @"TERMS & CONDITIONS";
    }    else if (tagWas == 3) {
        title = @"TUTORIAL";}
    else if (tagWas == 4) {
        title = @"Log Information";}
    
    headerTitleLbl.text = title;
}

// Hide the Left Menu, Dashboard button
// Show the back arrow button

-(void) manipulateHeaderButtons:(BOOL)boolValue
{
    leftMenuButton.hidden   = !boolValue;
    dashboardButton.hidden  = !boolValue;
    
    leftBackArrow.hidden    = boolValue;
}

-(void)makeTransitionForView:(UIView*)forView :(BOOL)hideViewBool
{
    
    
    
    [UIView transitionWithView:forView
                      duration:1
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    animations:^{
                        
                        forView.hidden = hideViewBool;
                        // Hide or Show Header buttons.
                        [self manipulateHeaderButtons:hideViewBool];
                        [self changeTitle];
                    }
                    completion:nil];
  }



-(void)hideAllDetailsMainView
{
    aboutUsDetailsMainView.hidden          = true;
    termsAndConditionsMainView.hidden      = true;
    toursDetailsMainView.hidden            = true;
    logDetailsMainView.hidden=true;
}

-(IBAction)sendMail:(id)sender
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc] init];
        mailComposer.mailComposeDelegate = self;
        
        [mailComposer setSubject:@"MasterCaution Log File"];
        
      /*  UIImage *myImage = [UIImage imageNamed:@"safeguardmode.png"];
        NSData *imageData = UIImagePNGRepresentation(myImage);
        [mailComposer addAttachmentData:imageData mimeType:@"image/png" fileName:@"FullTitle.png"];*/
        
        NSString *emailBody = @"Hi there!!";
        [mailComposer setMessageBody:emailBody isHTML:NO];
        
        
        // Determine the file name and extension
        NSString * file;
        
       
        int Count;
        NSString *path;
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        path = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"logger"];
        NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:NULL];
        file=[NSString stringWithFormat:@"%@/%@",path,@"master-caution-logger.txt"];
        for (Count = 0; Count < (int)[directoryContent count]; Count++)
        {
            NSLog(@"File %d: %@", (Count + 1), [directoryContent objectAtIndex:Count]);
           // file=[directoryContent objectAtIndex:Count];=
        }
        
        
        NSArray *filepart = [file componentsSeparatedByString:@"."];
        NSString *filename =@"master-caution-logger";
        NSString *extension = [filepart objectAtIndex:1];
        
        // Get the resource path and read the file using NSData
       // NSString *filePath = [[NSBundle mainBundle] pathForResource:filename ofType:extension];
        NSData *fileData = [NSData dataWithContentsOfFile:file];
        
        // Determine the MIME type
        NSString *mimeType;
        if ([extension isEqualToString:@"jpg"]) {
            mimeType = @"image/jpeg";
        } else if ([extension isEqualToString:@"png"]) {
            mimeType = @"image/png";
        } else if ([extension isEqualToString:@"doc"]) {
            mimeType = @"application/msword";
        } else if ([extension isEqualToString:@"ppt"]) {
            mimeType = @"application/vnd.ms-powerpoint";
        } else if ([extension isEqualToString:@"html"]) {
            mimeType = @"text/html";
        } else if ([extension isEqualToString:@"pdf"]) {
            mimeType = @"application/pdf";
        }
        else if ([extension isEqualToString:@"txt"]) {
            mimeType = @"text/plain";
        }
        
        // Add attachment
        [mailComposer addAttachmentData:fileData mimeType:mimeType fileName:filename];
        
        // Present mail view controller on screen
        [self presentViewController:mailComposer animated:YES completion:NULL];
        
       // [self presentModalViewController:mailComposer animated:YES];
    }
    
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                        message:@"Your device has not configure email "
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}
     
#pragma mark - mail compose delegate
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}


@end
