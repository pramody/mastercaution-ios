//
//  LiveEcgMonitor.m
//  HealthWatch
//
//  Created by METSL MAC MINI on 05/09/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import "LiveEcgMonitor.h"
#import <QuartzCore/QuartzCore.h>
#import "LeadPlayer.h"
#import "Helper.h"
#import "AppDelegate.h"
#import "ConfigureMCDController.h"
#import "GlobalVars.h"
#import <CoreText/CoreText.h>
#import " Draw the frame.h"

@interface LiveEcgMonitor ()
{
     int leadcnt2;
    
}
@property (nonatomic, assign) BOOL wrap;
@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) NSMutableArray *itemsdata;
@property (nonatomic, strong) NSMutableArray *itemsunit;
@property (nonatomic, strong) NSMutableArray *itemsimage;

@end

@implementation LiveEcgMonitor

@synthesize leads, btnStart,scrollView, labelProfileId, labelProfileName, btnDismiss;
@synthesize liveMode, labelRate, statusInfo, startRecordingIndex, HR, stopTheTimer;
@synthesize buffer, DEMO, labelMsg, photoView, btnRefresh, newBornMode;
@synthesize carousel,swipeview;
@synthesize wrap;
@synthesize items,postureimage,itemsdata,itemsunit,itemsimage,timer,repeatingTimer;



int leadCount=13;
int sampleRate=125;
int respiration=0;
int carousalrespiration=0;
float uVpb = 0.9;
int datacnt;
float drawingInterval = 0.04; // the interval is greater, the drawing is faster, but more choppy, smaller -> slower and smoother
int bufferSecond = 300;

float cur_x12=0.0;

- (void)viewDidLoad {
   // [super viewDidLoad];
    
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
     NSLogger *logger=[[NSLogger alloc]init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"MCDSettings" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
        logger.degbugger = true;
        [logger log:@"Coredata Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:[error localizedDescription], @"Adding clinical record", nil] error:TRUE];
        
    } else {
       
        if (result.count > 0) {
            NSManagedObject *mcdsett = (NSManagedObject *)[result objectAtIndex:0];
           
            respiration=[[mcdsett valueForKey:@"respiration"]intValue];
            respiration=0;
            carousalrespiration=[[mcdsett valueForKey:@"respiration"]intValue];
           
            switch ( [[mcdsett valueForKey:@"lead_config"]intValue]) {
                case 0:
                    leadCount = 7;//3
                    break;
                case 1:
                    leadCount = 8;//5 lead
                    break;
                case 2:
                    leadCount = 13;//12
                    break;
                case 3:
                    leadCount = 16;//15
                    break;
                    
                default:
                    break;
            }
            
            
            switch ( [[mcdsett valueForKey:@"sampling_rate"]intValue]) {
                case 0:
                    sampleRate = 125;//3
                    break;
                case 1:
                   // sampleRate = 125;
                     sampleRate = 250;//5 lead
                    break;
                case 2:
                    sampleRate = 500;//12
                    break;
                default:
                    break;
            }

        }
    }

  
   // leadCount=13;
    //sampleRate=125;
     //respiration=0;
    //if(respiration==0)
      //  leadCount=leadCount-1;
   
    [self addViews];
    [self initialMonitor];
  
   
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setUp) name:@"VitalParam" object:nil];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AccessoryDisconnect) name:@"AccessoryDisconnect" object:nil];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AccessoryConnect) name:@"AccessoryConnect" object:nil];
    

    
}


- (void)viewWillAppear:(BOOL)animated
{
    [self initialloaddata];
    

}




- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    
    NSLog(@" didFailLoadWithError");
}



-(void)initialloaddata
{
    GlobalVars *global = [GlobalVars sharedInstance];
    if(global.BluetoothConnectionflag==1)
    {
        self.autoScrollLabel.text = @"Please connect Device...!     Please connect Device...!     Please connect Device...!     Please connect Device...!";
    }
    else
    {
        self.autoScrollLabel.text = @"Syncing MCD settings. Please wait...!     Syncing MCD settings. Please wait...!     Syncing MCD settings. Please wait...!     Syncing MCD settings. Please wait...!";
    }
    
    UIColor * redUIColor= [UIColor colorWithPatternImage:[UIImage imageNamed:@"red_color.png"]];
    
    self.autoScrollLabel.textColor = redUIColor;
    self.autoScrollLabel.labelSpacing = 30; // distance between start and end labels
    self.autoScrollLabel.pauseInterval = 1.7; // seconds of pause before scrolling starts again
    self.autoScrollLabel.scrollSpeed = 30; // pixels per second
    // self.autoScrollLabel.textAlignment = NSTextAlignmentCenter; // centers text when no auto-scrolling is applied
    self.autoScrollLabel.fadeLength = 12.f;
    self.autoScrollLabel.backgroundColor=[UIColor whiteColor];
    self.autoScrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.autoScrollLabel observeApplicationNotifications];
    
    [self setLeadsLayout:self.interfaceOrientation];
    
    AppDelegate *  appDelegate =(AppDelegate *)UIApplication.sharedApplication.delegate ;
    appDelegate.orientationKey = @"PRO";
    [appDelegate forceOrientationTakePlace:appDelegate.orientationKey];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
   
    
    GlobalVars *globals = [GlobalVars sharedInstance];
    if(globals.currentSetting==NONE)
    {
        if(global.BluetoothConnectionflag==1)
        {
            [self.autoScrollLabel setHidden:NO];
        }
        else
        {
            if([[defaults valueForKey:@"MCDSyncflag"] isEqualToString: @"2"])
            {
            [self.autoScrollLabel setHidden:YES] ;
               
              
        ConfigureMCDController *config=[[ConfigureMCDController alloc]init];
        [config StartContinousECG];
            }
        }
    }
    else
    {
        timer= [NSTimer scheduledTimerWithTimeInterval: 2.0 target: self
                                               selector: @selector(callAfterSixtySecond:) userInfo: nil repeats: YES];
        self.repeatingTimer = timer;
    }
    
    
   
     datacnt=0;
    //carousal
    
    carousel.type=iCarouselTypeLinear;
    carousel.pagingEnabled = NO;
    //self.carousel.type = iCarouselTypeLinear;// iCarouselTypeCoverFlow2;
   [self.carousel scrollToItemAtIndex:2 animated:YES];
    
    // swipeview.pagingEnabled = NO;
   
}

-(void) callAfterSixtySecond:(NSTimer*) t
{
    
  
    GlobalVars *globals = [GlobalVars sharedInstance];
      NSLog(@"cREd");
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if(globals.currentSetting==NONE)
    {
        if([[defaults valueForKey:@"MCDSyncflag"] isEqualToString: @"2"])
        {
          NSLog(@"timer stop");
       [self.autoScrollLabel setHidden:YES] ;
        ConfigureMCDController *config=[[ConfigureMCDController alloc]init];
        [config StartContinousECG];
        [repeatingTimer invalidate];
        repeatingTimer = nil;
        }
        
    }
}



- (void)viewDidAppear:(BOOL)animated
{
    [self startLiveMonitoring];
}

-(BOOL)canBecomeFirstResponder
{
    return YES;
}


- (void)viewWillDisappear:(BOOL)animated
{
    ConfigureMCDController *config=[[ConfigureMCDController alloc]init];
    [config StopContinousECG];
   
    [repeatingTimer invalidate];
    repeatingTimer =nil;
    data12Arrays =[[NSArray alloc]init];
    
    datacnt=0;
    GlobalVars *globals = [GlobalVars sharedInstance];
    [[globals plotdata] removeAllObjects];
    LeadPlayer *lead = [[LeadPlayer alloc] init];
    [lead resetBuffer];
    [super viewWillDisappear:animated];
}


#pragma mark -
#pragma mark Initialization, Monitoring and Timer events

- (void)initialMonitor
{
    bufferCount = 10;
    self.labelMsg.text = @"25mm/s  10mm/mv";
    
    self.btnRecord.enabled = NO;
    self.btnDismiss.enabled = YES;
    
    NSMutableArray *buf = [[NSMutableArray alloc] init];
    self.buffer = buf;
}


- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

-(void)AccessoryDisconnect
{
    NSLog(@"accessorydis");
    [self.autoScrollLabel setHidden:NO];
    UIColor * redUIColor= [UIColor colorWithPatternImage:[UIImage imageNamed:@"red_color.png"]];
    self.autoScrollLabel.text = @"Device Disconnected...!     Device Disconnected...!     Device Disconnected...!     Device Disconnected...!     Device Disconnected...!     Device Disconnected...!";
    self.autoScrollLabel.textColor = redUIColor;
    self.autoScrollLabel.labelSpacing = 30; // distance between start and end labels
    self.autoScrollLabel.pauseInterval = 0.5; // seconds of pause before scrolling starts again
    self.autoScrollLabel.scrollSpeed = 30; // pixels per second
    self.autoScrollLabel.fadeLength = 12.f;
    self.autoScrollLabel.backgroundColor=[UIColor whiteColor];
    self.autoScrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.autoScrollLabel observeApplicationNotifications];
    [self.autoScrollLabel refreshLabels];
   // [self.view addSubview:self.autoScrollLabel];
    data12Arrays =[[NSArray alloc]init];
    
    datacnt=0;
     GlobalVars *globals = [GlobalVars sharedInstance];
    [[globals plotdata] removeAllObjects];
    LeadPlayer *lead = [[LeadPlayer alloc] init];
    [lead resetBuffer];

    if(respiration==0)
    {
        leadcnt2=leadCount-1;
    }
    else
        leadcnt2=leadCount;
    for (int i=0; i<leadcnt2; i++)
    {
        LeadPlayer *lead = [self.leads objectAtIndex:i];
        lead.alpha = 0;
    }

    //[lead clearDrawing];
    [lead redraw];
   
   [self.view setNeedsDisplay];
    
    GlobalVars * global=[GlobalVars sharedInstance];
    global.mcd_HeartRate=0;
    global.mcd_RespirationRate=0;
    global.mcd_Temperature=0;
    global.mcd_RR=0;
    global.mcd_PR=0;
    global.mcd_QRS=0;
    global.mcd_QT=0;
    global.mcd_QTc=0;
    postureimage.image=[UIImage imageNamed:@""];
    [carousel reloadData];
    [repeatingTimer invalidate];
    repeatingTimer=nil;
        
}


-(void)AccessoryConnect
{
    NSLog(@"accessoryconet");
    [self.autoScrollLabel setHidden:NO];
    UIColor * redUIColor= [UIColor colorWithPatternImage:[UIImage imageNamed:@"red_color.png"]];
    self.autoScrollLabel.text = @"Syncing MCD settings. Please wait...!     Syncing MCD settings. Please wait...!     Syncing MCD settings. Please wait...!     Syncing MCD settings. Please wait...!";
    self.autoScrollLabel.textColor = redUIColor;
    self.autoScrollLabel.labelSpacing = 30; // distance between start and end labels
    self.autoScrollLabel.pauseInterval = 1.7; // seconds of pause before scrolling starts again
    self.autoScrollLabel.scrollSpeed = 30; // pixels per second
    self.autoScrollLabel.fadeLength = 12.f;
    self.autoScrollLabel.backgroundColor=[UIColor whiteColor];
    self.autoScrollLabel.scrollDirection = CBAutoScrollDirectionLeft;
    [self.autoScrollLabel observeApplicationNotifications];
    [self.autoScrollLabel refreshLabels];
   
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([[defaults valueForKey:@"MCDSyncflag"] isEqualToString: @"1"])
    {
    timer=  [NSTimer scheduledTimerWithTimeInterval: 2.0 target: self
                                           selector: @selector(callAfterSixtySecond:) userInfo: nil repeats: YES];
        self.repeatingTimer = timer;
    }
    else if ([[defaults valueForKey:@"MCDSyncflag"] isEqualToString: @"2"])
    {
        [self.autoScrollLabel setHidden:YES];
        ConfigureMCDController *config=[[ConfigureMCDController alloc]init];
        [config StartContinousECG];
       
    }
   
    if(respiration==0)
    {
        leadcnt2=leadCount-1;
    }
    else
        leadcnt2=leadCount;
    for (int i=0; i<leadcnt2; i++)
    {
        LeadPlayer *lead = [self.leads objectAtIndex:i];
        lead.alpha = 1;
    }

    // [self.view addSubview:self.autoScrollLabel];
    
    
    
    [self.view setNeedsDisplay];
}
- (void)startLiveMonitoring
{
    monitoring = YES;
    stopTheTimer = NO;
    
   // [self startTimer_popDataFromBuffer];
    [self startTimer_drawing];
}

- (void)startTimer_popDataFromBuffer
{
    CGFloat popDataInterval = 420.0f / sampleRate;
     //CGFloat popDataInterval = 0.01;
  
    popDataTimer = [NSTimer scheduledTimerWithTimeInterval:popDataInterval
                                                    target:self
                                                  selector:@selector(timerEvent_popData)
                                                  userInfo:NULL
                                                   repeats:YES];
}


- (void)startTimer_drawing
{
    drawingTimer = [NSTimer scheduledTimerWithTimeInterval:drawingInterval
                                                    target:self
                                                  selector:@selector(timerEvent_drawing)
                                                  userInfo:NULL
                                                   repeats:YES];
}


- (void)timerEvent_drawing
{
    [self drawRealTime];
}

- (void)timerEvent_popData
{
    
    //[self popDemoDataAndPushToLeads];
}


- (void)popDemoDataAndPushToLeads1:(NSInteger [])in_pt :(int)numberoflead
{
  
    if(leadCount==7)
    {
        numberoflead=6;
    }
    else if (leadCount==8)
    {
        numberoflead=7;
    }
    else if (leadCount==13)
    {
       numberoflead=12;
    }
    else
    {
        numberoflead=15;
    }
    
    //[self.vwMonitor setNeedsDisplay];
    GlobalVars *globals = [GlobalVars sharedInstance];
    self.leads= globals.ecgarray;
    
    for (int i=0; i<numberoflead; i++)
    {
        LeadPlayer *lead = [self.leads objectAtIndex:i];
        //  NSLog(@"%@",[NSNumber numberWithInteger:in_pt[i]]);
        
        
        
         [lead.pointsArray addObject:[NSNumber numberWithInteger:in_pt[i]]];
        
        if (lead.pointsArray.count > bufferSecond * sampleRate)
        {
            [lead resetBuffer];
            //  [globals.ecgarray removeAllObjects];
            
        }
        
        
       /* if (in_pt[i]<-100||in_pt[i]<500)
        {
           
            // [lead.pointsArray addObject:_pointsArray];
        }
        else{
            [lead.pointsArray addObject:@"0"];
        }*/

       
      
       // [lead fireDrawing];
        //[lead fireDrawing :cur_x12];
        
        if (i==0)
        {
            countOfPointsInQueue = (int)lead.pointsArray.count;
            currentDrawingPoint = lead.currentPoint;
        }
        
   
       // NSLog(@"%@",[NSNumber numberWithInteger:in_pt[i]]);
        
    }
    
   // [lead.pointsArray removeAllObjects];
   /* cur_x12=cur_x12+1;
    if(cur_x12>768)
    {
        cur_x12=1;
    }
    */
   
}




- (void)pushPoints:(NSArray *)_pointsArray data12Index:(NSInteger)data12Index;
{
    LeadPlayer *lead = [self.leads objectAtIndex:data12Index];
    
    if (lead.pointsArray.count > bufferSecond * sampleRate)
    {
       // [lead resetBuffer];
    }
    
    if (lead.pointsArray.count - lead.currentPoint <= 2000)
    {
        [lead.pointsArray addObjectsFromArray:_pointsArray];
       // [lead.pointsArray addObject:_pointsArray];
    }
    
    if (data12Index==0)
    {
        countOfPointsInQueue = (int)lead.pointsArray.count;
        currentDrawingPoint = lead.currentPoint;
    }
}

- (NSArray *)convertDemoData:(short *)rawdata dataLength:(int)length doWilsonConvert:(BOOL)wilsonConvert
{
    NSMutableArray *data = [[NSMutableArray alloc] init];
    for (int i=0; i<12; i++)
    {
        NSMutableArray *array = [[NSMutableArray alloc] init];
        [data addObject:array];
    }
    
    for (int i=0; i<length; i++)
    {
        for (int j=0; j<leadCount; j++)
        {
           
            NSMutableArray *array = [data objectAtIndex:j];
            NSNumber *number = [NSNumber numberWithInt:rawdata[i]];
            [array insertObject:number atIndex:i];
        }
    }
    
    return data;
}

- (void)drawRealTime
{
    LeadPlayer *l = [self.leads objectAtIndex:0];
    
    if (l.pointsArray.count > l.currentPoint)
    {
        for (LeadPlayer *lead in self.leads)
        {
            
              [lead fireDrawing];
           
        }
    }
}

- (void)addViews
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    NSMutableArray *ecgtext;
   if(leadCount==7)
    {
        if(respiration==0)
         ecgtext=[[NSMutableArray alloc]initWithObjects:@" I",@" II",@" III",@"aVR",@"aVL",@"aVF", nil];
        else
        ecgtext=[[NSMutableArray alloc]initWithObjects:@" I",@" II",@" III",@"aVR",@"aVL",@"aVF",@"Resp", nil];
    }
    else if (leadCount==8)
    {
        if(respiration==0)
            ecgtext=[[NSMutableArray alloc]initWithObjects:@" I",@" II",@" III",@"aVR",@"aVL",@"aVF",@"V1", nil];
        else
        ecgtext=[[NSMutableArray alloc]initWithObjects:@" I",@" II",@" III",@"aVR",@"aVL",@"aVF",@"V1",@"Resp", nil];
    }
    else if (leadCount==13)
    {
        if(respiration==0)
            ecgtext=[[NSMutableArray alloc]initWithObjects:@" I",@" II",@" III",@"aVR",@"aVL",@"aVF",@"V1",@"V2",@"V3",@"V4",@"V5",@"V6", nil];
        else
        ecgtext=[[NSMutableArray alloc]initWithObjects:@" I",@" II",@" III",@"aVR",@"aVL",@"aVF",@"V1",@"V2",@"V3",@"V4",@"V5",@"V6",@"Resp", nil];
    }
    else
    {
        if(respiration==0)
            ecgtext=[[NSMutableArray alloc]initWithObjects:@" I",@" II",@" III",@"aVR",@"aVL",@"aVF",@"V1",@"V2",@"V3",@"V4",@"V5",@"V6",@"V7",@"V3R",@"V4R", nil];
        else
        ecgtext=[[NSMutableArray alloc]initWithObjects:@" I",@" II",@" III",@"aVR",@"aVL",@"aVF",@"V1",@"V2",@"V3",@"V4",@"V5",@"V6",@"V7",@"V3R",@"V4R",@"Resp", nil];
    }
    
    if(respiration==0)
    {
        leadcnt2=leadCount-1;
    }
    else
        leadcnt2=leadCount;
    for (int i=0; i<leadcnt2; i++) {
        
        LeadPlayer *lead = [[LeadPlayer alloc] init];
        
        lead.layer.cornerRadius = 8;
        lead.layer.borderColor = [[UIColor grayColor] CGColor];
        lead.layer.borderWidth = 1;
        lead.clipsToBounds = YES;
        
        lead.index = i;
        lead.pointsArray = [[NSMutableArray alloc] init];
        
        lead.liveMonitor = self;
		 lead.leadcolor=[UIColor greenColor];
        [array insertObject:lead atIndex:i];
        lead.label=ecgtext[i];
        
        [self.scrollView addSubview:lead];
    }
    
    
    self.leads = array;
     GlobalVars *globals = [GlobalVars sharedInstance];
    globals.ecgarray=array;
    
}

- (void)setLeadsLayout:(UIInterfaceOrientation)orientation
{
    float margin = 5;
    NSInteger leadHeight;//self.scrollView.frame.size.height / 3 - margin * 2;
    NSInteger leadWidth;//self.scrollView.frame.size.width;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        leadHeight = 71;
        leadWidth =self.scrollView.frame.size.width;
        scrollView.contentSize=CGSizeMake(self.scrollView.frame.size.width,1500);
    }
    else
    {
        leadHeight = 35;//63//self.scrollView.frame.size.height / 3 - margin * 2;
         leadWidth =320;
        scrollView.contentSize = CGSizeMake(320,700);
    }
    
    if(respiration==0)
    {
        leadcnt2=leadCount-1;
    }
    else
        leadcnt2=leadCount;
    
    for (int i=0; i<leadcnt2; i++)
    {
        LeadPlayer *lead = [self.leads objectAtIndex:i];
        float pos_y = i * (margin + leadHeight);
        
        [lead setFrame:CGRectMake(0., pos_y, leadWidth, leadHeight)];
        lead.pos_x_offset = lead.currentPoint;
        lead.alpha = 0;
        [lead setNeedsDisplay];
    }
    
    [UIView animateWithDuration:0.6f animations:^{
        for (int i=0; i<leadcnt2; i++)
        {
            LeadPlayer *lead = [self.leads objectAtIndex:i];
            lead.alpha = 1;
        }
    }];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    
}

-(IBAction)goBackToClinicalRecordsAction:(id)sender
{
   
    drawingTimer = nil;
    readDataTimer = nil;
    popDataTimer = nil;
    data12Arrays =[[NSArray alloc]init];
    Allarray=[[NSMutableArray alloc]init];
    datacnt=0;
     stopTheTimer = YES;
    GlobalVars *globals = [GlobalVars sharedInstance];
    [[globals plotdata] removeAllObjects];
    LeadPlayer *lead = [[LeadPlayer alloc] init];
    [lead resetBuffer];
    [lead redraw];
    [lead clearDrawing];
    [self dismissViewControllerAnimated:true completion:^{
        nil;
    }];
    [repeatingTimer invalidate];
    repeatingTimer = nil;
    //self.dismissViewControllerAnimated(true, completion: nil)
}

#pragma mark -
#pragma mark Memory and others

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
  [super viewDidUnload];
    [repeatingTimer invalidate];
    repeatingTimer = nil;
}

- (void)dealloc {
    
    
    drawingTimer = nil;
    readDataTimer = nil;
    popDataTimer = nil;
    
}


-(IBAction)goToDashboard:(id)sender{
    
    [self dismissViewControllerAnimated:true completion:^{
        nil;
    }];

    [repeatingTimer invalidate];
    repeatingTimer = nil;
   /* if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Mainstoryboard_iPad" bundle:nil];
        ECSlidingViewController * controller = (ECSlidingViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ECSlidingStoryboardId"];
        [self presentViewController:controller animated:YES completion:nil];
        
    }
    else
    {
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ECSlidingViewController * controller = (ECSlidingViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ECSlidingStoryboardId"];
        [self presentViewController:controller animated:YES completion:nil];
    }*/
}


#pragma mark -
#pragma mark iCarousel methods
#pragma mark - HTHorizontalSelectionListDataSource Protocol Methods


- (void) VitalParam
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"VitalParam" object:nil];
    
    
}


-(void)AccDisonncet
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AccessoryDisconnect" object:nil];
}

-(void)AccConnect
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AccessoryConnect" object:nil];
}

- (void)setUp
{
    GlobalVars * global=[GlobalVars sharedInstance];
    NSString *HR1=[NSString stringWithFormat:@"%ld",(long)global.mcd_HeartRate];
    NSString *Resprate=[NSString stringWithFormat:@"%ld",(long)global.mcd_RespirationRate];
     NSString *Temp=[NSString stringWithFormat:@"%ld",(long)global.mcd_Temperature];
     NSString *RR=[NSString stringWithFormat:@"%ld",(long)global.mcd_RR];
     NSString *PR=[NSString stringWithFormat:@"%ld",(long)global.mcd_PR];
     NSString *QRS=[NSString stringWithFormat:@"%ld",(long)global.mcd_QRS];
     NSString *QT=[NSString stringWithFormat:@"%ld",(long)global.mcd_QT];
     NSString *QTC=[NSString stringWithFormat:@"%ld",(long)global.mcd_QTc];
    
     if(global.mcd_Posture==1)
     {
         postureimage.image=[UIImage imageNamed:@"laying.png"];
     }
    else if(global.mcd_Posture==2)
    {
         postureimage.image=[UIImage imageNamed:@"running.png"];
    }
    else if(global.mcd_Posture==3)
    {
        postureimage.image=[UIImage imageNamed:@"walking.png"];
    }
    else
    {
         postureimage.image=[UIImage imageNamed:@"standing.png"];
      
    }
    
    //set up data
    self.wrap = NO;
    self.itemsdata = [NSMutableArray array];
    
    NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
    CommonHelper *help=[[CommonHelper alloc]init];
    
    NSString *temperaturetype=[TempDefault objectForKey:@"TEMPERATURE"];
  
    
    
        if(carousalrespiration==0)
        {
            
            if([temperaturetype isEqualToString:@"CELSIUS"])
            {
                self.items=[[NSMutableArray alloc]initWithObjects:@"HR",@"Temp",@"RR",@"PR",@"QRS",@"QT",@"QTC",nil];
                self.itemsunit=[[NSMutableArray alloc]initWithObjects:@"bpm",@"c",@"ms",@"ms",@"ms",@"ms",@"ms",nil];
                self.itemsimage=[[NSMutableArray alloc]initWithObjects:@"bpm.png",@"temperature.png",@"clock.png",@"clock.png",@"clock.png",@"clock.png",@"clock.png",nil];
                
                [self.itemsdata addObject:HR1];
                [self.itemsdata addObject:Temp];
                [self.itemsdata addObject:RR];
                [self.itemsdata addObject:PR];
                [self.itemsdata addObject:QRS];
                [self.itemsdata addObject:QT];
                [self.itemsdata addObject:QTC];
            }
            else
            {
                self.items=[[NSMutableArray alloc]initWithObjects:@"HR",@"Temp",@"RR",@"PR",@"QRS",@"QT",@"QTC",nil];
                self.itemsunit=[[NSMutableArray alloc]initWithObjects:@"bpm",@"F",@"ms",@"ms",@"ms",@"ms",@"ms",nil];
                self.itemsimage=[[NSMutableArray alloc]initWithObjects:@"bpm.png",@"temperature.png",@"clock.png",@"clock.png",@"clock.png",@"clock.png",@"clock.png",nil];
                
             
                [self.itemsdata addObject:HR1];
                [self.itemsdata addObject:[NSString stringWithFormat:@"%.1f",[[help celsiustoFahernheit:Temp]floatValue]]];
                [self.itemsdata addObject:RR];
                [self.itemsdata addObject:PR];
                [self.itemsdata addObject:QRS];
                [self.itemsdata addObject:QT];
                [self.itemsdata addObject:QTC];
            }
           
            
           
        }
        else
        {
            if(global.mcd_Posture==1)
            {
                if([temperaturetype isEqualToString:@"CELSIUS"])
                {
                    self.items=[[NSMutableArray alloc]initWithObjects:@"HR",@"Resp",@"Temp",@"RR",@"PR",@"QRS",@"QT",@"QTC",nil];
                    self.itemsunit=[[NSMutableArray alloc]initWithObjects:@"bpm",@"rpm",@"c",@"ms",@"ms",@"ms",@"ms",@"ms",nil];
                    self.itemsimage=[[NSMutableArray alloc]initWithObjects:@"bpm.png",@"rpm.png",@"temperature.png",@"clock.png",@"clock.png",@"clock.png",@"clock.png",@"clock.png",nil];
                    [self.itemsdata addObject:HR1];
                    [self.itemsdata addObject:Resprate];
                    [self.itemsdata addObject:Temp];
                    [self.itemsdata addObject:RR];
                    [self.itemsdata addObject:PR];
                    [self.itemsdata addObject:QRS];
                    [self.itemsdata addObject:QT];
                    [self.itemsdata addObject:QTC];
                }
                else
                {
                    self.items=[[NSMutableArray alloc]initWithObjects:@"HR",@"Resp",@"Temp",@"RR",@"PR",@"QRS",@"QT",@"QTC",nil];
                    self.itemsunit=[[NSMutableArray alloc]initWithObjects:@"bpm",@"rpm",@"F",@"ms",@"ms",@"ms",@"ms",@"ms",nil];
                    self.itemsimage=[[NSMutableArray alloc]initWithObjects:@"bpm.png",@"rpm.png",@"temperature.png",@"clock.png",@"clock.png",@"clock.png",@"clock.png",@"clock.png",nil];
                    [self.itemsdata addObject:HR1];
                    [self.itemsdata addObject:Resprate];
                    [self.itemsdata addObject:[NSString stringWithFormat:@"%.1f",[[help celsiustoFahernheit:Temp]floatValue]]];
                    [self.itemsdata addObject:RR];
                    [self.itemsdata addObject:PR];
                    [self.itemsdata addObject:QRS];
                    [self.itemsdata addObject:QT];
                    [self.itemsdata addObject:QTC];
                }
                
               
            }
            else
            {
                
                if([temperaturetype isEqualToString:@"CELSIUS"])
                {
                self.items=[[NSMutableArray alloc]initWithObjects:@"HR",@"Temp",@"RR",@"PR",@"QRS",@"QT",@"QTC",nil];
                self.itemsunit=[[NSMutableArray alloc]initWithObjects:@"bpm",@"c",@"ms",@"ms",@"ms",@"ms",@"ms",nil];
                self.itemsimage=[[NSMutableArray alloc]initWithObjects:@"bpm.png",@"temperature.png",@"clock.png",@"clock.png",@"clock.png",@"clock.png",@"clock.png",nil];
                
                [self.itemsdata addObject:HR1];
                [self.itemsdata addObject:Temp];
                [self.itemsdata addObject:RR];
                [self.itemsdata addObject:PR];
                [self.itemsdata addObject:QRS];
                [self.itemsdata addObject:QT];
                [self.itemsdata addObject:QTC];
                }
                else
                {
                    self.items=[[NSMutableArray alloc]initWithObjects:@"HR",@"Temp",@"RR",@"PR",@"QRS",@"QT",@"QTC",nil];
                    self.itemsunit=[[NSMutableArray alloc]initWithObjects:@"bpm",@"F",@"ms",@"ms",@"ms",@"ms",@"ms",nil];
                    self.itemsimage=[[NSMutableArray alloc]initWithObjects:@"bpm.png",@"temperature.png",@"clock.png",@"clock.png",@"clock.png",@"clock.png",@"clock.png",nil];
                    
                    [self.itemsdata addObject:HR1];
                    [self.itemsdata addObject:[NSString stringWithFormat:@"%.1f",[[help celsiustoFahernheit:Temp]floatValue]]];
                    [self.itemsdata addObject:RR];
                    [self.itemsdata addObject:PR];
                    [self.itemsdata addObject:QRS];
                    [self.itemsdata addObject:QT];
                    [self.itemsdata addObject:QTC];
                }
            }
        }
    
    
   
   
   /* for (int i = 0; i < 10; i++)
    {
        [self.items addObject:@(i)];
    }*/
   
    
    
    //[self.view setNeedsDisplay];
   
    [carousel setNeedsDisplay];
    [carousel reloadData];
    
    [swipeview setNeedsDisplay];
    [swipeview reloadData];
   
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]))
    {
        [self setUp];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder]))
    {
        [self setUp];
    }
    return self;
}

/*- (NSInteger)numberOfItemsInCarousel:(__unused iCarousel *)carousel1
{
  
    return (NSInteger)[self.items count];
    
}*/


- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel{
return [self.items count];
}


- (UIView *)carousel:(__unused iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
   
    UILabel *label = nil;
    UILabel *label1 = nil;
      UILabel *label2 = nil;
    UIImageView *unitimage;
     UIImageView *boundimage;
    //create new view if no view is available for recycling
   // if (view == nil)
   // {
        view = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, 60.0f, 100.0f)];
        //((UIImageView *)view).image = [UIImage imageNamed:@"page.png"];
        view.contentMode = UIViewContentModeLeft;
         label =[[UILabel alloc] init];
        [label setFrame:CGRectMake(8,15,50,20)];
       //  label = [[UILabel alloc] initWithFrame:CGRectMake(0,0,20,20)];
       // label = [[UILabel alloc] initWithFrame:view.bounds];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [label.font fontWithSize:10];
        label.textColor=[UIColor whiteColor];
        label.tag = 1;
        
        label1 =[[UILabel alloc] init];
        [label1 setFrame:CGRectMake(8,35,55,30)];
        label1.backgroundColor = [UIColor clearColor];
        label1.textAlignment = NSTextAlignmentCenter;
        label1.font = [label.font fontWithSize:20];
        label1.textColor=[UIColor whiteColor];
        label1.tag = 1;
        
        label2 =[[UILabel alloc] init];
        [label2 setFrame:CGRectMake(18,60,50,20)];
        label2.backgroundColor = [UIColor clearColor];
        label2.textAlignment = NSTextAlignmentCenter;
        label2.font = [label.font fontWithSize:10];
        label2.textColor=[UIColor whiteColor];
        label2.tag = 1;
        
        unitimage=[[UIImageView alloc]init];
        [unitimage setFrame:CGRectMake(17,65,10,10)];
        
        boundimage=[[UIImageView alloc]init];
        [boundimage setFrame:CGRectMake(0,0,2,80)];
        
      
        [view addSubview:label];
        [view addSubview:label1];
        [view addSubview:label2];
        [view addSubview:unitimage];
        [view addSubview:boundimage];
        
        

  //  }
   // else
    //{
        //get a reference to the label in the recycled view
      //  label = (UILabel *)[view viewWithTag:1];
        
    
    //}
    
    //set item label
    //remember to always set any properties of your carousel item
    //views outside of the `if (view == nil) {...}` check otherwise
    //you'll get weird issues with carousel item content appearing
    //in the wrong place in the carousel
    label.text =  self.items[(NSUInteger)index];//[self.items[(NSUInteger)index] stringValue];
    label1.text =  self.itemsdata[(NSUInteger)index];
     label2.text =  self.itemsunit[(NSUInteger)index];
     [unitimage setImage:[UIImage imageNamed:[self.itemsimage objectAtIndex:index]]];
    if(index!=0)
    boundimage.image=[UIImage imageNamed:@"vertical_blue_line.png"];
    
   
    
    return view;
}



- (NSInteger)numberOfPlaceholdersInCarousel:(__unused iCarousel *)carousel
{
    
    //note: placeholder views are only displayed on some carousels if wrapping is disabled
    return 2;
}

- (UIView *)carousel:(__unused iCarousel *)carousel placeholderViewAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    UILabel *label = nil;
    
    //create new view if no view is available for recycling
    if (view == nil)
    {
        //don't do anything specific to the index within
        //this `if (view == nil) {...}` statement because the view will be
        //recycled and used with other index values later
        view = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, 100.0f, 100.0f)];
      //  ((UIImageView *)view).image = [UIImage imageNamed:@"page.png"];
        view.contentMode = UIViewContentModeLeft;
        
       
        label = [[UILabel alloc] initWithFrame:view.bounds];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [label.font fontWithSize:20.0f];
        label.tag = 1;
        [view addSubview:label];
    }
    else
    {
        //get a reference to the label in the recycled view
        label = (UILabel *)[view viewWithTag:1];
    }
    
    //set item label
    //remember to always set any properties of your carousel item
    //views outside of the `if (view == nil) {...}` check otherwise
    //you'll get weird issues with carousel item content appearing
    //in the wrong place in the carousel
   // label.text = (index == 0)? @"[": @"]";
    
    return view;
}

- (CATransform3D)carousel:(__unused iCarousel *)carousel itemTransformForOffset:(CGFloat)offset baseTransform:(CATransform3D)transform
{
    //implement 'flip3D' style carousel
    transform = CATransform3DRotate(transform, M_PI / 8.0f, 0.0f, 1.0f, 0.0f);
    return CATransform3DTranslate(transform, 0.0f, 0.0f, offset * self.carousel.itemWidth);
    
}

- (CGFloat)carousel:(__unused iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    //customize carousel display
    switch (option)
    {
        case iCarouselOptionWrap:
        {
            //normally you would hard-code this to YES or NO
            return self.wrap;
        }
        case iCarouselOptionSpacing:
        {
            //add a bit of spacing between the item views
            return value * 1.05f;
        }
        case iCarouselOptionFadeMax:
        {
            if (self.carousel.type == iCarouselTypeCustom)
            {
                //set opacity based on distance from camera
                return 0.0f;
            }
            return value;
        }
        case iCarouselOptionShowBackfaces:
        case iCarouselOptionRadius:
        case iCarouselOptionAngle:
        case iCarouselOptionArc:
        case iCarouselOptionTilt:
        case iCarouselOptionCount:
        case iCarouselOptionFadeMin:
        case iCarouselOptionFadeMinAlpha:
        case iCarouselOptionFadeRange:
        case iCarouselOptionOffsetMultiplier:
        case iCarouselOptionVisibleItems:
        {
            return value;
        }
    }
}





#pragma mark -
#pragma mark iCarousel taps




- (void)carousel:(__unused iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    NSNumber *item = (self.items)[(NSUInteger)index];
    NSLog(@"Tapped view number: %@", item);
}

- (void)carouselCurrentItemIndexDidChange:(__unused iCarousel *)carousel
{
   // NSLog(@"Index: %@", @(self.carousel.currentItemIndex));
}


#pragma mark -
#pragma mark swipe method methods

- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView
{
    //return the total number of items in the carousel
     return (NSInteger)[self.items count];
}


- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    UILabel *label = nil;
    UILabel *label1 = nil;
    UILabel *label2 = nil;
    UIImageView *unitimage;
    UIImageView *boundimage;
    //create new view if no view is available for recycling
   // if (view == nil)
    //{
        
        view = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, 60.0f, 100.0f)];
        //((UIImageView *)view).image = [UIImage imageNamed:@"page.png"];
        view.contentMode = UIViewContentModeLeft;
        label =[[UILabel alloc] init];
        [label setFrame:CGRectMake(8,2,50,20)];
        //  label = [[UILabel alloc] initWithFrame:CGRectMake(0,0,20,20)];
        // label = [[UILabel alloc] initWithFrame:view.bounds];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [label.font fontWithSize:10];
        label.textColor=[UIColor whiteColor];
        label.tag = 1;
        
        label1 =[[UILabel alloc] init];
        [label1 setFrame:CGRectMake(8,15,55,30)];
        label1.backgroundColor = [UIColor clearColor];
        label1.textAlignment = NSTextAlignmentCenter;
        label1.font = [label.font fontWithSize:20];
        label1.textColor=[UIColor whiteColor];
        label1.tag = 1;
        
        label2 =[[UILabel alloc] init];
        [label2 setFrame:CGRectMake(18,35,50,20)];
        label2.backgroundColor = [UIColor clearColor];
        label2.textAlignment = NSTextAlignmentCenter;
        label2.font = [label.font fontWithSize:10];
        label2.textColor=[UIColor whiteColor];
        label2.tag = 1;
        
        unitimage=[[UIImageView alloc]init];
        [unitimage setFrame:CGRectMake(17,40,10,10)];
        
        boundimage=[[UIImageView alloc]init];
        [boundimage setFrame:CGRectMake(0,0,2,80)];
        
        
        [view addSubview:label];
        [view addSubview:label1];
        [view addSubview:label2];
        [view addSubview:unitimage];
        [view addSubview:boundimage];
        
        
        
   // }
   // else
    //{
        //get a reference to the label in the recycled view
       // label = (UILabel *)[view viewWithTag:1];
   // }
    
    //set item label
    //remember to always set any properties of your carousel item
    //views outside of the `if (view == nil) {...}` check otherwise
    //you'll get weird issues with carousel item content appearing
    //in the wrong place in the carousel
    label.text =  self.items[(NSUInteger)index];//[self.items[(NSUInteger)index] stringValue];
    label1.text =  self.itemsdata[(NSUInteger)index];
    label2.text =  self.itemsunit[(NSUInteger)index];
    [unitimage setImage:[UIImage imageNamed:[self.itemsimage objectAtIndex:index]]];
    if(index!=0)
        boundimage.image=[UIImage imageNamed:@"vertical_blue_line.png"];
    return view;
}

- (CGSize)swipeViewItemSize:(SwipeView *)swipeView
{
    return CGSizeMake(60, 50);
}


@end
