//
//  DoctorSettingTableViewCell.h
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 24/06/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomUIPickerView.h"

@interface DoctorSettingTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel * doctorValueLabel;
@property (nonatomic, strong) IBOutlet UILabel *yellowValueLabel;
@property (nonatomic, strong) IBOutlet UILabel *redValueLabel;
@property (nonatomic, strong) IBOutlet UILabel *redSingleValueLabel;



@property (nonatomic, strong) IBOutlet UIView* doctorAndPersonalMainView;
//@IBOutlet weak var personalMainView: UIView!

@property (nonatomic, strong) IBOutlet UILabel* subEventNameLabel;
@property (nonatomic, strong) IBOutlet UILabel* subEventMeasureLabel;
@property (nonatomic, strong) IBOutlet UIButton* YellowEventOnOff;
@property (nonatomic, strong) IBOutlet UIButton* RedEventOnOff;
@property (nonatomic, strong) IBOutlet CustomUIPickerView *subCatValuePickerView;
@property (nonatomic, strong) IBOutlet UIImageView *rightBlackArrow;

@end
