//
//  ClinicalRecordsViewController.m
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 22/06/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import "ClinicalRecordsViewController.h"
#import "ViewRecordedEcgViewController.h"


@interface ClinicalRecordsViewController ()
{
    UIImageOrientation scrollOrientation;
    CGPoint lastPos;

}


@end

@implementation ClinicalRecordsViewController
@synthesize tabHeaderMainView,tabDivider1,tabDivider2,tableClinicalRecords,tableViewCellCustom,allTabButton,detectedTabButton,manualTabButton,selectedTabIs,numberofevents,maximumevent,minimumevents,averagevents;

- (void)viewDidLoad {
    [super viewDidLoad];
    AppDelegate * appDelegate = (AppDelegate *)UIApplication.sharedApplication.delegate ;
    appDelegate.orientationKey = @"PRO";
    [appDelegate forceOrientationTakePlace:appDelegate.orientationKey];

    selectedIndex =-1;
    self.slidingViewController.topViewAnchoredGesture = ECSlidingViewControllerAnchoredGestureTapping | ECSlidingViewControllerAnchoredGesturePanning;
    self.slidingViewController.customAnchoredGestures =@[];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(EventOccured:) name:RWT_EVENT_STATUS_NOTIFICATION object:nil];
    
    [self initalizeElements];
}
-(void)viewWillAppear:(BOOL)animated
{
     [super viewWillAppear:animated];
    [self switchTab:allTabButton];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSLogger *logger=[[NSLogger alloc]init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"MCDSettings" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
        logger.degbugger = true;
        [logger log:@"Coredata Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:[error localizedDescription], @"Adding clinical record", nil] error:TRUE];
        
    } else {
        
        if (result.count > 0) {
            NSManagedObject *mcdsett = (NSManagedObject *)[result objectAtIndex:0];
            
            Respiration=[[mcdsett valueForKey:@"respiration"]intValue];
        }
    }
    
}


- (void)EventOccured:(NSNotification *)notification {
    // Connection status changed. Indicate on GUI.
    BOOL isConnected = [(NSNumber *) (notification.userInfo)[@"EventOccured"] boolValue];
    
    
  
    dispatch_async(dispatch_get_main_queue(), ^{
        // Set image based on connection status
        
        
        if (isConnected) {
           
            [self reloadtabledata];
        }
        
        
    });
}


-(void)reloadtabledata
{
    [self viewDidLoad];
     // [self initalizeElements];
    [self switchTab:allTabButton];
    
    
}

-(void) setDummyData
{
    //records hardcode for Manuals
    
    
    
    // 15 Mar
  /*  NSDictionary *rec1   = @{@"type" :@"manual", @"time" : @"12:30 PM",@"blood_pressure" : @"20",@"weight" : @"65", @"glucose" :@"35",@"event_name" : @"Manual Measure"};
    
    NSDictionary*rec11    = @{@"type" :@"detected",@"time" :@"11:30 PM",@"posture"  :@"standing",@"hr" :@"20",@"temp" :@"65",@"resp" :@"35",@"rr" :@"20", @"qrs" :@"43",@"qt" :@"23",@"qtc" :@"32",@"pr" :@"42",@"resp_var" :@"55",@"event_name" :@"Bradycardia"};
    
    
    // 16 Mar
    NSDictionary*rec2    = @{@"type" :@"manual",@"time" :@"1:00 PM",@"blood_pressure" :@"22",@"weight" :@"66",@"glucose" :@"24",@"event_name" :@"Manual Measure"};
    NSDictionary*rec3    = @{@"type" :@"manual",@"time" :@"2:30 PM",@"blood_pressure" :@"23",@"weight" :@"45",@"glucose" :@"16",@"event_name" :@"Manual Measure"};
    
    NSDictionary*rec12    = @{@"type" :@"detected",@"time" :@"11:30 PM",@"posture"  :@"standing",@"hr" :@"21",@"temp" :@"65",@"resp" :@"35",@"rr" :@"20", @"qrs" :@"43",@"qt" :@"23",@"qtc" :@"32",@"pr" :@"42",@"resp_var" :@"55",@"event_name" :@"Bradycardia"};
    
    
    
    //17 Mar
    NSDictionary*rec4     = @{@"type" :@"manual",@"time" : @"4:30 PM",     @"blood_pressure" : @"24", @"weight" : @"80", @"glucose" : @"25", @"event_name" : @"Manual Measure"};
    
    NSDictionary*rec13    = @{@"type" : @"detected", @"time" : @"11:30 PM",    @"posture"  :   @"sleeping", @"hr" : @"22",    @"temp" : @"65", @"resp" : @"35", @"rr" : @"20", @"qrs" : @"43", @"qt" : @"23", @"qtc" : @"32", @"pr" : @"42", @"resp_var" : @"55", @"event_name" : @"Tachycardia"};
    
    NSDictionary*rec14    = @{@"type" : @"detected", @"time" : @"11:30 PM",    @"posture"  :   @"running", @"hr" : @"23",    @"temp" : @"65", @"resp" : @"35", @"rr" : @"20", @"qrs" : @"43", @"qt" : @"23", @"qtc" : @"32", @"pr" : @"42", @"resp_var" : @"55", @"event_name" : @"Tachycardia"};
    
    NSDictionary*rec15    = @{@"type" : @"detected", @"time" : @"11:30 PM",    @"posture"  :   @"sitting", @"hr" : @"24",    @"temp" : @"65", @"resp" : @"35", @"rr" : @"20", @"qrs" : @"43", @"qt" : @"23", @"qtc" : @"32", @"pr" : @"42", @"resp_var" : @"55", @"event_name" : @"Bradycardia"};
    
    NSDictionary*rec16    = @{@"type" : @"detected", @"time" : @"11:30 PM",    @"posture"  :   @"standing", @"hr" : @"25",    @"temp" : @"65", @"resp" : @"35", @"rr" : @"20", @"qrs" : @"43", @"qt" : @"23", @"qtc" : @"32", @"pr" : @"42", @"resp_var" : @"55", @"event_name" : @"Bradycardia"};*/
    
    
    
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"EventStorage" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSString *ecodevalue;
    
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
      NSDictionary *rec1,*rec2,*rec3,*rec4,*rec5,*rec6,*rec7,*rec8,*rec9,*rec10,*rec11,*rec12,*rec13,*rec14,*rec15,*rec16,*rec17,*rec18,*rec19,*rec20,*rec21,*rec22,*rec23,*rec24,*rec25,*rec26;
      NSMutableArray * tempDataArray =[[NSMutableArray alloc]init];
      NSMutableArray *data_all   = [[NSMutableArray alloc]init];
      NSMutableArray *data_manual   = [[NSMutableArray alloc]init];
     NSMutableArray *data_detected   = [[NSMutableArray alloc]init];
   
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
        NSLogger *logger=[[NSLogger alloc]init];
        logger.degbugger = true;
        
        [logger log:@"Coredata Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:[error localizedDescription], @"History Clinical Record", nil] error:TRUE];
        
        
    } else {
        //NSLog(@"%@", result);
        numberofevents.text=[NSString stringWithFormat:@"%lu",(unsigned long)result.count] ;
        if (result.count > 0) {
            
            for(int j=0;j<result.count;j++)
            {
                NSManagedObject *eventsett = (NSManagedObject *)[result objectAtIndex:j];
               // NSLog(@"1 - %@", eventsett);
                ecodevalue=[[eventsett valueForKey:@"event_code"]stringValue];
                
              
                
                if([ecodevalue isEqualToString:@"2040"])
                {
                    NSString *bloodpressure,*weight,*glucose,*oximetry;
                   if([[eventsett valueForKey:@"event_bpsys"] length]==0)
                   {
                       bloodpressure=@"-";
                   }
                    else
                   bloodpressure=[NSString stringWithFormat:@"%@/%@", [eventsett valueForKey:@"event_bpsys"],[eventsett valueForKey:@"event_bpdia"]];
                    
                    if([[eventsett valueForKey:@"event_weight"] length]==0)
                    {
                        weight=@"-";
                    }
                    else
                        weight=[eventsett valueForKey:@"event_weight"];
                    
                    if([[eventsett valueForKey:@"event_glucose"] length]==0)
                    {
                        glucose=@"-";
                    }
                    else
                        glucose=[eventsett valueForKey:@"event_glucose"];
                    
                    if([[eventsett valueForKey:@"event_oximetry"] length]==0)
                    {
                        oximetry=@"-";
                    }
                    else
                        oximetry=[eventsett valueForKey:@"event_oximetry"];
                    
                     rec1   = @{@"type" :@"manual", @"time" :[eventsett valueForKey:@"event_datetime"],@"blood_pressure" :bloodpressure,@"weight" :weight, @"glucose" :glucose,@"oximetry" :oximetry,@"event_name" : @"Manual Measure",@"ecg_path" : [eventsett valueForKey:@"event_ecgfilepath"],@"event_type":[eventsett valueForKey:@"event_type"],@"event_receive_complete":[eventsett valueForKey:@"event_receive_complete"],@"event_respiration_onoff":[eventsett valueForKey:@"event_respiration_onoff"],@"event_code":[[eventsett valueForKey:@"event_code"]stringValue]};
                    
                   
                    [data_all addObject:rec1];
                   
                    [data_manual addObject:rec1];

                  
                    
                }
                
                if([ecodevalue isEqualToString:@"2034"])//PATIENT CALL
                {
                    rec24    = @{@"type" : @"detected", @"time" :[eventsett valueForKey:@"event_datetime"],    @"posture"  :[eventsett valueForKey:@"event_posture_code"], @"hr" : [eventsett valueForKey:@"event_heartrate"],    @"temp" : [eventsett valueForKey:@"event_temp"], @"resp" : [eventsett valueForKey:@"event_respiration"], @"rr" :[eventsett valueForKey:@"event_rr_interval"], @"qrs" :[eventsett valueForKey:@"event_qrs_interval"], @"qt" : [eventsett valueForKey:@"event_qt_interval"], @"qtc" :[eventsett valueForKey:@"event_qtc_interval"], @"pr" : [eventsett valueForKey:@"event_pr_interval"], @"resp_var" : [eventsett valueForKey:@"event_resp_variance"], @"event_name" :[eventsett valueForKey:@"event_reason"],@"ecg_path" : [eventsett valueForKey:@"event_ecgfilepath"],@"event_type":[eventsett valueForKey:@"event_type"],@"event_code":[[eventsett valueForKey:@"event_code"]stringValue],@"event_value":[eventsett valueForKey:@"event_value"],@"event_receive_complete":[eventsett valueForKey:@"event_receive_complete"],@"event_respiration_onoff":[eventsett valueForKey:@"event_respiration_onoff"]};
                    [data_all addObject:rec24];
                    [data_manual addObject:rec24];
                }
                
                if([ecodevalue isEqualToString:@"2043"])//ondemand
                {
                    rec25    = @{@"type" : @"detected", @"time" :[eventsett valueForKey:@"event_datetime"],    @"posture"  :[eventsett valueForKey:@"event_posture_code"], @"hr" : [eventsett valueForKey:@"event_heartrate"],    @"temp" : [eventsett valueForKey:@"event_temp"], @"resp" : [eventsett valueForKey:@"event_respiration"], @"rr" :[eventsett valueForKey:@"event_rr_interval"], @"qrs" :[eventsett valueForKey:@"event_qrs_interval"], @"qt" : [eventsett valueForKey:@"event_qt_interval"], @"qtc" :[eventsett valueForKey:@"event_qtc_interval"], @"pr" : [eventsett valueForKey:@"event_pr_interval"], @"resp_var" : [eventsett valueForKey:@"event_resp_variance"], @"event_name" :[eventsett valueForKey:@"event_reason"],@"ecg_path" : [eventsett valueForKey:@"event_ecgfilepath"],@"event_type":[eventsett valueForKey:@"event_type"],@"event_code":[[eventsett valueForKey:@"event_code"]stringValue],@"event_value":[eventsett valueForKey:@"event_value"],@"event_receive_complete":[eventsett valueForKey:@"event_receive_complete"],@"event_respiration_onoff":[eventsett valueForKey:@"event_respiration_onoff"]};
                    [data_all addObject:rec25];
                    [data_manual addObject:rec25];
                }
                //arrythmia
                if([ecodevalue isEqualToString:@"2007"])
                {
                    rec2   = @{@"type" : @"detected", @"time" :[eventsett valueForKey:@"event_datetime"],    @"posture"  :[eventsett valueForKey:@"event_posture_code"], @"hr" : [eventsett valueForKey:@"event_heartrate"],    @"temp" : [eventsett valueForKey:@"event_temp"], @"resp" : [eventsett valueForKey:@"event_respiration"], @"rr" :[eventsett valueForKey:@"event_rr_interval"], @"qrs" :[eventsett valueForKey:@"event_qrs_interval"], @"qt" : [eventsett valueForKey:@"event_qt_interval"], @"qtc" :[eventsett valueForKey:@"event_qtc_interval"], @"pr" : [eventsett valueForKey:@"event_pr_interval"], @"resp_var" : [eventsett valueForKey:@"event_resp_variance"], @"event_name" :[eventsett valueForKey:@"event_reason"],@"ecg_path" : [eventsett valueForKey:@"event_ecgfilepath"],@"event_type":[eventsett valueForKey:@"event_type"],@"event_code":[[eventsett valueForKey:@"event_code"]stringValue],@"event_value":[eventsett valueForKey:@"event_value"],@"event_receive_complete":[eventsett valueForKey:@"event_receive_complete"],@"event_respiration_onoff":[eventsett valueForKey:@"event_respiration_onoff"]};
                    [data_all addObject:rec2];
                    [data_detected addObject:rec2];
                }
                if([ecodevalue isEqualToString:@"2011"])
                {
                    rec3   = @{@"type" : @"detected", @"time" :[eventsett valueForKey:@"event_datetime"],    @"posture"  :[eventsett valueForKey:@"event_posture_code"], @"hr" : [eventsett valueForKey:@"event_heartrate"],    @"temp" : [eventsett valueForKey:@"event_temp"], @"resp" : [eventsett valueForKey:@"event_respiration"], @"rr" :[eventsett valueForKey:@"event_rr_interval"], @"qrs" :[eventsett valueForKey:@"event_qrs_interval"], @"qt" : [eventsett valueForKey:@"event_qt_interval"], @"qtc" :[eventsett valueForKey:@"event_qtc_interval"], @"pr" : [eventsett valueForKey:@"event_pr_interval"], @"resp_var" : [eventsett valueForKey:@"event_resp_variance"], @"event_name" :[eventsett valueForKey:@"event_reason"],@"ecg_path" : [eventsett valueForKey:@"event_ecgfilepath"],@"event_type":[eventsett valueForKey:@"event_type"],@"event_code":[[eventsett valueForKey:@"event_code"]stringValue],@"event_value":[eventsett valueForKey:@"event_value"],@"event_receive_complete":[eventsett valueForKey:@"event_receive_complete"],@"event_respiration_onoff":[eventsett valueForKey:@"event_respiration_onoff"]};
                    [data_all addObject:rec3];
                    [data_detected addObject:rec3];
                }
                if([ecodevalue isEqualToString:@"2044"])
                {
                    rec4   = @{@"type" : @"detected", @"time" :[eventsett valueForKey:@"event_datetime"],    @"posture"  :[eventsett valueForKey:@"event_posture_code"], @"hr" : [eventsett valueForKey:@"event_heartrate"],    @"temp" : [eventsett valueForKey:@"event_temp"], @"resp" : [eventsett valueForKey:@"event_respiration"], @"rr" :[eventsett valueForKey:@"event_rr_interval"], @"qrs" :[eventsett valueForKey:@"event_qrs_interval"], @"qt" : [eventsett valueForKey:@"event_qt_interval"], @"qtc" :[eventsett valueForKey:@"event_qtc_interval"], @"pr" : [eventsett valueForKey:@"event_pr_interval"], @"resp_var" : [eventsett valueForKey:@"event_resp_variance"], @"event_name" :[eventsett valueForKey:@"event_reason"],@"ecg_path" : [eventsett valueForKey:@"event_ecgfilepath"],@"event_type":[eventsett valueForKey:@"event_type"],@"event_code":[[eventsett valueForKey:@"event_code"]stringValue],@"event_value":[eventsett valueForKey:@"event_value"],@"event_receive_complete":[eventsett valueForKey:@"event_receive_complete"],@"event_respiration_onoff":[eventsett valueForKey:@"event_respiration_onoff"]};
                    [data_all addObject:rec4];
                    [data_detected addObject:rec4];
                }
                if([ecodevalue isEqualToString:@"2017"])
                {
                    rec5    = @{@"type" : @"detected", @"time" :[eventsett valueForKey:@"event_datetime"],    @"posture"  :[eventsett valueForKey:@"event_posture_code"], @"hr" : [eventsett valueForKey:@"event_heartrate"],    @"temp" : [eventsett valueForKey:@"event_temp"], @"resp" : [eventsett valueForKey:@"event_respiration"], @"rr" :[eventsett valueForKey:@"event_rr_interval"], @"qrs" :[eventsett valueForKey:@"event_qrs_interval"], @"qt" : [eventsett valueForKey:@"event_qt_interval"], @"qtc" :[eventsett valueForKey:@"event_qtc_interval"], @"pr" : [eventsett valueForKey:@"event_pr_interval"], @"resp_var" : [eventsett valueForKey:@"event_resp_variance"], @"event_name" :[eventsett valueForKey:@"event_reason"],@"ecg_path" : [eventsett valueForKey:@"event_ecgfilepath"],@"event_type":[eventsett valueForKey:@"event_type"],@"event_code":[[eventsett valueForKey:@"event_code"]stringValue],@"event_value":[eventsett valueForKey:@"event_value"],@"event_receive_complete":[eventsett valueForKey:@"event_receive_complete"],@"event_respiration_onoff":[eventsett valueForKey:@"event_respiration_onoff"]};
                    [data_all addObject:rec5];
                    [data_detected addObject:rec5];
                }
                if([ecodevalue isEqualToString:@"2026"])
                {
                    rec6    = @{@"type" : @"detected", @"time" :[eventsett valueForKey:@"event_datetime"],    @"posture"  :[eventsett valueForKey:@"event_posture_code"], @"hr" : [eventsett valueForKey:@"event_heartrate"],    @"temp" : [eventsett valueForKey:@"event_temp"], @"resp" : [eventsett valueForKey:@"event_respiration"], @"rr" :[eventsett valueForKey:@"event_rr_interval"], @"qrs" :[eventsett valueForKey:@"event_qrs_interval"], @"qt" : [eventsett valueForKey:@"event_qt_interval"], @"qtc" :[eventsett valueForKey:@"event_qtc_interval"], @"pr" : [eventsett valueForKey:@"event_pr_interval"], @"resp_var" : [eventsett valueForKey:@"event_resp_variance"], @"event_name" :[eventsett valueForKey:@"event_reason"],@"ecg_path" : [eventsett valueForKey:@"event_ecgfilepath"],@"event_type":[eventsett valueForKey:@"event_type"],@"event_code":[[eventsett valueForKey:@"event_code"]stringValue],@"event_value":[eventsett valueForKey:@"event_value"],@"event_receive_complete":[eventsett valueForKey:@"event_receive_complete"],@"event_respiration_onoff":[eventsett valueForKey:@"event_respiration_onoff"]};
                    [data_all addObject:rec6];
                    [data_detected addObject:rec6];
                }
                if([ecodevalue isEqualToString:@"2015"])
                {
                    rec7    = @{@"type" : @"detected", @"time" :[eventsett valueForKey:@"event_datetime"],    @"posture"  :[eventsett valueForKey:@"event_posture_code"], @"hr" : [eventsett valueForKey:@"event_heartrate"],    @"temp" : [eventsett valueForKey:@"event_temp"], @"resp" : [eventsett valueForKey:@"event_respiration"], @"rr" :[eventsett valueForKey:@"event_rr_interval"], @"qrs" :[eventsett valueForKey:@"event_qrs_interval"], @"qt" : [eventsett valueForKey:@"event_qt_interval"], @"qtc" :[eventsett valueForKey:@"event_qtc_interval"], @"pr" : [eventsett valueForKey:@"event_pr_interval"], @"resp_var" : [eventsett valueForKey:@"event_resp_variance"], @"event_name" :[eventsett valueForKey:@"event_reason"],@"ecg_path" : [eventsett valueForKey:@"event_ecgfilepath"],@"event_type":[eventsett valueForKey:@"event_type"],@"event_code":[[eventsett valueForKey:@"event_code"]stringValue],@"event_value":[eventsett valueForKey:@"event_value"],@"event_receive_complete":[eventsett valueForKey:@"event_receive_complete"],@"event_respiration_onoff":[eventsett valueForKey:@"event_respiration_onoff"]};
                    [data_all addObject:rec7];
                    [data_detected addObject:rec7];
                }
                if([ecodevalue isEqualToString:@"2016"])
                {
                    rec8    = @{@"type" : @"detected", @"time" :[eventsett valueForKey:@"event_datetime"],    @"posture"  :[eventsett valueForKey:@"event_posture_code"], @"hr" : [eventsett valueForKey:@"event_heartrate"],    @"temp" : [eventsett valueForKey:@"event_temp"], @"resp" : [eventsett valueForKey:@"event_respiration"], @"rr" :[eventsett valueForKey:@"event_rr_interval"], @"qrs" :[eventsett valueForKey:@"event_qrs_interval"], @"qt" : [eventsett valueForKey:@"event_qt_interval"], @"qtc" :[eventsett valueForKey:@"event_qtc_interval"], @"pr" : [eventsett valueForKey:@"event_pr_interval"], @"resp_var" : [eventsett valueForKey:@"event_resp_variance"], @"event_name" :[eventsett valueForKey:@"event_reason"],@"ecg_path" : [eventsett valueForKey:@"event_ecgfilepath"],@"event_type":[eventsett valueForKey:@"event_type"],@"event_code":[[eventsett valueForKey:@"event_code"]stringValue],@"event_value":[eventsett valueForKey:@"event_value"],@"event_receive_complete":[eventsett valueForKey:@"event_receive_complete"],@"event_respiration_onoff":[eventsett valueForKey:@"event_respiration_onoff"]};
                    [data_all addObject:rec8];
                    [data_detected addObject:rec8];
                }
                if([ecodevalue isEqualToString:@"2032"])
                {
                    rec9    = @{@"type" : @"detected", @"time" :[eventsett valueForKey:@"event_datetime"],    @"posture"  :[eventsett valueForKey:@"event_posture_code"], @"hr" : [eventsett valueForKey:@"event_heartrate"],    @"temp" : [eventsett valueForKey:@"event_temp"], @"resp" : [eventsett valueForKey:@"event_respiration"], @"rr" :[eventsett valueForKey:@"event_rr_interval"], @"qrs" :[eventsett valueForKey:@"event_qrs_interval"], @"qt" : [eventsett valueForKey:@"event_qt_interval"], @"qtc" :[eventsett valueForKey:@"event_qtc_interval"], @"pr" : [eventsett valueForKey:@"event_pr_interval"], @"resp_var" : [eventsett valueForKey:@"event_resp_variance"], @"event_name" :[eventsett valueForKey:@"event_reason"],@"ecg_path" : [eventsett valueForKey:@"event_ecgfilepath"],@"event_type":[eventsett valueForKey:@"event_type"],@"event_code":[[eventsett valueForKey:@"event_code"]stringValue],@"event_value":[eventsett valueForKey:@"event_value"],@"event_receive_complete":[eventsett valueForKey:@"event_receive_complete"],@"event_respiration_onoff":[eventsett valueForKey:@"event_respiration_onoff"]};
                    [data_all addObject:rec9];
                    [data_detected addObject:rec9];
                }
                if([ecodevalue isEqualToString:@"2029"])
                {
                    rec10    = @{@"type" : @"detected", @"time" :[eventsett valueForKey:@"event_datetime"],    @"posture"  :[eventsett valueForKey:@"event_posture_code"], @"hr" : [eventsett valueForKey:@"event_heartrate"],    @"temp" : [eventsett valueForKey:@"event_temp"], @"resp" : [eventsett valueForKey:@"event_respiration"], @"rr" :[eventsett valueForKey:@"event_rr_interval"], @"qrs" :[eventsett valueForKey:@"event_qrs_interval"], @"qt" : [eventsett valueForKey:@"event_qt_interval"], @"qtc" :[eventsett valueForKey:@"event_qtc_interval"], @"pr" : [eventsett valueForKey:@"event_pr_interval"], @"resp_var" : [eventsett valueForKey:@"event_resp_variance"], @"event_name" :[eventsett valueForKey:@"event_reason"],@"ecg_path" : [eventsett valueForKey:@"event_ecgfilepath"],@"event_type":[eventsett valueForKey:@"event_type"],@"event_code":[[eventsett valueForKey:@"event_code"]stringValue],@"event_value":[eventsett valueForKey:@"event_value"],@"event_receive_complete":[eventsett valueForKey:@"event_receive_complete"],@"event_respiration_onoff":[eventsett valueForKey:@"event_respiration_onoff"]};
                    [data_all addObject:rec10];
                    [data_detected addObject:rec10];
                }
                if([ecodevalue isEqualToString:@"2030"])
                {
                    rec11    = @{@"type" : @"detected", @"time" :[eventsett valueForKey:@"event_datetime"],    @"posture"  :[eventsett valueForKey:@"event_posture_code"], @"hr" : [eventsett valueForKey:@"event_heartrate"],    @"temp" : [eventsett valueForKey:@"event_temp"], @"resp" : [eventsett valueForKey:@"event_respiration"], @"rr" :[eventsett valueForKey:@"event_rr_interval"], @"qrs" :[eventsett valueForKey:@"event_qrs_interval"], @"qt" : [eventsett valueForKey:@"event_qt_interval"], @"qtc" :[eventsett valueForKey:@"event_qtc_interval"], @"pr" : [eventsett valueForKey:@"event_pr_interval"], @"resp_var" : [eventsett valueForKey:@"event_resp_variance"], @"event_name" :[eventsett valueForKey:@"event_reason"],@"ecg_path" : [eventsett valueForKey:@"event_ecgfilepath"],@"event_type":[eventsett valueForKey:@"event_type"],@"event_code":[[eventsett valueForKey:@"event_code"]stringValue],@"event_value":[eventsett valueForKey:@"event_value"],@"event_receive_complete":[eventsett valueForKey:@"event_receive_complete"],@"event_respiration_onoff":[eventsett valueForKey:@"event_respiration_onoff"]};
                    [data_all addObject:rec11];
                    [data_detected addObject:rec11];
                }
                if([ecodevalue isEqualToString:@"2031"])
                {
                    rec12    = @{@"type" : @"detected", @"time" :[eventsett valueForKey:@"event_datetime"],    @"posture"  :[eventsett valueForKey:@"event_posture_code"], @"hr" : [eventsett valueForKey:@"event_heartrate"],    @"temp" : [eventsett valueForKey:@"event_temp"], @"resp" : [eventsett valueForKey:@"event_respiration"], @"rr" :[eventsett valueForKey:@"event_rr_interval"], @"qrs" :[eventsett valueForKey:@"event_qrs_interval"], @"qt" : [eventsett valueForKey:@"event_qt_interval"], @"qtc" :[eventsett valueForKey:@"event_qtc_interval"], @"pr" : [eventsett valueForKey:@"event_pr_interval"], @"resp_var" : [eventsett valueForKey:@"event_resp_variance"], @"event_name" :[eventsett valueForKey:@"event_reason"],@"ecg_path" : [eventsett valueForKey:@"event_ecgfilepath"],@"event_type":[eventsett valueForKey:@"event_type"],@"event_code":[[eventsett valueForKey:@"event_code"]stringValue],@"event_value":[eventsett valueForKey:@"event_value"],@"event_receive_complete":[eventsett valueForKey:@"event_receive_complete"],@"event_respiration_onoff":[eventsett valueForKey:@"event_respiration_onoff"]};
                    [data_all addObject:rec12];
                    [data_detected addObject:rec12];
                }
                
                
                //ischemia
                if([ecodevalue isEqualToString:@"2001"])
                {
                    rec13   = @{@"type" : @"detected", @"time" :[eventsett valueForKey:@"event_datetime"],    @"posture"  :[eventsett valueForKey:@"event_posture_code"], @"hr" : [eventsett valueForKey:@"event_heartrate"],    @"temp" : [eventsett valueForKey:@"event_temp"], @"resp" : [eventsett valueForKey:@"event_respiration"], @"rr" :[eventsett valueForKey:@"event_rr_interval"], @"qrs" :[eventsett valueForKey:@"event_qrs_interval"], @"qt" : [eventsett valueForKey:@"event_qt_interval"], @"qtc" :[eventsett valueForKey:@"event_qtc_interval"], @"pr" : [eventsett valueForKey:@"event_pr_interval"], @"resp_var" : [eventsett valueForKey:@"event_resp_variance"], @"event_name" :[eventsett valueForKey:@"event_reason"],@"ecg_path" : [eventsett valueForKey:@"event_ecgfilepath"],@"event_type":[eventsett valueForKey:@"event_type"],@"event_code":[[eventsett valueForKey:@"event_code"]stringValue],@"event_value":[eventsett valueForKey:@"event_value"],@"event_receive_complete":[eventsett valueForKey:@"event_receive_complete"],@"event_respiration_onoff":[eventsett valueForKey:@"event_respiration_onoff"]};
                    [data_all addObject:rec13];
                    [data_detected addObject:rec13];
                }
                if([ecodevalue isEqualToString:@"2002"])
                {
                    rec14   = @{@"type" : @"detected", @"time" :[eventsett valueForKey:@"event_datetime"],    @"posture"  :[eventsett valueForKey:@"event_posture_code"], @"hr" : [eventsett valueForKey:@"event_heartrate"],    @"temp" : [eventsett valueForKey:@"event_temp"], @"resp" : [eventsett valueForKey:@"event_respiration"], @"rr" :[eventsett valueForKey:@"event_rr_interval"], @"qrs" :[eventsett valueForKey:@"event_qrs_interval"], @"qt" : [eventsett valueForKey:@"event_qt_interval"], @"qtc" :[eventsett valueForKey:@"event_qtc_interval"], @"pr" : [eventsett valueForKey:@"event_pr_interval"], @"resp_var" : [eventsett valueForKey:@"event_resp_variance"], @"event_name" :[eventsett valueForKey:@"event_reason"],@"ecg_path" : [eventsett valueForKey:@"event_ecgfilepath"],@"event_type":[eventsett valueForKey:@"event_type"],@"event_code":[[eventsett valueForKey:@"event_code"]stringValue],@"event_value":[eventsett valueForKey:@"event_value"],@"event_receive_complete":[eventsett valueForKey:@"event_receive_complete"],@"event_respiration_onoff":[eventsett valueForKey:@"event_respiration_onoff"]};
                    [data_all addObject:rec14];
                    [data_detected addObject:rec14];
                }
                if([ecodevalue isEqualToString:@"2003"])
                {
                    rec15   = @{@"type" : @"detected", @"time" :[eventsett valueForKey:@"event_datetime"],    @"posture"  :[eventsett valueForKey:@"event_posture_code"], @"hr" : [eventsett valueForKey:@"event_heartrate"],    @"temp" : [eventsett valueForKey:@"event_temp"], @"resp" : [eventsett valueForKey:@"event_respiration"], @"rr" :[eventsett valueForKey:@"event_rr_interval"], @"qrs" :[eventsett valueForKey:@"event_qrs_interval"], @"qt" : [eventsett valueForKey:@"event_qt_interval"], @"qtc" :[eventsett valueForKey:@"event_qtc_interval"], @"pr" : [eventsett valueForKey:@"event_pr_interval"], @"resp_var" : [eventsett valueForKey:@"event_resp_variance"], @"event_name" :[eventsett valueForKey:@"event_reason"],@"ecg_path" : [eventsett valueForKey:@"event_ecgfilepath"],@"event_type":[eventsett valueForKey:@"event_type"],@"event_code":[[eventsett valueForKey:@"event_code"]stringValue],@"event_value":[eventsett valueForKey:@"event_t_wave"],@"event_receive_complete":[eventsett valueForKey:@"event_receive_complete"],@"event_respiration_onoff":[eventsett valueForKey:@"event_respiration_onoff"]};
                    [data_all addObject:rec15];
                    [data_detected addObject:rec15];
                }
                if([ecodevalue isEqualToString:@"2005"])
                {
                    rec16   = @{@"type" : @"detected", @"time" :[eventsett valueForKey:@"event_datetime"],    @"posture"  :[eventsett valueForKey:@"event_posture_code"], @"hr" : [eventsett valueForKey:@"event_heartrate"],    @"temp" : [eventsett valueForKey:@"event_temp"], @"resp" : [eventsett valueForKey:@"event_respiration"], @"rr" :[eventsett valueForKey:@"event_rr_interval"], @"qrs" :[eventsett valueForKey:@"event_qrs_interval"], @"qt" : [eventsett valueForKey:@"event_qt_interval"], @"qtc" :[eventsett valueForKey:@"event_qtc_interval"], @"pr" : [eventsett valueForKey:@"event_pr_interval"], @"resp_var" : [eventsett valueForKey:@"event_resp_variance"], @"event_name" :[eventsett valueForKey:@"event_reason"],@"ecg_path" : [eventsett valueForKey:@"event_ecgfilepath"],@"event_type":[eventsett valueForKey:@"event_type"],@"event_code":[[eventsett valueForKey:@"event_code"]stringValue],@"event_value":[eventsett valueForKey:@"event_value"],@"event_receive_complete":[eventsett valueForKey:@"event_receive_complete"],@"event_respiration_onoff":[eventsett valueForKey:@"event_respiration_onoff"]};
                    [data_all addObject:rec16];
                    [data_detected addObject:rec16];
                }
                if([ecodevalue isEqualToString:@"2027"])
                {
                    rec26   = @{@"type" : @"detected", @"time" :[eventsett valueForKey:@"event_datetime"],    @"posture"  :[eventsett valueForKey:@"event_posture_code"], @"hr" : [eventsett valueForKey:@"event_heartrate"],    @"temp" : [eventsett valueForKey:@"event_temp"], @"resp" : [eventsett valueForKey:@"event_respiration"], @"rr" :[eventsett valueForKey:@"event_rr_interval"], @"qrs" :[eventsett valueForKey:@"event_qrs_interval"], @"qt" : [eventsett valueForKey:@"event_qt_interval"], @"qtc" :[eventsett valueForKey:@"event_qtc_interval"], @"pr" : [eventsett valueForKey:@"event_pr_interval"], @"resp_var" : [eventsett valueForKey:@"event_resp_variance"], @"event_name" :[eventsett valueForKey:@"event_reason"],@"ecg_path" : [eventsett valueForKey:@"event_ecgfilepath"],@"event_type":[eventsett valueForKey:@"event_type"],@"event_code":[[eventsett valueForKey:@"event_code"]stringValue],@"event_value":[eventsett valueForKey:@"event_value"],@"event_receive_complete":[eventsett valueForKey:@"event_receive_complete"],@"event_respiration_onoff":[eventsett valueForKey:@"event_respiration_onoff"]};
                    [data_all addObject:rec26];
                    [data_detected addObject:rec26];
                }
                if([ecodevalue isEqualToString:@"2006"])
                {
                    rec17   = @{@"type" : @"detected", @"time" :[eventsett valueForKey:@"event_datetime"],    @"posture"  :[eventsett valueForKey:@"event_posture_code"], @"hr" : [eventsett valueForKey:@"event_heartrate"],    @"temp" : [eventsett valueForKey:@"event_temp"], @"resp" : [eventsett valueForKey:@"event_respiration"], @"rr" :[eventsett valueForKey:@"event_rr_interval"], @"qrs" :[eventsett valueForKey:@"event_qrs_interval"], @"qt" : [eventsett valueForKey:@"event_qt_interval"], @"qtc" :[eventsett valueForKey:@"event_qtc_interval"], @"pr" : [eventsett valueForKey:@"event_pr_interval"], @"resp_var" : [eventsett valueForKey:@"event_resp_variance"], @"event_name" :[eventsett valueForKey:@"event_reason"],@"ecg_path" : [eventsett valueForKey:@"event_ecgfilepath"],@"event_type":[eventsett valueForKey:@"event_type"],@"event_code":[[eventsett valueForKey:@"event_code"]stringValue],@"event_value":[eventsett valueForKey:@"event_value"],@"event_receive_complete":[eventsett valueForKey:@"event_receive_complete"],@"event_respiration_onoff":[eventsett valueForKey:@"event_respiration_onoff"]};
                    [data_all addObject:rec17];
                    [data_detected addObject:rec17];
                }
                //other
                if([ecodevalue isEqualToString:@"2022"])
                {
                    rec18    = @{@"type" : @"detected", @"time" :[eventsett valueForKey:@"event_datetime"],    @"posture"  :[eventsett valueForKey:@"event_posture_code"], @"hr" : [eventsett valueForKey:@"event_heartrate"],    @"temp" : [eventsett valueForKey:@"event_temp"], @"resp" : [eventsett valueForKey:@"event_respiration"], @"rr" :[eventsett valueForKey:@"event_rr_interval"], @"qrs" :[eventsett valueForKey:@"event_qrs_interval"], @"qt" : [eventsett valueForKey:@"event_qt_interval"], @"qtc" :[eventsett valueForKey:@"event_qtc_interval"], @"pr" : [eventsett valueForKey:@"event_pr_interval"], @"resp_var" : [eventsett valueForKey:@"event_resp_variance"], @"event_name" :[eventsett valueForKey:@"event_reason"],@"ecg_path" : [eventsett valueForKey:@"event_ecgfilepath"],@"event_type":[eventsett valueForKey:@"event_type"],@"event_code":[[eventsett valueForKey:@"event_code"]stringValue],@"event_value":[eventsett valueForKey:@"event_value"],@"event_receive_complete":[eventsett valueForKey:@"event_receive_complete"],@"event_respiration_onoff":[eventsett valueForKey:@"event_respiration_onoff"]};
                    [data_all addObject:rec18];
                    [data_detected addObject:rec18];
                }
                if([ecodevalue isEqualToString:@"2035"])
                {
                    rec19    = @{@"type" : @"detected", @"time" :[eventsett valueForKey:@"event_datetime"],    @"posture"  :[eventsett valueForKey:@"event_posture_code"], @"hr" : [eventsett valueForKey:@"event_heartrate"],    @"temp" : [eventsett valueForKey:@"event_temp"], @"resp" : [eventsett valueForKey:@"event_respiration"], @"rr" :[eventsett valueForKey:@"event_rr_interval"], @"qrs" :[eventsett valueForKey:@"event_qrs_interval"], @"qt" : [eventsett valueForKey:@"event_qt_interval"], @"qtc" :[eventsett valueForKey:@"event_qtc_interval"], @"pr" : [eventsett valueForKey:@"event_pr_interval"], @"resp_var" : [eventsett valueForKey:@"event_resp_variance"], @"event_name" :[eventsett valueForKey:@"event_reason"],@"ecg_path" : [eventsett valueForKey:@"event_ecgfilepath"],@"event_type":[eventsett valueForKey:@"event_type"],@"event_code":[[eventsett valueForKey:@"event_code"]stringValue],@"event_value":[eventsett valueForKey:@"event_value"],@"event_receive_complete":[eventsett valueForKey:@"event_receive_complete"],@"event_respiration_onoff":[eventsett valueForKey:@"event_respiration_onoff"]};
                    [data_all addObject:rec19];
                    [data_detected addObject:rec19];
                }
                if([ecodevalue isEqualToString:@"2036"])
                {
                    rec20    = @{@"type" : @"detected", @"time" :[eventsett valueForKey:@"event_datetime"],    @"posture"  :[eventsett valueForKey:@"event_posture_code"], @"hr" : [eventsett valueForKey:@"event_heartrate"],    @"temp" : [eventsett valueForKey:@"event_temp"], @"resp" : [eventsett valueForKey:@"event_respiration"], @"rr" :[eventsett valueForKey:@"event_rr_interval"], @"qrs" :[eventsett valueForKey:@"event_qrs_interval"], @"qt" : [eventsett valueForKey:@"event_qt_interval"], @"qtc" :[eventsett valueForKey:@"event_qtc_interval"], @"pr" : [eventsett valueForKey:@"event_pr_interval"], @"resp_var" : [eventsett valueForKey:@"event_resp_variance"], @"event_name" :[eventsett valueForKey:@"event_reason"],@"ecg_path" : [eventsett valueForKey:@"event_ecgfilepath"],@"event_type":[eventsett valueForKey:@"event_type"],@"event_code":[[eventsett valueForKey:@"event_code"]stringValue],@"event_value":[eventsett valueForKey:@"event_value"],@"event_receive_complete":[eventsett valueForKey:@"event_receive_complete"],@"event_respiration_onoff":[eventsett valueForKey:@"event_respiration_onoff"]};
                    [data_all addObject:rec20];
                    [data_detected addObject:rec20];
                }
                if([ecodevalue isEqualToString:@"2021"])
                {
                    rec21    = @{@"type" : @"detected", @"time" :[eventsett valueForKey:@"event_datetime"],    @"posture"  :[eventsett valueForKey:@"event_posture_code"], @"hr" : [eventsett valueForKey:@"event_heartrate"],    @"temp" : [eventsett valueForKey:@"event_temp"], @"resp" : [eventsett valueForKey:@"event_respiration"], @"rr" :[eventsett valueForKey:@"event_rr_interval"], @"qrs" :[eventsett valueForKey:@"event_qrs_interval"], @"qt" : [eventsett valueForKey:@"event_qt_interval"], @"qtc" :[eventsett valueForKey:@"event_qtc_interval"], @"pr" : [eventsett valueForKey:@"event_pr_interval"], @"resp_var" : [eventsett valueForKey:@"event_resp_variance"], @"event_name" :[eventsett valueForKey:@"event_reason"],@"ecg_path" : [eventsett valueForKey:@"event_ecgfilepath"],@"event_type":[eventsett valueForKey:@"event_type"],@"event_code":[[eventsett valueForKey:@"event_code"]stringValue],@"event_value":[eventsett valueForKey:@"event_value"],@"event_receive_complete":[eventsett valueForKey:@"event_receive_complete"],@"event_respiration_onoff":[eventsett valueForKey:@"event_respiration_onoff"]};
                    [data_all addObject:rec21];
                    [data_detected addObject:rec21];
                }
                if([ecodevalue isEqualToString:@"2024"])
                {
                    rec22    = @{@"type" : @"detected", @"time" :[eventsett valueForKey:@"event_datetime"],    @"posture"  :[eventsett valueForKey:@"event_posture_code"], @"hr" : [eventsett valueForKey:@"event_heartrate"],    @"temp" : [eventsett valueForKey:@"event_temp"], @"resp" : [eventsett valueForKey:@"event_respiration"], @"rr" :[eventsett valueForKey:@"event_rr_interval"], @"qrs" :[eventsett valueForKey:@"event_qrs_interval"], @"qt" : [eventsett valueForKey:@"event_qt_interval"], @"qtc" :[eventsett valueForKey:@"event_qtc_interval"], @"pr" : [eventsett valueForKey:@"event_pr_interval"], @"resp_var" : [eventsett valueForKey:@"event_resp_variance"], @"event_name" :[eventsett valueForKey:@"event_reason"],@"ecg_path" : [eventsett valueForKey:@"event_ecgfilepath"],@"event_type":[eventsett valueForKey:@"event_type"],@"event_code":[[eventsett valueForKey:@"event_code"]stringValue],@"event_value":[eventsett valueForKey:@"event_value"],@"event_receive_complete":[eventsett valueForKey:@"event_receive_complete"],@"event_respiration_onoff":[eventsett valueForKey:@"event_respiration_onoff"]};
                    [data_all addObject:rec22];
                    [data_detected addObject:rec22];
                }
                if([ecodevalue isEqualToString:@"2023"])
                {
                    rec23 = @{@"type" : @"detected", @"time" :[eventsett valueForKey:@"event_datetime"],    @"posture"  :[eventsett valueForKey:@"event_posture_code"], @"hr" : [eventsett valueForKey:@"event_heartrate"],    @"temp" : [eventsett valueForKey:@"event_temp"], @"resp" : [eventsett valueForKey:@"event_respiration"], @"rr" :[eventsett valueForKey:@"event_rr_interval"], @"qrs" :[eventsett valueForKey:@"event_qrs_interval"], @"qt" : [eventsett valueForKey:@"event_qt_interval"], @"qtc" :[eventsett valueForKey:@"event_qtc_interval"], @"pr" : [eventsett valueForKey:@"event_pr_interval"], @"resp_var" : [eventsett valueForKey:@"event_resp_variance"], @"event_name" :[eventsett valueForKey:@"event_reason"],@"ecg_path" : [eventsett valueForKey:@"event_ecgfilepath"],@"event_type":[eventsett valueForKey:@"event_type"],@"event_code":[[eventsett valueForKey:@"event_code"]stringValue],@"event_value":[eventsett valueForKey:@"event_value"],@"event_receive_complete":[eventsett valueForKey:@"event_receive_complete"],@"event_respiration_onoff":[eventsett valueForKey:@"event_respiration_onoff"]};
                    
                    
                    [data_all addObject:rec23];
                    [data_detected addObject:rec23];
                }
                
                
               
               
                
             /*   if(selectedTabIs ==ALL)
                {
                    
                    // ALL TAB Data Generation
                    NSMutableArray *data_15_march   = [[NSMutableArray alloc]initWithObjects:rec1,rec4, nil];
                    NSMutableArray *data_16_march   = [[NSMutableArray alloc]initWithObjects:rec1,rec4, nil];
                    NSMutableArray *data_17_march   =[[NSMutableArray alloc]initWithObjects:rec1,rec4, nil];
                    
                    NSMutableDictionary *data_15_march_dic = [[NSMutableDictionary alloc] init];
                    [data_15_march_dic setObject:@"15th March 2014" forKey:@"date"];
                    [data_15_march_dic setObject:data_15_march forKey:@"data"];
                    
                    NSMutableDictionary *data_16_march_dic = [[NSMutableDictionary alloc] init];
                    [data_16_march_dic setObject:@"16th March 2014" forKey:@"date"];
                    [data_16_march_dic setObject:data_16_march forKey:@"data"];
                    
                    NSMutableDictionary *data_17_march_dic = [[NSMutableDictionary alloc] init];
                    [data_17_march_dic setObject:@"17th March 2014" forKey:@"date"];
                    [data_17_march_dic setObject:data_17_march forKey:@"data"];
                    
                    [tempDataArray addObject:data_15_march_dic];
                    [tempDataArray addObject:data_16_march_dic];
                    [tempDataArray addObject:data_17_march_dic];
                }
                else if(selectedTabIs == DETECTED)
                {
                    
                    // DETECTED TAB Data Genration
                    
                    NSMutableArray *data_15_march   = [[NSMutableArray alloc]initWithObjects:rec1, nil];
                    NSMutableArray *data_16_march = [[NSMutableArray alloc]initWithObjects:rec1, nil];
                    NSMutableArray * data_17_march =[[NSMutableArray alloc]initWithObjects:rec1, nil];
                    
                    
                    NSMutableDictionary *data_15_march_dic = [[NSMutableDictionary alloc] init];
                    [data_15_march_dic setObject:@"15th March 2014" forKey:@"date"];
                    [data_15_march_dic setObject:data_15_march forKey:@"data"];
                    
                    NSMutableDictionary *data_16_march_dic = [[NSMutableDictionary alloc] init];
                    [data_16_march_dic setObject:@"16th March 2014" forKey:@"date"];
                    [data_16_march_dic setObject:data_16_march forKey:@"data"];
                    
                    NSMutableDictionary *data_17_march_dic = [[NSMutableDictionary alloc] init];
                    [data_17_march_dic setObject:@"17th March 2014" forKey:@"date"];
                    [data_17_march_dic setObject:data_17_march forKey:@"data"];
                    
                    [tempDataArray addObject:data_15_march_dic];
                    [tempDataArray addObject:data_16_march_dic];
                    [tempDataArray addObject:data_17_march_dic];
                    
                    
                }
                else if(selectedTabIs == MANUAL)
                {
                    
                    // MANUAL TAB Data Genration
                    NSMutableArray *data_15_march   = [[NSMutableArray alloc]initWithObjects:rec1, nil];
                    NSMutableArray *data_16_march = [[NSMutableArray alloc]initWithObjects:rec1, nil];
                    NSMutableArray *data_17_march =[[NSMutableArray alloc]initWithObjects:rec1, nil];
                    
                    
                    NSMutableDictionary *data_15_march_dic = [[NSMutableDictionary alloc] init];
                    [data_15_march_dic setObject:@"15th March 2014" forKey:@"date"];
                    [data_15_march_dic setObject:data_15_march forKey:@"data"];
                    
                    NSMutableDictionary *data_16_march_dic = [[NSMutableDictionary alloc] init];
                    [data_16_march_dic setObject:@"16th March 2014" forKey:@"date"];
                    [data_16_march_dic setObject:data_16_march forKey:@"data"];
                    
                    NSMutableDictionary *data_17_march_dic = [[NSMutableDictionary alloc] init];
                    [data_17_march_dic setObject:@"17th March 2014" forKey:@"date"];
                    [data_17_march_dic setObject:data_17_march forKey:@"data"];
                    
                    [tempDataArray addObject:data_15_march_dic];
                    [tempDataArray addObject:data_16_march_dic];
                    [tempDataArray addObject:data_17_march_dic];
                    
                }*/
                
               
                
                
            }
        }
    }
  
    
      NSMutableDictionary*  sections = [[NSMutableDictionary alloc]init];
        // Loop through the items and create our keys
    
    if(selectedTabIs ==ALL)
    {
        BOOL found;
        
    for(NSDictionary *dictionary in data_all)
    {
         CommonHelper * commhelp=[[CommonHelper alloc] init];
         NSString *date =[commhelp UTCDateInYourFormat:@"yyyy-MM-dd HH:mm:ss" :@"yyyy-MM-dd HH:mm:ss" :dictionary[@"time"]];
         //NSString *date = dictionary[@"time"];
       
       
        NSString *Eventdttime = [commhelp getDateInYourFormat:@"yyyy-MM-dd HH:mm:ss" :@"yyyy-MM-dd" :DateAsNSString :date :nil];
            found = NO;
            for (NSString *str in [sections allKeys])
            {
             
   
                
                if ([str isEqualToString:Eventdttime])
                {
                    found = YES;
                }
            }
            if (!found)
            {
                [sections setObject:[[NSMutableArray alloc] init] forKey:Eventdttime];
              
            }
        }
        // Loop again and sort the items into their respective keys
        for (NSDictionary *dictionary in data_all)
        {
            CommonHelper * commhelp=[[CommonHelper alloc] init];
            [[sections objectForKey:[commhelp getDateInYourFormat:@"yyyy-MM-dd HH:mm:ss" :@"yyyy-MM-dd" :DateAsNSString :dictionary[@"time"] :nil]]addObject:dictionary];
            
        }
    
       for (NSString *key in [sections allKeys])
        {
                 [[sections objectForKey:key] sortUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"time" ascending:NO]]];
          //  NSLog(@"%@",[sections objectForKey:key]);
            
           
            
        }
        
       
    }
    else if (selectedTabIs == MANUAL1)
    {
        BOOL found;
        for(NSDictionary *dictionary in data_manual)
        {
            
            //NSString *date = dictionary[@"time"];
            CommonHelper * commhelp=[[CommonHelper alloc] init];
              NSString *date =[commhelp UTCDateInYourFormat:@"yyyy-MM-dd HH:mm:ss" :@"yyyy-MM-dd HH:mm:ss" :dictionary[@"time"]];
            NSString *Eventdttime = [commhelp getDateInYourFormat:@"yyyy-MM-dd HH:mm:ss" :@"yyyy-MM-dd" :DateAsNSString :date :nil];
            found = NO;
            for (NSString *str in [sections allKeys])
            {
                
                
                
                if ([str isEqualToString:Eventdttime])
                {
                    found = YES;
                }
            }
            if (!found)
            {
                [sections setObject:[[NSMutableArray alloc] init] forKey:Eventdttime];
                
            }
        }
        // Loop again and sort the items into their respective keys
        for (NSDictionary *dictionary in data_manual)
        {
            CommonHelper * commhelp=[[CommonHelper alloc] init];
            [[sections objectForKey:[commhelp getDateInYourFormat:@"yyyy-MM-dd HH:mm:ss" :@"yyyy-MM-dd" :DateAsNSString :dictionary[@"time"] :nil]]addObject:dictionary];
            
        }

        
        for (NSString *key in [sections allKeys])
        {
           // [[sections objectForKey:key] sortUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"MatchDate" ascending:NO]]];
             [[sections objectForKey:key] sortUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"time" ascending:NO]]];
          //  NSLog(@"%@",[sections objectForKey:key]);
            
           
            
        }

    }
    else if (selectedTabIs == DETECTED)
    {
        BOOL found;
        for(NSDictionary *dictionary in data_detected)
        {
            
            //NSString *date = dictionary[@"time"];
            CommonHelper * commhelp=[[CommonHelper alloc] init];
              NSString *date =[commhelp UTCDateInYourFormat:@"yyyy-MM-dd HH:mm:ss" :@"yyyy-MM-dd HH:mm:ss" :dictionary[@"time"]];
            NSString *Eventdttime = [commhelp getDateInYourFormat:@"yyyy-MM-dd HH:mm:ss" :@"yyyy-MM-dd" :DateAsNSString :date :nil];
            found = NO;
            for (NSString *str in [sections allKeys])
            {
                
                
                
                if ([str isEqualToString:Eventdttime])
                {
                    found = YES;
                }
            }
            if (!found)
            {
                [sections setObject:[[NSMutableArray alloc] init] forKey:Eventdttime];
                
            }
        }
        // Loop again and sort the items into their respective keys
        for (NSDictionary *dictionary in data_detected)
        {
            CommonHelper * commhelp=[[CommonHelper alloc] init];
            [[sections objectForKey:[commhelp getDateInYourFormat:@"yyyy-MM-dd HH:mm:ss" :@"yyyy-MM-dd" :DateAsNSString :dictionary[@"time"] :nil]]addObject:dictionary];
            
        }

        
        for (NSString *key in [sections allKeys])
        {
            [[sections objectForKey:key] sortUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"time" ascending:NO]]];
            
           /* NSMutableDictionary *data_16_march_dic = [[NSMutableDictionary alloc] init];
            [data_16_march_dic setObject:key forKey:@"date"];
            [data_16_march_dic setObject:[sections objectForKey:key] forKey:@"data"];
            
            [tempDataArray addObject: data_16_march_dic];*/
            
        }
        
       

    }
    
    // NSLog(@"section%@",sections);
    
   
    NSSortDescriptor *sortOrder = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:NO];
    NSArray *sortedKeys = [[sections allKeys] sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortOrder]];
   
    
    for (id key in sortedKeys)
    {
       // NSLog(@"%@kjjj",key);
     NSMutableDictionary *data_16_march_dic = [[NSMutableDictionary alloc] init];
    [data_16_march_dic setObject:key forKey:@"date"];
    [data_16_march_dic setObject:[sections objectForKey:key] forKey:@"data"];
        
    
    [tempDataArray addObject: data_16_march_dic];
    }
   
   
    allClinicalDataArray = tempDataArray;
   
    [self userSwitchTabPrepareDataToPresent];
    
    

}




-(void) userSwitchTabPrepareDataToPresent
{
    if (allClinicalDataArray.count > 0)
    {
        if (cellRowStatus == nil) {
            //cellRowStatus = NSMutableArray()
        } else {
            [cellRowStatus removeAllObjects];
        }
        NSMutableArray * rowsCount;
        int countSection = 0;
        for(NSDictionary * eachObj in allClinicalDataArray)
        {
             //NSLog(@"eachObj %@",eachObj);
             if([[eachObj objectForKey:@"data" ] count] > 0)
            {
                rowsCount = [[NSMutableArray alloc]init];
                 for(NSString* eachChildObj in [eachObj objectForKey:@"data"])
                {
                     //[rowsCount addObject:eachChildObj];
                    [rowsCount addObject:[NSNumber numberWithInt: 0]];
                }
                
            }
             [cellRowStatus insertObject:rowsCount atIndex:countSection];
            countSection++;
        }
    }
    
   

    //println("Current Data Array \(allClinicalDataArray)")
    //println("Current Data Row Status \(cellRowStatus)")
}



-(void) initalizeElements
{
  blueBorderColorForTab = [UIColor colorWithPatternImage:[UIImage imageNamed:@"blue_border_color"]];
    tabHeaderMainView.layer.borderWidth     =   1.0;
    tabHeaderMainView.layer.borderColor     =   blueBorderColorForTab.CGColor;
    
    tabHeaderMainView.layer.cornerRadius    =   7.0;
    tabHeaderMainView.layer.masksToBounds   =   true;
    
    tableBgGrayColor  = [UIColor colorWithPatternImage:[UIImage imageNamed:@"clinical_tbl_header_bg_gray.png"]];
    tabDivider1.backgroundColor = blueBorderColorForTab;
    
    tabDivider2.backgroundColor = blueBorderColorForTab;
    allClinicalDataArray=[[NSMutableArray alloc]init];
    cellRowStatus=[[NSMutableArray alloc]init];
    
}



-(IBAction)goToDashboard:(id)sender{
    self.slidingViewController.topViewController.view.layer.transform = CATransform3DMakeScale(1, 1, 1);
    self.slidingViewController.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"DashboardStoryboardId"];
}

-(IBAction)leftSideButtonAction:(id)sender{
    [self.slidingViewController anchorTopViewToRightAnimated:YES];
}

-(IBAction)switchTab:(UIButton *)sender
{
    NSString *onClickTempSelected;
    int tagwas=(int)[sender tag];
    //onClickTempSelected =  [ClinicalRecordsTabs  valueForKey:tagwas];
    if(tagwas==1)
    {
        onClickTempSelected=@"ALL";
        selectedTabIs=ALL;
    }
    else if(tagwas==2)
    {
        onClickTempSelected=@"DETECTED";
        selectedTabIs=DETECTED;
    }
    else if (tagwas==3)
    {
        onClickTempSelected=@"MANUAL1";
        selectedTabIs=MANUAL1;
    }
        // First make all False/Disable
        allTabButton.selected       = false;
        detectedTabButton.selected  = false;
        manualTabButton.selected    = false;
        
        // Assign the selected tab for further use
    
        
        //println("TAB IS \(selectedTabIs?.hashValue)")
        //println("TAB IS \(selectedTabIs?.rawValue)")
        
        switch(selectedTabIs)
        {
        case ALL:
                allTabButton.selected       =   true;
                break;
            
        case DETECTED:
                detectedTabButton.selected  =   true;
                break;
            
        case MANUAL1:
                manualTabButton.selected    =   true;
                break;
            
        default:
            //println("Caught Default in case While tab selecting the tab")
                break;
        }
        
        [self setDummyData ];
        
        // ToSwitch the data
         [tableClinicalRecords reloadData];
        

    
   /* allTabButton.selected       =   true;
    selectedTabIs = ALL;
    [self setDummyData ];
    
    // ToSwitch the data
    [tableClinicalRecords reloadData];*/
    
}




// MARK: - UITableView Delegates and DataSource


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return allClinicalDataArray.count;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger noOfRow = 0;
    if(section <= allClinicalDataArray.count)
    {
        
    noOfRow=[[[allClinicalDataArray objectAtIndex:section]valueForKey:@"data"]count];
        //            noOfRow = dataInSection.count
    }
    
    //println("Section \(section) == Row \(noOfRow)")
   // NSLog(@"Numberrow%ld",(long)noOfRow);
    return noOfRow;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if([cellRowStatus[indexPath.section][indexPath.row]intValue] == 1) // 1 = row is in OPEN STATE
    {
        NSArray *allRowData  = [allClinicalDataArray[indexPath.section] objectForKey:@"data"];
        NSString*typeName     =  [ [allRowData objectAtIndex:indexPath.row ]  objectForKey:@"type" ];
        
        if ([typeName isEqualToString:@"manual"])
        {
            return 160;
        }
        else if ([typeName isEqualToString:@"detected"])
        {
            return 200;
        }
        
    }
    return 54;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 24;
}



- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView*headerView= [[UIView alloc] initWithFrame:CGRectMake(0, 0, 250, 20)];
    headerView.backgroundColor = tableBgGrayColor;
    
    
    //date modification
   CommonHelper * commhelp=[[CommonHelper alloc] init];
   NSString *Eventdttime = [commhelp getDateInYourFormat:@"yyyy-MM-dd" :@"dd MMM yyyy" :DateAsNSString :[allClinicalDataArray[section] objectForKey:@"date"] :nil];
  
    NSString *dateStr =Eventdttime;
    NSDateFormatter *dtF = [[NSDateFormatter alloc] init];
    [dtF setDateFormat:@"dd-MMM-yyyy"];
    NSDate *d = [dtF dateFromString:dateStr];
    NSDateFormatter *monthDayFormatter = [[NSDateFormatter alloc] init] ;
    [monthDayFormatter setFormatterBehavior:NSDateFormatterBehaviorDefault];
    [monthDayFormatter setDateFormat:@"d MMM yyyy"];
    int date_day = [[monthDayFormatter stringFromDate:d] intValue];
    NSString *suffix_string = @"|st|nd|rd|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|th|st|nd|rd|th|th|th|th|th|th|th|st";
    NSArray *suffixes = [suffix_string componentsSeparatedByString: @"|"];
    NSString *suffix = [suffixes objectAtIndex:date_day];
    NSString *format = [NSString stringWithFormat:@"%d",date_day];
    NSString *dateStrfff = [format stringByAppendingString:suffix];
    //NSLog(@"%@", dateStrfff);
    [monthDayFormatter setDateFormat:@"MMM"];
    NSString *ss = [monthDayFormatter stringFromDate:d];
   
    
    NSString *final = [dateStrfff stringByAppendingString:[NSString stringWithFormat:@" %@",ss]];
    
    
    [monthDayFormatter setDateFormat:@"yyyy"];
    NSString *ss1 = [monthDayFormatter stringFromDate:d];
  
    
    NSString *final1 = [final stringByAppendingString:[NSString stringWithFormat:@" %@",ss1]];
   // NSLog(@"final string:---> %@",final1);
   
    
    UILabel * headerLbl  =  [[UILabel alloc] initWithFrame:CGRectMake(10, 9, 100, 10)];
    headerLbl.backgroundColor = UIColor.clearColor;
    headerLbl.text  =   final1;
    headerLbl.font  =   [UIFont fontWithName:@"Roboto-MediumItalic" size:12];
    headerLbl.textColor =   UIColor.grayColor;
    [headerView addSubview:headerLbl];
        return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
  return 0.1;
}



/*func tableView(tableView: UITableView!, viewForFooterInSection section: Int) -> UIView! {
    //        var headerView : UIView = UIView(frame: CGRectMake(15, 15, 250, 30.6))
    //        headerView.backgroundColor = UIColor.yellowColor()
    
    
    return UIView()
}*/



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
     NSString *cellIdentifier;
    NSString *eventTime,*typeName,*eventName,*posturename;
    
    
    if([[allClinicalDataArray[indexPath.section] objectForKey:@"data"]count] > 0)
    {
        NSArray * allRowData  = [allClinicalDataArray[indexPath.section] objectForKey:@"data"];
        
        typeName     =   [[allRowData objectAtIndex:indexPath.row] objectForKey:@"type"];
        eventName    =   [[allRowData objectAtIndex:indexPath.row] objectForKey:@"event_name"];
        
        
        CommonHelper * commhelp=[[CommonHelper alloc] init];
       // NSString *Eventtime = [commhelp getDateInYourFormat:@"yyyy-MM-dd HH:mm:ss" :@"HH:mm:ss" :DateAsNSString :[[allRowData objectAtIndex:indexPath.row] objectForKey:@"time"] :nil];
        
        NSString *localeventtime=[commhelp UTCDateInYourFormat:@"yyyy-MM-dd HH:mm:ss" :@"HH:mm:ss" :[[allRowData objectAtIndex:indexPath.row] objectForKey:@"time"]];
        eventTime    =   localeventtime;
        
       
        
        
        
        if ([typeName isEqualToString:@"manual"])
        {
            cellIdentifier = @"TableViewManualCellID";
            
        }
        else
        {
            cellIdentifier = @"TableViewDetectedCellID";
            
        }
        tableViewCellCustom = (ClinicalRecordsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
        
        //TableViewDetectedCellID
        if(tableViewCellCustom == nil)
        {
            tableViewCellCustom =[[ClinicalRecordsTableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
        }
        
        // Make ContentView Color as Bg Gray Color from image from pattern
        tableViewCellCustom.contentView.backgroundColor =   tableBgGrayColor;
        
        BOOL valueIs = true;
        if([cellRowStatus[indexPath.section][indexPath.row]intValue] == 1)
        {
            // if cell is detected and cell status is 1 = Expand that the height will be 168 else height will be 45
            valueIs = false;
        }
        
        
       
        tableViewCellCustom.eventNameTitleLBL.text = eventName;
        tableViewCellCustom.eventTimeTitleLBL.text=eventTime;
       
        if ([typeName isEqualToString:@"manual"])
        {
            // For Manual Prpperties
        tableViewCellCustom.manualExpandView.hidden = valueIs;
       tableViewCellCustom.manualBpLBL.text=[[allRowData objectAtIndex:indexPath.row] objectForKey:@"blood_pressure"];
       tableViewCellCustom.manualWgtLBL.text=[[allRowData objectAtIndex:indexPath.row] objectForKey:@"weight"];
       tableViewCellCustom.manualGlcoseLBL.text=[[allRowData objectAtIndex:indexPath.row] objectForKey:@"glucose"];
       tableViewCellCustom.manualOximetryLBL.text=[[allRowData objectAtIndex:indexPath.row] objectForKey:@"oximetry"];
      
       tableViewCellCustom.manualTextView.text=@"";
            NSMutableArray *results = [[NSMutableArray alloc]init];
            int flag=0;
            NSPredicate *pred;
            NSString* filter = @"%K == %@ &&%K == %@";
            NSArray* args = @[@"event_datetime",[[allRowData objectAtIndex:indexPath.row] objectForKey:@"time"],@"event_code",[[allRowData objectAtIndex:indexPath.row] objectForKey:@"event_code"]];
            if ([[allRowData objectAtIndex:indexPath.row] objectForKey:@"time"]!=NULL) {
                pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
                flag=1;
            } else {
                flag=0;
                NSLog(@"Enter Corect code empty");
                
            }
            
            if (flag == 1) {
                
                
                //NSLog(@"predicate: %@",pred);
                NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"EventStorage"];
                [fetchRequest setPredicate:pred];
                results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
                
                if (results.count > 0) {
                    
                    NSManagedObject *Eventsett = (NSManagedObject *)[results objectAtIndex:0];
                    
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    BOOL standalone_Mode = [defaults boolForKey:@"ActiveStandalonemode"];
                    
                    if (!standalone_Mode)
                    {
                        
                        switch ([[[allRowData objectAtIndex:indexPath.row] objectForKey:@"event_receive_complete"] intValue])
                        {
                           
                            case 1:
                                tableViewCellCustom.detectedReceiveType.text=@"";
                                if([[Eventsett valueForKey:@"event_updated_to_device"]isEqualToString:@"1"])
                                {
                                    
                                    tableViewCellCustom.manualReceiveType.text=@"";
                                }
                                else{
                                    tableViewCellCustom.manualReceiveType.text=@"Event Not Uploaded";
                                   tableViewCellCustom.manualReceiveType.textColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"yellow_color.png"]];
                                    
                                }
                                break;
                                
                            default:
                                break;
                        }
                        
                    }
                    else
                    {
                        switch ([[[allRowData objectAtIndex:indexPath.row] objectForKey:@"event_receive_complete"] intValue])
                        {
                           
                            case 1:
                                tableViewCellCustom.detectedReceiveType.text=@"";
                                if([[Eventsett valueForKey:@"event_updated_to_device"] isEqualToString:@"1"])
                                {
                                    
                                    tableViewCellCustom.manualReceiveType.text=@"";
                                }
                                else{
                                    tableViewCellCustom.manualReceiveType.text=@"";
                                    
                                }
                                break;
                                
                            default:
                                break;
                        }
                    }
                    
                    
                }
                
            }
    
        }
        else if( [typeName isEqualToString:@"detected"])
        {
           
       tableViewCellCustom.detectedExpandView.hidden = valueIs;
            posturename=[[allRowData objectAtIndex:indexPath.row] objectForKey:@"posture"];
            if([posturename isEqualToString:@"1"])
            {
                
               tableViewCellCustom.postureimage.image=[UIImage imageNamed:@"ic_sleeping_icon.png"];
            }
            else if ([posturename isEqualToString:@"2"])
            {
               
                  tableViewCellCustom.postureimage.image=[UIImage imageNamed:@"running.png"];
            }
            else if ([posturename isEqualToString:@"3"])
            {
                
                  tableViewCellCustom.postureimage.image=[UIImage imageNamed:@"walking.png"];
            }
            else
            {
                
                 tableViewCellCustom.postureimage.image=[UIImage imageNamed:@"ic_standing_icon.png"];
            }
            
            NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
            NSString *temperaturetype=[TempDefault objectForKey:@"TEMPERATURE"];
            if([temperaturetype isEqualToString:@"CELSIUS"])
            {
                tableViewCellCustom.tempunits.text=[NSString stringWithFormat:@"C%@", @"\u00B0"];
                 tableViewCellCustom.detectedTempLBL.text=[NSString stringWithFormat:@"%.02f",[[[allRowData objectAtIndex:indexPath.row] objectForKey:@"temp"] floatValue]];
            }
            else
            {
                CommonHelper *help=[[CommonHelper alloc]init];
                tableViewCellCustom.tempunits.text=[NSString stringWithFormat:@"F%@", @"\u00B0"];
                 tableViewCellCustom.detectedTempLBL.text=[help celsiustoFahernheit:[[allRowData objectAtIndex:indexPath.row] objectForKey:@"temp"]];
            }
                
        tableViewCellCustom.detectedHrLBL.text=[[allRowData objectAtIndex:indexPath.row] objectForKey:@"hr"];
       
        tableViewCellCustom.detectedRespLBL.text=[[allRowData objectAtIndex:indexPath.row] objectForKey:@"resp"];
        tableViewCellCustom.detectedRRLBL.text=[[allRowData objectAtIndex:indexPath.row] objectForKey:@"rr"];
        tableViewCellCustom.detectedQRSLBL.text=[[allRowData objectAtIndex:indexPath.row] objectForKey:@"qrs"];
        tableViewCellCustom.detectedQTLBL.text=[[allRowData objectAtIndex:indexPath.row] objectForKey:@"qt"];
        tableViewCellCustom.detectedQTcLBL.text=[[allRowData objectAtIndex:indexPath.row] objectForKey:@"qtc"];
        tableViewCellCustom.detectedPRLBL.text=[[allRowData objectAtIndex:indexPath.row] objectForKey:@"pr"];
        tableViewCellCustom.detectedRespvarLBL.text=[[allRowData objectAtIndex:indexPath.row] objectForKey:@"resp_var"];
       
            
        
             NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            BOOL standalone_Mode = [defaults boolForKey:@"ActiveStandalonemode"];
            
            
            NSMutableArray *results = [[NSMutableArray alloc]init];
            int flag=0;
            NSPredicate *pred;
            NSString* filter = @"%K == %@ &&%K == %@";
            NSArray* args = @[@"event_datetime",[[allRowData objectAtIndex:indexPath.row] objectForKey:@"time"],@"event_code",[[allRowData objectAtIndex:indexPath.row] objectForKey:@"event_code"]];
            if ([[allRowData objectAtIndex:indexPath.row] objectForKey:@"time"]!=NULL) {
                pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
                flag=1;
            } else {
                flag=0;
                NSLog(@"Enter Corect code empty");
                
            }
            
            if (flag == 1) {
                
                
                //NSLog(@"predicate: %@",pred);
                NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"EventStorage"];
                [fetchRequest setPredicate:pred];
                results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
                
                if (results.count > 0) {
                    
                    NSManagedObject *Eventsett = (NSManagedObject *)[results objectAtIndex:0];
                    
                    
                    UIColor *BlueColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"button_bg_color_1"]];
                    UIColor *GrayColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"gray_disabled_color_img"]];
                    if (!standalone_Mode)
                    {
                        
                        switch ([[[allRowData objectAtIndex:indexPath.row] objectForKey:@"event_receive_complete"] intValue]) {
                            case 0:
                            {
                                tableViewCellCustom.detectedReceiveType.text=@"Waiting For ECG..";
                                tableViewCellCustom.detectedReceiveType.textColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"yellow_color.png"]];
                            
                                
                                tableViewCellCustom.detectedReceiveType.text=@"Waiting For ECG";
                                tableViewCellCustom.detectedReceiveType.textColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"yellow_color.png"]];
                                
                                
                                tableViewCellCustom.viewLiveEcgButton.backgroundColor=GrayColor;
                                
                              /*  NSDate *date = [[NSDate alloc] init];
                                NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
                                [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                                NSDate *date1=[formatter dateFromString:[[allRowData objectAtIndex:indexPath.row] objectForKey:@"time"]];
                                NSTimeInterval secondsBetween = [date timeIntervalSinceDate:date1];
                                int milli = 20 * 60 * 1000;
                                if(secondsBetween>milli)
                                {
                                    tableViewCellCustom.detectedReceiveType.text=@"ECG NOT RECEIVED";
                                    [Eventsett setValue:@"2" forKey:@"event_receive_complete"];
                                    [self.managedObjectContext save:nil];
                                }*/
                                 NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] init];
                                [userInfo setObject:[[allRowData objectAtIndex:indexPath.row] objectForKey:@"time"] forKey:@"DateTime"];
                                 [userInfo setObject: [[allRowData objectAtIndex:indexPath.row] objectForKey:@"event_code"] forKey:@"EventCode"];
                                
                              
                            [NSTimer scheduledTimerWithTimeInterval: 1200.0 target: self
                                                               selector: @selector(Tableupdataing:)  userInfo:userInfo repeats: NO];
                               
                                
                               
                            }
                                break;
                            case 1:
                            {
                                tableViewCellCustom.detectedReceiveType.text=@"";
                                  tableViewCellCustom.viewLiveEcgButton.backgroundColor=BlueColor;
                                if([[Eventsett valueForKey:@"event_updated_to_device"]isEqualToString:@"1"])
                                {
                                    
                                    tableViewCellCustom.detectedReceiveType.text=@"";
                                }
                                else{
                                    tableViewCellCustom.detectedReceiveType.text=@"ECG Not Uploaded";
                                    tableViewCellCustom.detectedReceiveType.textColor=[UIColor darkGrayColor];
                                    
                                }
                            }
                                break;
                            case 2:
                            {
                                  tableViewCellCustom.viewLiveEcgButton.backgroundColor=GrayColor;
                                tableViewCellCustom.detectedReceiveType.text=@"ECG Not Received";
                                tableViewCellCustom.detectedReceiveType.textColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"yellow_color.png"]];
                            }
                                break;
                                
                            case 3:
                            {
                                  tableViewCellCustom.viewLiveEcgButton.backgroundColor=GrayColor;
                                tableViewCellCustom.detectedReceiveType.text=@"Receiving ECG";
                                tableViewCellCustom.detectedReceiveType.textColor=[UIColor blackColor];
                                break;
                            }
                                
                            default:
                                break;
                        }
                        
                    }
                    else
                    {
                        switch ([[[allRowData objectAtIndex:indexPath.row] objectForKey:@"event_receive_complete"] intValue])
                        {
                            case 0:
                            {
                                tableViewCellCustom.detectedReceiveType.text=@"Waiting for ECG";
                                tableViewCellCustom.detectedReceiveType.textColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"yellow_color.png"]];
                                
                                 tableViewCellCustom.viewLiveEcgButton.backgroundColor=GrayColor;
                                
                                  NSDate *date = [[NSDate alloc] init];
                                 NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
                                 [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                            NSDate *date1=[formatter dateFromString:[[allRowData objectAtIndex:indexPath.row] objectForKey:@"time"]];
                                  NSTimeInterval secondsBetween = [date timeIntervalSinceDate:date1];
                                  int milli = 20 * 60 * 1000;
                                  if(secondsBetween>milli)
                                  {
                                       tableViewCellCustom.detectedReceiveType.text=@"ECG Not Received";
                                      [Eventsett setValue:@"2" forKey:@"event_receive_complete"];
                                      [self.managedObjectContext save:nil];
                                  }
                               
                            }
                                break;
                            case 1:
                                tableViewCellCustom.detectedReceiveType.text=@"";
                                 tableViewCellCustom.viewLiveEcgButton.backgroundColor=BlueColor;
                                if([[Eventsett valueForKey:@"event_updated_to_device"] isEqualToString:@"1"])
                                {
                                    
                                    tableViewCellCustom.detectedReceiveType.text=@"";
                                }
                                else{
                                    tableViewCellCustom.detectedReceiveType.text=@"";
                                    
                                }
                                break;
                            case 2:
                                 tableViewCellCustom.viewLiveEcgButton.backgroundColor=GrayColor;
                                tableViewCellCustom.detectedReceiveType.text=@"ECG Not Received";
                                tableViewCellCustom.detectedReceiveType.textColor=[UIColor darkGrayColor];
                                break;
                                
                            case 3:
                                 tableViewCellCustom.viewLiveEcgButton.backgroundColor=GrayColor;
                                tableViewCellCustom.detectedReceiveType.text=@"Receiving ECG";
                                tableViewCellCustom.detectedReceiveType.textColor=[UIColor blackColor];
                                break;
                                
                            default:
                                break;
                        }
                    }
                    
                    
                    }

                }
            
        
        tableViewCellCustom.detectedTextView.text=@"";
            
        switch ([[[allRowData objectAtIndex:indexPath.row] objectForKey:@"event_type"] intValue]) {
                case 0:
                    tableViewCellCustom.detectedEventType.backgroundColor=[UIColor yellowColor]; // ("Y");
                    break;
                case 1:
                    tableViewCellCustom.detectedEventType.backgroundColor=[UIColor redColor];// ("R");
                    break;
                case 2:
                    tableViewCellCustom.detectedEventType.backgroundColor=[UIColor blueColor];// ("R");
                    break;
                case 3:
                    tableViewCellCustom.detectedEventType.backgroundColor=[UIColor grayColor];// ("R");
                    break;
                default:
                    break;
            }
            tableViewCellCustom.viewLiveEcgButton.tag = indexPath.row;
            [tableViewCellCustom.viewLiveEcgButton addTarget:self action:@selector(viewLiveECG:) forControlEvents:UIControlEventTouchDown];
            [tableViewCellCustom bringSubviewToFront:tableViewCellCustom.viewLiveEcgButton];
            
            //                tableViewCellCustom.selectionStyle = UITableViewCellSelectionStyle.None
            //                tableViewCellCustom.contentView.bringSubviewToFront(tableViewCellCustom.viewLiveEcgButton)
            if([posturename isEqualToString:@"1"])
            {
                 if([[[allRowData objectAtIndex:indexPath.row] objectForKey:@"event_respiration_onoff"] intValue]==0)
                 {
                     [tableViewCellCustom.detectedHeaderRespLBL setHidden:YES];
                     [tableViewCellCustom.detectedRespLBL  setHidden:YES];
                     [tableViewCellCustom.detectedRespImage setHidden:YES];
                     [tableViewCellCustom.detectedRespunit setHidden:YES];
                     
                     [tableViewCellCustom.detectedHeaderRespVarLBL setHidden:YES];
                     [tableViewCellCustom.detectedRespvarLBL  setHidden:YES];
                     [tableViewCellCustom.detectedRespVarImage setHidden:YES];
                     [tableViewCellCustom.detectedRespVarunit setHidden:YES];
                     
                    
                 }
                else
                {
                [tableViewCellCustom.detectedHeaderRespLBL setHidden:NO];
                    [tableViewCellCustom.detectedRespLBL  setHidden:NO];
                     [tableViewCellCustom.detectedRespImage setHidden:NO];
                     [tableViewCellCustom.detectedRespunit setHidden:NO];
                    
                    
                    [tableViewCellCustom.detectedHeaderRespVarLBL setHidden:NO];
                    [tableViewCellCustom.detectedRespvarLBL  setHidden:NO];
                    [tableViewCellCustom.detectedRespVarImage setHidden:NO];
                    [tableViewCellCustom.detectedRespVarunit setHidden:NO];
                    
                   
                }
            }
            else
            {
                [tableViewCellCustom.detectedHeaderRespLBL setHidden:YES];
                [tableViewCellCustom.detectedRespLBL  setHidden:YES];
                 [tableViewCellCustom.detectedRespImage setHidden:YES];
                 [tableViewCellCustom.detectedRespunit setHidden:YES];
                
                [tableViewCellCustom.detectedHeaderRespVarLBL setHidden:YES];
                [tableViewCellCustom.detectedRespvarLBL  setHidden:YES];
                [tableViewCellCustom.detectedRespVarImage setHidden:YES];
                [tableViewCellCustom.detectedRespVarunit setHidden:YES];
                
                
            }
            
        }
        
        
        tableViewCellCustom.clipsToBounds = true;
        
    }
    
    return tableViewCellCustom;
}


-(void) Tableupdataing:(NSTimer*) timer
{
    NSLog(@"tableupdating");
    NSDictionary *userInfo = [timer userInfo];
   

    NSString *dtetime=[userInfo valueForKey:@"DateTime"];
    NSString *evtcode=[userInfo valueForKey:@"EventCode"];
    NSMutableArray *results = [[NSMutableArray alloc]init];
    int flag=0;
    NSPredicate *pred;
    NSString* filter = @"%K == %@ &&%K == %@";
    NSArray* args = @[@"event_datetime",dtetime,@"event_code",evtcode];
    if ([userInfo objectForKey:@"DateTime"]!=NULL) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }
    
    if (flag == 1) {
        
        
        //NSLog(@"predicate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"EventStorage"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            [TableupdateTimer invalidate];
            NSManagedObject *Eventsett = (NSManagedObject *)[results objectAtIndex:0];
            if(![[Eventsett valueForKey:@"event_receive_complete"] isEqualToString:@"1"])
            {
            tableViewCellCustom.detectedReceiveType.text=@"ECG Not Received";
            [Eventsett setValue:@"2" forKey:@"event_receive_complete"];
            [self.managedObjectContext save:nil];
              [tableClinicalRecords reloadData];
            
            }
        }
        else
        {
            [TableupdateTimer invalidate];
        }
    }
    
   
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    // Either the cell can be "TableViewDetectedCellID" or "TableViewManualCellID"
    
    NSString * cellIdentifierString = [[tableView cellForRowAtIndexPath:indexPath]reuseIdentifier];
    
    //println("Cell on click \(cellIdentifierString)")
    
    if ([cellIdentifierString isEqualToString:@"TableViewDetectedCellID"] || [cellIdentifierString isEqualToString:@"TableViewManualCellID"])
    {
        //println(" Detected tap ")
        
       // NSIndexPath *previousPath = [NSIndexPath indexPathForRow:selectedIndex inSection:indexPath.section];

        // SET 0 = CLOSE
        // SET 1 = OPEN
        int rowStatus           =   [cellRowStatus[indexPath.section][indexPath.row ] intValue] ;
        
        // We are not using Else Block as bt default it is Else
        // Row is Alreay OPEN, Now CLOSE it and set the value to 0, indicating it as CLOSE
        int newUpdatedRowStatus =   0;
        
        if( rowStatus == 0 )
        {
            // Row is Already ClOSE, Now OPEN it and set the value to 1, indicating it as OPEN
            newUpdatedRowStatus =   1;
            
        }
        
        
       //[[cellRowStatus objectAtIndex:indexPath.section]replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithInteger:newUpdatedRowStatus]];
        [cellRowStatus[indexPath.section] replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithInteger:newUpdatedRowStatus]];
        [self.tableClinicalRecords reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        //[tableClinicalRecords reloadData];
        [self.tableClinicalRecords scrollToRowAtIndexPath :indexPath atScrollPosition:UITableViewScrollPositionTop animated: true];
        
        
    }
    
    
}


- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.isDragging) {
        UIView *myView = cell.contentView;
        CALayer *layer = myView.layer;
        CATransform3D rotationAndPerspectiveTransform = CATransform3DIdentity;
        rotationAndPerspectiveTransform.m34 = 1.0 / -1000;
        if (scrollOrientation == UIImageOrientationDown) {
            rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, M_PI*0.5, 1.0f, 0.0f, 0.0f);
        } else {
            rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, -M_PI*0.5, 1.0f, 0.0f, 0.0f);
        }
        layer.transform = rotationAndPerspectiveTransform;
        [UIView animateWithDuration:.5 animations:^{
            layer.transform = CATransform3DIdentity;
        }];
      
    }
}

- (void) scrollViewDidScroll:(UIScrollView *)scrollView {
    scrollOrientation = scrollView.contentOffset.y > lastPos.y?UIImageOrientationDown:UIImageOrientationUp;
    lastPos = scrollView.contentOffset;
}


-(void) viewLiveECG:(UIButton *)sender
{
    //println("view ECG for")
   // NSLog(@"send%ld",(long)sender.tag);

    
    UIButton *senderButton = (UIButton *)sender;
    //NSLog(@"current Row=%d",senderButton.tag);
    
    CGPoint touchPoint = [sender convertPoint:CGPointZero toView:tableClinicalRecords]; // maintable --> replace your tableview name
    NSIndexPath *clickedButtonIndexPath = [tableClinicalRecords indexPathForRowAtPoint:touchPoint];
    
    NSLog(@"index path.section ==%ld",(long)clickedButtonIndexPath.section);
    
    NSLog(@"index path.row ==%ld",(long)clickedButtonIndexPath.row);
    NSString *ecgj;
   /* UITableViewCell* clickedCell = (UITableViewCell*)[sender superview];
    

    NSIndexPath *clickedButtonPath = [tableClinicalRecords indexPathForCell:clickedCell];
    NSLog(@"sect%ld",(long)clickedButtonPath.section);*/
    
    if([[allClinicalDataArray[clickedButtonIndexPath.section] objectForKey:@"data"]count] > 0)
    {
        NSArray * allRowData  = [allClinicalDataArray[clickedButtonIndexPath.section] objectForKey:@"data"];
       ecgj= [[allRowData objectAtIndex:senderButton.tag] objectForKey:@"ecg_path"];
        NSLog(@"ecgpath%@",ecgj);
        
    if([[[allRowData objectAtIndex:senderButton.tag] objectForKey:@"event_receive_complete"] isEqualToString:@"1"])
    {
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Mainstoryboard_iPad" bundle:nil];
        ViewRecordedEcgViewController * controller = (ViewRecordedEcgViewController *)[storyboard instantiateViewControllerWithIdentifier:@"view_recorded_ecg_storyboard_id"];
          controller.ecgpath=ecgj;
        controller.HR1=[[allRowData objectAtIndex:senderButton.tag] objectForKey:@"hr"];
        controller.Posture=[[allRowData objectAtIndex:senderButton.tag] objectForKey:@"posture"];
        
        NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
        NSString *temperaturetype=[TempDefault objectForKey:@"TEMPERATURE"];
        if([temperaturetype isEqualToString:@"CELSIUS"])
        {
            tableViewCellCustom.tempunits.text=[NSString stringWithFormat:@"C%@", @"\u00B0"];
            controller.Temp= [NSString stringWithFormat:@"%.02f",[[[allRowData objectAtIndex:senderButton.tag] objectForKey:@"temp"] floatValue]];        }
        else
        {
            CommonHelper *help=[[CommonHelper alloc]init];
            tableViewCellCustom.tempunits.text=[NSString stringWithFormat:@"F%@", @"\u00B0"];
            controller.Temp=[help celsiustoFahernheit:[[allRowData objectAtIndex:senderButton.tag] objectForKey:@"temp"]];
        }
        
        
        
        controller.Resprate=[[allRowData objectAtIndex:senderButton.tag] objectForKey:@"resp"];
        controller.RR=[[allRowData objectAtIndex:senderButton.tag] objectForKey:@"rr"];
        controller.QRS=[[allRowData objectAtIndex:senderButton.tag] objectForKey:@"qrs"];
        controller.QT=[[allRowData objectAtIndex:senderButton.tag] objectForKey:@"qt"];
        controller.QTC=[[allRowData objectAtIndex:senderButton.tag] objectForKey:@"qtc"];
        controller.PR=[[allRowData objectAtIndex:senderButton.tag] objectForKey:@"pr"];
        controller.Eventcode=[[allRowData objectAtIndex:senderButton.tag] objectForKey:@"event_code"];
        controller.EventValue=[[allRowData objectAtIndex:senderButton.tag] objectForKey:@"event_value"];
        controller.EventDateTime=[[allRowData objectAtIndex:senderButton.tag] objectForKey:@"time"];
        
        [controller setUp];
        [self presentViewController:controller animated:YES completion:nil];
    }
    else
    {
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ViewRecordedEcgViewController * controller = (ViewRecordedEcgViewController *)[storyboard instantiateViewControllerWithIdentifier:@"view_recorded_ecg_storyboard_id"];
        controller.ecgpath=ecgj;
        controller.HR1=[[allRowData objectAtIndex:senderButton.tag] objectForKey:@"hr"];
        controller.Posture=[[allRowData objectAtIndex:senderButton.tag] objectForKey:@"posture"];
        
        NSUserDefaults * TempDefault = [NSUserDefaults standardUserDefaults];
        NSString *temperaturetype=[TempDefault objectForKey:@"TEMPERATURE"];
        if([temperaturetype isEqualToString:@"CELSIUS"])
        {
           // tableViewCellCustom.tempunits.text=[NSString stringWithFormat:@"C%@", @"\u00B0"];
           controller.Temp= [NSString stringWithFormat:@"%.02f",[[[allRowData objectAtIndex:senderButton.tag] objectForKey:@"temp"] floatValue]];
        }
        else
        {
            CommonHelper *help=[[CommonHelper alloc]init];
            //tableViewCellCustom.tempunits.text=[NSString stringWithFormat:@"F%@", @"\u00B0"];
           controller.Temp=[help celsiustoFahernheit:[[allRowData objectAtIndex:senderButton.tag] objectForKey:@"temp"]];
        }
        
       
        
        controller.Resprate=[[allRowData objectAtIndex:senderButton.tag] objectForKey:@"resp"];
        controller.RR=[[allRowData objectAtIndex:senderButton.tag] objectForKey:@"rr"];
        controller.QRS=[[allRowData objectAtIndex:senderButton.tag] objectForKey:@"qrs"];
        controller.QT=[[allRowData objectAtIndex:senderButton.tag] objectForKey:@"qt"];
        controller.QTC=[[allRowData objectAtIndex:senderButton.tag] objectForKey:@"qtc"];
        controller.PR=[[allRowData objectAtIndex:senderButton.tag] objectForKey:@"pr"];
        controller.Eventcode=[[allRowData objectAtIndex:senderButton.tag] objectForKey:@"event_code"];
        controller.EventValue=[[allRowData objectAtIndex:senderButton.tag] objectForKey:@"event_value"];
        controller.EventDateTime=[[allRowData objectAtIndex:senderButton.tag] objectForKey:@"time"];
       
        [controller setUp];
        //[controller plotting];
        [self presentViewController:controller animated:YES completion:nil];

        }
       }
     else
     {
         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"ECG is receiving"
                                                        delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
         [alert show];
        
     }
    }
    
}




- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}


@end
