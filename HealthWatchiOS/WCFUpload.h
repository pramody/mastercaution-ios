//
//  WCFUpload.h
//  HealthWatch
//
//  Created by METSL MAC MINI on 18/12/15.
//  Copyright © 2015 METSL MAC MINI. All rights reserved.
//

#import "UploadService.h"
#import "SMTPLibrary/SKPSMTPMessage.h"
#import <CFNetwork/CFNetwork.h>
#import "NSData+Base64Additions.h"
#import "LocationHandler.h"

@interface WCFUpload : UploadService<SKPSMTPMessageDelegate,LocationHandlerDelegate>
{
     SKPSMTPState HighestState;
}
-(void)SendPatientlivestatus;
-(void)mcmVersionDetails :(NSString *)hardwareVersion;
-(void)bitInsert :(NSString *)accelerometerStatus :(NSString *)electrodestatus :(NSString *)battery_status :(NSString *)bluetooth_status
                 :(NSString *)ir_sensor_status :(NSString *)sdcard_status;
-(void)sendEcgelectrodeStatus :(NSInteger)RA  :(NSInteger)LA :(NSInteger)LL :(NSInteger)RL :(NSInteger)V11 :(NSInteger)V22
                              :(NSInteger)V33 :(NSInteger)V44 :(NSInteger)V55 :(NSInteger)V66;

@property (nonatomic, strong) IBOutlet SKPSMTPMessage *test_smtp_message;

-(void)uploadMCDSetting;
-(void)UploadEventHeader;
-(void)UploadEventHeader1 :(NSString *)event_dat_tme  :(NSString *)event_code;
-(void)UploadECG:(NSString *)Ecgid :(NSString *)FileName;
-(void)uploadSuspensionSetting1 :(NSString *)eventsuspensiontime :(NSString *)eventcode;
-(void)UpdatePatientUnit;
@end
