//
//  ClinicalRecordsTableViewCell.h
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 22/06/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomUIButton.h"

@interface ClinicalRecordsTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel * tempLBL;
@property (nonatomic, strong) IBOutlet UILabel *tempunits;

@property (nonatomic, strong) IBOutlet CustomUIButton * expOrCollButton;


@property (nonatomic, strong) IBOutlet UILabel * eventNameTitleLBL;
@property (nonatomic, strong) IBOutlet UILabel * eventTimeTitleLBL;

// Detected Properties
@property (nonatomic, strong) IBOutlet UILabel * detectedTitleLBL;
@property (nonatomic, strong) IBOutlet UILabel * detectedHrLBL;
@property (nonatomic, strong) IBOutlet UILabel * detectedTempLBL;
@property (nonatomic, strong) IBOutlet UILabel * detectedRespLBL;
@property (nonatomic, strong) IBOutlet UILabel * detectedRRLBL;
@property (nonatomic, strong) IBOutlet UILabel * detectedQRSLBL;
@property (nonatomic, strong) IBOutlet UILabel * detectedQTLBL;
@property (nonatomic, strong) IBOutlet UILabel * detectedQTcLBL;
@property (nonatomic, strong) IBOutlet UILabel * detectedPRLBL;
@property (nonatomic, strong) IBOutlet UILabel * detectedRespvarLBL;
@property (nonatomic, strong) IBOutlet UILabel * detectedEventType;
@property (nonatomic, strong) IBOutlet UIView * detectedExpandView;
@property (nonatomic, strong) IBOutlet UILabel * detectedReceiveType;
@property (nonatomic, strong) IBOutlet UIView* topShowView;
@property (nonatomic, strong) IBOutlet UIImageView* postureimage;

@property (nonatomic, strong) IBOutlet UILabel * detectedHeaderRespLBL;
@property (nonatomic, strong) IBOutlet UILabel *detectedRespunit;
@property (nonatomic, strong) IBOutlet UIImageView* detectedRespImage;

@property (nonatomic, strong) IBOutlet UILabel * detectedHeaderRespVarLBL;
@property (nonatomic, strong) IBOutlet UILabel *detectedRespVarunit;
@property (nonatomic, strong) IBOutlet UIImageView* detectedRespVarImage;

@property (nonatomic, strong) IBOutlet UILabel * detectedHeaderRRLBL;
@property (nonatomic, strong) IBOutlet UILabel *detectedRRunit;
@property (nonatomic, strong) IBOutlet UIImageView* detectedRRImage;

@property (nonatomic, strong) IBOutlet UITextView*detectedTextView;

@property (nonatomic, strong) IBOutlet CustomUIButton * viewLiveEcgButton;


// Manual Properties
@property (nonatomic, strong) IBOutlet UILabel * manualTitleLBL;
@property (nonatomic, strong) IBOutlet UILabel * manualBpLBL;
@property (nonatomic, strong) IBOutlet UILabel * manualWgtLBL;
@property (nonatomic, strong) IBOutlet UILabel * manualGlcoseLBL;
@property (nonatomic, strong) IBOutlet UITextView * manualTextView;
@property (nonatomic, strong) IBOutlet UIView *manualExpandView;
@property (nonatomic, strong) IBOutlet UILabel *manualReceiveType;
@property (nonatomic, strong) IBOutlet UILabel *manualOximetryLBL;
@end
