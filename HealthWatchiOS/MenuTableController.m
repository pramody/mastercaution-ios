//
//  MenuTableController.m
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 17/06/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import "MenuTableController.h"
#import "LeftMenuCustomTableViewCell.h"
#import "OneTimeViewController.h"
#import "SDCAlertView.h"
#import "SDCAlertViewController.h"
#import <UIView+SDCAutoLayout.h>
#import "DashboardViewController.h"
#import "EADSessionController.h"
#import "DoctorProfileViewController.h"
#import <AudioToolbox/AudioToolbox.h>
BOOL alertflag=false;
@interface MenuTableController ()

@end

@implementation MenuTableController
@synthesize lblspin,spinner;

- (void)viewDidLoad {
    [super viewDidLoad];
    AppDelegate * appDelegate = (AppDelegate *)UIApplication.sharedApplication.delegate ;
    appDelegate.orientationKey = @"PRO";
    [appDelegate forceOrientationTakePlace:appDelegate.orientationKey];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    standalone_Mode = [defaults boolForKey:@"ActiveStandalonemode"];
    
    NSString *infoFilePath = [[NSBundle mainBundle] pathForResource:@"Info" ofType:@"plist"];
    leftMenuSideItemsNSDic = [NSDictionary dictionaryWithContentsOfFile:infoFilePath];
    self.leftMenuSideItemsNSArr = [leftMenuSideItemsNSDic objectForKey: @"Left_Side_Menu_Items"];
    //NSLog(@"%@",self.leftMenuSideItemsNSArr);
    
    [self.leftMenuTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    
      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(EventOccured:) name:RWT_EVENT_STATUS_NOTIFICATION object:nil];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"PatientDetails" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    NSString * userlvl;
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        
        if (result.count > 0) {
            NSManagedObject *patient = (NSManagedObject *)[result objectAtIndex:0];
           // NSLog(@"1 - %@", patient);
            
            userlvl   = [patient valueForKey:@"level"];
            
        }
    }
    /*  NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
     NSData *usrdta=[userDefault objectForKey:@"user_details"];
     userlevel=[usrdta valueForKey:@"Patientlevel"];*/
    if([userlvl isEqualToString:@"Level1"])
    {
        userlevel=@"SAFE GUARD MODE";
    }
    else if([userlvl isEqualToString:@"Level2"])
    {
        userlevel=@"PREMIUM GUARD MODE";
    }
    else if([userlvl isEqualToString:@"Level3"])
    {
        userlevel=@"AUTO PREMIUM SAFE GUARD MODE";
    }
    else
    {
        userlevel=@"PERSONAL ALERT GUARDIAN";
    }
    
    [self.leftMenuTableView reloadData];
    
    self.leftMenuTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
   // badgecnt=0;

}


- (void)EventOccured:(NSNotification *)notification {
    // Connection status changed. Indicate on GUI.
    BOOL isConnected = [(NSNumber *) (notification.userInfo)[@"NoEventOccured"] boolValue];
    
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        // Set image based on connection status
        
        
        if (isConnected) {
            
            badgecnt=[NSString stringWithFormat:@"%d",[badgecnt intValue]+1];
            [self.leftMenuTableView reloadData];
        }
        
        
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
   
       return self.leftMenuSideItemsNSArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
   /* if(section==0)
        return 4;
    else if(section==1)
        return 3;
    
    return 3;*/
       return [[self.leftMenuSideItemsNSArr objectAtIndex:section]count];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *QuoteCellIdentifier = @"TableViewCellID";
    
    LeftMenuCustomTableViewCell *cell = (LeftMenuCustomTableViewCell*)[tableView dequeueReusableCellWithIdentifier:QuoteCellIdentifier];
    
    if(!standalone_Mode)
    {
    // if(indexPath.section!=3)
    // {
    NSDictionary *eachSectionNSDic=self.leftMenuSideItemsNSArr[indexPath.section][indexPath.row];
    NSString *itemName= [eachSectionNSDic objectForKey:@"name"];
    NSString *itemImageNormal= [eachSectionNSDic objectForKey:@"image_name_normal"];
    NSString *itemImageHigh= [eachSectionNSDic objectForKey:@"image_name_high"];
    
    cell.leftMenuItemIcon.image=[UIImage imageNamed:itemImageNormal];
    cell.leftMenuItemIcon.highlightedImage=[UIImage imageNamed:itemImageHigh];
    cell.leftMenuItemName.text=[NSString stringWithFormat:@"%@",itemName];
    // }
    }
    else
    {
        NSDictionary *eachSectionNSDic=self.leftMenuSideItemsNSArr[indexPath.section][indexPath.row];
        NSString *itemName= [eachSectionNSDic objectForKey:@"name"];
        NSString *itemImageNormal= [eachSectionNSDic objectForKey:@"image_name_normal"];
        NSString *itemImageHigh= [eachSectionNSDic objectForKey:@"image_name_high"];
        
        cell.leftMenuItemIcon.image=[UIImage imageNamed:itemImageNormal];
        cell.leftMenuItemIcon.highlightedImage=[UIImage imageNamed:itemImageHigh];
        cell.leftMenuItemName.text=[NSString stringWithFormat:@"%@",itemName];
    }
    
    if(indexPath.section==0)
    {
        if(indexPath.row==2)
        {
        if(badgecnt.length!=0)
        {
        cell.leftMenuBadgeIcon.text=badgecnt;
            cell.leftMenuBadgeIcon.textAlignment=NSTextAlignmentCenter;
        cell.leftMenuBadgeIcon.layer.backgroundColor = [UIColor redColor].CGColor;
        cell.leftMenuBadgeIcon.layer.cornerRadius =  cell.leftMenuBadgeIcon.frame.size.height/2;
        cell.leftMenuBadgeIcon.layer.masksToBounds = YES;
        }
        }
    }
    
    UIView *customColorView = [[UIView alloc] init];
    
    if([userlevel isEqualToString:@"SAFE GUARD MODE"])
    {
        if(indexPath.section==1&&(indexPath.row==1||indexPath.row==2))
        {
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.leftMenuItemName.textColor=[UIColor lightGrayColor];
        }
        else
        {
            UIColor *blueColorMainAppUIColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"blue_bg_full.png"]];
            customColorView.backgroundColor=blueColorMainAppUIColor;
            cell.selectedBackgroundView =  customColorView;
        }
    }
    else if([userlevel isEqualToString:@"PREMIUM GUARD MODE"])
    {
        if(indexPath.section==1&&(indexPath.row==1||indexPath.row==2))
        {
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.leftMenuItemName.textColor=[UIColor lightGrayColor];
        }
        else
        {
            UIColor *blueColorMainAppUIColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"blue_bg_full.png"]];
            customColorView.backgroundColor=blueColorMainAppUIColor;
            cell.selectedBackgroundView =  customColorView;
        }


    }
    else if([userlevel isEqualToString:@"AUTO PREMIUM SAFE GUARD MODE"])
    {
      
            cell.leftMenuItemName.textColor=[UIColor darkGrayColor];
        
        UIColor *blueColorMainAppUIColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"blue_bg_full.png"]];
     
        customColorView.backgroundColor=blueColorMainAppUIColor;
        cell.selectedBackgroundView =  customColorView;

    }
    else
    {
       cell.leftMenuItemName.textColor=[UIColor darkGrayColor];
        UIColor *blueColorMainAppUIColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"blue_bg_full.png"]];
        /*customColorView.backgroundColor = [UIColor colorWithRed:180/255.0
         green:138/255.0
         blue:171/255.0
         alpha:0.5];*/
        customColorView.backgroundColor=blueColorMainAppUIColor;
        cell.selectedBackgroundView =  customColorView;
        

    }
         return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    NSDictionary *userSelectedMenuNSDict=self.leftMenuSideItemsNSArr[indexPath.section][indexPath.row];
    
    NSString * storyBoardIdStr = [userSelectedMenuNSDict objectForKey:@"storyboard_id"];
   if(![storyBoardIdStr isEqualToString:@"Standalone_mode_id"])
   {
    if([userlevel isEqualToString:@"SAFE GUARD MODE"])
    {
        if(indexPath.section==1&&(indexPath.row==1||indexPath.row==2))
        {
        
        }
        else
        {
            self.slidingViewController.topViewController.view.layer.transform = CATransform3DMakeScale(1, 1, 1);
            self.slidingViewController.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:storyBoardIdStr];
            [self.slidingViewController resetTopViewAnimated:true];
        }
    }
   else if([userlevel isEqualToString:@"PREMIUM GUARD MODE"])
    {
        if(indexPath.section==1&&(indexPath.row==1||indexPath.row==2))
        {
            
        }
        else
        {
            self.slidingViewController.topViewController.view.layer.transform = CATransform3DMakeScale(1, 1, 1);
            self.slidingViewController.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:storyBoardIdStr];
            [self.slidingViewController resetTopViewAnimated:true];
        }
    }
   else if([userlevel isEqualToString:@"AUTO PREMIUM SAFE GUARD MODE"])
   {
      
       
           self.slidingViewController.topViewController.view.layer.transform = CATransform3DMakeScale(1, 1, 1);
           self.slidingViewController.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:storyBoardIdStr];
           [self.slidingViewController resetTopViewAnimated:true];
   }
   else
   {
      
           self.slidingViewController.topViewController.view.layer.transform = CATransform3DMakeScale(1, 1, 1);
           self.slidingViewController.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:storyBoardIdStr];
           [self.slidingViewController resetTopViewAnimated:true];
       
   }
   }
    else
    {
         [self  fetchingpatientdata];
         GlobalVars *globals = [GlobalVars sharedInstance];
        NSLog(@"%@",globals.mcd_bluetoothstatus);
        if([globals.mcd_bluetoothstatus isEqualToString:@"1"]||globals.mcd_bluetoothstatus.length==0)
        {
            
            [self presentAlertViewForPassword];
     
        }
        else
        {
           
            SDCAlertView *alertView = [[SDCAlertView alloc] initWithTitle:@"Message"
                                                                  message:@"Please disconnect mcd before reset"
                                                                 delegate:self
                                                        cancelButtonTitle:@"Ok"
                                                        otherButtonTitles:nil,nil];
            [alertView setAlertViewStyle:SDCAlertViewStyleDefault];
            
            spinner = [[UIActivityIndicatorView alloc] init];
            spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
            [spinner setTranslatesAutoresizingMaskIntoConstraints:NO];
            //[spinner startAnimating];
            [alertView.contentView addSubview:spinner];
            [spinner sdc_horizontallyCenterInSuperviewWithOffset:-60.0];
            [spinner sdc_verticallyCenterInSuperviewWithOffset:SDCAutoLayoutStandardSiblingDistance];
            [alertView show];
        }
        
    }
    
    if(indexPath.section==0)
    {
        if(indexPath.row==2)
          badgecnt=Nil;
        [self.leftMenuTableView reloadData];
    }
    
}




- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView * sectionMainView ;
    sectionMainView.backgroundColor = UIColor.whiteColor;
    
    if (section > 0)
    {
        //let widthIs = tableViewCellCustom.frame.width
        // Light Gray Line
        UIView * dividerLine            = [[UIView alloc]initWithFrame:CGRectMake(20, 0, 235, 1)];
         dividerLine.backgroundColor     =   [UIColor lightGrayColor];
         dividerLine.alpha               =   0.6;
         //dividerLine.setTranslatesAutoresizingMaskIntoConstraints(false)
         [sectionMainView addSubview: dividerLine];
        
        
        //self.setUpConstrain(self.view, lineView: sectionMainView)
        //self.setUpConstrain(sectionMainView, lineView: dividerLine)
    }
    return sectionMainView;
}




- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (section > 0)
    {
        return 1;
    }
    return 0;
}

- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}


/*- (BOOL)alertView:(SDCAlertView *)alertView shouldDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==1)
    {
        DashboardViewController *dashbrd=[DashboardViewController alloc];
        [dashbrd updateservicestatus];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:EAAccessoryDidConnectNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:EADSessionDataReceivedNotification object:nil];
        
        EADSessionController *sessionController = [EADSessionController sharedController];
        
        [sessionController closeSession];
        [self resetAllData];
       
    }
    return YES;
}*/



- (void) resetAllData
{
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
        [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
        
        AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        [appDelegate resetCoreData];
        [appDelegate managedObjectContext];
    
   
      [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    //UIWindow *window=[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Mainstoryboard_iPad" bundle:nil];
        OneTimeViewController *resultsInListViewController = (OneTimeViewController *)[mainStoryboard instantiateViewControllerWithIdentifier:@"OneTimeViewController"];
        
        [appDelegate.window setRootViewController:resultsInListViewController];
        
      //  [appDelegate.window makeKeyAndVisible];
        
       // [appDelegate managedObjectContext];
    }
    else
    {
        
       
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        
        OneTimeViewController *resultsInListViewController = (OneTimeViewController *)[mainStoryboard instantiateViewControllerWithIdentifier:@"OneTimeViewController"];
        
        [appDelegate.window setRootViewController:resultsInListViewController];
        
        [appDelegate.window makeKeyAndVisible];

       
       // [self resetCoreData];
       // [appDelegate managedObjectContext];
    }
    
    
}

- (void) resetCoreData
{
    
     NSURL *tempUrl =  [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    //NSError *error=nil;
    NSURL *storeURL = [tempUrl URLByAppendingPathComponent:@"HealthWatchiOS.sqlite"];
    [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil];
    for (NSManagedObject *ct in [self.managedObjectContext registeredObjects]) {
        [self.managedObjectContext deleteObject:ct];
    }
    
    [[[UIApplication sharedApplication] scheduledLocalNotifications] enumerateObjectsUsingBlock:^(UILocalNotification *notification, NSUInteger idx, BOOL *stop) {
        NSLog(@"Notification %lu: %@",(unsigned long)idx, notification);
        [[UIApplication sharedApplication] cancelLocalNotification:notification] ;
    }];
    
    
}



- (void)presentAlertViewForPassword
{
    
    if(!standalone_Mode)
    {
        SDCAlertView *alert = [[SDCAlertView alloc] initWithTitle:@""
                                                          message:@"Please enter user name and password"
                                                         delegate:self
                                                cancelButtonTitle:@"Cancel"
                                                otherButtonTitles:@"OK", nil];
        [alert setAlertViewStyle:SDCAlertViewStyleLoginAndPasswordInput];
        [alert setTitle:@"Login"];
        [[alert textFieldAtIndex:0] setPlaceholder:@"User name"];
        [alert textFieldAtIndex:0].autocorrectionType = UITextAutocorrectionTypeNo;
        
        self.spinner = [[UIActivityIndicatorView alloc] init];
        self.spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
        [self.spinner setTranslatesAutoresizingMaskIntoConstraints:NO];
        //[spinner startAnimating];
        [alert.contentView addSubview:spinner];
        [spinner sdc_horizontallyCenterInSuperviewWithOffset:-60.0];
        [spinner sdc_verticallyCenterInSuperviewWithOffset:SDCAutoLayoutStandardSiblingDistance];
        
        
        
        lblspin = [[UILabel alloc] init];
        [lblspin setFont:[UIFont systemFontOfSize:12]];
        [lblspin setTranslatesAutoresizingMaskIntoConstraints:NO];
        [alert.contentView addSubview:lblspin];
        [lblspin sdc_horizontallyCenterInSuperview];
        [lblspin sdc_verticallyCenterInSuperviewWithOffset:SDCAutoLayoutStandardSiblingDistance];
        
        
        [alert show];
    }
    else
    {
        DoctorProfileViewController *doctorprofile=[[DoctorProfileViewController alloc]init];
        BOOL checkDocDetail=[doctorprofile UpdatedoctorPatient_rec];
        NSString *mssg;
        if(checkDocDetail)
        {
            
            mssg =@"In order to change this setting, the complete doctor details & patient details must be entered under User Screen and Doctor Screen";
            SDCAlertView *alert = [[SDCAlertView alloc] initWithTitle:@"Validation"
                                                              message:mssg
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                                    otherButtonTitles:@"OK", nil];
            [alert setAlertViewStyle:SDCAlertViewStyleDefault];
            [alert show];
            
        }
        else
        {
            NSString *newstr=@"Please enter your name";
            mssg =[NSString stringWithFormat:@"These settings should only be changed by a qualified physician. By approving this message you approve that you are the attending physician\r\r%@",newstr];
            SDCAlertView *alert = [[SDCAlertView alloc] initWithTitle:@"Login"
                                                              message:mssg
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                                    otherButtonTitles:@"OK", nil];
            [alert setAlertViewStyle:SDCAlertViewStylePlainTextInput];
            [alert textFieldAtIndex:0].autocorrectionType = UITextAutocorrectionTypeNo;
            
            self.spinner = [[UIActivityIndicatorView alloc] init];
            self.spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
            [self.spinner setTranslatesAutoresizingMaskIntoConstraints:NO];
            //[spinner startAnimating];
            [alert.contentView addSubview:spinner];
            [spinner sdc_horizontallyCenterInSuperviewWithOffset:-60.0];
            [spinner sdc_verticallyCenterInSuperviewWithOffset:SDCAutoLayoutStandardSiblingDistance];
            
            
            
            lblspin = [[UILabel alloc] init];
            [lblspin setTranslatesAutoresizingMaskIntoConstraints:NO];
            [alert.contentView addSubview:lblspin];
            [lblspin sdc_horizontallyCenterInSuperview];
            [lblspin sdc_verticallyCenterInSuperviewWithOffset:SDCAutoLayoutStandardSiblingDistance];
            
            
            [alert show];
        }
    }
    
    
    
}


- (BOOL)alertView:(SDCAlertView *)alertView shouldDismissWithButtonIndex:(NSInteger)buttonIndex {
   // GlobalVars *global = [GlobalVars sharedInstance];
    if(!standalone_Mode)
    {
        lblspin.text=@"";
        if(buttonIndex==1)
        {
            
            if(!alertflag)
            {
            [self.spinner startAnimating];
            UITextField *username = [alertView textFieldAtIndex:0];
            NSLog(@"username: %@", username.text);
            
            
            UITextField *password = [alertView textFieldAtIndex:1];
            NSLog(@"password: %@", password.text);
            
            
            if(username.text.length == 0 || password.text.length == 0) //check your two textflied has value
            {
                [spinner stopAnimating];
                direction = 1;
                shakes = 0;
                AudioServicesPlaySystemSound (1352);
                [self shake:alertView];
                NSLog(@"EMPTY");
                return NO;
            }
            else
            {
                
                lblspin.text=@"Validating...";
                NSString *domainname =[[NSUserDefaults standardUserDefaults] stringForKey:@"DomainName"];
                
                NSString * Fetchingpath=[NSString stringWithFormat:@"https://%@/MCC/MCCWcfService.svc/CargiverAuthentication/?Username=%@&Password=%@&patientId=%@",domainname,username.text,password.text,patientid];
                NSString *bookurl=[Fetchingpath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                NSURL *URL1=[NSURL URLWithString:bookurl];
                
                NSLog(@"url%@",URL1);
                
                NSURLRequest *request = [NSURLRequest requestWithURL:URL1];
                
                
                
                AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
                
                operation.responseSerializer = [AFHTTPResponseSerializer serializer];
                
                
                [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                    
                    NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                    NSLog(@"urlresp%@",string);
                    
                    if([string containsString:@"User Login successfully"])
                    {
                        
                        lblspin.text=@"OK";
                        [spinner stopAnimating];
                        
                        [self dismiss:alertView];
                        
                        alertflag =true;
                        SDCAlertView *alertView = [[SDCAlertView alloc] initWithTitle:@"Message"
                                                                              message:@"Are you sure you want to exit mode"
                                                                             delegate:self
                                                                    cancelButtonTitle:@"Cancel"
                                                                    otherButtonTitles:@"Ok",nil];
                        [alertView setAlertViewStyle:SDCAlertViewStyleDefault];
                        
                        spinner = [[UIActivityIndicatorView alloc] init];
                        spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
                        [spinner setTranslatesAutoresizingMaskIntoConstraints:NO];
                        //[spinner startAnimating];
                        [alertView.contentView addSubview:spinner];
                        [spinner sdc_horizontallyCenterInSuperviewWithOffset:-60.0];
                        [spinner sdc_verticallyCenterInSuperviewWithOffset:SDCAutoLayoutStandardSiblingDistance];
                        [alertView show];
                      
                        
                    }
                    else
                    {
                        AudioServicesPlaySystemSound (1352);
                        string = [string stringByReplacingOccurrencesOfString:@"\"" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [string length])];
                        if([string containsString:@"UserName or password is incorrect"])
                        {
                            lblspin.text= @"The user name or password is incorrect";
                        }
                        else if([string containsString:@"Invalid User"])
                        {
                            lblspin.text= @"Invalid user";
                        }
                        else
                            lblspin.text= string;
                        [spinner stopAnimating];
                        direction = 1;
                        shakes = 0;
                        [self shake:alertView];
                        [alertView textFieldAtIndex:1].text=@"";
                        [alertView textFieldAtIndex:0].text=@"";
                    }
                    
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error Retrieving doctors profile"
                                                                        message:[error localizedDescription]
                                                                       delegate:nil
                                                              cancelButtonTitle:@"Ok"
                                                              otherButtonTitles:nil];
                    
                    
                    [alertView show];
                    
                    [spinner stopAnimating];
                }];
                
                
                [operation start];
                //[operation waitUntilFinished];
                
                return[[operation responseString]boolValue];
                //return retvalue;
                
                
            }
            
            }
            else
            {
                alertflag =false;
                 [self dismiss:alertView];
                DashboardViewController *dashbrd=[DashboardViewController alloc];
                [dashbrd updateservicestatus];
                
                [[NSNotificationCenter defaultCenter] removeObserver:self name:EAAccessoryDidConnectNotification object:nil];
                [[NSNotificationCenter defaultCenter] removeObserver:self name:EADSessionDataReceivedNotification object:nil];
                
                EADSessionController *sessionController = [EADSessionController sharedController];
                
                [sessionController closeSession];
                [self resetAllData];
            }
        }
        else
        {
             alertflag =false;
            return YES;
        }
    }
    else
    {
        if(buttonIndex==0)
        {
            alertflag =false;
            return YES;
        }
        lblspin.text=@"";
        
        if(buttonIndex==1)
        {
            if(!alertflag)
            {
            [self.spinner startAnimating];
            UITextField *Docname = [alertView textFieldAtIndex:0];
            NSLog(@"Docname: %@", Docname.text);
            
            
            
            
            if(Docname.text.length == 0) //check your two textflied has value
            {
                [spinner stopAnimating];
                direction = 1;
                shakes = 0;
                AudioServicesPlaySystemSound (1352);
                [self shake:alertView];
                NSLog(@"EMPTY");
                
                return NO;
            }
            else
            {
                
                lblspin.text=@"Validating...";
                
                
                if([Docname.text isEqualToString:doctorname])
                {
                    lblspin.text=@"OK";
                    [spinner stopAnimating];
                    
                   
                    
                    [self dismiss:alertView];
                    alertflag =true;
                    SDCAlertView *alertView = [[SDCAlertView alloc] initWithTitle:@"Message"
                                                                          message:@"Are you sure you want to exit mode"
                                                                         delegate:self
                                                                cancelButtonTitle:@"Cancel"
                                                                otherButtonTitles:@"Ok",nil];
                    [alertView setAlertViewStyle:SDCAlertViewStyleDefault];
                    
                    spinner = [[UIActivityIndicatorView alloc] init];
                    spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
                    [spinner setTranslatesAutoresizingMaskIntoConstraints:NO];
                    //[spinner startAnimating];
                    [alertView.contentView addSubview:spinner];
                    [spinner sdc_horizontallyCenterInSuperviewWithOffset:-60.0];
                    [spinner sdc_verticallyCenterInSuperviewWithOffset:SDCAutoLayoutStandardSiblingDistance];
                    [alertView show];
                    
                }
                else
                {
                    
                    AudioServicesPlaySystemSound (1352);
                    lblspin.text=@"Invalid";
                    [spinner stopAnimating];
                    direction = 1;
                    shakes = 0;
                    [self shake:alertView];
                    [alertView textFieldAtIndex:0].text=@"";
                }
                
                
            }
            
            }
            else
            {
                alertflag =false;
                [self dismiss:alertView];
                DashboardViewController *dashbrd=[DashboardViewController alloc];
                [dashbrd updateservicestatus];
                
                [[NSNotificationCenter defaultCenter] removeObserver:self name:EAAccessoryDidConnectNotification object:nil];
                [[NSNotificationCenter defaultCenter] removeObserver:self name:EADSessionDataReceivedNotification object:nil];
                
                EADSessionController *sessionController = [EADSessionController sharedController];
                
                [sessionController closeSession];
                [self resetAllData];
            }
        }
        else
        {
             alertflag =false;
            return YES;
        }
        
    }
    
    return NO;
}

-(void)dismiss:(SDCAlertView*)alert
{
    [alert dismissWithClickedButtonIndex:0 animated:YES];
}
- (BOOL)alertView:(SDCAlertView *)alertView shouldDeselectButtonAtIndex:(NSInteger)buttonIndex {
    return YES;
}

-(void)fetchingpatientdata
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"PatientDetails" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
        // NSLog(@"%@", result);
        if (result.count > 0) {
            NSManagedObject *patient = (NSManagedObject *)[result objectAtIndex:0];
            //NSLog(@"1 - %@", patient);
            patientid=[patient valueForKey:@"patient_id"];
            deviceid=[patient valueForKey:@"device_id"];
            doctorname=[patient valueForKey:@"doc_name"];
        }
    }
}


-(void)shake:(SDCAlertView *)theOneYouWannaShake
{
    [UIView animateWithDuration:0.03 animations:^
     {
         theOneYouWannaShake.transform = CGAffineTransformMakeTranslation(5*direction, 0);
     }
                     completion:^(BOOL finished)
     {
         if(shakes >= 10)
         {
             theOneYouWannaShake.transform = CGAffineTransformIdentity;
             return;
         }
         shakes++;
         direction = direction * -1;
         [self shake:theOneYouWannaShake];
     }];
}



@end
