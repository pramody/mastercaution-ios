//
//  ViewRecordedEcgViewController.h
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 24/06/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//


#import "iCarousel.h"
#import <MessageUI/MessageUI.h>

#import "SMTPLibrary/SKPSMTPMessage.h"
#import <CFNetwork/CFNetwork.h>
#import "NSData+Base64Additions.h"
#import <CoreText/CoreText.h>
#import "ReaderViewController.h"
#import "CustomIndicatorView.h"


@interface ViewRecordedEcgViewController :UIViewController<SKPSMTPMessageDelegate,iCarouselDataSource, iCarouselDelegate,UIScrollViewDelegate,ReaderViewControllerDelegate>

{
    
    
    UIScrollView *scrollView;
    UIImage *screenShot;
     SKPSMTPState HighestState;
     NSInteger leadWidth;
    
    
}



@property (nonatomic, strong) IBOutlet UIView *vwProfile, *vwMonitor;
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet UILabel *lbDevice;
@property (nonatomic, strong) IBOutlet NSString *ecgpath;
@property (nonatomic, strong) IBOutlet iCarousel *carousel;
@property (nonatomic, strong) IBOutlet UIImageView *postureimage;


@property (nonatomic, strong) IBOutlet NSString *HR1;
@property (nonatomic, strong) IBOutlet NSString *Resprate;
@property (nonatomic, strong) IBOutlet NSString *Temp;
@property (nonatomic, strong) IBOutlet NSString *RR;
@property (nonatomic, strong) IBOutlet NSString *PR;
@property (nonatomic, strong) IBOutlet NSString *QRS;
@property (nonatomic, strong) IBOutlet NSString *QT;
@property (nonatomic, strong) IBOutlet NSString *QTC;
@property (nonatomic, strong) IBOutlet NSString *Posture;

@property (nonatomic, strong) IBOutlet NSString *Eventcode;
@property (nonatomic, strong) IBOutlet NSString *EventDateTime;
@property (nonatomic, strong) IBOutlet NSString *EventValue;

@property (nonatomic, strong)IBOutlet CustomIndicatorView *spinnerView;

@property (nonatomic, strong)IBOutlet ReaderViewController *_readerViewController;

@property (nonatomic, strong) IBOutlet UIButton *btnSendEmail;

- (void)setUp;
-(void)plotting :(NSString *)event_code :(NSString *)event_date_time :(NSString *)event_value :(NSString *)ecg_path;
-(void)plotting1 :(NSString *)event_code :(NSString *)event_date_time :(NSString *)event_value :(NSString *)ecg_path;
@property (nonatomic, strong) IBOutlet SKPSMTPMessage *test_smtp_message;

@end
