//
//  BuiltInTestViewController.m
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 17/06/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import "BuiltInTestViewController.h"
#import "ConfigureMCDController.h"

@interface BuiltInTestViewController ()

@end

@implementation BuiltInTestViewController
@synthesize btnleadRA,btnleadLA,btnleadLL,btnleadRL,btnleadV1,btnleadV2,btnleadV3,btnleadV4,btnleadV5,btnleadV6,btnleadV7,btnleadV3R,btnleadV4R,btnaccelorometer,btnbluetooth,btncloud,btninfrared,btnmcdbattery,btnsdcard,accelorometer,infrared,bluetooth,sdcard,cloud,mcdbattery;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(Updatinglead) name:@"LeadDataUpdated" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(built_value) name:@"BuiltinValue" object:nil];
   
   
}


- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    Greencolor = [UIColor colorWithRed:(137/255.0) green:(197/255.0) blue:(6/255.0) alpha:1];
     Redcolor = [UIColor colorWithRed:(255/255.0) green:(0/255.0) blue:(0/255.0) alpha:1];
    ConfigureMCDController  *config=[[ConfigureMCDController alloc]init];
    [config  Get_BuiltInTestREQ];
    [self Updatinglead];
    [self built_value];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSLogger *logger=[[NSLogger alloc]init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"MCDSettings" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
        logger.degbugger = true;
        [logger log:@"Coredata Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:[error localizedDescription], @"Adding clinical record", nil] error:TRUE];
        
    } else {
        
        if (result.count > 0) {
            NSManagedObject *mcdsett = (NSManagedObject *)[result objectAtIndex:0];
            
            
            switch ( [[mcdsett valueForKey:@"lead_config"]intValue]) {
                case 0:
                    btnleadV1.alpha=0;
                    btnleadV2.alpha=0;
                    btnleadV3.alpha=0;
                    btnleadV4.alpha=0;
                    btnleadV5.alpha=0;
                    btnleadV6.alpha=0;
                    btnleadV7.alpha=0;
                    btnleadV3R.alpha=0;
                    btnleadV4R.alpha=0;
                    
                    break;
                case 1:
                    btnleadV1.alpha=1;
                    btnleadV2.alpha=0;
                    btnleadV3.alpha=0;
                    btnleadV4.alpha=0;
                    btnleadV5.alpha=0;
                    btnleadV6.alpha=0;
                    btnleadV7.alpha=0;
                    btnleadV3R.alpha=0;
                    btnleadV4R.alpha=0;
                    break;
                case 2:
                    btnleadV1.alpha=1;
                    btnleadV2.alpha=1;
                    btnleadV3.alpha=1;
                    btnleadV4.alpha=1;
                    btnleadV5.alpha=1;
                    btnleadV6.alpha=1;
                    btnleadV7.alpha=0;
                    btnleadV3R.alpha=0;
                    btnleadV4R.alpha=0;
                    break;
                case 3:
                    btnleadV1.alpha=1;
                    btnleadV2.alpha=1;
                    btnleadV3.alpha=1;
                    btnleadV4.alpha=1;
                    btnleadV5.alpha=1;
                    btnleadV6.alpha=1;
                    btnleadV7.alpha=1;
                    btnleadV3R.alpha=1;
                    btnleadV4R.alpha=1;
                    break;
                    
                default:
                    break;
            }
            
        }
    }

}

-(void)leaddataupdate
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LeadDataUpdated" object:nil];
}

-(void)Updatinglead
{
    GlobalVars *globals = [GlobalVars sharedInstance];
    NSArray *leaddata=[NSArray arrayWithArray: globals.Leaddata];
    globals.mcd_garmentstatus=@"0";
    //NSLog(@"lead%@",[globals Leaddata]);
    //Lead
    if(leaddata.count!=0)
    {
    for(int i=0;i<leaddata.count;i++)
    {
        
        switch (i) {
            case 0:
                if([[globals.Leaddata  objectAtIndex:0]intValue]==0)
                {
                    UIImage *btnImage = [UIImage imageNamed:@"lead_normal.png"];
                    [btnleadRA setImage:btnImage forState:UIControlStateNormal];
                    [btnleadRA setTitleColor:Greencolor  forState:UIControlStateNormal];
                }
                else
                {
                    UIImage *btnImage = [UIImage imageNamed:@"lead_error.png"];
                    [btnleadRA setImage:btnImage forState:UIControlStateNormal];
                    [btnleadRA setTitleColor:Redcolor forState:UIControlStateNormal];
                }
                break;
            case 1:
                if([[globals.Leaddata  objectAtIndex:1]intValue]==0)
                {
                    UIImage *btnImage = [UIImage imageNamed:@"lead_normal.png"];
                    [btnleadLA setImage:btnImage forState:UIControlStateNormal];
                    [btnleadLA setTitleColor:Greencolor forState:UIControlStateNormal];
                }
                else
                {
                    UIImage *btnImage = [UIImage imageNamed:@"lead_error.png"];
                    [btnleadLA setImage:btnImage forState:UIControlStateNormal];
                    [btnleadLA setTitleColor:Redcolor forState:UIControlStateNormal];
                }
                break;
            case 2:
                if([[globals.Leaddata  objectAtIndex:2]intValue]==0)
                {
                    UIImage *btnImage = [UIImage imageNamed:@"lead_normal.png"];
                    [btnleadLL setImage:btnImage forState:UIControlStateNormal];
                    [btnleadLL setTitleColor:Greencolor forState:UIControlStateNormal];
                }
                else
                {
                    UIImage *btnImage = [UIImage imageNamed:@"lead_error.png"];
                    [btnleadLL setImage:btnImage forState:UIControlStateNormal];
                    [btnleadLL setTitleColor:Redcolor forState:UIControlStateNormal];
                }
                break;
            case 3:
                if([[globals.Leaddata  objectAtIndex:3]intValue]==0)
                {
                    UIImage *btnImage = [UIImage imageNamed:@"lead_normal.png"];
                    [btnleadRL setImage:btnImage forState:UIControlStateNormal];
                    [btnleadRL setTitleColor:Greencolor forState:UIControlStateNormal];
                }
                else
                {
                    UIImage *btnImage = [UIImage imageNamed:@"lead_error.png"];
                    [btnleadRL setImage:btnImage forState:UIControlStateNormal];
                    [btnleadRL setTitleColor:Redcolor forState:UIControlStateNormal];
                }
                break;
            case 4:
                if([[globals.Leaddata  objectAtIndex:4]intValue]==0)
                {
                    UIImage *btnImage = [UIImage imageNamed:@"lead_normal.png"];
                    [btnleadV1 setImage:btnImage forState:UIControlStateNormal];
                    [btnleadV1 setTitleColor:Greencolor forState:UIControlStateNormal];
                }
                else
                {
                    UIImage *btnImage = [UIImage imageNamed:@"lead_error.png"];
                    [btnleadV1 setImage:btnImage forState:UIControlStateNormal];
                    [btnleadV1 setTitleColor:Redcolor forState:UIControlStateNormal];
                }
                break;
            case 5:
                if([[globals.Leaddata  objectAtIndex:5]intValue]==0)
                {
                    UIImage *btnImage = [UIImage imageNamed:@"lead_normal.png"];
                    [btnleadV2 setImage:btnImage forState:UIControlStateNormal];
                    [btnleadV2 setTitleColor:Greencolor forState:UIControlStateNormal];
                }
                else
                {
                    UIImage *btnImage = [UIImage imageNamed:@"lead_error.png"];
                    [btnleadV2 setImage:btnImage forState:UIControlStateNormal];
                    [btnleadV2 setTitleColor:Redcolor forState:UIControlStateNormal];
                }
                break;
            case 6:
                if([[globals.Leaddata  objectAtIndex:6]intValue]==0)
                {
                    UIImage *btnImage = [UIImage imageNamed:@"lead_normal.png"];
                    [btnleadV3 setImage:btnImage forState:UIControlStateNormal];
                    [btnleadV3 setTitleColor:Greencolor forState:UIControlStateNormal];
                }
                else
                {
                    UIImage *btnImage = [UIImage imageNamed:@"lead_error.png"];
                    [btnleadV3 setImage:btnImage forState:UIControlStateNormal];
                    [btnleadV3 setTitleColor:Redcolor forState:UIControlStateNormal];
                }
                break;
            case 7:
                if([[globals.Leaddata  objectAtIndex:7]intValue]==0)
                {
                    UIImage *btnImage = [UIImage imageNamed:@"lead_normal.png"];
                    [btnleadV4 setImage:btnImage forState:UIControlStateNormal];
                    [btnleadV4 setTitleColor:Greencolor forState:UIControlStateNormal];
                }
                else
                {
                    UIImage *btnImage = [UIImage imageNamed:@"lead_error.png"];
                    [btnleadV4 setImage:btnImage forState:UIControlStateNormal];
                    [btnleadV4 setTitleColor:Redcolor forState:UIControlStateNormal];
                }
                break;
            case 8:
                if([[globals.Leaddata  objectAtIndex:8]intValue]==0)
                {
                    UIImage *btnImage = [UIImage imageNamed:@"lead_normal.png"];
                    [btnleadV5 setImage:btnImage forState:UIControlStateNormal];
                    [btnleadV5 setTitleColor:Greencolor forState:UIControlStateNormal];
                }
                else
                {
                    UIImage *btnImage = [UIImage imageNamed:@"lead_error.png"];
                    [btnleadV5 setImage:btnImage forState:UIControlStateNormal];
                    [btnleadV5 setTitleColor:Redcolor forState:UIControlStateNormal];
                }
                break;
            case 9:
                if([[globals.Leaddata  objectAtIndex:9]intValue]==0)
                {
                    UIImage *btnImage = [UIImage imageNamed:@"lead_normal.png"];
                    [btnleadV6 setImage:btnImage forState:UIControlStateNormal];
                    [btnleadV6 setTitleColor:Greencolor forState:UIControlStateNormal];
                }
                else
                {
                    UIImage *btnImage = [UIImage imageNamed:@"lead_error.png"];
                    [btnleadV6 setImage:btnImage forState:UIControlStateNormal];
                    [btnleadV6 setTitleColor:Redcolor forState:UIControlStateNormal];
                }
                break;
            case 10:
                if([[globals.Leaddata  objectAtIndex:9]intValue]==0)
                {
                    UIImage *btnImage = [UIImage imageNamed:@"lead_normal.png"];
                    [btnleadV7 setImage:btnImage forState:UIControlStateNormal];
                    [btnleadV7 setTitleColor:Greencolor forState:UIControlStateNormal];
                }
                else
                {
                    UIImage *btnImage = [UIImage imageNamed:@"lead_error.png"];
                    [btnleadV7 setImage:btnImage forState:UIControlStateNormal];
                    [btnleadV7 setTitleColor:Redcolor forState:UIControlStateNormal];
                }
                break;
            case 11:
                if([[globals.Leaddata  objectAtIndex:9]intValue]==0)
                {
                    UIImage *btnImage = [UIImage imageNamed:@"lead_normal.png"];
                    [btnleadV3R setImage:btnImage forState:UIControlStateNormal];
                    [btnleadV3R setTitleColor:Greencolor forState:UIControlStateNormal];
                }
                else
                {
                    UIImage *btnImage = [UIImage imageNamed:@"lead_error.png"];
                    [btnleadV3R setImage:btnImage forState:UIControlStateNormal];
                    [btnleadV3R setTitleColor:Redcolor forState:UIControlStateNormal];
                }
                break;
            case 12:
                if([[globals.Leaddata  objectAtIndex:9]intValue]==0)
                {
                    UIImage *btnImage = [UIImage imageNamed:@"lead_normal.png"];
                    [btnleadV4R setImage:btnImage forState:UIControlStateNormal];
                    [btnleadV4R setTitleColor:Greencolor forState:UIControlStateNormal];
                }
                else
                {
                    UIImage *btnImage = [UIImage imageNamed:@"lead_error.png"];
                    [btnleadV4R setImage:btnImage forState:UIControlStateNormal];
                    [btnleadV4R setTitleColor:Redcolor forState:UIControlStateNormal];
                }
                break;
                
            default:
                break;
        }
        
         if([[globals.Leaddata  objectAtIndex:i]intValue]==1)
         {
             globals.mcd_garmentstatus=@"1";
         }
        
    }
  }
  else
  {
      
          UIImage *btnImage = [UIImage imageNamed:@"lead_error.png"];
          [btnleadRA setImage:btnImage forState:UIControlStateNormal];
          [btnleadRA setTitleColor:Redcolor forState:UIControlStateNormal];
      
      
      
          [btnleadLA setImage:btnImage forState:UIControlStateNormal];
          [btnleadLA setTitleColor:Redcolor forState:UIControlStateNormal];
    
          [btnleadLL setImage:btnImage forState:UIControlStateNormal];
          [btnleadLL setTitleColor:Redcolor forState:UIControlStateNormal];
     
          [btnleadRL setImage:btnImage forState:UIControlStateNormal];
          [btnleadRL setTitleColor:Redcolor forState:UIControlStateNormal];
    
      
          [btnleadV1 setImage:btnImage forState:UIControlStateNormal];
          [btnleadV1 setTitleColor:Redcolor forState:UIControlStateNormal];
     
          [btnleadV2 setImage:btnImage forState:UIControlStateNormal];
          [btnleadV2 setTitleColor:Redcolor forState:UIControlStateNormal];
   
          [btnleadV3 setImage:btnImage forState:UIControlStateNormal];
          [btnleadV3 setTitleColor:Redcolor forState:UIControlStateNormal];
     
          [btnleadV4 setImage:btnImage forState:UIControlStateNormal];
          [btnleadV4 setTitleColor:Redcolor forState:UIControlStateNormal];
    
          [btnleadV5 setImage:btnImage forState:UIControlStateNormal];
          [btnleadV5 setTitleColor:Redcolor forState:UIControlStateNormal];
    
          [btnleadV6 setImage:btnImage forState:UIControlStateNormal];
          [btnleadV6 setTitleColor:Redcolor forState:UIControlStateNormal];
      
      
          [btnleadV7 setImage:btnImage forState:UIControlStateNormal];
          [btnleadV7 setTitleColor:Redcolor forState:UIControlStateNormal];
      
          [btnleadV3R setImage:btnImage forState:UIControlStateNormal];
          [btnleadV3R setTitleColor:Redcolor forState:UIControlStateNormal];
      
          [btnleadV4R setImage:btnImage forState:UIControlStateNormal];
          [btnleadV4R setTitleColor:Redcolor forState:UIControlStateNormal];
}
    
   // NSLog(@"%@,%@,%@,%@",globals.built_accelerometer,globals.built_sdcard,globals.built_battery,globals.built_temperature);
    

}

-(void)Builtindataupdate
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"BuiltinValue" object:nil];
}


-(void)built_value
{
    GlobalVars *globals = [GlobalVars sharedInstance];
    if([globals.built_sdcard intValue]==1)
    {
        
        UIImage *coreimage=[UIImage imageNamed:@"sdcard_normal.png"];
        sdcard.image=coreimage;
        UIImage *btnImage = [UIImage imageNamed:@"check_normal.png"];
        [ btnsdcard setImage:btnImage forState:UIControlStateNormal];
    }
    else
    {
        UIImage *coreimage=[UIImage imageNamed:@"sdcard_error.png"];
        sdcard.image=coreimage;
        UIImage *btnImage = [UIImage imageNamed:@"check_error.png"];
        [ btnsdcard setImage:btnImage forState:UIControlStateNormal];
    }
    
    
    if([globals.built_battery intValue]==1)
    {
        
        UIImage *coreimage=[UIImage imageNamed:@"battery_normal.png"];
        mcdbattery.image=coreimage;
        UIImage *btnImage = [UIImage imageNamed:@"check_normal.png"];
        [ btnmcdbattery setImage:btnImage forState:UIControlStateNormal];
    }
    else
    {
        UIImage *coreimage=[UIImage imageNamed:@"battery_error.png"];
        mcdbattery.image=coreimage;
        UIImage *btnImage = [UIImage imageNamed:@"check_error.png"];
        [ btnmcdbattery setImage:btnImage forState:UIControlStateNormal];
    }
    
    if([globals.built_bluetooth intValue]==1)
    {
        
        UIImage *coreimage=[UIImage imageNamed:@"bluetooth_normal.png"];
        bluetooth.image=coreimage;
        UIImage *btnImage = [UIImage imageNamed:@"check_normal.png"];
        [ btnbluetooth setImage:btnImage forState:UIControlStateNormal];
    }
    else
    {
        UIImage *coreimage=[UIImage imageNamed:@"bluetooth_error.png"];
        bluetooth.image=coreimage;
        UIImage *btnImage = [UIImage imageNamed:@"check_error.png"];
        [ btnbluetooth setImage:btnImage forState:UIControlStateNormal];
    }
    if([globals.mcd_cloudstatus intValue]==0)
    {
    
        UIImage *coreimage=[UIImage imageNamed:@"cloud_normal.png"];
        cloud.image=coreimage;
        UIImage *btnImage = [UIImage imageNamed:@"check_normal.png"];
        [ btncloud setImage:btnImage forState:UIControlStateNormal];
    }
    else
    {
        UIImage *coreimage=[UIImage imageNamed:@"cloud_error.png"];
        cloud.image=coreimage;
        UIImage *btnImage = [UIImage imageNamed:@"check_error.png"];
        [ btncloud setImage:btnImage forState:UIControlStateNormal];
    }
    if([globals.built_accelerometer intValue]==1)
    {
        
        UIImage *coreimage=[UIImage imageNamed:@"accelerometer_normal.png"];
        accelorometer.image=coreimage;
        UIImage *btnImage = [UIImage imageNamed:@"check_normal.png"];
        [ btnaccelorometer setImage:btnImage forState:UIControlStateNormal];
    }
    else
    {
        UIImage *coreimage=[UIImage imageNamed:@"accelerometer_error.png"];
        accelorometer.image=coreimage;
        UIImage *btnImage = [UIImage imageNamed:@"check_error.png"];
        [ btnaccelorometer setImage:btnImage forState:UIControlStateNormal];
    }
    if([globals.built_temperature intValue]==1)
    {
        
        UIImage *coreimage=[UIImage imageNamed:@"infrared_normal.png"];
        infrared.image=coreimage;
        UIImage *btnImage = [UIImage imageNamed:@"check_normal.png"];
        [ btninfrared setImage:btnImage forState:UIControlStateNormal];
    }
    else
    {
        UIImage *coreimage=[UIImage imageNamed:@"infrared_error.png"];
        infrared.image=coreimage;
        UIImage *btnImage = [UIImage imageNamed:@"check_error.png"];
        [ btninfrared setImage:btnImage forState:UIControlStateNormal];
    }
    
    [btnsdcard setNeedsDisplay];
    [btnmcdbattery setNeedsDisplay];
    [btnbluetooth setNeedsDisplay];
    [btnaccelorometer setNeedsDisplay];
    [btncloud setNeedsDisplay];
    [btninfrared setNeedsDisplay];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
