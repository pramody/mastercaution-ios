//
//  EventStorage.h
//  HealthWatch
//
//  Created by METSL MAC MINI on 01/09/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface EventStorage : NSManagedObject

@property (nonatomic, retain) NSString * event_datetime;
@property (nonatomic, retain) NSString * event_ecgfilepath;
@property (nonatomic, retain) NSString * event_ecg_id;
@property (nonatomic, retain) NSString * event_glucose;
@property (nonatomic, retain) NSString * event_latitude;
@property (nonatomic, retain) NSString * event_longitude;
@property (nonatomic, retain) NSString * event_reason;
@property (nonatomic, retain) NSString * event_temp;
@property (nonatomic, retain) NSString * event_value;
@property (nonatomic, retain) NSString * event_weight;
@property (nonatomic, retain) NSString * event_bpdia;
@property (nonatomic, retain) NSString * event_bpsys;
@property (nonatomic, retain) NSNumber * event_code;
@property (nonatomic, retain) NSString * event_heartrate;
@property (nonatomic, retain) NSString * event_lead_config;
@property (nonatomic, retain) NSString * event_lead_detection;
@property (nonatomic, retain) NSString * event_posture_code;
@property (nonatomic, retain) NSString * event_pr_interval;
@property (nonatomic, retain) NSString * event_qrs_interval;
@property (nonatomic, retain) NSString * event_qtc_interval;
@property (nonatomic, retain) NSString * event_qt_interval;
@property (nonatomic, retain) NSString * event_receive_complete;
@property (nonatomic, retain) NSString * event_respiration;
@property (nonatomic, retain) NSString * event_respiration_amplitude;
@property (nonatomic, retain) NSString * event_respiration_onoff;
@property (nonatomic, retain) NSString * event_resp_variance;
@property (nonatomic, retain) NSString * event_rr_interval;
@property (nonatomic, retain) NSString * event_sampling_rate;
@property (nonatomic, retain) NSString * event_thresholdtype;
@property (nonatomic, retain) NSString * event_type;
@property (nonatomic, retain) NSString * event_t_wave;
@property (nonatomic, retain) NSString * event_updated_to_device;
@property (nonatomic, retain) NSString * event_mcdlead;
@property (nonatomic, retain) NSString * event_mail_sms;
@property (nonatomic, retain) NSString * event_oximetry;

@end
