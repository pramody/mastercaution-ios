//
//  CustomUiPicker.h
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 19/06/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomUiPicker : UIPickerView
{
    
}
@property (nonatomic, strong) IBOutlet NSArray * passNsArray;
@property (nonatomic, readwrite) int tableSection;
@property (nonatomic, readwrite) int tableRow;
 


@end
