//
//  McdSettingsViewController.h
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 19/06/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//


#import "McdSettingsTableViewCell.h"

@interface McdSettingsViewController : UIViewController< ECSlidingViewControllerDelegate, UITextFieldDelegate , UITableViewDelegate, UIPickerViewDelegate,UIAlertViewDelegate>
{
    NSIndexPath * lastOpenIndexPath;
    UIColor * tableBgGrayColor;
    NSArray * filterStatusArray;
    NSArray *  mcdGainArray;
    NSArray *  mcgLeadSettingArray;
    NSArray *  mcdEventDetectionLeadArray;
    NSArray *  mcgSamplingRatesArray;
    NSArray *  mcdMuteArray;
    NSArray *  mcdRespirationArray;
    NSArray *  mcdOrientationArray;
    NSArray *  mcdbaselineArray;
     NSArray *  mcdInvertedTwave;
    NSString *  currentPickerViewOpendArray;
    UIColor *blueColorMainAppUIColor;
    
    NSString *ser_gain;
    NSString *ser_notchfil;
    NSString *ser_baseline;
    NSString *ser_pretime;
    NSString *ser_postime;
    NSString *ser_leadconfig;
    NSString *ser_lead;
    NSString *ser_samplingrte;
    
    NSString *ser_mute;
    NSString *ser_respiration;
     NSString *ser_inverted_T_wave;
    NSString *ser_orientation;
    
    UILabel *lblspin;
    NSUInteger indexpthsec;
    NSUInteger indexrw;
    NSString * patientid;
    NSString *deviceid;
     NSString *doctorname;
    NSString *caregivername;
     NSString *updatepretext;
    NSString *updateposttext;
    
    NSString *fiftyhz,*sixthz,*gain,*lead,*lead_config,*sampling_rate,*pretime,*postime,*mute,*respiration,*orientation,*baseline,*inverted_t_wave;
    
     BOOL standalone_Mode;
}

@property (nonatomic, strong) IBOutlet UIView * headerBlueView;
@property (nonatomic, strong) IBOutlet UITableView * tableViewMcdSettingsTable;
@property (nonatomic, strong) IBOutlet McdSettingsTableViewCell * tableViewMcdSettingsCell;

// Time  Intervals
@property (nonatomic, strong) IBOutlet UIView * preTimeGrayView;
@property (nonatomic, strong) IBOutlet UITextField * preTimeStepperText;
@property (nonatomic, strong) IBOutlet UIStepper * preTimeStepperControl;

@property (nonatomic, strong) IBOutlet UIView * postTimeGrayView;
@property (nonatomic, strong) IBOutlet UITextField * postTimeStepperText;
@property (nonatomic, strong) IBOutlet UIStepper * postTimeStepperControl;

// Filter Status Element
@property (nonatomic, strong) IBOutlet UIPickerView *notchhFilterStatusPickerViewControl;

@property (nonatomic, strong) IBOutlet UIPickerView * mcdGainPickerViewControl;
// MCG Lead Setting
@property (nonatomic, strong) IBOutlet UIPickerView * mcgLeadSettingPickerViewControl;

// MCD Lead Detection Setting
@property (nonatomic, strong) IBOutlet UIPickerView * mcdLeadDetectionSettingPickerViewControl;
// MCD Lead Detection Setting
@property (nonatomic, strong) IBOutlet UIPickerView * mcgSampleRatePickerViewControl;

@property (nonatomic, strong) IBOutlet UIActivityIndicatorView * spinner;

@property (nonatomic, strong) IBOutlet UIButton *  lockunlocButton;

-(IBAction)stepperChangeValueAction:(UIStepper *)sender;

- (void) UpdateMcdSetting:(int)data;
@end
