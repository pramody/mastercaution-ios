//
//  NSLogger.h
//  HealthWatch
//
//  Created by METSL MAC MINI on 14/08/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSLogger : NSObject {
}

-(void)log:(NSString *)title properties:(NSDictionary *)properties error:(BOOL)error;

-(NSURL *)logDirectory;
-(NSString *)logPrint;
-(NSData *)logData;

@property (nonatomic) BOOL degbugger;

@end