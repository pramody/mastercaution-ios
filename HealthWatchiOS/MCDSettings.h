//
//  MCDSettings.h
//  HealthWatch
//
//  Created by METSL MAC MINI on 31/07/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface MCDSettings : NSManagedObject

@property (nonatomic, retain) NSString * base_line_fil;
@property (nonatomic, retain) NSString * bt_device_add;
@property (nonatomic, retain) NSString * created_by;
@property (nonatomic, retain) NSString * created_datetime;
@property (nonatomic, retain) NSString * created_from;
@property (nonatomic, retain) NSString * fiftyhz_filter;
@property (nonatomic, retain) NSString * inverted_t_wave;
@property (nonatomic, retain) NSString * gain;
@property (nonatomic, retain) NSString * lead_config;
@property (nonatomic, retain) NSString * leads;
@property (nonatomic, retain) NSString * mcd_setdatetime;
@property (nonatomic, retain) NSString * modified_by;
@property (nonatomic, retain) NSString * modified_from;
@property (nonatomic, retain) NSString * modified_time;
@property (nonatomic, retain) NSString * orientation;
@property (nonatomic, retain) NSString * respiration;
@property (nonatomic, retain) NSString * respiration_cal;
@property (nonatomic, retain) NSString * sampling_rate;
@property (nonatomic, retain) NSString * sixtyhz_filter;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * timeinterval_post;
@property (nonatomic, retain) NSString * timeinterval_pre;
@property (nonatomic, retain) NSString * volume_level;
@property (nonatomic, retain) NSString * updated_to_site;
@property (nonatomic, retain) NSString * updated_to_mcd;
@property (nonatomic, retain) NSString * xmpp_msgid;

@end
