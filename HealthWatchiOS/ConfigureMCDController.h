//
//  ConfigureMCDController.h
//  HealthWatch
//
//  Created by METSL MAC MINI on 08/08/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//




#import <ExternalAccessory/ExternalAccessory.h>
#import "EADSessionController.h"

@class EADSessionController;
static NSString* const RWT_EVENT_STATUS_NOTIFICATION = @"EventChangedStatusNotification";
static NSString* const RWT_ACCESSORY_DISCONNECTION_NOTIFICATION = @"AccessoryDisconnectsNotification";
static NSString* const RWT_DOCTOR_LOGIN_NOTIFICATION = @"DoctorLoginStatusNotification";

 @interface ConfigureMCDController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate>
 {
     NSMutableArray *_accessoryList;
     EAAccessory *_accessory;
     
     EAAccessory *_selectedAccessory;
     EADSessionController *_eaSessionController;
    
     
     UIView *_noExternalAccessoriesPosterView;
     UILabel *_noExternalAccessoriesLabelView;
      uint32_t _totalBytesRead;
     NSThread* myThread2;
     NSThread* myThread1;
     int direction;
     int shakes;
     
     NSString *latit;
     NSString *longit;
     NSString *location;
     int posturecode;
     
     NSString *universalfilepath;
     NSTimer *thread2timer;
     NSFileHandle *fileHandle;
     
       NSTimer *sampletimer;
    
 }



 @property (retain, nonatomic) IBOutlet UITableView *mainTableView;
@property (retain, nonatomic) IBOutlet NSMutableArray *plot_dta1;
@property (retain, nonatomic) IBOutlet NSMutableArray *plotdataa;



-(void) Get_BuiltInTestREQ;
-(void)StartContinousECG;
-(void) StopContinousECG;
-(void) OnDemandECG:(int) time;
-(void) SET_Vol_level:(int) value;
-(void) SET_GAIN_VALUE:(float) GainValue;
-(void) SET_50HZ_60HZ_FilterState:(int) state;
-(void) SET_BaseLine_filterState:(int) state;
-(void) SET_TimeDivIntervfal:(int) pretime_sec :(int) posttime_sec;
-(void) setOrientation:(int)state;
-(void) setRespiration:(int) state;
-(void) SET_LeadSelect:(int) leadByte1;
-(void) SET_EventDetectionLead:(int) lead;
-(void) SET_SamplingRate:(int) rate;
-(void) SetLevel:(int) level;
-(void) SET_SuspTime:(int) Y_value :(int) SetingCode;

//threshold
-(void) SET_VentricularTachyNarrow:(int) Y1_value :(int) Y2_value :(int) flag :(int) posture ;
-(void) SET_VentricularTachyWide:(int) Y1_value :(int) Y2_value :(int) flag
                                :(int) posture;
-(void) SET_Bradycardia:(int) Y1_value :(int) Y2_value :(int) flag
                       :(int )posture;
-(void) SET_PauseValue:(int) Y_value :(int) flag :(int) posture;
-(void) SET_PVC:(int) active_flag :(int) Y_value :(int )posture;
-(void) SET_Asystole:(int) active_flag :(int) Y_value :(int) posture;
-(void) SET_BigemenyValue:(int) flag :(int) posture :(int) Y_value;
-(void) SET_TrigemenyStatus:(int) posture :(int) flag :(int) Y_value;
-(void) SET_CoupletStatus:(int) posture :(int) flag :(int) Y_value;
-(void) SET_TripletStatus:(int) posture :(int)flag :(int) Y_value;

-(void) SET_STelev:(int) Y_value :(int) flag :(int) posture;
-(void) SET_Depre:(int) Y_value :(int) flag  :(int) posture;
-(void) SET_ProlongQT:(int) Y_value :(int) flag :(int) posture;
-(void) SET_QRSwidthWide:(int) Y_value :( int) flag :(int) posture;

-(void) SET_Temperature:(int) _beforeFraction :(int) _afterFraction
                       :(int) flag :(int) posture;
-(void) SET_resperationRate :(int) high_low :(int) Y_value :(int) posture
                            :(int) flag;
-(void) SET_Apnea:(int) active_flag :(int) Y_value :(int)posture;
-(void) SET_NoMotion :(int)minutes;
-(void) SET_FallEvent:(int) active_flag :(int) posture;
-(void) SET_SuspensionPercent:(int) code :(int) percent :(int) posture ;

-(void) SetMCDSettings:(int) filter50_60 :(int) baselinefilter :
(int) detectionlead :(int) gainsettings :(int) samplingrate
                      :(int) pretime_sec :(int) posttime_sec :(int) volumelevel :(int) leadconfig
                      :(int) respiration :(int) orientation;


-(void)setInvertedTWave;
-(void) ManualPatientCall:(int)time;
- (void)sendBTEventNotificationWithEventOccured:(BOOL)isEventStatus;

-(void)startbluetoothstreaming;



@end

