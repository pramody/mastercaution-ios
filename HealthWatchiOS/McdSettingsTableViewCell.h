//
//  McdSettingsTableViewCell.h
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 19/06/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomUiPicker.h"

@interface McdSettingsTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel * rowSettingTitle;
@property (nonatomic, strong) IBOutlet UILabel * rowSettingValue;
// @IBOutlet weak var expOrCollButton: CustomUIButton!
@property (nonatomic, strong) IBOutlet UIImageView * rightArrowBlackImg;


@property (nonatomic, strong) IBOutlet UIView * expandView;
@property (nonatomic, strong) IBOutlet UIView * topShowView;

// Pre Time
// Time  Intervals
@property (nonatomic, strong) IBOutlet UIView * preTimeGrayView;
@property (nonatomic, strong) IBOutlet UITextField * preTimeStepperText;
@property (nonatomic, strong) IBOutlet UIStepper * preTimeStepperControl;

// Post Time
@property (nonatomic, strong) IBOutlet UIView * postTimeGrayView;
@property (nonatomic, strong) IBOutlet UITextField * postTimeStepperText;
@property (nonatomic, strong) IBOutlet UIStepper * postTimeStepperControl;

@property (nonatomic, strong) IBOutlet CustomUiPicker * oneForAllPickerViewControl;
@end
