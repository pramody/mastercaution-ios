#import <UIKit/UIKit.h>
#import <ExternalAccessory/ExternalAccessory.h>

@interface EADSessionTransferViewController : UIViewController <UITextFieldDelegate> {
    EAAccessory *_accessory;
    UILabel *_receivedBytesLabel;
    UITextField *_stringToSendTextField;
    UITextField *_hexToSendTextField;

    uint32_t _totalBytesRead;
}

- (IBAction)sendString:(id)sender;
- (IBAction)sendHex:(id)sender;
- (IBAction)send10K:(id)sender;

// UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField;

@property(nonatomic, retain) IBOutlet UILabel *receivedBytesLabel;
@property(nonatomic, retain) IBOutlet UITextField *stringToSendTextField;
@property(nonatomic, retain) IBOutlet UITextField *hexToSendTextField;

@end
