//
//  UserProfileViewController.h
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 18/06/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//


/*enum
{
 HEIGHT = 0,
 WEIGHT = 1,
 GENDER = 2,
 DOB    = 3,
};typedef NSInteger DropDownFor;*/

enum
{
 FIRSTNAME      =   0,
 LASTNAME       =   1,
 USERID         =   2,
 HEIGHTFEETS    =   3,
 HEIGHTINCHES   =   4,
 WEIGHT         =   5,
 BMI            =   6,
 GENDER         =   7,
 DOB            =   8
    
};typedef NSInteger validFields;

@interface UserProfileViewController : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    NSArray * heightInFeetsArray;
    NSArray* heightInInchesArray;
    NSMutableArray* heightInCmsArray;
    NSArray *genderDicArray;
    
    NSDate *dateTempHolderNSDate;
    UIColor* blueColorMainAppUIColor;
    
     BOOL FirstNameFlag;
     BOOL LastNameFlag;
     BOOL UIDFlag;
     BOOL HeightFlag;
     BOOL WeightFlag;
     BOOL GenderFlag;
     BOOL DOBirthFlag;
    
}

// All IBOutlets

// - General
@property (nonatomic, strong) IBOutlet UIView * headerBlueView;
@property (nonatomic, strong) IBOutlet UILabel * headerTitleLbl;
@property (nonatomic, strong) IBOutlet UIView * parentMainView;
@property (nonatomic, strong) IBOutlet UIView * displayUserLevelMainView;
@property (nonatomic, strong) IBOutlet UIButton * editButton;
@property (nonatomic, strong) IBOutlet UIButton * applyButton;
@property (nonatomic, strong) IBOutlet UILabel * userActiveLevelLabel;
@property (nonatomic, strong) IBOutlet UIImageView * userActiveLevelImage;
@property (nonatomic, strong) IBOutlet UIView * blockScreenMainView;
@property (nonatomic, strong) IBOutlet UIPickerView * pickerViewControl;
@property (nonatomic, strong) IBOutlet UIDatePicker * datePickerViewControl;

@property (nonatomic, strong) IBOutlet UILabel * dropDownTitle;

// - ReadyOnly
@property (nonatomic, strong) IBOutlet UIImageView * userImageReadyOnly;
@property (nonatomic, strong) IBOutlet UIView * ReadOnlyMainView;
@property (nonatomic, strong) IBOutlet UILabel * firstNameLabel;
@property (nonatomic, strong) IBOutlet UILabel * lastNameLabel;
@property (nonatomic, strong) IBOutlet UILabel * userUniqueIdLabel;

@property (nonatomic, strong) IBOutlet UILabel * heightLabel;
@property (nonatomic, strong) IBOutlet UILabel * weightLabel;
@property (nonatomic, strong) IBOutlet UILabel * bmiLabel;
@property (nonatomic, strong) IBOutlet UILabel * genderLabel;
@property (nonatomic, strong) IBOutlet UILabel * dobLabel;

@property (nonatomic, strong) IBOutlet UILabel * WeightHeaderLabel;
@property (nonatomic, strong) IBOutlet UILabel * HeightHeaderLabel;




@property (nonatomic, strong) IBOutlet UISwitch * Readswitch;

@property (nonatomic, strong) IBOutlet UIView * contentreadonly;
@property (nonatomic, strong) IBOutlet UIView * locationreadonly;

// - Edit
@property (nonatomic, strong) IBOutlet UIImageView * userImageEdit;
@property (nonatomic, strong) IBOutlet UIView * EditMainView;
@property (nonatomic, strong) IBOutlet UITextField * firstNameText;
@property (nonatomic, strong) IBOutlet UITextField * lastNameText;
@property (nonatomic, strong) IBOutlet UITextField * userUniqueIdText;

@property (nonatomic, strong) IBOutlet UITextField * heightTextField;
@property (nonatomic, strong) IBOutlet UITextField * weightTextField;
@property (nonatomic, strong) IBOutlet UILabel * bmiEditLabel;
@property (nonatomic, strong) IBOutlet UITextField * genderTextField;
@property (nonatomic, strong) IBOutlet UITextField * dobTextField;
@property (nonatomic, strong) IBOutlet UILabel * feetLabel;
@property (nonatomic, strong) IBOutlet UILabel * inchesLabel;

@property (nonatomic, strong) IBOutlet UILabel * WeightEditHeaderLabel;
@property (nonatomic, strong) IBOutlet UILabel * HeightEditHeaderLabel;

@property (nonatomic, strong) IBOutlet UISwitch * Editswitch;

@property (nonatomic, strong) UIPopoverController *popOver;

@property (nonatomic, strong) IBOutlet NSMutableArray * userProfileArray;
@property (nonatomic, strong)IBOutlet NSManagedObjectContext * managedObjectContext;
@property (nonatomic) validFields currentDropDownFor;

@property (nonatomic) UITapGestureRecognizer *tapRecognizer;


@end
