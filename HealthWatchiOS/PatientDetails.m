//
//  PatientDetails.m
//  HealthWatch
//
//  Created by METSL MAC MINI on 22/09/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import "PatientDetails.h"


@implementation PatientDetails

@dynamic active_since;
@dynamic blocked;
@dynamic city;
@dynamic code;
@dynamic country;
@dynamic device_id;
@dynamic dob1;
@dynamic doc_address;
@dynamic doc_email;
@dynamic doc_name;
@dynamic doc_ph_no;
@dynamic doc_id;
@dynamic doc_degree;
@dynamic doc_country;
@dynamic ecode;
@dynamic ecode_desc;
@dynamic family_name;
@dynamic gender1;
@dynamic given_name;
@dynamic height1;
@dynamic level;
@dynamic middle_name;
@dynamic mode;
@dynamic modified_datetime;
@dynamic notification_id;
@dynamic notification_pwd;
@dynamic pat_updated_site;
@dynamic patient_add;
@dynamic patient_id;
@dynamic patient_location;
@dynamic patient_name;
@dynamic server_name;
@dynamic state;
@dynamic status;
@dynamic street;
@dynamic uid;
@dynamic weight;
@dynamic zipcode;
@dynamic patient_mobno;



@end
