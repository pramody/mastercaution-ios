//
//  LeftMenuCustomTableViewCell.m
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 17/06/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import "LeftMenuCustomTableViewCell.h"

@implementation LeftMenuCustomTableViewCell
@synthesize leftMenuItemIcon,leftMenuItemName,leftMenuBadgeIcon;
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
