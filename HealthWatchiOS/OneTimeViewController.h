//
//  OneTimeViewController.h
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 12/06/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//


#import "CustomIndicatorView.h"
#import "MTPopupWindow.h"
@interface OneTimeViewController : UIViewController<MTPopupWindowDelegate>

@property (nonatomic, strong)IBOutlet UITextField *serverDomainTextField;
@property (nonatomic, strong)IBOutlet UITextField *serverActivationKeyTextField;
@property (nonatomic, strong)IBOutlet UIButton *submitButton;
@property (nonatomic, strong)IBOutlet UIButton *standaloneButton;
@property (nonatomic, strong)IBOutlet CustomIndicatorView *spinnerView;
@property (nonatomic, strong)IBOutlet UIView * indicatorMainBlockView;
@property (nonatomic, strong)IBOutlet NSManagedObjectContext * managedObjectContext;
-(IBAction)submitAction:(id)sender;
@end
