//
//  SuspensionSettings.m
//  HealthWatch
//
//  Created by METSL MAC MINI on 31/07/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import "SuspensionSettings.h"


@implementation SuspensionSettings

@dynamic created_date_time;
@dynamic created_from;
@dynamic device_id;
@dynamic event_code;
@dynamic modify_datetime;
@dynamic modify_from;
@dynamic patient_id;
@dynamic status;
@dynamic suspension_time;
@dynamic update_to_device;
@dynamic updated_to_site;

@end
