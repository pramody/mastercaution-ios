//
//  CustomUIPickerView.h
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 25/06/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomUIPickerView : UIPickerView
@property (nonatomic, strong) IBOutlet NSDictionary* passDictArray;
@property (nonatomic, strong) IBOutlet NSString* passInputType;// DOCTOR_TEXT, YELLOW_TEXT, RED_TEXT
@property (nonatomic, strong) IBOutlet NSString* passMainCatSlug; // other-events-slug, arrhythmia-slug, ischemia-slug
@property (nonatomic, strong) IBOutlet NSString* passMainSubCatSlug;
@property (nonatomic, strong) IBOutlet NSString* inputTypeCom3orCom1;
@end
