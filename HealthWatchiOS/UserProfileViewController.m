//
//  UserProfileViewController.m
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 18/06/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import "UserProfileViewController.h"

#import "PatientDetails.h"


@interface UserProfileViewController ()

@end

@implementation UserProfileViewController
@synthesize headerBlueView,userImageReadyOnly,displayUserLevelMainView,heightTextField,weightTextField,genderTextField,dobTextField,blockScreenMainView,datePickerViewControl,pickerViewControl,feetLabel,inchesLabel,dropDownTitle,currentDropDownFor,firstNameText,lastNameText,userUniqueIdText,bmiEditLabel,firstNameLabel,lastNameLabel,genderLabel,dobLabel,userUniqueIdLabel,heightLabel,weightLabel,bmiLabel,userProfileArray,managedObjectContext,userActiveLevelLabel,userActiveLevelImage,userImageEdit,popOver,Readswitch,Editswitch,tapRecognizer,contentreadonly,locationreadonly,HeightHeaderLabel,WeightHeaderLabel,HeightEditHeaderLabel,WeightEditHeaderLabel;
bool displayFrontView = true;
bool imageselectionflag=false;


 // To get the Index, Refer above Enum for ValidFields

int selectedHeightFeetsIndex  = 0;
int selectedHeightInchesIndex  = 0;
int selectedHeightCMSIndex  = 0;
int selectedGenderIndex     = 0;


// Error Message
NSString * validationErrorTitle            =   @"Validation Alert";
NSString * weightErrorBodyMessage          =   @"Weight cannot be blank, should between 1 to 999";

// Value Holder
int heightInFeetsHolder     = 0;
int heightInInchesHolder    = 0;
int weightHolder            = 0;
NSString * bmiHolder, *genderHolder;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
   heightInFeetsArray=@[@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10];
    heightInInchesArray= @[@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12];
    heightInCmsArray=[[NSMutableArray alloc]init];
    for(int i=0;i<=365;i++)
    {
        [heightInCmsArray addObject:[NSNumber numberWithInt:i]];
    }
    genderDicArray=@[@"male",@"female",@"other",@"unknown"];
    userProfileArray=[[NSMutableArray alloc]initWithObjects:@"", @"", @"", @"0", @"0", @"0", @"0", @"",@"",nil];
    
    AppDelegate * appDelegate = ( AppDelegate *)UIApplication.sharedApplication.delegate ;
    appDelegate.orientationKey = @"PRO";
    [appDelegate forceOrientationTakePlace:appDelegate.orientationKey];
    
    // ------------------------------------------- First time Sliding view Setups START  -------------------------------------------
    
    
    self.slidingViewController.topViewAnchoredGesture = ECSlidingViewControllerAnchoredGestureTapping | ECSlidingViewControllerAnchoredGesturePanning;
    self.slidingViewController.customAnchoredGestures =@[];
    
    [self intialSetups];
    
   
    
    
    FirstNameFlag=false;
    LastNameFlag=false;
    UIDFlag=false;
    HeightFlag=false;
    WeightFlag=false;
    GenderFlag=false;
    DOBirthFlag=false;
   
    //println("BMI IS = \(BMI)")
    
    
    // switch
    [Readswitch setEnabled:NO];
   
    
    
}



-(IBAction)goToDashboard:(id)sender{
    self.slidingViewController.topViewController.view.layer.transform = CATransform3DMakeScale(1, 1, 1);
    self.slidingViewController.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"DashboardStoryboardId"];
}

-(IBAction)leftSideButtonAction:(id)sender{
    [self.slidingViewController anchorTopViewToRightAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void) intialSetups
{
    blueColorMainAppUIColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"blue_bg_full.png"]];
    headerBlueView.backgroundColor = blueColorMainAppUIColor;

    
    userImageReadyOnly.layer.borderWidth =  1.0;
    userImageReadyOnly.layer.cornerRadius=userImageReadyOnly.frame.size.height /2;
     userImageReadyOnly.layer.masksToBounds = YES;
    userImageReadyOnly.layer.borderColor =  UIColor.lightGrayColor.CGColor;
    
    displayUserLevelMainView.layer.cornerRadius = 7.0;
    displayUserLevelMainView.layer.borderWidth =  1.5;
    displayUserLevelMainView.layer.borderColor =  UIColor.lightGrayColor.CGColor;
    
    // TEXTFIELD Border
    
    CGFloat borderWidth = 0.3;
    CGColorRef borderColor  = [[[UIColor grayColor]colorWithAlphaComponent:0.5]CGColor];
    
    heightTextField.layer.borderColor =     borderColor;
    heightTextField.layer.borderWidth =     borderWidth;
    
    weightTextField.layer.borderColor =     borderColor;
    weightTextField.layer.borderWidth =     borderWidth;
    [weightTextField addTarget:self action:@selector(weightInputCapture:) forControlEvents:UIControlEventEditingChanged];
   
    
    genderTextField.layer.borderColor =     borderColor;
    genderTextField.layer.borderWidth =     borderWidth;
    
    dobTextField.layer.borderColor =     borderColor;
    dobTextField.layer.borderWidth =     borderWidth;
    
    [contentreadonly.layer setCornerRadius:10.0f];
    
    // border
    [contentreadonly.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [contentreadonly.layer setBorderWidth:1.5f];
    
    // drop shadow
    [contentreadonly.layer setShadowColor:[UIColor blackColor].CGColor];
    [contentreadonly.layer setShadowOpacity:0.8];
    [contentreadonly.layer setShadowRadius:3.0];
    [contentreadonly.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [locationreadonly.layer setCornerRadius:10.0f];
    
    // border
    [locationreadonly.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [locationreadonly.layer setBorderWidth:1.5f];
    
    // drop shadow
    [locationreadonly.layer setShadowColor:[UIColor blackColor].CGColor];
    [locationreadonly.layer setShadowOpacity:0.8];
    [locationreadonly.layer setShadowRadius:3.0];
    [locationreadonly.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    
    // Make block screen transparent first
    blockScreenMainView.backgroundColor = [[UIColor darkGrayColor]colorWithAlphaComponent:0.5];
    
    // date picker settings
    
    datePickerViewControl.maximumDate = [NSDate date];
    
}


- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}
-(void) weightInputCapture:(UITextField*)textField
{
    NSLog(@"Text %@", textField.text);
    NSString* value  = (textField.text);
    if((value.length == 1 && value.intValue == 0) || value.length > 3)
    {
        weightTextField.text = [value substringToIndex:(value.length - 1)];
    }
}


-(void)viewWillAppear:(BOOL)animated {
    
    
    pickerViewControl.backgroundColor       = UIColor.whiteColor;
    datePickerViewControl.backgroundColor   = UIColor.whiteColor;
    
    pickerViewControl.delegate = self;
    
  
   
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(hideKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    if(!imageselectionflag)
    {
    BOOL savedSwitch= [[NSUserDefaults standardUserDefaults] boolForKey:@"SwitchState"];
    if(savedSwitch)
    {
        [self.Editswitch setOn:YES];
        [self.Readswitch setOn:YES];
    }
    else
    {
        [self.Editswitch setOn:NO];
        [self.Readswitch setOn:NO];
    }
    
    //fetching record
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    //PatientDetails *patientrecord = [NSEntityDescription insertNewObjectForEntityForName:@"PatientDetails"
    //inManagedObjectContext:self.managedObjectContext];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"PatientDetails" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
        
        NSLogger *logger=[[NSLogger alloc]init];
        logger.degbugger = true;
        
        [logger log:@"Coredata Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:[error localizedDescription], @"User Profile page", nil] error:TRUE];
        
    } else {
       // NSLog(@"%@", result);
        if (result.count > 0) {
            NSManagedObject *patient = (NSManagedObject *)[result objectAtIndex:0];
            //NSLog(@"1 - %@", patient);
            
            
            firstNameLabel.text=[NSString stringWithFormat:@"%@ %@",[patient valueForKey:@"given_name"],[patient valueForKey:@"family_name"]];
            firstNameText.text=[patient valueForKey:@"given_name"];
            //lastNameLabel.text=[patient valueForKey:@"middle_name"];
            lastNameText.text=[patient valueForKey:@"family_name"];
            userUniqueIdLabel.text=[patient valueForKey:@"uid"];
            userUniqueIdText.text=[patient valueForKey:@"uid"];
            
            NSUserDefaults *UnitDefault = [NSUserDefaults standardUserDefaults];
             NSString *heighttype=[UnitDefault objectForKey:@"HEIGHT"];
             if([heighttype isEqualToString:@"CENTIMETER"])
             {
            heightLabel.text=[patient valueForKey:@"height1"];
            heightTextField.text=[patient valueForKey:@"height1"];
                 HeightHeaderLabel.text=@"HEIGHT IN CMS";
                 HeightEditHeaderLabel.text=@"HEIGHT IN CMS";
                 [userProfileArray replaceObjectAtIndex:3 withObject:[patient valueForKey:@"height1"]];
                
             }
            else
            {
                heightLabel.text= [self returnRightMetric:[patient valueForKey:@"height1"] typeOfMetricUsed:@"cm"];
                heightTextField.text=[self returnRightMetric:[patient valueForKey:@"height1"] typeOfMetricUsed:@"cm"];
                HeightHeaderLabel.text=@"HEIGHT IN FT";
                HeightEditHeaderLabel.text=@"HEIGHT IN FT";
                
                NSString *hyt= [self returnRightMetric:[patient valueForKey:@"height1"] typeOfMetricUsed:@"cm"];
                
                NSArray *AllDatesBuf;
                AllDatesBuf=[hyt componentsSeparatedByString:@"’"];
                NSString * htf=AllDatesBuf[0];
                NSString * hti=[AllDatesBuf[1] stringByReplacingOccurrencesOfString:@"”" withString:@""];
                [userProfileArray replaceObjectAtIndex:3 withObject:htf];
                [userProfileArray replaceObjectAtIndex:4 withObject:hti];
            }
            
            
            NSString *weighttype=[UnitDefault objectForKey:@"WEIGHT"];
            if([weighttype isEqualToString:@"POUNDS"])
            {
                int pound=[[patient valueForKey:@"weight"] intValue] * 2.20462;
                weightLabel.text=[NSString stringWithFormat:@"%d", pound];
                weightTextField.text=[NSString stringWithFormat:@"%d", pound];
                WeightHeaderLabel.text=@"WEIGHT IN POUND";
                WeightEditHeaderLabel.text=@"WEIGHT IN POUND";
            }
           else
           {
            weightLabel.text=[patient valueForKey:@"weight"];
            weightTextField.text=[patient valueForKey:@"weight"];
               WeightHeaderLabel.text=@"WEIGHT IN KGS";
               WeightEditHeaderLabel.text=@"WEIGHT IN KGS";
           }
            
            if([[patient valueForKey:@"gender1"]  isEqualToString:@"M"])
            {
                genderLabel.text=@"MALE";
                genderTextField.text=@"MALE";
                
            }
            else if([[patient valueForKey:@"gender1"]  isEqualToString:@"F"])
            {
                genderLabel.text=@"FEMALE";
                genderTextField.text=@"FEMALE";
                
            }
            else if([[patient valueForKey:@"gender1"]  isEqualToString:@"O"])
            {
                genderLabel.text=@"OTHER";
                genderTextField.text=@"OTHER";
                
            }
            else
            {
                genderLabel.text=@"UNKNOWN";
                genderTextField.text=@"UNKNOWN";
            }
            
            //dob
            [NSLocale availableLocaleIdentifiers];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
            NSLog(@"%@",[patient valueForKey:@"dob1"]);
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setLocale:locale];
            [formatter setDateFormat:@"MM/dd/yyyy"];
            NSDate *date = [formatter dateFromString:[patient valueForKey:@"dob1"]];
            [formatter setDateFormat:@"dd MMM yyyy"];
            NSString *newDate = [formatter stringFromDate:date];
            dobLabel.text=newDate;
            dobTextField.text=newDate;
            
            
            
            //bmi
            int heightinc;
            int heightft;
            float value = [[patient valueForKey:@"height1"] floatValue];
            float number = value / 2.54;
            if (number > 12.0) {
                heightft=(int)floor(number / 12.0);
                heightinc=(int)number % 12;
                
                
            } else {
                heightft=0;
                heightinc=(int)round(number);
                
            }
            
            if (heightft > 0 && [weightLabel.text intValue] > 0)
            {   CommonHelper * commhelp=[[CommonHelper alloc] init];
                NSString *bmiValue = [commhelp getBmiValueForHeightInFeetInches:heightft :heightinc : [[patient valueForKey:@"weight"] intValue]];
               
              
                if (bmiValue.length > 0)
                {
                    
                    bmiLabel.text       = bmiValue;
                    bmiEditLabel.text=bmiValue;
                }
            }
            
            
            [userProfileArray replaceObjectAtIndex:0 withObject:firstNameText.text];
            [userProfileArray replaceObjectAtIndex:1 withObject:lastNameText.text];
            [userProfileArray replaceObjectAtIndex:2 withObject:userUniqueIdText.text];
            
            [userProfileArray replaceObjectAtIndex:5 withObject:weightTextField.text];
            [userProfileArray replaceObjectAtIndex:6 withObject:bmiEditLabel.text];
            [userProfileArray replaceObjectAtIndex:7 withObject:genderTextField.text];
            [userProfileArray replaceObjectAtIndex:8 withObject:dobTextField.text];
            
            //userimage
            NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
            
            NSData *usrdta=[userDefault objectForKey:@"user_details"];
            
            
            NSData* imageData = [usrdta valueForKey:@"PatientImage"];
            userImageReadyOnly.image = [UIImage imageWithData:imageData];
            userImageEdit.image=[UIImage imageWithData:imageData];
            
            NSString *  Patientlevel   = [patient valueForKey:@"Level"];
            if([Patientlevel isEqualToString:@"Level1"])
            {
                Patientlevel=@"SAFE GUARD MODE";
                userActiveLevelImage.image= [UIImage imageNamed:@"safeguardmode.png"];
                
            }
            else if([Patientlevel isEqualToString:@"Level2"])
            {
                
                Patientlevel=@"PREMIUM GUARD MODE";
                userActiveLevelImage.image= [UIImage imageNamed:@"premiumguardmode.png"];
            }
            else if([Patientlevel isEqualToString:@"Level3"])
            {
                Patientlevel=@"AUTO PREMIUM SAFE GUARD MODE";
                userActiveLevelImage.image= [UIImage imageNamed:@"autopremiumsafeguardmode.png"];
            }
            else
            {
                Patientlevel=@"PERSONAL ALERT GUARDIAN";
                userActiveLevelImage.image= [UIImage imageNamed:@"personalalert-guardian.png"];
            }
            userActiveLevelLabel.text=Patientlevel;
            
            
            
            
        }
    }

    }
    else
    {
        imageselectionflag=false;
    }
}



-(void) hideKeyboard
{
    [self.view endEditing:YES];
}


-(IBAction)editProfileAction:(UIButton *)sender
{
    [self hideKeyboard];
    //println("Button action here");
    
    BOOL doTransition = false;
    
    if([sender tag] == 0) // Edit Button Clicked
    {
        doTransition = true;
        displayFrontView=true;
        //println("Going to Edit");
    }
    else if([sender tag] == 1) // Apply Button Clciked
    {
        displayFrontView=false;
        doTransition = false; // this will first save the data and that it will execute the transition
        //println("Going to Apply");
               //println("View all data to Readyonly now \(userProfileArray)")
        bool validationIs = [self userProfileValidation];
        
        if( validationIs == true)
        {
              NSLogger *logger=[[NSLogger alloc]init];
            NSString * patientid,*deviceid,*createddatetime;
            NSEntityDescription *entity = [NSEntityDescription entityForName:@"PatientDetails" inManagedObjectContext:self.managedObjectContext];
            
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
            [fetchRequest setEntity:entity];
            
            NSError *error = nil;
            NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
            
            if (error) {
                NSLog(@"Unable to execute fetch request.");
                NSLog(@"%@, %@", error, error.localizedDescription);
                
                logger.degbugger = true;
                
                [logger log:@"Coredata Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:[error localizedDescription], @"User Profile page", nil] error:TRUE];
                
            } else {
                NSLog(@"%@", result);
                if (result.count > 0) {
                    NSManagedObject *patient = (NSManagedObject *)[result objectAtIndex:0];
                    //NSLog(@"1 - %@", patient);
                    patientid=[patient valueForKey:@"patient_id"];
                    deviceid=[patient valueForKey:@"device_id"];
                     createddatetime=[patient valueForKey:@"active_since"];
                    CommonHelper * commhelp=[[CommonHelper alloc] init];
                    createddatetime = [commhelp getDateInYourFormat :@"dd MMM yy" :@"MM/dd/yyyy" :DateAsNSString :createddatetime :nil];
                    NSLog(@"cred%@",createddatetime);
                }
            }
        
            int j=1;
           
            
          

           while(j<8)
           {
            if(FirstNameFlag)
            {
                FirstNameFlag=false;
                
                 [userProfileArray replaceObjectAtIndex:0 withObject:firstNameText.text];
                
                
                NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"PatientDetails"];
                PatientDetails *patientsettings=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
                [patientsettings setValue:firstNameText.text forKey:@"given_name"];
                [patientsettings setValue:@"0" forKey:@"pat_updated_site"];
                
                [self.managedObjectContext save:nil];
               
                
                
               // [operation start];
                
            }
            else if(LastNameFlag==true)
            {
                LastNameFlag=false;
                
                [userProfileArray replaceObjectAtIndex:1 withObject:lastNameText.text];
                
                NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"PatientDetails"];
                PatientDetails *patientsettings=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
                [patientsettings setValue:lastNameText.text forKey:@"family_name"];
                [patientsettings setValue:@"0" forKey:@"pat_updated_site"];
                
                [self.managedObjectContext save:nil];
                
            }
            else if(UIDFlag==true)
            {
                UIDFlag=false;
                
                [userProfileArray replaceObjectAtIndex:2 withObject:userUniqueIdText.text];
                
                NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"PatientDetails"];
                PatientDetails *patientsettings=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
                [patientsettings setValue:userUniqueIdText.text forKey:@"uid"];
                [patientsettings setValue:@"0" forKey:@"pat_updated_site"];
                
                [self.managedObjectContext save:nil];
                
            }
            else if (HeightFlag==true)
            {
                HeightFlag=false;
                NSString * heightsertext;
                NSUserDefaults *UnitDefault = [NSUserDefaults standardUserDefaults];
                NSString *heighttype=[UnitDefault objectForKey:@"HEIGHT"];
                if([heighttype isEqualToString:@"CENTIMETER"])
                {
                    heightsertext=heightTextField.text;
                }
                else
                 heightsertext=[self returnRightMetric:heightTextField.text typeOfMetricUsed:@"inches"];
                
                NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"PatientDetails"];
                PatientDetails *patientsettings=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
                [patientsettings setValue:heightsertext forKey:@"height1"];
                [patientsettings setValue:@"0" forKey:@"pat_updated_site"];
                
                [self.managedObjectContext save:nil];
                
                
             
            }
            else if (WeightFlag==true)
            {
                WeightFlag=false;
                
                NSUserDefaults *UnitDefault = [NSUserDefaults standardUserDefaults];
                NSString *weighttype=[UnitDefault objectForKey:@"WEIGHT"];
                if([weighttype isEqualToString:@"POUNDS"])
                {
                    int weight=round([weightTextField.text intValue] * 0.453592);
                    NSString * weightvalue =[NSString stringWithFormat:@"%d", weight ];
                    
                    NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"PatientDetails"];
                    PatientDetails *patientsettings=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
                    [patientsettings setValue:weightvalue forKey:@"weight"];
                    [patientsettings setValue:@"0" forKey:@"pat_updated_site"];
                    
                    [self.managedObjectContext save:nil];
                  
                }
                else
                {
                    NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"PatientDetails"];
                    PatientDetails *patientsettings=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
                    [patientsettings setValue:weightTextField.text forKey:@"weight"];
                    [patientsettings setValue:@"0" forKey:@"pat_updated_site"];
                    
                    [self.managedObjectContext save:nil];
                }
                
               
            
            }
            else if (GenderFlag==true)
            {
                GenderFlag=false;
                NSString * genderservtext,*genderdatabasetxt;
                
                if([genderTextField.text isEqualToString:@"Male"])
                {
                   genderservtext=@"0";
                    genderdatabasetxt=@"M";
                }
                else if([genderTextField.text isEqualToString:@"Female"])
                {
                    genderservtext=@"1";
                    genderdatabasetxt=@"F";
                }
                else if([genderTextField.text isEqualToString:@"Other"])
                {
                    genderservtext=@"2";
                    genderdatabasetxt=@"O";
                }
                else
                {
                      genderdatabasetxt=@"U";
                }
                
                NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"PatientDetails"];
                PatientDetails *patientsettings=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
                [patientsettings setValue:genderdatabasetxt forKey:@"gender1"];
                [patientsettings setValue:@"0" forKey:@"pat_updated_site"];
                
                [self.managedObjectContext save:nil];
                
                
            }
            else if (DOBirthFlag==true)
            {
                DOBirthFlag=false;
                 CommonHelper * commhelp=[[CommonHelper alloc] init];
                NSString * dobsertext = [commhelp getDateInYourFormat :@"dd MMM yy" :@"MM/dd/yyyy" :DateAsNSString :dobTextField.text :nil];
                
                NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"PatientDetails"];
                PatientDetails *patientsettings=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
                [patientsettings setValue:dobsertext forKey:@"dob1"];

                [patientsettings setValue:@"0" forKey:@"pat_updated_site"];
                
                [self.managedObjectContext save:nil];
                
                
            }
               j++;
           }
          
            
           
            doTransition = true;
            
             [self showAlertView:@"" msg: @"User data was added successfully"];
            [self updateLabelForReadOnlyView];
            
        }
        
        
    }
    
    
    if(doTransition == true)
    {
        [UIView transitionWithView:[self parentMainView]
                          duration:1
                           options:displayFrontView ?UIViewAnimationOptionTransitionFlipFromRight:UIViewAnimationOptionTransitionFlipFromLeft
                        animations:^{
      
                                    //  () -> Void in
                                      
                            NSString * headerTitle;
                                      self.EditMainView.hidden = true;
                                      self.ReadOnlyMainView.hidden = true;
                                      self.editButton.hidden = true;
                                      self.applyButton.hidden = true;
                                      
                                      if(displayFrontView)
                                      {
                                          self.EditMainView.hidden = false;
                                          self.applyButton.hidden = false;
                                          headerTitle =@"Edit User Profile";
                                          
                                      }
                                      else
                                      {
                                          self.ReadOnlyMainView.hidden = false;
                                          self.editButton.hidden = false;
                                          headerTitle =@"User Profile";
                                          
                                      }
                                      
                                      self.headerTitleLbl.text = headerTitle;
                                      
                                  }
                        completion:^(BOOL finished){
                          
                                displayFrontView = !displayFrontView;
                          
                        }];
       
    }
    
    
    
    
}



-(void) showBlockView:(BOOL)boolValue
{
    blockScreenMainView.hidden = !boolValue;
}

-(void) showPickerViewControl:(BOOL)boolValue
{
    pickerViewControl.hidden = !boolValue;
}

-(void) showDatePickerViewControl:(BOOL)boolValue
{
    datePickerViewControl.hidden = !boolValue;
}

-(IBAction)popDropDown:(UITextField *)sender
{
    //println("user Profile data here \(userProfileArray)")
    /*
     for all buttons (weight is text box)
     - case FIRSTNAME      =   0
     - case LASTNAME       =   1
     - case USERID         =   2
     - case HEIGHTFEETS    =   3
     - case HEIGHTINCHES   =   4
     - case WEIGHT         =   5
     - case BMI            =   6
     - case GENDER         =   7
     - case DOB            =   8
     */
    
    // Hide the Keyboard before opening the dropdown
    //self.hideKeyboard()
    int tagwas=[sender tag];
      if(tagwas==3)
      {
          currentDropDownFor=HEIGHTFEETS;
      }
    else if(tagwas==7)
    {
        currentDropDownFor=GENDER;
    }
    else if (tagwas==8)
    {
        
        currentDropDownFor=DOB;
    }
    
        // Hide All Drop downs
        [self hideAllControls];
        NSString * titleIs;
        
        feetLabel.hidden =   true;
        inchesLabel.hidden =   true;
        
        switch(currentDropDownFor)
        {
            
             case HEIGHTFEETS:
            {
                feetLabel.hidden =   false;
                inchesLabel.hidden =   false;
                       // Prepare Control to Display
               [self generateHeightPickerView ];
                [self showPickerViewControl:true];
            
               titleIs = @"Height in";
                
                NSUserDefaults *UnitDefault = [NSUserDefaults standardUserDefaults];
                NSString *heighttype=[UnitDefault objectForKey:@"HEIGHT"];
                if([heighttype isEqualToString:@"CENTIMETER"])
                {
                    feetLabel.hidden =   true;
                    inchesLabel.hidden =   true;
                    NSInteger cmsrowvalue = [heightInCmsArray indexOfObject:[NSNumber numberWithInt:[heightTextField.text intValue]]];
                    
                    [pickerViewControl selectRow:cmsrowvalue inComponent:0 animated:false];
                    
                }
                else
                {
                    NSArray *AllDatesBuf;
                    AllDatesBuf=[heightTextField.text componentsSeparatedByString:@"’"];
                    
                    NSString * htf=AllDatesBuf[0];
                    NSString * hti=[AllDatesBuf[1] stringByReplacingOccurrencesOfString:@"”" withString:@""];
                    NSInteger Feetrowvalue = [heightInFeetsArray indexOfObject:[NSNumber numberWithInt:[htf intValue]]];
                    NSInteger Inchrowvalue = [heightInInchesArray indexOfObject:[NSNumber numberWithInt:[hti intValue]]];
                    
                    [pickerViewControl selectRow:Feetrowvalue inComponent:0 animated:false];
                    [pickerViewControl selectRow:Inchrowvalue inComponent:1 animated:false];
                }

              
            
                break;
            }
            
        case GENDER:
            {
            //println("Caught Gender")
            
            // Prepare Control to Display
            
                [self generateHeightPickerView ];
                [self showPickerViewControl:true];
                
            NSInteger genderrowvalue = [genderDicArray indexOfObject:[genderTextField.text lowercaseString]];
             [pickerViewControl selectRow:genderrowvalue inComponent:0 animated:false];
            titleIs = @"I am";
            
            
                break;
            }
            
        case DOB:
            {
            //println("Caught DOB")
                [self showDatePickerViewControl:true];
            
            titleIs = @"My DOB is";
                CommonHelper *commhelp=[[CommonHelper alloc]init];
              NSString *dateTest = [commhelp getDateInYourFormat:@"dd MMM yyy" :@"yyyy-MM-dd" :DateAsNSString :dobTextField.text :nil];
               
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
                [formatter setDateFormat:@"yyyy-MM-dd"];
                NSDate *date = [formatter dateFromString:dateTest];
                [datePickerViewControl setDate:date];
                break;
            }
            
        default:
            //println("Caught Default in case")
                break;
            
        }
        [self showBlockView:true];
        dropDownTitle.text = titleIs;
    
    
}


-(void) showAlertView:(NSString *)title  msg:(NSString *) message
{
    /*UIAlertView *alert=[[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];*/
    
  
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:@"OK"
                                       style:UIAlertActionStyleCancel
                                       handler:^(UIAlertAction *action)
                                       {
                                           
                                       }];
        [alertVC addAction:cancelAction];
    
         [self presentViewController:alertVC animated:YES completion:nil];
        
       /* [[[[[UIApplication sharedApplication] windows] objectAtIndex:0] rootViewController] presentViewController:alertVC animated:YES completion:^{
            
        }];*/

    
   }

// Prepare Picker View for Height

-(void) generateHeightPickerView
{
    //heightInFeetsArray = NSArray()
    // loop data for Height in Feets
    
    /*for eachHeigh in 1...10
     {
     //heightInFeetsArray.append(eachHeigh)
     heightInFeetsArray+=[eachHeigh]
     }*/
    
    //println("heightInFeetsArray \(heightInFeetsArray)")
    
    //println("heightInInchesArray \(heightInInchesArray)")
    
    
    [[self pickerViewControl]reloadAllComponents];
    
    
}


//  -------   UIPICKER DELEGATES and DATASOURCE -------  START

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    
    NSInteger pickerComponents = 1; // Default for GENDER
    
    if (currentDropDownFor == HEIGHTFEETS)
    {
        NSUserDefaults *UnitDefault = [NSUserDefaults standardUserDefaults];
        NSString *heighttype=[UnitDefault objectForKey:@"HEIGHT"];
        if([heighttype isEqualToString:@"CENTIMETER"])
        {
             pickerComponents = 1;
        }
        else
        pickerComponents = 2;
    }
    
    return pickerComponents;
    
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component 
{
    NSInteger rowInComp = genderDicArray.count; // Default to GENDER [Male, Female]
    
    if (currentDropDownFor == HEIGHTFEETS)
    {
        NSUserDefaults *UnitDefault = [NSUserDefaults standardUserDefaults];
        NSString *heighttype=[UnitDefault objectForKey:@"HEIGHT"];
        if([heighttype isEqualToString:@"CENTIMETER"])
        {
             rowInComp = heightInCmsArray.count;
        }
        else
        {
        if (component == 0)
        {
            rowInComp = heightInFeetsArray.count;
        }
        else
        {
            rowInComp = heightInInchesArray.count;
        }
        }
    }
    
    return rowInComp;
    
}

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    NSString * titleValue;
    
    if (currentDropDownFor == HEIGHTFEETS)
    {
        NSUserDefaults *UnitDefault = [NSUserDefaults standardUserDefaults];
        NSString *heighttype=[UnitDefault objectForKey:@"HEIGHT"];
        if([heighttype isEqualToString:@"CENTIMETER"])
        {
             titleValue = [NSString stringWithFormat:@"%@",heightInCmsArray[row]];
        }
        else
        {
            if (component == 0)
            {
                titleValue = [NSString stringWithFormat:@"%@", heightInFeetsArray[row]];
            }
            else if (component == 1)
            {
                titleValue = [NSString stringWithFormat:@"%@",heightInInchesArray[row]];
            }
 
        }
    }
    else if( currentDropDownFor == GENDER)
    {
        titleValue = [genderDicArray[row] capitalizedString];
    }
    
    return titleValue;
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    //var selectedHeightFeetsIndex, selectedHeightInchesIndex, selectedGenderIndex  : Int!
    
    if (currentDropDownFor == HEIGHTFEETS)
    {
        NSUserDefaults *UnitDefault = [NSUserDefaults standardUserDefaults];
        NSString *heighttype=[UnitDefault objectForKey:@"HEIGHT"];
        if([heighttype isEqualToString:@"CENTIMETER"])
        {
            selectedHeightCMSIndex=(int)row;
        }
        else
        {
            if (component == 0) { selectedHeightFeetsIndex    =    (int)row ;}
            else if (component == 1){ selectedHeightInchesIndex   =   (int)row; }
        }
       
    }
    else if (currentDropDownFor == GENDER)
    {
        selectedGenderIndex =  (int)row;
    }
    
    
    //println("Feets \(selectedHeightFeetsIndex) == Inches \(selectedHeightInchesIndex) == Gender \(selectedGenderIndex)")
    
}


//  -------   UIPICKER DELEGATES and DATASOURCE -------  END




//  -------   UITEXTFIELD DELEGATES  -------  END



-(BOOL) textFieldShouldBeginEditing:(UITextField *)textField {
    //println("textFieldShouldBeginEditing \(textField)")
    
    BOOL showKeyBoard = true;
    
    /*if ([weightTextField isFirstResponder] || [firstNameText isFirstResponder] || [lastNameText isFirstResponder] || [userUniqueIdText isFirstResponder])
     {
     
         [self hideKeyboard];
         showKeyBoard = true;
     }*/
    if( heightTextField == textField || genderTextField == textField ||  dobTextField == textField )
    {
        showKeyBoard = false;
        [self hideKeyboard];
    }
    
    
    if(heightTextField == textField)
    {
        //
        //showKeyBoard = false
        
        [self popDropDown:heightTextField];
    }
    else if(weightTextField == textField)
    {
        //
        //showKeyBoard = tru;
    }
    else if(genderTextField == textField)
    {
        //
        //showKeyBoard = false
        [self popDropDown:genderTextField];
    }
    else if(dobTextField == textField)
    {
        //
        showKeyBoard = false;
        [self hideKeyboard];
        [self popDropDown:dobTextField];
    }
    
    
    return showKeyBoard;
}







-(BOOL) textFieldShouldEndEditing:(UITextField *)textField {
    //println("Should End Editing \(textField.text)")
   
    BOOL isValidData=true;
   // NSString * errorTitle, *errorMessage;
    CommonHelper *comphelp=[[CommonHelper alloc] init];
    NSString * inputValue  = [comphelp trimTheString:textField.text];
    
    if (firstNameText == textField)
    {
        // Remove trim if any
        firstNameText.text = inputValue;
      
        if(firstNameText.text.length>0)
        {
            /*NSCharacterSet *s = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"];
            s = [s invertedSet];
            NSString *str1 = [firstNameText.text stringByReplacingOccurrencesOfString:@" " withString:@""];
            NSRange r = [str1 rangeOfCharacterFromSet:s];
            if (r.location != NSNotFound) {
                NSLog(@"the string contains illegal characters");
                
                 [self showAlertView: @"Validation Alert"  msg: @"the string contains illegal characters"];
                FirstNameFlag=false;
            }
            else
            {
                FirstNameFlag=true;
            }*/
            FirstNameFlag=true;
        }
      
         [userProfileArray replaceObjectAtIndex:0 withObject:inputValue];
       
        //}
    }
    else if (lastNameText == textField)
    {
        // Remove trim if any
        lastNameText.text = inputValue;
        if(lastNameText.text.length>0)
        {
            
           
                LastNameFlag=true;
            
        }
         [userProfileArray replaceObjectAtIndex:1 withObject:inputValue];
       
        //}
        
    }
    else if (userUniqueIdText == textField)
    {
        // Remove trim if any
        userUniqueIdText.text = inputValue;
        [userProfileArray replaceObjectAtIndex:2 withObject:inputValue];
       
        UIDFlag=true;
       
        //}
        
        
    }
    else if (weightTextField == textField)
    {
        //println("userWeightEnterAsStr = \(inputValue)")
        int charLength                  =   (int) inputValue.length;
        //println("charLength = \(charLength)")
        int inputWeightInt;
        
        inputWeightInt = (charLength > 0 && charLength <= 3 ) ? [(inputValue) intValue] : 0;
        
        if ( inputWeightInt != 0 )
        {
            // Calculate BMI and Add to Label
            
            
           
            
              // [userProfileArray insertObject:[NSString stringWithFormat:@"%d",inputWeightInt] atIndex:5];
           [userProfileArray replaceObjectAtIndex:5 withObject:[NSString stringWithFormat:@"%d",inputWeightInt]];
            
            
            NSString  * heightFeets;
            NSString * heightInches;
            NSUserDefaults *UnitDefault = [NSUserDefaults standardUserDefaults];
            NSString *heighttype=[UnitDefault objectForKey:@"HEIGHT"];
            if([heighttype isEqualToString:@"CENTIMETER"])
            {
                
                
                NSString *hyt= [self returnRightMetric:userProfileArray[3] typeOfMetricUsed:@"cm"];
                
                NSArray *AllDatesBuf;
                AllDatesBuf=[hyt componentsSeparatedByString:@"’"];
                heightFeets=AllDatesBuf[0];
                heightInches=[AllDatesBuf[1] stringByReplacingOccurrencesOfString:@"”" withString:@""];
                
            }
            else
            {
                // heightFeets = (selectedHeightFeetsIndex < heightInFeetsArray.count) ? [NSString stringWithFormat:@"%@",heightInFeetsArray[selectedHeightFeetsIndex]] : [NSString stringWithFormat:@"0"];
                
                
                heightFeets       = userProfileArray[3]; // at 3rd index we have Height in Feets
                heightInches       = userProfileArray[4]; // at 4th index we have Height in Inches
            }
            [self calculateBmiAndUpdateLabelForEditView:[heightFeets intValue] : [heightInches intValue]  : inputWeightInt];
            WeightFlag=true;
            
        }
        else
        {
            //self.showAlertView(validationErrorTitle, message: weightErrorBodyMessage)
            //isValidData = false
            [userProfileArray replaceObjectAtIndex:5 withObject:[NSString stringWithFormat:@"%d",inputWeightInt]];
            WeightFlag=true;
           
            
        }
        
    }
    
    return isValidData;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    //println("Should Return \(textField.text)")
    [weightTextField resignFirstResponder];
   
    return YES;
   
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.weightTextField resignFirstResponder];
    
}

//  -------   UITEXTFIELD DELEGATES  -------  END


//  -------   CHECK VALIDATIONS

-(IBAction)dateChangeValue:(id)sender
{
    
    //println("date change to \(sender.date)")
    
    dateTempHolderNSDate = [sender date];
    
}


-(IBAction)changeimage:(id)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle: nil
                                                             delegate: self
                                                    cancelButtonTitle: @"Cancel"
                                               destructiveButtonTitle: nil
                                                    otherButtonTitles: @"Take a new photo",
                                  @"Choose from existing", nil];
    [actionSheet showInView:self.view];
    

}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    int i = (int)buttonIndex;
    
    switch(i) {
            
        case 0:
        {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                picker.delegate = self;
                picker.allowsEditing = YES;
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                
                if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
                   /* UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:picker];
                    [popover presentPopoverFromRect:self.userImageEdit.bounds inView:self.EditMainView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
                    self.popOver = popover;*/
                   
                    [self presentViewController:picker animated:NO completion:NULL];
                    
                } else {
                    [self presentViewController:picker animated:YES completion:NULL];
                    
                }
            }];

            
            
        }
            break;
        case 1:
        {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
               /* UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:picker];
                [popover presentPopoverFromRect:self.userImageEdit.bounds inView:self.EditMainView permittedArrowDirections:UIPopoverArrowDirectionAny animated:NO];
                self.popOver = popover;*/
                
                 [self presentViewController:picker animated:NO completion:NULL];
              
            } else {
                [self presentViewController:picker animated:YES completion:NULL];
                
            }
                }];
        }
            
        default:
            // Do Nothing.........
            break;
            
    }
}



- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.userImageEdit.image = chosenImage;
    
    displayFrontView=false;
    
    NSMutableDictionary *user_details=[[NSMutableDictionary alloc]init];
    [user_details setObject: UIImagePNGRepresentation(userImageEdit.image) forKey: @"PatientImage"];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:user_details forKey: @"user_details"];
    
    [defaults synchronize];
    imageselectionflag=true;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];

    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    imageselectionflag=true;
      [self dismissViewControllerAnimated:YES completion:NULL];
   
}

-(IBAction)donePressOnPop:(id)sender
{
    
    
        switch(currentDropDownFor)
        {
            /*
             
             case HEIGHT = 0
             case GENDER = 1
             case DOB    = 2
             */
        case HEIGHTFEETS:
            
            {
                
             NSLog(@"save for haieghts");
                NSString  * heightFeets;
                NSString * heightInches;
                NSUserDefaults *UnitDefault = [NSUserDefaults standardUserDefaults];
                NSString *heighttype=[UnitDefault objectForKey:@"HEIGHT"];
                if([heighttype isEqualToString:@"CENTIMETER"])
                {
                    NSString *heightcms=(selectedHeightCMSIndex < heightInCmsArray.count) ? [NSString stringWithFormat:@"%@",heightInCmsArray[selectedHeightCMSIndex]] : [NSString stringWithFormat:@"0"];
                    
                  NSString *hyt= [self returnRightMetric:heightcms typeOfMetricUsed:@"cm"];
                    
                    NSArray *AllDatesBuf;
                    AllDatesBuf=[hyt componentsSeparatedByString:@"’"];
                     [userProfileArray replaceObjectAtIndex:3 withObject:heightcms];
                    heightFeets=AllDatesBuf[0];
                    heightInches=[AllDatesBuf[1] stringByReplacingOccurrencesOfString:@"”" withString:@""];

                     heightTextField.text    =   heightcms;
                }
                else
                {
                    // heightFeets = (selectedHeightFeetsIndex < heightInFeetsArray.count) ? [NSString stringWithFormat:@"%@",heightInFeetsArray[selectedHeightFeetsIndex]] : [NSString stringWithFormat:@"0"];
                    
                    
                    heightFeets = (selectedHeightFeetsIndex < heightInFeetsArray.count) ? [NSString stringWithFormat:@"%@",heightInFeetsArray[selectedHeightFeetsIndex]] : [userProfileArray objectAtIndex:3];
                    
                    [userProfileArray replaceObjectAtIndex:3 withObject:heightFeets];
                    
                    
                    
                     heightInches = (selectedHeightInchesIndex < heightInInchesArray.count ) ? [NSString stringWithFormat:@"%@",heightInInchesArray[selectedHeightInchesIndex]] : [NSString stringWithFormat:@"0"];
                    
                    [userProfileArray replaceObjectAtIndex:4 withObject:heightInches];
                    
                    heightTextField.text=[NSString stringWithFormat:@"%d’%d”",[heightFeets intValue],[heightInches intValue]];
                }
            
               

            
                HeightFlag=true;
                int weight = [userProfileArray[5] intValue];
            
            
            
            //println("Selected index Feets is \(selectedHeightFeetsIndex) and  Value is \(heightFeets)")
            //println("Selected index Inches is \(selectedHeightInchesIndex) and  Value is \(heightInches)")
            
            
            // Calculate BMI and Add to Label
                [self calculateBmiAndUpdateLabelForEditView:[heightFeets intValue] : [heightInches intValue] : weight];
            
                break;
            }
            
        case GENDER:
            
                NSLog(@"save for gender");
            //println("Save for Gender")
            {
            
            NSString * gender = (selectedGenderIndex < genderDicArray.count ) ? [genderDicArray[selectedGenderIndex] capitalizedString] : @"";
            NSLog(@"%@",gender);
                //[userProfileArray[7] addObject:gender];
                 [userProfileArray replaceObjectAtIndex:7 withObject:gender];
            
            // Update TextField / Lable Gender
            
                genderTextField.text = userProfileArray[7];
            
                GenderFlag=true;
            
            //println("Selected index Inches is \(selectedGenderIndex) and  Value is \(gender)")
            
            
            
                break;
            }
            
        case DOB:
                NSLog(@"Save for DOB");
            //println("Date to save is \(dateTempHolderNSDate)")
            {
            
            if(dateTempHolderNSDate != nil)
            {
                CommonHelper * commhelp=[[CommonHelper alloc] init];
                NSString *dateTest = [commhelp getDateInYourFormat:@"yyyy-MM-dd" :@"dd MMM yyy" :DateAsNSDate :@"2014-04-25" :dateTempHolderNSDate];

                  [userProfileArray replaceObjectAtIndex:8 withObject:dateTest];
                dobTextField.text  = userProfileArray[8];
                DOBirthFlag=true;
            }
            
                break;
            }
            
        default:
            //println("Save for Default")
                break;
            
        }
        
        // Hide Main Block transparent view and Open Droped down
   // [self cancelPressOnPop];
    [self cancelPressOnPop:nil];
    
    
}

-(void) calculateBmiAndUpdateLabelForEditView:(int)feets :(int)inches :(int)weight
{
    if (feets > 0 && weight > 0)
    {   CommonHelper * commhelp=[[CommonHelper alloc] init];
        NSString *bmiValue = [commhelp getBmiValueForHeightInFeetInches:feets :inches :weight];
        if (bmiValue.length > 0)
        {
           [userProfileArray replaceObjectAtIndex:6 withObject:bmiValue];
            bmiEditLabel.text       = userProfileArray[6];
        }
    }
    
    
    
    NSString * useHeightFeetsAndInches;
    //println("Foo bar \(useHeightFeetsAndInches)")
    
    if (feets == 0 && inches == 0) //from Weight to Height
    { useHeightFeetsAndInches = @""; }
    else if (feets > 0) //from Height to Weight
    {   useHeightFeetsAndInches = [NSString stringWithFormat:@"%d’%d”",feets,inches];
    
    
    // Update Edited Labels
       
    weightTextField.text    =   (weight > 0)? [NSString stringWithFormat:@"%d", weight] : @"";
    }
    
}



-(BOOL)userProfileValidation
{
    BOOL returnValue =   true;
    NSString * titleTEXT       =   @"";
    NSString * messageTEXT     =   @"Invalid inputs";
   

    
    
    if(userProfileArray.count > 0)
    {
        int arrayLoopCount = (int)userProfileArray.count;
        
        for (int i = 0; i < arrayLoopCount; i++)
        {
            //println("i == \(i)")
            CommonHelper *comphelp=[[CommonHelper alloc] init];
            NSString * value  = [comphelp trimTheString:userProfileArray[i]];
           
            
            if (i == 0) // 1 = First Name
            {
                if(value.length==0)
                {
                returnValue     =   false;
                messageTEXT     =   @"First name cannot be blank";
                break;
                }
                else
                {
                    NSCharacterSet *s = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.-' "];
                    s = [s invertedSet];
                    //NSString *str1 = [firstNameText.text stringByReplacingOccurrencesOfString:@" " withString:@""];
                    NSString *str1 = firstNameText.text ;
                    NSRange r = [str1 rangeOfCharacterFromSet:s];
                    if (r.location != NSNotFound) {
                       // NSLog(@"the string contains illegal characters");
                        
                        returnValue     =   false;
                        messageTEXT     =   @"the string contains illegal characters";
                        break;
                       
                    }
                }
            }
            else if (i == 1) // 1 = Last Name
            {
                if(value.length ==0)
                {
                returnValue     =   false;
                messageTEXT     =   @"Last name cannot be blank";
                break;
                }
                else
                {
                    NSCharacterSet *s = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.-' "];
                    s = [s invertedSet];
                    NSString *str1 = lastNameText.text;
                    NSRange r = [str1 rangeOfCharacterFromSet:s];
                    if (r.location != NSNotFound) {
                        
                        returnValue     =   false;
                        messageTEXT     =   @"the string contains illegal characters";
                        break;
                       
                    }
                }
            }
            else if (i == 2 && value.length == 0) // 1 = User Id
            {
                returnValue     =   false;
                messageTEXT     =   @"User ID cannot be blank";
                break;
            }
            else if (i == 3) // 3 = Height in Feets and We are Skipping for height in inches as index 4
            {
                
                if ( value.length == 0 )
                {
                    returnValue =   false;
                    messageTEXT     =   @"Height in Feets cannot be blank";
                    break;
                }
            }
            else if (i == 5)
            {
                /*if ( value.length == 0 false )
                 {
                 returnValue =   false
                 messageTEXT     =   "Weight cannot be blank!"
                 break
                 }
                 else*/ if ( value.length <= 0 || value.length > 3 || value.integerValue == 0)
                 {
                     returnValue =   false;
                     messageTEXT     =   @"Weight should be between 1 to 999";
                     break;
                 }
            }
            else if ( i == 7 && value.length == 0)
            {
                returnValue =   false;
                messageTEXT     =   @"Gender cannot be blank";
                break;
                
            }
            else if ( i == 8 )
            {
                if(value.length==0)
                {
                returnValue =   false;
                messageTEXT     =   @"DOB cannot be blank";
                break;
                }
                else
                {
                    NSDate *date = [[NSDate alloc] init];
                    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
                    [dateFormatter2 setDateFormat:@"dd MMM yyy"];
                    NSString *dob = [dateFormatter2 stringFromDate:date];
                    if([value isEqualToString:dob])
                    {
                        returnValue =   false;
                        messageTEXT     =   @"DOB cannot be today's date";
                        break;
                    }
                }
            }
        }
        
        /*for eachField in userProfileArray
         {
         let key    : AnyObject = eachField.key
         //println("KEY -> \(key)")
         let value  : NSString = CommonHelper().trimTheString(eachField.value as NSString)
         
         //println("Field is : KEY -> \(key)  ====   VALUE -> \(value) ====== LENGTH -> \(value.length)")
         
         if ( key.isEqual("first_name") && value.length == 0)
         {
         returnValue =   false
         messageTEXT     =   "First name cannot be blank!"
         break
         
         }
         else if ( key.isEqual("last_name") && value.length == 0 )
         {
         returnValue =   false
         messageTEXT     =   "Last name cannot be blank!"
         break
         }
         else if ( key.isEqual("user_unique_id") && value.length == 0 )
         {
         returnValue =   false
         messageTEXT     =   "User Id cannot be blank!"
         break
         
         }
         else if ( key.isEqual("height_feets") )
         {
         if ( value.integerValue == 0  )
         {
         returnValue =   false
         messageTEXT     =   "Height in Feets Should be greater than 0!"
         break
         }
         else if ( value.length == 0 )
         {
         returnValue =   false
         messageTEXT     =   "Height in Feets cannot be blank!"
         break
         }
         
         }
         else if ( key.isEqual("weight") )
         {
         if ( value.length == 0 )
         {
         returnValue =   false
         messageTEXT     =   "Weight cannot be blank!"
         break
         }
         else if ( value.length <= 0 || value.length > 3 || value.integerValue == 0)
         {
         returnValue =   false
         messageTEXT     =   "Weight should be between 1 to 999!"
         break
         }
         else if (   )
         {
         returnValue =   false
         messageTEXT     =   "Weight Should be greater than 0!"
         break
         }*/
    }
   /* else if (([key isEqual:@"gender"]) && (value.length == 0))
    {
        returnValue =   false;
        messageTEXT     =  @"Gender cannot be blank!";
        break;
        
    }
    else if ( key.isEqual("dob") && value.length == 0)
    {
        returnValue =   false;
        messageTEXT     =   @"DOB cannot be blank!";
        break;
    }*/
    

if(returnValue == false)
{
    
    [self showAlertView:titleTEXT  msg: messageTEXT];
}

return returnValue;
}

-(void) updateLabelForReadOnlyView
{
    
    //   ************   This should be uploaded once it is saved in the Database  *****************
    // Update Ready only Labels
  
    NSLog(@"udfdf%@",userProfileArray);
    NSString * firstName     =   userProfileArray[0];
    NSString * lastName      =   userProfileArray[1];
    NSString * userUniqueId      =   userProfileArray[2];
   
    NSString * heightInches      =   userProfileArray[4];
    NSString * weight         =   userProfileArray[5];
    NSString * bmi              =   userProfileArray[6];
    NSString * gender             =   userProfileArray[7];
    NSString * dob             =   userProfileArray[8];
    
    
   // NSString stringWithFormat:@"%@ %@",[patient valueForKey:@"patient_name"],[patient valueForKey:@"middle_name"]
    firstNameLabel.text     =  [NSString stringWithFormat:@"%@ %@",firstName,lastName];
    //lastNameLabel.text      =  lastName;
    userUniqueIdLabel.text  =  userUniqueId;
    NSUserDefaults *UnitDefault = [NSUserDefaults standardUserDefaults];
    NSString *heighttype=[UnitDefault objectForKey:@"HEIGHT"];
    if([heighttype isEqualToString:@"CENTIMETER"])
    {
         NSString * heightcms     =   userProfileArray[3];
         heightLabel.text        =   [NSString stringWithFormat:@"%@",heightcms];
    }
    else
    {
         NSString * heightFeets     =   userProfileArray[3];
         heightLabel.text        =   [NSString stringWithFormat:@"%@’%@”",heightFeets,heightInches];
    }
    
   //  @"\(heightFeets)' \(heightInches)\"";
    weightLabel.text        =   weight;
    bmiLabel.text           =   bmi;
    genderLabel.text        =   gender;
    dobLabel.text           =   dob;
    
    NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
    
    NSData *usrdta=[userDefault objectForKey:@"user_details"];
    
    
    NSData* imageData = [usrdta valueForKey:@"PatientImage"];
    userImageReadyOnly.image = [UIImage imageWithData:imageData];
    
    BOOL savedSwitch= [[NSUserDefaults standardUserDefaults] boolForKey:@"SwitchState"];
    if(savedSwitch)
    {
        [self.Editswitch setOn:YES];
        [self.Readswitch setOn:YES];
    }
    else
    {
        [self.Editswitch setOn:NO];
        [self.Readswitch setOn:NO];
    }
    
}

-(IBAction)cancelPressOnPop:(id)sender
{
    // Hide Transparent Block View
    [self showBlockView:false];
    
    // Hide All Drop downs
    [self hideAllControls];
    
}

-(void) hideAllControls
{
    [self showPickerViewControl:false];
    [self showDatePickerViewControl:false];
}

- (NSString *)returnRightMetric:(NSString *)theMeasure typeOfMetricUsed:(NSString *)metricType
{
    NSString *result = nil;
    
    if ([metricType isEqualToString:@"inches"]) {
        NSArray* theConvertion = [theMeasure componentsSeparatedByCharactersInSet:
                                  [NSCharacterSet characterSetWithCharactersInString:@"’”"]];
        NSInteger value1 = [theConvertion[0] intValue];
        NSInteger value2 = [theConvertion[1] intValue];
        
        float number = ((value1 * 12) + value2) * 2.54;
       // result = [NSString stringWithFormat:@"%.02f cm", round(number * 100.0) / 100.0];
        result = [NSString stringWithFormat:@"%.f", round(number * 100.0) / 100.0];
        
        
    } else if ([metricType isEqualToString:@"cm"]) {
        float value = [theMeasure floatValue];
        float number = value / 2.54;
        
        if (number > 12.0) {
            result = [NSString stringWithFormat:@"%i’%i”", (int)floor(number / 12.0), (int)number % 12];
            
        } else {
            result = [NSString stringWithFormat:@"0’%i”", (int)round(number)];
        }
        
    }
    
    NSLog(@"result: %@", result);
    return result;
}

- (IBAction)changeSwitch:(id)sender{
    UISwitch *switchValue = sender;
    if (switchValue.on){
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"SwitchState"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSLog(@"Switch is ON");
        [self locationonoff:1];
        
    } else{
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"SwitchState"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSLog(@"Switch is OFF");
          [self locationonoff:0];
    }
    
   
}


-(void)locationonoff :(int)flag
{
   
    
    BOOL standalone_Mode;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    standalone_Mode = [defaults boolForKey:@"ActiveStandalonemode"];
    
    if(!standalone_Mode)
    {
        
        NSArray * electrodestatus =[[NSArray alloc]initWithObjects:@"False",@"True",nil];
        
        
       
        
        NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
        
        
        NSString * patientid = [userDefault valueForKey:@"PatientID"];
        //NSString * deviceid = [usrdta valueForKey:@"Deviceid"];
        
        NSString *domainname =[[NSUserDefaults standardUserDefaults] stringForKey:@"DomainName"];
        
        
        NSString * Fetchingpath=[NSString stringWithFormat:@"https://%@/MCC/MCCThresholdSettings.svc/AckDataChangeGPS_Setting/?GPSStatus=%@&PatientID=%@",domainname,electrodestatus[flag],patientid];
        
       
        NSLogger *logger=[[NSLogger alloc]init];
        
        NSString *bookurl=[Fetchingpath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *URL1=[NSURL URLWithString:bookurl];
        
        NSLog(@"url%@",URL1);
        
        NSURLRequest *request = [NSURLRequest requestWithURL:URL1];
        
        
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        
        operation.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            NSLog(@"urlresp%@",string);
            
            
            NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
            id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            
            if([ [json objectForKey:@"status"] isEqualToString:@"Success"])
            {
                
                
                logger.degbugger = true;
                [logger log:@"Server Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"Success", @"GPS Setting Page", nil] error:TRUE];
                
            }
            else
            {
                logger.degbugger = true;
                [logger log:@"Server Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:@"Failure", @"GPS Setting Page", nil] error:TRUE];
                
            }
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            /*UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error updating Vital Param"
                                                                message:[error localizedDescription]
                                                               delegate:nil
                                                      cancelButtonTitle:@"Ok"
                                                      otherButtonTitles:nil];
            
            
            [alertView show];*/
            
            logger.degbugger = true;
            [logger log:@"Server Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:[error localizedDescription], @"GPS Setting Page", nil] error:TRUE];
            
        }];
        
        
        [operation start];
        
    }
    
   
}


@end
