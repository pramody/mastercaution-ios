//
//   Draw the frame.m
//  Master Caution
//
//  Created by METSL MAC MINI on 01/09/16.
//  Copyright © 2016 METSL MAC MINI. All rights reserved.
//

#import " Draw the frame.h"
#import <CoreText/CoreText.h>

@implementation _Draw_the_frame
@synthesize endPoint,endPoint2;
int LEAD_COUNT1=13;
int MCDLEAD1=12;
int offsetArray1[17];
int RESPIRATION1=0;
float cur_x1, prev_x1;

int LEADCONFIG1=12;
NSInteger p_ecg_y1[17];
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/



- (void)drawRect:(CGRect)rect
{
    
    context = UIGraphicsGetCurrentContext();
   //myBitmapContext1 = MyCreateBitmapContext1(320,450);
     //[self drawLabel:context];
   // [self drawLineFromPoint:context];
    
    [self drawLabel:context];
     [self drawLineFromPoint:context];
    
   // CGImageRef myImage;
    //myImage = CGBitmapContextCreateImage(myBitmapContext1);// 5
    //CGContextDrawImage(context,rect, myImage);
    
    
    
}


-(void)drawLineFromPoint:(CGContextRef)ctx
{
    
    CGPoint from=endPoint;
    CGPoint to=endPoint2;
    int colortype =1;
    CGContextSetLineWidth(ctx,1.0);
    
    CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
    
    CGColorRef color;
    if(colortype==0)
    {
        CGFloat components[] = {0, 0, 0, 1.0};
        color = CGColorCreate(colorspace, components);
    }
    else
    {
        CGFloat components[] = {1, 0, 0, 1.0};
        color = CGColorCreate(colorspace, components);
    }
    
    
    // CGContextSetStrokeColor(context, [UIColor blackColor]);
    CGContextSetStrokeColorWithColor(ctx, color);
    
    //NSLog(@"%0.0f %0.0f",from.y,to.y);
    CGContextMoveToPoint(ctx, from.x, from.y);
    CGContextAddLineToPoint(ctx, to.x, to.y);
    CGContextStrokePath(ctx);
    CGColorSpaceRelease(colorspace);
    CGColorRelease(color);
    
    
}

-(void)drawLabel:(CGContextRef)ctx {
    int scrhgt=400-8;
    
    
    /*offsetArray1[0] = (scrhgt / LEADCONFIG1)+50;
     for (int i = 1; i < LEADCONFIG1 ; i++) {
     offsetArray1[i] = offsetArray1[i - 1]+ (scrhgt / LEADCONFIG1);
     
     }
     */
    
    
    int offsetlenth=0;
    if(MCDLEAD1==15)
    {
        
        offsetlenth=17;
    }
    else if(MCDLEAD1==12)
    {
        offsetlenth=13;
        
    }
    switch (LEAD_COUNT1) {
        case 7:
            
            offsetArray1[0] =  (scrhgt / 8)+50;
            for (int i = 1; i < offsetlenth  ; i++) {
                offsetArray1[i] = offsetArray1[i - 1]
                + (int) (scrhgt /8);
            }
            offsetArray1[12] = offsetArray1[6];
            break;
        case 8:
            offsetArray1[0] =  (scrhgt / 9)+50;
            for (int i = 1; i < offsetlenth; i++) {
                offsetArray1[i] = offsetArray1[i - 1]
                + (int) (scrhgt /9);
            }
            offsetArray1[12] = offsetArray1[7];
            break;
        case 13:
            offsetArray1[0] =(scrhgt /14);
            for (int i = 1; i < offsetlenth; i++) {
                offsetArray1[i] = offsetArray1[i - 1]
                + (int) (scrhgt /14);
            }
            break;
        case 16:
            //				offsetArray1 = new int[17];
            offsetArray1[0] = (scrhgt /17)+50;
            for (int i = 1; i < offsetlenth; i++) {
                offsetArray1[i] = offsetArray1[i - 1]
                + (int) (scrhgt / 17);
            }
            break;
        default:
            break;
    }
    
    NSMutableArray *ecgtext;
    if(LEAD_COUNT1==7)
    {
        if(RESPIRATION1==0)
            ecgtext=[[NSMutableArray alloc]initWithObjects:@" I",@" II",@" III",@"aVR",@"aVL",@"aVF", nil];
        else
            ecgtext=[[NSMutableArray alloc]initWithObjects:@" I",@" II",@" III",@"aVR",@"aVL",@"aVF",@"Resp", nil];
    }
    else if (LEAD_COUNT1==8)
    {
        if(RESPIRATION1==0)
            ecgtext=[[NSMutableArray alloc]initWithObjects:@" I",@" II",@" III",@"aVR",@"aVL",@"aVF",@"V1", nil];
        else
            ecgtext=[[NSMutableArray alloc]initWithObjects:@" I",@" II",@" III",@"aVR",@"aVL",@"aVF",@"V1",@"Resp", nil];
    }
    else if (LEAD_COUNT1==13)
    {
        if(RESPIRATION1==0)
            ecgtext=[[NSMutableArray alloc]initWithObjects:@" I",@" II",@" III",@"aVR",@"aVL",@"aVF",@"V1",@"V2",@"V3",@"V4",@"V5",@"V6", nil];
        else
            ecgtext=[[NSMutableArray alloc]initWithObjects:@" I",@" II",@" III",@"aVR",@"aVL",@"aVF",@"V1",@"V2",@"V3",@"V4",@"V5",@"V6",@"Resp", nil];
    }
    else
    {
        if(RESPIRATION1==0)
            ecgtext=[[NSMutableArray alloc]initWithObjects:@" I",@" II",@" III",@"aVR",@"aVL",@"aVF",@"V1",@"V2",@"V3",@"V4",@"V5",@"V6",@"V7",@"V3R",@"V4R", nil];
        else
            ecgtext=[[NSMutableArray alloc]initWithObjects:@" I",@" II",@" III",@"aVR",@"aVL",@"aVF",@"V1",@"V2",@"V3",@"V4",@"V5",@"V6",@"V7",@"V3R",@"V4R",@"Resp", nil];
    }
    
    
    
    for(int i=0;i<12;i++)
    {
        NSString* textToDraw = ecgtext[i];
        
        CFStringRef stringRef = (__bridge CFStringRef)textToDraw;
        // Prepare the text using a Core Text Framesetter
        CFAttributedStringRef currentText = CFAttributedStringCreate(NULL, stringRef, NULL);
        CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString(currentText);
        
        CGRect frameRect = CGRectMake(10, offsetArray1[i]-30, 300, 50);
        CGMutablePathRef framePath = CGPathCreateMutable();
        CGPathAddRect(framePath, NULL, frameRect);
        
        // Get the frame that will do the rendering.
        CFRange currentRange = CFRangeMake(0,0);
        CTFrameRef frameRef = CTFramesetterCreateFrame(framesetter, currentRange, framePath, NULL);
        
        
        // Get the graphics context.
        
        
        CGContextSetRGBFillColor(ctx, 1.0, 1.0, 1.0, 1.0);
        CGContextSelectFont(ctx, "Helvetica", 12,kCGEncodingMacRoman);
        CGAffineTransform xform = CGAffineTransformMake(1.0, 0.0, 0.0, -1.0, 0.0, 0.0);
        CGContextSetTextMatrix(ctx, xform);
        CGContextShowTextAtPoint(ctx, 8, 2, [textToDraw UTF8String], strlen([textToDraw UTF8String]));
        
        // Put the text matrix into a known state. This ensures
        // that no old scaling factors are left in place.
        //CGContextSetTextMatrix(currentContext, CGAffineTransformIdentity);
        
        // Core Text draws from the bottom-left corner up, so flip
        // the current transform prior to drawing.
        //CGContextTranslateCTM(currentContext, 0, 100);
        //CGContextScaleCTM(currentContext, 1.0, -1.0);
        
        // Draw the frame.
        CTFrameDraw(frameRef, ctx);
        
        CFRelease(frameRef);
        CFRelease(stringRef);
        CFRelease(framesetter);
        CGPathRelease(framePath);
    }
    cur_x1+=50;
    for (int i = 0; i < LEADCONFIG1; i++) {
        p_ecg_y1[i] = offsetArray1[i];
    }
    prev_x1=cur_x1;
    
}

//newview



-(void) plotWave :(NSInteger [])in_pt {
  
    
    
      
    
    NSInteger ecg_y[17];
    
    
    
    //NSLog(@"%ld",(long)in_pt[6]);
    
    cur_x1=cur_x1+0.20;
    //cur_x1++;
    int end = 15;
    
    
    
    
    switch (LEAD_COUNT1) {
        case 7:
            end = 6;
            break;
        case 8:
            end = 7;
            break;
        case 13:
            end = 12;
            break;
        default:
            break;
    }
    int offset = 0;
    for (int j = 0; j < end; j++) {
        offset = offsetArray1[j];
        
        ecg_y[j] = (short)offset-((in_pt[j] * 1.0f / 2) / 2.0f);
        
        
        
        CGPoint from = CGPointMake(prev_x1,  p_ecg_y1[j]);
        CGPoint to = CGPointMake(cur_x1, ecg_y[j]);
        endPoint=from;
        endPoint2=to;
        
        
        
        // [self drawLineFromPoint:from :to  :myBitmapContext1];
        
        
        p_ecg_y1[j] = ecg_y[j];
        
    }
    
    if (RESPIRATION1 == 1) {
        
        
        if (LEADCONFIG1 == 16) {
            offset = offsetArray1[15];
            
            ecg_y[15] = (short) (offset - (in_pt[15] * 1.0f / 2) / 1.0f);
            
            CGPoint from = CGPointMake(prev_x1,  p_ecg_y1[15]);
            CGPoint to = CGPointMake(cur_x1, ecg_y[15]);
            endPoint=from;
            endPoint2=to;
             //[self drawLineFromPoint:from :to :myBitmapContext1];
            
            p_ecg_y1[15] = ecg_y[15];
        } else {
            offset = offsetArray1[12] ;
            
            ecg_y[12] = (short) (offset - (in_pt[12] * 1.0f / 2) / 1.0f);
            
            CGPoint from = CGPointMake(prev_x1,  p_ecg_y1[12]);
            CGPoint to = CGPointMake(cur_x1, ecg_y[12]);
            endPoint=from;
            endPoint2=to;
            
           //  [self drawLineFromPoint:from :to :myBitmapContext1];
            p_ecg_y1[12] = ecg_y[12];
        }
    }
    else {
        if(MCDLEAD1==15)
        {
            p_ecg_y1[15] = ecg_y[end];
        }
        else if(MCDLEAD1==12)
        {
            p_ecg_y1[12] = ecg_y[end];
        }
        
    }
    
    prev_x1 = cur_x1;
    

   
}


CGContextRef MyCreateBitmapContext1 (int pixelsWide,
                                    int pixelsHigh)
{
    CGContextRef    context = NULL;
    CGColorSpaceRef colorSpace;
    void *          bitmapData;
    int             bitmapByteCount;
    int             bitmapBytesPerRow;
    
    bitmapBytesPerRow   = (pixelsWide * 4);// 1
    bitmapByteCount     = (bitmapBytesPerRow * pixelsHigh);
    
    colorSpace = CGColorSpaceCreateWithName(kCGColorSpaceGenericRGB);// 2
    bitmapData = calloc( bitmapByteCount,1);// 3
    if (bitmapData == NULL)
    {
        fprintf (stderr, "Memory not allocated!");
        return NULL;
    }
    context = CGBitmapContextCreate (bitmapData,// 4
                                     pixelsWide,
                                     pixelsHigh,
                                     8,      // bits per component
                                     bitmapBytesPerRow,
                                     colorSpace,
                                     kCGImageAlphaPremultipliedLast);
    if (context== NULL)
    {
        free (bitmapData);// 5
        fprintf (stderr, "Context not created!");
        return NULL;
    }
    CGColorSpaceRelease( colorSpace );// 6
    
    return context;// 7
}


@end
