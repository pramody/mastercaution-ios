//
//  ViewController.m
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 12/06/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
///

#import "LeadPlayer.h"
#import "LiveEcgMonitor.h"

@implementation LeadPlayer
@synthesize pointsArray, index, label, liveMonitor, lightSpot, currentPoint, pos_x_offset, viewCenter,leadcolor;

int pixelsPerCell = 10.00;
//int pixelsPerCell =  0.2; //Live ecg second per cell

float lineWidth_Grid = 0.5;
float lineWidth_LiveMonitor = 1.3;
float lineWidth_Static = 1;

int pointsPerSecond = 500;
float pixelPerPoint = 2 * 60.0f / 500.0f;//space
//float pixelPerPoint =2 * 60.0f / 500.0f;//liveeecg space
int pointPerDraw1 = 500.0f * 0.04f;//speed
int pointPerDraw = 900.0f * 0.022f;//liveecgspeed
//int pointPerDraw = 500.0f * 0.022f;
int ii=0;

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor blackColor];
        self.clearsContextBeforeDrawing = YES;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            pointPerDraw =2000.0f * 0.04f;
        }
        else
            pointPerDraw =72.0f;
      
      
     myBitmapContext = MyCreateBitmapContext (768,31);
       // [self addGestureRecgonizer];
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
       context = UIGraphicsGetCurrentContext();
    [self drawLabel:context];
    [self drawGrid:context];
    [self drawCurve:context];
   // CGImageRef myImage;
    //myImage = CGBitmapContextCreateImage (myBitmapContext);// 5
    //CGContextDrawImage(context,rect, myImage);// 6
    //char *bitmapData = CGBitmapContextGetData(myBitmapContext); // 7
  //  CGContextRelease (myBitmapContext);// 8
    //if (bitmapData) free(bitmapData); // 9
   // CGImageRelease(myImage);
   
   
   
    /* CGImageRef img = CGBitmapContextCreateImage(context);
    incrementalImage = [UIImage imageWithCGImage:img];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *docs = [paths objectAtIndex:0];
    NSString * pathname=[NSString stringWithFormat:@"image%d.jpg",ii];
    NSString* path =  [docs stringByAppendingFormat:@"/%@",pathname];
    
    NSData* imageData = [NSData dataWithData:UIImageJPEGRepresentation(incrementalImage,80)];
    NSError *writeError = nil;
    [imageData writeToFile:path options:NSDataWritingAtomic error:&writeError];
    UIGraphicsEndImageContext();*/
}


- (void)drawGrid:(CGContextRef)ctx {
    CGFloat full_height = self.frame.size.height;
    CGFloat full_width = self.frame.size.width;
    CGFloat cell_square_width = pixelsPerCell;
    
    CGContextSetLineWidth(ctx, 0.2);
    CGContextSetStrokeColorWithColor(ctx, [UIColor greenColor].CGColor);
    
    int pos_x = 1;
    while (pos_x < full_width) {
        CGContextMoveToPoint(ctx, pos_x, 1);
        CGContextAddLineToPoint(ctx, pos_x, full_height);
        pos_x += cell_square_width;
        
        CGContextStrokePath(ctx);
    }
    
    CGFloat pos_y = 1;
    while (pos_y <= full_height) {
        
        CGContextSetLineWidth(ctx, 0.2);
        
        CGContextMoveToPoint(ctx, 1, pos_y);
        CGContextAddLineToPoint(ctx, full_width, pos_y);
        pos_y += cell_square_width;
        
        CGContextStrokePath(ctx);
    }
    
    
    CGContextSetLineWidth(ctx, 0.1);
    
    cell_square_width = cell_square_width / 5;
    pos_x = 1 + cell_square_width;
    while (pos_x < full_width) {
        CGContextMoveToPoint(ctx, pos_x, 1);
        CGContextAddLineToPoint(ctx, pos_x, full_height);
        pos_x += cell_square_width;
        
        CGContextStrokePath(ctx);
    }
    
    pos_y = 1 + cell_square_width;
    while (pos_y <= full_height) {
        CGContextMoveToPoint(ctx, 1, pos_y);
        CGContextAddLineToPoint(ctx, full_width, pos_y);
        pos_y += cell_square_width;
        
        CGContextStrokePath(ctx);
    }
}

- (void)drawLabel:(CGContextRef)ctx {
    CGContextSetRGBFillColor(ctx, 1.0, 1.0, 1.0, 1.0);
    CGContextSelectFont(ctx, "Helvetica", 12,kCGEncodingMacRoman);
    
    CGAffineTransform xform = CGAffineTransformMake(1.0, 0.0, 0.0, -1.0, 0.0, 0.0);
    CGContextSetTextMatrix(ctx, xform);
    CGContextShowTextAtPoint(ctx, 8, 28, [self.label UTF8String], strlen([self.label UTF8String]));
    
   
}

- (void)clearDrawing {
     context = UIGraphicsGetCurrentContext();
    CGFloat full_height = self.frame.size.height;
    CGFloat full_width = self.frame.size.width;
    
    CGContextSetRGBFillColor(context, 0.0, 0.0, 0.0, 1.0);
    CGContextFillRect(context, CGRectMake(0, 0, full_width, full_height));
    [self setNeedsDisplay];
}

- (void)drawCurve:(CGContextRef)ctx
{
    if (count == 0) return;
    
    CGContextSetLineWidth(ctx, lineWidth_LiveMonitor);
    //CGContextSetStrokeColorWithColor(ctx, [UIColor greenColor].CGColor);
    CGContextSetStrokeColorWithColor(ctx, leadcolor.CGColor);
    CGContextAddLines(ctx, drawingPoints, count);
    CGContextStrokePath(ctx);
    
    endPoint = drawingPoints[count-1];
    endPoint2 = drawingPoints[count-2];
    endPoint3 = drawingPoints[count-3];
    
   
    
    
}

- (void)fireDrawing
{
    float uVpb = 0.9;
    float pixelPerUV;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        pixelPerUV = 5 * 10.0 / 3000;
    }
    else
         pixelPerUV = 5 * 10.0 / 5000;
   
    
    int pointCount = pointPerDraw;
    CGFloat pointPixel = pixelPerPoint;
    CGFloat full_height = self.frame.size.height;
    
  
    count = 0;
   for (int i=0; i<pointCount; i++)
    {
        if ([self pointAvailable:currentPoint])
        {
            CGFloat pos_x = [self getPosX:currentPoint];
            if (i > 0 && pos_x == 0) break;
            
            if (i == 0 && pos_x != 0)
            {
                drawingPoints[0] = endPoint3;
                drawingPoints[1] = endPoint2;
                drawingPoints[2] = endPoint;
                i+=3; pointCount+=3; count+=3;
            }
            
            CGFloat value = full_height/2 - [[self.pointsArray objectAtIndex:currentPoint] intValue] * uVpb * pixelPerUV*5;
            
            drawingPoints[i] = CGPointMake(pos_x, value);
            
            currentPoint++;
            count++;
        }
        else {
           
            break;
        }
    }
    
    if (count > 0)
    {
        CGRect rect = CGRectMake(drawingPoints[1].x, 0, count*pointPixel+20, full_height);
        [self setNeedsDisplayInRect:rect];
        
    }

   
    
    /*
  
     CGFloat value = full_height/2 - [[self.pointsArray objectAtIndex:0] intValue] * uVpb * pixelPerUV*5;
    
     drawingPoints[0] = drawingPoints[1];
     drawingPoints[1] = CGPointMake(cur_x, value);
    
     CGFloat full_width = self.frame.size.width;
    CGRect rect = CGRectMake(0,0,full_width,full_height);
    
    myBitmapContext = MyCreateBitmapContext (768,71);
   // [self drawLabel:myBitmapContext];
    //[self drawGrid:myBitmapContext];
  
    [self setNeedsDisplayInRect:rect];
    
    //[self setNeedsDisplay];*/
   
    
    
}


- (void)fireDrawing :(float)cur_x
{
    float uVpb = 0.9;
    float pixelPerUV = 5 * 10.0 / 3000;
    
    int pointCount = pointPerDraw;
    CGFloat pointPixel = pixelPerPoint;
    CGFloat full_height = self.frame.size.height;
    
    
    count = 0;
    for (int i=0; i<pointCount; i++)
    {
        if ([self pointAvailable:currentPoint])
        {
            CGFloat pos_x = [self getPosX:currentPoint];
            if (i > 0 && pos_x == 0) break;
            
            if (i == 0 && pos_x != 0)
            {
                drawingPoints[0] = endPoint3;
                drawingPoints[1] = endPoint2;
                drawingPoints[2] = endPoint;
                i+=3; pointCount+=3; count+=3;
            }
            
            CGFloat value = full_height/2 - [[self.pointsArray objectAtIndex:currentPoint] intValue] * uVpb * pixelPerUV*5;
            
            drawingPoints[i] = CGPointMake(pos_x, value);
            
            currentPoint++;
            count++;
        }
        else {
            
            break;
        }
    }
    
    if (count > 0)
    {
        CGRect rect = CGRectMake(drawingPoints[1].x, 0, count*pointPixel+20, full_height);
        [self setNeedsDisplayInRect:rect];
        
    }
    
    
    /*
     
     CGFloat value = full_height/2 - [[self.pointsArray objectAtIndex:0] intValue] * uVpb * pixelPerUV*5;
     
     drawingPoints[0] = drawingPoints[1];
     drawingPoints[1] = CGPointMake(cur_x, value);
     
     CGFloat full_width = self.frame.size.width;
     CGRect rect = CGRectMake(0,0,full_width,full_height);
     
     myBitmapContext = MyCreateBitmapContext (768,71);
     // [self drawLabel:myBitmapContext];
     //[self drawGrid:myBitmapContext];
     
     [self setNeedsDisplayInRect:rect];
     
     //[self setNeedsDisplay];*/
    
    
    
}



CGContextRef MyCreateBitmapContext (int pixelsWide,
                                    int pixelsHigh)
{
    CGContextRef    context = NULL;
    CGColorSpaceRef colorSpace;
    void *          bitmapData;
    int             bitmapByteCount;
    int             bitmapBytesPerRow;
    
    bitmapBytesPerRow   = (pixelsWide * 4);// 1
    bitmapByteCount     = (bitmapBytesPerRow * pixelsHigh);
    
    colorSpace = CGColorSpaceCreateWithName(kCGColorSpaceGenericRGB);// 2
    bitmapData = calloc( bitmapByteCount,1);// 3
    if (bitmapData == NULL)
    {
        fprintf (stderr, "Memory not allocated!");
        return NULL;
    }
    context = CGBitmapContextCreate (bitmapData,// 4
                                     pixelsWide,
                                     pixelsHigh,
                                     8,      // bits per component
                                     bitmapBytesPerRow,
                                     colorSpace,
                                     kCGImageAlphaPremultipliedLast);
    if (context== NULL)
    {
        free (bitmapData);// 5
        fprintf (stderr, "Context not created!");
        return NULL;
    }
    CGColorSpaceRelease( colorSpace );// 6
    
    return context;// 7
}

- (void)fireDrawing1
{
    float uVpb = 0.9;
    float pixelPerUV = 5 * 10.0 / 5000;
    
    int pointCount = pointPerDraw1;
    CGFloat pointPixel = pixelPerPoint;
    CGFloat full_height = self.frame.size.height;
    
    
    count = 0;
    for (int i=0; i<pointCount; i++)
    {
        if ([self pointAvailable:currentPoint])
        {
            CGFloat pos_x = [self getPosX:currentPoint];
            if (i > 0 && pos_x == 0) break;
            
            if (i == 0 && pos_x != 0)
            {
                drawingPoints[0] = endPoint3;
                drawingPoints[1] = endPoint2;
                drawingPoints[2] = endPoint;
                i+=3; pointCount+=3; count+=3;
            }
            
            CGFloat value = full_height/2 - [[self.pointsArray objectAtIndex:currentPoint] intValue] * uVpb * pixelPerUV*20;
            
            drawingPoints[i] = CGPointMake(pos_x, value);
            
            currentPoint++;
            count++;
        }
        else {
            
            break;
        }
    }
    
    if (count > 0)
    {
        CGRect rect = CGRectMake(drawingPoints[1].x, 0, count*pointPixel+20, full_height);
        [self setNeedsDisplayInRect:rect];
        
    }
    
}


- (void)resetBuffer
{
    CGFloat full_width = self.frame.size.width;
    CGFloat pixelsPerPoint = pixelPerPoint;
    int cyclePoints = ceil(full_width / pixelsPerPoint);
    int temp = currentPoint % cyclePoints;
    int countToRemove = currentPoint - temp;
    
    currentPoint = temp;
    
    if (self.pointsArray.count > countToRemove)
    {
        [pointsArray removeObjectsInRange:NSMakeRange(0, countToRemove)];
    }
    
    
    
    //currentPoint = 0;
    //	[pointsArray removeAllObjects];
    //	[self setNeedsDisplay];
}

- (CGFloat)getPosX:(int)point
{
    CGFloat full_width = self.frame.size.width;
    
    int cyclePoints = ceil(full_width / pixelPerPoint);
    int aPoint = (point - pos_x_offset) % cyclePoints;
    
    return aPoint * pixelPerPoint;
}

- (BOOL)pointAvailable:(NSInteger)pointIndex
{
    int pCount = self.pointsArray.count;
    return ((pCount > pointIndex) && ([self.pointsArray objectAtIndex:pointIndex] != NULL));
}

- (void)redraw
{
    [self setNeedsDisplay];
    self.clearsContextBeforeDrawing = YES;
    
}

#pragma mark -
#pragma mark GestureRecognizer


- (void)singleTapGestureRecognizer:(UIGestureRecognizer *)sender
{
   

}

- (void)doubleTapGestureRecognizer:(UIGestureRecognizer *)sender
{
}

- (void)longPressGestureRecognizerStateChanged:(UIGestureRecognizer *)sender
{
}

- (void)addGestureRecgonizer
{
    
}

- (void)dealloc {
    
    //	NSLog(@"Lead dealloced");
    
    self.liveMonitor = nil;
    
}




@end
