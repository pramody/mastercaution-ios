//
//  LeftMenuCustomTableViewCell.h
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 17/06/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftMenuCustomTableViewCell : UITableViewCell
@property (nonatomic, strong) IBOutlet UILabel *leftMenuItemName;
@property (nonatomic, strong) IBOutlet UIImageView *leftMenuItemIcon;
@property (nonatomic, strong) IBOutlet UILabel *leftMenuBadgeIcon;
@end
