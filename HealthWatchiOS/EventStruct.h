//
//  EventStruct.h
//  HealthWatch
//
//  Created by METSL MAC MINI on 28/12/15.
//  Copyright © 2015 METSL MAC MINI. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EventStruct : NSObject

@property (nonatomic, readwrite)  enum ComparisionType comparisionType;
@property (nonatomic, readwrite) int code;
@property (nonatomic, readwrite) float value;
@end
