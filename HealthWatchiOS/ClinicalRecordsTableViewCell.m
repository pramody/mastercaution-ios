//
//  ClinicalRecordsTableViewCell.m
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 22/06/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import "ClinicalRecordsTableViewCell.h"

@implementation ClinicalRecordsTableViewCell
@synthesize tempLBL,expOrCollButton,eventNameTitleLBL,eventTimeTitleLBL,detectedExpandView,detectedTitleLBL,topShowView,viewLiveEcgButton,manualExpandView,manualTitleLBL,manualBpLBL,manualGlcoseLBL,manualWgtLBL,detectedHrLBL,detectedPRLBL,detectedQRSLBL,detectedQTcLBL,detectedQTLBL,detectedRespLBL,detectedRespvarLBL,detectedRRLBL,detectedTempLBL,postureimage,manualTextView,detectedTextView,tempunits,detectedEventType,detectedReceiveType,manualReceiveType,detectedHeaderRespLBL,detectedRespImage,detectedRespunit,manualOximetryLBL,detectedHeaderRRLBL,detectedRRImage,detectedRRunit,detectedHeaderRespVarLBL,detectedRespVarunit,detectedRespVarImage;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
