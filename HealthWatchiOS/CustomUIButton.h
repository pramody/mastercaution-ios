//
//  CustomUIButton.h
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 23/06/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomUIButton : UIButton



@property (nonatomic, strong) IBOutlet NSIndexPath * indexPathCust;
@property (nonatomic, strong) IBOutlet UITableView * tabelViewCust;

@property (nonatomic, readwrite) int buttnSection;
@property (nonatomic, readwrite) int buttnRow;


@end
