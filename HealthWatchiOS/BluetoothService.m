//
//  BluetoothService.m
//  HealthWatch
//
//  Created by METSL MAC MINI on 02/07/16.
//  Copyright © 2016 METSL MAC MINI. All rights reserved.
//

#import "BluetoothService.h"

@implementation BluetoothService
- (void)doInBackground{
    NSLog(@"Blueooth service time remaining = %.1f seconds", [UIApplication sharedApplication].backgroundTimeRemaining);
    NSLog(@"Blueooth Service running at %.1ld seconds", [self getCurrentNetworkTime]);
    
    GlobalVars *global=[GlobalVars sharedInstance];
    if(global.BluetoothConnectionflag==1)
    {
        ConfigureMCDController *confgg=[[ConfigureMCDController alloc]init];
        [confgg startbluetoothstreaming];
        
    }
    
    if( [UIApplication sharedApplication].backgroundTimeRemaining<10)
    {
        [ global.bluetoothService startService];
    }
    
   
    
}


- (long) getCurrentNetworkTime{
    return ([[NSDate date] timeIntervalSince1970]);
}


@end
