//
//  codes.h
//  HealthWatch
//
//  Created by METSL MAC MINI on 20/08/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#ifndef HealthWatch_codes_h
#define HealthWatch_codes_h

 static int USERVALIDATION = 1001;
 static int ACTIVATION = 1002;
 static int PATIENT_DETAILS = 1003;
 static int BP_WEIGHT_DETAILS = 1004;
 static int ST_ELEVATION_VALUES = 2001;
 static int ST_DEPRESSION_VALUES = 2002;
 static int T_WAVE_VALUE = 2003;
 static int Q_WAVE_VALUE = 2004;
 static int PROLNGED_QT_VALUES = 2005;
 static int QRS_WIDTH_VALUES = 2006;
 static int NARROW_QRS_REGULAR_D_DESCRIPTION = 2007;//supraventricular
 static int NARROW_QRS_IR_REGULAR_D_DESCRIPTION = 2008;
 static int NARROW_QRS_REGULAR_RELATIVE_DESCRIPTION = 2009;
 static int NARROW_QRS_IR_REGULAR_RELATIVE_DESCRIPTION = 2010;

 static int WIDE_QRS_REGULAR_D_DESCRIPTION = 2011;//ventricular tachycardia
 static int WIDE_QRS_IR_REGULAR_D_DESCRIPTION = 2012;
 static int WIDE_QRS_REGULAR_RELATIVE_DESCRIPTION = 2013;
 static int WIDE_QRS_IR_REGULAR_RELATIVE_DESCRIPTION = 2014;
 static int PVC_VALUE = 2015;

 static int ASYSTOLE_VALUE = 2016;
 static int BRADICARDIA_REGULAR_D_DESCRIPTION = 2017;
 static int BRADICARDIA_IR_REGULAR_D_DESCRIPTION = 2018;
 static int BRADICARDIA_REGULAR_RELATIVE_DESCRIPTION = 2019;
 static int BRADICARDIA_IR_REGULAR_RELATIVE_DESCRIPTION = 2020;

 static int APNEA_VALUE = 2021;
 static int TEMPERATURE_VALUE = 2022;
 static int FALL_EVENT_VALUE = 2023;
 static int NO_MOTION_VALUE = 2024;
 static int RESPIRATION_RATE_VALUE = 2025;
 static int PAUSE = 2026;

 static int QRS_WIDE_WIDTH=2027;
 static int VTACH=2028;
 static int TRIPLET=2029;
 static int COUPLET=2030;
 static int TRIGEMINY=2031;
 static int BIGEMINY =2032;
 static int UNKNOWM =2033;
 static int MANUAL =2034;

 static int HIGH_RESPIRATION_RATE=2035;
 static int LOW_RESPIRATION_RATE =2036;
 static int HIGH_RESPIRATION_AMPLITUDE =2037;
 static int LOW_RESPIRATION_AMPLITUDE =2038;
 static int RESPIRATION_VARIANCE =2039;

 static int OTHER_MANUAL_EVENT =2040;

 static int HR_SUSPENSION =2041;
 static int ST_SUSPENSION =2042;
 static int ON_DEMAND =2043;

 static int TACHYCARDIA =2044;

 static int MCD_SETTINGS = 3001;

//posture code
#define LayingPostureCode 1
#define RunningPostureCode 2
#define WalkingPostureCode 3
#define StandingSittingPostureCode 4

enum CurrentSetting {
    
    NONE , MCDSETTING , ARRYTHMIA, ISCHMIA, OTHER , SUSPENSION , SYNC
    
};

enum ComparisionType {
    NORMAL, INVERSE, NOO
};




//ecg codes
#define L1  0
#define L2  1
#define L3  2
#define AVR 3
#define AVL 4
#define AVF 5
#define V1  6
#define V2  7
#define V3  8
#define V4  9
#define V5  10
#define V6  11
#define RESP 12
#define X  13
#define Y  14
#define Z  15





#endif
