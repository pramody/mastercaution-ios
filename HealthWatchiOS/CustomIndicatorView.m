//
//  CustomIndicatorView.m
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 03/07/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import "CustomIndicatorView.h"

@implementation CustomIndicatorView

-(void) startLoading:(UIView *)parentView
{
    parentView.hidden = false;
    parentView.backgroundColor = [[UIColor lightGrayColor]colorWithAlphaComponent:0.3];
    
    self.bounds = CGRectMake(0, 0, 45, 45);
    self.tintColor = UIColor.whiteColor;
   // self.center = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds));
  
    self.backgroundColor = UIColor.clearColor;
    [self startAnimating];
}

-(void)stopLoading:(UIView *)parentView
{
    parentView.hidden = true;
    [self stopAnimating];
}

@end
