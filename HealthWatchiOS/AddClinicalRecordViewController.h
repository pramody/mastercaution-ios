//
//  AddClinicalRecordViewController.h
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 01/07/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import "CustomIndicatorView.h"
#import "LocationHandler.h"
enum
{
 SYSTOLIC   =   0,
 DAISTOLIC  =   1,
 OXIMETRY   =   2,
 WEIGHT     =   3,
 BMI        =   4,
 GLUCOSE    =   5
};typedef NSInteger validFields;

@interface AddClinicalRecordViewController : UIViewController<UITextFieldDelegate,LocationHandlerDelegate>
{
   
    UIColor *blueColorMainAppUIColor;
    UIColor *backGroundGrayColor;
    NSString * patientid;
    NSString *deviceid;
    NSString *latitude,*longititude;
     NSString *location;
}


// IBOutlets Of View Controller
@property (nonatomic, strong) IBOutlet UIView* headerBlueView;
@property (nonatomic, strong) IBOutlet UIScrollView * mainScrollView;
@property (nonatomic, strong) IBOutlet UITextField * systolicTextBox;
@property (nonatomic, strong) IBOutlet UITextField *  diastolicTextBox ;
@property (nonatomic, strong) IBOutlet UITextField *  oximetryTextBox ;
@property (nonatomic, strong) IBOutlet UITextField *  weightTextBox ;
@property (nonatomic, strong) IBOutlet UITextField *  glucoseTextBox ;
@property (nonatomic, strong) IBOutlet UILabel *  totalBmiLbl ;


@property (nonatomic, strong) IBOutlet NSMutableArray *clinicalDataArray;

@property (nonatomic, strong)IBOutlet CustomIndicatorView *spinnerView;
@property (nonatomic, strong)IBOutlet UIView * indicatorMainBlockView;


@end
