//
//  MenuTableController.h
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 17/06/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//



@interface MenuTableController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    NSDictionary *leftMenuSideItemsNSDic;
    NSString *userlevel;
    BOOL standalone_Mode;
    UIActivityIndicatorView* spinner;
    NSString * badgecnt;
    int direction;
    int shakes;
    NSString * patientid;
    NSString *deviceid;
    NSString *doctorname;
}

@property (nonatomic, strong) IBOutlet NSArray *leftMenuSideItemsNSArr;
@property (nonatomic, strong) IBOutlet UITableView *leftMenuTableView;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView * spinner;
@property (nonatomic, strong) IBOutlet UILabel * lblspin;

@end
