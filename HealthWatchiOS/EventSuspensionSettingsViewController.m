//
//  EventSuspensionSettingsViewController.m
//  HealthWatchiOS
//
//  Created by METSL MAC MINI on 01/07/15.
//  Copyright (c) 2015 METSL MAC MINI. All rights reserved.
//

#import "EventSuspensionSettingsViewController.h"


#import "SDCAlertView.h"
#import "SDCAlertViewController.h"
#import <UIView+SDCAutoLayout.h>
#import <AudioToolbox/AudioToolbox.h>
#import "SuspensionSettings.h"
#import "DoctorProfileViewController.h"
#import "WCFUpload.h"


@interface EventSuspensionSettingsViewController ()
{
UIImageOrientation scrollOrientation;
CGPoint lastPos;
}
@end

@implementation EventSuspensionSettingsViewController
@synthesize headerBlueView,tableViewMcdSettingsCell,tableViewMcdSettingsTable,spinner,lockunlocButton;

NSMutableArray* listingItemsDataMain1;
NSMutableArray* cellRowStatus1;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    AppDelegate * appDelegate = (AppDelegate *)UIApplication.sharedApplication.delegate ;
    appDelegate.orientationKey = @"PRO";
    [appDelegate forceOrientationTakePlace:appDelegate.orientationKey];
    self.slidingViewController.topViewAnchoredGesture = ECSlidingViewControllerAnchoredGestureTapping | ECSlidingViewControllerAnchoredGesturePanning;
    self.slidingViewController.customAnchoredGestures =@[];
    
    listingItemsDataMain1=[[NSMutableArray alloc] init];
    cellRowStatus1=[[NSMutableArray alloc]init];
   
    
   /* filterStatusArray   =  @[@"OFF", @"50", @"60"];
   // mcdGainArray                    = @[@"0.5", @"1", @"1.5", @"2"];
    mcgLeadSettingArray             =  @[@"3",@"5", @"12", @"15"];
    mcdEventDetectionLeadArray      =   @[@"II", @"V2", @"V5"];
    mcgSamplingRatesArray           =  @[@"125", @"200", @"500", @"1000"];*/
    
    NSMutableArray *arrayData_two=[[NSMutableArray alloc]init];
    int i;
    for(i=0;i<=600;i++)
    {
        [arrayData_two addObject:[NSNumber numberWithInt:i]];
    }
    mcdGainArray=arrayData_two.copy;
    
    [self prepareDataForTabelView];

    [self setupInitialValue];
      [self fetchingpatientdata];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(DoctorLoginStatus:) name:RWT_DOCTOR_LOGIN_NOTIFICATION object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    GlobalVars *global = [GlobalVars sharedInstance];
    if(global.dropdownValidFlag)
        lockunlocButton.selected=true;
    
    [self Twavestatus];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupInitialValue
{
    blueColorMainAppUIColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"blue_bg_full.png"]];
     headerBlueView.backgroundColor = blueColorMainAppUIColor;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    standalone_Mode = [defaults boolForKey:@"ActiveStandalonemode"];
}

-(IBAction)goToDashboard:(id)sender{
    self.slidingViewController.topViewController.view.layer.transform = CATransform3DMakeScale(1, 1, 1);
    self.slidingViewController.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"DashboardStoryboardId"];
}

-(IBAction)leftSideButtonAction:(id)sender{
    [self.slidingViewController anchorTopViewToRightAnimated:YES];
}




- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}



-(void) prepareDataForTabelView
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"SuspensionSettings" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSString *ecodevalue,*suspensiontime;
    
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    NSMutableDictionary *filterStatusMuDic = [[NSMutableDictionary alloc] init];
     NSMutableDictionary *ecgSettingsMuDic = [[NSMutableDictionary alloc] init];
     NSMutableDictionary *OtherEventMuDic = [[NSMutableDictionary alloc] init];
    
    //filter
     NSMutableDictionary *filterStatusRowOne = [[NSMutableDictionary alloc] init];
     NSMutableDictionary *filterStatusRowTwo = [[NSMutableDictionary alloc] init];
     NSMutableDictionary *filterStatusRowThree = [[NSMutableDictionary alloc] init];
     NSMutableDictionary *filterStatusRowFour = [[NSMutableDictionary alloc] init];
     NSMutableDictionary *filterStatusRowFive = [[NSMutableDictionary alloc] init];
     NSMutableDictionary *filterStatusRowSix = [[NSMutableDictionary alloc] init];
     NSMutableDictionary *filterStatusRowSeven = [[NSMutableDictionary alloc] init];
     NSMutableDictionary *filterStatusRowEight = [[NSMutableDictionary alloc] init];
     NSMutableDictionary *filterStatusRowNine = [[NSMutableDictionary alloc] init];
     NSMutableDictionary *filterStatusRowTen = [[NSMutableDictionary alloc] init];
    
    
     //ecg
    NSMutableDictionary *ecgSettingsMuDicRowOne = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *ecgSettingsMuDicRowTwo = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *ecgSettingsMuDicRowThree = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *ecgSettingsMuDicRowFour = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *ecgSettingsMuDicRowFive = [[NSMutableDictionary alloc] init];
    
    //3rd row
    /* NSMutableDictionary *OtherSettingsMuDicRowOne = [[NSMutableDictionary alloc] init];
     NSMutableDictionary *OtherSettingsMuDicRowTwo = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *OtherSettingsMuDicRowThree = [[NSMutableDictionary alloc] init];*/
    NSMutableDictionary *OtherSettingsMuDicRowFour = [[NSMutableDictionary alloc] init];
    
    if (error) {
      //  NSLog(@"Unable to execute fetch request.");
       // NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
       // NSLog(@"%@", result);
        if (result.count > 0) {
            
            for(int j=0;j<result.count;j++)
            {
                NSManagedObject *suspenssett = (NSManagedObject *)[result objectAtIndex:j];
                //NSLog(@"1 - %@", suspenssett);
                ecodevalue=[[suspenssett valueForKey:@"event_code"]stringValue];
                suspensiontime=[[suspenssett valueForKey:@"suspension_time"]stringValue];
            [filterStatusMuDic setObject:@"Arrhythmia Suspension Time Settings (seconds)" forKey:@"title"];
            [filterStatusMuDic setObject:@"ecg_settings.png" forKey:@"image_name"];
            
            
           
            if([ecodevalue isEqualToString:@"2007"])
            {
                [filterStatusRowOne setObject:@"Supra Ventricular Tachycardia" forKey:@"title"];
                [filterStatusRowOne setObject:suspensiontime forKey:@"value"];
                [filterStatusRowOne setObject:mcdGainArray forKey:@"list"];
            }
            
         
            if([ecodevalue isEqualToString:@"2011"])
            {
                [filterStatusRowTwo setObject:@"Ventricular Tachycardia" forKey:@"title"];
                [filterStatusRowTwo setObject:suspensiontime forKey:@"value"];
                [filterStatusRowTwo setObject:mcdGainArray forKey:@"list"];
            }
             
            
           
            if([ecodevalue isEqualToString:@"2017"])
            {
                [filterStatusRowThree setObject:@"Bradycardia" forKey:@"title"];
                [filterStatusRowThree setObject:suspensiontime forKey:@"value"];
                [filterStatusRowThree setObject:mcdGainArray forKey:@"list"];
            }
                
            
           
            if([ecodevalue isEqualToString:@"2026"])
            {
                [filterStatusRowFour setObject:@"Pause" forKey:@"title"];
                [filterStatusRowFour setObject:suspensiontime forKey:@"value"];
                [filterStatusRowFour setObject:mcdGainArray forKey:@"list"];
            }
           
            
            
            
            if([ecodevalue isEqualToString:@"2015"])
            {
                [filterStatusRowFive setObject:@"PVC" forKey:@"title"];
                [filterStatusRowFive setObject:suspensiontime forKey:@"value"];
                [filterStatusRowFive setObject:mcdGainArray forKey:@"list"];
            }
           
            
            
            if([ecodevalue isEqualToString:@"2016"])
            {
                [filterStatusRowSix setObject:@"Asystole" forKey:@"title"];
                [filterStatusRowSix setObject:suspensiontime forKey:@"value"];
                [filterStatusRowSix setObject:mcdGainArray forKey:@"list"];
            }
           
            
           
            if([ecodevalue isEqualToString:@"2032"])
            {
                [filterStatusRowSeven setObject:@"Bigemeny" forKey:@"title"];
                [filterStatusRowSeven setObject:suspensiontime forKey:@"value"];
                [filterStatusRowSeven setObject:mcdGainArray forKey:@"list"];
            }
           
            
            
            if([ecodevalue isEqualToString:@"2031"])
            {
                [filterStatusRowEight setObject:@"Trigeminy" forKey:@"title"];
                [filterStatusRowEight setObject:suspensiontime forKey:@"value"];
                [filterStatusRowEight setObject:mcdGainArray forKey:@"list"];
            }
            
            
            
            
            if([ecodevalue isEqualToString:@"2030"])
            {
                [filterStatusRowNine setObject:@"Couplet" forKey:@"title"];
                [filterStatusRowNine setObject:suspensiontime forKey:@"value"];
                [filterStatusRowNine setObject:mcdGainArray forKey:@"list"];
            }
           
            
            
            if([ecodevalue isEqualToString:@"2029"])
            {
                [filterStatusRowTen setObject:@"Triplet" forKey:@"title"];
                [filterStatusRowTen setObject:suspensiontime forKey:@"value"];
                [filterStatusRowTen setObject:mcdGainArray forKey:@"list"];
            }
          
                
        }
            NSMutableArray *filterStatusAllRows = [NSMutableArray arrayWithObjects:filterStatusRowOne,filterStatusRowTwo,filterStatusRowThree,filterStatusRowFour,filterStatusRowFive,filterStatusRowSix,filterStatusRowSeven,filterStatusRowEight,filterStatusRowNine,filterStatusRowTen, nil];
            [filterStatusMuDic setObject:filterStatusAllRows forKey:@"rows"];
          
            
         
            
            
            // 2nd Section FILTER STATUS
            
            for(int j=0;j<result.count;j++)
            {
                NSManagedObject *suspenssett = (NSManagedObject *)[result objectAtIndex:j];
               // NSLog(@"1 - %@", suspenssett);
                ecodevalue=[[suspenssett valueForKey:@"event_code"]stringValue];
                suspensiontime=[[suspenssett valueForKey:@"suspension_time"]stringValue];
            [ecgSettingsMuDic setObject:@"Ischemia Suspension Time settings (seconds)" forKey:@"title"];
            [ecgSettingsMuDic setObject:@"ecg_settings.png" forKey:@"image_name"];
            
            
            if([ecodevalue isEqualToString:@"2001"])
            {
                [ecgSettingsMuDicRowOne setObject:@"ST Elevation" forKey:@"title"];
                [ecgSettingsMuDicRowOne setObject:suspensiontime forKey:@"value"];
                [ecgSettingsMuDicRowOne setObject:mcdGainArray forKey:@"list"];
            }
                
          
            if([ecodevalue isEqualToString:@"2002"])
            {
                [ecgSettingsMuDicRowTwo setObject:@"ST Depression" forKey:@"title"];
                [ecgSettingsMuDicRowTwo setObject:suspensiontime forKey:@"value"];
                [ecgSettingsMuDicRowTwo setObject:mcdGainArray forKey:@"list"];
            }
            
            
            if([ecodevalue isEqualToString:@"2005"])
            {
                [ecgSettingsMuDicRowThree setObject:@"Prolonged QT" forKey:@"title"];
                [ecgSettingsMuDicRowThree setObject:suspensiontime forKey:@"value"];
                [ecgSettingsMuDicRowThree setObject:mcdGainArray forKey:@"list"];
            }
            
            
            
            if([ecodevalue isEqualToString:@"2027"])
            {
                [ecgSettingsMuDicRowFour setObject:@"QRS Wide" forKey:@"title"];
                [ecgSettingsMuDicRowFour setObject:suspensiontime forKey:@"value"];
                [ecgSettingsMuDicRowFour setObject:mcdGainArray forKey:@"list"];
            }
                
            
            if([ecodevalue isEqualToString:@"2003"])
            {
                [ecgSettingsMuDicRowFive setObject:@"T Wave" forKey:@"title"];
                [ecgSettingsMuDicRowFive setObject:suspensiontime forKey:@"value"];
                [ecgSettingsMuDicRowFive setObject:mcdGainArray forKey:@"list"];
            }
            
        }
            NSMutableArray *ecgSettingsMuDicAllRows = [NSMutableArray arrayWithObjects:ecgSettingsMuDicRowOne,ecgSettingsMuDicRowTwo,ecgSettingsMuDicRowThree,ecgSettingsMuDicRowFour,ecgSettingsMuDicRowFive, nil];
            
            [ecgSettingsMuDic setObject:ecgSettingsMuDicAllRows forKey:@"rows"];
            
            
            // 2nd Section FILTER STATUS
            
            for(int j=0;j<result.count;j++)
            {
                NSManagedObject *suspenssett = (NSManagedObject *)[result objectAtIndex:j];
               // NSLog(@"1 - %@", suspenssett);
                ecodevalue=[[suspenssett valueForKey:@"event_code"]stringValue];
                suspensiontime=[[suspenssett valueForKey:@"suspension_time"]stringValue];
            [OtherEventMuDic setObject:@"Other Suspension Time Settings(seconds)" forKey:@"title"];
            [OtherEventMuDic setObject:@"ecg_settings.png" forKey:@"image_name"];
            
           
          /*  if([ecodevalue isEqualToString:@"2022"])
            {
                [OtherSettingsMuDicRowOne setObject:@"Temperature" forKey:@"title"];
                [OtherSettingsMuDicRowOne setObject:suspensiontime forKey:@"value"];
                [OtherSettingsMuDicRowOne setObject:mcdGainArray forKey:@"list"];
            }
                
           
            if([ecodevalue isEqualToString:@"2036"])
            {
                [OtherSettingsMuDicRowTwo setObject:@"Respiration Rate" forKey:@"title"];
                [OtherSettingsMuDicRowTwo setObject:suspensiontime forKey:@"value"];
                [OtherSettingsMuDicRowTwo setObject:mcdGainArray forKey:@"list"];
            }
           
            
            if([ecodevalue isEqualToString:@"2021"])
            {
                [OtherSettingsMuDicRowThree setObject:@"Apnea" forKey:@"title"];
                [OtherSettingsMuDicRowThree setObject:suspensiontime forKey:@"value"];
                [OtherSettingsMuDicRowThree setObject:mcdGainArray forKey:@"list"];
            }
           
            */
            
            if([ecodevalue isEqualToString:@"2024"])
            {
                [OtherSettingsMuDicRowFour setObject:@"No Motion" forKey:@"title"];
                [OtherSettingsMuDicRowFour setObject:suspensiontime forKey:@"value"];
                [OtherSettingsMuDicRowFour setObject:mcdGainArray forKey:@"list"];
            }
          
            
        }
            
          /*  NSMutableArray *OtherSettingsMuDicAllRows = [NSMutableArray arrayWithObjects:OtherSettingsMuDicRowOne,OtherSettingsMuDicRowTwo,OtherSettingsMuDicRowThree,OtherSettingsMuDicRowFour,nil];*/
                
            NSMutableArray *OtherSettingsMuDicAllRows = [NSMutableArray arrayWithObjects:OtherSettingsMuDicRowFour,nil];
            [OtherEventMuDic setObject:OtherSettingsMuDicAllRows forKey:@"rows"];

           
        }
        
    }

  //  NSLog(@"%@",ecodevalue);
  //  NSLog(@"%@",suspensiontime);
    // 1st Section FILTER STATUS
    
    
    
    //NSMutableArray *listingItemsDataMain = [[NSMutableArray alloc] init];
   
    [listingItemsDataMain1 addObject:filterStatusMuDic];
    [listingItemsDataMain1 addObject:ecgSettingsMuDic];
    [listingItemsDataMain1 addObject:OtherEventMuDic];
    
    [self prepareRowStatus];
    
}

-(void) prepareRowStatus
{
    if (listingItemsDataMain1.count > 0)
    {
        if (cellRowStatus1 == nil) {
            // cellRowStatus = NSMutableArray()
        } else {
            [cellRowStatus1 removeAllObjects];
        }
        NSMutableArray *rowsCount;
        int countSection = 0;
        for(NSDictionary * eachObj in listingItemsDataMain1)
        {
           // NSLog(@"eachObj %@",eachObj);
            if([[eachObj objectForKey:@"rows" ] count] > 0)
            {
                rowsCount = [[NSMutableArray alloc] init];
                for(NSString* eachChildObj in [eachObj objectForKey:@"rows"])
                {
                    [rowsCount addObject:[NSNumber numberWithInt: 0]];
                }
                
            }
            [cellRowStatus1 insertObject:rowsCount atIndex:countSection];
            countSection++;
        }
    }
    
  //  NSLog(@"Current Data Row Status %@",cellRowStatus1);
}

-(IBAction)lockUnlockAction:(UIButton *)sender {
    
     GlobalVars *global = [GlobalVars sharedInstance];
    if (sender.selected == false)
    {
        
        // --- : Before Unlocking the setting lock, Doctor authentication is mandatory in case of [MCM is conneted with MCC] and bypass [Doctor authentication] in case of stand alone mode
        if(global.BluetoothConnectionflag==2)
        {
            [self presentAlertViewForPassword];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                            message:@"Please Connect MCD Device"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }

    }
    else
    {
        global.dropdownValidFlag=false;
        lockunlocButton.selected=false;
        [self DoctorUniversalLoginTimer:NO];
    }
    
    
    
}


-(void) hideKeyboard
{
    [self.view endEditing:true];
}







// MARK: - UITableView Delegates and DataSource


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return listingItemsDataMain1.count;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    int noOfRow = (int)[[[listingItemsDataMain1 objectAtIndex:section]valueForKey:@"rows"]count];
    return noOfRow;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    if([cellRowStatus1[indexPath.section][indexPath.row]intValue] == 1)
    {
        return 257;
    }
    
    return 64;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 26;
}





- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView * headerView =[[UIView alloc] initWithFrame:CGRectMake(10, 0, 320, 26)];
    headerView.backgroundColor = blueColorMainAppUIColor;
    
    NSString * iconName = [listingItemsDataMain1[section] objectForKey:@"image_name"];
    NSString * titleName = [listingItemsDataMain1[section] objectForKey:@"title"];
    
    UIImageView * headerIcon  =  [[UIImageView alloc] initWithFrame:CGRectMake(6, 6, 14, 14)];
    headerIcon.image                =  [UIImage imageNamed:iconName];
    [headerView addSubview:headerIcon];
    
    
    UILabel * headerLbl  =   [[UILabel alloc] initWithFrame:CGRectMake(25,5,300,15)];
    headerLbl.backgroundColor   =   [UIColor clearColor];
    headerLbl.text              =   titleName; //(allClinicalDataArray[section]["date"] as NSString)
    headerLbl.font              = [UIFont fontWithName:@"Roboto-Medium" size:13]; //Roboto-Bold, Roboto-Condensed 15.0
    headerLbl.textColor         =   [UIColor whiteColor];
    [headerView addSubview:headerLbl];
    
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}



/*- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
 //        var headerView : UIView = UIView(frame: CGRectMake(15, 15, 250, 30.6))
 //        headerView.backgroundColor = UIColor.yellowColor()
 
 
 return UIView;
 }*/


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString * itemName, *itemValue,*cellIdentifier;
    // NEW ----
   
    
        cellIdentifier = @"McdSettingsCellID2";
    
    
    tableViewMcdSettingsCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(tableViewMcdSettingsCell == nil)
    {
        tableViewMcdSettingsCell = [[McdSettingsTableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
    }
    
    NSArray * allRowData  = [listingItemsDataMain1[indexPath.section] objectForKey:@"rows"];
    if(allRowData.count > 0)
    {
        
        
        itemName        =   [[allRowData objectAtIndex:[indexPath row]] objectForKey:@"title"];
        
       
            // For Mcd Setting Cell -- For Rest Row
            
            itemValue       =   [[allRowData objectAtIndex:[indexPath row]]objectForKey:@"value"];
        
            if(indexPath.section == 0)
            {
               
                    itemValue =[NSString stringWithFormat:@"%@",itemValue];
            [tableViewMcdSettingsCell.oneForAllPickerViewControl selectRow:[itemValue intValue]   inComponent:0 animated:false];
                
            }
            else if( indexPath.section == 1 )
            {
                itemValue = [NSString stringWithFormat:@"%@",itemValue];
                 [tableViewMcdSettingsCell.oneForAllPickerViewControl selectRow:[itemValue intValue]   inComponent:0 animated:false];
            }
            else if( indexPath.section == 2)
            {
                itemValue =[NSString stringWithFormat:@"%@",itemValue];
                 [tableViewMcdSettingsCell.oneForAllPickerViewControl selectRow:[itemValue intValue]   inComponent:0 animated:false];
            }
        
        
        // --- Assigning [Item Name, Item Value]
        
        if([itemName isEqualToString:@"T Wave"])
        {
            if([_TWave_Status isEqualToString:@"0"])
            {
            tableViewMcdSettingsCell.rowSettingTitle.textColor=[UIColor lightGrayColor];
            }
        }
        tableViewMcdSettingsCell.rowSettingTitle.text  =   itemName;
        tableViewMcdSettingsCell.rowSettingValue.text   =  itemValue ;
        
        // --- Expand View To Show and Add elements
        BOOL valueIs  =   true;
        CGFloat angleValue  =   6.14;
        if([cellRowStatus1[indexPath.section][indexPath.row]intValue] == 1)
        {
            // if cell is detected and cell status is 1 = Expand that the height will be 257 else height will be 64
            valueIs     =   false;
            angleValue  =   7.85;
        }
        
        tableViewMcdSettingsCell.rightArrowBlackImg.transform = CGAffineTransformMakeRotation(angleValue);
        tableViewMcdSettingsCell.expandView.hidden = valueIs;
        
        
        
        // Below Code is for Expand View for Both Cell1 and Cell2, As per condition.
        
      
            // In these cells we have all uipicker as user will expand the view
            
            NSArray * pickerDataArray =  [[allRowData objectAtIndex:[indexPath row]]objectForKey:@"list"];
            
           // NSLog(@"inff%ld",(long)indexPath.row);
            
            tableViewMcdSettingsCell.oneForAllPickerViewControl.delegate        = self;
            //tableViewMcdSettingsCell.oneForAllPickerViewControl.dataSource  = self;
            tableViewMcdSettingsCell.oneForAllPickerViewControl.tableRow      =  (int)indexPath.row;
            tableViewMcdSettingsCell.oneForAllPickerViewControl.tableSection  =  (int)indexPath.section;
            tableViewMcdSettingsCell.oneForAllPickerViewControl.passNsArray   =  pickerDataArray;
           [tableViewMcdSettingsCell.oneForAllPickerViewControl selectRow:[itemValue intValue]   inComponent:0 animated:false];
            [tableViewMcdSettingsCell.oneForAllPickerViewControl reloadAllComponents];
            
            
            
      
        
        
        
        
        
    } // End if Check the count of RowData
    
    
    // NEW ----
    
    
    
    tableViewMcdSettingsCell.clipsToBounds = true;
    
    return tableViewMcdSettingsCell;
}

- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.isDragging) {
        UIView *myView = cell.contentView;
        CALayer *layer = myView.layer;
        CATransform3D rotationAndPerspectiveTransform = CATransform3DIdentity;
        rotationAndPerspectiveTransform.m34 = 1.0 / -1000;
        if (scrollOrientation == UIImageOrientationDown) {
            rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, M_PI*0.5, 1.0f, 0.0f, 0.0f);
        } else {
            rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, -M_PI*0.5, 1.0f, 0.0f, 0.0f);
        }
        layer.transform = rotationAndPerspectiveTransform;
        [UIView animateWithDuration:.8 animations:^{
            layer.transform = CATransform3DIdentity;
        }];
        
    }
}




- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    GlobalVars *global = [GlobalVars sharedInstance];
    // Either the cell can be "TableViewDetectedCellID" or "TableViewManualCellID"
    
    //println(" Detected tap ")
    
    
        // SET 0 = CLOSE
        // SET 1 = OPEN
    
    indexpthsec=[indexPath section];
    indexrw=[indexPath row];
    
    global.indexpathsection=indexpthsec;
    global.indexpathrow=indexrw;
    
    NSLog(@"index%lu%lu",(unsigned long)indexpthsec,(unsigned long)indexrw);
        if(indexpthsec==1&&indexrw==4)//inverted t condition
        {
            
            if([_TWave_Status isEqualToString:@"0"])
                    {
                        
                    }
                    else
                    {
                        int rowStatus           =  [cellRowStatus1[indexPath.section][indexPath.row]intValue] ;// [cellRowStatus:indexPath.section indexPath.row intValue];
                        
                        
                        // We are not using Else Block as bt default it is Else
                        // Row is Alreay OPEN, Now CLOSE it and set the value to 0, indicating it as CLOSE
                        int newUpdatedRowStatus =   0;
                        
                        if( rowStatus == 0 )
                        {
                            // Row is Already ClOSE, Now OPEN it and set the value to 1, indicating it as OPEN
                            newUpdatedRowStatus =   1;
                            if(!global.dropdownValidFlag)
                            {
                                
                                if(global.BluetoothConnectionflag==2)
                                {
                                    [self presentAlertViewForPassword];
                                }
                                else
                                {
                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                                                    message:@"Please Connect MCD Device"
                                                                                   delegate:nil
                                                                          cancelButtonTitle:@"OK"
                                                                          otherButtonTitles:nil];
                                    [alert show];
                                }
                            }
                            // Open a row here
                            //self.assignIndividualArrayToCommonArray(indexPath)
                        }
                        
                        
                        
                        
                        
                        //  ########  Make the Last IndexPath to Zero which will close the Cell
                        if (lastOpenIndexPath != nil)
                        {
                            [cellRowStatus1[lastOpenIndexPath.section] replaceObjectAtIndex:lastOpenIndexPath.row withObject:[NSNumber numberWithInteger:0]];
                            tableViewMcdSettingsCell = [tableView cellForRowAtIndexPath:lastOpenIndexPath];
                            tableViewMcdSettingsCell.expandView.hidden = true;
                            tableViewMcdSettingsCell.rightArrowBlackImg.transform = CGAffineTransformMakeRotation(6.14);
                            [self.tableViewMcdSettingsTable reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                            //println("Old Index \(lastOpenIndexPath.section) ----  \(lastOpenIndexPath.row)")
                        }
                        //println("Current Index \(indexPath.section) ----  \(indexPath.row)")
                        
                        
                        
                        //  ########   Update the Current Index Path
                        if(global.dropdownValidFlag)
                        {
                            //dropdownValidFlag=false;
                            if(global.BluetoothConnectionflag==2)
                            {
                            [cellRowStatus1[indexPath.section] replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithInteger:newUpdatedRowStatus]];
                            [self.tableViewMcdSettingsTable reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                            [self.tableViewMcdSettingsTable scrollToRowAtIndexPath :indexPath atScrollPosition:UITableViewScrollPositionTop animated: true];
                            
                            // Storing LastOpen IndexPath and Store the current one
                            lastOpenIndexPath = indexPath;
                            }
                            else
                            {
                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                                                message:@"Please Connect MCD Device"
                                                                               delegate:nil
                                                                      cancelButtonTitle:@"OK"
                                                                      otherButtonTitles:nil];
                                [alert show];
                            }
                        }

                    }
            
        }
       else
       {
        int rowStatus           =  [cellRowStatus1[indexPath.section][indexPath.row]intValue] ;// [cellRowStatus:indexPath.section indexPath.row intValue];
        
        
        // We are not using Else Block as bt default it is Else
        // Row is Alreay OPEN, Now CLOSE it and set the value to 0, indicating it as CLOSE
        int newUpdatedRowStatus =   0;
        
        if( rowStatus == 0 )
        {
            // Row is Already ClOSE, Now OPEN it and set the value to 1, indicating it as OPEN
            newUpdatedRowStatus =   1;
            if(!global.dropdownValidFlag)
            {
                
                if(global.BluetoothConnectionflag==2)
                {
                    [self presentAlertViewForPassword];
                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                                    message:@"Please Connect MCD Device"
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];
                }

            }
            // Open a row here
            //self.assignIndividualArrayToCommonArray(indexPath)
        }
        
        
        
        
        
        //  ########  Make the Last IndexPath to Zero which will close the Cell
        if (lastOpenIndexPath != nil)
        {
            [cellRowStatus1[lastOpenIndexPath.section] replaceObjectAtIndex:lastOpenIndexPath.row withObject:[NSNumber numberWithInteger:0]];
            tableViewMcdSettingsCell = [tableView cellForRowAtIndexPath:lastOpenIndexPath];
            tableViewMcdSettingsCell.expandView.hidden = true;
            tableViewMcdSettingsCell.rightArrowBlackImg.transform = CGAffineTransformMakeRotation(6.14);
            [self.tableViewMcdSettingsTable reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            //println("Old Index \(lastOpenIndexPath.section) ----  \(lastOpenIndexPath.row)")
        }
        //println("Current Index \(indexPath.section) ----  \(indexPath.row)")
        
        
        
        //  ########   Update the Current Index Path
    if(global.dropdownValidFlag)
    {
        //dropdownValidFlag=false;
        [cellRowStatus1[indexPath.section] replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithInteger:newUpdatedRowStatus]];
        [self.tableViewMcdSettingsTable reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [self.tableViewMcdSettingsTable scrollToRowAtIndexPath :indexPath atScrollPosition:UITableViewScrollPositionTop animated: true];
        
        // Storing LastOpen IndexPath and Store the current one
        lastOpenIndexPath = indexPath;
    }
       }
    
    //println("Cell Row Status \(cellRowStatus)")
    
}






//    MARK:  UIPICKER VIEW

- (NSInteger)numberOfComponentsInPickerView:(CustomUiPicker *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(CustomUiPicker *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [[pickerView passNsArray]count];
}

- (NSString*)pickerView:(CustomUiPicker *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    NSString * display;
    NSArray * pickerArrayDataForTitle = pickerView.passNsArray;
    if(pickerArrayDataForTitle.count > 0)
    {
       /* if(pickerView.tableSection == 1 && pickerView.tableRow == 0) // For NotchFilter only
        {
            display = [NSString stringWithFormat:@"%@",pickerArrayDataForTitle[row]];
            if (row != 0)
            {
                display = [NSString stringWithFormat:@"%@",pickerArrayDataForTitle[row]]; //Hz
            }
        }
        else
        {
            display = [NSString stringWithFormat:@"%@",pickerArrayDataForTitle[row]];;
        }*/
         display = [NSString stringWithFormat:@"%@",pickerArrayDataForTitle[row]];
    }
    
    return display;
}

- (void)pickerView:(CustomUiPicker *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    // NSLogger *logger = [[NSLogger alloc] init];
    
    NSArray *pickerArrayDataForTitle     = pickerView.passNsArray;
    NSString * selectedValue               = pickerArrayDataForTitle[row];
    int forSection                  =   pickerView.tableSection;
    int forRow                      = pickerView.tableRow;
    
    [[[[listingItemsDataMain1 objectAtIndex:forSection]objectForKey:@"rows"]objectAtIndex:forRow]setValue:selectedValue forKey:@"value"];
    
    [self.tableViewMcdSettingsTable reloadData];
    
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"SuspensionSettings" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSString *ecode,*suspensiontime,*ecodevalue;
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
       // NSLog(@"Unable to execute fetch request.");
       // NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
       // NSLog(@"%@", result);
        if (result.count > 0) {
            if(forSection==0)
            {
                switch (forRow) {
                    case 0:
                        ecode=@"2007";
                        suspensiontime=selectedValue;
                        ecodevalue=@"6";
                        break;
                    case 1:
                        ecode=@"2011";
                        suspensiontime=selectedValue;
                         ecodevalue=@"7";
                        break;
                    case 2:
                        ecode=@"2017";
                        suspensiontime=selectedValue;
                         ecodevalue=@"10";
                        break;
                    case 3:
                        ecode=@"2026";
                        suspensiontime=selectedValue;
                         ecodevalue=@"17";
                        break;
                    case 4:
                        ecode=@"2015";
                        suspensiontime=selectedValue;
                         ecodevalue=@"8";
                        break;
                    case 5:
                        ecode=@"2016";
                        suspensiontime=selectedValue;
                         ecodevalue=@"9";
                        break;
                    case 6:
                        ecode=@"2032";
                        suspensiontime=selectedValue;
                         ecodevalue=@"3";
                        break;
                    case 7:
                        ecode=@"2031";
                        suspensiontime=selectedValue;
                         ecodevalue=@"18";
                        break;
                    case 8:
                        ecode=@"2030";
                        suspensiontime=selectedValue;
                         ecodevalue=@"19";
                        break;
                    case 9:
                        ecode=@"2029";
                        suspensiontime=selectedValue;
                         ecodevalue=@"20";
                        break;
                        
                    default:
                        break;
                        
                }
            }
            else if(forSection==1)
            {
                switch (forRow) {
                    case 0:
                        ecode=@"2001";
                        suspensiontime=selectedValue;
                         ecodevalue=@"0";
                        break;
                    case 1:
                        ecode=@"2002";
                        suspensiontime=selectedValue;
                         ecodevalue=@"1";
                        break;
                    case 2:
                        ecode=@"2005";
                        suspensiontime=selectedValue;
                         ecodevalue=@"8";
                        break;
                    case 3:
                        ecode=@"2027";
                        suspensiontime=selectedValue;
                         ecodevalue=@"5";
                        break;
                    case 4:
                        ecode=@"2003";
                        suspensiontime=selectedValue;
                         ecodevalue=@"2";
                        break;
                    default:
                        break;
                        
                }
            
            }
            else
            {
                
                switch (forRow) {
                    /*case 0:
                        ecode=@"2022";
                        suspensiontime=selectedValue;
                         ecodevalue=@"12";
                        break;
                    case 1:
                        ecode=@"2036";
                        suspensiontime=selectedValue;
                         ecodevalue=@"15";
                        break;
                    case 2:
                        ecode=@"2021";
                        suspensiontime=selectedValue;
                         ecodevalue=@"11";
                        break;*/
                    case 0:
                        ecode=@"2024";
                        suspensiontime=selectedValue;
                         ecodevalue=@"14";
                        break;
                    default:
                        break;
                        
                }

            }
            
            
                GlobalVars *globals = [GlobalVars sharedInstance];
                if([globals.mcd_bluetoothstatus isEqualToString:@"0"])
                {
                    ConfigureMCDController *config=[[ConfigureMCDController alloc]init];
                    globals.suspsett_ecodevalue=ecode;
                    globals.suspsett_time=suspensiontime;
                    [config SET_SuspTime:[suspensiontime intValue] :[ecodevalue intValue]];
                }
                    
            
        
            
        }
   }

}


-(void)UpdateSuspensionSett:(int)data
{
    NSDate *date = [[NSDate alloc] init];
   // NSLog(@"%@", date);
    
    CommonHelper * commhelp=[[CommonHelper alloc] init];
    NSString *modifieddate=[commhelp getDateUTC:@"yyyy-MM-dd" :@"MM/dd/yyyy HH:mm:ss" :DateAsNSDate :@"2014-04-25" :date];
    NSMutableArray *results = [[NSMutableArray alloc]init];
    int flag=0;
    NSPredicate *pred;
    
    GlobalVars *globals = [GlobalVars sharedInstance];

    NSString* filter = @"%K == %@";
    NSArray* args = @[@"event_code",[globals suspsett_ecodevalue]];
    if ([globals suspsett_ecodevalue].length!=0) {
        pred = [NSPredicate predicateWithFormat:filter argumentArray:args];
        flag=1;
    } else {
        flag=0;
        NSLog(@"Enter Corect code empty");
        
    }


    if (flag == 1) {
        
       // NSLog(@"picate: %@",pred);
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:@"SuspensionSettings"];
        [fetchRequest setPredicate:pred];
        results = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if (results.count > 0) {
            NSManagedObject* suspensionsett = [results objectAtIndex:0];
            [suspensionsett setValue:[NSNumber numberWithInt:[[globals suspsett_ecodevalue] intValue]] forKey:@"event_code"];
            [suspensionsett setValue:[globals suspsett_time] forKey:@"suspension_time"];
            [suspensionsett setValue:@"MCM" forKey:@"modify_from"];
            [suspensionsett setValue:modifieddate forKey:@"modify_datetime"];
            [suspensionsett setValue:@"0" forKey:@"updated_to_site"];
             [suspensionsett setValue:@"1" forKey:@"update_to_device"];
            
            [self.managedObjectContext save:nil];
        } else {
           // NSLog(@"Enter Corect code number");
        }
    }
    
    WCFUpload *wcfcall=[[WCFUpload alloc]init];
    [wcfcall uploadSuspensionSetting1:[globals suspsett_time] :[globals suspsett_ecodevalue]];

}

- (void)presentAlertViewForPassword
{
    if(!standalone_Mode)
    {
        SDCAlertView *alert = [[SDCAlertView alloc] initWithTitle:@""
                                                          message:@"Please enter user name and password"
                                                         delegate:self
                                                cancelButtonTitle:@"Cancel"
                                                otherButtonTitles:@"OK", nil];
        [alert setAlertViewStyle:SDCAlertViewStyleLoginAndPasswordInput];
        [alert setTitle:@"Login"];
        [[alert textFieldAtIndex:0] setPlaceholder:@"User name"];
        [alert textFieldAtIndex:0].autocorrectionType = UITextAutocorrectionTypeNo;
       
        
        self.spinner = [[UIActivityIndicatorView alloc] init];
        self.spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
        [self.spinner setTranslatesAutoresizingMaskIntoConstraints:NO];
        //[spinner startAnimating];
        [alert.contentView addSubview:spinner];
        [spinner sdc_horizontallyCenterInSuperviewWithOffset:-60.0];
        [spinner sdc_verticallyCenterInSuperviewWithOffset:SDCAutoLayoutStandardSiblingDistance];
        
        
        
        lblspin = [[UILabel alloc] init];
         [lblspin setFont:[UIFont systemFontOfSize:12]];
        [lblspin setTranslatesAutoresizingMaskIntoConstraints:NO];
        [alert.contentView addSubview:lblspin];
        [lblspin sdc_horizontallyCenterInSuperview];
        [lblspin sdc_verticallyCenterInSuperviewWithOffset:SDCAutoLayoutStandardSiblingDistance];
        
        
        [alert show];
    }
    else
    {
        DoctorProfileViewController *doctorprofile=[[DoctorProfileViewController alloc]init];
        BOOL checkDocDetail=[doctorprofile UpdatedoctorPatient_rec];
        NSString *mssg;
        if(checkDocDetail)
        {
            
            mssg =@"In order to change this setting, the complete doctor details & patient details must be entered under User Screen and Doctor Screen";
            SDCAlertView *alert = [[SDCAlertView alloc] initWithTitle:@"Validation"
                                                              message:mssg
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                                    otherButtonTitles:@"OK", nil];
            [alert setAlertViewStyle:SDCAlertViewStyleDefault];
            [alert show];
            
        }
        else
        {
            mssg =@"These settings should only be changed by a qualified physician. By approving this message you approve that you are the attending physician";
            SDCAlertView *alert = [[SDCAlertView alloc] initWithTitle:@"Login"
                                                              message:mssg
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                                    otherButtonTitles:@"OK", nil];
            [alert setAlertViewStyle:SDCAlertViewStylePlainTextInput];
            
            self.spinner = [[UIActivityIndicatorView alloc] init];
            self.spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
            [self.spinner setTranslatesAutoresizingMaskIntoConstraints:NO];
            //[spinner startAnimating];
            [alert.contentView addSubview:spinner];
            [spinner sdc_horizontallyCenterInSuperviewWithOffset:-60.0];
            [spinner sdc_verticallyCenterInSuperviewWithOffset:SDCAutoLayoutStandardSiblingDistance];
            
            
            
            lblspin = [[UILabel alloc] init];
            [lblspin setTranslatesAutoresizingMaskIntoConstraints:NO];
            [alert.contentView addSubview:lblspin];
            [lblspin sdc_horizontallyCenterInSuperview];
            [lblspin sdc_verticallyCenterInSuperviewWithOffset:SDCAutoLayoutStandardSiblingDistance];
            
            
            [alert show];
        }
        
        
        
    }

 

}

- (BOOL)alertView:(SDCAlertView *)alertView shouldDismissWithButtonIndex:(NSInteger)buttonIndex {
      GlobalVars *global = [GlobalVars sharedInstance];
  if(!standalone_Mode)
  {
    lblspin.text=@"";
    
    if(buttonIndex==1)
    {
        [self.spinner startAnimating];
        UITextField *username = [alertView textFieldAtIndex:0];
       // NSLog(@"username: %@", username.text);
        
        
        UITextField *password = [alertView textFieldAtIndex:1];
       // NSLog(@"password: %@", password.text);
        
        
        if(username.text.length == 0 || password.text.length == 0) //check your two textflied has value
        {
            [spinner stopAnimating];
            direction = 1;
            shakes = 0;
            AudioServicesPlaySystemSound (1352);
            [self shake:alertView];
           // NSLog(@"EMPTY");
            
            return NO;
        }
        else
        {
            
            lblspin.text=@"Validating...";
            NSString *domainname =[[NSUserDefaults standardUserDefaults] stringForKey:@"DomainName"];
            
            NSString * Fetchingpath=[NSString stringWithFormat:@"https://%@/MCC/MCCWcfService.svc/CargiverAuthentication/?Username=%@&Password=%@&patientId=%@",domainname,username.text,password.text,patientid];
            NSString *bookurl=[Fetchingpath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *URL1=[NSURL URLWithString:bookurl];
            
           // NSLog(@"url%@",URL1);
            
            NSURLRequest *request = [NSURLRequest requestWithURL:URL1];
            
            
            
            AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
            
            operation.responseSerializer = [AFHTTPResponseSerializer serializer];
            
            
            [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                
               
                NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
              //  NSLog(@"urlresp%@",string);
                
                if([string containsString:@"User Login successfully"])
                {
                    
                    global.dropdownValidFlag=true;
                    lblspin.text=@"OK";
                    [spinner stopAnimating];
                    
                    NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
                    [userDefault setObject:username.text forKey:@"CareGiver"];
                    [userDefault synchronize];
                     lockunlocButton.selected=true;
                     [NSTimer scheduledTimerWithTimeInterval: 120.0 target: self
                                                                      selector: @selector(callAfterSixtySecond:) userInfo: nil repeats:NO];
                    dispatch_async (dispatch_get_main_queue(), ^{
                        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:indexrw inSection:indexpthsec];
                        [self tableView:self.tableViewMcdSettingsTable didSelectRowAtIndexPath:indexPath];
                        
                        [self.tableViewMcdSettingsTable reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                        [self.tableViewMcdSettingsTable scrollToRowAtIndexPath :indexPath atScrollPosition:UITableViewScrollPositionTop animated: true];
                        
                    });
                    [self dismiss:alertView];
                    
                }
                else
                {
                     lockunlocButton.selected=false;
                    AudioServicesPlaySystemSound (1352);
                     string = [string stringByReplacingOccurrencesOfString:@"\"" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [string length])];
                    
                    if([string containsString:@"UserName or password is incorrect"])
                    {
                        lblspin.text= @"The user name or password is incorrect";
                    }
                    else if([string containsString:@"Invalid User"])
                    {
                        lblspin.text= @"Invalid user";
                    }
                    else
                        lblspin.text= string;
                    [spinner stopAnimating];
                    direction = 1;
                    shakes = 0;
                    [self shake:alertView];
                    [alertView textFieldAtIndex:1].text=@"";
                    [alertView textFieldAtIndex:0].text=@"";
                }
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error Retrieving doctors profile"
                                                                    message:[error localizedDescription]
                                                                   delegate:nil
                                                          cancelButtonTitle:@"Ok"
                                                          otherButtonTitles:nil];
                
                
                [alertView show];
                lblspin.text=@"";
                 lockunlocButton.selected=false;
                [spinner stopAnimating];
            }];
            
            
            [operation start];
            // [operation waitUntilFinished];
            return[[operation responseString]boolValue];
            
            
            
        }
        
        
    }
    else
        return YES;
   }
    else
    {
        if(buttonIndex==0)
        {
            return YES;
        }
        lblspin.text=@"";
        
        if(buttonIndex==1)
        {
            [self.spinner startAnimating];
            UITextField *Docname = [alertView textFieldAtIndex:0];
           // NSLog(@"Docname: %@", Docname.text);
            
            
            
            
            if(Docname.text.length == 0) //check your two textfiled has value
            {
                [spinner stopAnimating];
                direction = 1;
                shakes = 0;
                AudioServicesPlaySystemSound (1352);
                [self shake:alertView];
                //NSLog(@"EMPTY");
                
                return NO;
            }
            else
            {
                
                lblspin.text=@"Validating...";
                
                
                if([Docname.text isEqualToString:doctorname])
                {
                    global.dropdownValidFlag=true;
                    lblspin.text=@"OK";
                    [spinner stopAnimating];
                    lockunlocButton.selected=true;
                  
                    [NSTimer scheduledTimerWithTimeInterval: 120.0 target: self
                                                   selector: @selector(callAfterSixtySecond:) userInfo: nil repeats: NO];
                    dispatch_async (dispatch_get_main_queue(), ^{
                        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:indexrw inSection:indexpthsec];
                        [self tableView:self.tableViewMcdSettingsTable didSelectRowAtIndexPath:indexPath];
                        
                        [self.tableViewMcdSettingsTable reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                        [self.tableViewMcdSettingsTable scrollToRowAtIndexPath :indexPath atScrollPosition:UITableViewScrollPositionTop animated: true];
                        
                    });
                    [self dismiss:alertView];
                    
                }
                else
                {
                    lockunlocButton.selected=false;
                    AudioServicesPlaySystemSound (1352);
                    lblspin.text=@"Invalid";
                    [spinner stopAnimating];
                    direction = 1;
                    shakes = 0;
                    [self shake:alertView];
                    [alertView textFieldAtIndex:0].text=@"";
                }
                
                
            }
            
            
        }
        else
            return YES;
        
    }
    
    return NO;
    
}

- (BOOL)alertView:(SDCAlertView *)alertView shouldDeselectButtonAtIndex:(NSInteger)buttonIndex {
    return YES;
}


-(void)shake:(SDCAlertView *)theOneYouWannaShake
{
    [UIView animateWithDuration:0.03 animations:^
     {
         theOneYouWannaShake.transform = CGAffineTransformMakeTranslation(5*direction, 0);
     }
                     completion:^(BOOL finished)
     {
         if(shakes >= 10)
         {
             theOneYouWannaShake.transform = CGAffineTransformIdentity;
             return;
         }
         shakes++;
         direction = direction * -1;
         [self shake:theOneYouWannaShake];
     }];
}

-(void)dismiss:(SDCAlertView*)alert
{
    [alert dismissWithClickedButtonIndex:0 animated:YES];
}

-(void) callAfterSixtySecond:(NSTimer*) t
{
     GlobalVars *global = [GlobalVars sharedInstance];
    NSLog(@"Red");
      global.dropdownValidFlag=false;
     lockunlocButton.selected=false;
         //closing cell after timeout
      NSIndexPath *indexPath = [NSIndexPath indexPathForRow:global.indexpathrow inSection:global.indexpathsection];
    lastOpenIndexPath = indexPath;

   
    NSLog(@"%lu%lu%ld",(unsigned long)global.indexpathrow,(unsigned long)global.indexpathsection
          ,(long)lastOpenIndexPath.row);
    dispatch_async (dispatch_get_main_queue(), ^{
        [cellRowStatus1[lastOpenIndexPath.section] replaceObjectAtIndex:lastOpenIndexPath.row withObject:[NSNumber numberWithInteger:0]];
       /* [self tableView:self.tableViewMcdSettingsTable cellForRowAtIndexPath:lastOpenIndexPath];
        
        tableViewMcdSettingsCell.expandView.hidden = true;
        tableViewMcdSettingsCell.rightArrowBlackImg.transform = CGAffineTransformMakeRotation(6.14);*/
        [self.tableViewMcdSettingsTable reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
         NSLog(@"%@",self.tableViewMcdSettingsTable);
         NSLog(@"%@",cellRowStatus1);
    });
   
   
    
   
   
  
     [self DoctorUniversalLoginTimer:NO];
}

-(void)fetchingpatientdata
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"PatientDetails" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
       // NSLog(@"Unable to execute fetch request.");
       // NSLog(@"%@, %@", error, error.localizedDescription);
        
    } else {
       // NSLog(@"%@", result);
        if (result.count > 0) {
            NSManagedObject *patient = (NSManagedObject *)[result objectAtIndex:0];
           // NSLog(@"1 - %@", patient);
            patientid=[patient valueForKey:@"patient_id"];
            deviceid=[patient valueForKey:@"device_id"];
            doctorname=[patient valueForKey:@"doc_name"];
        }
    }
}


- (void)DoctorLoginStatus:(NSNotification *)notification {
    // Connection status changed. Indicate on GUI.
    BOOL status = [(NSNumber *) (notification.userInfo)[@"NoDoctorLogin"] boolValue];
    
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        // Set image based on connection status
        
        
        if (status) {
            
            lockunlocButton.selected=true;
            
             GlobalVars *global = [GlobalVars sharedInstance];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:global.indexpathrow inSection:global.indexpathsection];
            lastOpenIndexPath = indexPath;
            
            NSLog(@"%lu%lu%ld",(unsigned long)global.indexpathrow,(unsigned long)global.indexpathsection
                  ,(long)lastOpenIndexPath.row);
            dispatch_async (dispatch_get_main_queue(), ^{
                [cellRowStatus1[lastOpenIndexPath.section] replaceObjectAtIndex:lastOpenIndexPath.row withObject:[NSNumber numberWithInteger:0]];
               
                [self.tableViewMcdSettingsTable reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
                NSLog(@"%@",cellRowStatus1);
            });

        }
        else
        lockunlocButton.selected=false;
        
    });
}

- (void)DoctorUniversalLoginTimer:(BOOL)isLoginStatus {
    NSDictionary *connectionDetails = @{@"NoDoctorLogin": @(isLoginStatus)};
    [[NSNotificationCenter defaultCenter] postNotificationName:RWT_DOCTOR_LOGIN_NOTIFICATION object:self userInfo:connectionDetails];
}



-(void)Twavestatus
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"MCDSettings" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *result = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        //   NSLog(@"Unable to execute fetch request.");
        // NSLog(@"%@, %@", error, error.localizedDescription);
        NSLogger *logger=[[NSLogger alloc]init];
        logger.degbugger = true;
        
        [logger log:@"Coredata Response" properties:[NSDictionary dictionaryWithObjectsAndKeys:[error localizedDescription], @"Mcd Setting page", nil] error:TRUE];
        
    } else {
        // NSLog(@"%@", result);
        if (result.count > 0) {
            NSManagedObject *mcdsett = (NSManagedObject *)[result objectAtIndex:0];
            // NSLog(@"1 - %@", mcdsett);
            
            _TWave_Status=[mcdsett valueForKey:@"inverted_t_wave"];
            
                
    }

 }
}


- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationLandscapeRight;
}

- (BOOL)shouldAutorotate {
    return NO;
}

@end
